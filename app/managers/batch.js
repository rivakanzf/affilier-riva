import DictionaryModel from 'coeur/models/dictionary'

import BatchRecord from 'app/records/batch';

import BatchService from 'app/services/batch';
import OrderService from 'app/services/order';

import MeManager from './me';
import OrderDetailManager from './order.detail';

import CommonHelper from 'coeur/helpers/common';


class BatchManager extends DictionaryModel {

	static displayName = 'batches'

	constructor() {
		super(BatchRecord, {
			expiricy: 1000 * 60 * 5,
		})
	}

	dataGetter(id) {
		return BatchService.getBatch(id, MeManager.state.token).then(batch => {
			return this.updateLater(batch)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return BatchService.getBatches(ids, MeManager.state.token).then(batches => {
			return this.updateLater(batches)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getAll({
		status = undefined,
		date = undefined,
		search = undefined,
		offset = 0,
		count = 20,
	} = {}) {
		return BatchService.getFiltered(CommonHelper.stripUndefined({
			status,
			date,
			search,
			offset,
			count,
		}), MeManager.state.token).then(({
			count: _count,
			data,
		}) => {
			return {
				count: _count,
				data: this.updateBulk(data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateStatus({
		id,
		code,
	}) {
		return BatchService.updateStatus({
			id,
			code,
		}, MeManager.state.token).then(batch => {
			return this.update(batch)
		})
	}

	updateOrderStatus({
		orderId,
		orderDetailId,
		code,
	}) {
		return OrderService.updateStatus({
			id: orderId,
			code,
		}, MeManager.state.token).then(() => {
			return OrderDetailManager.dataGetter(orderDetailId)
		})
	}

	updateOrderShipment({
		orderId,
		orderDetailId,
		awb,
		trackLink,
	}) {
		return OrderService.updateShipment({
			id: orderId,
			awb,
			trackLink,
		}, MeManager.state.token).then(() => {
			return OrderDetailManager.dataGetter(orderDetailId)
		})
	}

	updateOrderShipmentViaShipper({
		orderId,
		orderDetailId,
	}) {
		return OrderService.updateShipmentViaShipper({
			id: orderId,
		}, MeManager.state.token).then(() => {
			return OrderDetailManager.dataGetter(orderDetailId)
		})
	}

	getTrackLink({
		orderId,
		orderDetailId,
	}) {
		return OrderService.getTrackLink({
			id: orderId,
		}, MeManager.state.token).then(() => {
			return OrderDetailManager.dataGetter(orderDetailId)
		})
	}
}

export default new BatchManager()
