import DictionaryModel from 'coeur/models/dictionary'

import BrandRecord from 'app/records/brand';

import BrandService from 'app/services/brand';

import MeManager from './me';


class BrandManager extends DictionaryModel {

	static displayName = 'brands'

	constructor() {
		super(BrandRecord, {
			expiricy: 1000 * 60 * 60,
			ordered: true,
		})
	}

	dataGetter(id) {
		return BrandService.getBrand(id, MeManager.state.token).then(brand => {
			return this.updateLater(brand)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return BrandService.getBrands(ids, MeManager.state.token).then(brands => {
			return this.updateLater(brands)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getAll() {
		return BrandService.getBrands(undefined, MeManager.state.token).then(({
			count: _count,
			data,
		}) => {
			return {
				count: _count,
				data: this.updateBulk(data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	searchByTitle({
		title,
		offset,
		count,
	}) {
		return BrandService.searchByTitle({
			title,
			offset,
			count,
		}, MeManager.state.token).then(({
			count: _count,
			data,
		}) => {
			return {
				count: _count,
				data: this.updateBulk(data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createBrand({
		title,
	}) {
		// TODO
		return BrandService.addBrands([{
			title,
		}], MeManager.state.token).then(brands => {
			return this.updateBulk(brands)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createBrands(titles) {
		return BrandService.addBrands(titles.map(title => {
			return {
				title,
			}
		}), MeManager.state.token).then(brands => {
			return this.updateBulk(brands)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateBrand(update) {
		// NOTE: MUST CONTAIN ID
		return BrandService.updateBrands([update], MeManager.state.token).then(brands => {
			return this.updateBulk(brands)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createPickupPoint({
		id,
		title,
		description,
		pic,
		phone,
		address,
		district,
		postal,
		metadata,
	}) {
		// TODO
		return BrandService.createPickupPoint({
			id,
			title,
			description,
			pic,
			phone,
			address,
			district,
			postal,
			metadata,
		}, MeManager.state.token).then(brand => {
			return this.update(brand)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new BrandManager()
