import DictionaryModel from 'coeur/models/dictionary'

import CouponRecord from 'app/records/coupon'

import CouponService from 'app/services/coupon'

import MeManager from './me';


class CouponManager extends DictionaryModel {

	static displayName = 'coupons'

	constructor() {
		super(CouponRecord, {
			expiricy: 1000 * 60 * 60,
		})
	}

	// transformConfig(config = {}) {
	// 	return super.transformConfig({
	// 		...config,
	// 		email: config.email || config.eail,
	// 	})
	// }

	dataGetter(id) {
		return CouponService.getCoupon(id, MeManager.state.token).then(media => {
			return this.updateLater(media)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return CouponService.getCoupons(ids, MeManager.state.token).then(medias => {
			return this.updateLater(medias)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getAll({
		title,
		offset,
		count,
	} = {}) {
		return CouponService.filterCoupons({
			title,
			offset,
			count,
		}, MeManager.state.token).then(result => {
			return {
				count: result.count,
				data: this.updateLater(result.data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createCoupon({
		title,
		description,
		type,
		amount,
		expiry,
		minimumSpend,
		maximumSpend,
		excludeSale,
		usageLimit,
		usageLimitItem,
		usageLimitPerUser,
		matchboxIds = [],
	}) {
		// TODO
		return CouponService.addCoupons([{
			title,
			description,
			type,
			amount,
			expiry,
			minimumSpend,
			maximumSpend,
			excludeSale,
			usageLimit,
			usageLimitItem,
			usageLimitPerUser,
			matchboxIds,
		}], MeManager.state.token).then(coupons => {
			return this.updateBulk(coupons)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateCoupon({
		id,
		title,
		description,
		type,
		amount,
		expiry,
		minimumSpend,
		maximumSpend,
		excludeSale,
		usageLimit,
		usageLimitItem,
		usageLimitPerUser,
		matchboxIds = [],
	}) {
		// NOTE: MUST CONTAIN ID
		return CouponService.updateCoupons([{
			id,
			title,
			description,
			type,
			amount,
			expiry,
			minimumSpend,
			maximumSpend,
			excludeSale,
			usageLimit,
			usageLimitItem,
			usageLimitPerUser,
			matchboxIds,
		}], MeManager.state.token).then(products => {
			return this.updateBulk(products)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new CouponManager()
