import DictionaryModel from 'coeur/models/dictionary'

import ProductRecord from 'app/records/product';

import ProductService from 'app/services/product';

import MeManager from './me';


class ProductManager extends DictionaryModel {

	static displayName = 'products'

	constructor() {
		super(ProductRecord, {
			expiricy: 1000 * 60 * 60,
		})
	}

	dataGetter(id) {
		return ProductService.getProduct(id, MeManager.state.token).then(product => {
			return this.updateLater(product)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return ProductService.getProducts(ids, MeManager.state.token).then(products => {
			return this.updateLater(products)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	searchByTitle({
		title,
		brandId,
		offset,
		count,
	}) {
		return ProductService.searchByTitle({
			title,
			brandId,
			offset,
			count,
		}, MeManager.state.token).then(({
			count: _count,
			data,
		}) => {
			return {
				count: _count,
				data: this.updateBulk(data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createProduct({
		brandId,
		title,
		spu,
		url,
		metadata,
	}) {
		// TODO
		return ProductService.addProducts([{
			brandId,
			title,
			spu,
			url,
			metadata,
		}], MeManager.state.token).then(products => {
			return this.updateBulk(products)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateProduct(update) {
		// NOTE: MUST CONTAIN ID
		return ProductService.updateProducts([update], MeManager.state.token).then(products => {
			return this.updateBulk(products)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new ProductManager()
