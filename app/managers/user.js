import DictionaryModel from 'coeur/models/dictionary'

import UserRecord from 'app/records/user'

import TypeformService from 'app/services/typeform';
import UserService from 'app/services/user'

import MeManager from './me';


class UserManager extends DictionaryModel {

	static displayName = 'users'

	constructor() {
		super(UserRecord, {
			expiricy: 1000 * 60 * 60,
		})
	}

	// transformConfig(config = {}) {
	// 	return super.transformConfig({
	// 		...config,
	// 		email: config.email || config.eail,
	// 	})
	// }

	dataGetter(id) {
		return UserService.getUser(id, MeManager.state.token).then(media => {
			return this.updateLater(media)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return UserService.getUsers(ids, MeManager.state.token).then(medias => {
			return this.updateLater(medias)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getStyleProfile({
		userId,
	}) {
		const user = this.get(userId)

		return user.isStyleProfileCompleted ? TypeformService.getByEmail(user.email, MeManager.state.token).catch(err => {
			this.warn(err)

			throw err
		}) : Promise.reject(false)
	}

	sendSPQNotification({
		userId,
	}) {
		const user = this.get(userId)

		return user.isStyleProfileCompleted ? Promise.reject(false) : UserService.sendSPQNotification(userId, MeManager.state.token).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new UserManager()
