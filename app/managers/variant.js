import DictionaryModel from 'coeur/models/dictionary'

import VariantRecord from 'app/records/variant';

import VariantService from 'app/services/variant';

import MeManager from './me';
import InventoryManager from './inventory';


class VariantManager extends DictionaryModel {

	static displayName = 'variants'

	constructor() {
		super(VariantRecord, {
			expiricy: 1000 * 60 * 5,
		})
	}

	dataGetter(id) {
		return VariantService.getVariant(id, MeManager.state.token).then(variant => {
			return this.updateLater(variant)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		// return VariantService.getVariants(ids, MeManager.state.token).then(variants => {
		// 	variants.forEach(variant => {
		// 		if(variant.inventories && variant.inventories.length) {
		// 			InventoryManager.updateLater(variant.inventories)
		// 		}
		// 	})

		// 	return this.updateLater(variants)
		// }).catch(err => {
		// 	this.warn(err)

		// 	throw err
		// })
	}

	getAll() {
		return VariantService.getVariants(undefined, MeManager.state.token).then(result => {
			return {
				count: result.count,
				data: this.updateLater(result.data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getAllWithInventory({
		offset,
		count,
		brandId,
		color,
		size,
		productTitle,
	}) {
		return VariantService.getVariantsWithInventory({
			offset,
			count,
			brandId,
			color,
			size,
			productTitle,
		}, MeManager.state.token).then(result => {
			return {
				count: result.count,
				data: this.updateLater(result.data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	searchByProductId({
		productId,
		search,
		offset,
		count,
	}) {
		return VariantService.searchByProductId({
			productId,
			search,
			offset,
			count,
		}, MeManager.state.token).then(({
			count: _count,
			data,
		}) => {
			return {
				count: _count,
				data: this.updateBulk(data),
			}
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	createVariant({
		productId,
		url,
		size,
		color,
		basePrice,
		price,
		sku,
		metadata,
	}) {
		// TODO
		return VariantService.addVariants([{
			productId,
			url,
			size,
			color,
			basePrice,
			price,
			sku,
			metadata,
		}], MeManager.state.token).then(variants => {
			return this.updateBulk(variants)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateVariant(update) {
		// NOTE: MUST CONTAIN ID
		return VariantService.updateVariants([update], MeManager.state.token).then(variants => {
			return this.updateBulk(variants)[0]
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	updateVariantMedias({
		id,
		mediaIds,
	}) {
		return VariantService.updateMedias({
			id,
			mediaIds,
		}, MeManager.state.token).then(variant => {
			return this.update(variant)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	addVariantInventories({
		id,
		inventoryIds,
	}) {
		return VariantService.addInventories({
			id,
			inventoryIds,
		}, MeManager.state.token).then(variant => {
			return this.update(variant)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new VariantManager()
