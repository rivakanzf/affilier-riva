import RecordModel from 'coeur/models/record'


function BrandRecordFactory(additionalConfig = {}) {
	return class BrandRecord extends RecordModel('brand', {
		id				: RecordModel.TYPES.ID,
		title			: RecordModel.TYPES.STRING,
		mediaIds		: RecordModel.TYPES.ARRAY,
		pickupPointIds	: RecordModel.TYPES.ARRAY,
		productIds		: RecordModel.TYPES.ARRAY,

		...additionalConfig,
	}) {
	}
}

export default BrandRecordFactory
