import RecordModel from 'coeur/models/record'

class StatusRecord extends RecordModel('status', {
	code			: RecordModel.TYPES.STRING,
	note			: RecordModel.TYPES.STRING,
	createdAt		: RecordModel.TYPES.DATE,
}) {}


function InventoryRecordFactory(additionalConfig = {}) {
	return class InventoryRecord extends RecordModel('inventory', {
		id				: RecordModel.TYPES.ID,

		pickupPointId	: RecordModel.TYPES.ID,
		purchasePrice	: RecordModel.TYPES.NUMBER,

		note			: RecordModel.TYPES.STRING,

		statuses		: RecordModel.TYPES.ARRAY,

		...additionalConfig,
	}) {
		get status() {
			return this.statuses[this.statuses.length - 1] || new StatusRecord()
		}
	}
}

InventoryRecordFactory.statusRecord = StatusRecord
InventoryRecordFactory.getModel = function(config = {}) {
	return {
		...config,
		statuses: config.statuses && config.statuses.map(status => new StatusRecord(status)),
	}
}

export default InventoryRecordFactory
