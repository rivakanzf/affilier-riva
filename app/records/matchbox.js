import RecordModel from 'coeur/models/record';


function MatchboxRecordFactory(additionalConfig = {}) {
	return class MatchboxRecord extends RecordModel('matchbox', {
		id				: RecordModel.TYPES.ID,
		title			: RecordModel.TYPES.STRING,
		description		: RecordModel.TYPES.STRING,
		url				: RecordModel.TYPES.STRING,
		type			: RecordModel.TYPES.STRING,
		count			: RecordModel.TYPES.STRING,

		images			: RecordModel.TYPES.ARRAY,

		basePrice		: RecordModel.TYPES.NUMBER,
		price			: RecordModel.TYPES.NUMBER,
		value			: RecordModel.TYPES.NUMBER,

		ownerBrandId	: RecordModel.TYPES.ID,
		sellerBrandId	: RecordModel.TYPES.ID,

		feature			: RecordModel.TYPES.ARRAY,
		warranty		: RecordModel.TYPES.ARRAY,

		metadata		: RecordModel.TYPES.OBJECT,
		publishedAt		: RecordModel.TYPES.DATE,

		...additionalConfig,
	}) {
		get image() {
			return this.images && this.images.length && this.images[0]
		}

		get isDiscounted() {
			return this.basePrice !== this.price
		}

		get isNew() {
			return this.publishedAt - Date.now() < 1000 * 60 * 60 * 24 * 30
		}
	}
}

MatchboxRecordFactory.featureRecord = class MatchboxFeatureRecord extends RecordModel('feature', {
	title			: RecordModel.TYPES.STRING,
	isActive		: RecordModel.TYPES.BOOL.TRUE,
}) {}


export default MatchboxRecordFactory
