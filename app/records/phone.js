import RecordModel from 'coeur/models/record'

export default function PhoneRecordFactory(additionalConfig = {}) {
	return class PhoneRecord extends RecordModel('phone', {
		id				: RecordModel.TYPES.ID,
		title			: RecordModel.TYPES.STRING,
		country			: RecordModel.TYPES.STRING,
		number			: RecordModel.TYPES.STRING,

		...additionalConfig,
	}) {
	}
}
