import RecordModel from 'coeur/models/record'


function ProductRecordFactory(additionalConfig = {}) {
	return class ProductRecord extends RecordModel('product', {
		id				: RecordModel.TYPES.ID,
		brandId			: RecordModel.TYPES.ID,
		title			: RecordModel.TYPES.STRING,
		spu				: RecordModel.TYPES.STRING,
		url				: RecordModel.TYPES.STRING,
		metadata		: RecordModel.TYPES.OBJECT,

		variantIds		: RecordModel.TYPES.ARRAY,

		...additionalConfig,
	}) {
	}
}

ProductRecordFactory.getModel = function(config = {}) {
	return {
		...config,
		variantIds: config.variantIds || (config.variants && config.variants.map(variant => variant.id)),
	}
}


export default ProductRecordFactory
