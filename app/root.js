import React from 'react'
import Root from 'coeur/kernel/root'

import AddressManager from 'app/managers/address';
import BrandManager from 'app/managers/brand';
import InventoryManager from 'app/managers/inventory';
import PickupPointManager from 'app/managers/pickup.point';
import ProductManager from 'app/managers/product';
import VariantManager from 'app/managers/variant';

import BatchManager from 'app/managers/batch';
import CouponManager from 'app/managers/coupon';
import MeManager from 'app/managers/me';
import MediaManager from 'app/managers/media';
import OrderDetailManager from 'app/managers/order.detail';
// import PageManager from 'app/managers/page';
import UserManager from 'app/managers/user';
// import VolatileManager from 'app/managers/volatile';

import BoxBit from 'modules/bits/box';
// import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';

import AlertPagelet from 'modules/pagelets/alert';
import MenuPagelet from 'modules/pagelets/menu';
import NotificationPagelet from 'modules/pagelets/notification';

import Router from './router'

export default class Weblique extends Root {

	initialize(Store) {
		if(window) {
			window.screen
			&& window.screen.orientation
			&& window.screen.orientation.lock
			&& window.screen.orientation.lock('portrait').catch(err => {
				if(process.env.DEBUG === 'true') {
					console.warn(err)
				}
			})

			this.state.isPortrait = window.orientation !== -90 && window.orientation !== 90
		}

		return new Store([
			AddressManager,
			BrandManager,
			InventoryManager,
			PickupPointManager,
			ProductManager,
			VariantManager,

			BatchManager,
			CouponManager,
			// MatchboxManager,
			MeManager,
			MediaManager,
			OrderDetailManager,
			// PageManager,
			UserManager,
			// VolatileManager,
		], process.env.DEBUG === 'true')
	}

	initializeGraphQL(GraphQL) {
		return GraphQL.init(process.env.CONNECTER_URL + process.env.GRAPHQL_PATH)
	}

	onAppStateChange() {
		super.onAppStateChange(document.visibilityState)
	}

	onConnectivityChange(connection) {
		super.onConnectivityChange(connection.type !== 'offline')
	}

	onOrientationChange() {
		super.onOrientationChange(window.orientation)
	}

	componentDidMount() {
		document.addEventListener('visibilitychange', this.onAppStateChange);
		window.addEventListener('online', this.onConnectivityChange);
		window.addEventListener('offline', this.onConnectivityChange);
		window.addEventListener('orientationchange', this.onOrientationChange);
	}

	componentWillUnmount() {
		document.removeEventListener('visibilitychange', this.onAppStateChange);
		window.removeEventListener('online', this.onConnectivityChange);
		window.removeEventListener('offline', this.onConnectivityChange);
		window.removeEventListener('orientationchange', this.onOrientationChange);
	}

	registerUtilities() {
		super.registerUtilities({
			MENU_TYPES: MenuPagelet.TYPES,
			NOTIFICATION_TYPES: NotificationPagelet.TYPES,
		})
	}

	render() {
		return super.render({
			Navigator: function() {
				return (
					<Router.BrowserRouter>
						<Router.Route />
					</Router.BrowserRouter>
				)
			},
			AlertPagelet,
			MenuPagelet,
			NotificationPagelet,
			BoxBit,
			LoaderBit,
		})
	}
}
