import React from 'react';
import Loadable from 'react-loadable';
import LoaderPage from 'modules/pages/loader';

import MeManager from 'app/managers/me';


import {
	BrowserRouter,
	HashRouter,
	Route,
	Switch,
	Redirect,
} from 'react-router-dom';

const PrivateRoute = function({ component: Component, /* roles = [], */ ...rest }) {
	return (
		<Route {...rest} render={function(props) {
			const isAuthorized = !!MeManager.state.token

			return isAuthorized ? (
				<Component { ...props } />
			) : (
				<Redirect to="/login" />
			)
		}} />
	)
}

export default {
	BrowserRouter,
	HashRouter,
	Route() {
		return (
			<Switch>
				<Route exact path="/login" component={ Loadable({
					loader: () => import('modules/pages/login').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/" component={ Loadable({
					loader: () => import('modules/pages/profile').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/order" component={ Loadable({
					loader: () => import('modules/pages/order').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/order/add" component={ Loadable({
					loader: () => import('modules/pages/order.add').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/order/:id" component={ Loadable({
					loader: () => import('modules/pages/order.update').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/client" component={ Loadable({
					loader: () => import('modules/pages/client').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/client/:id" component={ Loadable({
					loader: () => import('modules/pages/client.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/collections" component={ Loadable({
					loader: () => import('modules/pages/collection').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/collection/create" component={ Loadable({
					loader: () => import('modules/pages/collection.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/collection/:id" component={ Loadable({
					loader: () => import('modules/pages/collection.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/coupons" component={ Loadable({
					loader: () => import('modules/pages/coupon').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/coupon/create" component={ Loadable({
					loader: () => import('modules/pages/coupon.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/coupon/:id" component={ Loadable({
					loader: () => import('modules/pages/coupon.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/request" component={Loadable({
					loader: () => import('modules/pages/request').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				{/* <PrivateRoute exact path="/payment-links" component={ Loadable({
					loader: () => import('modules/pages/payment.link'),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/payment-links/create" component={ Loadable({
					loader: () => import('modules/pages/add.payment.link'),
					loading: LoaderPage,
				}) } /> */}

				{/* <PrivateRoute exact path="/external-voucher" component={ Loadable({
					loader: () => import('modules/pages/ext.voucher'),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/external-voucher/detail" component={ Loadable({
					loader: () => import('modules/pages/ext.voucher.detail'),
					loading: LoaderPage,
				}) } /> */}

				<PrivateRoute exact path="/styleboard" component={ Loadable({
					loader: () => import('modules/pages/styleboard').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/styleboard/stylist" component={Loadable({
					loader: () => import('modules/pages/styleboard.stylist').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/styleboard/:id" component={Loadable({
					loader: () => import('modules/pages/styleboard.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/stylecard" component={Loadable({
					loader: () => import('modules/pages/stylecard').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/cluster" component={Loadable({
					loader: () => import('modules/pages/cluster').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/cluster/:id" component={Loadable({
					loader: () => import('modules/pages/cluster.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/stylecard/:id" component={Loadable({
					loader: () => import('modules/pages/stylecard.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/stylesheet" component={ Loadable({
					loader: () => import('modules/pages/stylesheet').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/stylesheet/stylist" component={ Loadable({
					loader: () => import('modules/pages/stylesheet.stylist').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/stylesheet/:id" component={ Loadable({
					loader: () => import('modules/pages/stylesheet.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/stylesheet/:id/print/invoice" component={ Loadable({
					loader: () => import('modules/pages/print.invoice').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/stylesheet/:id/print/stylist/note" component={ Loadable({
					loader: () => import('modules/pages/print.stylist.note').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/stylesheet/:id/print/address" component={ Loadable({
					loader: () => import('modules/pages/print.address').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/barcode" component={ Loadable({
					loader: () => import('modules/pages/barcode').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/address/note" component={ Loadable({
					loader: () => import('modules/pages/print.address').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/shipping/:id/print/barcode" component={ Loadable({
					loader: () => import('modules/pages/print.barcode').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				{/* <PrivateRoute exact path="/style.sheets/exchange" component={ Loadable({
					loader: () => import('modules/pages/style.sheets.exchange'),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/style.sheets/stylist" component={ Loadable({
					loader: () => import('modules/pages/style.sheets.stylist'),
					loading: LoaderPage,
				}) } /> */}

				{/* <PrivateRoute exact path="/brand" component={ Loadable({
					loader: () => import('modules/pages/brand'),
					loading: LoaderPage,
				}) } /> */}
				<PrivateRoute exact path="/deals" component={ Loadable({
					loader: () => import('modules/pages/product.deals').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/inventory/booking" component={ Loadable({
					loader: () => import('modules/pages/inventory.booking').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/stylist-inventory" component={ Loadable({
					loader: () => import('modules/pages/inventory.stylist').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/product/add" component={ Loadable({
					loader: () => import('modules/pages/product.update').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/product/:id?" component={ Loadable({
					loader: () => import('modules/pages/product.update').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/product/:productId/variant/add" component={ Loadable({
					loader: () => import('modules/pages/product.variation').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/product/:productId/variant/:id" component={ Loadable({
					loader: () => import('modules/pages/product.variation').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/exchange" component={ Loadable({
					loader: () => import('modules/pages/exchange').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				<PrivateRoute exact path="/exchange/:id" component={ Loadable({
					loader: () => import('modules/pages/exchange.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/purchase" component={Loadable({
					loader: () => import('modules/pages/purchase').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				{/* <PrivateRoute exact path="/purchase/fulfillment" component={ Loadable({
					loader: () => import('modules/pages/purchase.fulfillment').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/purchase/purchasing" component={ Loadable({
					loader: () => import('modules/pages/purchase.purchasing').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/purchase/inventory" component={ Loadable({
					loader: () => import('modules/pages/purchase.inventory').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />
				<PrivateRoute exact path="/purchase/order" component={ Loadable({
					loader: () => import('modules/pages/purchase.order').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} /> */}
				<PrivateRoute exact path="/purchase/order/:id" component={ Loadable({
					loader: () => import('modules/pages/purchase.order.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/print" component={ Loadable({
					loader: () => import('modules/pages/print').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/shipment" component={Loadable({
					loader: () => import('modules/pages/shipment').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/profile" component={ Loadable({
					loader: () => import('modules/pages/profile').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/broadcast" component={ Loadable({
					loader: () => import('modules/pages/broadcast').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/broadcast/detail" component={ Loadable({
					loader: () => import('modules/pages/broadcast.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/broadcast/detail/:id" component={ Loadable({
					loader: () => import('modules/pages/broadcast.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />
				
				<PrivateRoute exact path="/report/inventory" component={ Loadable({
					loader: () => import('modules/pages/report.inventory').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/report/BCA" component={ Loadable({
					loader: () => import('modules/pages/report.bca').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/campaign" component={ Loadable({
					loader: () => import('modules/pages/campaign').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/discounts" component={ Loadable({
					loader: () => import('modules/pages/discount').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				<PrivateRoute exact path="/discount/create" component={ Loadable({
					loader: () => import('modules/pages/discount.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/discount/:id" component={ Loadable({
					loader: () => import('modules/pages/discount.detail').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/summary" component={ Loadable({
					loader: () => import('modules/pages/summary').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/custom" component={ Loadable({
					loader: () => import('modules/pages/custom.link').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<PrivateRoute exact path="/report" component={ Loadable({
					loader: () => import('modules/pages/report').catch(err => console.warn(err)),
					loading: LoaderPage,
				})} />

				<Route component={ Loadable({
					loader: () => import('modules/pages/four.o.four').catch(err => console.warn(err)),
					loading: LoaderPage,
				}) } />

				
				{/* <Redirect from="*" to="/404" /> */}

			</Switch>
		)
	},
}
