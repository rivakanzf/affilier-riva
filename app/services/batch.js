import ServiceModel from 'coeur/models/service';


class BatchService extends ServiceModel {

	static displayName = 'batch'

	constructor() {
		super(process.env.API_URL + 'admin/batch/', 30 * 1000)
	}

	getBatches(ids = [], token) {
		return this.get(ids.join(','), {
			token,
		})
	}

	getBatch(id, token) {
		return this.get(id, {
			token,
		})
	}

	getFiltered(filter, token) {
		return this.get('', {
			query: filter,
			token,
		})
	}

	updateStatus({
		id,
		code,
	}, token) {
		return this.post(id + '/status', {
			data: {
				code,
			},
			token,
		})
	}
}

export default new BatchService()
