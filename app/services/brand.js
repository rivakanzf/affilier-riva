import ServiceModel from 'coeur/models/service';


class BrandService extends ServiceModel {

	static displayName = 'brand'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/brand/', 0)
	}

	// getBrands(ids = [], token) {
	// 	return this.get(ids.join(','), {
	// 		token,
	// 	})
	// }

	// getBrand(id, token) {
	// 	return this.get(id, {
	// 		token,
	// 	})
	// }

	addBrand(title, token) {
		return this.post('', {
			data: {
				title,
			},
			token,
		})
	}

	createAddress(brandId, data, token) {
		return this.post( brandId + '/address', {
			data,
			token,
		})
	}

	getBrandByCategories(categoryIds, token) {
		return this.get('', {
			query: {
				category_ids: categoryIds.join(','),
			},
			token,
		})
	}

	getAddress(brandId, addressId, token) {
		return this.get( brandId + '/address/' + addressId, {
			token,
		})
	}

	updateAddress(brandId, addressId, data, token) {
		return this.post(brandId + '/address/' + addressId, {
			data,
			token,
		})
	}

	// addBrands(brands, token) {
	// 	return this.post('', {
	// 		data: {
	// 			creates: brands,
	// 		},
	// 		token,
	// 	})
	// }

	// updateBrands(brands, token) {
	// 	return this.post('', {
	// 		data: {
	// 			updates: brands,
	// 		},
	// 		token,
	// 	})
	// }

	// searchByTitle({
	// 	title,
	// 	offset,
	// 	count,
	// }, token) {
	// 	return this.get('', {
	// 		query: {
	// 			title,
	// 			offset,
	// 			count,
	// 		},
	// 		token,
	// 	})
	// }

	// getPickupPoints(brandId, token) {
	// 	return this.get(brandId + '/pickuppoint', {
	// 		token,
	// 	})
	// }

	// createPickupPoint({
	// 	id,
	// 	title,
	// 	description,
	// 	pic,
	// 	phone,
	// 	address,
	// 	district,
	// 	postal,
	// 	metadata,
	// }, token) {
	// 	return this.post(id + '/pickuppoint', {
	// 		data: {
	// 			pickupPoint: {
	// 				title,
	// 				description,
	// 			},
	// 			address: {
	// 				pic,
	// 				phone,
	// 				address,
	// 				district,
	// 				postal,
	// 				metadata,
	// 			},
	// 		},
	// 		token,
	// 	})
	// }
}

export default new BrandService()
