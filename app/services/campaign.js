import ServiceModel from 'coeur/models/service';

class CampaignService extends ServiceModel {
	static displayName = 'campaign'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/campaign/', 0)
	}

	getCampaigns(query, token) {
		return this.get('', {
			query,
			token,
		})
	}
}

export default new CampaignService()
