import ServiceModel from 'coeur/models/service';


class CategoryService extends ServiceModel {

	static displayName = 'category'

	constructor() {
		super(process.env.CONNECTER_URL + process.env.CONNECTER_VERSION + 'category/', 0)
	}

	// getBrands(ids = [], token) {
	// 	return this.get(ids.join(','), {
	// 		token,
	// 	})
	// }

	// getBrand(id, token) {
	// 	return this.get(id, {
	// 		token,
	// 	})
	// }

	getRootCategory(id, token) {
		return this.get(id + '/root', {
			token,
		})
	}

	getCategory(id, token) {
		return this.get(id, {
			token,
		})
	}

	// addBrands(brands, token) {
	// 	return this.post('', {
	// 		data: {
	// 			creates: brands,
	// 		},
	// 		token,
	// 	})
	// }

	// updateBrands(brands, token) {
	// 	return this.post('', {
	// 		data: {
	// 			updates: brands,
	// 		},
	// 		token,
	// 	})
	// }

	// searchByTitle({
	// 	title,
	// 	offset,
	// 	count,
	// }, token) {
	// 	return this.get('', {
	// 		query: {
	// 			title,
	// 			offset,
	// 			count,
	// 		},
	// 		token,
	// 	})
	// }

	// getPickupPoints(brandId, token) {
	// 	return this.get(brandId + '/pickuppoint', {
	// 		token,
	// 	})
	// }

	// createPickupPoint({
	// 	id,
	// 	title,
	// 	description,
	// 	pic,
	// 	phone,
	// 	address,
	// 	district,
	// 	postal,
	// 	metadata,
	// }, token) {
	// 	return this.post(id + '/pickuppoint', {
	// 		data: {
	// 			pickupPoint: {
	// 				title,
	// 				description,
	// 			},
	// 			address: {
	// 				pic,
	// 				phone,
	// 				address,
	// 				district,
	// 				postal,
	// 				metadata,
	// 			},
	// 		},
	// 		token,
	// 	})
	// }
}

export default new CategoryService()
