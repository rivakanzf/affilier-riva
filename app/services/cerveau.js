import ServiceModel from 'coeur/models/service';


class CerveauService extends ServiceModel {

	static displayName = 'user'

	constructor() {
		super(process.env.CERVEAU_URL + 'cluster/', 0)
	}
	
	getUserCluster(id) {
		return this.get(`user/${id}`)
	}
}

export default new CerveauService()
