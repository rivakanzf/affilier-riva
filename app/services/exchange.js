import ServiceModel from 'coeur/models/service';


class ExchangeService extends ServiceModel {

	static displayName = 'exchange'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/exchange/', 0)
	}

	filter(query, token) {
		return this.get('', {
			query,
			token,
		})
	}

	getDetail(id, token) {
		return this.get(id, {
			token,
		})
	}

	getDetails(id, token) {
		return this.get(id + '/details', {
			token,
		})
	}

	process(id, data, token) {
		return this.post(id + '/process', {
			data,
			token,
		})
	}

	updateToRefund(id, token) {
		return this.post('detail/' + id + '/refund', {
			token,
		})
	}

	updateResi(id, data, token) {
		return this.post(id + '/shipment', {
			data,
			token,
		})
	}

	createReplacementShipment(id, data, token) {
		return this.post(id + '/shipment', {
			data,
			token,
		})
	}

}

export default new ExchangeService()
