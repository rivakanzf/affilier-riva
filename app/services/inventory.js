import ServiceModel from 'coeur/models/service';


class InventoryService extends ServiceModel {

	static displayName = 'inventory'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/inventory/', 0)
	}

	filter(filter, token) {
		return this.get('', {
			query: filter,
			token,
		})
	}

	book(id, note, type, typeId, token) {
		return this.post(id + '/book', {
			data: {
				note,
				type,
				type_id: typeId,
			},
			token,
		})
	}

	cancelBooking(id, note, status, type, typeId, force, token) {
		return this.delete(id + '/book', {
			data: {
				status,
				note,
				type,
				type_id: typeId,
				force,
			},
			token,
		})
	}

	unpackInventory(id, note, status, stylesheetId, token) {
		return this.post(id + '/unpack', {
			data: {
				status,
				note,
				stylesheet_id: stylesheetId,
			},
			token,
		})
	}

	getInventories(ids = [], token) {
		return this.get(ids.join(','), {
			token,
		})
	}

	getInventory(id, token) {
		return this.get(id, {
			token,
		})
	}

	addInventories(inventories, token) {
		return this.post('', {
			data: {
				creates: inventories,
			},
			token,
		})
	}

	updateInventories(inventories, token) {
		return this.post('', {
			data: {
				updates: inventories,
			},
			token,
		})
	}

	deleteInventories(ids, token) {
		return this.delete('', {
			data: {
				ids,
			},
			token,
		})
	}

	updateStatus({
		id,
		code,
		note,
	}, token) {
		return this.post(id + '/status', {
			data: {
				code,
				note,
			},
			token,
		})
	}
}

export default new InventoryService()
