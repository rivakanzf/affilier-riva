import ServiceModel from 'coeur/models/service';


class LocationService extends ServiceModel {

	static displayName = 'location'

	constructor() {
		super(process.env.CONNECTER_URL + process.env.CONNECTER_VERSION + 'location/')
	}

	getAreas(postal) {
		return this.get('areas', {
			query: {
				postal,
			},
		})
	}

	getAreasBySuburb(suburbId) {
		return this.get('areas', {
			query: {
				suburb_id: suburbId,
			},
		})
	}

	searchAreas(search) {
		return this.get('areas', {
			query: {
				search,
			},
		})
	}

	getSuburbs(cityId) {
		return this.get('suburbs', {
			query: {
				city_id: cityId,
			},
		})
	}

	getCities(provinceId) {
		return this.get('cities', {
			query: {
				province_id: provinceId,
			},
		})
	}

	getProvinces() {
		return this.get('provinces')
	}

	getLocation(locationId) {
		return this.get(locationId)
	}
}

export default new LocationService()
