import ServiceModel from 'coeur/models/service';


class MailService extends ServiceModel {

	static displayName = 'mail'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/mail/', 0)
	}

	getLatestPaymentNotification(orderId, token) {
		return this.get('checkout/' + orderId, {
			token,
		})
	}

	getLatestShipmentNotification(shipmentIds, token) {
		return this.get('shipment/' + shipmentIds.join(','), {
			token,
		})
	}

	getLatestSPQNotification(userId, token) {
		return this.get('spq/' + userId, {
			token,
		})
	}

	getLatestFeedbackNotification(userId, token) {
		return this.get('feedback/' + userId, {
			token,
		})
	}

	sendPaymentNotification(orderId, token) {
		return this.post('checkout/' + orderId, {
			token,
		})
	}

	sendShipmentNotification(shipmentId, token) {
		return this.post('shipment/' + shipmentId, {
			token,
		})
	}

	sendSPQNotification(userId, token) {
		return this.post('spq/' + userId, {
			token,
		})
	}

	sendFeedbackNotification(userId, data, token) {
		return this.post('feedback/' + userId, {
			data,
			token,
		})
	}
}

export default new MailService()
