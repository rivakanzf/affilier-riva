import ServiceModel from 'coeur/models/service';


class MeService extends ServiceModel {

	static displayName = 'me'

	constructor() {
		super(process.env.CONNECTER_URL + process.env.CONNECTER_VERSION + 'user/')
	}

	authenticate(token) {
		return this.get('', {
			token,
		})
	}
}

export default new MeService()
