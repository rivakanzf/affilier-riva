import ServiceModel from 'coeur/models/service';


class MediaService extends ServiceModel {

	static displayName = 'media'

	constructor() {
		super(process.env.API_URL + 'admin/media/', 0)
	}

	getMedias(ids = [], token) {
		return this.get(ids.join(','), {
			token,
		})
	}

	getMedia(id, token) {
		return this.get(id, {
			token,
		})
	}

	upload({
		image,
		onUploadProgress,
		onDownloadProgress,
	}, token) {
		return this.post('', {
			data: {
				image,
			},
			onUploadProgress,
			onDownloadProgress,
			token,
		})
	}
}

export default new MediaService()
