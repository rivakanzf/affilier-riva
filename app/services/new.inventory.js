import ServiceModel from 'coeur/models/service';


class InventoryService extends ServiceModel {

	static displayName = 'order'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/inventory/')
	}

	getDetail = this._createSmartGetters((ids, params) => {
		return this.get(ids.join(','), {
			query: params.query,
			token: params.token,
		})
	}, (id, params) => {
		return this.get(id, {
			query: params.query,
			token: params.token,
		})
	}, function(id, [ids, datas]) {
		if (ids.length) {
			return datas.find(d => d.id === id)
		} else {
			return datas[0]
		}
	})

	getPurchaseRequest(id, token) {
		return this.get('purchase/' + id, {
			token,
		})
	}

	getAllInventory(inventory, token) {
		return this.get('', {
			query: inventory,
			token,
		})
	}

	getStatusHistory(id, token) {
		return this.get(id + '/status', {
			token,
		})
	}

	addStatus(id, data, token) {
		return this.post(id + '/status', {
			data,
			token,
		})
	}

	history(id, token) {
		return this.get(id + '/history', {
			token,
		})
	}

	createInventoryItem(data, token) {
		return this.post('', {
			data,
			token,
		})
	}

	updateInventoryItem(id, data, token) {
		return this.post(id, {
			data,
			token,
		})
	}

	onRemoveInventoryFromVariant(ids, token) {
		if(ids.length) {
			return this.delete(`${ids.join(',')}`, {
				query: {
					ids: ids.join(','),
				},
				token,
			})
		}

		return this.delete('', {
			query: ids,
			token,
		})
	}
}

export default new InventoryService()
