import ServiceModel from 'coeur/models/service';


class PointService extends ServiceModel {

	static displayName = 'wallet'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/point/', 0)
	}

	getUserPoint(userId, token) {
		return this.get('user/' + userId, {
			token,
		})
	}

	depositIntoUser(userId, amount, note, token) {
		return this.post('user/' + userId + '/add', {
			data: {
				amount,
				note,
			},
			token,
		})
	}

	deductUser(userId, amount, note, token) {
		return this.post('user/' + userId + '/subtract', {
			data: {
				amount,
				note,
			},
			token,
		})
	}
}

export default new PointService()
