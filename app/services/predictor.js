import ServiceModel from 'coeur/models/service';


class PredictorService extends ServiceModel {

	static displayName = 'predictor'

	constructor() {
		super(process.env.CERVEAU_URL + 'predictor/', 0)
	}
	
	getRecommendations(imageLink, count) {
		return this.get('auto', {
			query: {
				image: imageLink,
				count: count,
			},
		})
	}

	getManualRecommendations(imageLink, count, listXy, listQuery) {
		return this.get('vertices', {
			query: {
				image: imageLink,
				count,
				list_xy: `[${listXy}]`,
				// list_xy: '[(227,464,315,583)]',
				list_query: `[${listQuery}]`,
				// list_query: '["category=bag"]',
			},
		})
	}
}

export default new PredictorService()
