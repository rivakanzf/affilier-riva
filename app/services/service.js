import ServiceModel from 'coeur/models/service';


class ServiceService extends ServiceModel {

	static displayName = 'service'

	constructor() {
		super(process.env.API_URL + process.env.CONNECTER_VERSION + 'service/', 60 * 1000)
	}

	getServices(ids = []) {
		if(ids.length) {
			return this.get('', {
				query: {
					ids: ids.join(','),
				},
			})
		}

		return this.get('')
	}

	getService(id) {
		return this.get(id)
	}
}

export default new ServiceService()
