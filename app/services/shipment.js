import ServiceModel from 'coeur/models/service';


class ShipmentService extends ServiceModel {

	static displayName = 'shipment'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/', 0)
	}

	createShipment(data, token) {
		return this.post('shipment', {
			data,
			token,
		})
	}

	updateShipment(shipmentId, data, token) {
		return this.post('shipment/' + shipmentId, {
			data,
			token,
		})
	}

	createAddress(shipmentId, data, token) {
		return this.post('shipment/' + shipmentId + '/address', {
			data,
			token,
		})
	}

	getShipmentDetail(id, token) {
		return this.get('shipment/' + id, {
			token,
		}).then(shipments => shipments[0])
	}

	getAddress(shipmentId, addressId, token) {
		return this.get('shipment/' + shipmentId + '/address/' + addressId, {
			token,
		})
	}

	updateShipmentStatus(data, token) {
		return this.post('shipment/' + data.id + '/status', {
			data,
			token,
		})
	}

	updateAddress(shipmentId, addressId, data, token) {
		return this.post('shipment/' + shipmentId + '/address/' + addressId, {
			data,
			token,
		})
	}

	getRates(orderDetailIds, destination, method, token) {
		return this.post('shipment/rates', {
			token,
			data: {
				order_detail_ids: orderDetailIds,
				destination,
				types: [method],
			},
		})
	}

	uploadCSV(data, token) {
		return this.post('shipment/upload/csv', {
			token,
			data: { data },
		})
	}

	getShipment(data, token) {
		return this.get('shipment/', {
			token,
			query: data,
		})
	}

	getShipmentData(ids, token) {
		return this.get('shipment/' + ids, {
			token,
		})
	}

	getSicepatWaybill(id, token) {
		return this.get('shipment/' + id + '/sicepat/waybill', {
			token,
		})
	}
}

export default new ShipmentService()
