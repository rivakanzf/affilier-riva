import ServiceModel from 'coeur/models/service';


class TypeformService extends ServiceModel {

	static displayName = 'typeform'

	constructor() {
		super(process.env.API_URL + 'admin/typeform/', 0)
	}

	getByEmail(email, token) {
		return this.get('', {
			query: {
				email,
			},
			token,
		})
	}
}

export default new TypeformService()
