import ServiceModel from 'coeur/models/service';


class WalletService extends ServiceModel {

	static displayName = 'wallet'

	constructor() {
		super(process.env.CONNECTER_URL + 'admin/wallet/', 0)
	}

	getDetailByUser(userId, token) {
		return this.get('user/' + userId, {
			token,
		})
	}

	depositIntoUser(userId, amount, note, token) {
		return this.post('user/' + userId + '/add', {
			data: {
				amount,
				note,
			},
			token,
		})
	}

	deductUser(userId, amount, note, token) {
		return this.post('user/' + userId + '/subtract', {
			data: {
				amount,
				note,
			},
			token,
		})
	}
}

export default new WalletService()
