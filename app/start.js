/*
|--------------------------------------------------------------------------
| START JS
|--------------------------------------------------------------------------
|
| Put what needs to be loaded first here
|
*/

/*
| Extending constants
*/
import Colors from 'coeur/constants/color';
import Defaults from 'coeur/constants/default';
import Fonts from 'coeur/constants/font';
import Sizes from 'coeur/constants/size';

import LocalColors from 'utils/constants/color';

Colors.extend({
	...LocalColors,
})

Defaults.extend({
	DEBUG: process.env.DEBUG === 'true',
	STORE_KEY: 'dashboard.yuna',
	MATCHBOX_MAX_ATTACHMENT: 3,
	ORDER_STATUSES: {
		UNPAID: 'Awaiting Payment',
		PAID: 'Paid',
		CANCELLED: 'Cancelled',
		PROCESSING: 'Processing',
		ON_COURIER: 'Shipping',
		DELIVERED: 'Delivered',
		RETURNED: 'Returned',
	},
})

Fonts.extend({
	spec: 'Spectral',
	geo: 'Geomanist',
	cav: 'Caveat',
})

Sizes.extend({
	margin: {
		thick: 32,
		default: 16,
	},
	screen: {
		width: Sizes.screen.width,
		height: Sizes.screen.height,
	},
})


/*
| Extending helpers
*/


import TimeHelper from 'coeur/helpers/time';
import CommonHelper from 'coeur/helpers/common';
import FormatHelper from 'coeur/helpers/format';

function asSimpleArray(string) {
	const isNumbers = string.split(',').findIndex(n => !isNaN(n)) > -1

	return isNumbers ? string.split(',').map(n => +n) : string
}

CommonHelper.extend({
	default(val, def) {
		return val === undefined ? def : val
	},
	getQueryString() {
		const query = new URLSearchParams(window.location.search.substring(0, window.location.search.length))

		return Array.from(query).map(([key, value]) => {
			return {
				[key]: +value ? +value : asSimpleArray(value),
			}
		}).reduce((sum, curr) => {
			return {
				...sum,
				...curr,
			}
		}, {})
	},
	asArray(numberOrArrayOfNumber) {
		return Array.isArray(numberOrArrayOfNumber) ? numberOrArrayOfNumber : [numberOrArrayOfNumber]
	},
})

TimeHelper.extend({
	getRange(_start, _end, D = 'D', M = 'MMM', Y = 'YYYY') {
		const start = TimeHelper.moment(_start)
			, end = TimeHelper.moment(_end)
			, isSameYear = start.year() === end.year()
			, isSameMonth = start.month() === end.month()
			, isSameDate = start.date() === end.date()

		// eslint-disable-next-line no-nested-ternary
		return isSameYear
			// eslint-disable-next-line no-nested-ternary
			? isSameMonth
				? isSameDate
					? `${end.format(D)} ${end.format(`${M} ${Y}`)}`
					: `${start.format(D)} - ${end.format(D)} ${end.format(`${M} ${Y}`)}`
				: `${start.format(`${D} ${M}`)} - ${end.format(`${D} ${M} ${Y}`)}`
			: `${start.format(`${D} ${M} ${Y}`)} - ${end.format(`${D} ${M} ${Y}`)}`
	},
})

const ranges = [
	{ divider: 1e12, suffix: 'T' },
	{ divider: 1e9, suffix: 'B' },
	{ divider: 1e6, suffix: 'M' },
	{ divider: 1e3, suffix: 'k' },
];

FormatHelper.extend({
	number(n) {
		if (n < 0) {
			return '-' + this.number(-n)
		}

		for (let i = 0; i < ranges.length; i++) {
			if (n >= ranges[i].divider) {
				return (n % ranges[i].divider ? (n / ranges[i].divider).toFixed(1) : (n / ranges[i].divider).toString()) + ranges[i].suffix
			}
		}

		return n.toString()
	},
})
