import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonBit from 'coeur/modules/bits/button'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import GeomanistBit from '../../geomanist'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonIcon extends CoreButtonBit({
		BoxBit,
		LoaderBit,
		TextBit: GeomanistBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.black.palette(2), Colors.white.primary, Colors.black.palette(2, .2)],
				ACTIVE: [Colors.black.palette(2, .2), Colors.white.primary, Colors.black.palette(2, .2)],
				DISABLED: [Colors.black.palette(2, .5), Colors.white.primary, Colors.black.palette(2, .2)],
				LOADING: [Colors.black.palette(2), Colors.white.primary, Colors.black.palette(2, .2)],
			},
		},
		SIZES: {
			NORMAL: Styles.normal,
			WIDE: Styles.wide,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = GeomanistBit.TYPES.PARAGRAPH_2
				break;
			}

			return (
				<GeomanistBit weight={this.props.weight} type={textType} style={[textStyle, { color: this.state.color }]}>{ this.props.title }</GeomanistBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
