import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderRadius: 4,
		borderWidth: 1,
		borderStyle: 'solid',
	},

	normal: {
		height: 34,
		padding: 2,
	},

	wide: {
		height: 34,
		padding: 2,
		paddingLeft: 16,
		paddingRight: 16,
	},
})
