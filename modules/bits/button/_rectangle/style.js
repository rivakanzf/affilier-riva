import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderRadius: 2,
		borderWidth: 1,
		borderStyle: 'solid',
	},

	normal: {
		height: 42,
		paddingLeft: 24,
		paddingRight: 24,
	},

	medium: {
		height: 44,
		paddingLeft: 16,
		paddingRight: 16,
	},

	small: {
		height: 34,
		paddingLeft: 16,
		paddingRight: 16,
	},

	tiny: {
		height: 20,
		paddingLeft: 8,
		paddingRight: 8,
	},
})
