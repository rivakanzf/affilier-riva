import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderWidth: 0,
	},

	normal: {
		height: 42,
		paddingLeft: 24,
		paddingRight: 24,
	},

	compact: {
		height: 34,
		paddingLeft: 16,
		paddingRight: 16,
	},

	medium: {
		height: 44,
		paddingLeft: 16,
		paddingRight: 16,
	},
})
