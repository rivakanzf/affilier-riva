import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import ButtonGhost from './_ghost';
import ButtonIcon from './_icon';
import ButtonProgress from './_progress';
import ButtonRectangle from './_rectangle';
import ButtonTab from './_tab';
import ButtonSearch from './_search';


const SHAPES = {
	GHOST: 'GHOST',
	PROGRESS: 'PROGRESS',
	RECTANGLE: 'RECTANGLE',
	ICON: 'ICON',
	TAB: 'TAB',
	SEARCH: 'SEARCH',
}


export default ConnectHelper(
	class ButtonBit extends StatefulModel {

		static TYPES = {
			...ButtonRectangle.TYPES,
			THEMES: {
				...ButtonGhost.TYPES.THEMES,
				...ButtonIcon.TYPES.THEMES,
				...ButtonProgress.TYPES.THEMES,
				...ButtonRectangle.TYPES.THEMES,
				...ButtonSearch.TYPES.THEMES,
				...ButtonTab.TYPES.THEMES,
			},
			SIZES: {
				...ButtonGhost.TYPES.SIZES,
				...ButtonIcon.TYPES.SIZES,
				...ButtonProgress.TYPES.SIZES,
				...ButtonRectangle.TYPES.SIZES,
				...ButtonSearch.TYPES.SIZES,
				...ButtonTab.TYPES.SIZES,
			},
			SHAPES,
		}

		static propTypes(PropTypes) {
			return {
				...ButtonRectangle.propTypes,
				theme: PropTypes.oneOf(this.TYPES.THEMES),
				size: PropTypes.oneOf(this.TYPES.SIZES),
				type: PropTypes.oneOf(this.TYPES.SHAPES),
			}
		}

		static defaultProps = {
			...ButtonRectangle.defaultProps,
			type: SHAPES.RECTANGLE,
			weight: 'semibold',
		}

		view() {
			const {
				type,
				...props
			} = this.props

			switch(type) {
			case this.TYPES.SHAPES.SEARCH:
				return (
					<ButtonSearch { ...props } />
				)
			case this.TYPES.SHAPES.GHOST:
				return (
					<ButtonGhost { ...props } />
				)
			case this.TYPES.SHAPES.PROGRESS:
				return (
					<ButtonProgress { ...props } />
				)
			case this.TYPES.SHAPES.ICON:
				return (
					<ButtonIcon { ...props } />
				)
			case this.TYPES.SHAPES.TAB:
				return (
					<ButtonTab { ...props } />
				)
			case this.TYPES.SHAPES.RECTANGLE:
			default:
				return (
					<ButtonRectangle { ...props } />
				)
			}
		}
	}
)
