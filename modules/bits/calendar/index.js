import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

import Calendar from 'calendar-js';

const options = {
	...Array(12).fill(undefined).map((x, i) => {
		const month = TimeHelper.moment().set('M', i)
		return {
			months: month.format('MMMM'),
			monthsAbbr: month.format('MMM'),
		}
	}).reduce((sum, curr) => {
		return {
			months: sum.months.concat(curr.months),
			monthsAbbr: sum.monthsAbbr.concat(curr.monthsAbbr),
		}
	}, {
		months: [],
		monthsAbbr: [],
	}),
	...Array(7).fill(undefined).map((x, i) => {
		const weekday = TimeHelper.moment().set('D', i + 2)
		return {
			weekdays: weekday.format('dddd'),
			weekdaysAbbr: weekday.format('ddd'),
		}
	}).reduce((sum, curr) => {
		return {
			weekdays: sum.weekdays.concat(curr.weekdays),
			weekdaysAbbr: sum.weekdaysAbbr.concat(curr.weekdaysAbbr),
		}
	}, {
		weekdays: [],
		weekdaysAbbr: [],
	}),
}

const calendar = Calendar(options)

export default ConnectHelper(
	class CalendarBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				// TODO:
				// range: PropTypes.bool,
				date: PropTypes.date,
				onChange: PropTypes.func,
				onClose: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {

			const date = TimeHelper.moment(p.date)

			super(p, {
				date: date.date(),
				month: date.month(),
				year: date.year(),
				calendar: calendar.of(date.year(), date.month()),
				current: [date.date(), date.month(), date.year()],
				isChanged: false,
			})
		}

		onShowMonths = () => {
			this.props.utilities.menu.show({
				title: 'Select month',
				actions: options.months.map((m, i) => {
					return {
						title: m,
						isActive: i === this.state.month,
						onPress: this.onSelectMonth.bind(this, i),
						type: this.props.utilities.menu.TYPES.ACTION.RADIO,
					}
				}),
			})
		}

		onShowYears = () => {
			this.props.utilities.menu.show({
				title: 'Select year',
				actions: calendar.years(this.state.year - 10, this.state.year + 10).map(y => {
					const year = parseInt(y, 10)
					return {
						title: year,
						isActive: year === this.state.year,
						onPress: this.onSelectYear.bind(this, year),
						type: this.props.utilities.menu.TYPES.ACTION.RADIO,
					}
				}),
			})
		}

		onSelectMonth = month => {
			this.setState({
				month,
				calendar: calendar.of(this.state.year, month),
			})
		}

		onSelectYear = year => {
			this.setState({
				year,
				calendar: calendar.of(year, this.state.month),
			})
		}

		onSelectDate = date => {
			if(this.isNow(date) && this.state.isChanged) {
				const result = TimeHelper.moment()
					.set('date', date)
					.set('month', this.state.month)
					.set('year', this.state.year)

				this.props.onChange &&
				this.props.onChange(
					result.toDate(),
					result,
				)

				this.props.onClose &&
				this.props.onClose()

			} else {
				this.setState({
					date,
					current: [date, this.state.month, this.state.year],
					isChanged: true,
				})
			}

		}

		isNow(date) {
			return date === this.state.current[0] && this.state.month === this.state.current[1] && this.state.year === this.state.current[2]
		}

		rowRenderer = (dates, i) => {
			return (
				<BoxBit row key={i} unflex>
					{ dates.map(this.dateRenderer) }
				</BoxBit>
			)
		}

		titleRenderer = (title, i, arr) => {
			return (
				<BoxBit key={i} centering style={Styles.title}>
					<GeomanistBit align="center" type={GeomanistBit.TYPES.NOTE_2} style={[Styles.text, i === 0 && Styles.red, i === arr.length - 1 && Styles.red]}>{ title }</GeomanistBit>
				</BoxBit>
			)
		}

		dateRenderer = (date, i, arr) => {
			const isNow = this.isNow(date)

			return date ? (
				<TouchableBit key={i} centering style={[Styles.box, isNow && Styles.boxActive]} onPress={ this.onSelectDate.bind(this, date) } onDoublePress={ this.onSelectDate.bind(this, date) }>
					<GeomanistBit align="center" weight={ isNow ? 'medium' : 'normal' } type={GeomanistBit.TYPES.NOTE_1} style={[isNow ? Styles.textActive : Styles.text, i === 0 && Styles.red, i === arr.length - 1 && Styles.red]}>{ date }</GeomanistBit>
				</TouchableBit>
			) : (
				<BoxBit key={i} centering style={Styles.box}>
					<GeomanistBit align="center" type={GeomanistBit.TYPES.NOTE_1} style={[Styles.text, i === 0 && Styles.red, i === arr.length - 1 && Styles.red]}>{''}</GeomanistBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<BoxBit unflex row style={Styles.header}>
						<TouchableBit centering style={Styles.button} onPress={ this.onShowYears }>
							<GeomanistBit align="center" weight="medium" type={GeomanistBit.TYPES.NOTE_1} style={Styles.textActive}>{ this.state.year }</GeomanistBit>
						</TouchableBit>
						<TouchableBit centering style={[Styles.button, Styles.month]} onPress={ this.onShowMonths }>
							<GeomanistBit align="center" weight="medium" type={GeomanistBit.TYPES.NOTE_1} style={Styles.textActive}>{ options.months[this.state.month] }</GeomanistBit>
						</TouchableBit>
						<TouchableBit unflex centering style={Styles.close} onPress={ this.props.onClose }>
							<IconBit
								name="close"
								size={16}
							/>
						</TouchableBit>
					</BoxBit>
					<BoxBit unflex style={Styles.content}>
						<BoxBit row unflex>
							{ this.state.calendar.weekdaysAbbr.map(this.titleRenderer) }
						</BoxBit>
						{ this.state.calendar.calendar.map(this.rowRenderer) }
					</BoxBit>
					<BoxBit unflex style={Styles.footer}>
						<GeomanistBit align="center" type={GeomanistBit.TYPES.NOTE_1} style={ this.state.isChanged ? Styles.text : Styles.note}>{ this.state.isChanged ? 'Tap again to select' : 'Select a date' /* 'Swipe left or right to navigate' */ }</GeomanistBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
