import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		width: 280,
		backgroundColor: Colors.white.primary,
	},

	header: {
		paddingLeft: 8,
		paddingRight: 16,
		paddingTop: 12,
		paddingBottom: 12,
	},

	button: {
		backgroundColor: Colors.primary,
		borderRadius: 2,
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: 16,
		paddingRight: 16,
		marginLeft: 8,
		marginRight: 8,
	},

	textActive: {
		color: Colors.white.primary,
	},

	text: {
		color: Colors.black.palette(2, .6),
	},

	red: {
		color: Colors.red.palette(7),
	},

	month: {
		flexGrow: 2,
	},

	close: {
		paddingTop: 8,
		paddingLeft: 8,
		paddingRight: 8,
		paddingBottom: 8,
	},

	content: {
		paddingTop: 16,
		paddingLeft: 16,
		paddingRight: 16,
		paddingBottom: 16,
	},

	footer: {
		paddingBottom: 16,
	},

	note: {
		color: Colors.black.palette(2, .3),
	},

	title: {
		paddingLeft: 8,
		paddingRight: 8,
		paddingTop: 4,
		paddingBottom: 4,
	},

	box: {
		paddingLeft: 6,
		paddingRight: 6,
		paddingTop: 8,
		paddingBottom: 8,
		marginLeft: 2,
		marginRight: 2,
		marginTop: 4,
		marginBottom: 4,
	},

	boxActive: {
		borderRadius: 2,
		backgroundColor: Colors.primary,
	},

})
