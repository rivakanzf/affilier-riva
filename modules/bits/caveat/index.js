import ConnectHelper from 'coeur/helpers/connect';
import CoreTextTypeBit from 'coeur/modules/bits/text.type';

// import Colors from 'coeur/constants/color'
import Fonts from 'coeur/constants/font'

import TextBit from '../text'


export default ConnectHelper(
	class CaveatBit extends CoreTextTypeBit({
		TextBit,
		fontFamily: Fonts.cav,
		weights: [
			'normal',
			'bold',
		],
		TYPES: {
			HEADER_1: {
				fontSize: 44,
				lineHeight: 48,
				fontWeight: '700',
				letterSpacing: -1.6,
			},

			HEADER_2: {
				fontSize: 36,
				lineHeight: 36,
				fontWeight: '700',
				letterSpacing: -1.2,
			},

			HEADER_3: {
				fontSize: 30,
				lineHeight: 32,
				fontWeight: '700',
				letterSpacing: -1,
			},

			HEADER_4: {
				fontSize: 28,
				lineHeight: 32,
				fontWeight: '700',
				letterSpacing: -.8,
			},

			SUBHEADER_1: {
				fontSize: 22,
				lineHeight: 26,
				fontWeight: '400',
				letterSpacing: -.6,
			},

			SUBHEADER_2: {
				fontSize: 18,
				lineHeight: 22,
				fontWeight: '400',
				letterSpacing: -.4,
			},

			SUBHEADER_3: {
				fontSize: 14,
				lineHeight: 22,
				fontWeight: '400',
				letterSpacing: -.4,
			},

			NOTE_1: {
				fontSize: 12,
				lineHeight: 18,
				fontWeight: '700',
				letterSpacing: .4,
			},
		},
	}) {}
)
