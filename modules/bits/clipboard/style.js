import StyleSheet from 'coeur/libs/style.sheet'
// import ColorModel from 'coeur/models/color';
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		borderWidth: 0,
		backgroundColor: Colors.solid.grey.palette(1),
	},
	input: {
		height: 44,
		paddingLeft: Sizes.margin.default,
		paddingTop: 12,
		paddingBottom: 12,
		paddingRight: Sizes.margin.default / 2,
		fontWeight: 'normal',
		color: Colors.black.palette(2),
		backgroundColor: Colors.transparent,
		fontSize: 14,
		lineHeight: 20,
		letterSpacing: 0.3,
	},
	icon: {
		marginLeft: Sizes.margin.default / 2,
		marginRight: Sizes.margin.default / 2,
	},
})
