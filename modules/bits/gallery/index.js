import ConnectHelper from 'coeur/helpers/connect';
import CoreGalleryBit from 'coeur/modules/bits/gallery';

import BoxBit from '../box';
import ScrollSnapBit from '../scroll.snap'

export default ConnectHelper(
	class GalleryBit extends CoreGalleryBit({
		BoxBit,
		ScrollSnapBit,
	}) {}
)
