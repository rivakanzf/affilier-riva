import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	label: {
		marginTop: 4,
		marginBottom: 4,
	},
	container: {
		alignItems: 'center',
	},
	radio: {
		marginRight: 8,
	},
	note: {
		color: Colors.black.palette(2, .6),
	},
})
