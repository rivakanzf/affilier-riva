import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'coeur/constants/color';

import BoxBit from '../box'
import CalendarBit from '../calendar'
import GeomanistBit from '../geomanist'
import IconBit from '../icon';
import InputValidatedBit from '../input.validated'
import TouchableBit from '../touchable';

import Styles from './style'


export default ConnectHelper(
	class InputDateBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				title: PropTypes.string,
				inside: PropTypes.bool,
				date: PropTypes.date,
				description: PropTypes.string,
				format: PropTypes.string,
				readonly: PropTypes.bool,
				disabled: PropTypes.bool,
				children: PropTypes.node,
				onChange: PropTypes.func,
				style: PropTypes.style,
				postfix: PropTypes.node,
				inputStyle: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			title: 'Date',
			format: 'DD MMM YYYY',
			unflex: false,
			disabled: false,
			postfix: false,
		}

		constructor(p) {
			super(p, {
				date: p.date ? TimeHelper.moment(p.date) : null,
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onFocus = () => {
			if(!this.props.readonly && !this.props.disabled) {
				this.props.utilities.alert.modal({
					component: (
						<CalendarBit
							date={ this.state.date ? this.state.date.toDate() : undefined }
							onChange={ this.onChange }
							onClose={ this.onModalRequestClose }
						/>
					),
				})
			}
		}

		onChange = (date, moment) => {
			this.setState({
				date: moment,
			})

			this.props.onChange &&
			this.props.onChange(date, moment)
		}

		onClear = () => {
			this.setState({
				date: undefined,
			})

			this.props.onChange &&
			this.props.onChange(undefined)
		}

		postfixRenderer() {
			return !this.props.readonly && !this.props.disabled && this.state.date ? (
				<TouchableBit unflex onPress={this.onClear}>
					<IconBit size={24} color={ Colors.red.palette(3) } name="close-fat" style={Styles.close} />
				</TouchableBit>
			) : (
				<IconBit size={20} color={ Colors.black.palette(2, .3) } name="calendar" style={Styles.calendar} />
			)
		}

		view() {
			return (
				<BoxBit row unflex>
					<InputValidatedBit unflex={ this.props.unflex } readonly disabled={ this.props.disabled } key={+this.state.date}
						title={ this.props.inside ? undefined : this.props.title }
						description={ this.props.description }
						type={ InputValidatedBit.TYPES.INPUT }
						value={ this.state.date ? this.state.date.format(this.props.format) : undefined }
						placeholder={ this.props.format }
						onPress={ this.onFocus }
						onFocus={ this.onFocus }
						style={ this.props.style }
						inputContainerStyle={ Styles.unreadonly }
						prefix={ this.props.title && this.props.inside ? (
							<BoxBit unflex style={ Styles.insider }>
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.inside }>
									{ this.props.title }
								</GeomanistBit>
							</BoxBit>
						) : undefined }
						postfix={ this.postfixRenderer() }
						inputStyle={ [ this.props.title && this.props.inside ? Styles.inputInside : undefined, Styles.input,  this.props.inputStyle] }
					/>
					<BoxBit unflex style={ !!this.props.title ? Styles.children : undefined }>
						{ this.props.children }
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
