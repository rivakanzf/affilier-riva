import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	children: {
		justifyContent: 'flex-end',
	},

	close: {
		marginRight: 4,
		marginLeft: 4,
	},

	calendar: {
		marginRight: 8,
		marginLeft: 8,
	},

	insider: {
		backgroundColor: Colors.grey.palette(2),
		paddingLeft: 8,
		paddingRight: 8,
		paddingTop: 6,
		paddingBottom: 6,
		borderRadius: 2,
		marginLeft: 8,
	},

	inside: {
		color: Colors.black.palette(2, .8),
	},

	inputInside: {
		paddingLeft: 8,
	},

	input: {
		paddingRight: 0,
	},

	unreadonly: {
		opacity: 1,
	},
})
