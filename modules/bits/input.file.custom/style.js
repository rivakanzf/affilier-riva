import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		// backgroundColor: Colors.white.primary,
		// height: 44,
		// borderWidth: 1,
		// borderColor: Colors.black.palette(2, .16),
		// borderRadius: 2,
	},

	text: {
		color: Colors.black.palette(2, .9),
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},

	label: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		overflow: 'hidden',
		zIndex: 1,
		cursor: 'pointer',
	},

	input: {
		width: .1,
		height: .1,
		opacity: 0,
		overflow: 'hidden',
		position: 'absolute',
		left: -1,
		zIndex: -1,
	},

	floater: {
		position: 'absolute',
		top: 0,
		right: 0,
		bottom: 0,
		paddingRight: Sizes.margin.default / 2,
		zIndex: 2,
	},

	title: {
		left: 0,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default + 20,
		justifyContent: 'center',
		width: '100%',
	},

	disabled: {
		opacity: .3,
	},

})
