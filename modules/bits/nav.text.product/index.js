import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from '../box'
import GeomanistBit from '../geomanist'
import NavTextBit from '../nav.text'

import BadgeStatusSmallLego from 'modules/legos/badge.status.small'

import Styles from './style'

import { capitalize } from 'lodash'


export default ConnectHelper(
	class NavTextProductBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				ref_id: PropTypes.id,
				type: PropTypes.oneOf([
					'STYLESHEET',
					'INVENTORY',
					'VOUCHER',
					'STYLEBOARD',
				]),
				linkless: PropTypes.bool,
				status: PropTypes.string,
				description: PropTypes.string,
				onPress: PropTypes.func,
				style: PropTypes.style,
				textStyle: PropTypes.style,
			}
		}

		title() {
			switch (this.props.type) {
			case 'STYLEBOARD':
				return this.props.ref_id ? `Styleboard ID: #SB-${ this.props.ref_id }` : this.props.description
			case 'STYLESHEET':
				return this.props.ref_id ? `Stylesheet ID: #ST-${ this.props.ref_id }` : this.props.description
			case 'INVENTORY':
				return this.props.ref_id ? `Inventory ID: #IN-${ this.props.ref_id }${ this.props.item ? ` – Status: ${ capitalize(this.props.status) }` : '' }` : this.props.description
			case 'VOUCHER':
				return this.props.ref_id ? `Voucher ID: #VO-${ this.props.ref_id }${ this.props.item ? ` – ${ this.props.item.is_physical ? 'Physical' : 'E-Voucher' } – Status: ${ capitalize(this.props.status) }` : '' }` : this.props.description
			default:
				return ''
			}
		}

		titleWithStatus() {
			switch (this.props.type) {
			case 'STYLESHEET':
				return this.props.ref_id ? (
					<BoxBit row unflex>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={[Styles.note, Styles.margin, this.props.textStyle]}>
							Stylesheet ID: #ST-{ this.props.ref_id }
						</GeomanistBit>
						<BadgeStatusSmallLego status={ this.props.status } />
					</BoxBit>
				) : this.props.description
			case 'STYLEBOARD':
				return this.props.ref_id ? (
					<BoxBit row unflex>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={[Styles.note, Styles.margin, this.props.textStyle]}>
							Styleboard ID: #SB-{ this.props.ref_id }
						</GeomanistBit>
						<BadgeStatusSmallLego status={ this.props.status } />
					</BoxBit>
				) : this.props.description
			case 'INVENTORY':
				return ''
			case 'VOUCHER':
				return `Voucher ID: #VO-${ this.props.ref_id }`
			default:
				return ''
			}
		}

		link() {
			if(this.props.linkless) {
				return undefined
			}

			switch (this.props.type) {
			case 'STYLEBOARD':
				return 'View Styleboard'
			case 'STYLESHEET':
				return 'View Stylesheet'
			case 'INVENTORY':
				return 'View Variant'
			case 'VOUCHER':
				return 'View Voucher'
			default:
				return undefined
			}
		}

		href() {
			if (this.props.linkless) {
				return undefined
			}

			switch (this.props.type) {
			case 'STYLEBOARD':
				return this.props.ref_id ? `styleboard/${this.props.ref_id}` : undefined
			case 'STYLESHEET':
				return this.props.ref_id ? `stylesheet/${this.props.ref_id}` : undefined
			case 'INVENTORY':
				return undefined
				// return this.props.ref_id ? `stylesheet/${this.props.ref_id}` : undefined
			case 'VOUCHER':
				return this.props.ref_id ? `voucher/${this.props.ref_id}` : undefined
			default:
				return undefined
			}
		}

		view() {
			return (
				<NavTextBit
					titleAsNode={ !!this.props.item }
					title={ !!this.props.status ? this.titleWithStatus() : this.title() }
					link={ this.link() }
					href={ this.href() }
					onPress={ this.props.onPress }
					textStyle={[ Styles.note, this.props.textStyle ]}
					style={ this.props.style }
				/>
			)
		}
	}
)
