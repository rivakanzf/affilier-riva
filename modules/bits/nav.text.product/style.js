import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	note: {
		color: Colors.black.palette(2, .6),
	},

	margin: {
		marginRight: 8,
	},

})
