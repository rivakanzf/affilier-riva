import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from '../box'
import GeomanistBit from '../geomanist'
import NavTextBit from '../nav.text'

import Styles from './style'


export default ConnectHelper(
	class NavTextTitleBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				description: PropTypes.string,
				unflex: PropTypes.bool,
				link: PropTypes.string,
				href: PropTypes.string,
				param: PropTypes.object,
				onPress: PropTypes.func,
				style: PropTypes.style,
				textStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: true,
		}

		view() {
			return (
				<BoxBit unflex={ this.props.unflex } style={ [Styles.container, this.props.style] }>
					{ this.props.title ? (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.title }>
							{ this.props.title }
						</GeomanistBit>
					) : false }
					<NavTextBit
						title={ this.props.description }
						link={ this.props.link }
						href={ this.props.href }
						param={ this.props.param }
						onPress={ this.props.onPress }
						textStyle={[ Styles.note, this.props.textStyle ]}
						arrowless={ this.props.arrowless }
					/>
				</BoxBit>
			)
		}
	}
)
