import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';

import BoxBit from '../box'
import GeomanistBit from '../geomanist'
import NavBit from '../nav'

import Styles from './style'


export default ConnectHelper(
	class NavTextBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string.isRequired,
				link: PropTypes.string,
				href: PropTypes.string,
				arrowless: PropTypes.bool,
				param: PropTypes.object,
				titleAsNode: PropTypes.bool,
				onPress: PropTypes.func,
				style: PropTypes.style,
				textStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			title: 'Date',
			format: 'DD MMM YYYY',
		}

		constructor(p) {
			super(p, {
				date: p.date ? TimeHelper.moment(p.date) : null,
			})
		}

		view() {
			return (
				<BoxBit unflex style={ [Styles.container, this.props.style] }>
					{ this.props.titleAsNode ? this.props.title : (
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={ [this.props.textStyle, Styles.text] }>
							{ this.props.title }
						</GeomanistBit>
					) }
					{ this.props.href ? (
						<NavBit
							title={ this.props.link }
							href={ this.props.href }
							arrowless={ this.props.arrowless }
							param={ this.props.param }
							onPress={ this.props.onPress }
						/>
					) : false }
				</BoxBit>
			)
		}
	}
)
