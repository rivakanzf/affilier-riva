import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import PageContext from 'coeur/contexts/page';

import Colors from 'coeur/constants/color';

import GeomanistBit from '../geomanist'
import IconBit from '../icon'
import TouchableBit from '../touchable'

import Styles from './style'


export default ConnectHelper(
	class NavBit extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string.isRequired,
				href: PropTypes.string.isRequired,
				arrowless: PropTypes.bool,
				param: PropTypes.object,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			PageContext,
		]

		static defaultProps = {
			title: 'Date',
			format: 'DD MMM YYYY',
		}

		constructor(p) {
			super(p, {
				date: p.date ? TimeHelper.moment(p.date) : null,
			})
		}

		onNavigate = () => {
			if(this.props.onPress) {
				this.props.onPress(this.props.href, this.props.param)
			} else {
				this.props.page.navigator.navigate(this.props.href, this.props.param)
			}
		}

		view() {
			return (
				<TouchableBit unflex row onPress={ this.onNavigate } style={ this.props.style }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.primary }>
						{ this.props.title }
					</GeomanistBit>
					<IconBit
						name="arrow-right-tailed"
						size={ 20 }
						color={ Colors.primary }
					/>
				</TouchableBit>
			)
		}
	}
)
