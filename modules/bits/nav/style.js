import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	primary: {
		color: Colors.primary,
		paddingRight: 4,
	},

})
