import ConnectHelper from 'coeur/helpers/connect';
import CorePageImageBit from 'coeur/modules/bits/page.image';

import BoxBit from '../box';
import ImageBit from '../image';
import PageBit from '../page';

import Styles from './style';


export default ConnectHelper(
	class PageImageBit extends CorePageImageBit({
		Styles,
		BoxBit,
		ImageBit,
		PageBit,
	}) {}
)
