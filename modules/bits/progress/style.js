import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'utils/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		height: 16,
		overflow: 'hidden',
		backgroundColor: Colors.grey.palette(1, .2),
	},

	bar: {
		height: 16,
		backgroundColor: Colors.red.palette(4),
	},
})
