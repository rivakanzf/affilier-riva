import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import CoreSelectionBit from 'coeur/modules/bits/selection';

import Colors from 'coeur/constants/color';

import IconBit from '../icon';
import GeomanistBit from 'modules/bits/geomanist';
// import TextInputBit from '../text.input';

import Styles from './style';


export default ConnectHelper(
	class SelectionSmallBit extends CoreSelectionBit({
		IconBit,
		// TextInputBit,
		iconDisabledColor: Colors.black.palette(2, .5),
		Styles,
	}) {
		textRenderer(value, style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={style}>{ value }</GeomanistBit>
			)
		}
	}
)
