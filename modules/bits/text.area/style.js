import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Fonts from 'coeur/constants/font';

export default StyleSheet.create({
	container: {
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .16),
		borderStyle: 'solid',
		borderRadius: 2,
		transition: '.3s color, .3s border, .3s opacity',
	},
	focused: {
		borderColor: Colors.black.palette(2, .2),
	},
	input: {
		height: 76,
		maxHeight: 126,
		resize: 'none',
		border: 0,
		padding: 8,
		paddingLeft: 16,
		paddingRight: 16,
		fontFamily: Fonts.geo,
		fontSize: 14,
		fontWeight: 400,
		lineHeight: 20,
		letterSpacing: .3,
		color: Colors.black.palette(2),
	},
})
