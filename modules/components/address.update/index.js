import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import LocationService from 'app/services/location';
import UserService from 'app/services/user.new';
import BrandService from 'app/services/brand';
import OrderService from 'app/services/new.order';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import InputValidatedBit from 'modules/bits/input.validated';

import LoaderLego from 'modules/legos/loader';
import SelectLocationLego from 'modules/legos/select.location';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

import { debounce, isEmpty } from 'lodash'


export default ConnectHelper(
	class AddressUpdateComponent extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				type: PropTypes.oneOf([
					'brand',
					'user',
					'order',
				]).isRequired,
				typeId: PropTypes.id.isRequired,
				address: PropTypes.object,
				getSubmitter: PropTypes.func,
				onUpdate: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static propsToPromise(state, oP) {
			if(oP.address && !oP.id) {
				if(oP.address.location_id) {
					return [oP.address.id, LocationService.getLocation(oP.address.location_id).then(location => {
						return {
							...oP.address,
							location,
						}
					})]
				} else {
					return [oP.address.id, Promise.resolve(oP.address)]
				}
			} else if(oP.id) {
				switch (oP.type) {
				case 'brand':
					return [oP.id, BrandService.getAddress(oP.typeId, oP.id, state.me.token)]
				case 'user':
				default:
					return [oP.id, UserService.getAddress(oP.typeId, oP.id, state.me.token)]
				case 'order':
					return [oP.id, OrderService.getAddress(oP.typeId, oP.id, state.me.token).then(addresses => addresses[0])]
				}
			} else {
				return null
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(isEmpty(nS.address) && !isEmpty(nP.data)) {
				return {
					address: nP.data,
					isDone: !!(nP.data.receiver && nP.data.phone && nP.data.address),
					isMetadataDone: !!nP.data.location_id,
					isChanged: false,
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				address: {},
				invalidPostal: [],
				isLoading: false,
				isLoadingArea: false,
				isMetadataDone: false,
				isDone: false,
				isChanged: false,
			})
		}

		componentDidMount() {
			this.props.getSubmitter &&
			this.props.getSubmitter(this.submit)
		}

		componentDidUpdate() {
			this.props.onUpdate &&
			this.props.onUpdate(this.state.address, this.state.isMetadataDone && this.state.isDone, this.state.isChanged)
		}

		checkInvalidPostal = value => {
			return Promise.resolve(!this.state.invalidPostal.length || this.state.invalidPostal.indexOf(value) === -1)
		}

		onUpdate = (isDone, {
			title,
			receiver,
			phone,
			address,
		}) => {
			this.setState({
				address: {
					...this.state.address,
					title: title.value,
					receiver: receiver.value,
					phone: phone.value,
					address: address.value,
				},
				isChanged: true,
			})

			if(this.state.isDone !== isDone) {
				this.setState({
					isDone,
				})
			}
		}

		getAreas(suburbId) {
			this.setState({
				isLoadingArea: true,
			}, () => {
				LocationService.getAreasBySuburb(suburbId).then(this.onAreaLoaded).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						message: 'Oops… Something went wrong when getting data, please try again later',
						timeout: 3000,
					})

					this.setState({
						isLoadingArea: false,
					})
				})
			})
		}

		onChangeLocation = (type, key, value) => {
			const location = this.state.address.location || {}
			let shouldGetArea = false

			if(this.isChanged(location, type, key, value)) {
				switch(type) {
				case 'province':
					location.province_id = key
					location.province_name = value
					location.city_id = undefined
					location.city_name = undefined
					location.suburb_id = undefined
					location.suburb_name = undefined
					location.area_name = undefined
					break;
				case 'city':
					location.city_id = key
					location.city_name = value
					location.suburb_id = undefined
					location.suburb_name = undefined
					location.area_name = undefined
					break;
				case 'district':
					location.suburb_id = key
					location.suburb_name = value
					location.area_name = undefined

					shouldGetArea = true

					break;
				}

				this.setState({
					isMetadataDone: false,
					address: {
						...this.state.address,
						location,
						location_id: undefined,
						postal: undefined,
					},
				}, () => {
					if(shouldGetArea) {
						this.getAreas(location.suburb_id)
					}
				})
			}
		}

		onChangePostal = debounce((postal, isValid) => {

			this.state.address.postal = postal

			if(isValid) {
				this.setState({
					isLoadingArea: true,
				}, () => {
					LocationService.getAreas(postal).then(this.onAreaLoaded).catch(err => {
						if(err.code === 'ERR_101') {
							this.props.utilities.notification.show({
								message: 'Oops… Postal code invalid',
								timeout: 3000,
							})

							this.setState({
								isLoadingArea: false,
								invalidPostal: [
									...this.state.invalidPostal,
									postal,
								],
							})
						} else {
							this.props.utilities.notification.show({
								title: 'Oops…',
								message: 'Something went wrong. Please try again',
								timeout: 3000,
							})

							this.setState({
								isLoadingArea: false,
							})
						}
					})
				})
			}
		}, 300)

		onAreaLoaded = areas => {
			if (areas.length > 1) {
				this.props.utilities.menu.show({
					title: 'Select area',
					actions: areas.map(area => {
						return {
							title: area.area_name,
							type: this.props.utilities.menu.TYPES.ACTION.RADIO,
							onPress: this.saveArea.bind(this, area),
						}
					}),
				})
			} else {
				this.saveArea(areas[0])
			}

			this.setState({
				isLoadingArea: false,
			})
		}

		onSaved = address => {
			this.props.utilities.notification.show({
				title: 'Yeay!',
				message: 'Address saved successfully.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			return address
		}

		onUpdated = isUpdated => {
			if(isUpdated) {
				this.props.utilities.notification.show({
					title: 'Yeay!',
					message: 'Address saved successfully.',
					type: this.props.utilities.notification.TYPES.SUCCESS,
				})
			} else {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Cannot save data into the database, contact your system administrator.',
				})
			}

			return {
				id: this.props.id,
				...this.getAddressInterface(),
			}
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			throw err
		}

		isChanged = (location, type, key) => {
			switch(type) {
			case 'province':
			default:
				return location.province_id !== key
			case 'city':
				return location.city_id !== key
			case 'district':
				return location.suburb_id !== key
			}
		}

		saveArea = area => {
			this.setState({
				address: {
					...this.state.address,
					location: area,
					location_id: area.id,
					postal: area.zip_code,
					isChanged: true,
				},
				isMetadataDone: true,
			})
		}

		getAddressInterface() {
			return {
				title: this.state.address.title,
				receiver: this.state.address.receiver,
				phone: this.state.address.phone,
				address: this.state.address.address,
				district: `${this.state.address.location.suburb_name} – ${this.state.address.location.area_name}`,
				postal: this.state.address.postal,
				location_id: this.state.address.location_id,
			}
		}

		submit = () => {
			if(!this.state.isDone || !this.state.isMetadataDone) {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Data not complete',
				})

				return Promise.reject(false)
			}

			if(this.props.type && this.props.typeId) {
				if(this.props.id) {
					// updating
					switch (this.props.type) {
					case 'brand':
						return BrandService.updateAddress(this.props.typeId, this.props.id, this.getAddressInterface(), this.props.token).then(this.onUpdated).catch(this.onError)
					case 'user':
					default:
						return UserService.updateAddress(this.props.typeId, this.props.id, this.getAddressInterface(), this.props.token).then(this.onUpdated).catch(this.onError)
					case 'order':
						return OrderService.updateAddress(this.props.typeId, this.props.id, this.getAddressInterface(), this.props.token).then(this.onUpdated).catch(this.onError)
					}
				} else {
					// creating
					switch (this.props.type) {
					case 'brand':
						return BrandService.createAddress(this.props.typeId, this.getAddressInterface(), this.props.token).then(this.onSaved).catch(this.onError)
					case 'user':
					default:
						return UserService.addAddress(this.props.typeId, this.getAddressInterface(), this.props.token).then(this.onSaved).catch(this.onError)
					case 'order':
						return OrderService.createAddress(this.props.typeId, this.getAddressInterface(), this.props.token).then(this.onSaved).catch(this.onError)
					}
				}
			} else {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'No type and typeId supplied, please contact system administrator.',
				})

				return Promise.reject(false)
			}
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<FormPagelet
					key={`${this.state.isLoadingArea}|${this.state.invalidPostal.length}|${this.state.address.postal}|${this.state.address.location_id}|${this.state.address.location ? `${this.state.address.location.province_id}|${this.state.address.location.city_id}|${this.state.address.location.suburb_id}` : ''}`}
					config={[{
						id: 'title',
						title: 'Title',
						placeholder: 'E.g. Rumah, Office...',
						type: FormPagelet.TYPES.INPUT,
						unflex: true,
						required: false,
						value: this.state.address.title,
						style: Styles.span,
					}, {
						id: 'receiver',
						title: 'Receiver Name',
						placeholder: 'Enter receiver\'s name',
						type: FormPagelet.TYPES.NAME,
						autogrow: false,
						unflex: true,
						required: true,
						value: this.state.address.receiver,
						style: Styles.column,
					}, {
						id: 'phone',
						title: 'Phone Number',
						placeholder: 'Enter phone number',
						type: FormPagelet.TYPES.PHONE,
						required: true,
						unflex: true,
						autogrow: false,
						value: this.state.address.phone,
						style: Styles.columnLeft,
						inputStyle: Styles.phone,
					}, {
						id: 'address',
						title: 'Address',
						placeholder: 'Enter address',
						showCounter: true,
						maxlength: 200,
						type: FormPagelet.TYPES.TEXTAREA,
						autogrow: false,
						unflex: true,
						required: true,
						value: this.state.address.address,
						style: Styles.span,
					}, {
						children: (
							<SelectLocationLego form
								disabled={ this.state.isLoadingArea }
								id="all"
								title="Province"
								type="PROVINCE"
								onChange={ this.onChangeLocation.bind(this, 'province') }
								value={ this.state.address.location && this.state.address.location.province_id }
								style={ Styles.column }
							/>
						),
					}, {
						children: (
							<SelectLocationLego form
								disabled={ this.state.isLoadingArea }
								id={ this.state.address.location && this.state.address.location.province_id }
								title="City"
								type="CITY"
								onChange={ this.onChangeLocation.bind(this, 'city') }
								value={ this.state.address.location && this.state.address.location.city_id }
								style={ Styles.columnLeft }
							/>
						),
					}, {
						children: (
							<BoxBit unflex style={Styles.breaker} />
						),
					}, {
						children: (
							<SelectLocationLego form
								disabled={ this.state.isLoadingArea }
								id={ this.state.address.location && this.state.address.location.city_id }
								title="District (Kecamatan)"
								type="SUBURB"
								onChange={ this.onChangeLocation.bind(this, 'district') }
								value={ this.state.address.location && this.state.address.location.suburb_id }
								style={ Styles.column }
							/>
						),
					}, {
						// we do this to separate the update cycle
						children: (
							<InputValidatedBit required
								disabled={ this.state.isLoadingArea }
								type={InputValidatedBit.TYPES.POSTAL}
								key={this.state.invalidPostal.length}
								validator={this.checkInvalidPostal}
								title="Postal Code"
								placeholder="Enter Postal Code"
								description={ this.state.address.location && this.state.address.location.area_name ? `Kel. ${this.state.address.location.area_name}` : ' ' }
								onChange={this.onChangePostal}
								value={this.state.address.postal}
								style={Styles.columnLeft}
							/>
						),
					}]}
					onUpdate={this.onUpdate}
					style={[Styles.form, this.props.style]}
				/>
			)
		}
	}
)
