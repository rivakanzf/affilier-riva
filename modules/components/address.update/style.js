import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	form: {
		flexDirection: 'row',
		flexWrap: 'wrap',
	},

	column: {
		flex: 1,
		// width: '50%',
		marginTop: Sizes.margin.default,
	},

	columnLeft: {
		flex: 1,
		marginTop: Sizes.margin.default,
		marginLeft: Sizes.margin.default,
	},

	span: {
		alignSelf: 'stretch',
		width: '100%',
		marginTop: Sizes.margin.default,
	},

	phone: {
		paddingLeft: 0,
	},

	breaker: {
		alignSelf: 'stretch',
		width: '100%',
	},

	// padderLeft: {
	// 	marginLeft: 8,
	// },

	// padderRight: {
	// 	marginRight: 8,
	// },
})
