import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import TouchableBit from 'modules/bits/touchable';
import GeomanistBit from 'modules/bits/geomanist';

import RowsLego from 'modules/legos/rows';

import AddressListPagelet from 'modules/pagelets/address.list'

import Styles from './style';


export default ConnectHelper(
	class AddressesComponent extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				addable: PropTypes.bool,
				type: PropTypes.oneOf([
					'order',
					'user',
					'brand',
				]),
				typeId: PropTypes.id,
				data: PropTypes.array,
				children: PropTypes.node,
				onChange:  PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			title: 'Shipping Information',
			data: [],
		}

		onModalRequestClose = () => {
			this.forceUpdate()
			this.props.utilities.alert.hide()
		}

		onShowAddresses = () => {
			this.props.utilities.alert.modal({
				component: (
					<AddressListPagelet
						type={ this.props.type }
						typeId={ this.props.typeId }
						addable={ this.props.addable }
						addresses={ this.props.data }
						onClose={ this.onModalRequestClose }
					/>
				),
			})
		}

		view() {
			const address = this.props.data[0]

			return (
				<RowsLego unflex={false}
					data={[{
						data: [{
							title: this.props.title,
							content: address
								? `${address.receiver}\n${address.phone}\n${address.address} - ${address.postal}\n${address.district}`
								: 'No address registered. ',
						}, this.props.children ? {
							headerless: true,
							children: this.props.children,
						} : {
							blank: true,
						}],
					// eslint-disable-next-line no-nested-ternary
					}, this.props.data.length > 1 ? {
						data: [{
							headerless: true,
							children: (
								<TouchableBit onPress={this.onShowAddresses} style={Styles.button}>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.text}>
										+{this.props.data.length - 1} Shipping Information
									</GeomanistBit>
								</TouchableBit>
							),
						}],
					} : this.props.addable ? {
						data: [{
							headerless: true,
							children: (
								<TouchableBit onPress={ this.onShowAddresses } style={ Styles.button }>
									<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.text }>
										Add address
									</GeomanistBit>
								</TouchableBit>
							),
						}],
					} : null ]}
				/>
			)
		}
	}
)
