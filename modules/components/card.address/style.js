import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		padding: [16, 24],
		backgroundColor: Colors.white.primary,
	},

	divider: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: Colors.black.palette(2, .2),
		borderTopStyle: 'solid',
	},

	button: {
		width: 80,
		marginLeft: 8,
	},

	mark: {
		marginTop: 16,
	},

	footnote: {
		color: Colors.green.primary,
		marginLeft: 8,
	},
})
