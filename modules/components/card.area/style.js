import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(3, .16),
	},

	header: {
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	capital: {
		textTransform: 'capitalize',
	},
	loader: {
		marginTop: 2,
		marginBottom: 2,
	},

	button: {
		width: 80,
		marginLeft: 8,
	},

	darkGrey: {
		color: Colors.black.palette(3),
	},
	darkGrey60: {
		color: Colors.black.palette(3, .6),
	},

	footnote: {
		color: Colors.primary,
	},
})
