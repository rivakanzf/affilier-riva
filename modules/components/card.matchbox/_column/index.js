import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import SpectralBit from 'modules/bits/spectral';

import Styles from './style';


export default ConnectHelper(
	class ColumnPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string,
					content: PropTypes.string,
					children: PropTypes.node,
				})).isRequired,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			data: [],
		}

		view() {
			return (
				<BoxBit unflex row style={[Styles.container, this.props.style]}>
					{ this.props.data.map((d, i) => {
						return (
							<BoxBit key={i} style={Styles.column}>
								<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_3} weight="medium" style={Styles.point}>{ d.title }</GeomanistBit>
								{ d.children ? d.children : (
									<SpectralBit type={SpectralBit.TYPES.SUBHEADER_2}>{ d.content }</SpectralBit>
								) }
							</BoxBit>
						)
					}) }
				</BoxBit>
			)
		}
	}
)
