import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderRadius: 4,
		margin: [16, 0],
		overflow: 'hidden',
	},

	badge: {
		width: 44,
		height: 24,
		position: 'absolute',
		top: 8,
		left: 8,
		backgroundColor: Colors.pink.palette(1),
		borderRadius: 2,
	},

	badgeColor: {
		color: Colors.black.palette(1, .9),
	},

	image: {
		height: 184 * (Sizes.screen.width - (Sizes.margin.thick * 2)) / 328,
	},

	content: {
		backgroundColor: Colors.white.primary,
		padding: 16,
	},
})
