import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		margin: [0, -6],
	},

	// border: {
	// 	borderBottomColor: Colors.black.palette(2, .2),
	// 	borderBottomWidth: 1,
	// 	borderBottomStyle: 'solid',
	// },

	column: {
		margin: [0, 6],
	},

	title: {
		color: Colors.black.palette(2, .5),
		marginBottom: 4,
	},

	content: {
		color: Colors.black.palette(1, .9),
	},

})
