import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		padding: [16, Sizes.margin.thick],
		backgroundColor: Colors.white.primary,
		borderColor: Colors.black.palette(2, .2),
		borderWidth: 0,
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
	},

	header: {
		justifyContent: 'space-between',
		alignItems: 'center',
	},

	status: {
		marginBottom: 8,
	},

	bottom4: {
		marginBottom: 4,
	},

	row: {
		marginBottom: 16,
	},

	colorBlack: {
		color: Colors.black.palette(1, .9),
	},

	colorGrey: {
		color: Colors.black.palette(2, .7),
	},

	colorPale: {
		color: Colors.black.palette(2, .5),
	},
})
