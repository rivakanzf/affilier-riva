import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		width: 176,
	},

	image: {
		height: 232,
	},

	content: {
		padding: 8,
	},

	aline: {
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		textOverflow: 'ellipsis',
	},

	brand: {
		color: Colors.black.palette(2, .8),
	},

	title: {
		color: Colors.black.palette(2, .8),
	},

	discount: {
		color: Colors.red.palette(7),
		marginRight: Sizes.margin.default,
	},

	retail: {
		textDecoration: 'line-through',
		color: Colors.black.palette(1, .7),
	},

	price: {
		color: Colors.black.palette(2, .7),
	},

	empty: {
		height: 24,
	},

	colors: {
		marginTop: 8,
		alignItems: 'center',
		flexWrap: 'wrap',
	},

	color: {
		marginRight: 4,
		padding: 4,
	},

	colorActive: {
		padding: 2,
		borderWidth: 2,
		borderColor: Colors.black.primary,
		borderRadius: 16,
	},

	sizes: {
		padding: 4,
		alignSelf: 'flex-start',
	},

	size: {
		minWidth: 24,
		marginTop: 2,
		marginBottom: 2,
		marginLeft: 1,
		marginRight: 1,
		paddingLeft: 4,
		paddingRight: 4,
		paddingTop: 1,
		paddingBottom: 1,
		backgroundColor: Colors.solid.grey.palette(1, .16),
		// backgroundColor: Colors.solid.grey.palette(1),
	},

	sizeActive: {
		backgroundColor: Colors.solid.grey.palette(2),
	},

	sizeTitle: {
		color: Colors.black.palette(2, .6),
	},

	sizeTitleActive: {
		color: Colors.white.primary,
	},

	icon: {
		backgroundColor: Colors.yellow.primary,
		position: 'absolute',
		zIndex: 1,
		padding: 4,
	},

})
