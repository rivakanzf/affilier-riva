import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/connected.stateful'

import FormatHelper from 'coeur/helpers/format'

import BoxBit from 'modules/bits/box'
import BoxImageBit from 'modules/bits/box.image'
import GeomanistBit from 'modules/bits/geomanist'
import TouchableBit from 'modules/bits/touchable'

import ColorLego from 'modules/legos/color'

import Styles from './style'


export default ConnectHelper(
	class CardProductComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				variantId: PropTypes.id,
				width: PropTypes.number,
				image: PropTypes.string,
				brand: PropTypes.string,
				title: PropTypes.string,
				price: PropTypes.number,
				purchasePrice: PropTypes.number,
				colors: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({
					hex: PropTypes.string,
					image: PropTypes.string,
				}))),
				onPress: PropTypes.func,
				style: PropTypes.style,
				imageStyle: PropTypes.style,
			}
		}

		onPress = () => {
			this.props.onPress &&
			this.props.onPress(this.props.variantId)
		}

		colorRenderer = (colors, index) => {
			return (
				<ColorLego border={ this.props.noBorder ? 0 : undefined } colors={colors} key={index} style={!!index ? Styles.color : {}} />
			)
		}

		view() {
			return (
				<TouchableBit unflex onPress={ this.onPress } style={[Styles.container, {
					width: this.props.width,
				}, this.props.style]}>

					<BoxBit unflex>
						<BoxImageBit unflex overlay
							source={ this.props.image && `//${this.props.image}` }
							resizeMode={ BoxImageBit.TYPES.COVER }
							broken={ !this.props.image }
							transfrom={{ crop: 'fit' }}
							style={[
								Styles.image, {
									height: this.props.width * 1.32,
								},
							]}
						/>
						{ this.props.colors &&
							<BoxBit unflex row style={Styles.colors}>
								{ this.props.colors.map(this.colorRenderer) }
							</BoxBit>
						}
					</BoxBit>
					<BoxBit unflex style={Styles.content}>
						<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.aline, Styles.brand]}>
							{ this.props.brand && this.props.brand }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.aline, Styles.title]}>
							{ this.props.title }
						</GeomanistBit>
						{ this.props.price !== this.props.purchasePrice ? (
							<BoxBit unflex row>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.retail}>IDR { FormatHelper.currencyFormat(this.props.price) }</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.discount}>IDR { FormatHelper.currencyFormat(this.props.purchasePrice) }</GeomanistBit>
							</BoxBit>
						) : (
							<BoxBit unflex>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.price}>IDR { FormatHelper.currencyFormat(this.props.price) }</GeomanistBit>
								<BoxBit unflex style={Styles.empty} />
							</BoxBit>
						) }
						<BoxBit style={Styles.mT6}>
							<BoxBit unflex style={Styles.getUrl}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} align="center">
									Get Link
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
						
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)
