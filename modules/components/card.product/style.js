import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'utils/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		width: 176,
		backgroundColor: Colors.white.primary,
	},

	image: {
		height: 232,
	},

	content: {
		padding: 8,
	},

	aline: {
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		textOverflow: 'ellipsis',
	},

	brand: {
		color: Colors.black.palette(2, .6),
	},

	title: {
		color: Colors.black.palette(2),
	},

	discount: {
		color: Colors.red.palette(7),
	},
	
	retail: {
		textDecoration: 'line-through',
		color: Colors.black.palette(2, .6),
		paddingRight: 6,
	},

	price: {
		color: Colors.black.palette(2, .6),
	},

	empty: {
		height: 24,
	},

	colors: {
		backgroundColor: Colors.grey.palette(1, .1),
		position: 'absolute',
		bottom: 0,
		// marginLeft: 4,
		padding: 4,
		// paddingRight: 0,
		alignItems: 'center',
		flexWrap: 'wrap',
	},

	color: {
		// marginRight: 4,
		marginLeft: 4,
	},

	icon: {
		backgroundColor: Colors.yellow.primary,
		position: 'absolute',
		zIndex: 1,
		padding: 4,
	},

	mT6: {
		marginTop: 6,
	},

	getUrl: {
		paddingTop: 8,
		paddingBottom: 8,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.primary,
		borderRadius: 5,
	},

})
