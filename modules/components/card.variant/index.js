/* eslint-disable no-nested-ternary */
import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import FormatHelper from 'coeur/helpers/format';

import UtilitiesContext from 'coeur/contexts/utilities'


import VariantService from 'app/services/variant'
import ProductService from 'app/services/product'
import StylecardService from 'app/services/stylecard'

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';
import LoaderBit from 'modules/bits/loader'
import SelectionBit from 'modules/bits/selection'
import ButtonBit from 'modules/bits/button'

import { capitalize } from 'lodash'
// import CardComponentModel from 'coeur/models/components/card';
// import CardMediaComponent from 'modules/components/card.media';

import Styles from './style'


export default ConnectHelper(
	class CardVariantComponent extends PromiseStatefulModel {

		static stateToProps(state) {
			return {
				token: state.me.token,
				data: {},
				availableVariants: [],
			}
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				width: PropTypes.number,
				stylecardId: PropTypes.number,
				isAddable: PropTypes.bool,
				onAddItem: PropTypes.func,
				onAddItemRec: PropTypes.func,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				selectedId: null,
				isLoading: true,
				isAddingOrRemoving: false,
				isAvailable: true,
			})
		}

		componentDidMount() {
			this.getData()
		}

		getData = () => {
			VariantService.getVariantById(this.props.id, { item: true }, this.props.token).then(res => {
				this.setState({
					data: res,
					isAvailable: res.is_available,
				}, () => {
					ProductService.getProductVariants(this.state.data.product_id, this.props.token).then(res2 => {
						this.setState({
							availableVariants: res2.data,
							isLoading: false,
						})
					}).catch(() => this.setState({isLoading: false}))
				})
			})
		}

		onChangeVariant = (key) => {
			this.setState(state => ({
				selectedId: state.selectedId !== key ? key : null,
				isAvailable: state.availableVariants.find(variant => variant.id === key).is_available,
			}))
		}

		onPress = () => {
			this.props.onPress &&
			this.props.onPress(this.props.id)
		}

		onAddToStylecard = () => {
			this.setState({
				isAddingOrRemoving: true,
			})

			StylecardService.putVariantIntoStylecard(this.props.stylecardId, { id: this.state.selectedId }, this.props.token).then(() => {
				this.props.utilities.notification.show({
					title: 'Adding Success',
					message: 'Variant is added into style card(s)',
					type: 'SUCCESS',
				})
				this.setState({
					isAddingOrRemoving: false,
				})
				this.props.onAddItem &&
				this.props.onAddItem()

				this.props.onAddItemRec &&
				this.props.onAddItemRec()
			}).catch(() => {
				this.props.utilities.notification.show({
					title: 'Failed',
					message: 'Variant cant be added into style card(s)',
				})
				this.setState({
					isAddingOrRemoving: false,
				})
			})
			
		}

		contentRenderer = () => {
			return (
				<TouchableBit key={`image_${this.state.selectedId}`} unflex onPress={ this.onPress } style={[Styles.container, this.props.style]}>
					<ImageBit
						resizeMode={ImageBit.TYPES.COVER}
						broken={ !this.state.data.image.url }
						source={ this.state.data.image.url && `//${this.state.data.image.url}` }
						style={ [Styles.image, {
							height: this.props.width * 1.32,
						}] }
					/>
					<BoxBit style={Styles.content}>
						<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.aline, Styles.brand]}>
							{ this.state.data.brand && this.state.data.brand.toUpperCase() }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.aline, Styles.title]}>
							{ this.state.data.title }
						</GeomanistBit>
						{ this.state.data.price !== this.state.data.retail_price ? (
							<BoxBit unflex>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.retail}>IDR { FormatHelper.currencyFormat(this.state.data.retail_price) }</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.discount}>IDR { FormatHelper.currencyFormat(this.state.data.price) }</GeomanistBit>
							</BoxBit>
						) : (
							<BoxBit unflex>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.price}>IDR { FormatHelper.currencyFormat(this.state.data.price) }</GeomanistBit>
								<BoxBit unflex style={Styles.empty} />
							</BoxBit>
						) }
					</BoxBit>
					<SelectionBit
						key={this.state.selectedId}
						options={this.optionsRenderer()}
						onChange={this.onChangeVariant}
					/>
					<ButtonBit
						title={this.state.isAvailable ? 'Add' : 'Not available'}
						theme={ButtonBit.TYPES.THEMES.BLACK}
						onPress={ this.onAddToStylecard }
						state={ this.state.isAddingOrRemoving
							? ButtonBit.TYPES.STATES.LOADING
							: !this.props.isAddable || !this.state.isAvailable || !this.state.selectedId
								? ButtonBit.TYPES.STATES.DISABLED
								: ButtonBit.TYPES.STATES.NORMAL
						}
					/>
				</TouchableBit>
			)
		}

		optionsRenderer = () => {
			return this.state.availableVariants.map(variant => {
				return {
					key: variant.id,
					title: `${capitalize(variant.color)} - ${capitalize(variant.size.title)}`,
					selected: variant.id === this.state.selectedId,
				}
			})
		}

		loadingRenderer = () => {
			return (
				<BoxBit unflex style={ Styles.container }>
					<LoaderBit simple/>
				</BoxBit>
			)
		}

		view() {
			return (
				this.state.isLoading
					? this.loadingRenderer()
					: this.contentRenderer()
			)
		}
	}
)
