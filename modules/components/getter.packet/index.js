import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
// import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';

import { isEmpty } from 'lodash'

export default ConnectHelper(
	class GetterPacketComponent extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				packet: PropTypes.object,
				packetId: PropTypes.id,
				variantId: PropTypes.id,
				productId: PropTypes.id,
				children: PropTypes.func,
			}
		}

		static propsToQuery(state, oP) {
			if (oP.variantId) {
				return [`
					query {
						variantById(id: ${oP.variantId}) {
							packet {
								weight
								length
								width
								height
							}
							product {
								packet {
									weight
									length
									width
									height
								}
							}
						}
					}
				`, {
					fetchPolicy: 'no-cache',
				}, state.me.token]
			} else if (oP.productId) {
				return [`
					query {
						productById(id: ${oP.productId}) {
							packet {
								weight
								length
								width
								height
							}
						}
					}
				`, {
					fetchPolicy: 'no-cache',
				}, state.me.token]
			} else if (oP.packetId) {
				return [`
					query {
						packetById(id: ${oP.packetId}) {
							weight
							length
							width
							height
						}
					}
				`, {
					fetchPolicy: 'no-cache',
				}, state.me.token]
			} else {
				return null
			}
		}

		static getDerivedStateFromProps(nP) {
			if (!nP.packetId && !nP.variantId && !nP.productId) {
				return null
			} else if (isEmpty(nP.data)) {
				return null
			} else if (nP.isLoading) {
				return null
			}

			if (nP.variantId && (!isEmpty(nP.data.variantById && nP.data.variantById.packet) || !isEmpty(nP.data.variantById && nP.data.variantById.product && nP.data.variantById.product.packet))) {
				return {
					data: {
						weight: nP.data.variantById.packet && nP.data.variantById.packet.weight,
						length: nP.data.variantById.packet && nP.data.variantById.packet.length,
						width: nP.data.variantById.packet && nP.data.variantById.packet.width,
						height: nP.data.variantById.packet && nP.data.variantById.packet.height,
					},
					parentData: {
						weight: nP.data.variantById.product && nP.data.variantById.product.packet && nP.data.variantById.product.packet.weight,
						length: nP.data.variantById.product && nP.data.variantById.product.packet && nP.data.variantById.product.packet.length,
						width: nP.data.variantById.product && nP.data.variantById.product.packet && nP.data.variantById.product.packet.width,
						height: nP.data.variantById.product && nP.data.variantById.product.packet && nP.data.variantById.product.packet.height,
					},
				}
			} else if (nP.productId && !isEmpty(nP.data.productById && nP.data.productById.packet)) {
				return {
					parentData: {
						weight: nP.data.productById.packet.weight,
						length: nP.data.productById.packet.length,
						width: nP.data.productById.packet.width,
						height: nP.data.productById.packet.height,
					},
				}
			} else if(nP.packetId && !isEmpty(nP.data.packetById)) {
				return {
					data: {
						weight: nP.data.packetById.weight,
						length: nP.data.packetById.length,
						width: nP.data.packetById.width,
						height: nP.data.packetById.height,
					},
				}
			}

			return null
		}

		static defaultProps = {
			packet: {},
		}

		constructor(p) {
			super(p, {
				data: {
					weight: p.packet.weight,
					length: p.packet.length,
					width: p.packet.width,
					height: p.packet.height,
				},
				parentData: {
					weight: undefined,
					length: undefined,
					width: undefined,
					height: undefined,
				},
			})
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (
				this.props.children(this.props.variantId || this.props.productId ? [this.state.data, this.state.parentData] : this.state.data)
			)
		}
	}
)
