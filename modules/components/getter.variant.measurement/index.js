import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

// import Colors from 'coeur/constants/color'
import CommonHelper from 'coeur/helpers/common';

import VariantService from 'app/services/variant'

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
// import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';

const cache = {}

export default ConnectHelper(
	class GetterVariantMeasurementComponent extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				variantId: PropTypes.id,
				measurements: PropTypes.array,
				children: PropTypes.func,
			}
		}

		static propsToPromise(state, oP) {
			if(oP.variantId) {
				const _cache = CommonHelper.default(oP.cache, true)

				if(_cache && cache[oP.variantId]) {
					return Promise.resolve({
						measurements: cache[oP.variantId],
					})
				} else {
					return VariantService.getVariantMeasurements(oP.variantId, state.me.token).then(measurements => {
						cache[oP.variantId] = measurements

						return {
							measurements,
						}
					})
				}
			} else {
				return null
			}
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (this.props.children(this.props.data))
		}
	}
)
