import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

// import Colors from 'coeur/constants/color'
import CommonHelper from 'coeur/helpers/common';

import VariantService from 'app/services/variant'

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
// import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';

const cache = {}

export default ConnectHelper(
	class GetterVariantTagComponent extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				variantId: PropTypes.id,
				tags: PropTypes.array,
				children: PropTypes.func,
			}
		}

		static propsToPromise(state, oP) {
			if(oP.variantId) {
				const _cache = CommonHelper.default(oP.cache, true)

				if(_cache && cache[oP.variantId]) {
					return Promise.resolve({
						tags: cache[oP.variantId],
					})
				} else {
					return VariantService.getVariantTagsGrouped(oP.variantId, state.me.token).then(tags => {
						cache[oP.variantId] = tags

						return {
							tags,
						}
					})
				}
			} else {
				return null
			}
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (this.props.children(this.props.data))
		}
	}
)
