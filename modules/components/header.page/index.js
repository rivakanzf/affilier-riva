import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TextInputBit from 'modules/bits/text.input';

import BreadcrumbLego from 'modules/legos/breadcrumb';

import Styles from './style';


export default ConnectHelper(
	class HeaderPageComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				paths: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string.isRequired,
					onPress: PropTypes.func,
				})),
				search: PropTypes.string,
				searchPlaceholder: PropTypes.string,
				buttonPlaceholder: PropTypes.string,
				onSearch: PropTypes.func,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			searchPlaceholder: 'Search',
			buttonPlaceholder: 'Add',
		}

		constructor(p) {
			super(p, {}, [])

			this._searchValue = undefined
		}

		onUpdateSearch = (e, val) => {
			this._searchValue = val
		}

		onSearch = () => {
			this.props.onSearch &&
			this.props.onSearch(this._searchValue)
		}

		view() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					{ this.props.paths && (
						<BreadcrumbLego paths={ this.props.paths } style={Styles.breadcrumb} />
					) }
					<BoxBit row unflex style={Styles.content}>
						<BoxBit style={Styles.header}>
							<GeomanistBit
								type={GeomanistBit.TYPES.HEADER_4}
								style={Styles.title}>
								{ this.props.title }
							</GeomanistBit>
						</BoxBit>
						{ this.props.onPress ? (
							<ButtonBit
								title={ this.props.buttonPlaceholder }
								weight="medium"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								onPress={ this.props.onPress }
							/>
						) : false }
						{ this.props.onSearch ? (
							<TextInputBit
								defaultValue={ this.props.search }
								placeholder={ this.props.searchPlaceholder }
								onChange={ this.onUpdateSearch }
								onSubmitEditing={ this.onSearch }
								inputStyle={ Styles.input }
								style={ Styles.search }
								postfix={(
									// <TouchableBit unflex onPress={this.onSearch} style={Styles.icon} >
									<IconBit
										name="search"
										size={ 20 }
										color={ Colors.black.palette(2, .6) }
										style={ Styles.icon }
									/>
									// </TouchableBit>
								)}
							/>
						) : false }
					</BoxBit>
					<BoxBit row unflex style={Styles.filters}>
						{ React.Children.map(this.filterRenderer) }
					</BoxBit>
				</BoxBit>
			);
		}
	}
)
