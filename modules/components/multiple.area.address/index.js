import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import AddressService from 'app/services/address';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';
import HeaderBit from 'modules/bits/header';

import NotificationLego from 'modules/legos/notification';

import CardAreaComponent from 'modules/components/card.area';

import Styles from './style';
// import { isEmpty } from 'lodash';


export default ConnectHelper(
	class MultipleAreaAddressComponent extends StatefulModel {
		header = {
			leftActions: [{
				type: HeaderBit.TYPES.CLOSE,
				onPress: this.onRequestClose,
			}],
			rightActions: [{
				data: 'DONE',
				isActive: true,
				type: HeaderBit.TYPES.TEXT,
				onPress: this.onSubmit,
			}],
		}

		static propTypes(PropTypes) {
			return {
				data: PropTypes.array,
				selectedAddressId: PropTypes.number,
				onRequestClose: PropTypes.func,
				onSubmit: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				isChanged: false,
				selectedAreaId: -1,
			}, [
				'onSubmit',
			])
		}

		onPress = (id) => {
			this.setState({
				isChanged: true,
				selectedAreaId: id,
			})
		}
		
		onSubmit() {
			if(this.state.isChanged) {
				this.setState({
					isLoading: true,
				}, () => {
					AddressService.updateAddress(this.props.selectedAddressId, {
						metadata: {
							tikiAreaId: this.state.selectedAreaId,
						},
					}, this.props.token)
						.then(() => {
							this.props.onSubmit &&
							this.props.onSubmit()
						}).catch(this.props.onError)
				})
			} else {
				this.onRequestClose()
			}
		}

		addressRenderer = (data, i) => {
			return (
				<CardAreaComponent
					key={i}
					selectionMode
					id={data.id}
					data={data}
					onPress={this.onPress.bind(this, data.id)}
					isActive={this.state.selectedAreaId === data.id}
				/>
			)
		}

		onRequestClose = () => {
			this.props.onRequestClose &&
			this.props.onRequestClose()
		}

		loadingRenderer() {
			return (
				<BoxBit centering>
					<LoaderBit />
				</BoxBit>
			)
		}

		view() {
			return (
				<PageBit
					header={ <HeaderBit { ...this.header }
						title={ this.props.title }
						style={Styles.borderBottom}
					/> }
					style={Styles.page}
					contentContainerStyle={ Styles.container }
				>
					<NotificationLego
						message="You have ambigous location, please select one of these area"
						type={NotificationLego.TYPES.GREY}
					/>
					{ this.state.isLoading
						? this.loadingRenderer()
						: this.props.data.map(this.addressRenderer)
					}
				</PageBit>
			)
		}
	}
)
