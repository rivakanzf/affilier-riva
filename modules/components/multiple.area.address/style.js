import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	page: {
		height: Sizes.app.height,
	},
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingBottom: 64,
	},

	firstRow: {
		borderTopWidth: StyleSheet.hairlineWidth,
	},

	status: {
		marginTop: 16,
	},

	icon: {
		marginLeft: 12,
	},

	date: {
		color: Colors.black.palette(3, .7),
	},

	colorCoal: {
		color: Colors.black.palette(3),
	},
})
