import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		transition: '.3s all cubic-bezier(0.5, 0.01, 0.4, 1.05)',
	},
	content: {
		backgroundColor: Colors.solid.grey.palette(1),
	},
	geser: {
		transform: 'translateX(-240px)',
	},
	icon: {
		marginRight: 8,
	},
	title: {
		color: Colors.primary,
	},
	desc: {
		marginTop: 4,
		marginBottom: 24,
	},
})
