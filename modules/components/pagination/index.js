import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';

import Styles from './style';


export default ConnectHelper(
	class PaginationComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				current: PropTypes.number,
				count: PropTypes.number,
				total: PropTypes.number,
				onNavigationChange: PropTypes.func,
				style: PropTypes.style,
				// onGoToFirstPress: PropTypes.func,
				// onGoToPrevPress: PropTypes.func,
				// onIndexPress: PropTypes.func,
				// onGoToNextPress: PropTypes.func,
				// onGoToLastPress: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static getDerivedStateFromProps(nP, nS) {
			if (
				nS.lastCurrent !== nP.current
				|| nS.lastCount !== nP.count
				|| nS.lastTotal !== nP.total
			) {
				return {
					lastCurrent: nP.current,
					lastCount: nP.count,
					lastTotal: nP.total,
					current: nP.current,
					count: nP.count,
				}
			}

			return null
		}

		static defaultProps = {
			count: Infinity,
		}

		constructor(p) {
			super(p, {
				lastCurrent: p.current,
				lastCount: p.count,
				lastTotal: p.total,
				current: p.current,
				count: p.count,
			})
		}

		shouldComponentUpdate(nP, nS) {
			return nS.current !== this.state.current
				|| nS.count !== this.state.count
				|| nP.total !== this.props.total
		}

		componentDidUpdate() {
			this.props.onNavigationChange &&
			this.props.onNavigationChange(this.state.current, this.state.count)
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onGoToFirst = () => {
			this.setState({
				current: 0,
			})
		}

		onGoToLast = () => {
			this.setState({
				current: this.props.total - this.state.count,
			})
		}

		onGoToPrev = () => {
			this.setState({
				current: Math.max(0, this.state.current - this.state.count),
			})
		}

		onGoToNext = () => {
			this.setState(state => ({
				current: state.current + state.count,
			}))
		}

		onChangeIndex = index => {
			this.setState({
				current: Math.max(0, Math.min(this.props.total - this.state.count, parseInt(index, 10))),
			})
		}

		onChangeViewPerPage = count => {
			this.setState({
				count,
			})
		}

		onChangeCount = () => {
			this.props.utilities.menu.show({
				title: 'View per page',
				actions: [{
					title: '20',
					isActive: this.state.count === 20,
					onPress: this.onChangeViewPerPage.bind(this, 20),
					type: this.props.utilities.menu.TYPES.ACTION.RADIO,
				}, {
					title: '40',
					isActive: this.state.count === 40,
					onPress: this.onChangeViewPerPage.bind(this, 40),
					type: this.props.utilities.menu.TYPES.ACTION.RADIO,
				}, {
					title: '80',
					isActive: this.state.count === 80,
					onPress: this.onChangeViewPerPage.bind(this, 80),
					type: this.props.utilities.menu.TYPES.ACTION.RADIO,
				}, {
					title: '160',
					isActive: this.state.count === 160,
					onPress: this.onChangeViewPerPage.bind(this, 160),
					type: this.props.utilities.menu.TYPES.ACTION.RADIO,
				}],
				closeOnPress: true,
			})
		}

		onJumpToPage = () => {
			if( this.props.total + 1 - this.state.count > 0 ) {
				this.props.utilities.alert.modal({
					component: (
						<ModalPromptPagelet
							title="Go To Index"
							placeholder={`0 - ${this.props.total + 1 - this.state.count}`}
							onCancel={this.onModalRequestClose}
							onConfirm={this.onChangeIndex}
						/>
					),
				})
			}
		}

		view() {
			const end = Math.min(this.state.count + this.state.current, this.props.total)

			return (
				<BoxBit unflex row style={ this.props.style }>
					{ this.state.current + 1 > end ? false : (
						<React.Fragment>
							<TouchableBit unflex centering style={ Styles.nav }>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="semibold" style={Styles.text} >
									{ this.state.current + 1 }
								</GeomanistBit>
							</TouchableBit>
							<BoxBit unflex centering>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.text} >
									-
								</GeomanistBit>
							</BoxBit>
							<TouchableBit unflex centering style={ Styles.nav }>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="semibold" style={Styles.text} >
									{ this.state.current + this.state.count }
								</GeomanistBit>
							</TouchableBit>
							<BoxBit unflex centering>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.text} >
									of <TextBit weight="semibold" style={Styles.text}>{ this.props.total }</TextBit>
								</GeomanistBit>
							</BoxBit>
						</React.Fragment>
					) }
					<ButtonBit
						state={ this.state.current === 0 ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						type={ ButtonBit.TYPES.SHAPES.ICON }
						icon="arrow-left"
						onPress={ this.onGoToPrev }
						style={ Styles.button }
					/>
					<ButtonBit
						state={ this.props.total === end ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						type={ ButtonBit.TYPES.SHAPES.ICON }
						icon="arrow-right"
						onPress={ this.onGoToNext }
					/>
				</BoxBit>
			)
		}
	}
)
