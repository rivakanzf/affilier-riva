import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/connected.stateful'

import Sizes from 'coeur/constants/size'

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box'
import IconBit from 'modules/bits/icon'
import GeomanistBit from 'modules/bits/geomanist'
import TouchableBit from 'modules/bits/touchable'

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view'
import Styles from './style'

export default ConnectHelper(
	class ShareComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				affiliate: PropTypes.string,
				route: PropTypes.string,
				onClose: PropTypes.func,
				onCopyLink: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				firstName: state.me.firstName,
				userId: state.me.id,
			}
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()
		}

		onCopyLink = () => {
			this.props.onCopyLink &&
			this.props.onCopyLink(this.shareLink())
		}

		shareLink = () => {
			const siteName = process.env.ENV === 'production' ? 'https://helloyuna.io/' : 'https://staging.helloyuna.io/'
			const route = this.props.route ? this.props.route : 'product/variant/'
			const id = this.props.id
			const affiliate = 'affiliate=' + this.props.userId
			
			const link = siteName + route + id + '?tr_source=affiliate&' + affiliate

			return link
		}

		view() {
			return (
				<ModalQuickViewPagelet unflex
					layout="free"
					onClose={this.onClose}
					style={Styles.page}
				>
					<BoxBit unflex centering>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
							Link Affiliate
						</GeomanistBit>

						<BoxBit row>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{ this.shareLink() }
							</GeomanistBit>
							<TouchableBit unflex onPress={ this.onCopyLink } style={ Styles.copyIcon }>
								<IconBit name="copy" size={16}/>
							</TouchableBit>
						</BoxBit>
					</BoxBit>
				</ModalQuickViewPagelet>
			)
		}
	}
)

