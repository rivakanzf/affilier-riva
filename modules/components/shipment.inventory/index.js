import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';

import ManualShipmentPart from 'modules/pages/order.update/_shipment/manual_shipment';
import Styles from './style';
import { isEmpty } from 'lodash';


export default ConnectHelper(
	class ShipmentInventoryComponent extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				data: PropTypes.array,
				orderId: PropTypes.id,
				method: PropTypes.string,
				onShip: PropTypes.func,
				onRefresh: PropTypes.func,

				receiver: PropTypes.string,
				phone: PropTypes.string,
				address: PropTypes.string,
				district: PropTypes.string,
				postal: PropTypes.string,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
			}, [
				'awbRenderer',
				'listRenderer',

				'onCheck',
				'onPress',
			])

			this.products = [];
		}

		isEligibleToShip(status) {
			return status === 'PRIMED'
		}

		onCheck(product) {
			const found = this.products.findIndex(s => s.id === product.orderDetailId)

			if(found > -1) {
				this.products.splice(found, 1)
			} else {
				this.products.push({
					id: product.orderDetailId,
					title: product.title,
					packetId: product.packet ? product.packet.id : undefined,
					address: product.metadata.address,
				})
			}
			this.props.onUpdate &&
			this.props.onUpdate(this.products)
		}

		onPress(detailId, shipment) {
			// INI UNTUK EDIT/VIEW
			const newDetail = []

			this.props.utilities.alert.modal({
				component: (
					<ManualShipmentPart
						isEditing
						orderId={ this.props.orderId }
						shipmentId={ shipment.id }
						detailIds={ newDetail.push(detailId)}

						method={ this.props.method }
						onRefresh={ this.props.onRefresh }

						receiver={ this.props.receiver }
						phone={ this.props.phone }
						address={ this.props.address }
						district={ this.props.district }
						postal={ this.props.postal }
					/>
				),
			})
		}

		onNavigateToStylesheet(stylesheetId) {
			this.props.onNavigateToStylesheet &&
			this.props.onNavigateToStylesheet(stylesheetId)
		}

		awbRenderer(detailId, shipment, i) {
			return (
				<BoxBit key={i} unflex row style={[Styles.awbContainer, {
					marginTop: i === 0 ? 16 : 4,
				}]}>
					<BoxBit row style={{flexGrow: 1}}>
						<IconBit
							name="shipment"
							size={20}
							color={Colors.black.palette(2, .8)}
							style={{paddingRight: 8}}
						/>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							weight="medium"
							style={[Styles.darkGrey, {paddingRight: 8}]}
						>
							{i + 1}
						</GeomanistBit>
						<BoxBit />
					</BoxBit>
					<BoxBit unflex style={Styles.divider} />
					<BoxBit row style={{flexGrow: 9, alignItems: 'center'}}>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={[Styles.darkGrey, Styles.uppercase]}
						>
							Service: {shipment.courier} - {shipment.service} — AWB: {shipment.awb || '-'}
						</GeomanistBit>
					</BoxBit>
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_3}
						style={[Styles.darkGrey, Styles.capital]}
					>
						{ shipment.status && shipment.status.code.toLowerCase() || '-'}
					</GeomanistBit>
					<TouchableBit unflex onPress={ this.onPress.bind(this, detailId, shipment) } style={Styles.padder}>
						<IconBit
							name="quick-view"
							color={Colors.primary}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		listRenderer(product, i) {
			return (
				<BoxBit key={i} unflex style={Styles.list}>
					<BoxBit unflex row centering>
						<ImageBit
							broken={!product.asset}
							source={product.asset || undefined}
							style={Styles.image}
						/>
						<BoxBit>
							<GeomanistBit
								type={GeomanistBit.TYPES.PARAGRAPH_3}
								weight="medium"
								style={[Styles.darkGrey, Styles.capital]}
							>
								{ product.title || '-' }
							</GeomanistBit>
							<GeomanistBit
								type={GeomanistBit.TYPES.PARAGRAPH_3}
								style={Styles.darkGrey60}
							>
								Matchbox ID: #{product.id} — Qty: 1
							</GeomanistBit>

							{ product.type === 'MATCHBOX' && (
								<TouchableBit
									unflex
									row
									onPress={this.onNavigateToStylesheet.bind(this, product.stylesheetId)}
								>
									<GeomanistBit
										type={GeomanistBit.TYPES.PARAGRAPH_3}
										weight="medium"
										style={Styles.primary}
									>
										View Stylesheet
									</GeomanistBit>
									<IconBit name="arrow-right-tailed" size={20} color={Colors.primary} />
								</TouchableBit>
							)}
						</BoxBit>

						<BoxBit unflex style={Styles.padder}>
							{ product.status
								&& this.isEligibleToShip(product.status)
								&& <CheckboxBit onPress={this.onCheck.bind(this, product)}/>
								|| null
							}
						</BoxBit>

					</BoxBit>
					{ !isEmpty(product.shipments)
						? product.shipments.map(this.awbRenderer.bind(this, product.orderDetailId))
						: null
					}
				</BoxBit>
			);
		}

		view() {
			return this.props.data && this.props.data.map(this.listRenderer)
		}
	}
)
