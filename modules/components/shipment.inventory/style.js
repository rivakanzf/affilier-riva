import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	list: {
		padding: 16,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},
	image: {
		height: 60,
		width: 60,
		marginRight: 16,
	},
	darkGrey: {
		color: Colors.black.palette(2),
	},
	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},
	primary: {
		color: Colors.primary,
	},
	uppercase: {
		textTransform: 'uppercase',
	},
	capital: {
		textTransform: 'capitalize',
	},
	margin: {
		marginTop: 8,
		marginBottom: 8,
		marginLeft: 8,
		marginRight: 8,
	},
	padder: {
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: 8,
		paddingRight: 8,
	},
	awbContainer: {
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: 16,
		paddingRight: 16,
		alignItems: 'center',
		backgroundColor: Colors.solid.grey.palette(1),
	},
	clipboard: {
		paddingLeft: 4,
		opacity: 1,
		backgroundColor: Colors.solid.grey.palette(1),
	},
	copy: {
		color: Colors.primary,
		borderWidth: 1,
		borderColor: Colors.primary,
		borderStyle: 'solid',
		borderRadius: 2,
		paddingTop: 3,
		paddingBottom: 3,
		paddingLeft: 8,
		paddingRight: 8,
		alignSelf: 'center',
		marginLeft: 16,
		marginRight: 16,
	},

	divider: {
		width: 1,
		height: 32,
		marginLeft: 8,
		marginRight: 8,
		backgroundColor: Colors.black.palette(2, .1),
	},

})
