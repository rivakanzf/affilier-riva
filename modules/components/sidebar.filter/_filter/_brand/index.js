import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
// import Colors from 'coeur/constants/color';

import BrandService from 'app/services/brand'

// import BoxBit from 'modules/bits/box';
// import CollapsibleBit from 'modules/bits/collapsible';
// import IconBit from 'modules/bits/icon';
// import CheckboxBit from 'modules/bits/checkbox';
// import GeomanistBit from 'modules/bits/geomanist';
// import RadioBit from 'modules/bits/radio';
// import TouchableBit from 'modules/bits/touchable';

import FilterCollapsibleLego from 'modules/legos/filter.collapsible';
import FilterSelectionLego from 'modules/legos/filter.selection';

import { isEmpty, orderBy, isEqual, intersection } from 'lodash'

const cache = {}


export default ConnectHelper(
	class BrandPart extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				categoryIds: PropTypes.arrayOf(PropTypes.id),
				brandIds: PropTypes.arrayOf(PropTypes.id),
				onChange: PropTypes.func,
			}
		}

		static propsToPromise(state, oP) {
			const key = oP.categoryIds ? oP.categoryIds.join(',') : 'all'
			return [key, cache[key] ? Promise.resolve(cache[key]) : BrandService.getBrandByCategories(oP.categoryIds ? oP.categoryIds : [], state.me.token)]
		}

		static getDerivedStateFromProps(nP) {
			const key = nP.categoryIds ? nP.categoryIds.join(',') : 'all'

			if (!isEmpty(nP.data)) {
				cache[key] = nP.data
			} else if (cache[key]) {
				// check for cache
			} else {
				return null
			}

			return {
				brands: [].concat(orderBy(cache[key].map(c => {
					return {
						key: c.id,
						title: c.title,
					}
				}), ['title'], 'asc')),
			}
		}

		static defaultProps = {
			categoryIds: [],
			brandIds: [],
		}

		constructor(p) {
			super(p, {
				brands: [],
			})
		}

		componentDidUpdate(pP, pS) {
			if(pP.isLoading && !this.props.isLoading && !isEqual(pS.brands, this.state.brands)) {
				this.props.onChange &&
				this.props.onChange(intersection(this.props.brandIds, this.state.brands.map(b => b.key)))
			}
		}

		viewOnError() {
			return (
				<FilterCollapsibleLego disabled title="Brand" />
			)
		}

		viewOnLoading() {
			return (
				<FilterCollapsibleLego disabled title="Brand" />
			)
		}

		view() {
			return (
				<FilterCollapsibleLego key={this.props.categoryIds.join(',')} title="Brand">
					<FilterSelectionLego key={ this.props.brandIds.join(',') } multiple values={ this.props.brandIds } selections={ this.state.brands } onChange={ this.props.onChange } />
				</FilterCollapsibleLego>
			)
		}
	}
)
