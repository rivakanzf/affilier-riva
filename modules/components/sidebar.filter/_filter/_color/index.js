import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
// import Colors from 'coeur/constants/color';

// import BoxBit from 'modules/bits/box';
// import CollapsibleBit from 'modules/bits/collapsible';
// import IconBit from 'modules/bits/icon';
// import CheckboxBit from 'modules/bits/checkbox';
// import GeomanistBit from 'modules/bits/geomanist';
// import RadioBit from 'modules/bits/radio';
// import TouchableBit from 'modules/bits/touchable';

import FilterCollapsibleLego from 'modules/legos/filter.collapsible';
import FilterSelectionLego from 'modules/legos/filter.selection';

import { isEmpty, orderBy, capitalize } from 'lodash'

let cache = null


export default ConnectHelper(
	class ColorPart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				colorIds: PropTypes.arrayOf(PropTypes.id),
				onChange: PropTypes.func,
			}
		}

		static propsToQuery(state) {
			return !cache ? [`query {
				colorsList {
					id
					title
				}
			}`, {}, state.me.token] : null
		}

		static getDerivedStateFromProps(nP) {
			if (!isEmpty(nP.data)) {
				cache = nP.data
			} else if (cache) {
				// check for cache
			} else {
				return null
			}

			return {
				colors: [].concat(orderBy(cache.colorsList.map(c => {
					return {
						key: c.id,
						title: capitalize(c.title),
					}
				}), ['title'], 'asc')),
			}
		}

		static defaultProps = {
			colorIds: [],
		}

		constructor(p) {
			super(p, {
				colors: [],
			})
		}

		view() {
			return (
				<FilterCollapsibleLego title="Color">
					<FilterSelectionLego key={ this.props.colorIds.join(',') } multiple selections={ this.state.colors } values={ this.props.colorIds } onChange={ this.props.onChange } />
				</FilterCollapsibleLego>
			)
		}
	}
)
