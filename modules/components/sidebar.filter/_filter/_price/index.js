import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
// import Colors from 'coeur/constants/color';

// import BoxBit from 'modules/bits/box';
// import CollapsibleBit from 'modules/bits/collapsible';
// import IconBit from 'modules/bits/icon';
// import CheckboxBit from 'modules/bits/checkbox';
// import GeomanistBit from 'modules/bits/geomanist';
// import RadioBit from 'modules/bits/radio';
// import TouchableBit from 'modules/bits/touchable';

import FilterCollapsibleLego from 'modules/legos/filter.collapsible';
import FilterInputLego from 'modules/legos/filter.input';
// import Styles from './style';

export default ConnectHelper(
	class PricePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				price: PropTypes.arrayOf(PropTypes.number),
				onChange: PropTypes.func,
			}
		}

		constructor(p) {
			super(p)

			this.inputs = [{
				id: 'min',
				key: 'min',
				value: p.price ? p.price[0] : null,
				title: 'Minimum',
			}, {
				id: 'max',
				key: 'max',
				value: p.price ? p.price[1] : null,
				title: 'Maximum',
			}]
		}

		onChange = (val) => {
			this.props.onChange &&
			this.props.onChange(val ? val.map(v => parseInt(v, 10)) : [])
		}

		view() {
			return (
				<FilterCollapsibleLego key="persist" noTopBorder title="Price">
					<FilterInputLego
						data={ this.inputs }
						price={ this.props.price }
						onChange={ this.onChange }
					/>
				</FilterCollapsibleLego>
			)
		}
	}
)
