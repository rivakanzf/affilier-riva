import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import MeManager from 'app/managers/me';

import AuthenticationHelper from 'utils/helpers/authentication'

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ScrollViewBit from 'modules/bits/scroll.view';
import TouchableBit from 'modules/bits/touchable';

// import InitialLego from '../initial';

import Styles from './style';

import { uniqBy, sortBy } from 'lodash'


export default ConnectHelper(
	class SidebarNavigationComponent extends ConnectedStatefulModel {

		static TYPES = {
			PRODUCT_DEALS: 'PRODUCT_DEALS',
			SUMMARY: 'SUMMARY',
			CUSTOM_LINK: 'CUSTOM_LINK',
			REPORT: 'REPORT',
		}

		static propTypes(PropTypes) {
			return {
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me,
				roles: state.me.roles,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p)

			// this.extVoucher = ['external-voucher', 'external-voucher.detail']
			// this.paymentLink = ['add.payment.link', 'payment-link']

			this.autoCloser = null
			this.tab = []
			this.configurations = {
				[this.TYPES.PRODUCT_DEALS]: {
					title: 'Product Deals',
					icon: 'product',
					onPress: this.onNavigateToProductDeals,
					isActive: ['product.deals'].indexOf(p.page.routeName) > -1,
				},
				[this.TYPES.SUMMARY]: {
					title: 'Summary',
					icon: 'purchases',
					onPress: this.onNavigateToSummary,
					isActive: ['summary'].indexOf(p.page.routeName) > -1,
				},
				[this.TYPES.CUSTOM_LINK]: {
					title: 'Custom Link',
					icon: 'edit',
					onPress: this.onNavigateToCustomLink,
					isActive: ['custom.link'].indexOf(p.page.routeName) > -1,
				},
				[this.TYPES.REPORT]: {
					title: 'Report',
					icon: 'requests',
					onPress: this.onNavigateToReport,
					isActive: ['report'].indexOf(p.page.routeName) > -1,
				},
			}

			// =========================================================================================
			//  INVENTORY
			// =========================================================================================
			if (AuthenticationHelper.isAuthorized(this.props.roles, 'inventory')) {
				this.tab.push(
					this.configurations.PRODUCT_DEALS,
					this.configurations.CUSTOM_LINK,
				)
			}

			// =========================================================================================
			//  STYLIST
			// =========================================================================================
			if(AuthenticationHelper.isAuthorized(this.props.roles, 'stylist')) {
				this.tab.push(
					this.configurations.PRODUCT_DEALS,

				)
			}

			// =========================================================================================
			//  ANALYST
			// =========================================================================================
			if (AuthenticationHelper.isAuthorized(this.props.roles, 'analyst')) {
				this.tab.push(
					this.configurations.PRODUCT_DEALS,
					this.configurations.SUMMARY,
					this.configurations.CUSTOM_LINK,
					this.configurations.REPORT,
				)
			}

			// =========================================================================================
			//  MARKETING
			// =========================================================================================
			if (AuthenticationHelper.isAuthorized(this.props.roles, 'marketing')) {
				this.tab.push(
					this.configurations.SUMMARY,
					this.configurations.CUSTOM_LINK,
				)
			}

			// =========================================================================================
			//  PACKING
			// =========================================================================================
			if (AuthenticationHelper.isAuthorized(this.props.roles, 'packing')) {
				this.tab.push(
					this.configurations.PRODUCT_DEALS,
					this.configurations.CUSTOM_LINK,
				)
			}

			this.tab = sortBy(uniqBy(this.tab, t => t.title), ['title'])
		}

		logout = {
			title: 'Logout',
			icon: 'logout',
			onPress: () => {
				this.props.page.navigator.navigate('login')
				MeManager.logout()
			},
		}


		onNavigateToProductDeals = () => {
			this.props.page.navigator.navigate('deals')
		}


		onNavigateToSummary = () => {
			this.props.page.navigator.navigate('summary')
		}

		onNavigateToCustomLink = () => {
			this.props.page.navigator.navigate('custom')
		}

		onNavigateToReport = () => {
			this.props.page.navigator.navigate('report')
		}

		tabRenderer = ({title, icon, onPress, isActive}, i) => {
			return (
				<TouchableBit key={i} unflex row style={[Styles.nav, isActive && Styles.backgroundGrey]} onPress={onPress} >
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_2}
						weight="medium"
						style={ isActive ? Styles.text : Styles.textInactive }
					>
						{ title }
					</GeomanistBit>
					<IconBit
						name={ icon }
						size={ 20 }
						color={ isActive ? Colors.white.primary : Colors.white.palette(1, .4) }
						style={ Styles.icon }
					/>
				</TouchableBit>
			)
		}

		view() {
			return (
				<BoxBit unflex accessible style={[Styles.container,  this.props.style]}>
					<BoxBit unflex style={Styles.logo}>
						<IconBit size={32} name="yuna-gram" color={Colors.white.primary} />
					</BoxBit>
					<ScrollViewBit>
						{ this.tab.map(this.tabRenderer) }
					</ScrollViewBit>
					{ this.tabRenderer(this.logout, -1) }
				</BoxBit>
			);
		}
	}
)
