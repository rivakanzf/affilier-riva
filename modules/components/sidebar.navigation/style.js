import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		position: 'fixed',
		right: 0,
		top: 0,
		width: 240,
		height: Sizes.screen.height,
		backgroundColor: Colors.black.palette(2),
		zIndex: 3,
		transform: 'translateZ(1px) translateX(240px)',
	},

	nav: {
		alignItems: 'center',
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: 24,
		paddingRight: 24,
		justifyContent: 'flex-end',
	},

	backgroundGrey: {
		backgroundColor: Colors.grey.primary,
	},

	profile: {
	},

	logo: {
		paddingTop: 24,
		paddingBottom: 24,
		paddingLeft: 32,
		paddingRight: 24,
		alignItems: 'flex-end',
	},

	icon: {
		marginLeft: 12,
	},

	text: {
		color: Colors.white.primary,
	},
	textInactive: {
		color: Colors.white.palette(1, .4),
	},
})
