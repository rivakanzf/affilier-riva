import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class AddMoreLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				placeholder: PropTypes.string,
				style: PropTypes.style,
				children: PropTypes.node,
			}
		}

		static defaultProps = {
			add: false,
		}

		constructor(p) {
			super(p, {}, [
				'addButton',
			])
		}

		addButton() {
			this.setState({
				add: true,
			})
		}

		view() {
			return (
				<BoxBit unflex style={this.props.style}>
					<TouchableBit unflex style={this.state.add ? Styles.hide : Styles.icon} onPress={this.addButton}>
						<IconBit
							name="expand"
							color={Colors.black.palette(2, .8)}
						/>
					</TouchableBit>
					{ this.state.add && this.props.children }
				</BoxBit>
			)
		}
	}
)
