import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	icon: {
		width: 44,
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 9,
		paddingBottom: 9,
		borderWidth: 1,
		borderRadius: 2,
		borderColor: Colors.black.palette(2, .2),
		backgroundColor: Colors.solid.grey.palette(1),
	},

	hide: {
		display: 'none',
	},
})
