import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import {
	getReadableStatus,
} from '../select.status'

import Styles from './style';

import { memoize } from 'lodash';

const getStatusColorStyle = memoize(function(status) {
	switch (status) {
	// Common Statuses
	case 'PENDING':
		return Styles.statusBlue;
	case 'SUCCESS':
		return Styles.statusGreen
	case 'FAILED':
		return Styles.statusRed
	case 'EXCEPTION':
		return Styles.statusRed

	// Order Statuses
	case 'PENDING_PAYMENT':
		return Styles.statusBlue;
	case 'PAID_WITH_EXCEPTION':
		return Styles.statusOrange;
	case 'PAID':
		return Styles.statusOrange;
	case 'PROCESSING':
		return Styles.statusYellow
	case 'RESOLVED':
		return Styles.statusGreen
	case 'EXPIRED':
		return Styles.statusGrey

	// Inventory Status
	case 'AVAILABLE':
		return Styles.statusGreen
	case 'UNAVAILABLE':
		return Styles.statusOrange
	case 'DEFECT':
		return Styles.statusRed

	// Stylesheet Status
	case 'STYLING':
		return Styles.statusYellow
	case 'PUBLISHED':
		return Styles.statusOrange
	case 'APPROVED':
		return Styles.statusGreen

	// Stylesheet Inventory Status
	case 'BOOKED':
		return Styles.statusGreen
	case 'PACKED':
		return Styles.statusOrange
	case 'INVENTORY_EXCEPTION':
	case 'INVENTORY_INVALID':
		return Styles.statusRed
	case 'INVENTORY_LOCKED':
		return false
	case 'REQUESTING':
	case 'PURCHASING':
	case 'PURCHASED':
		return Styles.statusGreen
	case 'PURCHASE_CANCELLED':
		return Styles.statusRed
	case 'PURCHASE_COMPLETED':
		return Styles.statusOrange
	case 'PURCHASE_EXCEPTION':
		return Styles.statusRed
	case 'REQUEST_APPROVED':
		return Styles.statusGreen
	case 'REQUEST_REJECTED':
		return Styles.statusRed
	case 'REQUEST_COMPLETED':
		return Styles.statusOrange
	case 'REQUEST_EXCEPTION':
	case 'INVALID':
		return Styles.statusRed

	case 'DELIVERED':
		return Styles.statusGreen
	case 'DELIVERY_CONFIRMED':
		return Styles.statusGreen

	case 'ACTIVE':
		return Styles.statusGreen
	case 'INACTIVE':
		return Styles.statusOrange

	case 'KEPT':
		return Styles.statusGreen
	case 'EXCHANGED':
		return Styles.statusOrange
	case 'REFUNDED':
		return Styles.statusBlue

	default:
		return false
	}
})

const getTextColorStyle = memoize(function(status) {
	switch (status) {
	// Common Statuses
	case 'PENDING':
		return Styles.textBlue;
	case 'SUCCESS':
		return Styles.textGreen
	case 'FAILED':
		return Styles.textRed
	case 'EXCEPTION':
		return Styles.textRed

	// Order Statuses
	case 'PENDING_PAYMENT':
		return Styles.textBlue;
	case 'PAID_WITH_EXCEPTION':
		return Styles.textOrange;
	case 'PAID':
		return Styles.textOrange;
	case 'PROCESSING':
		return Styles.textYellow
	case 'RESOLVED':
		return Styles.textGreen
	case 'EXPIRED':
		return Styles.textGrey

	// Inventory Status
	case 'AVAILABLE':
		return Styles.textGreen
	case 'UNAVAILABLE':
		return Styles.textOrange
	case 'DEFECT':
		return Styles.textRed

	// Stylesheet Status
	case 'STYLING':
		return Styles.textYellow
	case 'PUBLISHED':
		return Styles.textOrange
	case 'APPROVED':
		return Styles.textGreen

	// Stylesheet Inventory Status
	case 'BOOKED':
		return Styles.textGreen
	case 'PACKED':
		return Styles.textOrange
	case 'INVENTORY_EXCEPTION':
	case 'INVENTORY_INVALID':
		return Styles.textRed
	case 'INVENTORY_LOCKED':
		return false
	case 'REQUESTING':
	case 'PURCHASING':
	case 'PURCHASED':
		return Styles.textGreen
	case 'PURCHASE_CANCELLED':
		return Styles.textRed
	case 'PURCHASE_COMPLETED':
		return Styles.textOrange
	case 'PURCHASE_EXCEPTION':
		return Styles.textRed
	case 'REQUEST_APPROVED':
		return Styles.textGreen
	case 'REQUEST_REJECTED':
		return Styles.textRed
	case 'REQUEST_COMPLETED':
		return Styles.textOrange
	case 'REQUEST_EXCEPTION':
	case 'INVALID':
		return Styles.textRed

	case 'DELIVERED':
		return Styles.textOrange
	case 'DELIVERY_CONFIRMED':
		return Styles.textGreen

	case 'ACTIVE':
		return Styles.textGreen
	case 'INACTIVE':
		return Styles.textOrange

	case 'KEPT':
		return Styles.textGreen
	case 'EXCHANGED':
		return Styles.textOrange
	case 'REFUNDED':
		return Styles.textBlue

	default:
		return false
	}
})

export default ConnectHelper(
	class BadgeStatusLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				tight: PropTypes.bool,
				unflex: PropTypes.bool,
				status: PropTypes.string,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			status: '-',
			unflex: true,
		}

		getStatusStyle(status) {
			return getStatusColorStyle(status)
		}

		getTextStyle(status) {
			return getTextColorStyle(status)
		}

		getStatusTitle(status) {
			return getReadableStatus(status)
		}

		textRenderer() {
			return (
				<GeomanistBit align="center" weight="medium" type={ GeomanistBit.TYPES.NOTE_2 } style={[Styles.text, this.getTextStyle(this.props.status)]}>{this.getStatusTitle(this.props.status)}</GeomanistBit>
			)
		}

		view() {
			return this.props.status ?
				this.props.onPress && (
					<TouchableBit row onPress={this.props.onPress} style={Styles.row}>
						<BoxBit row unflex={ this.props.unflex } centering style={[
							Styles.status,
							this.getStatusStyle(this.props.status),
							this.props.style,
						]}>
							{ this.textRenderer() }
						</BoxBit>
						{ this.props.tight ? false : (
							<BoxBit style={Styles.spacer} />
						) }
						<IconBit name="dropdown" size={20} color={Colors.black.palette(2, .8)} />
					</TouchableBit>
				) || (
					<BoxBit row unflex={ this.props.unflex } centering style={[
						Styles.status,
						this.getStatusStyle(this.props.status),
						this.props.style,
					]}>
						{ this.textRenderer() }
					</BoxBit>
				) : '...'
		}
	}
)
