import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	status: {
		// backgroundColor: Colors.black.palette(2),
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .9),
		borderRadius: 2,
		paddingTop: 2,
		paddingBottom: 2,
		paddingLeft: 3,
		paddingRight: 3,
		overflow: 'hidden',
	},

	spacer: {
		flexGrow: .2,
	},

	row: {
		alignItems: 'center',
	},

	text: {
		color: Colors.black.palette(2, .9),
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},

	textGreen: {
		color: Colors.green.palette(2),
	},

	textRed: {
		color: Colors.red.palette(7),
	},

	textYellow: {
		color: Colors.yellow.palette(3),
	},

	textOrange: {
		color: Colors.red.palette(6),
	},

	textBlue: {
		color: Colors.blue.palette(2),
	},

	textGrey: {
		color: Colors.solid.grey.palette(5),
	},

	statusGreen: {
		borderColor: Colors.green.palette(2),
	},

	statusRed: {
		borderColor: Colors.red.palette(7),
	},

	statusYellow: {
		borderColor: Colors.yellow.palette(3),
	},

	statusOrange: {
		borderColor: Colors.red.palette(6),
	},

	statusBlue: {
		borderColor: Colors.blue.palette(2),
	},

	statusGrey: {
		borderColor: Colors.solid.grey.palette(5),
	},
})
