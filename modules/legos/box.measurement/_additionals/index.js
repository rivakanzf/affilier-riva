import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SelectionBit from 'modules/bits/selection';
import TextInputBit from 'modules/bits/text.input';

import RowLego from 'modules/legos/row';
import UnitLego from 'modules/legos/unit';

import Styles from './style';


export default ConnectHelper(
	class AdditionalsPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				measurements: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					title: PropTypes.string,
					isRequired: PropTypes.bool,
				})),
				answers: PropTypes.object,
				onChange: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				measurements: p.measurements.filter(m => {
					return !!p.answers[m.id]
				}).map(m => {
					return {
						id: m.id,
						value: p.answers[m.id],
					}
				}),
			})

			this.measurements = p.answers
		}

		onChange = (id, e, value) => {
			this.measurements[id] = value

			Object.keys(this.measurements).forEach(key => {
				if (this.measurements[key] === undefined || this.measurements[key] === '') {
					delete this.measurements[key]
				}
			})

			this.props.onChange &&
			this.props.onChange(this.measurements)
		}

		onChangeKey = (index, id) => {
			const measurements = this.state.measurements.slice()
			measurements[index].id = id

			this.setState({
				measurements,
			})

			this.props.onChange &&
			this.props.onChange(this.state.measurements)
		}

		onAddKey = (id) => {
			this.setState({
				measurements: [...this.state.measurements, {
					id,
					value: '',
				}],
			})
		}

		onAddMeasurement = () => {
			// TODO
			this.props.utilities.menu.show({
				actions: this.props.measurements.map(m => {
					return {
						key: m.id,
						title: m.title,
						onPress: this.onAddKey.bind(this, m.id),
					}
				}).filter(m => {
					return this.state.measurements.findIndex(measurement => measurement.id === m.key) === -1
				}),
			})
		}

		measurementRenderer = ({ id, value }, i) => {
			return (
				<RowLego key={id}
					data={[{
						headerless: true,
						children: (
							<SelectionBit options={this.props.measurements.map(m => {
								return {
									key: m.id,
									title: m.title,
									selected: m.id === id,
								}
							}).filter(m => {
								return this.state.measurements.findIndex(measurement => measurement.id === m.key) === -1 || m.key === id
							})} onChange={this.onChangeKey.bind(this, i)} />
						),
					}, {
						headerless: true,
						children: (
							<BoxBit row>
								<TextInputBit unflex={false}
									onChange={this.onChange.bind(this, id)}
									defaultValue={value}
								/>
								<UnitLego title="cm" left />
							</BoxBit>
						),
					}]}
					style={Styles.row}
				/>
			)
		}

		view() {
			return (
				<BoxBit unflex>
					<BoxBit unflex type={BoxBit.TYPES.THIN} style={Styles.content}>
						{ this.state.measurements.map(this.measurementRenderer) }
					</BoxBit>
					<BoxBit unflex row style={Styles.footer}>
						{/* <ButtonBit
							title="Cancel"
							type={ButtonBit.TYPES.SHAPES.RECTANGLE}
							theme={ButtonBit.TYPES.THEMES.SECONDARY}
							size={ButtonBit.TYPES.SIZES.SMALL}
							weight="medium"
							onPress={this.onClose}
						/> */}
						<BoxBit />

						<ButtonBit
							title={'Add Measurement'}
							state={this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.measurements.length !== this.props.measurements.length && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED}
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							size={ButtonBit.TYPES.SIZES.SMALL}
							weight="medium"
							onPress={this.onAddMeasurement}
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)

