import React from 'react';
import ToggleableComponentModel from 'coeur/models/components/toggleable';
import ConnectHelper from 'coeur/helpers/connect';

import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

export default ConnectHelper(
	class TogglerPart extends ToggleableComponentModel {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				isSelected: PropTypes.bool,
				title: PropTypes.string,
			}
		}

		view() {
			return (
				<TouchableBit unflex
					onPress={this.onToggle}
					style={[Styles.tag, this.props.isSelected && Styles.tagPartialActive, this.state.isActive && Styles.tagActive]}
				>
					<GeomanistBit weight="medium" type={GeomanistBit.TYPES.PARAGRAPH_3} style={[Styles.title, this.props.isSelected && Styles.titlePartialActive, this.state.isActive && Styles.titleActive]} >
						{ this.props.title }
					</GeomanistBit>
				</TouchableBit>
			)
		}
	}
)
