import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';


import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class BreadcrumbLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onNavigateToHome: PropTypes.func,
				paths: PropTypes.array.isRequired,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			paths: [],
		}

		// static contexts = [
		// 	PageContext,
		// ]

		constructor(p) {
			super(p, {}, [
				'onNavigateToHome',
			])
		}

		onNavigateToHome() {
			// HACK
			// TODO: FIX the ROUTER!!!!!!!!!!!!!!
			window.location.hash = '/'
			window.location.reload()
		}

		view() {
			const paths = this.props.paths.slice();

			return (
				<BoxBit unflex row style={[Styles.centering, this.props.style]}>
					<TouchableBit unflex onPress={ this.onNavigateToHome }>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={[Styles.darkGrey80, Styles.padder]}
						>
							Home
						</GeomanistBit>
					</TouchableBit>
					{ paths.map((path, i) => {
						return (
							<TouchableBit key={i} unflex row centering onPress={ path.onPress }>
								<GeomanistBit
									type={GeomanistBit.TYPES.PARAGRAPH_3}
									style={Styles.darkGrey80}>
									/ { path.title }
								</GeomanistBit>
							</TouchableBit>
						)
					}) }
				</BoxBit>
			)
		}
	}
)
