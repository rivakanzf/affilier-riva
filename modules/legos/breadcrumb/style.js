import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	homeIcon: {
		marginTop: -1.5,
	},

	mainText: {
		color: Colors.black.palette(2, .6),
	},
})
