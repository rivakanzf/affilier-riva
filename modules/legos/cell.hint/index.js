import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import CellLego from '../cell';
import HintLego from '../hint';


export default ConnectHelper(
	class CellHintLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				hint: PropTypes.string,

				title: PropTypes.string.isRequired,
				content: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.number,
				]),
				description: PropTypes.string,

				blank: PropTypes.bool,
				children: PropTypes.node,
				onPress: PropTypes.func,

				style: PropTypes.style,
				headerStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			hint: '',
		}

		view() {
			return (
				<CellLego
					{ ...this.props.hint ? {
						header:( <HintLego title={this.props.hint} /> ),
					} : false}
					title={this.props.title}
					content={this.props.content}
					description={this.props.description}
					blank={this.props.blank}
					children={this.props.children}
					onPress={this.props.onPress}
					style={this.props.style}
					headerStyle={this.props.headerStyle}
				/>
			)
		}
	}
)
