import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class CellLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				header: PropTypes.node,
				headerless: PropTypes.bool,

				span: PropTypes.number,
				row: PropTypes.bool,
				subtle: PropTypes.bool,

				title: PropTypes.string,
				content: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.number,
				]),
				description: PropTypes.string,

				blank: PropTypes.bool,
				children: PropTypes.node,
				onPress: PropTypes.func,

				style: PropTypes.style,
				headerStyle: PropTypes.style,
				contentStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			title: '',
			headerless: false,
		}

		content() {
			return (
				<React.Fragment>
					{ !this.props.headerless ? (
						<BoxBit row unflex style={[Styles.header, this.props.headerStyle]}>
							{ this.props.title && (
								<GeomanistBit selectable type={ this.props.subtle ? GeomanistBit.TYPES.NOTE_1 : GeomanistBit.TYPES.PARAGRAPH_3 } weight={ this.props.subtle ? 'normal' : 'semibold'} style={ this.props.subtle ? Styles.subtle : Styles.title }>
									{ this.props.title }
								</GeomanistBit>
							) }
							{ this.props.blank && (
								<BoxBit unflex style={Styles.blank} />
							) }
							{ this.props.header }
						</BoxBit>
					) : false }
					{ this.props.children || (
						<BoxBit unflex style={this.props.contentStyle}>
							{ this.props.content && (
								<GeomanistBit selectable type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.content}>
									{ this.props.content }
								</GeomanistBit>
							) }
						</BoxBit>
					) }
					{ this.props.description && (
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.desc}>
							{ this.props.description }
						</GeomanistBit>
					) }
				</React.Fragment>
			)
		}

		view() {
			return this.props.onPress ? (
				<TouchableBit onPress={ this.props.onPress } row={ this.props.row } style={[Styles.container, this.props.span ? {
					flexGrow: this.props.span,
				} : false, this.props.style]}>
					{ this.content() }
				</TouchableBit>
			) : (
				<BoxBit row={ this.props.row } style={[Styles.container, this.props.span ? {
					flexGrow: this.props.span,
				} : false, this.props.style]}>
					{ this.content() }
				</BoxBit>
			)
		}
	}
)
