import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		// paddingTop: 16,
	},

	noBorder: {
		borderTopWidth: 0,
		textTransform: 'capitalize',
	},

	mainText: {
		color: Colors.black.palette(1, .7),
	},

	desc: {
		color: Colors.black.palette(2, .6),
		paddingTop: 8,
	},

	title: {
		color: Colors.black.palette(2),
	},

	subtle: {
		color: Colors.black.palette(2, .6),
		textTransform: 'uppercase',
	},

	header: {
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingBottom: 4,
		paddingTop: 4,
		marginTop: 16,
	},

	blank: {
		height: 20,
	},

	content: {
		color: Colors.black.palette(2),
	},
	capital: {
		textTransform: 'capitalize',
	},
})
