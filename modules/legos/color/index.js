import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import StyleSheet from 'coeur/libs/style.sheet'

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
// import GeomanistBit from 'modules/bits/geomanist';

import Styles from './style';


export default ConnectHelper(
	class ColorLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				colors: PropTypes.arrayOf(PropTypes.shape({
					hex: PropTypes.string,
					image: PropTypes.string,
				})),
				size: PropTypes.number,
				radius: PropTypes.number,
				noBorder: PropTypes.bool,
				border: PropTypes.number,
				// title: PropTypes.string,
				// color: PropTypes.string,
				// image: PropTypes.image,

				style: PropTypes.style,
			};
		}

		static defaultProps = {
			colors: [],
			size: 16,
			border: StyleSheet.hairlineWidth,
		}

		constructor(p) {
			super(p, {
				colors: [],
			})
		}

		colorRenderer = (half, color, index) => {

			if(!color) {
				return false
			}

			return color.image ? (
				<ImageBit key={index} resizeMode={ImageBit.TYPES.COVER} unflex source={color.image} style={{
					width: (this.props.size) - (this.props.border * 2),
					height: ((this.props.size) - (this.props.border * 2)) / (half ? 2 : 1),
				}} />
			) : (
				<BoxBit key={index} unflex style={{
					width: (this.props.size) - (this.props.border * 2),
					height: ((this.props.size) - (this.props.border * 2)) / (half ? 2 : 1),
					backgroundColor: color.hex,
				}} />
			)
		}

		view() {
			return (
				<BoxBit unflex
					style={[Styles.container, {
						borderRadius: this.props.radius,
						borderWidth: this.props.noBorder ? 0 : this.props.border,
						width: this.props.size,
						height: this.props.size,
					}, this.props.style]}
				>
					<BoxBit unflex style={this.props.colors.length > 1 ? Styles.content : undefined}>
						{ this.props.colors.length > 1 ? this.props.colors.map(this.colorRenderer.bind(this, true)) : this.colorRenderer(false, this.props.colors[0]) }
					</BoxBit>
				</BoxBit>
			);
		}
	}
)
