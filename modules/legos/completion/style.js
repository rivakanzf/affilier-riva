import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		marginLeft: -1,
		marginRight: -1,
	},

	box: {
		marginLeft: 1,
		marginRight: 1,
		backgroundColor: Colors.grey.palette(2),
		paddingTop: 12,
		paddingBottom: 8,
		paddingLeft: 4,
		paddingRight: 4,
		height: 50,
	},

	text: {
		marginTop: 2,
		color: Colors.black.palette(2, .6),
	},

	text_1: {
		color: Colors.red.palette(7),
	},

	text_2: {
		color: Colors.yellow.palette(3),
	},

	text_3: {
		color: Colors.green.palette(2),
	},
})
