import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import CouponService from 'app/services/coupon';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import BoxLego from 'modules/legos/box';
import ListLego from 'modules/legos/list';
import RowsLego from 'modules/legos/rows';

import Styles from './style';

export default ConnectHelper(
	class CouponItemLego extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				type: PropTypes.oneOf([
					'MATCHBOX',
					'SERVICE',
					'PRODUCT',
					'VARIANT',
					'USER',
				]).isRequired,
				title: PropTypes.string,
				style: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			return oP.type ? CouponService.getDetailOfType({
				id: oP.id,
				type: oP.type.toLowerCase(),
			}, state.me.token) : false
		}

		onPress = () => {
			// TODO
		}

		listRenderer = (list, i) => {
			return (
				<ListLego key={i} row style={Styles.list}>
					<BoxBit row style={{flexGrow: 1}}>
						<ImageBit
							source={list.image || undefined}
							style={Styles.image}
						/>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
							{list.title}
						</GeomanistBit>
						<TouchableBit unflex onPres={this.onPress}>
							<IconBit size={20} name="trash"/>
						</TouchableBit>
					</BoxBit>
				</ListLego>
			)
		}
		view() {
			return this.props.isLoading ? this.viewOnLoading() : (
				<BoxLego title={this.props.type} style={this.props.style}>
					{this.props.data.map(this.listRenderer)}
				</BoxLego>
			)
		}
	}
)
