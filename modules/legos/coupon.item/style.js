import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		
	},

	list: {
		backgroundColor: Colors.white.primary,
	},

	image: {
		width: 40,
		height: 40,
		marginRight: Sizes.margin.default,
	},
})
