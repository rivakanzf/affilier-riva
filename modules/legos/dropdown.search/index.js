import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';
import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import TextInputBit from 'modules/bits/text.input';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ShadowBit from 'modules/bits/shadow';
import TouchableBit from 'modules/bits/touchable';


import Styles from './style';
import { debounce } from 'lodash';

export default ConnectHelper(
	class DropdownSearchLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				small: PropTypes.bool,
				disabled: PropTypes.bool,
				title: PropTypes.string,
				children: PropTypes.node,

				onSearch: PropTypes.func,
				onSelect: PropTypes.func,

				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isDisplay: true,
				placeholder: p.placeholder,
				search: p.search,
			}, [
				'onToggle',
			])

			this.textInput = '';
			this.setTextInputRef = el => {
				this.textInput = el;
			}
			this.focusTextInput = () => {
				if(this.textInput) this.textInput.onForceFocus()
			}
			this._updateInput = debounce( this.onChange.bind(this), 370)
		}

		onToggle() {
			this.setState({
				isDisplay: !this.state.isDisplay,
			}, () => {
				this.state.isDisplay &&
				this.focusTextInput()
			})
		}

		onChange(e, val) {
			this.setState({
				search: val,
			})
		}

		onSelect = (val, changePlaceholder = true, valPlaceholder = '') => {
			this.setState(state => {
				return {
					placeholder: changePlaceholder ? valPlaceholder : state.placeholder,
				}
			}, () => {
				this.props.onSelect &&
				this.props.onSelect(val)
			})
			
		}

		view() {
			return (
				<BoxBit unflex style={this.props.style}>
					{ !!this.props.title && (
						<GeomanistBit weight="semibold"
							type={this.props.small
								? GeomanistBit.TYPES.PARAGRAPH_3
								: GeomanistBit.TYPES.HEADER_5
							} style={Styles.title}
						>
							{ this.props.title }
						</GeomanistBit>
					)}
					<TouchableBit unflex row style={[Styles.input, Styles.border]} onPress={this.onToggle}>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={Styles.darkGrey60}
						>
							{ this.state.placeholder || '-' }
						</GeomanistBit>
						<IconBit
							name="dropdown"
							size={20}
							color={Colors.black.palette(2, .8)}
						/>
					</TouchableBit>
					{ this.state.isDisplay && (
						<ShadowBit y={2} blur={8} spread={2} style={Styles.container}>
							<TouchableBit row style={Styles.input} onPress={this.onToggle}>
								<GeomanistBit
									type={GeomanistBit.TYPES.PARAGRAPH_3}
									style={Styles.darkGrey60}
								>
									{ this.state.placeholder || '-' }
								</GeomanistBit>
								<IconBit
									name="dropdown-up"
									size={20}
									color={Colors.black.palette(2, .8)}
								/>
							</TouchableBit>
							<BoxBit>
								<TextInputBit
									ref={ this.setTextInputRef }
									disabled={ this.props.disabled}
									onChange={ this._updateInput }
									defaultValue={this.state.search}
									style={Styles.padder}
								/>
								{ React.cloneElement(this.props.children, {
									search: this.state.search,
									onSelect: this.onSelect,
									onHide: this.onToggle,
								}) }
							</BoxBit>
						</ShadowBit>
					) }
				</BoxBit>
			)
		}
	}
)
