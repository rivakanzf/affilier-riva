import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';
import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ShadowBit from 'modules/bits/shadow';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

export default ConnectHelper(
	class DropdownLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				children: PropTypes.node,
				placeholder: PropTypes.string,
				onUpdate: PropTypes.func,

				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				placeholder: p.placeholder,
				display: false,
			}, [
				'onToggle',
				'onUpdate',
				'onChangePlaceholder',
			])
		}

		onToggle() {
			this.setState({
				display: this.state.display ? false : true,
			})
		}

		onChangePlaceholder( value ) {
			this.setState({
				placeholder: value,
			})
		}

		onUpdate(val) {
			this.props.onUpdate &&
			this.props.onUpdate(val)
		}

		view() {
			return (
				<BoxBit unflex style={this.props.style}>
					{ !!this.props.title && (
						<GeomanistBit weight="semibold" type={ this.props.small ? GeomanistBit.TYPES.PARAGRAPH_3 : GeomanistBit.TYPES.SUBHEADER_5} style={Styles.title}>
							{ this.props.title }
						</GeomanistBit>
					)}
					<TouchableBit unflex row style={[Styles.input, Styles.border]} onPress={this.onToggle}>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={Styles.darkGrey60}
						>
							{ this.state.placeholder || '-' }
						</GeomanistBit>
						<IconBit
							name="dropdown"
							size={20}
							color={Colors.black.palette(2, .8)}
						/>
					</TouchableBit>
					{ this.state.display && (
						<ShadowBit y={2} blur={8} spread={2} style={Styles.container}>
							<TouchableBit row style={Styles.input} onPress={this.onToggle}>
								<GeomanistBit
									type={GeomanistBit.TYPES.PARAGRAPH_3}
									style={Styles.darkGrey60}
								>
									{ this.props.placeholder || '-' }
								</GeomanistBit>
								<IconBit
									name="dropdown-up"
									size={20}
									color={Colors.black.palette(2, .8)}
								/>
							</TouchableBit>
							<BoxBit unflex>
								{ React.cloneElement(this.props.children, {
									onHide: this.onToggle,
									onChangePlaceholder: this.onChangePlaceholder,
									onUpdate: this.onUpdate,
								}) }
							</BoxBit>
						</ShadowBit>
					) }
				</BoxBit>
			)
		}
	}
)
