import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		position: 'absolute',
		zIndex: 2,
		backgroundColor: Colors.white.primary,
		width: '100%',
		borderRadius: 2,
		overflow: 'hidden',
	},

	title: {
		color: Colors.black.palette(2),
		marginTop: 4,
		marginBottom: 4,
	},

	content: {
		paddingLeft: 16,
		paddingRight: 16,
	},

	padder: {
		marginLeft: 16,
		marginRight: 16,
		marginBottom: 8,
	},
	padder16: {
		marginLeft: Sizes.margin.default,
		marginRight: Sizes.margin.default,
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},
	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},

	white: {
		color: Colors.white.primary,
	},

	client: {
		paddingTop: 11,
		paddingBottom: 11,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 2,
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingLeft: Sizes.margin.default,
		paddingRight: 8,
	},

	search: {
		paddingTop: 9,
		paddingBottom: 16,
		borderRadius: 2,
		overflow: 'hidden',
		position: 'absolute',
		zIndex: 1,
		backgroundColor: Colors.white.primary,
		width: '100%',
	},

	space: {
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: 8,
	},

	paddingRight: {
		paddingRight: 12,
	},

	isSearching: {
		paddingTop: 8,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		color: Colors.black.palette(2, .6),
	},

	listContainer: {
		maxHeight: 132,
		marginTop: 8,
	},

	list: {
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		justifyContent: 'space-between',
	},
	select: {
		paddingTop: 3,
		paddingBottom: 3,
		paddingLeft: 8,
		paddingRight: 8,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.white.primary,
		borderRadius: 2,
	},

	isSelected: {
		backgroundColor: Colors.primary,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	listContent: {
		backgroundColor: Colors.solid.grey.palette(4),
		paddingTop: 12,
		paddingBottom: 11,
		paddingLeft: 8,
		paddingRight: 8,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		alignItems: 'center',
	},
	image: {
		height: 36,
		width: 36,
	},
	qty: {
		width: 44,
		height: 32,
		paddingLeft: 8,
		paddingRight: 4,
	},
	desc: {
		paddingLeft: 8,
	},

	footer: {
		justifyContent: 'space-between',
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	input: {
		justifyContent: 'space-between',
		paddingLeft: 16,
		paddingRight: 8,
		paddingBottom: 11,
		paddingTop: 11,
	},

	border: {
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
	},
})
