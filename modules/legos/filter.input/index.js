import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import Defaults from 'coeur/constants/default';
// import FormatHelper from 'coeur/helpers/format';
// import Colors from 'coeur/constants/color';

// import BoxBit from 'modules/bits/box';
// import CollapsibleBit from 'modules/bits/collapsible';
// import IconBit from 'modules/bits/icon';
import GeomanistBit from 'modules/bits/geomanist';
import InputValidatedBit from 'modules/bits/input.validated';
import BoxBit from 'modules/bits/box';
// import RadioBit from 'modules/bits/radio';


import Styles from './style';


export default ConnectHelper(
	class FilterInputLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.arrayOf(PropTypes.shape({
					key: PropTypes.any,
					title: PropTypes.string,
					value: PropTypes.id,
					disabled: PropTypes.bool,
				})),
				values: PropTypes.arrayOf(PropTypes.any),
				onChange: PropTypes.func,
			}
		}

		static defaultProps = {
			data: [],
			values: [],
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.data !== nS.prevData) {
				return {
					nextIdById: nP.data.reduce((sum, data, i) => {
						if(data.id) {
							const nextEligibleData = nP.data.slice(i + 1).find(c => c.id && !c.disabled)

							return {
								...sum,
								[data.id]: nextEligibleData && nextEligibleData.id,
							}
						} else {
							return sum
						}
					}, {}),
					prevData: nP.data,
					data: nP.data,
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				data: p.data,
				prevData: [],
				nextIdById: {},
			})

			this.reference = {}
			this.values = p.data.reduce((sum, curr) => {
				return curr.id ? {
					...sum,
					[curr.id] : curr.value,
				} : sum
			}, {})
		}

		bindReference(id, input) {
			this.reference[id] = input
		}

		onChange = (id, value) => {
			this.values = {
				...this.values,
				[id]: value,
			}
		}

		focus() {
			if(this.reference[this.props.config[0].id]) {
				this.reference[this.props.config[0].id].focus()
			} else if(this.reference[this.state.nextIdById[this.props.config[0].id]]) {
				this.reference[this.state.nextIdById[this.props.config[0].id]].focus()
			}
		}

		onSubmitEditing(id, value, type) {
			if(this.state.nextIdById[id]) {
				this.reference[this.state.nextIdById[id]].focus();
			} else if(Defaults.PLATFORM === 'web' && type === 'Enter' || Defaults.PLATFORM !== 'web') {
				this.reference[id].blur();

				this.props.onChange &&
				this.props.onChange(Object.values(this.values))
			}
		}

		inputRenderer = ({ id, title, value }, index) => {
			return (
				<BoxBit unflex key={index} style={Styles.item}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
						{title}
					</GeomanistBit>
					<InputValidatedBit
						value={ value }
						inputRef={ this.bindReference.bind(this, id) }
						prefix={ this.prefixRenderer }
						type={InputValidatedBit.TYPES.CURRENCY}
						onSubmitEditing={ this.onSubmitEditing.bind(this, id) }
						onChange={ this.onChange.bind(this, id) }
					/>
				</BoxBit>
			);
		}

		view() {
			return (this.state.data.map(this.inputRenderer))
		}
	}
)
