import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	item: {
		marginBottom: 4,
	},
	title: {
		marginLeft: 8,
		color: Colors.black.palette(2, .8),
	},
})
