import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		justifyContent: 'space-between',
		// alignItems: 'center',
	},

	grid: {
		flexWrap: 'wrap',
		// alignItems: 'center',
		justifyContent: 'space-between',
	},

	space: {
		justifyContent: 'space-between',
	},

	boxDiscount: {
		height: 16,
		paddingLeft: 4,
		paddingRight: 4,
		backgroundColor: Colors.red.palette(8),
		alignSelf: 'flex-end',
		marginTop: 8,
		marginRight: 8,
	},

	white: {
		color: Colors.white.primary,
	},

	overlay: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		backgroundColor: Colors.black.palette(2, .3),
	},

	count: {
		color: Colors.white.primary,
	},
})
