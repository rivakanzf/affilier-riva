import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		width: Sizes.screen.width,
		transform: 'translateZ(1px) translateY(-70px)',
		transition: '.3s all ease-in-out',
	},

	content: {
		zIndex: 2,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		alignItems: 'center',
	},

	visible: {
		transform: 'translateZ(1px) translateY(0)',
		transition: '.3s all ease-in-out',
	},

	darkGrey80: {
		color: Colors.black.palette(2, .8),
	},

	title: {
		textTransform: 'capitalize',
		color: Colors.black.palette(2),
		marginRight: 16,
	},

	initial: {
		marginRight: 12,
		height: 48,
		width: 48,
	},

})
