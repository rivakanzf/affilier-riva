import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	line: {
		height: 48,
		borderRightWidth: 1,
		borderColor: Colors.black.primary,
		transform: 'skewX(-25deg)',
		marginLeft: 24,
		marginRight: 24,
	},

	tabs: {
		marginLeft: -Sizes.margin.thick,
		marginRight: -Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		backgroundColor: Colors.solid.grey.palette(1),
	},

	title: {
		paddingLeft: 16,
		paddingRight: 16,
		paddingTop: 12,
		paddingBottom: 10,
		color: Colors.black.palette(2, .8),
		textTransform: 'capitalize',
	},

	tabActive: {
		width: 24,
		borderBottomWidth: 1,
		borderColor: Colors.black.primary,
		position: 'absolute',
		bottom: -.5,
		alignSelf: 'center',
	},

	header: {
		flexGrow: 1,
	},

	search: {
		paddingLeft: 12,
		marginLeft: 24,
		marginRight: 24,
		flexGrow: 2,
	},

})
