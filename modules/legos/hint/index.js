import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class HintLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string.isRequired,
				size: PropTypes.number,
				color: PropTypes.string,
				style: PropTypes.style,
				containerStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			hint: false,
			color: Colors.black.palette(2, .6),
		}

		constructor(p) {
			super(p, {}, [
				'toggleHint',
			])
		}

		toggleHint() {
			this.setState((state) => {
				return {
					hint: state.hint ? false : true,
				}
			})
		}

		view() {
			return (
				<TouchableBit
					unflex
					onPress={this.toggleHint}
					style={this.props.containerStyle}
				>
					<IconBit
						name="hint-fill"
						size={ this.props.size }
						color={ this.state.hint === true ? Colors.white.primary : this.props.color }
						style={ Styles.icon }
					/>
					{ this.state.hint && (
						<BoxBit unflex row style={[Styles.hint, this.props.style]}>
							<GeomanistBit
								type={GeomanistBit.TYPES.NOTE_1}
								style={Styles.white}
							>
								{ this.props.title }
							</GeomanistBit>
						</BoxBit>
					)}
				</TouchableBit>
			)
		}
	}
)
