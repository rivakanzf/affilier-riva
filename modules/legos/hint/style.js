import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	hint: {
		position: 'absolute',
		width: 256,
		right: 0,
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: 8,
		paddingRight: 32,
		backgroundColor: Colors.black.palette(2, .8),
		borderRadius: 8,
		zIndex: 1,
		transition: '.3s all ease-in-out',
	},

	white: {
		color: Colors.white.primary,
	},

	icon: {
		zIndex: 2,
	},
})
