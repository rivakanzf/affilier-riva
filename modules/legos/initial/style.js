import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		width: 52,
		height: 52,
		borderRadius: 26,
		overflow: 'hidden',
	},
	image: {
		width: '100%',
		height: '100%',
	},
	title: {
		color: Colors.white.primary,
		textShadow: `${ Colors.black.palette(2, .3) } 0 1px 1px`,
	},
})
