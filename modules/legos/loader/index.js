import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
// import IconBit from 'modules/bits/icon';
// import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class LoaderLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				simple: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		view() {
			return (
				<BoxBit unflex row centering style={[Styles.container, this.props.style]}>
					<LoaderBit simple={ this.props.simple } />
				</BoxBit>
			)
		}
	}
)
