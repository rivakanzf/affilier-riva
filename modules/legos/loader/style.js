import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		backgroundColor: Colors.white.primary,
		paddingTop: Sizes.margin.thick,
		paddingBottom: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		borderRadius: 2,
	},
})
