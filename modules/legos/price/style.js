import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	retail: {
		textDecoration: 'line-through',
	},

	discounted: {
		color: Colors.red.palette(7),
	},
})
