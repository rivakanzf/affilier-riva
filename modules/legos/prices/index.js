import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import CellLego from 'modules/legos/cell';
import PromoLego from 'modules/legos/promo';

import Styles from './style';


export default ConnectHelper(
	class PricesLego
		extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				coupons: PropTypes.oneOfType([
					PropTypes.array,
					PropTypes.node,
				]),
				campaigns: PropTypes.array,
				prices: PropTypes.object,
				style: PropTypes.style,

				// shipments: PropTypes.array,
			}
		}

		static defaultProps = {
			campaigns: [],
			prices: {},
		}

		priceRenderer = (title, amount, bold, style) => {
			return (
				<BoxBit row unflex style={[ Styles.row, style ]}>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="semibold" style={ Styles.title }>
						{ title }
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align="right" weight={ bold ? 'medium' : 'normal' } style={ Styles.price }>
						IDR { FormatHelper.currency(amount) }
					</GeomanistBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit row unflex style={[ Styles.container, this.props.style ]}>
					<CellLego title="Coupons &amp; Campaigns" headerStyle={ Styles.header } style={ Styles.column }>
						<PromoLego coupons={ this.props.coupons } campaigns={ this.props.campaigns } />
					</CellLego>
					<BoxBit>
						{ this.priceRenderer('Subtotal', this.props.prices.subtotal) }
						{ this.priceRenderer('Styling Fee', this.props.prices.handling) }
						{ this.priceRenderer('Shipping', this.props.prices.shipping) }
						{ this.props.prices.roundup ? this.priceRenderer('Round Up', this.props.prices.roundup) : false }
						{ this.priceRenderer('Discount', -this.props.prices.discount) }
						{ this.priceRenderer('Cashback', this.props.prices.cashback) }
						{ this.priceRenderer('Wallet', -this.props.prices.wallet) }
						{ this.priceRenderer('Point', -this.props.prices.point)}
						{ this.priceRenderer('TOTAL', this.props.prices.total, true, Styles.total) }
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
