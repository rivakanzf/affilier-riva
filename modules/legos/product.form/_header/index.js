import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';
import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';
import ImageBit from 'modules/bits/image';
import IconBit from 'modules/bits/icon';
import GeomanistBit from 'modules/bits/geomanist';

import TableLego from 'modules/legos/table';
import Styles from './style';

import { isEmpty, isEqual } from 'lodash';

export default ConnectHelper(
	class HeaderPart extends QueryStatefulModel {
		static TYPES = {
			MATCHBOX: 'MATCHBOX',
			INVENTORY: 'INVENTORY',
			VARIANT: 'VARIANT',
			VOUCHER: 'VOUCHER',
			SERVICE: 'SERVICE',
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				title: PropTypes.string,
				type: PropTypes.string,

				onClear: PropTypes.func,
				onChange : PropTypes.func,
			}
		}

		static propsToQuery(state, oP) {
			if(oP.id) {
				return oP.type === 'SERVICE' ? [` query {
					product: serviceById(id: ${oP.id}) {
						price
						assets: serviceAssets {
							url
						}
					}
				}`, {}, state.me.token] : oP.type === 'MATCHBOX'
					&&  [`query {
						product: matchboxById(id: ${oP.id}) {
							price
							assets: matchboxAssets {
								url
							}
						}
					}`, {}, state.me.token]
					||  [`query {
						product: variantById(id: ${oP.id}) {
							price
							assets: variantAssets {
								url
							}
						}
					}`, {}, state.me.token]
			} else {
				return false
			}
		}

		static getDerivedStateFromProps(nP) {
			if(!nP.type) {
				return null
			} else if(isEmpty(nP.data)) {
				return null
			} else if(nP.isLoading) {
				return null
			}

			return {
				title: nP.title,
				type: nP.type,
				image: nP.data.product.assets,
				price: nP.data.product.price,
			}
		}
		
		constructor(p) {
			super(p, {
				title: '',
				type: '',
				image: {},
				price: 0,
				qty: 1,
			})
		}

		componentDidUpdate(pP, pS) {
			if( !isEqual(pS.qty, this.state.qty)
				|| !isEqual(pS.price, this.state.price)
			) {
				this.props.onChange &&
				this.props.onChange({
					id: this.props.id,
					title: this.state.title,
					type: this.state.type,
					image: this.state.image,
					price: this.state.price,
					qty: this.state.qty,
				})
			}
		}

		onChangeQty = (e, val) => {
			if(val > 0) {
				this.setState({
					qty: val,
				})
			}
		}
		getSource(src) {
			if(Array.isArray(src)) {
				return src[0]
			}

			return src
		}
		
		view() {
			return (
				<BoxBit unflex row style={Styles.header}>
					<TableLego
						headers={[{
							title: 'Product',
							width: 3,
						}, {
							title: 'Qty',
							width: .5,
							align: 'right',
						}, {
							title: 'Subtotal',
							width: 1,
							align: 'right',
						}, {
							title: '',
							width: .5,
						}]}
						rows={[{
							data: [{
								children: (
									<BoxBit row unflex style={Styles.flex}>
										<ImageBit
											broken={!this.state.image}
											source={this.getSource(this.state.image) || undefined}
											style={Styles.image}
										/>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
											{ this.state.title}
										</GeomanistBit>
									</BoxBit>
								),
							}, {
								children: (
									<TextInputBit
										type={TextInputBit.TYPES.NUMBER}
										defaultValue={1}
										onChange={ this.onChangeQty }
										inputStyle={Styles.input}
									/>
								),
							}, {
								title: `IDR ${FormatHelper.currencyFormat(this.state.price * this.state.qty)}`,
							}, {
								children: this.props.onClear && (
									<TouchableBit onPress={ this.props.onClear } style={Styles.clearIcon}>
										<IconBit
											name="close"
											size={20}
										/>
									</TouchableBit>
								),
							}],
							style: Styles.noBorder,
						}]}
						style={Styles.table}
					/>
				</BoxBit>
			)
		}
	}
)
