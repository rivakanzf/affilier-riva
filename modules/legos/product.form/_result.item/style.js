import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'utils/constants/color'

export default StyleSheet.create({
	list: {
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
	},
	isHovered: {
		backgroundColor: Colors.primary,
		justifyContent: 'space-between',
	},
	white: {
		color: Colors.white.primary,
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},
	capital: {
		textTransform: 'capitalize',
	},
	select: {
		paddingTop: 3,
		paddingBottom: 3,
		paddingLeft: 8,
		paddingRight: 8,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.white.primary,
		borderRadius: 2,
	},
	searchBar: {
		maxHeight: 168,
		marginTop: 8,
	},

	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},

	desc: {
		height: 24,
		alignItems: 'center',
	},

	placeholder: {
		justifyContent: 'space-between',
		paddingLeft: 16,
		paddingRight: 16,
		paddingBottom: 10,
	},

	padder: {
		marginLeft: 16,
		marginRight: 16,
	},

	padderRight: {
		paddingRight: 4,
	},

	loader: {
		paddingLeft: 16,
		paddingRight: 16,
		alignItems: 'center',
		marginBottom: 16,
		height: 20,
	},

})
