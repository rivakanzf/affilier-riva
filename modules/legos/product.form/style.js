import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	noMargin: {
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
	},

	content: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		marginBottom: 24,
		backgroundColor: Colors.solid.grey.palette(1),
	},

	padderTop: {
		paddingTop: Sizes.margin.default,
	},

	empty: {
		height: 88,
	},
	container: {
		height: 480,
	},

	
	title: {
		paddingBottom: 8,
		paddingTop: 8,
	},
	attachment: {
		marginTop: 24,
		color: Colors.default.palette(3),
	},
	type: {
		paddingBottom: Sizes.margin.default,
	},

	padder: {
		marginTop: Sizes.margin.default,
	},

	text: {
		color: Colors.black.palette(2, .6),
	},
	clearIcon: {
		justifyContent: 'center'
	},
	image: {
		height: 36,
		width: 36,
		marginRight: 8,
	},

	header: {
		alignItems: 'center',
	},

	button: {
		marginTop: -6,
		marginBottom: -6,
		marginLeft: Sizes.margin.default,
	},

})
