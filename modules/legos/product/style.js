import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({


	product: {
		justifyContent: 'flex-start',
		alignItems: 'center',
	},

	image: {
		width: 60,
		height: 60,
		marginRight: 16,
	},

	box: {
		overflow: 'hidden',
	},

	title: {
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},

	description: {
		color: Colors.black.palette(2, .6),
	},

})
