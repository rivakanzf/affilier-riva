import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	wrapper: {
		flexWrap: 'wrap',
		alignItems: 'flex-start',
	},

	promo: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingTop: 7,
		paddingBottom: 6,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderStyle: 'solid',
		borderRadius: 2,
		marginRight: 8,
		marginBottom: 8,
	},

	amount: {
		color: Colors.black.palette(2, .6),
	},

})
