import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class QuickViewCellLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				unflex: PropTypes.bool,
				content: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.node,
				]),
				description: PropTypes.string,
				children: PropTypes.node,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: true,
		}

		view() {
			return (
				<BoxBit unflex={ this.props.unflex } style={[ Styles.container, this.props.style ]}>
					<GeomanistBit type={ GeomanistBit.TYPES.SUBHEADER_2 } weight="medium" style={ Styles.title }>
						{ this.props.title }
					</GeomanistBit>
					{/* eslint-disable-next-line no-nested-ternary */}
					{ typeof this.props.content === 'string' ? this.props.onPress ? (
						<TouchableBit row unflex style={ Styles.center } onPress={ this.props.onPress }>
							<GeomanistBit type={ GeomanistBit.TYPES.HEADER_4 }>
								{ this.props.content }
							</GeomanistBit>
							<IconBit name="arrow-right-tailed" style={ Styles.icon } />
						</TouchableBit>
					) : (
						<GeomanistBit type={ GeomanistBit.TYPES.HEADER_4 }>
							{ this.props.content }
						</GeomanistBit>
					) : this.props.content }
					{ this.props.description ? (
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
							{ this.props.description }
						</GeomanistBit>
					) : false }
					{ this.props.children }
				</BoxBit>
			)
		}
	}
)
