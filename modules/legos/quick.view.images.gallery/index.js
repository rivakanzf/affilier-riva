import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(

	class QuickViewImagesGalleryLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				images: PropTypes.arrayOf(PropTypes.image),
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			images: [],
		}

		constructor(p) {
			super(p, {
				selectedIndex: 0,
			})
		}

		onSelectIndex = i => {
			this.setState({
				selectedIndex: i,
			})
		}

		navRenderer = (img, i) => {
			return (
				<TouchableBit key={ i } unflex onPress={ this.onSelectIndex.bind(this, i) } style={ i === this.state.selectedIndex ? Styles.active : Styles.inactive } />
			)
		}

		view() {
			return (
				<React.Fragment>
					<BoxBit style={Styles.contentContainer}>
						<BoxImageBit unflex key={ this.state.selectedIndex } transform={{crop: 'fit'}} source={ this.props.images[this.state.selectedIndex] } style={[ Styles.container, this.props.style ]} />
					</BoxBit>
					<BoxBit unflex style={ Styles.shadowLeft } />
					<BoxBit unflex style={ Styles.shadowRight } />
					{ this.props.images.length === 1 ? false : (
						<BoxBit unflex centering row style={ Styles.nav }>
							{ this.props.images.map(this.navRenderer) }
						</BoxBit>
					) }
				</React.Fragment>
			)
		}
	}
);
