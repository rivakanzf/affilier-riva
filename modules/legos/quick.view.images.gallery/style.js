import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'
// import Styles from 'coeur/constants/style'

export default StyleSheet.create({

	contentContainer: {
		overflow: 'hidden',
	},

	container: {
		backgroundColor: Colors.grey.palette(4),
		width: '100%',
		paddingBottom: '133.333%',
		margin: 'auto 0',
	},

	nav: {
		position: 'absolute',
		left: 0,
		right: 0,
		bottom: -24,
	},

	active: {
		borderWidth: 0,
		borderColor: Colors.white.primary,
		width: 10,
		height: 10,
		borderRadius: 6,
		backgroundColor: Colors.white.primary,
		margin: 3,
	},

	inactive: {
		borderWidth: 1,
		borderColor: Colors.white.primary,
		width: 8,
		height: 8,
		borderRadius: 6,
		margin: 4,
		// backgroundColor: Colors.grey.palette(3),
	},

	shadowLeft: {
		position: 'absolute',
		zIndex: 1,
		top: 0,
		left: 0,
		bottom: 0,
		width: 10,
		backgroundImage: 'linear-gradient(to right, rgba(0, 0, 0, .02) , rgba(0, 0, 0, 0));',
	},

	shadowRight: {
		position: 'absolute',
		zIndex: 1,
		top: 0,
		right: 0,
		bottom: 0,
		width: 10,
		backgroundImage: 'linear-gradient(to left, rgba(0, 0, 0, .02) , rgba(0, 0, 0, 0));',
	},

})
