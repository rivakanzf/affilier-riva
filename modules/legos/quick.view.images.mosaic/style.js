import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({

	container: {
		overflow: 'hidden',
		backgroundColor: Colors.grey.palette(4),
	},

	wrapper: {
		marginLeft: -1,
		marginRight: -1,
		marginTop: -1,
		marginBottom: -1,
	},

	box: {
		borderWidth: 1,
		borderColor: Colors.white.primary,
	},

	image: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
	},

	overlay: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		backgroundColor: Colors.black.palette(2, .4),
	},

	count: {
		color: Colors.white.primary,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	shadowLeft: {
		position: 'absolute',
		zIndex: 1,
		top: 0,
		left: 0,
		bottom: 0,
		width: 10,
		backgroundImage: 'linear-gradient(to right, rgba(0, 0, 0, .02) , rgba(0, 0, 0, 0));',
	},

	shadowRight: {
		position: 'absolute',
		zIndex: 1,
		top: 0,
		right: 0,
		bottom: 0,
		width: 10,
		backgroundImage: 'linear-gradient(to left, rgba(0, 0, 0, .02) , rgba(0, 0, 0, 0));',
	},
})
