import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
// import ImageBit from 'modules/bits/image';
// import GeomanistBit from 'modules/bits/geomanist';

import Styles from './style';


export default ConnectHelper(
	class QuickViewOverviewLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.string,
				subheader: PropTypes.oneOfType([
					PropTypes.string,
					PropTypes.node,
				]),
				title: PropTypes.string,
				mark: PropTypes.string,
				price: PropTypes.number,
				retailPrice: PropTypes.number,
				description: PropTypes.string,
				style: PropTypes.style,
			};
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.id }>
						{ this.props.id }
					</GeomanistBit>
					<BoxBit style={ Styles.center }>
						{ typeof this.props.subheader === 'string' ? (
							<GeomanistBit type={ GeomanistBit.TYPES.PRIMARY_1 } style={ Styles.note }>
								{ this.props.subheader }
							</GeomanistBit>
						) : this.props.subheader }
						<GeomanistBit type={ GeomanistBit.TYPES.HEADER_2 } style={ Styles.title }>
							{ this.props.title }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PRIMARY_3 } weight="normal" style={ Styles.mark }>
							{ this.props.mark }
						</GeomanistBit>
						{ /* eslint-disable-next-line no-nested-ternary */ }
						{ this.props.price ? this.props.price !== this.props.retailPrice ? (
							<React.Fragment>
								<GeomanistBit type={ GeomanistBit.TYPES.HEADER_5 } style={ Styles.discount} >
									IDR { FormatHelper.currencyFormat(this.props.retailPrice) }
								</GeomanistBit>
								<GeomanistBit type={ GeomanistBit.TYPES.HEADER_5 } style={ Styles.discounted } >
									IDR { FormatHelper.currencyFormat(this.props.price) }
								</GeomanistBit>
							</React.Fragment>
						) : (
							<GeomanistBit type={ GeomanistBit.TYPES.HEADER_5 } style={ Styles.discounted } >
								IDR { FormatHelper.currencyFormat(this.props.price) }
							</GeomanistBit>
						) : false }
						<BoxBit unflex style={ Styles.divider } />
						{ this.props.description ? (
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } italic style={ Styles.description }>
								“{ this.props.description }”
							</GeomanistBit>
						) : false }
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
