import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	center: {
		justifyContent: 'center',
	},

	mark: {
		color: Colors.black.palette(2, .4),
		marginBottom: 12,
	},

	divider: {
		marginTop: 24,
		marginBottom: Sizes.margin.default,
		height: 2,
		width: Sizes.margin.thick,
		backgroundColor: Colors.black.palette(2, .7),
	},

	description: {
		marginTop: Sizes.margin.default,
		color: Colors.black.palette(2, .6),
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	discount: {
		paddingRight: Sizes.margin.default,
		textDecoration: 'line-through',
	},

	discounted: {
		color: Colors.red.palette(4),
	},

	title: {
		marginTop: 8,
		marginBottom: 8,
		wordBreak: 'break-word',
	},

	id: {
		marginTop: Sizes.margin.default,
	},
})
