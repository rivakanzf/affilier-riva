import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	row: {
		marginTop: 4,
		paddingBottom: 4,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
	},

	data: {
		paddingTop: 4,
		paddingRight: 4,
		paddingBottom: 4,
		width: 1,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	primary: {
		color: Colors.primary,
	},

	title: {
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		width: '100%',
	},

	status: {
		borderWidth: 0,
		alignSelf: 'flex-start',
		marginTop: 4,
		marginLeft: 4,
		// marginRight: 4,
		// backgroundColor: Colors.yellow.palette(2),
		// paddingLeft: 4,
		// paddingRight: 4,
	},

})
