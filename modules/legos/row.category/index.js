import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import CategoryService from 'app/services/category'

import RowLego from 'modules/legos/row';
import SelectCategoryLego from 'modules/legos/select.category';

import Styles from './style';


export default ConnectHelper(
	class RowCategoryLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				disabled: PropTypes.bool,
				categoryId: PropTypes.number,
				isLoading: PropTypes.bool,
				onChange: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				parentId: null,
				categoryId: p.categoryId,
				isLoadingCategory: false,
			})
		}

		componentDidMount() {
			// TODO: get parent
			if(this.state.categoryId) {
				// eslint-disable-next-line react/no-did-mount-set-state
				this.setState({
					isLoadingCategory: true,
				}, () => {
					CategoryService.getCategory(this.state.categoryId, this.props.token).then(category => {
						this.setState({
							parentId: category.category_id,
							isLoadingCategory: false,
						})
					}).catch(err => {
						this.warn(err)
						this.setState({
							isLoadingCategory: false,
						})
					})
				})
			}
		}

		onChange = (id, name) => {
			this.setState({
				categoryId: id,
			})

			this.props.onChange &&
			this.props.onChange(id, name)
		}

		onChangeParent = id => {
			this.setState({
				parentId: id,
			})

			this.props.onChangeParent &&
			this.props.onChangeParent(id)
		}

		view() {
			return (
				<RowLego
					key={ `${this.state.parentId}${this.state.categoryId}` }
					data={[{
						title: 'Category',
						children: (
							<SelectCategoryLego isRequired
								cache={false}
								disabled={ this.props.disabled }
								categoryId={ this.state.parentId }
								isLoading={ this.props.isLoading || this.state.isLoadingCategory }
								onChange={ this.onChangeParent }
							/>
						),
					}, {
						title: 'Type',
						children: (
							<SelectCategoryLego isRequired
								cache={false}
								disabled={!this.state.parentId || this.props.disabled}
								parentCategoryId={ this.state.parentId }
								categoryId={this.state.categoryId}
								isLoading={this.props.isLoading}
								onChange={this.onChange}
							/>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
