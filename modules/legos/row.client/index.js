import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import NavTextBit from 'modules/bits/nav.text';

import RowLego from 'modules/legos/row';

import Styles from './style';


export default ConnectHelper(
	class RowClientLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				userId: PropTypes.id,
				userName: PropTypes.string,
				userEmail: PropTypes.string,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		onNavigateToOrders() {
			this.props.page.navigator.navigate('order', {
				name: this.props.userName,
			})
		}

		param = {
			search: this.props.userEmail,
		}

		view() {
			return (
				<RowLego
					data={[{
						title: 'Client',
						children: (
							<NavTextBit title={ this.props.userName } link="View profile" href={`client/${this.props.userId}`} />
						),
					}, {
						title: 'Email Address',
						children: (
							<NavTextBit title={ this.props.userEmail } link="View other orders" href={'order'} param={this.param} />
						),
					}]}
					style={[Styles.padder, this.props.style]}
				/>
			)
		}
	}
)
