import React from 'react';
import ConnectedStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UserService from 'app/services/user.new'

import LoaderLego from 'modules/legos/loader';
import RowImageLego from 'modules/legos/row.image';

import Styles from './style'


export default ConnectHelper(
	class RowImageUserLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				addable: PropTypes.bool,
				id: PropTypes.id,
				assetIds: PropTypes.arrayOf(PropTypes.id),
				onUpdate: PropTypes.func,
				max_attachment: PropTypes.number,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return oP.assetIds && oP.assetIds.length ? UserService.getUserAssets(oP.id, oP.assetIds.join(','), state.me.token).catch(() => []) : Promise.resolve([])
		}

		static defaultProps = {
			addable: true,
		}

		uploader = (image, index) => {
			return UserService.upload(this.props.id, {
				image,
				order: index,
			}, this.props.token)
		}

		remover = id => {
			return UserService.removeAsset(this.props.id, id, this.props.token)
		}

		viewOnLoading() {
			return (
				<LoaderLego simple style={ Styles.empty } />
			)
		}

		view() {
			return (
				<RowImageLego
					title={ this.props.title }
					addable={ this.props.addable }
					reorder={ false }
					images={ this.props.data.map(images => {
						return images
					}) }
					uploader={ this.uploader }
					remover={ this.remover }
					orderer={ this.orderer }
					onUpdate={ this.props.onUpdate }
					max_attachment={ this.props.max_attachment }
				/>
			)
		}
	}
)
