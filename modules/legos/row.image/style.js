import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	wrap: {
		flexWrap: 'wrap',
	},
	container: {
		marginRight: Sizes.margin.default,
		marginBottom: Sizes.margin.default,
	},
	image: {
		height: 128,
		width: 96,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .16),
	},
	mover: {
		height: 24,
		width: 24,
		position: 'absolute',
		top: 29,
		right: 1,
		background: Colors.white.primary,
	},

	moverLeft: {
		height: 24,
		width: 24,
		position: 'absolute',
		top: 29,
		left: 1,
		background: Colors.white.primary,
	},
	loading: {
		width: 100,
	},
})
