import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	padder: {
		marginBottom: 0,
	},
	// required: {
	// 	color: Colors.red.palette(3),
	// },
	header: {
		padding: 0,
		marginTop: 0,
	},
})
