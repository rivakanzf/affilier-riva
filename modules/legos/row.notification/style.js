import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	padder: {
		marginBottom: 0,
		// height: 84,
	},
	center: {
		justifyContent: 'center',
	},
	title: {
		marginRight: 24,
	},
	hint: {
		marginRight: 8,
	},
	empty: {
		height: 44,
		justifyContent: 'center',
	},
	pressable: {
		color: Colors.primary,
	},
	content: {
		paddingTop: Sizes.margin.default - 4,
	},
	row: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingTop: 8,
		paddingBottom: 8,
		backgroundColor: Colors.solid.grey.palette(1),
		marginTop: 4,
	},
})
