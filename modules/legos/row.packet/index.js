import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import RowLego from 'modules/legos/row';
import UnitLego from 'modules/legos/unit';

import GetterPacketComponent from 'modules/components/getter.packet';

import Styles from './style'


export default ConnectHelper(
	class RowPacketLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				readonly: PropTypes.bool,
				deletable: PropTypes.bool,
				packetId: PropTypes.id,
				variantId: PropTypes.id,
				productId: PropTypes.id,
				packet: PropTypes.object,
				onChange: PropTypes.func,
				onDelete: PropTypes.func,
			}
		}

		constructor(p) {
			super(p)

			this.data = {
				weight: null,
				height: null,
				width: null,
				length: null,
			}

			this.placeholder = {
				weight: null,
				height: null,
				width: null,
				length: null,
			}
		}

		onChange = (type, e, val) => {
			this.data[type] = val ? parseFloat(val) : val

			this.props.onChange &&
			this.props.onChange(this.data)
		}

		rowRenderer = (data) => {
			if(Array.isArray(data)) {
				this.data.weight = data[0].weight
				this.data.height = data[0].height
				this.data.width = data[0].width
				this.data.length = data[0].length
				this.placeholder.weight = data[1].weight
				this.placeholder.height = data[1].height
				this.placeholder.width = data[1].width
				this.placeholder.length = data[1].length
			} else {
				this.data.weight = data.weight
				this.data.height = data.height
				this.data.width = data.width
				this.data.length = data.length
			}

			return (
				<RowLego
					key={ `${this.props.packetId}${this.props.variantId}` }
					data={[{
						headerless: true,
						children: (
							<BoxBit row>
								<BoxBit>
									<GeomanistBit selectable type={GeomanistBit.TYPES.PARAGRAPH_3} weight="semibold" style={Styles.title}>
										Length
									</GeomanistBit>
									<TextInputBit unflex={false}
										readonly={ this.props.readonly }
										onChange={this.onChange.bind(this, 'length')}
										defaultValue={this.data.length || undefined}
										placeholder={`${this.placeholder.length || '-'}`}
										style={Styles.input}
									/>
								</BoxBit>
								<BoxBit>
									<GeomanistBit selectable type={GeomanistBit.TYPES.PARAGRAPH_3} weight="semibold" style={Styles.title}>
										Width
									</GeomanistBit>
									<TextInputBit unflex={false}
										readonly={ this.props.readonly }
										onChange={this.onChange.bind(this, 'width')}
										defaultValue={this.data.width || undefined}
										placeholder={`${this.placeholder.width || '-'}`}
										style={Styles.input}
									/>
								</BoxBit>
								<BoxBit>
									<GeomanistBit selectable type={GeomanistBit.TYPES.PARAGRAPH_3} weight="semibold" style={Styles.title}>
										Height
									</GeomanistBit>
									<TextInputBit unflex={false}
										readonly={ this.props.readonly }
										onChange={this.onChange.bind(this, 'height')}
										defaultValue={this.data.height || undefined}
										placeholder={`${this.placeholder.height || '-'}`}
										style={Styles.input}
									/>
								</BoxBit>
								<UnitLego title="cm" style={Styles.unit} />
							</BoxBit>
						),
					}, {
						title: 'Weight',
						children: (
							<BoxBit row>
								<TextInputBit unflex={false}
									readonly={this.props.readonly}
									onChange={this.onChange.bind(this, 'weight')}
									defaultValue={this.data.weight || undefined}
									placeholder={`${this.placeholder.weight || 'Enter weight'}`}
								/>
								<UnitLego title="kg" left />
								{ this.props.deletable && this.props.packetId ? (
									<TouchableBit unflex centering style={Styles.trash} onPress={ this.props.onDelete }>
										<IconBit
											name="trash"
											color={Colors.primary}
											size={24}
										/>
									</TouchableBit>
								) : false }
							</BoxBit>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}

		view() {
			return (
				<GetterPacketComponent packetId={ this.props.packetId } variantId={ this.props.variantId } productId={ this.props.productId } packet={ this.props.packet } children={this.rowRenderer} />
			)
		}
	}
)
