import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class RowLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string,
				})),
				activeIndex: PropTypes.number,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			data: [],
		}

		constructor(p) {
			super(p, {
				activeIndex: p.activeIndex,
			})
		}

		onSwitch = (index) => {
			this.setState({
				activeIndex: index,
			})

			this.props.onPress &&
			this.props.onPress(index)
		}

		contentRenderer = (data, i) => {
			return (
				<TouchableBit key={i} row unflex style={Styles.pr24} onPress={ this.onSwitch.bind(this, i) }>
					<RadioBit isActive={i === this.state.activeIndex} dumb onPress={ this.onSwitch.bind(this, i) } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.redirectText}>
						{ data.title }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		view() {
			return (
				<BoxBit unflex row style={Styles.containerRedirect}>
					{ this.props.data.map(this.contentRenderer) }
				</BoxBit>
			)
		}
	}
)
