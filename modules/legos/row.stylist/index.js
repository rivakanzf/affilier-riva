import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import RowLego from 'modules/legos/row';
import SelectionBit from 'modules/bits/selection'

import Styles from './style'


export default ConnectHelper(
	class RowStylistLego extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				stylist: PropTypes.string,
				placeholder: PropTypes.string,
				onChange: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				stylists: [],
			}, [])
		}

		view() {
			return (
				<RowLego
					data={[{
						title: 'Stylist*',
						children: (
							<SelectionBit
								key={this.props.stylist}
								options={this.state.stylists || []}
								onChange={this.props.onChange}
								placeholder={this.props.placeholder}
							/>
						),
					}]}
					style={Styles.padder}
				/>
			)
		}
	}
)
