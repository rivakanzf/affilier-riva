import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	padder: {
		marginBottom: 0,
	},
	// required: {
	// 	color: Colors.red.palette(3),
	// },

	containerMaxlength: {
		justifyContent: 'space-between',
		paddingTop: 8,
	},

	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},
})
