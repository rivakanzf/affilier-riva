import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import SelectionBit from 'modules/bits/selection';

import { orderBy, isEmpty } from 'lodash';

let cache = null


export default ConnectHelper(
	class SelectBrandAddressLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				brandAddressId: PropTypes.id,
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				unflex: PropTypes.bool,
				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToQuery(state, oP) {
			const _cache = CommonHelper.default(oP.cache, true)

			return !_cache || !cache ? [`
				query {
					brandAddressesList(filter: {
						isPickupPoint: {
							equalTo: true
						}
					}) {
						id
						title
					}
				}
			`, {
				fetchPolicy: _cache ? 'cache-first' : 'no-cache',
			}, state.me.token] : null
		}

		static getDerivedStateFromProps(nP) {
			if(!isEmpty(nP.data)) {
				cache = nP.data
			} else if(cache) {
				// check for cache
			} else {
				return null
			}

			return {
				brands: (!nP.isRequired ? [{
					key: '',
					title: nP.emptyTitle,
				}] : []).concat(orderBy(cache.brandAddressesList.map(b => {
					return {
						selected: b.id === nP.brandAddressId,
						key: b.id,
						title: b.title,
					}
				}), ['title'], 'asc')),
			}
		}

		static defaultProps = {
			cache: true,
			unflex: false,
			placeholder: 'Select address',
			emptyTitle: 'No address',
		}

		constructor(p) {
			super(p, {
				brands: [],
			}, [])
		}

		viewOnLoading() {
			return (
				<SelectionBit unflex={ this.props.unflex } disabled
					options={[]}
					placeholder={'Loading ...'}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		view() {
			return (
				<SelectionBit
					key={ this.props.brandId }
					unflex={ this.props.unflex }
					placeholder={ this.props.placeholder }
					options={this.state.brands}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
					onChange={this.props.onChange}
				/>
			)
		}
	}
)
