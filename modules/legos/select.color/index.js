import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import SelectionBit from 'modules/bits/selection';

import { capitalize, orderBy, isEmpty } from 'lodash'

let cache = null


export default ConnectHelper(
	class SelectColorLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				colorId: PropTypes.number,
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				disabled: PropTypes.bool,
				unflex: PropTypes.bool,
				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToQuery(state, oP) {
			const _cache = CommonHelper.default(oP.cache, true)

			return !_cache || !cache ? [`
				query {
					colorsList {
						id
						title
					}
				}
			`, {
				fetchPolicy: _cache ? 'cache-first' : 'no-cache',
			}, state.me.token] : null
		}

		static getDerivedStateFromProps(nP) {
			if (!isEmpty(nP.data)) {
				cache = nP.data
			} else if (cache) {
				// check for cache
			} else {
				return null
			}

			return {
				colors: (!nP.isRequired ? [{
					key: '',
					title: nP.emptyTitle,
				}] : []).concat(orderBy(cache.colorsList.map(c => {
					return {
						selected: c.id === nP.colorId,
						key: c.id,
						title: capitalize(c.title),
					}
				}), ['title'], 'asc')),
			}
		}

		static defaultProps = {
			cache: true,
			placeholder: 'Filter by color',
			emptyTitle: 'All Colors',
			unflex: false,
		}

		constructor(p) {
			super(p, {
				colors: [],
			}, [])
		}

		viewOnLoading() {
			return (
				<SelectionBit unflex={ this.props.unflex } disabled
					placeholder={'Loading ...'}
					options={[]}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		view() {
			return (
				<SelectionBit unflex={ this.props.unflex }
					disabled={ this.props.disabled }
					key={ this.props.colorId }
					placeholder={ this.props.placeholder }
					options={ this.state.colors || [] }
					onChange={ this.props.onChange }
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}
	}
)
