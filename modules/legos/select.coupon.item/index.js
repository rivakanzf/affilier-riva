import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import CheckboxBit from 'modules/bits/checkbox';
import RefService from 'app/services/ref';

import { capitalize, isEmpty } from 'lodash'
import Styles from './style';

const cache = {}

export default ConnectHelper(
	class SelectCouponItemLego extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				unflex: PropTypes.bool,
				disabled: PropTypes.bool,

				value: PropTypes.array,

				onChangeValidAll: PropTypes.func.isRequired,
				onChangeValidItem: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			// const _cache = CommonHelper.default(oP.cache, true)

			// return !_cache || isEmpty(cache) ? RefService.getRefAvailableCouponValidity() : null
			return RefService.getRefAvailableCouponValidity()
		}

		static getDerivedStateFromProps(nP) {
			if (!isEmpty(nP.data)) {
				cache[nP.type] = nP.data
			} else if (cache[nP.type]) {
				// check for cache
			} else {
				return null
			}


			return (!isEmpty(nP.data)) && ({
				isAll: nP.isAllValid,
				couponValidity: nP.data.map(title => title),
			})
		}

		static defaultProps = {
			cache: true,
			disabled: false,
			unflex: false,
		}

		constructor(p) {
			super(p, {
				isAll: false,
				couponValidity: [],
				selected: p.value || [],
			}, [])
		}

		onChange = (val) => {
			this.setState({
				isAll: val,
				selected: val ? this.state.couponValidity.map(title => title) : [],
			}, () => {
				this.props.onChangeValidAll &&
				this.props.onChangeValidAll(val, this.state.selected)
			})
		}

		onChangeItem = title => {
			let newArr = this.state.selected.slice()
			const index = newArr.indexOf(title)


			if(index > -1) {
				newArr.splice(index, 1)
			} else {
				newArr = [...newArr, title]
			}
			this.setState({
				selected: newArr,
			}, () => {
				this.props.onChangeValidItem &&
				this.props.onChangeValidItem(this.state.selected)
			})
		}

		itemRenderer = (title, i) => {
			return (
				<BoxBit key={i} unflex row style={Styles.item}>
					<CheckboxBit
						dumb
						isActive={this.state.selected.indexOf(title) > -1}
						onPress={this.onChangeItem.bind(this, title)}
						style={Styles.padder}
					/>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={this.props.inputStyle}>
						{ capitalize(title) }
					</GeomanistBit>
				</BoxBit>
			)
		}

		mainRenderer() {
			return (
				<BoxBit unflex row style={Styles.item}>
					<CheckboxBit isActive={this.state.isAll} onPress={this.onChange} style={Styles.padder}/>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={this.props.inputStyle}>Valid For All</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit unflex row accessible={false }style={Styles.item}>
					<CheckboxBit style={Styles.padder}/>
					<BoxBit unflex style={Styles.loading}/>
				</BoxBit>
			)
		}
		
		view() {
			return (
				<BoxBit accessible={!this.props.disabled}>
					{ this.mainRenderer() }
					<BoxBit unflex>
						{ this.state.couponValidity.map(this.itemRenderer) }
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
