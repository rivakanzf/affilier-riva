import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	padder: {
		marginRight: 8,
	},
	item: {
		marginTop: 4,
		marginBottom: 4,
	},

	loading: {
		width: 150,
		height: 24,
		backgroundColor: Colors.grey.palette(3, .2),
	},

})
