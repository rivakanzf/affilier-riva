import React from 'react';
import QueryStatefulModel from 'coeur/models/query.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import SelectionBit from 'modules/bits/selection';

import { capitalize, flattenDeep, orderBy, isEmpty } from 'lodash'

const cache = {}


export default ConnectHelper(
	class SelectCategoryLego extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				value: PropTypes.string,

				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				unflex: PropTypes.bool,
				disabled: PropTypes.bool,
				onChange: PropTypes.func.isRequired,

				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			cache: true,
			disabled: false,
			unflex: false,
			placeholder: 'Select Coupon Type',
			emptyTitle: 'All Coupon Type',
		}

		constructor(p) {
			super(p, {
				couponType: [
					'PERCENT',
					'TOTAL',
					'SHIPPING',
					// 'ITEM', NOT INCLUDED YET
				],
			})
		}


		getTitle = (type) => {
			switch(type) {
			case 'SHIPPING':
				return 'Shipping Amount';
			case 'ITEM':
				return 'Each Applicable Item';
			case 'TOTAL':
				return 'Total Amount';
			case 'PERCENT':
				return 'Percentage';
			default:
				return type
			}
		}

		viewOnLoading() {
			return (
				<SelectionBit
					disabled
					unflex={ this.props.unflex }
					placeholder={'Loading ...'}
					options={[]}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		// onChange= (key) => {
		// 	this.props.onChange &&
		// 	this.props.onChange('selection', key)
		// }

		view() {
			return (
				<SelectionBit
					disabled={this.props.disabled}
					key={ this.props.value }
					unflex={ this.props.unflex }
					placeholder={ this.props.placeholder }
					options={this.state.couponType.map(title => {
						return {
							key: title,
							title: this.getTitle(title),
							selected: this.getTitle(this.props.value) === this.getTitle(title),
						}
					})}
					onChange={this.props.onChange}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}
	}
)
