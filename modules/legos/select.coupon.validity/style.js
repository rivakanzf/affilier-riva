import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	item: {
		marginTop: 4,
		marginBottom: 4,
	},

	loading: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

})
