import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import SelectionBit from 'modules/bits/selection';
// import { isEmpty } from 'lodash'

export default ConnectHelper(
	class SelectDateLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				status: PropTypes.string,
				statuses: PropTypes.arrayOf(PropTypes.shape({
					key: PropTypes.string,
					title: PropTypes.string,
				})),
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				unflex: PropTypes.bool,
				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.statuses !== nS.lastStatuses || nP.status !== nS.lastStatus) {
				return {
					lastStatus: nP.status,
					lastStatuses: nP.statuses,
					statuses: (!nP.isRequired ? [{
						key: '',
						title: nP.emptyTitle,
					}] : []).concat(nP.statuses.map(status => {
						return {
							key: status.key,
							title: status.title,
							selected: status.key === nP.status,
						}
					})),
				}
			}

			return null
		}

		static defaultProps = {
			statuses: [{
				key: '1 days',
				title: 'Today',
			}, {
				key: '7 days',
				title: 'This week',
			}, {
				key: '1 months',
				title: 'This month',
			}, {
				key: '2 months',
				title: 'Last 2 months',
			}, {
				key: '3 months',
				title: 'Last 3 months',
			}, {
				key: '1 years',
				title: 'Last year',
			}],
			placeholder: 'Select dates',
			emptyTitle: 'All dates',
			unflex: false,
		}

		constructor(p) {
			super(p, {
				lastStatus: undefined,
				lastStatuses: [],
				statuses: [],
			}, [])
		}

		viewOnLoading() {
			return (
				<SelectionBit
					unflex={ this.props.unflex } disabled
					placeholder={'Loading ...'}
					options={[]}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		view() {
			return (
				<SelectionBit
					key={ this.props.status }
					unflex={ this.props.unflex }
					placeholder={ this.props.placeholder }
					options={this.state.statuses || []}
					onChange={this.props.onChange}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}
	}
)
