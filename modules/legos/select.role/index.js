import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import SelectionBit from 'modules/bits/selection';


export default ConnectHelper(
	class SelectRoleLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				role: PropTypes.string,
				disabledRoles: PropTypes.arrayOf(PropTypes.string),
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				unflex: PropTypes.bool,
				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			unflex: false,
			placeholder: 'Filter by role',
			emptyTitle: 'All Roles',
			disabledRoles: [],
		}

		constructor(p) {
			super(p, {
				roles: (!p.isRequired ? [{
					key: '',
					title: p.emptyTitle,
				}] : []).concat([{
					key: 'admin',
					selected: p.role === 'admin',
					disabled: p.disabledRoles.indexOf('admin') > -1,
					title: 'Administrator',
				}, {
					key: 'superadmin',
					selected: p.role === 'superadmin',
					disabled: true,
					title: 'Super Admin',
				}, {
					key: 'headstylist',
					selected: p.role === 'headstylist',
					disabled: p.disabledRoles.indexOf('headstylist') > -1,
					title: 'Head Stylist',
				}, {
					key: 'stylist',
					selected: p.role === 'stylist',
					disabled: p.disabledRoles.indexOf('stylist') > -1,
					title: 'Stylist',
				}, {
					key: 'fulfillment',
					selected: p.role === 'fulfillment',
					disabled: p.disabledRoles.indexOf('fulfillment') > -1,
					title: 'Fulfillment',
				}, {
					key: 'inventory',
					selected: p.role === 'inventory',
					disabled: p.disabledRoles.indexOf('inventory') > -1,
					title: 'Inventory',
				}, {
					key: 'purchasing',
					selected: p.role === 'purchasing',
					disabled: p.disabledRoles.indexOf('purchasing') > -1,
					title: 'Purchasing',
				}, {
					key: 'analyst',
					selected: p.role === 'analyst',
					disabled: p.disabledRoles.indexOf('analyst') > -1,
					title: 'Analyst',
				}, {
					key: 'finance',
					selected: p.role === 'finance',
					disabled: p.disabledRoles.indexOf('finance') > -1,
					title: 'Finance',
				}, {
					key: 'packing',
					selected: p.role === 'packing',
					disabled: p.disabledRoles.indexOf('packing') > -1,
					title: 'Packing',
				}, {
					key: 'marketing',
					selected: p.role === 'marketing',
					disabled: p.disabledRoles.indexOf('marketing') > -1,
					title: 'Marketing',
				}]),
			}, [])
		}

		view() {
			return (
				<SelectionBit unflex={ this.props.unflex }
					placeholder={ this.props.placeholder }
					options={ this.state.roles }
					onChange={ this.props.onChange }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
				/>
			)
		}
	}
)
