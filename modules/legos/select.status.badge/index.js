import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import BadgeStatusLego from '../badge.status';
import SelectStatusLego, { TYPES } from '../select.status';

import Styles from './style';


export default ConnectHelper(
	class SelectStatusBadge extends StatefulModel {

		static TYPES = TYPES

		static propTypes(PropTypes) {
			return {
				tight: PropTypes.bool,
				type: PropTypes.oneOf(this.TYPES).isRequired,
				status: PropTypes.string,
				disabledStatuses: PropTypes.arrayOf(PropTypes.string),
				isRequired: PropTypes.bool,

				onChange: PropTypes.func.isRequired,
				disabled: PropTypes.bool,

				style: PropTypes.style,
			}
		}

		static defaultProps = {
			disabledStatuses: [],
		}

		constructor(p) {
			super(p, {
				statuses: [],
			}, [])
		}

		onChange = () => {
			// do nothing
		}

		viewOnLoading() {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.darkGrey}>-</GeomanistBit>
			)
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					<BadgeStatusLego tight={ this.props.tight } unflex={false} onPress={ this.onChange } status={ this.props.status } disabled={ this.props.disabled } />
					<BoxBit style={Styles.select}>
						<SelectStatusLego
							isRequired={ this.props.isRequired }
							type={ this.props.type }
							status={ this.props.status }
							disabled={ this.props.disabled }
							disabledStatuses={ this.props.disabledStatuses }
							onChange={ this.props.onChange }
							style={ Styles.input }
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
