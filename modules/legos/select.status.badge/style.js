import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		// paddingRight: 20,
	},
	text: {
		color: Colors.black.palette(2),
	},
	select: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		opacity: 0,
	},
	input: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		height: 'auto',
	},
	icon: {
		position: 'absolute',
		right: 0,
		top: 0,
		bottom: 0,
	},
})
