import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import RefService from 'app/services/ref';

import SelectionBit from 'modules/bits/selection';
import { capitalize, isEmpty, memoize } from 'lodash'

const cache = {}

export const getReadableStatus = memoize(function(status) {
	switch(status) {
	case 'PRIMED':
		return 'Packed'
	default:
		return status.split('_').map(s => capitalize(s)).join(' ')
	}
})

export const TYPES = {
	ASSETS: 'ASSETS',
	BOOKING_SOURCES: 'BOOKING_SOURCES',
	BOOKING_STATUSES: 'BOOKING_STATUSES',
	CODES: 'CODES',
	COUPONS: 'COUPONS',
	EXCHANGES: 'EXCHANGES',
	COUPON_VALIDITY: 'COUPON_VALIDITY',
	FEEDBACKS: 'FEEDBACKS',
	INVENTORIES: 'INVENTORIES',
	NOTIFICATIONS: 'NOTIFICATIONS',
	ORDERS: 'ORDERS',
	ORDER_DETAILS: 'ORDER_DETAILS',
	ORDER_REQUESTS: 'ORDER_REQUESTS',
	ORDER_DETAIL_STATUSES: 'ORDER_DETAIL_STATUSES',
	PAYMENTS: 'PAYMENTS',
	PURCHASE_REQUESTS: 'PURCHASE_REQUESTS',
	PURCHASES: 'PURCHASES',
	QUESTION_GROUPS: 'QUESTION_GROUPS',
	QUESTIONS: 'QUESTIONS',
	REDEEM_STATUSES: 'REDEEM_STATUSES',
	REQUIREMENT_OPERATORS: 'REQUIREMENT_OPERATORS',
	SHIPMENTS: 'SHIPMENTS',
	SHIPMENT_STATUSES: 'SHIPMENT_STATUSES',
	SOCIALS: 'SOCIALS',
	STYLESHEETS: 'STYLESHEETS',
	STYLEBOARD_STATUSES: 'STYLEBOARD_STATUSES',
	STYLECARD_STATUSES: 'STYLECARD_STATUSES',
	STYLESHEET_STATUSES: 'STYLESHEET_STATUSES',
	TOKENS: 'TOKENS',
	USER_REQUESTS: 'USER_REQUESTS',
	VOUCHERS: 'VOUCHERS',
	VOUCHER_STATUSES: 'VOUCHER_STATUSES',
}

export default ConnectHelper(
	class SelectStatusLego extends PromiseStatefulModel {

		static TYPES = TYPES

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf(this.TYPES).isRequired,
				cache: PropTypes.bool,
				unflex: PropTypes.bool,
				disabled: PropTypes.bool,
				nullAsPending: PropTypes.bool,
				status: PropTypes.string,
				disabledStatuses: PropTypes.arrayOf(PropTypes.string),

				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,

				onChange: PropTypes.func,

				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			const _cache = CommonHelper.default(oP.cache, true)

			return !_cache || !cache[oP.type] ? RefService.getEnum(oP.type).then(statuses => {
				if(oP.nullAsPending) {
					return ['PENDING', ...statuses]
				} else {
					return statuses
				}
			}) : null
		}

		static getDerivedStateFromProps(nP) {
			if (!isEmpty(nP.data)) {
				cache[nP.type] = nP.data
			} else if (cache[nP.type]) {
				// check for cache
			} else {
				return null
			}

			return {
				statuses: (!nP.isRequired ? [{
					key: '',
					title: nP.emptyTitle,
				}] : []).concat(cache[nP.type].map(s => {
					return {
						key: nP.nullAsPending && s === 'PENDING' ? 'null' : s,
						title: getReadableStatus(s),
						selected: s === nP.status,
						disabled: nP.disabledStatuses.indexOf(s) > -1,
					}
				})),
			}
		}

		static defaultProps = {
			cache: true,
			unflex: false,
			disabledStatuses: [],
			placeholder: 'Select status',
			emptyTitle: 'All status',
		}

		constructor(p) {
			super(p, {
				statuses: [],
			}, [])
		}

		viewOnLoading() {
			return (
				<SelectionBit
					unflex={ this.props.unflex } disabled
					placeholder={ 'Loading ...' }
					options={ [] }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
				/>
			)
		}

		view() {
			return (
				<SelectionBit
					key={ this.props.status }
					disabled={ this.props.disabled }
					unflex={ this.props.unflex }
					placeholder={ this.props.placeholder }
					options={ this.state.statuses || [] }
					onChange={ this.props.onChange }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
				/>
			)
		}
	}
)
