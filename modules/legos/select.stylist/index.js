import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import UserService from 'app/services/user.new';

import SelectionBit from 'modules/bits/selection';

import { orderBy, isEmpty } from 'lodash';

let cache = null
let getter = null

export default ConnectHelper(
	class SelectStylistLego extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				cache: PropTypes.bool,
				stylist: PropTypes.string,
				placeholder: PropTypes.string,
				emptyTitle: PropTypes.string,
				isRequired: PropTypes.bool,
				unflex: PropTypes.bool,
				onChange: PropTypes.func.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			const _cache = CommonHelper.default(oP.cache, true)

			if (!getter && (!_cache || !cache)) {
				getter = UserService.getUsers({
					limit: 64,
					role: 'stylist',
				}, state.me.token).then(res => res.data).catch(() => [])

				return getter
			} else if(getter) {
				return getter
			}

			return null
		}

		static getDerivedStateFromProps(nP) {
			if(!isEmpty(nP.data)) {
				cache = nP.data
			} else if(cache) {
				// check for cache
			} else {
				return null
			}

			const stylist = (!nP.isRequired ? [{
				key: '',
				title: nP.emptyTitle,
			}] : []).concat(orderBy(cache.map(user => {
				return {
					selected: user.name === nP.stylist,
					key: user.id,
					title: user.name,
				}
			}), ['title'], 'asc'))

			if (nP.stylist && stylist.findIndex(s => s.selected) === -1) {
				stylist.push({
					selected: true,
					disabled: true,
					key: '',
					title: nP.stylist,
				})
			}

			return {
				stylist,
			}
		}

		static defaultProps = {
			cache: true,
			unflex: false,
			placeholder: 'Search Stylist',
			emptyTitle: 'All Stylist',
		}

		constructor(p) {
			super(p, {
				stylist: [],
			}, [])
		}

		viewOnLoading() {
			return (
				<SelectionBit unflex={ this.props.unflex } disabled
					options={[]}
					placeholder={'Loading ...'}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}

		view() {
			return (
				<SelectionBit unflex={ this.props.unflex }
					key={ this.props.stylist }
					disabled={ !this.state.stylist.length }
					placeholder={ this.props.placeholder }
					options={ this.state.stylist }
					onChange={ this.props.onChange }
					style={ this.props.style }
					inputStyle={ this.props.inputStyle }
				/>
			)
		}
	}
)
