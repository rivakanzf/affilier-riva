import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class SelectionAddressLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				odd: PropTypes.bool,
				editable: PropTypes.bool,
				selectable: PropTypes.bool,
				isSelected: PropTypes.bool,
				id: PropTypes.number,
				title: PropTypes.string,
				receiver: PropTypes.string,
				phone: PropTypes.string,
				address: PropTypes.string,
				district: PropTypes.string,
				postal: PropTypes.string,
				onPress: PropTypes.func,
				onEdit: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			selectable: true,
		}

		onPress = () => {
			this.props.onPress &&
			this.props.onPress(this.props.id)
		}

		view() {
			return (
				<TouchableBit row unflex activeOpacity={ this.props.selectable ? undefined : 1 } accessible={ this.props.selectable || this.props.editable }
					style={[Styles.container, this.props.odd && Styles.containerOdd]}
					onPress={ this.props.selectable ? this.onPress : undefined }
				>
					<TouchableBit unflex enlargeHitSlop onPress={ this.props.onEdit } style={Styles.icon}>
						{ this.props.editable ? (
							<IconBit
								name="edit"
								color={ Colors.primary }
							/>
						) : false }
					</TouchableBit>
					<BoxBit style={Styles.content}>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.addressCapital}>
							{ this.props.title || this.props.receiver || '-'}
						</GeomanistBit>
						{ this.props.title ? (
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.addressCapital}>
								{ this.props.receiver || '-' }
							</GeomanistBit>
						) : false }
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.address}>
							{ this.props.phone || '-' }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.addressCapital}>
							{ this.props.address || '-' }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.addressCapital}>
							{ this.props.district || '-' }
						</GeomanistBit>
					</BoxBit>
					{ this.props.selectable && (
						<BoxBit unflex centering>
							<RadioBit dumb
								isActive={ this.props.isSelected }
								onPress={ this.onPress }
							/>
						</BoxBit>
					) }
				</TouchableBit>
			)
		}
	}
)
