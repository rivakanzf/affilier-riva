import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	address: {
		paddingRight: 8,
	},

	addressCapital: {
		paddingRight: 8,
		textTransform: 'capitalize',

	},

	container: {
		padding: Sizes.margin.default,
		justifyContent: 'space-between',
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		backgroundColor: Colors.white.primary,
	},

	containerOdd: {
		backgroundColor: Colors.solid.grey.palette(4),
	},

	content: {
		paddingRight: Sizes.margin.thick,
	},

	icon: {
		marginRight: Sizes.margin.default,
	},

	button: {
		marginTop: 8,
	},

})
