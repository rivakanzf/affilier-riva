import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class SelectionAddressLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				odd: PropTypes.bool,
				isSelected: PropTypes.bool,
				id: PropTypes.number,
				title: PropTypes.string,
				district: PropTypes.string,
				city: PropTypes.string,
				province: PropTypes.string,
				postal: PropTypes.string,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		onPress = () => {
			this.props.onPress &&
			this.props.onPress(this.props.id)
		}

		view() {
			return (
				<TouchableBit row unflex
					style={[Styles.container, this.props.odd && Styles.containerOdd]}
					onPress={ this.onPress }
				>
					<BoxBit style={Styles.content}>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
							{ this.props.title } – { this.props.district }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={ Styles.note }>
							{ this.props.city } – { this.props.province }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={ Styles.note }>
							{ this.props.postal }
						</GeomanistBit>
					</BoxBit>
					<BoxBit unflex centering>
						<RadioBit dumb
							isActive={ this.props.isSelected }
							onPress={ this.onPress }
						/>
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)
