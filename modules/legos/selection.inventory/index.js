import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';
import FormatHelper from 'coeur/helpers/format';

// import InventoryService from 'app/services/new.inventory';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

import { isEmpty } from 'lodash'


export default ConnectHelper(
	class SelectionInventoryLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				selectable: PropTypes.bool,
				borderless: PropTypes.bool,
				isSelected: PropTypes.bool,
				data: PropTypes.shape({
					id: PropTypes.id,
					image: PropTypes.object,
					brand: PropTypes.string,
					title: PropTypes.string,
					size: PropTypes.string,
					color: PropTypes.string,
					price: PropTypes.number,
					retail_price: PropTypes.number,
					quantity: PropTypes.number,
				}),
				onSelect: PropTypes.func,
				style: PropTypes.style,
			}
		}

		// static propsToPromise(state, oP) {
		// 	if(oP.dumb) {
		// 		return Promise.resolve(oP.data)
		// 	} else {
		// 		return oP.id ? InventoryService.getDetail(oP.id, {
		// 			token: state.me.token,
		// 		}) : null
		// 	}
		// }

		onPress = () => {
			this.props.onSelect &&
			this.props.onSelect(this.props.id)
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering style={ [Styles.container, Styles.empty, this.props.style ] }>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return isEmpty(this.props.data) ? this.viewOnLoading() : (
				<TouchableBit unflex row onPress={ this.onPress } style={[Styles.container, this.props.borderless && Styles.borderless, this.props.style]}>
					<ImageBit overlay
						resizeMode={ ImageBit.TYPES.COVER }
						broken={ !this.props.data.image }
						source={ this.props.data.image || undefined }
						style={ Styles.image }
					/>
					<BoxBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={Styles.text}>
							{ this.props.data.brand || '-' }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={Styles.text}>
							{ this.props.data.title || '-' }
						</GeomanistBit>
						{ this.props.data.price === this.props.data.retail_price ? (
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.price}>
								IDR { FormatHelper.currencyFormat(this.props.data.price) || '-' }
							</GeomanistBit>
						) : (
							<BoxBit unflex row>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.discounted}>
									IDR {FormatHelper.currencyFormat(this.props.data.price) || '-'}
								</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.retail}>
									IDR {FormatHelper.currencyFormat(this.props.data.retail_price) || '-'}
								</GeomanistBit>
							</BoxBit>
						) }
						<BoxBit />
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
							Color: { this.props.data.color } – Size: { this.props.data.size }
						</GeomanistBit>
					</BoxBit>
					<BoxBit unflex centering>
						<RadioBit dumb isActive={ this.props.isSelected } onPress={ this.onPress } />
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)
