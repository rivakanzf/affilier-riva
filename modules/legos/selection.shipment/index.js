import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import HintLego from 'modules/legos/hint';
import ShipmentListLego from 'modules/legos/shipment.list';

import Styles from './style'


export default ConnectHelper(
	class SelectionShipmentLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				// image: PropTypes.image,
				title: PropTypes.string,
				description: PropTypes.string,
				note: PropTypes.string,
				isSelected: PropTypes.bool,
				selectable: PropTypes.bool,
				editable: PropTypes.bool,
				shipments: PropTypes.array,
				onSelect: PropTypes.func,
				onEdit: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			selectable: true,
			shipments: [],
		}

		onSelect = () => {
			this.props.onSelect &&
			this.props.onSelect(this.props.id)
		}

		onEditOrView = shipment => {
			// TODO
			this.props.onEdit &&
			this.props.onEdit(shipment)
		}

		shipmentRenderer = shipment => {
			return (
				<ShipmentListLego
					key={ shipment.id }
					id={ shipment.id }
					isReturn={ shipment.is_return }
					courier={ shipment.courier }
					service={ shipment.service }
					awb={ shipment.awb }
					status={ shipment.status }
					editable={ this.props.editable }
					onPress={ this.onEditOrView.bind(this, shipment) }
					style={ Styles.shipment }
				/>
			)
		}

		view() {
			return (
				<BoxBit style={[ Styles.container, this.props.style ]}>
					<TouchableBit accessible={ this.props.selectable ? true : 'box-none' } unflex row onPress={ this.onSelect }>
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium">{ this.props.title }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ this.props.description }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.note }>
								{ this.props.note }
							</GeomanistBit>
						</BoxBit>
						{ this.props.selectable ? (
							<BoxBit unflex centering>
								<CheckboxBit dumb
									isActive={ this.props.isSelected }
									onPress={ this.onSelect }
									style={ Styles.select }
								/>
							</BoxBit>
						) : (
							<BoxBit unflex centering>
								<HintLego title="This item's status need to be 'PRIMED' first before you can do shipment." />
							</BoxBit>
						) }
					</TouchableBit>
					{ this.props.shipments.length ? (
						<BoxBit unflex style={ Styles.shipments }>
							{ this.props.shipments.map(this.shipmentRenderer) }
						</BoxBit>
					) : false }
				</BoxBit>
			)
		}
	}
)
