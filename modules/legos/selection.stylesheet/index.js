import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import FormatHelper from 'coeur/helpers/format';

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ScrollViewBit from 'modules/bits/scroll.view';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import CompletionLego from 'modules/legos/completion';
import ColorLego from 'modules/legos/color';
import QuickViewImagesMosaicLego from 'modules/legos/quick.view.images.mosaic';

import QuickViewItemPagelet from 'modules/pagelets/quick.view.item';
import QuickViewProductPagelet from 'modules/pagelets/quick.view.product';

import Styles from './style';


export default ConnectHelper(
	class SelectionStylesheetLego extends StatefulModel {

		static TYPES = {
			STYLESHEET: 'STYLESHEET',
			STYLECARD: 'STYLECARD',
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				user: PropTypes.string,
				title: PropTypes.string,
				count: PropTypes.number,
				lineup: PropTypes.string,
				minValue: PropTypes.number,
				value: PropTypes.number,
				realValue: PropTypes.number,
				type: PropTypes.oneOf(this.TYPES),
				items: PropTypes.arrayOf(PropTypes.shape({
					variant_id: PropTypes.id,
					brand: PropTypes.string,
					title: PropTypes.string,
					image: PropTypes.object,
					color: PropTypes.string,
					category: PropTypes.string,
					colors: PropTypes.arrayOf(PropTypes.object),
					size: PropTypes.string,
					price: PropTypes.number,
					retail_price: PropTypes.number,
				})),
				selected: PropTypes.bool,
				percentage: PropTypes.bool,
				accessible: PropTypes.bool,
				addedRetail: PropTypes.number,
				addedPurchase: PropTypes.number,
				addedCount: PropTypes.number,
				onPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			items: [],
			accessible: true,
			type: 'STYLESHEET',
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.selected !== nS.lastSelected) {
				const {
					totalPurchase,
					totalRetail,
				} = nP.items.map(i => {
					return {
						totalPurchase: i.price,
						totalRetail: i.retail_price,
					}
				}).reduce((sum, curr) => {
					return {
						totalPurchase: sum.totalPurchase + curr.totalPurchase,
						totalRetail: sum.totalRetail + curr.totalRetail,
					}
				}, {
					totalPurchase: nP.addedPurchase,
					totalRetail: nP.addedRetail,
				})

				return {
					totalPurchase,
					totalRetail,
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				detailView: false,
				lastSelected: null,
				totalRetail: 0,
				totalPurchase: 0,
			})
		}

		onPress = () => {
			this.props.onPress &&
			this.props.onPress(this.props.id)
		}

		onToggleView = () => {
			this.setState({
				detailView: !this.state.detailView,
			})
		}

		onView = (variantId, brand, title, image, color, size, price, retailPrice, status, note) => {
			this.props.utilities.alert.modal({
				component: variantId ? (
					<QuickViewProductPagelet
						variantId={ variantId }
						variants
						includeZeroInventory
					/>
				) : (
					<QuickViewItemPagelet
						data={{
							brand,
							title,
							description: `Color: ${ color } – Size: ${ size }`,
							price,
							retail_price: retailPrice,
							image,
							status,
							note,
						}}
					/>
				),
			})
		}

		itemRenderer = ({brand, variant_id: variantId, title, category, size, image, color, price, retail_price: retailPrice, note}, index) => {
			return (
				<BoxBit key={ index } unflex row style={ Styles.item }>
					<BoxBit style={ Styles.content }>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_4 } style={ Styles.text }>
							{ brand }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_4 } style={ Styles.text } weight="medium">
							{ title }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_5 }>
							{ category }, { color } - { size }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_5 } style={ Styles.text }>
							IDR <TextBit style={ Styles.discounted }>{ FormatHelper.currency(retailPrice) }</TextBit><TextBit style={ Styles.retail }>{ FormatHelper.currency(price) }</TextBit>
						</GeomanistBit>
					</BoxBit>
					<TouchableBit unflex onPress={ this.onView.bind(this, variantId, brand, title, image, color, size, price, retailPrice, status, note) }>
						<IconBit
							name="quick-view"
							size={ 12 }
							color={ Colors.primary }
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		colorRenderer = image => {
			const inventory = this.props.items.find(i => i.image === image)

			return (
				<BoxBit>
					<BoxBit />
					<ColorLego colors={ inventory.colors } size={ 12 } radius={ 6 } border={ 1 } style={ Styles.color } />
				</BoxBit>
			)
		}

		view() {
			return (
				<TouchableBit unflex activeOpacity={ this.props.accessible ? undefined : 1 }  accessible={ this.props.accessible } onPress={ this.onPress } style={[ Styles.stylesheet, !this.props.accessible && Styles.unaccessible, this.props.selected && Styles.selected, this.props.style ]}>
					<BoxBit unflex row>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_3 }>
							#S{ this.props.type === this.TYPES.STYLECARD ? 'C' : 'T' }-{ this.props.id }
						</GeomanistBit>
						<BoxBit />
						<TouchableBit unflex centering onPress={ this.onToggleView } style={ Styles.icon }>
							<IconBit
								name={ this.state.detailView ? 'inventory' : 'drag-handle' }
								size={ this.state.detailView ? 12 : 16 }
								color={ Colors.primary }
							/>
						</TouchableBit>
						{ this.props.accessible && (
							<BoxBit unflex style={ Styles.icon }>
								<CheckboxBit dumb isActive={ this.props.selected } onPress={ this.onPress } />
							</BoxBit>
						) }
					</BoxBit>
					{ this.state.detailView ? (
						<ScrollViewBit unflex style={ Styles.items }>
							{ this.props.items.map(this.itemRenderer) }
						</ScrollViewBit>
					) : (
						<React.Fragment>
							{ this.props.stylist ? (
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_3 } style={ Styles.title }>
									By { this.props.stylist }
								</GeomanistBit>
							) : false }
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ this.props.stylist ? undefined : Styles.title }>
								{ this.props.title || 'Untitled' }
							</GeomanistBit>
							{ this.props.user ? (
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_3 }>
									for { this.props.user }
								</GeomanistBit>
							) : false }
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } ellipsis lines={ 2 } style={ Styles.lineup }>
								{ this.props.lineup ? this.props.lineup : 'Lineup: -' }
							</GeomanistBit>
							<BoxBit />
							<QuickViewImagesMosaicLego unflex small
								images={ this.props.items.map(i => i.image) }
								empty={ 'No items yet' }
								childRenderer={ this.colorRenderer }
								style={ Styles.images }
							/>
							<CompletionLego
								percentage={ this.props.percentage }
								count={ this.props.items.length + this.props.addedCount }
								purchase={ this.state.totalPurchase }
								retail={ this.state.totalRetail }

								targetCount={ this.props.count }
								targetRetail={ this.props.value }
								maxPurchase={ this.props.realValue }
								minRetail={ this.props.minValue }

								style={ Styles.completion }
							/>
						</React.Fragment>
					) }
				</TouchableBit>
			)
		}
	}
)
