import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	stylesheet: {
		paddingLeft: 8,
		paddingRight: 8,
		paddingTop: 8,
		paddingBottom: 3,
		borderRadius: 2,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
		backgroundColor: Colors.white.primary,
		width: 176,
		height: 275,
	},

	unaccessible: {
		opacity: .8,
		filter: 'grayscale(1)',
	},

	selected: {
		boxShadow: `0 0 2px ${ Colors.primary }`,
		// borderColor: Colors.primary,
		// borderWidth: StyleSheet.hairlineWidth,
	},

	title: {
		marginTop: 8,
	},

	lineup: {
		marginTop: 4,
		height: 28,
		// width: 160,
		color: Colors.black.palette(2, .6),
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		display: '-webkit-box',
  		'-webkit-line-clamp': '2',
  		'-webkit-box-orient': 'vertical',
	},

	images: {
		marginTop: 8,
		height: 112,
		// width: 172,
		marginLeft: -6,
		marginRight: -6,
	},

	completion: {
		marginTop: 2,
		marginLeft: -7,
		marginRight: -7,
	},

	color: {
		marginLeft: 4,
		marginBottom: 4,
		borderColor: Colors.white.primary,
	},

	icon: {
		width: 24,
		marginLeft: 4,
	},

	items: {
		// width: 160,
		height: 231,
		marginTop: 8,
	},

	item: {
		overflow: 'hidden',
		paddingTop: 4,
		paddingBottom: 4,
		paddingLeft: 8,
		paddingRight: 8,
		backgroundColor: Colors.solid.grey.palette(1),
		marginBottom: 2,
		minHeight: 28,
	},

	content: {
		justifyContent: 'space-between',
    	overflow: 'hidden',
    	marginRight: 4,
	},

	text: {
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap',
	},

	discounted: {
		textDecoration: 'line-through',
	},

	retail: {
		color: Colors.red.palette(7),
		marginLeft: 4,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	checkbox: {
		alignItems: 'flex-end',
		justifyContent: 'center',
	},


	box: {
		flexBasis: '40%',
		overflow: 'hidden',
		paddingRight: 8,
		alignItems: 'center',
	},


	row: {
		flexBasis: '100%',
		overflow: 'hidden',
		paddingRight: 8,
	},

	thin: {
		flexBasis: '60%',
		overflow: 'hidden',
		paddingRight: 8,
	},

})
