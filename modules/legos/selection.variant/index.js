import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';
import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';

import Styles from './style';


export default ConnectHelper(
	class SelectionVariantLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				image: PropTypes.string,
				brand: PropTypes.string,
				title: PropTypes.string,
				category: PropTypes.string,
				size: PropTypes.string,
				color: PropTypes.string,
				price: PropTypes.number,
				retailPrice: PropTypes.number,
				style: PropTypes.style,
			}
		}

		view() {
			return (
				<BoxBit unflex row style={[Styles.container, this.props.borderless && Styles.borderless, this.props.style]}>
					<ImageBit overlay
						resizeMode={ ImageBit.TYPES.COVER }
						broken={ !this.props.image }
						source={ this.props.image || undefined }
						style={ Styles.image }
					/>
					<BoxBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={Styles.text}>
							{ this.props.brand || '-' }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={Styles.text}>
							{ this.props.title || '-' }
						</GeomanistBit>
						{ this.props.price === this.props.retailPrice ? (
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.price}>
								IDR { FormatHelper.currencyFormat(this.props.price) || '-' }
							</GeomanistBit>
						) : (
							<BoxBit unflex row>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.discounted}>
									IDR {FormatHelper.currencyFormat(this.props.price) || '-'}
								</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.retail}>
									IDR {FormatHelper.currencyFormat(this.props.retailPrice) || '-'}
								</GeomanistBit>
							</BoxBit>
						) }
						<BoxBit />
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
							{ this.props.category } – Color: { this.props.color } – Size: { this.props.size }
						</GeomanistBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
