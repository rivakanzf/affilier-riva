import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	shipment: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingTop: 12,
		paddingBottom: 12,
		backgroundColor: Colors.solid.grey.palette(1),
		alignItems: 'center',
	},

	ship: {
		paddingRight: 8,
	},

	return: {
		color: Colors.green.palette(1),
		minWidth: 43,
	},

	id: {
		minWidth: 43,
	},

	divider: {
		width: 1,
		height: 32,
		marginTop: -3,
		marginBottom: -6,
		borderRightWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		borderStyle: 'solid',
		marginLeft: 7,
		marginRight: 8 - StyleSheet.hairlineWidth,
	},

	description: {
		flexBasis: '100%',
	},

	status: {
		paddingLeft: 8,
		flexBasis: '25%',
		overflow: 'hidden',
	},

	stats: {
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},

	icon: {
		marginTop: -4,
		marginBottom: -4,
	},


})
