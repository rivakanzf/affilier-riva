import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';
import StylesCapsule from './style.capsule';


export default ConnectHelper(
	class TabLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				capsule: PropTypes.bool,
				activeIndex: PropTypes.number,
				tabs: PropTypes.array,
				style: PropTypes.style,
				static: PropTypes.bool,
			}
		}

		static defaultProps = {
			activeIndex: 0,
		}

		constructor(p) {
			super(p, {
				style: p.capsule ?  StylesCapsule : Styles,
			})
		}

		shouldComponentUpdate(nP) {
			return nP.activeIndex !== this.state.activeIndex
		}

		tabRenderer = (tab, i) => {
			const isActive = this.props.static ? false : this.props.activeIndex === i

			return (
				<TouchableBit key={i} activeOpacity={1} accessible={ !tab.disabled } row unflex onPress={ tab.onPress } style={[ this.state.style.tab, isActive && this.state.style.tabActive, tab.disabled && this.state.style.disabled ]}>
					{ tab.icon && (
						<IconBit
							name={ tab.icon }
							size={20}
							color={ isActive ? Colors.primary : Colors.black.palette(2, .4) }
							style={{paddingRight: 8}}
						/>
					) }
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight={ this.props.capsule ? 'normal' : 'medium' } style={ isActive ? this.state.style.isActive : this.state.style.isInactive }>
						{ tab.title }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		view() {
			return (
				<BoxBit unflex row style={[this.state.style.container, this.props.style]}>
					{ this.props.tabs.map(this.tabRenderer) }
				</BoxBit>
			)
		}
	}
)
