import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		paddingTop: 16,
		paddingBottom: 16,
	},

	tabActive: {
		background: Colors.primary,
		marginBottom: -StyleSheet.hairlineWidth,
		zIndex: 1,
	},

	tab: {
		paddingLeft: 16,
		paddingRight: 16,
		paddingTop: 10,
		paddingBottom: 10,
		border: `1px solid ${Colors.primary}`,
		borderRadius: 4,
		marginRight: 8,
		textTransform: 'capitalize',
	},

	disabled: {
		opacity: .6,
	},

	isActive: {
		color: '#fff',
	},

	isInactive: {
		color: Colors.primary, // Colors.black.palette(2, .4),
	},
})
