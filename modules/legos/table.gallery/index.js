import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import GridBit from 'modules/bits/grid';
import LoaderBit from 'modules/bits/loader';

import ListLego from 'modules/legos/list';

import Styles from './style';


export default ConnectHelper(
	class TableGalleryLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				noBorder: PropTypes.bool,
				isSearching: PropTypes.bool,
				column: PropTypes.number,
				rows: PropTypes.array.isRequired,
				itemRenderer: PropTypes.func,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			column: 4,
			noBorder: false,
		}

		view() {
			return (
				<BoxBit style={this.props.style}>
					{ this.props.isSearching ? (
						<ListLego index={1} unflex={false} style={Styles.transparent}>
							<BoxBit centering>
								<LoaderBit />
							</BoxBit>
						</ListLego>
					) : this.props.rows.length && (
						<GridBit
							column={ this.props.column }
							data={ this.props.rows }
							renderItem={ this.props.itemRenderer }
							itemContainerStyle={ [Styles.item, this.props.noBorder ? Styles.noBorder : Styles.item] }
							contentContainerStyle={ this.props.contentContainerStyle }
						/>
					) || (
						<ListLego index={1} unflex={false} style={{padding: 0}}>
							<BoxBit centering style style={Styles.empty}>
								<GeomanistBit
									type={GeomanistBit.TYPES.PARAGRAPH_2}
									style={Styles.darkGrey60}
								>
									No Data
								</GeomanistBit>
							</BoxBit>
						</ListLego>
					) }
				</BoxBit>
			)
		}
	}
)
