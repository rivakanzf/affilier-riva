import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	pagination: {
		alignSelf: 'flex-start',
	},

	transparent: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	action: {
		marginBottom: 16,
		marginTop: 16,
	},

	inputContainer: {
		borderRadius: 4,
		overflow: 'hidden',
		height: 44,
		marginLeft: 16,
	},

	button: {
		height: 44,
	},

	icon: {
		marginRight: 8,
	},

	item: {
		marginBottom: 16,
	},

	row: {
		justifyContent: 'space-between',
		paddingLeft: 8,
		paddingRight: 8,
	},

	padder16: {
		paddingTop: 16,
	},
	padderBottom24: {
		marginBottom: 24,
	},

	dark90: {
		color: Colors.black.palette(1, .9),
	},
	result: {
		color: Colors.black.palette(2, .8),
		paddingLeft: 16,
		paddingTop: 6,
		paddingBottom: 6,
	},
	noBorder: {
		borderWidth: 0,
		padding: 0,
	},

	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},

	empty: {
		backgroundColor: Colors.solid.grey.palette(4),
	},
})
