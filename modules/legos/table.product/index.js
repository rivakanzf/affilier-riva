import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ImageBit from 'modules/bits/image';
import NavTextProductBit from 'modules/bits/nav.text.product';
import TextBit from 'modules/bits/text';

import BadgeStatusLego from 'modules/legos/badge.status';
import SelectStatusBadgeLego from 'modules/legos/select.status.badge';
import TableLego from 'modules/legos/table';

import Styles from './style';


export default ConnectHelper(
	class TableProductLego extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				unflex: PropTypes.bool,
				imageless: PropTypes.bool,
				orderDetails: PropTypes.array,
				onNavigateToOrderDetail: PropTypes.func,
				onChangeStatus: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			orderDetails: [],
		}

		header = [{
			title: 'Product',
			width: 2.8,
		}, {
			title: 'Status',
			width: 1,
		}, {
			title: 'Price',
			width: .9,
			align: 'right',
		}]

		onNavigateToDetail = (id, type, refId) => {
			this.props.onNavigateToOrderDetail &&
			this.props.onNavigateToOrderDetail(id, type, refId)
		}

		onChangeStatus = (id, status) => {
			this.props.onChangeStatus &&
			this.props.onChangeStatus(id, status)
		}

		rowRenderer = orderDetail => {
			return {
				data: [{
					children: (
						<BoxBit row style={ Styles.product }>
							{ this.props.imageless ? false : (
								<ImageBit overlay resizeMode={ ImageBit.TYPES.CONTAIN } broken={ !orderDetail.image } source={ orderDetail.image } style={Styles.image} />
							) }
							<BoxBit style={ Styles.box }>
								<BoxBit style={{ justifyContent: 'space-between' }} row>
									<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
										#OD-{ orderDetail.id }{ orderDetail.is_exchange ? (
											<TextBit style={ Styles.exchange }> EXCHANGE</TextBit>
										) : null}
									</GeomanistBit>
									{ !!orderDetail.source &&
										<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
											Source: { orderDetail.source }
										</GeomanistBit>
									}
								</BoxBit>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.title }>
									{ orderDetail.title }
								</GeomanistBit>
								<NavTextProductBit linkless
									id={ orderDetail.id }
									ref_id={ orderDetail.ref_id }
									type={ orderDetail.type }
									status={ orderDetail.item_status }
									description={ orderDetail.description }
								/>
							</BoxBit>
						</BoxBit>
					),
				}, {
					children: this.props.onChangeStatus ? (
						<SelectStatusBadgeLego tight isRequired type={ SelectStatusBadgeLego.TYPES.ORDER_DETAIL_STATUSES } unflex={ false } status={ orderDetail.status } onChange={ this.onChangeStatus.bind(this, orderDetail.id) } />
					) : (
						<BoxBit row style={ Styles.status }>
							<BadgeStatusLego unflex={ false } status={ orderDetail.status } />
						</BoxBit>
					),
				}, {
					title: `IDR ${ FormatHelper.currency(orderDetail.price) }`,
					style: Styles.price,
				}],
				onPress: this.onNavigateToDetail.bind(this, orderDetail.id, orderDetail.type, orderDetail.ref_id),
			}
		}

		view() {
			return (
				<TableLego unflex={ this.props.unflex } lastBorder={ false } alternating={ false }
					headers={ this.header }
					rows={ this.props.orderDetails.map(this.rowRenderer) }
					style={ [Styles.table, this.props.style] }
				/>
			)
		}
	}
)
