import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	header: {
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: 12,
		paddingRight: 12,
		// paddingRight: 21,
	},

	body: {
		paddingLeft: 12,
		paddingRight: 12,
		paddingTop: 6,
		paddingBottom: 6,
	},

	row: {
		justifyContent: 'center',
		wordBreak: 'break-all',
		paddingLeft: 4,
		paddingRight: 4,
		overflow: 'hidden',
	},

	empty: {
		backgroundColor: Colors.solid.grey.palette(4),
		borderBottomWidth: 0,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},
})
