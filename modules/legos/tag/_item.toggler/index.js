import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

export default ConnectHelper(
	class TagsLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				choice: PropTypes.array,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				productTags: p.productTags || [],
			}, [
				'titleRenderer',
			])
		}

		onToggle(id) {
			this.props.onToggle &&
			this.props.onToggle(id)
		}

		titleRenderer({id, title}, i) {
			const ownValue = this.props.tags.indexOf(id)
			const productValue = this.state.productTags && this.state.productTags.indexOf(id)
			const equalValue = ownValue > -1 && productValue > -1

			return (
				<TouchableBit unflex
					key={i}
					accessible={ productValue && productValue === -1 || false}
					onPress={this.onToggle.bind(this, id)}
					style={[Styles.tag, (productValue > -1 || equalValue) && Styles.parentBackground || ownValue > -1 && Styles.isActive]}
				>
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_3}
						weight="medium"
						style={[ (productValue > -1 || equalValue) && Styles.parentText || ownValue > -1 && Styles.white, Styles.darkGrey80 ]}
					>
						{ title }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		view() {
			return this.props.options && this.props.options.map(this.titleRenderer)
		}
	}
)
