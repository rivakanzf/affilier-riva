import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	dark90: {
		color: Colors.black.palette(1, .9),
	},
	darkGrey80: {
		// color: Colors.black.palette(2, .8),
		lineHeight: 24,
	},
	white: {
		color: Colors.white.primary,
	},
	tag: {
		paddingTop: 9,
		paddingBottom: 9,
		paddingLeft: 16,
		paddingRight: 16,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 26,
		marginRight: 8,
		marginTop: 8,
	},
	isActive: {
		backgroundColor: Colors.primary,
		borderColor: Colors.solid.grey.palette(2),
	},
	parentBackground: {
		borderColor: Colors.primary,
	},
	parentText: {
		color: Colors.primary,
	},
})
