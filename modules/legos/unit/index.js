import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';

import Styles from './style';


export default ConnectHelper(
	class UnitLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				left: PropTypes.bool,
				right: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		view() {
			return (
				<BoxBit unflex centering style={[Styles.container, this.props.left && Styles.left, this.props.right && Styles.right, this.props.style]}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>{ this.props.title }</GeomanistBit>
				</BoxBit>
			)
		}
	}
)
