import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	ok: {
		color: Colors.blue.primary,
	},
	cancel: {
		color: Colors.red.primary,
	},
	info: {
		color: Colors.grey.palette(1, .4),
	},
})
