import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'
import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: 32,
		paddingRight: 32,
		width: Styles.size.fluidWidth,
		// height: Styles.size.fluidWidth,
	},

	breadcrumb: {
		paddingBottom: 16,
	},

	title: {
		paddingTop: 12,
		paddingBottom: 12,
		textTransform: 'capitalize',
	},

	address: {
		marginTop: 32,
	},

	firstInput: {
		marginTop: 0,
	},

	button: {
		marginTop: 12,
	},

	rightButton: {
		marginLeft: 12,
	},

	inputContainerPhone: {
		paddingLeft: 16,
	},

	inputPhone: {
		paddingLeft: 0,
	},
})
