import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import ShipmentService from 'app/services/shipment';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ScrollViewBit from 'modules/bits/scroll.view';

import BoxLego from 'modules/legos/box';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

import { debounce, mapKeys, camelCase } from 'lodash'


export default ConnectHelper(
	class SelectShipmentPart extends PromiseStatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				invalidPostal: [],
				isSaving: false,
				isLoading: false,
				isDone: false,
			}, [
				'onError',
				'onClose',
				'onUpdate',
				'onSaveAddress',
				'footerRenderer',
				'checkInvalidPostal',
				'validateAndRetrieveArea',
			])

			this.data = {
				phone: p.phone,
				title: p.title,
				postal: p.postal,
				address: p.address,
				receiver: p.receiver,
				district: p.district,
			}

			this.validateAndRetrieveArea = debounce(this.validateAndRetrieveArea, 300)
		}

		onError(err) {
			this.setState({
				isSaving: false,
			}, () => {
				this.props.utilities.notification.show({
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		footerRenderer(i) {
			return (
				<BoxBit key={i} row style={Styles.footer}>
					<ButtonBit
						title="Cancel"
						weight="medium"
						type={ButtonBit.TYPES.SHAPES.RECTANGLE}
						size={ButtonBit.TYPES.SIZES.SMALL}
						onPress={this.onClose}
					/>
					<ButtonBit
						title="Edit"
						weight="medium"
						state={ this.state.isDone ? ButtonBit.TYPES.STATES.NORMAL : this.state.isSaving && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.DISABLED }
						type={ButtonBit.TYPES.SHAPES.PROGRESS}
						size={ButtonBit.TYPES.SIZES.SMALL}
						onPress={ this.onSaveAddress }
					/>
				</BoxBit>
			)
		}

		onClose() {
			this.props.utilities.alert.hide()
		}

		checkInvalidPostal(value) {
			return Promise.resolve(!this.state.invalidPostal.length || this.state.invalidPostal.indexOf(value) === -1)
		}

		onUpdate(isDone, {
			receiver,
			phone,
			address,
			postal,
			district,
		}) {

			if(postal.value !== this.data.postal && postal.isValid) {
				this.validateAndRetrieveArea(postal.value)
			}

			this.data = {
				...this.data,
				receiver: receiver.value,
				phone: phone.value,
				address: address.value,
				postal: postal.value,
				district: district.value,
			}

			if(this.state.isDone !== isDone) {
				this.setState({
					isDone,
				})
			}
		}

		validateAndRetrieveArea(postal) {
			ShipmentService.getAreas(postal).then(areas => {
				if(areas.length > 1) {
					this.props.utilities.menu.show({
						title: 'Select area',
						actions: areas.map(area => {
							return {
								title: area.area_name,
								type: this.props.utilities.menu.TYPES.ACTION.RADIO,
								onPress: this.saveDistrict.bind(this, area),
							}
						}),
					})
				} else if(areas.length === 1) {
					this.saveDistrict(areas[0])
				} else {
					throw new Error('invalid postal')
				}
			}).catch(() => {
				this.props.utilities.notification.show({
					message: 'Oops… Postal code invalid',
					timeout: 3000,
				})

				this.setState({
					invalidPostal: [
						...this.state.invalidPostal,
						postal,
					],
				})
			})
		}

		saveDistrict(area) {
			this.data = {
				...this.data,
				district: `${area.district}/${area.area}`,
				metadata: mapKeys(area, (value, key) => {
					return camelCase(key)
				}),
			}

			this.forceUpdate()
		}

		onSaveAddress() {
			// TODO: then must call onRequestClose
			this.setState({
				isSaving: true,
			}, () => {
				this.props.onChange &&
				this.props.onChange(this.data)

				this.onClose();
			})
		}

		view() {
			return (
				<BoxLego title="Edit Shipping Address" style={Styles.container} contentContainerStyle={Styles.content}>
					<ScrollViewBit unflex style={Styles.padder}>
						<FormPagelet
							key={ `${this.state.invalidPostal.length}${this.data.district}` }
							config={[{
								id: 'receiver',
								title: 'Full Name',
								placeholder: 'Enter full name',
								type: FormPagelet.TYPES.NAME,
								autogrow: false,
								unflex: true,
								required: true,
								value: this.data.receiver,
								style: [Styles.columnTop, Styles.padderRight],
							}, {
								id: 'phone',
								title: 'Phone Number',
								placeholder: 'Enter phone number',
								type: FormPagelet.TYPES.PHONE,
								required: true,
								unflex: true,
								autogrow: false,
								value: this.data.phone,
								style: [Styles.columnTop, Styles.padderLeft],
								inputStyle: Styles.phoneNumber,
							}, {
								id: 'address',
								title: 'Address',
								placeholder: 'Enter address',
								showCounter: true,
								maxlength: 500,
								type: FormPagelet.TYPES.TEXTAREA,
								autogrow: false,
								unflex: true,
								required: true,
								value: this.data.address,
								style: Styles.textAreaLayout,
							}, {
								id: 'postal',
								title: 'Postal Code',
								placeholder: 'Enter Postal Code',
								key: this.state.invalidPostal.length,
								required: true,
								type: FormPagelet.TYPES.POSTAL,
								validator: this.checkInvalidPostal,
								value: this.data.postal,
								style: [Styles.column, Styles.padderRight],
							}, {
								id: 'district',
								title: 'District (Kecamatan/Kelurahan)',
								key: this.data.district || 'autofill',
								disabled: true,
								required: true,
								placeholder: 'Autofill',
								type: FormPagelet.TYPES.TITLE,
								value: this.data.district,
								style: [Styles.column, Styles.padderLeft],
							}]}
							onUpdate={ this.onUpdate }
							// onSubmit={ this.onSaveAddress }
							style={Styles.form}
						/>
					</ScrollViewBit>
					{ this.footerRenderer() }
				</BoxLego>
			)
		}
	}
)
