import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ScrollViewBit from 'modules/bits/scroll.view';
import TouchableBit from 'modules/bits/touchable';
import LoaderBit from 'modules/bits/loader';


import ProductService from 'app/services/product';


import Styles from './style';


export default ConnectHelper(
	class ResultProductPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				search: PropTypes.string,
				onSelect: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static defaultProps = {
			search: '',
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				items: [],
			}, [
			])

			this._data = [{
				id: 'MBX - 001',
				brand: 'Yuna & Co.',
				name: 'Basic Matchbox',
			}]
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				ProductService.getAllProduct({
					search: this.props.search,
					variant: true,
					inventory: false,
				}, this.props.token).then(res => {
					this.setState({
						isLoading: false,
						products: res.data,
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
						products: [],
					})
				})
			})
		}

		componentDidMount() {
			this.getData()
		}

		componentDidUpdate(pP) {
			if(pP.search !== this.props.search) {
				this.getData()
			}
		}

		onPress = product => {
			this.props.onSelect &&
			this.props.onSelect(product, true, `${product.id} - ${product.brand} - ${product.title}`)

			this.props.onHide &&
			this.props.onHide()
		}

		onMouseEnter = i => {
			this.setState({
				hoverIndex: i,
			})
		}

		onMouseLeave = () => {
			this.setState({
				hoverIndex: undefined,
			})
		}

		loadingRenderer = () => {
			return (
				<BoxBit unflex row style={Styles.loader}>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={[Styles.darkGrey60, Styles.padderRight]}>
						Fetching result...
					</GeomanistBit>
					<LoaderBit simple size={20} />
				</BoxBit>
			);
		}

		emptyRenderer() {
			return (
				<BoxBit unflex row style={Styles.loader}>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.darkGrey60}>
						Currently have no data recorded.
					</GeomanistBit>
				</BoxBit>
			);
		}

		resultRenderer = (product, i) => {
			return (
				<TouchableBit key={i} row
					onMouseEnter={ this.onMouseEnter.bind(this, i) }
					onMouseLeave={ this.onMouseLeave.bind(this, i) }
					style={[i === this.state.hoverIndex ? Styles.isHovered : Styles.listContent, Styles.list]}
					onPress={ this.onPress.bind(this, product) }
				>
					<BoxBit unflex style={Styles.desc}>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={i === this.state.hoverIndex ? Styles.white : Styles.darkGrey}
						>
							#{product.id} — {product.brand} — {product.title}
						</GeomanistBit>
					</BoxBit>
					{ i === this.state.hoverIndex && (
						<TouchableBit unflex style={Styles.select}>
							<GeomanistBit
								type={GeomanistBit.TYPES.NOTE_1}
								weight="medium"
								style={Styles.white}
							>
								Select
							</GeomanistBit>
						</TouchableBit>
					) }
				</TouchableBit>
			)
		}

		view() {
			return this.state.isLoading ? this.loadingRenderer() : (
				<ScrollViewBit style={ Styles.searchBar }>
					{
						this.state.products.length === 0
							? this.emptyRenderer()
							: this.state.products.map(this.resultRenderer)
					}
				</ScrollViewBit>
			)
		}
	}
)
