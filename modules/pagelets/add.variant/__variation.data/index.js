import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import InputCurrencyBit from 'modules/bits/input.currency';
import TextInputBit from 'modules/bits/text.input'

import RowLego from 'modules/legos/row'
import RowColorLego from 'modules/legos/row.color'
import SelectSizeLego from 'modules/legos/select.size'

import VariantService from 'app/services/variant';

import UtilitiesContext from 'coeur/contexts/utilities';

import Styles from './style';

import HeaderPart from '../__header'

export default ConnectHelper(
	class VariationDataPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				selectedProductId: PropTypes.number,
				onAddNewVariant: PropTypes.func,
				closeForm: PropTypes.func,

				size: PropTypes.string,
				sizeId: PropTypes.number,
				colors: PropTypes.array,
				colorsIds: PropTypes.array,


			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				variantSave:{
					sku:null,
					url:null,
					size: p.sizeId,
					retailPrice:null,
					discountedPrice:null,
					colors: p.colorsIds,
				},
				variant:{
					sku:null,
					url:null,
					size:p.size,
					retailPrice:null,
					discountedPrice:null,
					colors:p.colors,
				},
				isLoading: false,
			}, [
			])
		}
		
		onChange = (key, e, val) => {
			this.setState((prevState) => ({
				variant:{
					...prevState.variant,
					[key]:val,
				},
				variantSave:{
					...prevState.variantSave,
					[key]:val,
				},
			}))

		}

		onChangePrice = (key, price) => {
			this.setState( (prevState) => ({
				variant:{
					...prevState.variant,
					[key]: price,
				},
				variantSave:{
					...prevState.variantSave,
					[key]: price,
				},


			}))
		}

		onChangeColors = (val, stringVal) => {
			this.setState( (prevState) => ({
				variant:{
					...prevState.variant,
					colors: stringVal,
				},
				variantSave:{
					...prevState.variantSave,
					colors: val,
				},
			}))
		}

		onChangeSize = (val, stringVal) => {
			this.setState( (prevState) => ({
				variant:{
					...prevState.variant,
					size: stringVal,
				},
				variantSave:{
					...prevState.variantSave,
					size: val,
				},
			}))
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail.message : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onSubmit = () => {
			this.setState({isLoading:true})
			VariantService.createVariant({
				product_id: this.props.selectedProductId,
				size_id: this.state.variantSave.size,
				color_ids: this.state.variantSave.colors,
				url: this.state.variantSave.url,
				sku: this.state.variantSave.sku,
				retail_price: this.state.variantSave.retailPrice,
				price: this.state.variantSave.discountedPrice,
			}, this.props.token)
				.then((res) => {
					
					const passingData = {
						id: res.id,
						asset:{
							url: this.state.variant.url,
						},
						size:{
							title: this.state.variant.size,
						},
						prices:[this.state.variant.discountedPrice, this.state.variant.retailPrice],
						colors:this.state.variant.colors.map(this.getColorObj),
					}
					this.setState({
						variantSave:{
							colors: [],
							size: null,
							sku:null,
							url:null,
							retailPrice:null,
							discountedPrice:null,
						},
						variant:{
							colors: [],
							size: null,
							sku:null,
							url:null,
							retailPrice:null,
							discountedPrice:null,
						},
					})
					this.setState({isLoading:false})
					this.props.onAddNewVariant(passingData)
					this.props.closeForm()
					
				})
				.catch((err) => {this.onError(err)})
		}

		getColorObj = (color) => {
			return {
				title:color,
			}
		}

		view() {
			return (
				<BoxBit>
					<HeaderPart title="Variation Data" style={Styles.header} />
					<BoxBit style={Styles.contentContainer}>
						<RowLego
							data={[{
								title: 'SKU',
								children: (
									<TextInputBit
										placeholder="Enter SKU"
										value={this.state.variantSave.sku}
										onChange={this.onChange.bind(this, 'sku')}
									/>
								),
							}, {
								title: 'URL',
								children: (
									<TextInputBit
										placeholder="Enter URL"
										value={this.state.variantSave.url}
										onChange={this.onChange.bind(this, 'url')}
									/>
								),
							}]}
						/>
						<RowColorLego
							onChange={this.onChangeColors}
							colorIds={ this.state.variantSave.colors }/>
						<RowLego
							data={[{
								title: 'Size',
								children: (
									<SelectSizeLego
										onChange={this.onChangeSize}
										sizeId={this.state.variantSave.size}
									/>
								),
							}, {
								title: '',
								children: (
									<BoxBit />
								),
							}]}
						/>
						<RowLego
							data={[{
								title: 'Retail Price',
								children: (
									<InputCurrencyBit
										placeholder="Enter price"
										onChange={ this.onChangePrice.bind(this, 'retailPrice')}
									/>
								),
							}, {
								title: 'Discounted Price',
								children: (
									<InputCurrencyBit
										placeholder="Enter price"
										onChange={ this.onChangePrice.bind(this, 'discountedPrice')}
										// value={this.state.variantSave.discountedPrice}
									/>
								),
							}]}
						/>
						<ButtonBit
							title="Save Variation"
							weight="medium"
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							size={ButtonBit.TYPES.SIZES.SMALL}
							style={Styles.button}
							state={ this.state.isLoading
								? ButtonBit.TYPES.STATES.LOADING
								: ButtonBit.TYPES.STATES.NORMAL
							}
							onPress={this.onSubmit}
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
