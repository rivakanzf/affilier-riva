import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'

import Styles from './style';


export default ConnectHelper(
	class FooterPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				index: PropTypes.number,
				title: PropTypes.string,
				onClose: PropTypes.func,
				onBack: PropTypes.func,
				onNext: PropTypes.func,
				disable: PropTypes.bool,
			}
		}

		view() {
			return (
				<BoxBit unflex row style={Styles.container}>
					<ButtonBit
						title="Cancel"
						weight="medium"
						size={ButtonBit.TYPES.SIZES.SMALL}
						style={Styles.button}
						onPress={this.props.onClose}
					/>
					<BoxBit />
					<ButtonBit
						title="Back"
						weight="medium"
						size={ButtonBit.TYPES.SIZES.SMALL}
						state={this.props.disable ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL}
						style={Styles.button}
						onPress={this.props.onBack}
					/>
					<BoxBit unflex style={Styles.divider} />
					<ButtonBit
						title={ this.props.index === 3 ? 'Close' : 'Next' }
						weight="medium"
						type={this.props.index === 3 ? ButtonBit.TYPES.SHAPES.PROGRESS : ButtonBit.TYPES.SHAPES.RECTANGLE}
						size={ButtonBit.TYPES.SIZES.SMALL}
						style={Styles.button}
						onPress={this.props.index === 3 ? this.props.onClose : this.props.onNext}
					/>
				</BoxBit>
			)
		}
	}
)
