import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		height: 56,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		justifyContent: 'center',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	divider: {
		width: 8,
	},

	button: {
		alignSelf: 'center',
	},
})
