import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import RadioBit from 'modules/bits/radio'

import ListLego from 'modules/legos/list'

import Styles from './style';

export default ConnectHelper(
	class ProductListPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				selectedProductId: PropTypes.number,
				id: PropTypes.number,
				brand: PropTypes.string,
				title: PropTypes.string,
				category: PropTypes.string,
				onPress: PropTypes.func,
			}
		}

		onPress = () =>{
			this.props.onPress &&
			this.props.onPress(this.props.id)
			
			this.props.onChangeSelected &&
			this.props.onChangeSelected(this.props.id)
		}
	 
		view() {
			return(
				<ListLego row
					onPress={this.onPress}
					style={Styles.containerList}
					key={this.props.id}
				>
					<BoxBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium">
							{`${this.props.brand} — ${this.props.title}`}
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.blueGrey}>
							{this.props.category}
						</GeomanistBit>
					</BoxBit>
					<RadioBit
						dumb
						isActive={ this.props.id === this.props.selectedProductId }
						style={ Styles.radio }
					/>
				</ListLego>
			)
			
		}

	})
