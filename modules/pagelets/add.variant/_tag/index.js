import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';


import ButtonBit from 'modules/bits/button';
import BoxBit from 'modules/bits/box';

import TagPagelet from 'modules/pagelets/tag'

import Styles from './style';


export default ConnectHelper(
	class TagPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				style: PropTypes.style,
				categoryId: PropTypes.number,
				productId: PropTypes.number,
				variantId: PropTypes.number,

				onSaveTag: PropTypes.func,
				isLoading: PropTypes.bool,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				tagIds: [],
			})

			
		}
		tag = [{
			header: {
				title: 'Fit - Bottom',
			},
			options: [{
				title: 'tes',
			}],
		}]

		onChange = (key, value) => {
			this.setState({
				tagIds: value,
			})
		}

		onSubmit = () => {
			this.props.onSaveTag(this.state.tagIds)
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					<TagPagelet
						categoryId= { this.props.categoryId }
						productId={ this.props.productId }
						variantId= { this.props.variantId }
						onChange={ this.onChange.bind(this, 'tagIds') }
					/>
					<ButtonBit
						title="Save tag"
						weight="medium"
						type={ButtonBit.TYPES.SHAPES.PROGRESS}
						size={ButtonBit.TYPES.SIZES.SMALL}
						style={Styles.button}
						state={ this.props.isLoading
							? ButtonBit.TYPES.STATES.LOADING
							: ButtonBit.TYPES.STATES.NORMAL
						}
						onPress={this.onSubmit}
					/>
				</BoxBit>
			)
		}
	}
)
