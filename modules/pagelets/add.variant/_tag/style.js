import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'utils/constants/color'

export default StyleSheet.create({

	container: {
		padding: Sizes.margin.default,
	},

	header: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	contentContainer: {
		padding: Sizes.margin.default,
	},
	
	inputFile: {
		height: 128,
		width: 96,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		backgroundColor: Colors.white.primary,
	},
})
