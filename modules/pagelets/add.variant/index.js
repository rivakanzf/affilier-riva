import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box'
import TouchableBit from 'modules/bits/touchable';

import TabStepLego from 'modules/legos/step'


import VariantService from 'app/services/variant';

import Styles from './style';

import FooterPart from './_footer'
import HeaderPart from './__header'
import ImagePart from './_image'
import ProductPart from './_product'
import TagPart from './_tag'
import VariationPart from './_variation'

import { isEmpty } from 'lodash'


export default ConnectHelper(
	class AddVariantComponent extends QueryStatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		static propTypes(PropTypes) {
			return {
				refresh: PropTypes.func,
				
				id: PropTypes.number,

				title:PropTypes.string,
				brand: PropTypes.string,
				brandId: PropTypes.number,
				category: PropTypes.string,
				categoryId: PropTypes.number,
				size: PropTypes.string,
				sizeId: PropTypes.number,
				colors: PropTypes.array,
				colorsIds: PropTypes.array,
				
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}
																			
		static propsToQuery(state, oP) {
			if(!!!oP.id) {
				return null
			}
			return [ `query {
				productById	(id: ${ oP.id }) {
					id
					title
					description
					brand { title }
					categoryId
          			category { title category { title } }
          			productTags { tag { id title tag { id title } } }
					variants (condition: { deletedAt: null }) {
						id note price retailPrice isPublished size { id title sizeId }
						variantColors(orderBy: ORDER_ASC) {
							color { hex image title }
						}
						variantAssets(orderBy: ORDER_ASC, condition: { deletedAt: null }) {
							url
              				metadata
						}
            			variantTags { tag { id title tag { id title } } }
						inventories(condition: { deletedAt: null, status: AVAILABLE }) { id }
					}
				}
			}`, {
				fetchPolicy: 'no-cache',
			}, state.me.token]
		}

		static getDerivedStateFromProps(nP, nS) {
			if(isEmpty(nP.data)) {
				return null
			} else {
				return {
					...nS,
					selectedProductId: nS.selectedProductId
						? nS.selectedProductId
						: nP.data.productById.id,
					products: isEmpty(nS.products) ? [{
						id: nP.data.productById.id,
						brand: nP.data.productById.brand.title,
						category: nP.data.productById.category.title,
						title: nP.data.productById.title,
						variants: nP.data.productById.variants.map(variant => {
							return {
								id: variant.id,
								asset: {
									url: variant.variantAssets.url,
								},
								size: {
									title: variant.size.title,
								},
								colors: variant.variantColors.map(varcol => {
									return{
										title:varcol.color.title,
									}
								}),

							}
						}),
					}] : nS.products,
				}
			}

		}

		constructor(p) {
			super(p, {
				index: 0,
				products:[],
				selectedProductId: null,
				selectedVariantId: null,
			}, [
			])
		}

		step = [{
			title: 'Select Product',
			count: '1',
		}, {
			title: 'Create Variation',
			count: '2',
		}, {
			title: 'Add Images',
			count: '3',
		}, {
			title: 'Add Tags',
			count: '4',
		}]

		getSelectedProduct = () => {
			return this.state.products.filter(product => product.id === this.state.selectedProductId )
		}

		onClose = () => {
			this.props.utilities.alert.close()

			this.props.refresh
			&& this.props.refresh()
		}

		onAddProducts = (product) => {
			this.setState({
				products: !!this.state.products.some(pd => pd.id === product.id) ? this.state.products : [...this.state.products, product],
				selectedProductId: product.id,
			})

		}

		onNext = () => {
			this.setState({
				index: this.state.index + 1,
			})
		}

		onBack = () => {
			this.setState({
				index: this.state.index - 1,
			})
		}

		onChange = (key, val) => {
			this.setState({
				[key]: val,
			})
		}

		onError = err => {
			this.warn(err)
			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onSaveTag = (tagIds) => {
			this.setState({
				isLoading:true,
			})
			VariantService.updateVariant(this.state.selectedVariantId, {
				tag_ids: tagIds,
			}, this.props.token).then(() => {
				this.props.utilities.notification.show({
					message: 'Tag saved',
					type: this.props.utilities.notification.TYPES.SUCCESS,
				})

				this.setState({
					isLoading:false,
				})
			}).catch(this.onError)

		}

		onAddNewVariant = (variant) => {
			
			const product = this.getSelectedProduct()[0]

			product.variants = product.variants.concat(variant)
			this.setState({
				selectedVariantId: variant.id,
			})

			this.forceUpdate()
		}

		onSelectProduct = (product) => {
			this.setState({
				products: !!this.state.products.some(pd => pd.id === product.id)
					? this.state.products : [...this.state.products, product],
				selectedProductId: product.id,
			})
		}

		onChangeSelected = ( productId ) => {
			this.setState({
				selectedProductId: productId,
			})
		}

		dataRenderer = () => {
			switch(this.state.index) {
			default:
			case 0:
				return (
					<ProductPart
						style={ Styles.content }
						products = { this.state.products }
						selectedProductId = { this.state.selectedProductId }
						onSelectProduct = { this.onSelectProduct }
						onChangeSelected = { this.onChangeSelected }

						title={ this.props.title }
						brand={ this.props.brand }
						brandId={ this.props.brandId }
						category={ this.props.category }
						categoryId={ this.props.categoryId }
						
					/>
				)
			case 1:
				return (
					<VariationPart style={Styles.content}
						product={this.getSelectedProduct()}
						selectedProductId={this.state.selectedProductId}
						onAddNewVariant={ this.onAddNewVariant }

						size={ this.props.size }
						sizeId={ this.props.sizeId }
						colors={ this.props.colors }
						colorsIds={ this.props.colorsIds }
					/>
				)
			case 2:
				return (
					<ImagePart
						style={Styles.content}
						selectedVariantId={this.state.selectedVariantId}
					/>
				)
			case 3:
				return (
					<TagPart
						style={Styles.content}
						categoryId= { this.props.categoryId }
						productId={ this.state.selectedProductId }
						variantId= { this.state.selectedVariantId }
						onSaveTag= { this.onSaveTag }
						isLoading= { this.state.isLoading }
					
					/>
				)
			}
		}

		view() {

			return (
				<BoxBit centering>
					<TouchableBit onPress={this.onClose} style={Styles.container} />
					<BoxBit unflex style={Styles.contentContainer}>
						<HeaderPart title="Add New Variant" />
						<TabStepLego activeIndex={ this.state.index } steps={this.step} />
						<BoxBit>
							{ this.dataRenderer() }
						</BoxBit>
						<FooterPart
							index={ this.state.index }
							onClose={this.onClose}
							onNext={this.onNext}
							onBack={this.onBack}
							isLoading={this.state.isLoading}
							disable={this.state.index === 0}
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
