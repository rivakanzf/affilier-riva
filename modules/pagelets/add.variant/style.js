import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		width: Sizes.app.width,
		height: Sizes.app.height,
		cursor: 'default',
		position: 'absolute',
	},

	contentContainer: {
		width: 592,
		minHeight: 592,
		borderRadius: 4,
		backgroundColor: Colors.white.primary,
	},

	content: {
		width: 592,
	},
})
