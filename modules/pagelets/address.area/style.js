import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	content: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},
	empty: {
		height: 240,
	},
	text: {
		color: Colors.black.palette(2, .6),
		marginLeft: 8,
	},
	header: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		alignItems: 'center',
		// justifyContent: 'space-between',
	},
	primary: {
		color: Colors.primary,
	},
	add: {
		marginTop: -16,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
	},
	adder: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
	},
	icon: {
		marginRight: Sizes.margin.default,
	},
	button: {
		justifyContent: 'center',
	},
})
