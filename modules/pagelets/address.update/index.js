import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import AddressUpdateComponent from 'modules/components/address.update';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';


export default ConnectHelper(
	class AddressUpdatePagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf([
					'user',
					'brand',
					'order',
				]).isRequired,
				typeId: PropTypes.id,
				id: PropTypes.id,
				title: PropTypes.string,
				cancel: PropTypes.string,
				confirm: PropTypes.string,
				onClose: PropTypes.func,
				onConfirm: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				isValid: false,
				isSaving: false,
				isChanged: false,
			})

			this.submit = null
		}

		getSubmitter = submitter => {
			this.submit = submitter
		}

		onConfirm = () => {
			if(this.submit) {
				this.setState({
					isSaving: true,
				}, () => {
					this.submit().then(address => {
						this.props.onConfirm &&
						this.props.onConfirm(address)

						this.setState({
							isSaving: false,
						})
					}).catch(() => {
						this.setState({
							isSaving: false,
						})
					})
				})
			}
		}

		onUpdate = (data, isValid, isChanged) => {
			if(this.state.isValid !== isValid || this.state.isChanged !== isChanged) {
				this.setState({
					isValid,
					isChanged,
				})
			}
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ this.props.title || ( !this.props.id ? 'Create Address' : 'Edit Address' ) }
					confirm={ this.props.confirm || 'Save' }
					cancel={ this.props.cancel || 'Cancel' }
					loading={ this.state.isSaving }
					disabled={ !this.state.isValid || !this.state.isChanged }
					onClose={ this.props.onClose }
					onCancel={ this.props.onClose }
					onConfirm={ this.onConfirm }
					contentContainerStyle={ Styles.content }
				>
					<AddressUpdateComponent
						getSubmitter={ this.getSubmitter }
						type={ this.props.type }
						typeId={ this.props.typeId }
						id={ this.props.id }
						onUpdate={ this.onUpdate }
						style={ Styles.add }
					/>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
