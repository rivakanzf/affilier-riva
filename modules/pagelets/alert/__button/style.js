import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	button: {
		paddingTop: 11,
		paddingBottom: 12,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 2,
		marginRight: 12,
	},
	text: {
		paddingTop: 0,
		paddingBottom: 0,
		paddingLeft: 0,
		paddingRight: 0,
	},
	buttonOk: {
		backgroundColor: Colors.primary,
	},
	buttonCancel: {
		backgroundColor: Colors.white.primary,
	},
	buttonInfo: {
		backgroundColor: Colors.white.primary,
	},
	ok: {
		color: Colors.white.primary,
	},
	cancel: {
		color: Colors.primary,
	},
	info: {
		color: Colors.primary,
	},
})
