import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import {
	DefaultPart as CoreDefaultPart,
} from 'coeur/modules/pagelets/alert'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import ButtonPart from '../__button';
import IconPart from '../__icon';
import RadioPart from '../__radio';

import Styles from './style'


export default ConnectHelper(
	class DefaultPart extends CoreDefaultPart({
		Styles,
		BoxBit,
		TextBit: GeomanistBit,
		ButtonPart,
		IconPart,
		RadioPart,
	}) {
		titleRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.HEADER_5} weight="semibold" style={style}>
					{ this.props.title }
				</GeomanistBit>
			)
		}

		messageRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={style}>
					{ this.props.message }
				</GeomanistBit>
			)
		}

		descriptionRenderer(style) {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={style}>
					{ this.props.description }
				</GeomanistBit>
			)
		}
	}
)
