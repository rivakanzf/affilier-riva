import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	pagination: {
		marginLeft: 16,
		// marginBottom: 16,
	},

	action: {
		marginBottom: 20,
	},

	inputContainer: {
		borderRadius: 4,
		overflow: 'hidden',
		flex: 5,
		height: 44,
	},

	icon: {
		marginLeft: 11,
	},

	header: {
		paddingTop: 16,
		paddingBottom: 16,
		// paddingRight: 21,
		paddingLeft: 8,
		paddingRight: 8,
	},

	padder: {
		paddingBottom: 16,
	},

	loading: {
		opacity: .3,
	},

	body: {
		paddingLeft: 8,
		paddingRight: 8,
	},

	row: {
		justifyContent: 'center',
		wordBreak: 'break-all',
		paddingLeft: 8,
		paddingRight: 8,
	},

	container: {
		flexWrap: 'wrap',
		justifyContent: 'space-between',
	},
	content: {
		paddingBottom: 0,
		marginBottom: 16,
		width: '49.2%',
	},
	contentHeader: {
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingBottom: 16,
	},

	tab: {
		paddingBottom: 16,
	},
	name: {
		color: Colors.black.palette(1, .9),
	},
	status: {
		paddingTop: 9,
		paddingBottom: 9,
		paddingLeft: 15,
		paddingRight: 15,
		color: Colors.black.palette(2, .8),
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderStyle: 'solid',
		borderRadius: 2,
		backgroundColor: Colors.solid.grey.palette(1),
	},
})
