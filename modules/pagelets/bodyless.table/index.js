import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import MeManager from '@yuna/utils/managers/me'
import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
// import CheckBoxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import ScrollViewBit from 'modules/bits/scroll.view';
import TextInputBit from 'modules/bits/text.input';

// import BadgeStatusLego from 'modules/legos/badge.status';
import ListLego from 'modules/legos/list';

import PaginationComponent from 'modules/components/pagination';

// import TableLego from 'modules/legos/table';

import Styles from './style';

import _ from 'lodash';

export default ConnectHelper(
	class QueryTableLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				paging: PropTypes.bool,
				current: PropTypes.number,
				total: PropTypes.number,
				onNavigationChange: PropTypes.func,

				searchable: PropTypes.bool,
				isSearching: PropTypes.bool,
				onSearch: PropTypes.func,
				action: PropTypes.node,
				filter: PropTypes.node,

				isLoading: PropTypes.bool,
				headers: PropTypes.array.isRequired,
				// rows: PropTypes.array.isRequired,
				headerKey: PropTypes.updater,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			paging: false,
			current: 0,
			total: 0,

			searchable: false,
		}

		constructor(p) {
			super(p, {
			}, [
				'onUpdateSearch',
				'onSearch',
			])

			this._searchValue = undefined
			this.onSearch = _.debounce(this.onSearch, 300)
		}

		onUpdateSearch(e, value) {
			this._searchValue = value
		}

		onSearch() {
			this.props.onSearch &&
			this.props.onSearch(this._searchValue)
		}

		view() {
			return (
				<BoxBit style={this.props.style}>
					<BoxBit unflex row style={Styles.padder}>
						{ this.props.filter }
						<BoxBit />
						{ this.props.searchable && (
							<TextInputBit
								onChange={ this.onUpdateSearch }
								onSubmitEditing={ this.onSearch }
								style={ Styles.inputContainer }
								inputStyle={ Styles.input }
								placeholder="Search orders"
								prefix={(
									<IconBit
										name="search"
										size={24}
										style={Styles.icon}
										color={Colors.black.palette(2, .6)}
									/>
								)}
								postfix={(
									<ButtonBit
										title="Search"
										weight="normal"
										type={ ButtonBit.TYPES.SHAPES.SEARCH }
										size={ ButtonBit.TYPES.SIZES.MEDIUM }
										width={ ButtonBit.TYPES.WIDTHS.FIT }
										state={ this.props.isSearching ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
										onPress={ this.onSearch }
									/>
								)}
							/>
						) }
					</BoxBit>
					<BoxBit unflex row style={this.props.searchable || this.props.paging ? Styles.action : undefined}>
						{ this.props.action }
						<BoxBit />
						{ this.props.paging && (
							<PaginationComponent
								current={ this.props.current }
								count={ this.props.count }
								total={ this.props.total || 0}
								onNavigationChange={ this.props.onNavigationChange }
								style={ Styles.pagination }
							/>
						) }
					</BoxBit>
					<ListLego key={this.props.headerKey} isHeader row style={Styles.header}>
						{ this.props.headers.map((header, i) => {
							return (
								<BoxBit key={i} style={[Styles.row, {
									flexGrow: header.width,
								}]}>
									{ header.children ? header.children : (
										<GeomanistBit type={GeomanistBit.TYPES.SECONDARY_2}>{ header.title }</GeomanistBit>
									) }
								</BoxBit>
							)
						}) }
					</ListLego>
					<ScrollViewBit>
						{ this.props.isLoading ? (
							<ListLego index={1} unflex={false}>
								<BoxBit centering>
									<LoaderBit />
								</BoxBit>
							</ListLego>
						) :  this.props.rows || (
							<ListLego index={1} unflex={false}>
								<BoxBit centering>
									<GeomanistBit>No Data</GeomanistBit>
								</BoxBit>
							</ListLego>
						) }
					</ScrollViewBit>
				</BoxBit>
			)
		}
	}
)
