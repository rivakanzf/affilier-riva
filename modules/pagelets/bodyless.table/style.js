import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	pagination: {
		marginLeft: 16,
		// marginBottom: 16,
	},

	action: {
		marginBottom: 20,
	},

	inputContainer: {
		borderRadius: 4,
		overflow: 'hidden',
		flex: 5,
		height: 44,
	},

	icon: {
		marginLeft: 11,
	},

	header: {
		paddingTop: 16,
		paddingBottom: 16,
		// paddingRight: 21,
		paddingLeft: 8,
		paddingRight: 8,
	},

	padder: {
		paddingBottom: 16,
	},

	loading: {
		opacity: .3,
	},

	body: {
		paddingLeft: 8,
		paddingRight: 8,
	},

	row: {
		justifyContent: 'center',
		wordBreak: 'break-all',
		paddingLeft: 8,
		paddingRight: 8,
	},
})
