import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'
import UtilitiesContext from 'coeur/contexts/utilities'


import CollectionService from 'app/services/collection';
import CouponService from 'app/services/coupon';
import ItemService from 'app/services/item';
import UserService from 'app/services/user.new';
import StylecardService from 'app/services/stylecard'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
import RadioBit from 'modules/bits/radio';
import TextBit from 'modules/bits/text';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import RowLego from 'modules/legos/row';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'

import Styles from './style';

import { debounce, isEqual } from 'lodash';


export default ConnectHelper(
	class AddPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				title: PropTypes.string,
				type: PropTypes.string,
				onChange: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [ UtilitiesContext ]

		static stateToProps(state) {
			return {
				token: state.me.token,
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist', 'stylist'),

			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isAdding: false,
				search: null,
				selectedId: -1,
				data: [],
			})
		}

		componentDidMount() {
			if(this.props.type && this.state.search) {
				this.getData()
			}
		}

		componentDidUpdate(pP, pS) {
			if(!isEqual(pS.search, this.state.search)) {
				this.getData()
			}
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				if(this.props.type === 'COUPON') {
					CouponService.getCoupons({
						limit: 15,
						offset: 0,
						search: this.state.search,
					}, this.props.token)
						.then(res => {
							this.setState({
								data: res.data,
								isLoading: false,
							})
						})
						.catch(() => {
							this.setState({
								data: [],
								isLoading: false,
							})
						})
				} else {
					ItemService.getAllItem({
						search: this.state.search,
						type: this.props.type,
					}, this.props.token)
						.then(res => {
							this.setState({
								data: res,
								isLoading: false,
							})
						})
						.catch(() => {
							this.setState({
								data: [],
								isLoading: false,
							})
						})
				}
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onSearch = debounce((e, val) => {
			this.setState({
				search: val,
			})
		}, 300)

		onAddItems = () => {

			const id = this.state.selectedId
			const data = this.state.data.find(d => d.id === id)

			this.setState({
				isAdding: true,
			}, () => {
				if (this.props.type === 'COUPON') {
					this.props.onChange &&
					this.props.onChange('coupon_id', id)

					this.onClose()
	
					this.props.onUpdate &&
					this.props.onUpdate({
						id: data.id,
						title: data.title,
					})
				} else {
					CollectionService.addItemToCollection(this.props.id, {
						itemId: id,
					}, this.props.token).then(() => {
						this.props.utilities.notification.show({
							title: 'Success',
							message: 'Item added to collection.',
							type: 'SUCCESS',
						})
	
						this.onClose()
	
						this.props.onUpdate &&
						this.props.onUpdate({
							id: data.id,
							title: data.title,
						})
					}).catch(err => {
						this.warn(err)
	
						this.setState({
							isLoading: false,
						})
	
						this.props.utilities.notification.show({
							title: 'Oops',
							message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
						})
					})
				}
			})
		}

		onSelect = id => {
			if(this.state.selectedId === id) {
				this.onAddItems()
			} else {
				this.setState({
					selectedId: id,
				})
			}
		}

		onDoublePress = id => {
			this.setState({
				selectedId: id,
			}, this.onAddItems)
		}

		listRenderer = item => {
			return (
				<TouchableBit key={ item.id } unflex row
					onDoublePress={ this.onDoublePress.bind(this, item.id) }
					onPress={ this.onSelect.bind(this, item.id) }
					style={ [Styles.item, this.state.selectedId === item.id && Styles.selected] }
				>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						<TextBit style={ Styles.id }>[#{ item.id }]</TextBit> { item.title || '-' }
					</GeomanistBit>
					<BoxBit />
					<RadioBit dumb isActive={ this.state.selectedId === item.id } style={ Styles.radio } />
				</TouchableBit>
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={`Add New ${ this.props.title }`}
					subheader={(
						<RowLego data={[{
							title: 'Product Title',
							children: (
								<TextInputBit defaultValue={ this.state.search } placeholder="Input here…" onChange={ this.onSearch } />
							),
						}, {
							blank: true,
						}]} style={Styles.header} />
					)}
					loading={ this.state.isAdding }
					disabled={ this.state.selectedId === -1 }
					confirm={ 'Add Into Collection'}
					onCancel={ this.onClose }
					onConfirm={ this.onAddItems }
					contentContainerStyle={ Styles.content }
				>
					{ /* eslint-disable-next-line no-nested-ternary */ }
					{ this.state.isLoading ? (
						<BoxBit centering>
							<LoaderBit simple />
						</BoxBit>
					) : this.state.data.length ? this.state.data.map(this.listRenderer) : (
						<BoxBit centering>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>No item found</GeomanistBit>
						</BoxBit>
					) }
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
