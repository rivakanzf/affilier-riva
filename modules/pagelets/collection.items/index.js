import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import StringHelper from 'coeur/helpers/string'
import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import CollectionService from 'app/services/collection';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';
import InputFileCustomBit from 'modules/bits/input.file.custom';

import BoxLego from 'modules/legos/box';

import AddPart from './_add';

import Styles from './style';

import { uniqBy } from 'lodash'


export default ConnectHelper(
	class CollectionItemsPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				// id: PropTypes.id,
				isValidForAll: PropTypes.bool,
				type: PropTypes.oneOf([
					'VARIANT',
					'COUPON',
				]).isRequired,
				onChange: PropTypes.func,
				style: PropTypes.style,
				onUploadDataLoaded: PropTypes.func,
				onUploadDataError: PropTypes.func,
				isUploading: PropTypes.bool,
			}
		}

		static contexts = [ UtilitiesContext ]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static defaultProps = {
			type: 'VARIANT',
		}

		constructor(p) {
			super(p, {
				readonly: p.readonly,
				isLoaded: false,
				isLoading: false,
				data: [],
			})
		}

		componentDidMount() {
			if(this.props.type) {
				this.getData()
			}
		}

		getData = () => {
			if (this.props.id) {
				this.setState({
					isLoading: true,
				}, () => {
					CollectionService.getCollection(this.props.id, this.props.token)
						.then(res => {
							if(res) {
								if (this.props.type === 'COUPON') {
									if (res.coupon) {
										this.setState({
											isLoading: false,
											data: res.coupon,
										})
									} else {
										this.setState({
											isLoading: false,
										})
									}
								} else {
									this.setState({
										isLoading: false,
										data: res.variants,
									})
								}
							}
						})
						.catch(this.onError)
				})
			}
		}

		title = () => {
			switch(this.props.type) {
			case 'VARIANT':
				return 'Variants'
			case 'COUPON':
				return 'Coupon'
			default:
				return this.props.type
			}
		}

		onError = err => {
			this.warn(err);

			this.setState({
				isLoading: false,
			}, () => {
				if(err && err.code !== 'ERR_101') {
					this.props.utilities.notification.show({
						title: 'Oops',
						message: 'Try again later',
					})
				}
			})
		}

		onAdd = () => {
			this.props.utilities.alert.modal({
				component: (
					<AddPart
						id={this.props.id}
						type={this.props.type}
						title={this.title()}
						onChange={ this.props.onChange }
						onUpdate={this.onAddedIntoCollection}
					/>
				),
			})
		}

		onRemove = item => {
			this.setState({
				isLoading: true,
			}, () => {
				if (this.props.type === 'COUPON') {
					this.props.onChange &&
					this.props.onChange('coupon_id', null)
					this.setState({
						isLoading: false,
						data: {},
					}, () => {
						// console.log('ini after remove', this.state.data)
					})
				} else {
					CollectionService.removeItemFromCollection(this.props.id, {
						itemId: item.id,
					}, this.props.token).then(res => {
						if(res) {
							this.props.utilities.notification.show({
								title: 'Removal Success',
								message: 'Success item.',
								type: 'SUCCESS',
							})
	
							this.setState({
								isLoading: false,
								data: this.state.data.filter(i => i.id !== item.id),
							})
						} else {
							throw new Error('No result, or deletion failed')
						}
					}).catch(err => {
						this.warn(err)
	
						this.props.utilities.notification.show({
							title: 'Oops…',
							message: `Something went wrong. (${ err && (err.detail || err.message) || err })`,
						})
	
						this.setState({
							isLoading: false,
						})
					})
				}
			})
		}

		onAddedIntoCollection = newItem => {
			if (this.props.type === 'COUPON') {
				this.setState({
					data: newItem,
				})
			} else {
				this.setState({
					data: uniqBy([...this.state.data, newItem], 'id'),
				})
			}
		}

		itemRenderer = item => {
			return (
				<BoxBit key={ item.id } unflex row style={ Styles.item }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						{ item.id ? (
							<TextBit style={ Styles.id }>[#{ item.id }]</TextBit>
						) : false } { item.title || '-' }
					</GeomanistBit>
					<BoxBit />
					{ item.id ? (
						<TouchableBit unflex centering onPress={ this.onRemove.bind(this, item) } style={ Styles.trash }>
							<IconBit
								name="close-fat"
								size={ 20 }
								color={ Colors.red.palette(3) }
							/>
						</TouchableBit>
					) : false }
				</BoxBit>
			)
		}

		couponRenderer = () => {
			return (
				<BoxBit unflex row style={ Styles.item }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
						{ this.state.data ? (
							<TextBit style={ Styles.id }>[#{ this.state.data.id }]</TextBit>
						) : false } { this.state.data.title || '-' }
					</GeomanistBit>
					<BoxBit />
					<TouchableBit unflex centering onPress={ this.onRemove.bind(this, this.state.data) } style={ Styles.trash }>
						<IconBit
							name="close-fat"
							size={ 20 }
							color={ Colors.red.palette(3) }
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		headerRenderer() {
			return (
				<BoxBit unflex row style={Styles.header}>
					{ this.props.type !== 'COUPON' ?
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={ Styles.note } >
							{ this.props.isValidForAll ? '' : StringHelper.pluralize(this.state.data.length, this.props.type === 'USER' ? 'user' : 'item', '', 's') }
						</GeomanistBit> : false
					}
					<ButtonBit
						title={ `Add ${ this.props.type === 'USER' ? 'User' : 'Item' }` }
						weight="medium"
						size={ ButtonBit.TYPES.SIZES.SMALL }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						state={ this.props.type === 'COUPON' && this.state.data.id ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onAdd }
						style={ Styles.button }
					/>
				</BoxBit>
			)
		}

		emptyRenderer() {
			return (
				<BoxBit unflex centering style={Styles.empty}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.text}>
						No Data
					</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxLego
					title={ this.title() }
					header={ this.headerRenderer() }
					style={ this.props.style }
					contentContainerStyle={ Styles.content }
				>
					<BoxBit unflex centering style={ Styles.empty }>
						<LoaderBit simple />
					</BoxBit>
				</BoxLego>
			)
		}

		view() {
			return this.state.isLoading ? this.viewOnLoading() : (
				<BoxLego
					title={ this.title() }
					header={ this.headerRenderer() }
					style={ this.props.style }
					contentContainerStyle={ Styles.content }
				>
					{ this.props.type !== 'COUPON' && (
						<InputFileCustomBit
							key={ this.props.inputFileKey }
							title="Add via CSV"
							loading={ this.props.isUploading }
							onDataLoaded={ this.props.onUploadDataLoaded }
							onDataError={ this.props.onUploadDataError }
							style={{ marginBottom: 15}}
						/>
					)}
					
					{ this.props.type === 'COUPON' ?
						<BoxBit unflex style={ Styles.padder }>
							{this.state.data.id ? this.couponRenderer() : this.emptyRenderer()}
						</BoxBit> :
						<BoxBit unflex style={ Styles.padder }>
							{ /* eslint-disable-next-line no-nested-ternary */ }
							{ this.props.isValidForAll
								? this.itemRenderer({ title: `All ${this.title()}` })
								: this.state.data.length
									? this.state.data.map(this.itemRenderer)
									: this.emptyRenderer()
							}
						</BoxBit>
					}
				</BoxLego>
			)
		}
	}
)
