import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import StringHelper from 'coeur/helpers/string'
import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import CouponService from 'app/services/coupon';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';
import TextInputBit from 'modules/bits/text.input';

import BoxLego from 'modules/legos/box';

import AddPart from './_add';

import Styles from './style';

import { uniqBy } from 'lodash'


export default ConnectHelper(
	class CouponItemsPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				isValidForAll: PropTypes.bool,
				type: PropTypes.oneOf([
					'MATCHBOX',
					'USER',
					'PRODUCT',
					'VARIANT',
					'SERVICE',
					'STYLECARD',
				]).isRequired,
				style: PropTypes.style,
			}
		}

		static contexts = [ UtilitiesContext ]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static defaultProps = {
			type: 'MATCHBOX',
		}

		constructor(p) {
			super(p, {
				readonly: p.readonly,
				isLoaded: false,
				isLoading: false,
				data: [],
			})
		}

		componentDidMount() {
			if(this.props.type) {
				this.getData()
			}
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				CouponService.getDetailOfType({
					id: this.props.id,
					type: this.props.type.toLowerCase(),
				}, this.props.token)
					.then(res => {
						if(res) {
							this.setState({
								isLoading: false,
								data: res,
							})
						}
					})
					.catch(this.onError)
			})
		}

		title = () => {
			switch(this.props.type) {
			case 'MATCHBOX':
				return 'Matchboxes'
			case 'PRODUCT':
				return 'Products'
			case 'USER':
				return 'Users'
			case 'SERVICE':
				return 'Services'
			case 'VARIANT':
				return 'Variants'
			case 'STYLECARD':
				return 'Stylecards'
			default:
				return this.props.type
			}
		}

		onError = err => {
			this.warn(err);

			this.setState({
				isLoading: false,
			}, () => {
				if(err && err.code !== 'ERR_101') {
					this.props.utilities.notification.show({
						title: 'Oops',
						message: 'Try again later',
					})
				}
			})
		}

		onAdd = () => {
			this.props.utilities.alert.modal({
				component: (
					<AddPart
						id={this.props.id}
						type={this.props.type}
						title={this.state.title}
						onUpdate={this.onAddedIntoCoupon}
					/>
				),
			})
		}

		onRemove = item => {
			this.setState({
				isLoading: true,
			}, () => {
				CouponService.removeItemFromCoupon(this.props.id, {
					type: this.props.type.toLowerCase(),
					typeId: item.id,
				}, this.props.token).then(res => {
					if(res) {
						this.props.utilities.notification.show({
							title: 'Removal Success',
							message: 'Success item.',
							type: 'SUCCESS',
						})

						this.setState({
							isLoading: false,
							data: this.state.data.filter(i => i.id !== item.id),
						})
					} else {
						throw new Error('No result, or deletion failed')
					}
				}).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops…',
						message: `Something went wrong. (${ err && (err.detail || err.message) || err })`,
					})

					this.setState({
						isLoading: false,
					})
				})
			})
		}

		onAddedIntoCoupon = newItem => {
			this.setState({
				data: uniqBy([...this.state.data, newItem], 'id'),
			})
		}

		itemRenderer = item => {
			return (
				<BoxBit key={ item.id } row>
					<BoxBit row style={ Styles.item }>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							{ item.id ? (
								<TextBit style={ Styles.id }>[#{ item.id }]</TextBit>
							) : false } { item.title || '-' }
						</GeomanistBit>
						{ this.props.type === 'PRODUCT' || this.props.type === 'VARIANT' && (
							<BoxBit row unflex>
								{ item.id ? (<>
									<TextInputBit
										placeholder="Variative Price"
										style={ Styles.variativePrice }
										onChange={ () => {}}
									/>
									<TouchableBit unflex centering onPress={ this.onRemove.bind(this, item) } style={ Styles.trash }>
										<IconBit
											name="close-fat"
											size={ 20 }
											color={ Colors.red.palette(3) }
										/>
									</TouchableBit>
								</>) : false }
							</BoxBit>
						)}
					</BoxBit>
				</BoxBit>
			)
		}

		headerRenderer() {
			return (
				<BoxBit unflex row style={Styles.header}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={ Styles.note } >
						{ this.props.isValidForAll ? '' : StringHelper.pluralize(this.state.data.length, this.props.type === 'USER' ? 'user' : 'item', '', 's') }
					</GeomanistBit>
					<ButtonBit
						title={ `Add ${ this.props.type === 'USER' ? 'User' : 'Item' }` }
						weight="medium"
						size={ ButtonBit.TYPES.SIZES.SMALL }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						state={ this.props.isValidForAll ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onAdd }
						style={ Styles.button }
					/>
				</BoxBit>
			)
		}

		emptyRenderer() {
			return (
				<BoxBit unflex centering style={Styles.empty}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.text}>
						No Data
					</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxLego
					title={ `Eligible ${this.title()}` }
					header={ this.headerRenderer() }
					style={ this.props.style }
					contentContainerStyle={ Styles.content }
				>
					<BoxBit unflex centering style={ Styles.empty }>
						<LoaderBit simple />
					</BoxBit>
				</BoxLego>
			)
		}

		view() {
			return this.state.isLoading ? this.viewOnLoading() : (
				<BoxLego
					title={ `Eligible ${this.title()}` }
					header={ this.headerRenderer() }
					style={ this.props.style }
					contentContainerStyle={ Styles.content }
				>
					<BoxBit unflex style={ Styles.padder }>
						{ /* eslint-disable-next-line no-nested-ternary */ }
						{ this.props.isValidForAll
							? this.itemRenderer({ title: `All ${this.title()}` })
							: this.state.data.length
								? this.state.data.map(this.itemRenderer)
								: this.emptyRenderer()
						}
					</BoxBit>
				</BoxLego>
			)
		}
	}
)
