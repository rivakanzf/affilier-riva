import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BrandManager from 'app/managers/brand';
import InventoryManager from 'app/managers/inventory';
import PickupPointManager from 'app/managers/pickup.point';
import ProductManager from 'app/managers/product';
import VariantManager from 'app/managers/variant';

import UtilitiesContext from 'coeur/contexts/utilities';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';
// import StringHelper from 'coeur/helpers/string';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import BoxLego from 'modules/legos/box';
import BoxRowLego from 'modules/legos/box.row';
import TableLego from 'modules/legos/table';

import CardMediaComponent from 'modules/components/card.media';

import DetailPagelet from 'modules/pagelets/detail'
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class EditInventoryPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				variantId: PropTypes.id.isRequired,

				// is required
				id: PropTypes.id,
				// or if not provided
				pickupPointId: PropTypes.id,

				onRequestClose: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, inventory = id && InventoryManager.get(id) || new InventoryManager.record({
					statuses: [new InventoryManager._recordFactory.statusRecord({
						code: 'READY',
					})],
				})
				, pickupPointId = inventory.pickupPointId || oP.pickupPointId
				, pickupPoint = pickupPointId && PickupPointManager.get(pickupPointId) || new PickupPointManager.record()

				, variantId = oP.variantId
				, variant = variantId && VariantManager.get(variantId) || new VariantManager.record()
				, product = variant.productId && ProductManager.get(variant.productId) || new ProductManager.record()
				, brand = product.brandId && BrandManager.get(product.brandId) || new BrandManager.record()

			return {
				inventory,
				pickupPoint,
				variant,
				product,
				brand,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.inventory._isGetting !== nS.isGettingInventory) {
				return {
					isGettingInventory: nP.inventory._isGetting,
					data: {
						...nS.data,
						purchasePrice: nP.inventory.purchasePrice,
						note: nP.inventory.note,
						statusCode: nP.inventory.status && nP.inventory.status.code,
						statusNote: nP.inventory.status && nP.inventory.status.note,
					},
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				isGettingInventory: p.inventory._isGetting,
				data: {
					purchasePrice: p.inventory.purchasePrice,
					note: p.inventory.note,
					statusCode: p.inventory.status && p.inventory.status.code,
					statusNote: p.inventory.status && p.inventory.status.note,
					qty: '1',
				},
				isDone: false,
				isLoading: false,
			}, [
				'onUpdate',
				'onUpdateStatus',
				'onSubmit',
				'onSetOwner',
				'onSuccess',
				'onError',
				'onCancel',
			])

			this._statuses = [{
				key: 'READY',
				title: 'Ready',
				onPress: this.onUpdateStatus.bind(this, 'READY'),
			}, {
				key: 'TAKEN',
				title: 'Not Available',
				onPress: this.onUpdateStatus.bind(this, 'TAKEN'),
			}, {
				key: 'BOOKED',
				title: 'Booked',
				onPress: this.onUpdateStatus.bind(this, 'BOOKED'),
			}, {
				key: 'CLEANING',
				title: 'Cleaning',
				onPress: this.onUpdateStatus.bind(this, 'CLEANING'),
			}, {
				key: 'DEFECT',
				title: 'Defect',
				onPress: this.onUpdateStatus.bind(this, 'DEFECT'),
			}]
		}

		onUpdate(isDone, {
			purchasePrice,
			note,
			// statusCode,
			statusNote,
			qty,
		}) {
			this.setState({
				isDone,
				data: {
					...this.state.data,
					purchasePrice: purchasePrice.value,
					note: note.value,
					statusNote: statusNote.value,
					// statusCode: statusCode.value,
					qty: qty && qty.value || '1',
				},
			})
		}

		onUpdateStatus(code) {
			this.setState({
				data: {
					...this.state.data,
					statusCode: code,
				},
			})
		}

		onSubmit() {
			this.setState({
				isLoading: true,
			}, () => {
				const {
					purchasePrice,
					note,
					statusCode,
					statusNote,
					qty,
				} = this.state.data

				if(this.props.id) {
					// UPDATE
					InventoryManager.updateInventory({
						id: this.props.id,
						purchasePrice,
						note,
					}).then(inventory => {
						if(statusCode && statusCode !== inventory.status.code) {
							InventoryManager.updateStatus({
								id: this.props.id,
								code: statusCode,
								note: statusNote,
							}).then(this.onSuccess).catch(this.onError)
						} else {
							this.onSuccess(inventory)
						}
					}).catch(this.onError)
				} else {
					// CREATE
					InventoryManager.createInventories(new Array(Math.max(qty, 1)).fill({
						pickupPointId: this.props.pickupPointId,
						purchasePrice,
						note,
						statuses: [{
							code: statusCode,
							note: statusNote,
						}],
					})).then(this.onSetOwner).catch(this.onError)
				}
			})
		}

		onSetOwner(inventories) {
			VariantManager.addVariantInventories({
				id: this.props.variantId,
				inventoryIds: inventories.map(inventory => inventory.id),
			}).then(this.onSuccess).catch(this.onError)
		}

		onSuccess(inventoriesOrInventory) {
			this.props.utilities.notification.show({
				title: 'Congrats',
				message: 'Inventory created / updated.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.setState({
				isLoading: false,
			})

			this.props.onUpdate &&
			this.props.onUpdate(inventoriesOrInventory)

			this.onCancel()
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onCancel() {
			if(this.props.onRequestClose) {
				this.props.onRequestClose()
			} else {
				this.props.utilities.alert.hide()
			}
		}

		footerRenderer() {
			return (
				<BoxBit unflex row>
					<ButtonBit
						title={ 'CANCEL' }
						type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
						width={ ButtonBit.TYPES.WIDTHS.FIT }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onCancel }
					/>
					<ButtonBit
						title={ 'SAVE' }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isDone && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
						onPress={ this.onSubmit }
						style={ Styles.rightButton }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<DetailPagelet
					paths={ this.props.paths }
					footer={ this.footerRenderer() }
				>
					<BoxRowLego
						title="General"
						data={[{
							data: [{
								title: 'Pickup Point',
								content: this.props.pickupPoint.title,
							}],
						}, {
							data: [{
								title: 'Brand',
								content: this.props.brand.title,
							}],
						}, {
							data: [{
								title: 'Product',
								content: this.props.product.title || '-',
							}, {
								title: 'Variant (Color - Size)',
								content: `${this.props.variant.color} - ${this.props.variant.size}`,
							}],
						}, {
							data: [{
								title: 'Retail Price',
								content: FormatHelper.currencyFormat(this.props.variant.basePrice) || '-',
							}, {
								title: 'Discounted Price',
								content: FormatHelper.currencyFormat(this.props.variant.price) || '-',
							}],
						}, {
							data: [{
								title: 'Images',
								children: this.props.variant.mediaIds.length ? (
									<BoxBit unflex row style={Styles.images}>
										{this.props.variant.mediaIds.map(mediaId => {
											return (
												<CardMediaComponent
													id={mediaId}
													key={mediaId}
													style={Styles.media}
												/>
											)
										})}
									</BoxBit>
								) : undefined,
								content: '-',
							}],
						}]}
						contentContainerStyle={Styles.box}
					/>
					<BoxLego
						title="Inventory Detail"
						contentContainerStyle={Styles.box}>
						<FormPagelet
							config={[{
								title: 'Purchase Price',
								id: 'purchasePrice',
								required: true,
								autofocus: true,
								type: FormPagelet.TYPES.CURRENCY,
								value: this.state.data.purchasePrice ? this.state.data.purchasePrice + '' : undefined,
							}, {
								title: 'Note',
								id: 'note',
								type: FormPagelet.TYPES.FREE_TAREA,
								value: this.state.data.note,
							}].concat(this.props.id ? [] : [{
								title: 'Quantity',
								id: 'qty',
								type: FormPagelet.TYPES.CURRENCY,
								value: this.state.data.qty,
							}]).concat([{
								title: 'Status',
								id: 'statusCode',
								type: FormPagelet.TYPES.SELECTION,
								value: this.state.data.statusCode,
								options: this._statuses,
							}, {
								title: 'Status Note',
								id: 'statusNote',
								type: FormPagelet.TYPES.FREE_TAREA,
								value: this.state.data.statusNote,
							}])}
							onUpdate={ this.onUpdate }
							onSubmit={ this.onSubmit }
						/>
					</BoxLego>
					{ this.props.id ? (
						<BoxLego
							title="Status History"
							contentContainerStyle={ Styles.statuses }>
							<TableLego
								headers={[{
									title: 'Status',
									width: 4,
								}, {
									title: 'Note',
									width: 8,
								}, {
									title: 'Issued At',
									width: 2,
								}]}
								rows={ this.props.inventory.statuses.slice().reverse().map(status => {
									return {
										data: [{
											title: status.code,
										}, {
											title: status.note,
										}, {
											title: TimeHelper.moment(status.createdAt).format('DD MMM YYYY [at] HH:mm:ss'),
										}],
									}
								}) }
							/>
						</BoxLego>
					) : false }
				</DetailPagelet>
			)
		}
	}
)
