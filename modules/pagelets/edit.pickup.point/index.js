import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import AddressManager from 'app/managers/address';
import BrandManager from 'app/managers/brand';
import PickupPointManager from 'app/managers/pickup.point';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import BoxLego from 'modules/legos/box';

import DetailPagelet from 'modules/pagelets/detail'
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

import _ from 'lodash';


export default ConnectHelper(
	class EditPickupPointPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				brandId: PropTypes.id.isRequired,
				onRequestClose: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, brandId = oP.brandId
				, pickupPoint = id && PickupPointManager.get(id) || new PickupPointManager.record()
				, address = pickupPoint.addressId && AddressManager.get(pickupPoint.addressId) || new AddressManager.record()
				, brand = brandId && BrandManager.get(brandId) || new BrandManager.record()

			return {
				address,
				brand,
				pickupPoint,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.pickupPoint._isGetting !== nS.isGettingPickupPoint) {
				return {
					isGettingPickupPoint: nP.pickupPoint._isGetting,
					data: {
						...nS.data,
						title: nP.pickupPoint.title,
						description: nP.pickupPoint.description,
					},
				}
			} else if(nP.address._isGetting !== nS.isGettingAddress) {
				return {
					isGettingAddress: nP.address._isGetting,
					data: {
						...nS.data,
						pic: nP.address.pic,
						phone: nP.address.phone,
						address: nP.address.address,
						district: nP.address.district,
						postal: nP.address.postal,
					},
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				isGettingPickupPoint: p.pickupPoint._isGetting,
				isGettingAddress: p.address._isGetting,
				data: {
					title: p.pickupPoint.title,
					description: p.pickupPoint.description,
					pic: p.address.pic,
					phone: p.address.phone,
					address: p.address.address,
					district: p.address.district,
					postal: p.address.postal,
				},
				isDone: {
					address: false,
					pickupPoint: false,
				},
				isLoading: false,
				invalidPostal: [],
			}, [
				'invalidPostalLength',
				'checkInvalidPostal',
				'validateAndRetrieveArea',
				'saveDistrict',
				'onUpdate',
				'onSubmit',
				'onSuccess',
				'onError',
				'onCancel',
			])
		}

		invalidPostalLength() {
			return this.state.invalidPostal.length
		}

		checkInvalidPostal(value) {
			return !this.state.invalidPostal.length || this.state.invalidPostal.indexOf(value) === -1
		}

		validateAndRetrieveArea(postal) {
			AddressManager.validatePostalAndRetrieveAreas(postal).then(areas => {
				if(areas.length > 1) {
					this.props.utilities.menu.show({
						title: 'Select area',
						actions: areas.map(area => {
							return {
								title: area.area_name,
								type: this.props.utilities.menu.TYPES.ACTION.RADIO,
								onPress: this.saveDistrict.bind(this, area),
							}
						}),
						closeOnPress: true,
					})
				} else if(areas.length === 1) {
					this.saveDistrict(areas[0])
				} else {
					throw new Error('invalid postal')
				}
			}).catch(() => {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Postal code invalid',
					timeout: 3000,
				})

				this.setState({
					invalidPostal: [
						...this.state.invalidPostal,
						postal,
					],
				})
			})
		}

		saveDistrict(area) {
			this.setState({
				data: {
					...this.state.data,
					district: `${area.suburb_name}/${area.area_name}`,
					metadata: _.mapKeys(area, (value, key) => {
						return _.camelCase(key)
					}),
				},
			})
		}

		onUpdate(isDone, {
			title,
			description,
			pic,
			phone,
			address,
			district,
			postal,
		}) {

			const _postal = postal ? postal.value : this.state.data.postal
			let _district = district ? district.value : this.state.data.district,
				_isDone = isDone

			if(_postal !== this.state.data.postal && postal.isValid) {
				_district = null
				_isDone = false
				this.validateAndRetrieveArea(postal.value)
			}

			this.setState({
				isDone: {
					...this.state.isDone,
					[ title ? 'pickupPoint' : 'address' ]: _isDone,
				},
				data: {
					title: title ? title.value : this.state.data.title,
					description: description ? description.value : this.state.data.description,
					pic: pic ? pic.value : this.state.data.pic,
					phone: phone ? phone.value : this.state.data.phone,
					address: address ? address.value : this.state.data.address,
					district: _district,
					postal: _postal,
				},
			})
		}

		onSubmit() {
			this.setState({
				isLoading: true,
			}, () => {
				const {
					title,
					description,
					pic,
					phone,
					address,
					district,
					postal,
				} = this.state.data

				// if(this.props.pickupPoint.id) {
				// }

				if(this.props.id) {
					if(this.props.address.id) {
						AddressManager.updateAddress({
							id: this.props.address.id,
							pic,
							phone,
							address,
							district,
							postal,
						}).then(this.onSubmitPickupPoint).catch(this.onError)
					} else {
						AddressManager.createAddress({
							pic,
							phone,
							address,
							district,
							postal,
						}).then(this.onSubmitPickupPoint).catch(this.onError)
					}
				} else {
					BrandManager.createPickupPoint({
						id: this.props.brandId,
						title,
						description,
						pic,
						phone,
						address,
						district,
						postal,
					}).then(this.onSuccess).catch(this.onError)
				}
			})
		}

		onSubmitPickupPoint(address) {
			if(!this.props.pickupPoint.id) {
				PickupPointManager.createPickupPoint({
					addressId: address.id,
					title: this.state.data.title,
					description: this.state.data.description,
				}).then(this.onSuccess).catch(this.onError)
			} else {
				PickupPointManager.updatePickupPoint({
					id: this.props.pickupPoint.id,
					addressId: address.id,
					title: this.state.data.title,
					description: this.state.data.description,
				}).then(this.onSuccess).catch(this.onError)
			}
		}

		onSuccess() {
			this.props.utilities.notification.show({
				title: 'Congrats',
				message: 'Pickup point updated.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.setState({
				isLoading: false,
			})

			this.onCancel()
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onCancel() {
			if(this.props.onRequestClose) {
				this.props.onRequestClose()
			} else {
				this.props.utilities.alert.hide()
			}
		}

		footerRenderer() {
			return (
				<BoxBit unflex row>
					<ButtonBit
						title={ 'CANCEL' }
						type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
						width={ ButtonBit.TYPES.WIDTHS.FIT }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onCancel }
					/>
					<ButtonBit
						title={ 'SAVE' }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isDone.address && this.state.isDone.pickupPoint && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
						onPress={ this.onSubmit }
						style={ Styles.rightButton }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<DetailPagelet
					paths={ this.props.paths }
					footer={ this.footerRenderer() }
				>
					<BoxLego
						title="Pickup Point"
						contentContainerStyle={Styles.box}>
						<FormPagelet
							config={[{
								title: 'Title',
								required: true,
								id: 'title',
								type: FormPagelet.TYPES.TITLE,
								value: this.state.data.title,
							}, {
								title: 'Description',
								id: 'description',
								type: FormPagelet.TYPES.FREE_TAREA,
								value: this.state.data.description,
							}]}
							onUpdate={ this.onUpdate }
						/>
					</BoxLego>
					<BoxLego
						title="Address Detail">
						<FormPagelet
							key={ this.state.data.district || this.invalidPostalLength() }
							config={[{
								title: 'Receiver Name',
								id: 'pic',
								type: FormPagelet.TYPES.NAME,
								value: this.state.data.pic,
								placeholder: this.state.data.title,
							}, {
								title: 'Phone',
								id: 'phone',
								type: FormPagelet.TYPES.PHONE,
								value: this.state.data.phone,
								inputContainerStyle: Styles.inputContainerPhone,
								inputStyle: Styles.inputPhone,
							}, {
								title: 'Address',
								required: true,
								id: 'address',
								type: FormPagelet.TYPES.ADDRESS,
								value: this.state.data.address,
							}, {
								title: 'District (Kecamatan / Kelurahan)',
								id: 'district',
								required: true,
								disabled: true,
								placeholder: 'Autofill',
								type: FormPagelet.TYPES.TITLE,
								value: this.state.data.district,
							}, {
								title: 'Postal Code',
								id: 'postal',
								required: true,
								type: FormPagelet.TYPES.POSTAL,
								validator: this.checkInvalidPostal,
								value: this.state.data.postal,
							}]}
							onUpdate={ this.onUpdate }
							onSubmit={ this.onSubmit }
						/>
					</BoxLego>
				</DetailPagelet>
			)
		}
	}
)
