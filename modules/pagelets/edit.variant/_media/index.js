import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import MediaManager from 'app/managers/me';
// import PickupPointManager from 'app/managers/pickup.point';
// import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

// import FormatHelper from 'coeur/helpers/format';
// import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import InputFileBit from 'modules/bits/input.file';
import LoaderBit from 'modules/bits/loader';
import ProgressBit from 'modules/bits/progress';

import Styles from './style';

const tempRegex = /temp\[.+\]/


export default ConnectHelper(
	class EditVariantMediaPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				value: PropTypes.any,
				progress: PropTypes.number,
				onChange: PropTypes.func,
				onDataLoaded: PropTypes.func,
				onRemoveMedia: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, isItMedia = !tempRegex.test(id)
				, media = isItMedia ? MediaManager.get(id) : false

			return {
				media,
			}
		}

		constructor(p) {
			super(p, {
			}, [
				'onChangeMedia',
			])
		}

		onChangeMedia(id, file) {
			if(file === undefined) {
				this.props.onRemoveMedia &&
				this.props.onRemoveMedia(id)
			}
		}

		view() {
			return this.props.media ? this.props.media._isGetting && (
				<BoxBit unflex centering style={[Styles.container, this.props.style]}>
					<LoaderBit size={16} />
				</BoxBit>
			) || (
				<InputFileBit
					id={ this.props.media.id }
					maxSize={3}
					value={ this.props.media }
					onChange={ this.onChangeMedia }
					style={ this.props.style }
				/>
			) : (
				<BoxBit unflex style={[Styles.container, this.props.progress && Styles.uploading, this.props.style]}>
					<InputFileBit
						id={ this.props.id }
						maxSize={3}
						value={ this.props.value }
						onDataLoaded={ this.props.onDataLoaded }
						onChange={ this.props.onChange }
					/>
					{ this.props.progress && (
						<ProgressBit progress={ this.props.progress } style={Styles.progress} barStyle={Styles.progressBar} />
					) }
				</BoxBit>
			)
		}
	}
)
