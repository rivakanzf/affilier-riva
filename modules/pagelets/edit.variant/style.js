import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'
// import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	rightButton: {
		marginLeft: 12,
	},

	box: {
		marginBottom: 16,
	},

	images: {
		flexWrap: 'wrap',
	},

	image: {
		margin: 4,
	},
})
