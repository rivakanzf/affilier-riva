import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	content: {
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
	},

	empty: {
		marginTop: Sizes.margin.default,
		marginBottom: Sizes.margin.default,
	},

	comment: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
	},

	tab: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingTop: 4,
		paddingBottom: 4,
		marginTop: -2,
		marginBottom: -2,
	},

	activeTab: {
		backgroundColor: Colors.grey.palette(2),
	},

	activeTabText: {

	},

	inactiveTabText: {
		color: Colors.black.palette(2, .4),
	},

	item: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
	},

	image: {
		width: 60,
		height: 78,
		marginRight: Sizes.margin.default,
	},

	discounted: {
		color: Colors.red.palette(7),
	},

	retail: {
		color: Colors.black.palette(2),
		textDecoration: 'line-through',
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	feedbacks: {

	},

	feedback: {
		// borderTopWidth: StyleSheet.hairlineWidth,
		// borderTopColor: Colors.black.palette(2, .16),
	},

	actions: {
		marginTop: Sizes.margin.default,
		marginLeft: -Sizes.margin.default,
		marginRight: -Sizes.margin.default,
		marginBottom: -Sizes.margin.default,
		// paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		backgroundColor: Colors.grey.palette(5),
		borderTopWidth: StyleSheet.hairlineWidth,
		// borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	otherPrice: {
		marginTop: Sizes.margin.default,
	},

	input: {
		height: 34,
	},

	button: {
		marginRight: Sizes.margin.default / 2,
	},

	footer: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		justifyContent: 'flex-end',
	},

})
