import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import ButtonBit from 'modules/bits/button'
import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';


import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

import Styles from './style';


export default ConnectHelper(
	class ImageEditorRecommendationPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				variantId: PropTypes.id,
				source: PropTypes.image,
				onPress: PropTypes.func,
				style: PropTypes.style,
				newIndex: PropTypes.number,
				onAddCroppedImage: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				crop: {
					unit: '%',
					width: 0,
				},
				isLoading: false,
				coor: {
					x1: 0,
					y1: 0,
					x2: 0,
					y2: 0,
				},
				scale: {
					scaleX: 0,
					scaleY: 0,
				},
			})
			this.imageWidth = React.createRef()
		}

		onImageLoaded = image => {
			this.imageRef = image;
			this.imageRef.setAttribute('crossorigin', 'anonymous')
			this.setState({
				width: image.naturalWidth,
			})
		};

		onCropComplete = crop => {
			this.makeClientCrop(crop);
		};

		makeClientCrop(crop) {
			if (this.imageRef && crop.width && crop.height) {
			  	this.getCroppedImg(
					this.imageRef,
					crop,
					'newFile.jpeg'
				)
			}
		}

		onCropChange = (crop) => {
			this.setState({ crop });
		}

		onSaveImage = () => {
			// this.setState({
			// 	isLoading: true,
			// })

			this.props.onAddCroppedImage &&
			this.props.onAddCroppedImage(this.state.croppedImg, this.state.coor, this.state.scale)
		}

		getCroppedImg(image, crop) {
			const canvas = document.createElement('canvas');
			const scaleX = image.naturalWidth / image.width;
			const scaleY = image.naturalHeight / image.height;
			this.setState({
				scale: {
					scaleX,
					scaleY,
				},
			})
			canvas.width = crop.width;
			canvas.height = crop.height;
			const ctx = canvas.getContext('2d');
			ctx.drawImage(
			  image,
			  crop.x * scaleX,
			  crop.y * scaleY,
			  crop.width * scaleX,
			  crop.height * scaleY,
			  0,
			  0,
			  crop.width,
			  crop.height
			);

			this.setState({
				croppedImg: canvas.toDataURL('image/jpeg'),
				coor: {
					x1: crop.x,
					y1: crop.y,
					x2: crop.width + crop.x,
					y2: crop.height + crop.y,
				},
			})
		}

		view() {
			return(
				<BoxBit style={[Styles.container, this.props.style]}>
					<BoxBit>
						<BoxBit>
							<ReactCrop
								src={ this.props.source }
								crop={this.state.crop}
								onImageLoaded={this.onImageLoaded}
								onComplete={this.onCropComplete}
								onChange={this.onCropChange}
								imageStyle={{
									maxWidth: '500px',
									borderWidth: 1,
									borderStyle: 'solid',
									objectFit: 'contain',
								}}
								ref={this.imageWidth}
							/>
						</BoxBit>
					</BoxBit>

					{
						this.state.croppedImg &&
						<BoxBit>
							<ButtonBit
								title={'Add'}
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								onPress={ this.onSaveImage }

								state={this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL}
							/>
							<ImageBit
								source={ this.state.croppedImg }
								transform={{crop: 'fit'}}

							/>
						</BoxBit>

					}

					<TouchableBit unflex style={Styles.icon} onPress={this.props.onPress}>
						<IconBit name="close" size={36}/>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
