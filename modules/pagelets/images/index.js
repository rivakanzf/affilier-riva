import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities'

import Masonry from 'react-masonry-component';

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import GeomanistBit  from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import InputFileBigBit from 'modules/bits/input.file.big';
import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';

import ImagePagelet from 'modules/pagelets/image'
import NotFoundPagelet from 'modules/pagelets/not.found';

import Styles, { height } from './style';

const SYNTHETIC_PREFIX = 'asset_'
let id = 1


export default ConnectHelper(
	class RowImageLego extends StatefulModel {
		_isMounted = false;

		static propTypes(PropTypes) {
			return {
				addable: PropTypes.bool,
				reorder: PropTypes.bool,
				images: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					width: PropTypes.number,
					height: PropTypes.number,
					url: PropTypes.string,
				})),
				prefix: PropTypes.string,
				uploader: PropTypes.func,
				remover: PropTypes.func,
				orderer: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			images: [],
			reorder: true,
			addable: true,
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				images: p.images,
			})
		}

		transform = { original: true }

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onImagePopUp = source => {
			this.props.utilities.alert.modal({
				component: (
					<ImagePagelet source={source} onPress={this.onModalRequestClose} transform={this.transform}/>
				),
			})
		}

		onChangeOrder = index => {
			const images = this.state.images
			// swapping magic
			images[index] = ([images[index + 1], images[index + 1] = images[index]][0])

			this.setState({
				images: [...images],
			}, () => {
				this.props.orderer &&
				this.props.orderer(this.state.images)

				this.props.onUpdate &&
				this.props.onUpdate(images)
			})
		}

		onDataLoaded = (index, value) => {
			if(value === undefined) {
				// removing
				// todo remove from cloudinary and database
				if (this.state.images[index].id && (
					typeof this.state.images[index].id !== 'string'
					|| !this.state.images[index].id.match(SYNTHETIC_PREFIX)
				)) {
					// remove
					this.props.remover &&
					this.props.remover(this.state.images[index].id)
				}

				const images = this.state.images.slice()
				images.splice(index, 1)

				this.setState({
					images,
				})
			} else if(index === 'adder') {
				// adding
				// upload

				const _id = `${SYNTHETIC_PREFIX}${id++}` // synthetic id

				if(this.props.uploader) {
					this.setState({
						images: [...this.state.images, {
							nonce: _id,
							data: value,
						}],
					})

					this.props.uploader(value, this.state.images.length).then(asset => {
						const images = this.state.images.slice()
						const assetIndex = images.findIndex(a => a.nonce === _id)

						if (assetIndex > -1) {
							images.splice(assetIndex, 1, asset)

							this.setState({
								images,
							})
						} else {
							// removed
							this.props.remover &&
							this.props.remover(asset.id)
						}

						this.props.onUpdate &&
						this.props.onUpdate(images)
					})
				} else {
					this.setState({
						images: [...this.state.images, {
							id: _id,
							data: value,
						}],
					})
				}
			}
		}

		imageRenderer = (file, index) => {
			const type = file.id !== undefined
				? 'asset' : 'uploading'

			const isAccessible = index !== this.state.images.length - 1

			return type === 'uploading' ? (
				<BoxBit key={index} unflex style={Styles.wrapper}>
					<BoxBit unflex centering style={Styles.image}>
						<LoaderBit simple />
					</BoxBit>
				</BoxBit>
			) : (
				<TouchableBit unflex key={index} style={Styles.wrapper} onPress={this.onImagePopUp.bind(this, file)}>
					{ this.props.addable ? (
						<InputFileBigBit
							unflex
							id={index}
							maxSize={3}
							value={type === 'asset' ? file : file.data}
							style={[Styles.image, {
								height: height(file.width, file.height),
							}]}
							onDataLoaded={this.onDataLoaded}
						/>
					) : (
						<ImageBit overlay resizeMode={ ImageBit.TYPES.COVER } source={type === 'asset' ? file : file.data } style={[Styles.image, {
							height: height(file.width, file.height),
						}]} />
					) }
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } weight="medium">
						#{ this.props.prefix }-{ file.id }
					</GeomanistBit>
					{ file.metadata.format ? (
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
							Format: { file.metadata.format.toUpperCase() }
						</GeomanistBit>
					) : false }
					{ file.metadata.bytes ? (
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
							Size: { Math.floor(file.metadata.bytes / 1024) }kB
						</GeomanistBit>
					) : false }
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
						Dimension: { file.width }w x { file.height }h
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
						Taken at: { TimeHelper.format(file.created_at, 'DD/MM/YYYY HH:mm') }
					</GeomanistBit>
					{ this.props.reorder ? (
						<TouchableBit accessible={ isAccessible } style={Styles.mover} onPress={ this.onChangeOrder.bind(this, index) }>
							<IconBit
								name="arrow-right"
								color={ isAccessible ? undefined : Colors.black.palette(2, .3) }
							/>
						</TouchableBit>
					) : false}
				</TouchableBit>
			)
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					{ !this.state.images.length && !this.props.addable ? (
						<NotFoundPagelet description="Imagine nothing" />
					) : (
						<Masonry>
							{ this.state.images.map(this.imageRenderer) }
							{ this.props.addable ? (
								<BoxBit unflex style={Styles.wrapper}>
									<InputFileBigBit unflex
										key={ this.state.images.length }
										id={'adder'}
										maxSize={3}
										style={Styles.image}
										onDataLoaded={ this.onDataLoaded }
									/>
									<GeomanistBit italic type={ GeomanistBit.TYPES.NOTE_2 } style={Styles.more}>
										Add more photos, will you?
									</GeomanistBit>
								</BoxBit>
							) : false }
						</Masonry>
					) }
				</BoxBit>
			)
		}
	}
)
