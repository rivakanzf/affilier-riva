import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import InventoryService from 'app/services/inventory'

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import RowStatusInventoryLego from 'modules/legos/row.status.inventory';

import ModalHintPagelet from 'modules/pagelets/modal.hint'

export default ConnectHelper(
	class InventoryStatusPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				status: PropTypes.string,
				note: PropTypes.string,
				type: PropTypes.oneOf([
					'ORDER_DETAIL',
					'VOUCHER',
					'STYLESHEET',
					'USER',
				]),
				typeId: PropTypes.id,
				force: PropTypes.bool,
				onClose: PropTypes.func,
				onChange: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isChanged: false,
				isSaving: false,
				status: p.status,
				note: p.note,
			})
		}

		onChange = ({ status, note }) => {
			this.state.status = status
			this.state.note = note

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onModalRequestClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		onSave = () => {
			this.setState({
				isSaving: true,
			}, () => {
				if (this.props.status === 'BOOKED') {
					// CANCEL BOOKING
					InventoryService.cancelBooking(this.props.id, this.state.note, this.state.status, this.props.type, this.props.typeId, this.props.force, this.props.token).then(() => {
						this.props.utilities.notification.show({
							title: 'Success!',
							message: 'Booking cancelled',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.props.onChange &&
						this.props.onChange(this.state.status, this.state.note)

						this.onModalRequestClose()
					}).catch(this.onError)
				} else if (this.state.status === 'BOOKED') {
					InventoryService.book(this.props.id, this.state.note, this.props.type, this.props.typeId, this.props.token).then(() => {
						this.props.utilities.notification.show({
							title: 'Yeay!',
							message: 'Inventory booked',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.props.onChange &&
						this.props.onChange(this.state.status, this.state.note)

						this.onModalRequestClose()
					}).catch(this.onError)
				} else {
					InventoryService.updateStatus({
						id: this.props.id,
						code: this.state.status,
						note: this.state.note,
					}, this.props.token).then(() => {
						this.props.utilities.notification.show({
							title: 'High five!',
							message: 'Inventory status updated.',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.props.onChange &&
						this.props.onChange(this.state.status, this.state.note)

						this.onModalRequestClose()
					}).catch(this.onError)
				}
			})
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isSaving: false,
			})
		}

		footerRenderer() {
			return [(
				<ButtonBit
					key={0}
					title="Cancel"
					weight="medium"
					size={ButtonBit.TYPES.SIZES.SMALL}
					theme={ButtonBit.TYPES.THEMES.SECONDARY}
					onPress={ this.onModalRequestClose }
				/>
			), (
				<BoxBit key={1} />
			), (
				<ButtonBit
					key={2}
					title="Change"
					weight="medium"
					type={ButtonBit.TYPES.SHAPES.PROGRESS}
					size={ButtonBit.TYPES.SIZES.SMALL}
					state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isChanged && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
					onPress={this.onSave}
				/>
			)]
		}

		view() {
			return (
				<ModalHintPagelet
					title="Change Inventory Status"
					hint="Changing inventory status might affect many things since it is related to bookings. Please proceed with caution."
					footer={this.footerRenderer()}
				>
					<RowStatusInventoryLego
						status={ this.props.status }
						note={ this.props.note }
						onChange={ this.onChange }
					/>
				</ModalHintPagelet>
			)
		}
	}
)

