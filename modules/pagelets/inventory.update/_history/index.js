import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import InventoryService from 'app/services/new.inventory';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
import GeomanistBit from 'modules/bits/geomanist';

import BadgeStatusLego from 'modules/legos/badge.status';
import TableLego from 'modules/legos/table';

import Styles from './style';

import { isEmpty } from 'lodash';


export default ConnectHelper(
	class HistoryPart extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
			}
		}

		static propsToPromise(state, oP) {
			return InventoryService.history(oP.id, state.me.token)
		}

		static getDerivedStateFromProps(nP) {
			if (!nP.id) {
				return null
			} else if(isEmpty(nP.data)) {
				return null
			}

			const bookings = nP.data.bookings || []
			const histories = nP.data.histories || []

			return {
				timeline: histories.map(h => {
					const isBooking = h.status === 'BOOKED'
					// get booking data

					if(isBooking) {
						const bookingIndex = bookings.findIndex(b => {
							return TimeHelper.moment(b.created_at).diff(TimeHelper.moment(h.created_at)) < 5000
						})

						return {
							...h,
							booking: bookingIndex > -1 ? bookings.splice(bookingIndex, 1)[0] : null,
						}
					} else {
						return h
					}
				}),
			}
		}

		constructor(p) {
			super(p, {
				timeline: [],
			}, [])
		}

		header = [{
			title: 'Date',
			width: 1.5,
		}, {
			title: 'Note',
			width: 4,
		}, {
			title: 'Status',
			width: 2,
		}]

		getTitle = booking => {
			switch(booking.source) {
			case 'ORDER_DETAIL':
				return `Order – #${booking.orderDetail.order.number}/${booking.orderDetail.id}`
			case 'STYLESHEET':
				return `Stylesheet – ${booking.stylesheet.matchbox.title} #${booking.stylesheet.id}`
			case 'USER':
				return `Stylist – ${booking.user.profile.first_name} ${booking.user.profile.last_name}`
			case 'VOUCHER':
				return `Voucher – #${booking.voucher.code} (Voucher ID: #${booking.voucher.id})`
			default:
				return false
			}
		}

		timelineRenderer = data => {
			return {
				data: [{
					children: (
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} >
								{ TimeHelper.format(data.created_at, 'DD MMM YY') }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
								{ TimeHelper.format(data.created_at, 'HH:mm') }
							</GeomanistBit>
						</BoxBit>
					),
				}, data.booking ? {
					children: (
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} >
								{ data.note }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
								{ this.getTitle(data.booking) }
							</GeomanistBit>
						</BoxBit>
					),
				} : {
					title: data.note,
				}, {
					children: (
						<BoxBit row style={Styles.badge}>
							<BadgeStatusLego status={data.status} />
						</BoxBit>
					),
				}],
			}
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit>
					{ !this.state.timeline.length ? (
						<BoxBit centering style={Styles.empty}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								No data recorded
							</GeomanistBit>
						</BoxBit>
					) : (
						<TableLego
							headers={this.header}
							rows={this.state.timeline.map(this.timelineRenderer)}
						/>
					) }
				</BoxBit>
			)
		}
	}
)
