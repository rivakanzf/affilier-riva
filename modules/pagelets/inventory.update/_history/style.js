import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	empty: {
		height: 240,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	badge: {
		alignItems: 'flex-start',
	},
})
