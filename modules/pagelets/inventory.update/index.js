import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import InventoryService from 'app/services/new.inventory';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import BoxLego from 'modules/legos/box';
import HintLego from 'modules/legos/hint';
import TabLego from 'modules/legos/tab';

import GetterInventoryComponent from 'modules/components/getter.inventory';

import DetailPart from './_detail';
import HistoryPart from './_history';

import Styles from './style';


export default ConnectHelper(
	class InventoryUpdatePagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				variantId: PropTypes.id,
				brandId: PropTypes.id,
				brandAddressId: PropTypes.id,
				note: PropTypes.string,
				price: PropTypes.number,
				rack: PropTypes.string,
				isOwnedByYuna: PropTypes.bool,
				status: PropTypes.string,
				onUpdate: PropTypes.func,
				onDuplicate: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				tabIndex: 0,
				isLoading: false,
				isChanged: false,
			})

			this.data = {
				brandId: null,
				brandAddressId: null,
				note: undefined,
				price: undefined,
				status: null,
				quantity: 1,
				isOwnedByYuna: false,
				rack: undefined,
			}
		}

		tabs = [{
			title: 'Detail',
			onPress: () => {
				this.setState({
					tabIndex: 0,
				})
			},
		}, {
			title: 'Status History',
			onPress: () => {
				this.setState({
					tabIndex: 1,
				})
			},
		}]

		onChange = (key, value) => {
			if(key === 'quantity' || key === 'brandId' || key === 'brandAddressId') {
				this.data[key] = +value
			} else {
				this.data[key] = value
			}

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onSave = () => {
			if(
				this.data.brandId &&
				this.data.brandAddressId &&
				this.data.price &&
				this.data.status
			) {
				this.setState({
					isLoading: true,
				}, () => {
					if (!this.props.id) {
						InventoryService.createInventoryItem({
							variant_id: this.props.variantId,
							brand_id: this.data.brandId,
							brand_address_id: this.data.brandAddressId,
							price: this.data.price,
							isOwnedByYuna: false,
							note: this.data.note,
							quantity: this.data.quantity,
							rack: this.data.rack,
							status: this.data.status,
						}, this.props.token)
							.then(isUpdated => {
								if (isUpdated) {
									this.props.utilities.notification.show({
										title: 'Yeay!',
										message: 'Inventory data updated.',
										type: this.props.utilities.notification.TYPES.SUCCESS,
									})

									this.props.onUpdate &&
									this.props.onUpdate(this.data)

									this.onClose()
								} else {
									this.onError(isUpdated)
								}
							})
							.catch(this.onError)
					} else {
						InventoryService.updateInventoryItem(this.props.id, {
							variant_id: this.props.variantId,
							brand_id: this.data.brandId,
							brand_address_id: this.data.brandAddressId,
							price: this.data.price,
							note: this.data.note,
							rack: this.data.rack,
							status: this.data.status,
						}, this.props.token)
							.then(isUpdated => {
								if (isUpdated) {
									this.props.utilities.notification.show({
										title: 'Yeay!',
										message: 'Inventory data updated.',
										type: this.props.utilities.notification.TYPES.SUCCESS,
									})

									this.props.onUpdate &&
									this.props.onUpdate(this.data)

									this.onClose()
								} else {
									this.onError(isUpdated)
								}
							})
							.catch(this.onError)
					}
				})
			} else {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: 'Data incomplete',
				})
			}
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onDuplicate = () => {
			this.onClose()

			this.props.onDuplicate &&
			this.props.onDuplicate({
				brandId: this.data.brandId,
				brandAddressId: this.data.brandAddressId,
				note: this.data.note,
				price: this.data.price,
				rack: this.data.rack,
				status: 'AVAILABLE', // this.data.status,
			})
		}

		headerRenderer() {
			return this.props.id ? (
				<BoxBit unflex row style={Styles.tabs}>
					<BoxBit unflex style={Styles.divider} />
					<BoxBit>
						<TabLego
							tabs={this.tabs}
							activeIndex={this.state.tabIndex}
						/>
					</BoxBit>
					<BoxBit style={Styles.divider} />
				</BoxBit>
			) : false
		}

		contentRenderer() {
			switch(this.state.tabIndex) {
			case 0:
				return (
					<GetterInventoryComponent
						id={ this.props.id }
						children={ this.detailRenderer }
					/>
				)
			case 1:
				return this.historyRenderer()
			default:
				return false
			}
		}

		detailRenderer = ({ brandId, brandAddressId, note, price, rack, status, isOwnedByYuna }) => {
			if(
				this.props.brandId &&
				this.props.brandAddressId &&
				this.props.note &&
				this.props.price &&
				this.props.isOwnedByYuna &&
				this.props.status
			) {
				// DUPLICATING
				this.data.brandId = this.props.brandId
				this.data.brandAddressId = this.props.brandAddressId
				this.data.note = this.props.note
				this.data.price = this.props.price
				this.data.rack = this.props.rack
				this.data.status = this.props.status
				this.data.isOwnedByYuna = this.props.isOwnedByYuna

				this.setState({
					isChanged: true,
				})
			} else {
				this.data.brandId = brandId
				this.data.brandAddressId = brandAddressId
				this.data.note = note
				this.data.price = price
				this.data.rack = rack
				this.data.status = status
				this.data.isOwnedByYuna = isOwnedByYuna
			}

			return (
				<DetailPart
					isEditting={ !!this.props.id }
					brandId={ this.data.brandId }
					brandAddressId={ this.data.brandAddressId }
					note={ this.data.note }
					price={ this.data.price }
					quantity={ this.data.quantity }
					rack={ this.data.rack }
					status={ this.data.status }
					isOwnedByYuna={ this.data.isOwnedByYuna }
					onChangeBrand={ this.onChange.bind(this, 'brandId') }
					onChangeBrandAddress={ this.onChange.bind(this, 'brandAddressId') }
					onChangeNote={ this.onChange.bind(this, 'note') }
					onChangePrice={ this.onChange.bind(this, 'price') }
					onChangeQuantity={ this.onChange.bind(this, 'quantity') }
					onChangeOwnership={ this.onChange.bind(this, 'isOwnedByYuna') }
					onChangeRack={ this.onChange.bind(this, 'rack') }
					onChangeStatus={ this.onChange.bind(this, 'status') }
				/>
			)
		}

		historyRenderer() {
			return (
				<HistoryPart id={ this.props.id } />
			)
		}

		footerRenderer() {
			return (
				<BoxBit unflex row style={Styles.footer}>
					<ButtonBit
						title="Cancel"
						type={ButtonBit.TYPES.SHAPES.RECTANGLE}
						theme={ButtonBit.TYPES.THEMES.SECONDARY}
						size={ButtonBit.TYPES.SIZES.SMALL}
						weight="medium"
						onPress={this.onClose}
					/>

					<ButtonBit
						title={this.props.id ? 'Update' : 'Create'}
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isChanged && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
						type={ButtonBit.TYPES.SHAPES.PROGRESS}
						size={ButtonBit.TYPES.SIZES.SMALL}
						weight="medium"
						onPress={ this.onSave }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxLego
					title={ this.props.id ? 'Edit Inventory item' : 'Add Inventory item' }
					header={ this.props.id ? (
						<BoxBit row centering style={Styles.header}>
							<ButtonBit
								title="Duplicate"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								size={ ButtonBit.TYPES.SIZES.SMALL }
								onPress={ this.onDuplicate }
								style={ Styles.button }
							/>
							<HintLego title="Duplicate this inventory to create another inventory with the same data set." />
						</BoxBit>
					) : undefined }
					contentContainerStyle={Styles.container}>
					{ this.headerRenderer() }
					{ this.contentRenderer() }
					{ this.footerRenderer() }
				</BoxLego>
			)
		}
	}
)
