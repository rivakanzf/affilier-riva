import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		width: 592,
		maxHeight: 592,
		minHeight: 240,
		margin: 0,
	},

	header: {
		justifyContent: 'flex-end',
	},

	button: {
		marginRight: 8,
	},

	tabs: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 12,
	},

	divider: {
		width: 16,
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(2, .2),
	},

	footer: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 16,
		paddingRight: 16,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		justifyContent: 'space-between',
	},
})
