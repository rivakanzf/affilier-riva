import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import ModalPagelet from '../modal'

import Colors from 'coeur/constants/color'

import Styles from './style'


export default ConnectHelper(
	class ModalClosablePagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				small: PropTypes.bool,
				header: PropTypes.node,
				subheader: PropTypes.node,
				footer: PropTypes.node,
				onClose: PropTypes.func,
				children: PropTypes.node,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
			}
		}

		view() {
			return (
				<ModalPagelet
					small={ this.props.small }
					title={ this.props.title }
					header={(
						<BoxBit row unflex>
							{ this.props.header }
							{ this.props.onClose ? (
								<TouchableBit unflex centering onPress={this.props.onClose} style={Styles.close}>
									<IconBit
										unflex
										name="close"
										color={Colors.black.palette(2, .8)}
									/>
								</TouchableBit>
							) : false }
						</BoxBit>
					)}
					subheader={ this.props.subheader }
					footer={ this.props.footer }
					children={ this.props.children }
					style={ this.props.style }
					contentContainerStyle={ this.props.contentContainerStyle }
				/>
			)
		}
	}
)

