import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import GeomanistBit from 'modules/bits/geomanist';
import TextAreaBit from 'modules/bits/text.area';
import TextInputBit from 'modules/bits/text.input';

// import ModalClosableConfirmationPagelet from '../modal.closable.confirmation'

import {
	DefaultPart,
} from 'modules/pagelets/alert'

import Styles from './style'


export default ConnectHelper(
	class ModalPromptPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				showInput: PropTypes.bool,
				note: PropTypes.bool,
				title: PropTypes.string,
				message: PropTypes.string,
				cancel: PropTypes.string,
				confirm: PropTypes.string,
				placeholder: PropTypes.string,
				value: PropTypes.string,
				description: PropTypes.string,
				onCancel: PropTypes.func,
				onConfirm: PropTypes.func,
				closeAfterSave: PropTypes.bool,
				children: PropTypes.node,
				readonly: PropTypes.bool,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			confirm: 'SUBMIT',
			cancel: 'CANCEL',
			showInput: true,
			closeAfterSave: true,
		}

		value = undefined
		actions = [ this.props.onConfirm ? {
			title: this.props.confirm,
			onPress: () => this.onSave(),
		} : false, this.props.onCancel ? {
			title: this.props.cancel,
			onPress: this.props.onCancel,
			type: 'CANCEL',
		} : false].filter(c => c !== false)

		onChange = (e, val) => {
			this.value = val
		}

		onSave = () => {
			this.props.closeAfterSave &&
			this.props.onCancel()

			this.props.onConfirm &&
			this.props.onConfirm(this.value)
		}

		view() {
			return (
				<DefaultPart
					actions={ this.actions }
					title={ this.props.title }
					message={ this.props.message }
					description={ this.props.description }
				>
					{
						this.props.showInput && (
							this.props.note ? (
								<TextAreaBit
									readonly={ this.props.readonly }
									autofocus={ !this.props.readonly }
									placeholder={ this.props.placeholder }
									defaultValue={ this.props.value }
									onChange={ this.onChange }
									onSubmitEditing={ this.onSave }
									style={ Styles.padder }
								/>
							) : (
								<TextInputBit
									readonly={ this.props.readonly }
									autofocus={ !this.props.readonly }
									placeholder={ this.props.placeholder }
									defaultValue={ this.props.value }
									onChange={ this.onChange }
									onSubmitEditing={ this.onSave }
									style={ Styles.padder }
								/>
							)
						)
					}
					{ this.props.children }
				</DefaultPart>
			)
		}
	}
)

