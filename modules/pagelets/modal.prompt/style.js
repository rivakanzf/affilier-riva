import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	padder: {
		marginTop: Sizes.margin.default,
	},
	description: {
		color: Colors.black.palette(2, .6),
		paddingTop: 8,
	},
})
