import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import ScrollViewBit from 'modules/bits/scroll.view';

import BoxLego from 'modules/legos/box';

import Styles from './style';


export default ConnectHelper(
	class ModalPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				small: PropTypes.bool,
				header: PropTypes.node,
				subheader: PropTypes.node,
				footer: PropTypes.node,
				children: PropTypes.node,
				style: PropTypes.style,
				contentContainerStyle: PropTypes.style,
			}
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.small && Styles.small, this.props.style]}>
					<BoxLego
						unflex={ false }
						small={ this.props.small }
						title={ this.props.title }
						header={ this.props.header }
						style={[Styles.contentContainer, !this.props.footer ? Styles.contentContainerFull : {}]}
						contentContainerStyle={ Styles.content }
					>
						{ this.props.subheader }
						<ScrollViewBit contentContainerStyle={[Styles.scroller, this.props.contentContainerStyle]}>
							{ this.props.children }
						</ScrollViewBit>
					</BoxLego>
					{ this.props.footer ? (
						<BoxBit row unflex style={Styles.footer}>
							{ this.props.footer }
						</BoxBit>
					) : false }
				</BoxBit>
			)
		}
	}
)

