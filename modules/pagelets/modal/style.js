import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.white.primary,
		width: 560,
		maxHeight: '95%',
		overflow: 'hidden',
		borderRadius: 4,
	},

	contentContainer: {
		backgroundColor: Colors.transparent,
		maxHeight: '90%',
	},

	contentContainerFull: {
		maxHeight: '100%',
	},

	content: {
		height: '90%',
		marginTop: 0,
		marginLeft: 0,
		marginBottom: 0,
		marginRight: 0,
	},

	scroller: {
		maxHeight: '100%',
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	small: {
		width: 280,
	},

	footer: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		borderStyle: 'solid',
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		alignItems: 'center',
	},
})
