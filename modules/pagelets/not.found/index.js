import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';

import Styles from './style';


export default ConnectHelper(
	class NotFoundPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				description: PropTypes.string,
				loading: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			title: 'Oops!',
			description: '404. We can\'t seems to find what you\'re looking for…',
		}

		view() {
			return (
				<BoxBit centering style={[Styles.container, !this.props.loading && Styles.content, this.props.style]}>
					{ this.props.loading ? (
						<LoaderBit simple />
					) : (
						<React.Fragment>
							<GeomanistBit type={GeomanistBit.TYPES.HEADER_4} style={ Styles.title }>{ this.props.title }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.desc }>{ this.props.description }</GeomanistBit>
						</React.Fragment>
					) }
				</BoxBit>
			)
		}
	}
)
