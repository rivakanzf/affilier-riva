import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		height: '100%',
		alignSelf: 'stretch',
	},
	content: {
		backgroundColor: Colors.grey.palette(5),
		borderWidth: StyleSheet.hairlineWidth,
		borderRadius: 4,
		borderColor: Colors.black.palette(2, .16),
	},
	title: {
		textAlign: 'center',
	},
	desc: {
		textAlign: 'center',
		paddingTop: 8,
	},
})
