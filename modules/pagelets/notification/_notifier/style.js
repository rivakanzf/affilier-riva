import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		paddingTop: 24,
		paddingRight: 24,
		paddingLeft: 24,
		paddingBottom: 32,
		width: '100%',
		pointerEvents: 'all',
	},

	background_DEFAULT: {
		backgroundColor: Colors.red.primary,
	},
	background_SUCCESS: {
		backgroundColor: Colors.green.primary,
	},

	color_DEFAULT: {
		color: Colors.white.primary,
	},
	color_SUCCESS: {
		color: Colors.white.primary,
	},
})
