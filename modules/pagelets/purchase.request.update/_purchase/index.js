import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import BoxBit from 'modules/bits/box'
import InputCurrencyBit from 'modules/bits/input.currency';
import InputValidatedBit from 'modules/bits/input.validated';
import LinkBit from 'modules/bits/link';
import TextInputBit from 'modules/bits/text.input';

import RowBrandLego from 'modules/legos/row.brand';
import RowCategoryLego from 'modules/legos/row.category';
import SelectSizeLego from 'modules/legos/select.size';
import SelectColorLego from 'modules/legos/select.color';

import GetterCategoryComponent from 'modules/components/getter.category';

import RowsLego from 'modules/legos/rows';

import Styles from './style';

export default ConnectHelper(
	class PurchasePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				editable: PropTypes.bool,
				noquantity: PropTypes.bool,
				variant_id: PropTypes.id,
				url: PropTypes.string, // ok
				seller_id: PropTypes.id,
				brand_id: PropTypes.id, // ok
				title: PropTypes.string, // ok
				category_id: PropTypes.id, // ok
				color_id: PropTypes.id, // ok
				size_id: PropTypes.id, // ok
				quantity: PropTypes.number,
				price: PropTypes.number,
				retail_price: PropTypes.number,
				note: PropTypes.string,
				onUpdate: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				categoryId: null,
			})
		}

		shouldComponentUpdate(nP, nS) {
			// When ReactDOM checks to see if a field is controlled, it checks to see if value != null (note that it's !=, not !==), and since undefined == null in JavaScript, it decides that it's uncontrolled. because '' != null
			// Solution: do not update, use default value
			return nS.categoryId !== this.state.categoryId
		}

		onChange(id, val) {
			if(id === 'category_id') {
				this.setState({
					categoryId: val,
				})
			}

			if(id === 'quantity') {
				this.onUpdateInput(id, null, +val)
			} else {
				this.onUpdateInput(id, null, val)
			}

		}

		onUpdateInput = (key, e, val) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, val)
		}

		onUpdateNumber = (key, e, val) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, parseInt(val, 10))
		}

		sizeRenderer = ({ categoryId }) => {
			return (
				<SelectSizeLego unflex disabled={ !categoryId || !this.props.editable }
					categoryId={ categoryId }
					sizeId={ this.props.size_id }
					onChange={ this.onChange.bind(this, 'size_id') }
				/>
			)
		}

		view() {
			return (
				<RowsLego
					data={[{
						data: [{
							title: 'URL',
							children: this.props.editable ? (
								<TextInputBit
									placeholder="Input here…"
									defaultValue={ this.props.url }
									onChange={ this.onUpdateInput.bind(this, 'url') }
								/>
							) : (
								<LinkBit underline target={ LinkBit.TYPES.BLANK } href={ this.props.url }>
									{ this.props.url }
								</LinkBit>
							),
						}],
					}, {
						data: [{
							title: 'Product Name',
							children: (
								<TextInputBit disabled={ !this.props.editable }
									placeholder="Input here…"
									defaultValue={this.props.title}
									onChange={this.onUpdateInput.bind(this, 'title')}
								/>
							),
						}],
					}, {
						row: false,
						children: (
							<RowBrandLego disabled={ !this.props.editable }
								title="Seller"
								brandId={ this.props.seller_id }
								onChange={ this.onChange.bind(this, 'seller_id') }
							/>
						),
					}, {
						row: false,
						children: (
							<RowBrandLego disabled={ !this.props.editable }
								brandId={ this.props.brand_id }
								onChange={ this.onChange.bind(this, 'brand_id') }
							/>
						),
					}, {
						row: false,
						children: (
							<RowCategoryLego disabled={ !this.props.editable }
								categoryId={ this.props.category_id }
								onChange={ this.onChange.bind(this, 'category_id') }
							/>
						),
					}, {
						data: [{
							title: 'Color',
							children: (
								<SelectColorLego unflex disabled={ !this.props.editable }
									colorId={ this.props.color_id }
									onChange={ this.onChange.bind(this, 'color_id') }
								/>
							),
						}, {
							title: 'Size',
							children: (
								<GetterCategoryComponent key={ this.state.categoryId } categoryId={ this.state.categoryId || this.props.category_id } children={ this.sizeRenderer } />
							),
						}],
						style: Styles.row,
					}, {
						data: [{
							title: 'Retail Price',
							children: (
								<InputCurrencyBit readonly={ !this.props.editable }
									placeholder="Input here…"
									value={ this.props.retail_price }
									onChange={ this.onChange.bind(this, 'retail_price') }
								/>
							),
						}, {
							title: 'Discounted Price',
							children: (
								<InputCurrencyBit readonly={ !this.props.editable }
									placeholder="Input here…"
									value={ this.props.price }
									onChange={ this.onChange.bind(this, 'price') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Quantity',
							children: (
								<InputValidatedBit disabled={ this.props.noquantity ? true : !this.props.editable }
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here…"
									value={ this.props.quantity }
									onChange={ this.onChange.bind(this, 'quantity') }
								/>
							),
						}, {
							blank: true,
						}],
					}, {
						data: [{
							title: 'Note (optional)',
							children: (
								<InputValidatedBit disabled={ !this.props.editable }
									type={ InputValidatedBit.TYPES.TEXTAREA }
									placeholder="Input here…"
									value={ this.props.note }
									onChange={ this.onChange.bind(this, 'note') }
								/>
							),
							description: 'Place your note about this request (optional).',
						}],
					}]}
					style={ Styles.container }
				/>
			)
		}
	}
)
