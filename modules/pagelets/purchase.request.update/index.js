/* eslint-disable no-nested-ternary */
import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities'

import PurchaseService from 'app/services/purchase';
import VariantService from 'app/services/variant'

import ButtonBit from 'modules/bits/button';

import LoaderLego from 'modules/legos/loader'

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'

import PurchasePart from './_purchase';

import { isEmpty } from 'lodash';


export default ConnectHelper(
	class PurchaseRequestUpdatePagelet extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				data: PropTypes.object,
				title: PropTypes.string,
				editable: PropTypes.bool,
				noquantity: PropTypes.bool,
				pendingOnEdit: PropTypes.bool,
				unremoveable: PropTypes.bool,
				bodyless: PropTypes.bool,
				onUpdate: PropTypes.func,
				onUpdated: PropTypes.func,
				onRemove: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return oP.id ? PurchaseService.getDetail(oP.id, {
				basic: true,
			}, state.me.token) : oP.data ? Promise.resolve(oP.data) : null
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.override) {
				return {
					isLoaded: true,
					purchase: {
						...nP.purchaseData,
					},
				}
			}
			if (!nS.isLoaded && !isEmpty(nP.data)) {
				return {
					isLoaded: true,
					purchase: nP.data,
				}
			}
			
			return null
		}

		static defaultProps = {
			pendingOnEdit: true,
			title: 'Edit Purchase Request',
		}

		constructor(p) {
			super(p, {
				isLoaded: false,
				isLoading: false,
				isChanged: false,
				purchase: {
					quantity: p.noquantity ? 1 : undefined,
				},
			})
		}

		onRemove = () => {
			this.props.onRemove &&
			this.props.onRemove()

			this.onClose()
		}

		onUpdateRequest = () => {
			if(this.props.onUpdate) {
				this.props.onUpdate(this.state.purchase)
				this.onClose()
			} else if(this.props.id) {
				this.setState({
					isLoading: true,
				}, () => {
					PurchaseService.updateRequest(this.props.id, {
						...this.state.purchase,
						...(this.props.pendingOnEdit ? {
							status: 'PENDING',
						} : {}),
					}, this.props.token).then(isUpdated => {
						this.props.utilities.notification.show({
							title: 'Success',
							message: 'Purchase request updated',
							type: 'SUCCESS',
						})

						this.props.onUpdated &&
						this.props.onUpdated(isUpdated)

						this.onClose()
					}).catch(this.onError)
				})
			} else {
				this.setState({
					isLoading: true,
				}, () => {
					PurchaseService.createRequest(this.state.purchase, this.props.token).then(data => {
						this.props.utilities.notification.show({
							title: 'Success',
							message: 'Purchase request created',
							type: 'SUCCESS',
						})

						this.props.onUpdated &&
						this.props.onUpdated(data)

						this.onClose()
					}).catch(this.onError)
				})
			}
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onUpdatePurchaseData = (key, value) => {
			this.state.purchase[key] = value

			this.setState({
				purchase: { ...this.state.purchase },
				isChanged: true,
			})
		}

		headerRenderer() {
			return this.props.unremoveable ? undefined : (
				<ButtonBit
					title="Remove"
					type={ ButtonBit.TYPES.SHAPES.PROGRESS }
					size={ ButtonBit.TYPES.SIZES.SMALL }
					state={ !this.props.editable ? ButtonBit.TYPES.STATES.DISABLED : this.state.isLoading && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL }
					onPress={ this.onRemove }
				/>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return this.props.bodyless ? (
				<PurchasePart { ...this.state.purchase } editable={ this.props.editable } noquantity={ this.props.noquantity } onUpdate={ this.props.onUpdate || this.onUpdatePurchaseData } />
			) : (
				<ModalClosableConfirmationPagelet
					title={ this.props.title }
					header={ this.headerRenderer() }
					loading={ this.state.isLoading }
					disabled={ this.props.editable && !this.state.isChanged }
					confirm={ this.props.editable ? 'Submit' : 'Done' }
					onCancel={ this.props.editable ? this.onClose : undefined }
					onConfirm={ this.props.editable ? this.onUpdateRequest : this.onClose }
				>
					<PurchasePart { ...this.state.purchase } editable={ this.props.editable } noquantity={ this.props.noquantity } onUpdate={ this.onUpdatePurchaseData } />
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
