import React from 'react';
import StatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import Sizes from 'coeur/constants/size'
import FormatHelper from 'coeur/helpers/format'

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist'
import ImageBit from 'modules/bits/image'
import TouchableBit from 'modules/bits/touchable'
import IconBit from 'modules/bits/icon'

import LoaderLego from 'modules/legos/loader';

import Styles from './style';


export default ConnectHelper(
	class QuickViewStylecardPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onClose: PropTypes.func,
				onNavigate: PropTypes.func,
				featuredImage: PropTypes.image,
				data: PropTypes.array,
				percentage: PropTypes.array,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		productRenderer = ({ image, price, retail_price: retailPrice  }, i, arr) => {
			const boxHalf = arr.length <= 4
			const isDiscountedPrice = price < retailPrice

			return (
				<BoxBit key={ i } unflex style={[Styles.gridItem, { width: boxHalf ? '34%' : '29%' }]}>
					<ImageBit unflex={ false } source={ image } style={ Styles.gridItemImage } transform={{crop: 'mfit'}} resizeMode={ ImageBit.TYPES.COVER }/>
					<GeomanistBit type={GeomanistBit.TYPES.CAPTION_2} style={[Styles.priceText, { ...!!isDiscountedPrice ? { color: 'red' } : {}}]}>
						IDR {FormatHelper.currencyFormat(price) }
					</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<BoxBit onClose={ this.props.onClose } style={ Styles.container }>
					<BoxBit style={ Styles.contentContainer }>
						{ !!this.props.featuredImage && (
							<BoxBit centering style={ Styles.containerFeaturedImage }>
								<ImageBit source={'//' + this.props.featuredImage} resizeMode={ ImageBit.TYPES.CONTAIN } transform={{crop: 'fit'}} style={Styles.featuredImage}/>
							</BoxBit>
						) }
						<BoxBit row centering style={ Styles.gridBox }>
							{ this.props.data.map(this.productRenderer) }
						</BoxBit>
					</BoxBit>

					<BoxBit unflex centering style={ Styles.title }>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={ Styles.titleText }>
							GRATIS ongkir se-Indonesia*
						</GeomanistBit>
					</BoxBit>

					<TouchableBit unflex centering style={Styles.icon} onPress={this.onClose}>
						<IconBit name="close" size={24}/>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
