import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.white.palette(1),
		width: Sizes.app.height / 16 * 9,
		maxHeight: Sizes.app.height,
	},

	contentContainer: {
		overflow: 'hidden',
		paddingTop: Sizes.margin.default,
		paddingBottom: 6,
		paddingRight: 8,
		paddingLeft: 8,
	},

	containerFeaturedImage: {
		maxHeight: '38%',
		overflow: 'hidden',
		marginBottom: 6,
	},

	featuredImage: {
		width: '100%',
		height: '100%',
		// paddingBottom: `${4 / 3 * 100}%`,
	},

	gridBox: {
		height: '100%',
		flexWrap: 'wrap',
		overflow: 'hidden',
	},

	gridItem: {
		height: '47%',
		margin: 6,
	},
	
	gridItemImage: {
		maxHeight: '90%',
		minHeight: '90%',
	},

	priceText: {
		textAlign: 'center',
		paddingTop: 4,
	},

	icon: {
		position: 'absolute',
		left: '100%',
		marginLeft: Sizes.margin.thick,
		top: Sizes.margin.default,
		height: 36,
		width: 36,
		backgroundColor: Colors.white.primary,
		borderRadius: 18,
	},

	title: {
		backgroundColor: Colors.grey.palette(3, .3),
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingTop: 11,
		paddingBottom: 11,
	},

	titleText: {
		textAlign: 'center',
		lineHeight: 16,
		fontSize: 13,
	},

	grid: {
		marginTop: 'auto',
	},

	'@media screen and (min-width: 1800px)': {
		gridItem: {
			height: '48%',
		},
	},

})
