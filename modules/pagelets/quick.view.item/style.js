import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	content: {
		marginTop: Sizes.margin.default,
	},
	image: {
		width: 140,
		height: 188,
		marginRight: Sizes.margin.default,
	},
	retail: {
		textDecoration: 'line-through',
	},
	discount: {
		color: Colors.red.palette(3),
		marginRight: 8,
	},
	description: {
		color: Colors.black.palette(2, .6),
	},
	note: {
		borderRadius: 4,
		marginTop: Sizes.margin.default / 2,
		paddingTop: Sizes.margin.default / 2,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default / 2,
		backgroundColor: Colors.solid.grey.palette(1),
		color: Colors.black.palette(2, .6),
	},
	status: {
		marginTop: 4,
		marginBottom: 4,
	},
})
