import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import OrderService from 'app/services/new.order';
import OrderDetailService from 'app/services/order.detail';

import ButtonBit from 'modules/bits/button';

import BoxRowLego from 'modules/legos/box.row';
import SelectStatusLego from 'modules/legos/select.status';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';

import QuickCreatePurchaseRequestPagelet from 'modules/pagelets/quick.create.purchase.request';


import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				type: PropTypes.string,
				title: PropTypes.string,
				refId: PropTypes.id,
				variantId: PropTypes.id,
				status: PropTypes.string,
				onStatusChanged: PropTypes.func,
				onInventoryBooked: PropTypes.func,
				onInventoryUnbooked: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			shipmentIds: [],
		}

		constructor(p) {
			super(p, {
				isSending: false,
				isChanging: false,
				updater: 1,
				status: p.status,
				readable: '',
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.close()
		}

		onChangeStatus = (status, readable) => {
			this.setState({
				status,
				readable,
			})
		}

		onUnbookInventory = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Unbooking Inventory"
						message={ 'Tell us why you unbook this inventory' }
						placeholder="Input here"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmUnbookInventory }
					/>
				),
			})
		}

		onConfirmUnbookInventory = note => {
			this.setState({
				isSending: true,
			})

			OrderDetailService.unbookInventory(this.props.id, note, this.props.token).then(isBooked => {
				if (isBooked) {
					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: 'Inventory successfuly unbooked.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.props.onInventoryUnbooked &&
					this.props.onInventoryUnbooked()

					this.setState({
						isSending: false,
					})
				} else {
					throw new Error('Inventory cannot be unbooked.')
				}
			}).catch(this.onError)
		}

		onBookInventory = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Inventory ID"
						message={ 'Input inventory Id you want to book' }
						placeholder="Input here"
						onCancel={ this.onModalRequestClose }
						onConfirm={ idString => {
							const id = parseInt(idString, 10)

							this.props.utilities.alert.modal({
								component: (
									<ModalPromptPagelet
										title="Add note"
										// eslint-disable-next-line no-nested-ternary
										message={ 'You might want to add a note for this change.' }
										placeholder="Input here"
										onCancel={ this.onModalRequestClose }
										onConfirm={ this.onConfirmBookInventory.bind(this, id) }
									/>
								),
							})
						} }
					/>
				),
			})
		}

		onConfirmBookInventory = (id, note) => {
			this.setState({
				isSending: true,
			})

			OrderDetailService.bookInventory(this.props.id, id, note, this.props.token).then(isBooked => {
				if (isBooked) {

					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: 'Inventory successfuly booked.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.props.onInventoryBooked &&
					this.props.onInventoryBooked(id)

					this.setState({
						isSending: false,
					})
				} else {
					throw new Error('Inventory not booked.')
				}
			}).catch(this.onError)

		}

		onChangeOrderDetailStatus = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						// eslint-disable-next-line no-nested-ternary
						message={ this.state.status === 'PRIMED'
							? 'Important: this action will make all stylesheet & inventories be marked as unavailable.'
							: this.state.status === 'RESOLVED'
								? 'Important: this action make all stylesheet non exchangeable.'
								: 'You might want to add a note for this change.' }
						placeholder="Input here"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmChangeOrderDetailStatus }
					/>
				),
			})
		}

		onConfirmChangeOrderDetailStatus = note => {
			this.setState({
				isChanging: true,
			})

			OrderService.setOrderDetailStatus(this.props.id, {
				code: this.state.status,
				note,
			}, this.props.token).then(isUpdated => {
				if(isUpdated) {
					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: 'Order detail status updated.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.props.onStatusChanged &&
					this.props.onStatusChanged(this.state.status)

					this.setState({
						updater: this.state.updater + 1,
						isChanging: false,
					})
				} else {
					throw new Error('Data not updated.')
				}
			}).catch(this.onError)
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isChanging: false,
				isSending: false,
			})

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later',
			})
		}

		onCreatePR = () => {
			this.props.utilities.alert.modal({
				component: (
					<QuickCreatePurchaseRequestPagelet
						override
						approved
						title={ this.props.title }
						id={ this.props.variantId }
					/>
				),
			})
		}

		onCreateRefund = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Refund this item"
						message={ 'Catatan' }
						placeholder="Input here"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmRefund }
					/>
				),
			})
		}

		onConfirmRefund = note => {
			OrderDetailService.createRefund(this.props.id, note, this.props.token).then(() => {
				window.location.reload()
			}).catch(this.onError)
		}

		view() {
			return (
				<BoxRowLego borderless
					title="Actions"
					data={[{
						data: [{
							title: 'Order Detail Status',
							children: (
								<SelectStatusLego key={ this.state.updater } unflex isRequired
									type={ SelectStatusLego.TYPES.ORDER_DETAIL_STATUSES }
									status={ this.state.status}
									onChange={ this.onChangeStatus }
								/>
							),
						}, {
							blank: true,
							children: (
								<ButtonBit
									weight="medium"
									title={ 'UPDATE STATUS' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.state.isChanging
										? ButtonBit.TYPES.STATES.LOADING
										: this.props.status === this.state.status && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL
									}
									onPress={ this.onChangeOrderDetailStatus }
								/>
							),
						}],
					},	
					this.props.onInventoryBooked ? {
						data: [{
							title: 'Inventory Fulfillment',
							children: (
								<ButtonBit
									title={ 'Book Inventory' }
									align={ ButtonBit.TYPES.ALIGNS.LEFT }
									weight="normal"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									theme={ ButtonBit.TYPES.THEMES.SECONDARY }
									state={ this.props.disabled ? ButtonBit.TYPES.STATES.DISABLED : this.state.isSending && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL }
									onPress={ this.onBookInventory }
								/>
							),
						}, {
							blank: true,
							content: 'Please book inventory according to request variant.',
						}],
					} : null, this.props.onInventoryUnbooked ? {
						data: [{
							title: 'Inventory Fulfillment',
							children: (
								<ButtonBit
									title={ 'Unbook Inventory' }
									align={ ButtonBit.TYPES.ALIGNS.LEFT }
									weight="normal"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									theme={ ButtonBit.TYPES.THEMES.SECONDARY }
									state={ this.props.disabled ? ButtonBit.TYPES.STATES.DISABLED : this.state.isSending && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL }
									onPress={ this.onUnbookInventory }
								/>
							),
						}, {
							blank: true,
							content: 'Unbook this inventory so it can be used in another order.',
						}],
					} : null].concat([
						{
							data: this.props.type === 'VARIANT' && [{
								children: (
									<ButtonBit
										title={ 'Create PR' }
										align={ ButtonBit.TYPES.ALIGNS.LEFT }
										weight="normal"
										type={ ButtonBit.TYPES.SHAPES.PROGRESS }
										theme={ ButtonBit.TYPES.THEMES.SECONDARY }
										onPress={ this.onCreatePR }
									/>
								),
							}, {
								blank: true,
								content: 'Create PR for this item.',
							}],
						},
						this.props.status !== 'EXCEPTION'
							? {
								data: this.props.type === 'VARIANT' && [{
									children: (
										<ButtonBit
											title={ 'Refund' }
											align={ ButtonBit.TYPES.ALIGNS.LEFT }
											weight="normal"
											type={ ButtonBit.TYPES.SHAPES.PROGRESS }
											theme={ ButtonBit.TYPES.THEMES.SECONDARY }
											onPress={ this.onCreateRefund }
										/>
									),
								}, {
									blank: true,
									content: 'Refund this item.',
								}],
							}
							: false,
					])}
					style={[ Styles.container, this.props.style ]}
				/>
			)
		}
	}
)
