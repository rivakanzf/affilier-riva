import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import OrderDetailService from 'app/services/order.detail';

import BoxBit from 'modules/bits/box';
import NavTextTitleBit from 'modules/bits/nav.text.title';
import NavTextBit from 'modules/bits/nav.text';

import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';
import LoaderLego from 'modules/legos/loader';
import PriceLego from 'modules/legos/price';
import PromoLego from 'modules/legos/promo';
import RowImageUserLego from 'modules/legos/row.image.user';
import ShipmentListLego from 'modules/legos/shipment.list';

import AddressesComponent from 'modules/components/addresses';

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view';
import QuickViewItemPagelet from 'modules/pagelets/quick.view.item';
import QuickViewVoucherPagelet from 'modules/pagelets/quick.view.voucher';

import ActionsPart from './_actions'

import Styles from './style'

import { capitalize, chunk } from 'lodash'


export default ConnectHelper(
	class QuickViewOrderDetailPagelet extends ConnectedStatefulModel {

		static TYPES = {
			INVENTORY: 'INVENTORY',
			STYLESHEET: 'STYLESHEET',
			STYLEBOARD: 'STYLEBOARD',
			VOUCHER: 'VOUCHER',
			WALLET_TRANSACTION: 'WALLET_TRANSACTION',
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				onClose: PropTypes.func,
				onNavigate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		// static propsToPromise(state, p) {
		// 	return OrderDetailService.getDetailOrderDetail(p.id, {
		// 		with_discount: true,
		// 		with_exchange: true,
		// 		with_request: true,
		// 		with_shipment: true,
		// 		with_order: true,
		// 	}, state.me.token).then(res => {
		// 		return res
		// 	})
		// }
		constructor(p) {
			super(p, {
				isLoading: true,
				data: {
					request: {},
				},
			})
		}

		componentDidMount() {
			this.getData()
		}

		getData = () => {
			OrderDetailService.getDetailOrderDetail(this.props.id, {
				with_discount: true,
				with_exchange: true,
				with_request: true,
				with_shipment: true,
				with_order: true,
			}, this.props.token).then(res => {
				this.setState({
					isLoading: false,
					data: res,
				})
			})
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		onNavigateToDetail = () => {
			switch(this.state.data.type) {
			case 'INVENTORY':
				this.props.utilities.alert.modal({
					component: (
						<QuickViewItemPagelet type={ this.state.data.type } id={ this.state.data.ref_id } />
					),
				})

				return

			case 'STYLESHEET':
				this.onClose()
				this.props.onNavigate('stylesheet/' + this.state.data.ref_id)
				return

			case 'STYLEBOARD':
				this.onNavigateToStyleboardDetail(this.state.data.ref_id)
				return

			case 'VOUCHER':
				this.props.utilities.alert.modal({
					component: (
						<QuickViewVoucherPagelet id={ this.state.data.ref_id } />
					),
				})
			case 'WALLET_TRANSACTION':
			default:
				return
			}
		}

		onNavigateToVoucherDetail = id => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewVoucherPagelet id={ id } />
				),
			})
		}

		onNavigateToStyleboardDetail = id => {
			this.onClose()
			this.props.onNavigate('styleboard/' + id)
		}

		onNavigateToStylecardDetail = id => {
			this.onClose()
			this.props.onNavigate('stylecard/' + id)
		}

		onNavigateToOtherDetail = id => {
			this.onClose()

			this.props.utilities.alert.modal({
				component: (
					<QuickViewOrderDetailPagelet
						id={ id }
						token={this.props.token}
						utilities={this.props.utilities}
						onNavigate={ this.props.onNavigate }
					/>
				),
			})
		}

		onChangeStatus = status => {
			this.state.data.status = status

			this.forceUpdate()
		}

		onBookInventory = id => {
			this.state.data.ref_id = id

			this.forceUpdate()
		}

		onUnbookInventory = () => {
			this.state.data.ref_id = null

			this.forceUpdate()
		}

		metadataRenderer = metadata => {
			return chunk(Object.keys(metadata).filter(k => k !== 'address'), 2).map(keys => {
				return {
					data: keys.map(key => {
						switch(key) {
						case 'styleboard_id':
							return {
								title: 'Style Board',
								children: (
									<NavTextBit
										title={ `#SB-${ metadata[key] }` }
										link={ 'View Style Board' }
										onPress={ this.onNavigateToStyleboardDetail.bind(this, metadata[key]) }
									/>
								),
							}
						case 'stylecard_id':
							return {
								title: 'Style Card',
								children: (
									<NavTextBit
										title={ `#SC-${ metadata[key] }` }
										link={ 'View Style Card' }
										onPress={ this.onNavigateToStylecardDetail.bind(this, metadata[key]) }
									/>
								),
							}
						case 'note':
						case 'merchant':
						case 'outfit':
						case 'attire':
						case 'product':
						case 'lineup':
						case 'name':
						case 'email':
						case 'code':
							return {
								title: capitalize(key),
								content: metadata[key],
							}
						case 'amount':
							return {
								title: 'Amount',
								content: `IDR ${ FormatHelper.currency(metadata[key]) }`,
							}
						case 'is_physical':
							return {
								title: 'Physical/Digital',
								content: metadata[key] ? 'Physical' : 'Digital',
							}
						case 'asset_ids':
							return {
								headerless: true,
								children: (
									<RowImageUserLego title="User Assets" id={ this.state.data.user_id } assetIds={ metadata[key] } />
								),
							}
						case 'stylecard_variant_ids':
							return {
								title: 'Style Card Variant Ids',
								content: metadata[key].join(', '),
							}
						case 'stylesheet_inventory_ids':
							return {
								title: 'Style Sheet Inventory Ids',
								content: metadata[key].join(', '),
							}
						default:
							return {
								title: key.split('_').map(t => capitalize(t)).join(' '),
								content: metadata[key],
							}
						}
					}),
				}
			})
		}

		shipmentRenderer = shipment => {
			return (
				<ShipmentListLego
					key={ shipment.id }
					id={ shipment.id }
					isReturn={ shipment.is_return }
					courier={ shipment.courier }
					service={ shipment.service }
					awb={ shipment.awb }
					status={ shipment.status }
					editable={ false }
					style={ Styles.shipment }
				/>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return this.state.isLoading ? this.viewOnLoading() : (
				<ModalQuickViewPagelet
					title={ `Order Detail #OD-${ this.props.id }` }
					layout="free"
					onClose={ this.onClose }
				>
					<BoxBit row style={ Styles.container }>
						<BoxBit style={ Styles.column }>
							<BoxRowLego borderless
								title="General"
								data={[{
									data: [{
										title: 'Product',
										children: (
											<NavTextTitleBit
												title={ this.state.data.title }
												description={ this.state.data.description }
											/>
										),
									}],
								}, {
									data: [{
										title: 'Type',
										content: this.state.data.type,
										children: this.state.data.ref_id && this.state.data.type !== 'WALLET_TRANSACTION' ? (
											<NavTextBit
												title={ `${ this.state.data.type }${ this.state.data.ref_id ? ` - #${ this.state.data.ref_id }` : '' }` }
												link={ 'View Reference' }
												href={ ' ' }
												onPress={ this.onNavigateToDetail }
											/>
										) : undefined,
									}, {
										title: 'Status',
										children: (
											<BadgeStatusLego status={ this.state.data.status } />
										),
									}],
								}, {
									data: [{
										title: 'Price',
										content: `IDR ${ FormatHelper.currency(this.state.data.price) }`,
										children: this.state.data.price !== this.state.data.retail_price ? (
											<PriceLego align="left"
												price={ this.state.data.price }
												retailPrice={ this.state.data.retail_price }
											/>
										) : undefined,
									}, {
										title: 'Handling/Styling Fee',
										content: `IDR ${ FormatHelper.currency(this.state.data.handling_fee) }`,
									}],
								}, {
									data: [{
										title: 'Exchange',
										content: '-',
										children: this.state.data.exchange ? (
											<NavTextBit
												title={ `#OD-${ this.state.data.exchange }` }
												href={ ' ' }
												link={ 'View Order Detail' }
												onPress={ this.onNavigateToOtherDetail.bind(this, this.state.data.exchange) }
											/>
										) : undefined,
									}, {
										title: 'Replacement',
										content: '-',
										children: this.state.data.replacing ? (
											<NavTextBit
												title={ `#OD-${ this.state.data.replacing }` }
												href={ ' ' }
												link={ 'View Order Detail' }
												onPress={ this.onNavigateToOtherDetail.bind(this, this.state.data.replacing) }
											/>
										) : undefined,
									}],
								}, {
									data: [{
										title: 'Physical/Digital',
										content: this.state.data.is_digital ? 'Digital' : 'Physical',
									}],
								}, {
									data: [{
										title: 'Campaign & Coupons',
										children: (
											<PromoLego campaigns={ this.state.data.campaigns } coupons={ this.state.data.coupons } />
										),
									}],
								}]}
							/>
							<BoxRowLego borderless
								title="Request Detail"
								style={ Styles.header }
								data={[{
									data: [{
										title: 'Type',
										children: this.state.data.request.type === 'VOUCHER' ? (
											<NavTextBit
												title={ `${ this.state.data.request.type } – #VO-${ this.state.data.request.ref_id }` }
												link={ 'View Voucher' }
												href={ ' ' }
												onPress={ this.onNavigateToVoucherDetail.bind(this, this.state.data.request.ref_id) }
											/>
										) : (
											<NavTextTitleBit
												title={ this.state.data.request.type }
												description={ `#${ this.state.data.request.ref_id }` }
											/>
										),
									}, {
										title: 'As Voucher',
										content: this.state.data.request.as_voucher ? 'Yes' : 'No',
									}],
								}, ...this.metadataRenderer(this.state.data.request.metadata)]}
							/>
						</BoxBit>
						<BoxBit>
							<ActionsPart
								type={ this.state.data.request.type }
								id={ this.state.data.id }
								title={ this.state.data.title }
								refId={ this.state.data.ref_id }
								variantId={ this.state.data.request.ref_id }
								status={ this.state.data.status }
								onStatusChanged={ this.onChangeStatus }
								onInventoryBooked={ this.state.data.type === 'INVENTORY' && !this.state.data.ref_id ? this.onBookInventory : undefined }
								onInventoryUnbooked={ this.state.data.type === 'INVENTORY' && this.state.data.ref_id ? this.onUnbookInventory : undefined }
							/>
							<BoxRowLego borderless unflex={ false }
								title="Shipments"
								style={ Styles.header }
								data={[{
									data: [{
										headerless: true,
										children: (
											<AddressesComponent
												type="order"
												typeId={ this.state.data.order_id }
												data={ [this.state.data.request.address] }
											/>
										),
									}, {
										title: 'Shipment At',
										content: TimeHelper.format(this.state.data.shipment_at, 'DD MMMM YYYY HH:mm'),
									}],
								}, {
									data: [{
										title: '',
										children: (
											<BoxBit unflex style={ Styles.shipments }>
												{ this.state.data.shipments.map(this.shipmentRenderer) }
											</BoxBit>
										),
									}],
								}]}
							/>
						</BoxBit>
					</BoxBit>
				</ModalQuickViewPagelet>
			)
		}
	}
)
