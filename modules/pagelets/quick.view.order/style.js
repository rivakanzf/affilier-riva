import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	empty: {
		paddingTop: Sizes.margin.default,
		minHeight: 300,
	},

	error: {
		color: Colors.primary,
		marginLeft: 8,
	},

	container: {
		paddingBottom: 0,
		paddingLeft: 0,
		paddingRight: 0,
	},

	table: {
		marginTop: Sizes.margin.default,
		marginBottom: - StyleSheet.hairlineWidth,
	},

})
