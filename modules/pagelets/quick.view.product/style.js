import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	detail: {
		paddingTop: 8,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	scroll: {
		direction: 'rtl',
	},

	color: {
		padding: 3,
		marginRight: 4,
		width: 34,
		height: 34,
		marginBottom: 4,
	},

	colorActive: {
		padding: 3,
		borderColor: Colors.grey.palette(3, .8),
		borderWidth: 2,
		borderRadius: 24,
	},

	size: {
		wordBreak: 'break-all',
	},

	tags: {
		borderRadius: 4,
		backgroundColor: Colors.grey.palette(2),
		marginRight: 4,
		marginBottom: 4,
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: 12,
		paddingRight: 12,
	},

	type: {
		color: Colors.black.palette(2, .6),
		marginBottom: 4,
	},

	tag: {
		// wordBreak: 'break-all',
		color: Colors.black.palette(2, .9),
	},

	wrapper: {
		marginTop: 16,
		flexWrap: 'wrap',
	},

	button: {
		marginLeft: 8,
		marginRight: 8,
		marginBottom: 8,
	},

	buttonMargin: {
		marginRight: 0,
	},

})
