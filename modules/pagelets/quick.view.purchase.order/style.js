import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		paddingBottom: 0,
		paddingLeft: 0,
		paddingRight: 0,
	},

	table: {
		marginTop: Sizes.margin.default,
		marginBottom: - StyleSheet.hairlineWidth,
	},

	product: {
		justifyContent: 'center',
		alignItems: 'flex-start',
		overflow: 'hidden',
	},

	title: {
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		width: '100%',
	},

	seller: {
		color: Colors.primary,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	retail: {
		textDecoration: 'line-through',
	},

	discounted: {
		color: Colors.red.palette(7),
	},

})
