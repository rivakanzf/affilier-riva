import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';
import FormatHelper from 'coeur/helpers/format';

import Colors from 'utils/constants/color'

import PurchaseService from 'app/services/purchase';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import DateWarningBit from 'modules/bits/date.warning';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LinkBit from 'modules/bits/link';
import NavTextBit from 'modules/bits/nav.text';

import BadgeStatusLego from 'modules/legos/badge.status';
// import CompletionLego from 'modules/legos/completion';
import LoaderLego from 'modules/legos/loader';
import RowsLego from 'modules/legos/rows';

import ModalClosablePagelet from 'modules/pagelets/modal.closable';

import Styles from './style';


export default ConnectHelper(
	class QuickViewPurchasePagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				data: PropTypes.shape({
					title: PropTypes.string,
					description: PropTypes.string,
					status: PropTypes.string,
					url: PropTypes.string,
					note: PropTypes.string,
					price: PropTypes.number,
					retail_price: PropTypes.number,
					shipment_at: PropTypes.date,
					created_at: PropTypes.date,
					user_id: PropTypes.id,
					user_name: PropTypes.string,
					stylesheet_id: PropTypes.id,
					stylesheet_title: PropTypes.string,
				}),
				disabled: PropTypes.bool,
				onClose: PropTypes.func,
				onNavigateToClient: PropTypes.func,
				onNavigateToStylesheet: PropTypes.func,
				onApprove: PropTypes.func,
				onReject: PropTypes.func,
			}
		}

		static propsToPromise(state, p) {
			return PurchaseService.getDetail(p.id, {}, state.me.token)
		}

		getShipmentStyle(shipmentDate) {
			const diff = TimeHelper.moment(shipmentDate).diff(new Date(), 'd')

			if (diff > 3) {
				return Styles.note
			} else if (diff === 3) {
				return Styles.warning
			} else {
				return Styles.alert
			}
		}

		viewOnError() {
			return (
				<BoxBit row centering style={Styles.empty}>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.error}>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalClosablePagelet
					title={ 'Purchase Request Preview' }
					header={( <BadgeStatusLego status={ this.props.data.status } /> )}
					footer={(
						<React.Fragment>
							<ButtonBit
								title={ 'Close' }
								weight="medium"
								size={ ButtonBit.TYPES.SIZES.SMALL }
								theme={ ButtonBit.TYPES.THEMES.SECONDARY }
								onPress={ this.props.onCancel }
							/>
							<BoxBit />
							<ButtonBit
								title={ 'Reject' }
								weight="medium"
								size={ ButtonBit.TYPES.SIZES.SMALL }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								theme={ ButtonBit.TYPES.THEMES.SECONDARY }
								state={ this.props.disabled ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.props.onReject }
								style={ Styles.button }
							/>
							<ButtonBit
								title={ 'Approve' }
								weight="medium"
								size={ ButtonBit.TYPES.SIZES.SMALL }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								state={ this.props.disabled ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.props.onApprove }
							/>
						</React.Fragment>
					)}
					confirm="View Order"
					onClose={ this.props.onClose }
					contentContainerStyle={ Styles.container }
				>
					<RowsLego
						data={[{
							data: [{
								title: 'Stylesheet',
								children: (
									<NavTextBit
										title={ `${ this.props.data.stylesheet_title }` }
										link={ `View Stylesheet #${ this.props.data.stylesheet_id }` }
										href={ `${this.props.data.stylesheet_id}` }
										onPress={ this.props.onNavigateToStylesheet }
									/>
								),
							}, {
								title: 'Shipment Date',
								children: (
									<DateWarningBit date={ this.props.data.shipment_at } mark={[2, 5]} />
								),
								content: TimeHelper.format(this.props.data.shipment_at, 'DD MMMM YYYY'),
							}],
						}]}
						style={ Styles.box }
					/>
					<RowsLego
						data={[{
							data: [{
								title: 'Product',
								children: (
									<React.Fragment>
										<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
											{ this.props.data.title }
										</GeomanistBit>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
											{ this.props.data.description }
										</GeomanistBit>
									</React.Fragment>
								),
							}, {
								title: 'Requestor',
								children: (
									<NavTextBit
										title={ this.props.data.user_name }
										link={ 'View User' }
										href={ `${this.props.data.user_id}` }
										onPress={ this.props.onNavigateToClient }
									/>
								),
							}],
						}, {
							data: [{
								title: 'Retail Price',
								content: `IDR ${ FormatHelper.currency(this.props.data.retail_price) }`,
							}, {
								title: 'Purchase Price',
								content: `IDR ${ FormatHelper.currency(this.props.data.price || this.props.data.retail_price) }`,
							}],
						}, {
							data: [{
								title: 'Request Date',
								content: TimeHelper.format(this.props.data.created_at, 'DD MMMM YYYY [—] HH:mm'),
							}, {
								title: 'Note',
								content: this.props.data.note || '-',
							}],
						}, {
							data: [{
								title: 'URL',
								children: (
									<LinkBit underline target={ LinkBit.TYPES.BLANK } href={ this.props.data.url }>
										{ this.props.data.url }
									</LinkBit>
								),
							}],
						}]}
						style={ Styles.content }
					/>
				</ModalClosablePagelet>
			)
		}
	}
)
