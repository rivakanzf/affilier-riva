import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	empty: {
		paddingTop: Sizes.margin.default,
		minHeight: 300,
	},

	content: {
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingBottom: Sizes.margin.thick,
	},

	image: {
		width: 90,
		height: 90,
		marginRight: Sizes.margin.default,
	},

	badge: {
		alignSelf: 'flex-end',
		marginTop: Sizes.margin.default,
	},

})
