/* eslint-disable no-nested-ternary */
import React from 'react'
import QueryStatefulModel from 'coeur/models/query.stateful'
import ConnectHelper from 'coeur/helpers/connect'
import StringHelper from 'coeur/helpers/string';

import PredictorService from 'app/services/predictor'
// import UserService from 'app/services/user.new'

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box'
import ImageBit from 'modules/bits/image'
import SpectralBit from 'modules/bits/spectral'
import LoaderBit from 'modules/bits/loader'
import TouchableBit from 'modules/bits/touchable'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'
import ShadowBit from 'modules/bits/shadow'
import RadioBit from 'modules/bits/radio'
import IconBit from 'modules/bits/icon'
// import InputValidatedBit from 'modules/bits/input.validated'
// import TextInputBit from 'modules/bits/text.input'

import TabLego from 'modules/legos/tab'
// import RowCategoryLego from 'modules/legos/row.category';
import SelectCategoryLego from 'modules/legos/select.category';

import ImageEditorRecommendationPagelet from 'modules/pagelets/image.editor.recommendation'
import ModalClosablePagelet from 'modules/pagelets/modal.closable'

import CardVariantComponent from 'modules/components/card.variant'

import { uniq, isEmpty, orderBy } from 'lodash'

import Styles from './style';

let colorsListCache = []

export default ConnectHelper(
	class RecommendationsPagelet extends QueryStatefulModel {
		
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				onClose: PropTypes.func,
				stylecardId: PropTypes.number,
				stylecardStatus: PropTypes.string,
				image: PropTypes.string,
				imageLink: PropTypes.string,
				variants: PropTypes.array,
				itemCount: PropTypes.number,
				onAddItem: PropTypes.func,
			}
		}
		
		static contexts = [
			UtilitiesContext,
		]
		
		constructor(p) {
			super(p, {
				isLoading: true,
				tabIndex: 0,
				tabMethodIndex: 0,
				categories: [],
				manualCategories: [],
				data: [],
				manualData: [],
				croppedImages: [],
				itemCount: 0,
				filterChange: false,
			})
		}

		static propsToQuery(state) {
			return [`query {
				colorsList {
					id, title, hex, image
				}
			}`, {
				skip: !isEmpty(colorsListCache),
			}, state.me.token]
		}

		static getDerivedStateFromProps(nP) {
			if(!isEmpty(nP.data)) {
				const { colorsList } = nP.data

				const useTitleGroup = title => {
					let text = title
					if(
						title === 'purple' ||
						title === 'pink' ||
						title === 'red' ||
						title === 'yellow' ||
						title === 'green' ||
						title === 'blue' ||
						title === 'brown' ||
						title === 'beige' ||
						title === 'grey' ||
						title === 'white' ||
						title === 'gold'
					) {
						text = title + '_group'
					}
					return text
				}

				const compareStr = (str1, str2) => {
					return str1.toLowerCase() === str2
				}

				colorsListCache = orderBy(colorsList.reduce((acc, color) => {
					const title = color.title.toLowerCase()
					if(
						title === 'purple' ||
						title === 'pink' ||
						title === 'red' ||
						title === 'yellow' ||
						title === 'green' ||
						title === 'blue' ||
						title === 'brown' ||
						title === 'beige' ||
						title === 'grey' ||
						title === 'white' ||
						title === 'gold' ||

						title === 'orange' ||
						title === 'black' ||
						title === 'silver' ||
						title === 'multi color' ||
						title === 'no color'
					) {
						acc.push({
							...color,
							title: useTitleGroup(title),
							// spq tagging color
							subColors: title === 'purple' ? colorsList.filter(c => {

								return compareStr(c.title, 'lilac') ||
								compareStr(c.title, title)

							}) : title === 'pink' ? colorsList.filter(c => {

								return compareStr(c.title, 'fuschia') ||
								compareStr(c.title, 'dusty pink') ||
								compareStr(c.title, title)

							}) : title === 'red' ? colorsList.filter(c => {

								return compareStr(c.title, 'maroon') ||
								compareStr(c.title, title)

							}) : title === 'yellow' ? colorsList.filter(c => {

								return compareStr(c.title, 'mustard') ||
								compareStr(c.title, 'cream') ||
								compareStr(c.title, 'light yellow') ||
								compareStr(c.title, title)

							}) : title === 'green' ? colorsList.filter(c => {

								return compareStr(c.title, 'khaki') ||
								compareStr(c.title, 'emerald') ||
								compareStr(c.title, 'olive') ||
								compareStr(c.title, 'mint') ||
								compareStr(c.title, title)

							}) : title === 'blue' ? colorsList.filter(c => {

								return compareStr(c.title, 'teal') ||
								compareStr(c.title, 'turquoise') ||
								compareStr(c.title, 'dark blue') ||
								compareStr(c.title, 'light blue') ||
								compareStr(c.title, title)

							}) : title === 'brown' ? colorsList.filter(c => {

								return compareStr(c.title, 'dark brown') ||
								compareStr(c.title, 'camel') ||
								compareStr(c.title, 'sand') ||
								compareStr(c.title, title)

							}) : title === 'beige' ? colorsList.filter(c => {

								return compareStr(c.title, 'nude') ||
								compareStr(c.title, 'ecru') ||
								compareStr(c.title, title)

							}) : title === 'grey' ? colorsList.filter(c => {

								return compareStr(c.title, 'dark grey') ||
								compareStr(c.title, title)

							}) : title === 'white' ? colorsList.filter(c => {

								return compareStr(c.title, 'off white') ||
								compareStr(c.title, title)

							}) : title === 'gold' ? colorsList.filter(c => {

								return compareStr(c.title, 'rose gold') ||
								compareStr(c.title, title)

							}) : [],
						})
					}
					return acc
				}, []), ['title'], 'asc')
			}
			return false
		}

		componentDidMount() {
			if (this.props.imageLink) {
				this.getData(this.props.imageLink)
			} else {
				this.getData('http://res.cloudinary.com/dh56t7y6j/image/upload/v1621844304/' + this.props.image)
			}
		}

		onChangeTab = i => {
			this.setState({
				tabIndex: i,
			})
		}
		
		onChangeMethodTab = i => {
			this.setState(state => ({
				isLoading: state.tabMethodIndex === 0 && i === 1 ? false : state.isLoading,
				tabMethodIndex: i,
			}))
		}

		getData = (imageLink) => {
			this.setState({
				itemCount: this.props.itemCount,
			})
			PredictorService.getRecommendations(imageLink, 30)
				.then(res => {
					const categories = uniq(res.slice(1).map(data => data[0].category))
					this.setState({
						data: res,
						categories,
						isLoading: false,
					})
					// return UserService.upload(this.props.id, { image: res[0] }, this.props.token)
				})
				.catch(() => {
					this.setState({
						isLoading: false,
					})
				})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		toCropImage = (source) => {
			this.props.utilities.alert.modal({
				component: (
					<ImageEditorRecommendationPagelet
						// variantId={ this.props.variantId }
						source={source}
						onPress={this.onModalRequestClose}
						onAddCroppedImage={ this.onAddCroppedImage }
						// newIndex={this.state.images.length}
						// transform={this.transform}
					/>
				),
			})
		}

		onAddCroppedImage = (croppedImage, coor, scale) => {
			const tempCroppedImages = this.state.croppedImages
			tempCroppedImages.push({
				image: croppedImage,
				coor: {
					x1: Math.ceil(coor.x1 * scale.scaleX),
					y1: Math.ceil(coor.y1 * scale.scaleY),
					x2: Math.ceil(coor.x2 * scale.scaleX),
					y2: Math.ceil(coor.y2 * scale.scaleY),
				},
				label: '',
				filter: {
					color: {
						show: false,
						list: colorsListCache,
						selected: null,
					},
					size: {
						show: false,
						list: ['regular', 'petite', 'plus', 'tall'],
						selected: null,
					},
					brand: {
						show: false,
						list: ['consign', 'internal', 'external'],
						selected: null,
					},
				},
			})
			this.setState({
				croppedImages: tempCroppedImages,
			})
			this.props.utilities.alert.hide()
		}

		onChangeCategory = (label, i) => {
			const tempCroppedImages = this.state.croppedImages
			tempCroppedImages[i].label = label
			this.setState({
				croppedImages: tempCroppedImages,
			})
		}

		onReset = () => {
			this.setState({
				croppedImages: [],
				manualData: [],
			})
		}

		onAddItemRec = () => {
			this.setState((prevState) => ({
				itemCount: prevState.itemCount + 1,
			}))
		}

		onShowResult = () => {
			const imageLink = 'http://res.cloudinary.com/dh56t7y6j/image/upload/v1621844304/' + this.props.image
			const listXy = this.state.croppedImages.map(xy => {
				return `(${xy.coor.x1},${xy.coor.y1},${xy.coor.x2},${xy.coor.y2})`
			})

			const listQuery = this.state.croppedImages.map(xy => {
				let query = `"category=${xy.label}`

				if(!!xy.filter.color.selected) {
					query += `,primary_color=${xy.filter.color.selected}`
				}
				if(!!xy.filter.size.selected) {
					query += `,size_group=${xy.filter.size.selected}`
				}
				if(!!xy.filter.brand.selected) {
					query += `,brand_group=${xy.filter.brand.selected}`
				}

				query += '"'
				return query
			})

			this.setState({
				isLoading: true,
			}, () => {
				PredictorService.getManualRecommendations(imageLink, 50, listXy, listQuery)
					.then(res => {
						this.setState(state => ({
							manualData: res,
							manualCategories: state.croppedImages.map(data => data.label),
							isLoading: false,
						}))
					})
					.catch(() => {
						this.setState({
							isLoading: false,
						})
					})
			})
		}

		croppedRenderer = (item, i) => {
			return (
				<BoxBit row key={ i } style={ Styles.croppedItem }>
					<ImageBit source={ item.image } style={ Styles.croppedImage }/>
					<SelectCategoryLego
						key={ `cat_${this.props.categoryId}` }
						placeholder={
							item.label || 'Select Category'
						}
						onChange={(id, label) => this.onChangeCategory(label, i) }
					/>
				</BoxBit>
			)
		}

		onApplyFilter = () => {
			this.setState(state => ({
				filterChange: false,
				croppedImages: state.croppedImages.map(data => {
					return {
						...data,
						filter: {
							color: {
								...data.filter.color,
								show: false,
							},
							size: {
								...data.filter.size,
								show: false,
							},
							brand: {
								...data.filter.brand,
								show: false,
							},
						},
					}
				}),
			}), this.onShowResult)
		}

		onSelectFilterItem = (title, key) => {
			const { croppedImages } = this.state
			const { selected } = croppedImages[this.state.tabIndex].filter[key]

			croppedImages[this.state.tabIndex].filter[key].selected = selected !== title
				? title
				: null

			this.setState({
				filterChange: true,
				croppedImages,
			})
		}

		onShowFilterItem = key =>{
			const { croppedImages, tabIndex } = this.state
			croppedImages[tabIndex].filter[key].show =
			!croppedImages[tabIndex].filter[key].show

			this.setState({
				croppedImages,
			})
		}

		filterItemSubColorRenderer = (data, selected) => {
			return (
				<TouchableBit key={ data.id } row style={ Styles.filterTabItemSub } onPress={() => this.onSelectFilterItem(data.title, 'color')}>
					<BoxBit unflex style={[Styles.filterTabItemColor, data.title === selected ? Styles.filterTabItemColorSelected : {}]}>
						<BoxBit style={[Styles.filterTabItemColorBG, { backgroundColor: data.hex }]}/>
					</BoxBit>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.filterTabItemTitle }>
						{ data.title || data }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		filterItemRenderer = (data, type, selected) => {
			return !!type ? (
				<BoxBit key={ data.title } style={ Styles.filterTabItem }>
					<TouchableBit row onPress={() => this.onSelectFilterItem(data.title || data, type)} style={ Styles.filterTabItemAlign }>
						{ type === 'color' ? (
							<BoxBit unflex style={[Styles.filterTabItemColor, data.title === selected ? Styles.filterTabItemColorSelected : {}]}>
								{ data.title === 'multi color' ? (
									<ImageBit unflex={ false } broken={ !data.image } source={ data.image } style={ Styles.filterTabItemColorBG }/>
								) : data.title.indexOf('_') > -1 ? (
									<BoxBit style={ Styles.filterTabItemColorNoBG }/>
								) : (
									<BoxBit style={[Styles.filterTabItemColorBG, { backgroundColor: data.hex }]}/>
								) }
							</BoxBit>
						) : (
							<RadioBit
								dumb
								isActive={ data === selected }
								style={ Styles.filterTabItemCheckBox }
								onPress={() => this.onSelectFilterItem(data, type)}/>
						) }

						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.filterTabItemTitle }>
							{ data.title ? data.title.replace('_', ' ') : data }
						</GeomanistBit>
					</TouchableBit>
					{ data.subColors && data.subColors.length ? (
						<BoxBit style={[Styles.filterTabItemSubs, data.title === selected ? Styles.filterTabItemSelected : {}]}>
							{ data.subColors.map(d => this.filterItemSubColorRenderer(d, selected)) }
						</BoxBit>
					) : false }
				</BoxBit>
			) : false
		}

		filterRenderer = data => {
			const [ colorSelected, sizeSelected, brandSelected ] =
			[ data.filter.color.selected, data.filter.size.selected, data.filter.brand.selected ]

			return (
				<BoxBit unflex row style={ Styles.prl16 }>
					<ButtonBit
						title="Apply"
						state={ this.state.filterChange ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
						theme={ ButtonBit.TYPES.THEMES.BLACK }
						style={[Styles.filterTab, Styles.filterTabUseButton]}
						onPress={ this.onApplyFilter }
					/>
					<TouchableBit unflex style={ Styles.filterTab } onPress={() => this.onShowFilterItem('color')}>
						<BoxBit unflex row style={ Styles.filterTabTitleContainer }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={[Styles.filterTabTitle, Styles.mr8]}>
								{ data.filter.color.selected
									? `Color: ${data.filter.color.selected.replace('_', ' ')}`
									: 'Colors'
								}
							</GeomanistBit>
							<IconBit name="dropdown"/>
						</BoxBit>
						{ data.filter.color.show && (
							<ShadowBit y={ 0 } blur={ 24 } style={ Styles.filterTabItems }>
								{ data.filter.color.list.map(list => {
									return this.filterItemRenderer(list, 'color', colorSelected)
								}) }
							</ShadowBit>
						) }
					</TouchableBit>
					<TouchableBit unflex style={ Styles.filterTab } onPress={() => this.onShowFilterItem('size')}>
						<BoxBit unflex row style={ Styles.filterTabTitleContainer }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={[Styles.filterTabTitle, Styles.mr8]}>
								{ data.filter.size.selected
									? `Size: ${data.filter.size.selected}`
									: 'Sizes'
								}
							</GeomanistBit>
							<IconBit name="dropdown"/>
						</BoxBit>
						{ data.filter.size.show && (
							<ShadowBit y={ 0 } blur={ 24 } style={ Styles.filterTabItems }>
								{ data.filter.size.list.map(list => {
									return this.filterItemRenderer(list, 'size', sizeSelected)
								}) }
							</ShadowBit>
						) }
					</TouchableBit>
					<TouchableBit unflex style={ Styles.filterTab } onPress={() => this.onShowFilterItem('brand')}>
						<BoxBit unflex row style={ Styles.filterTabTitleContainer }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={[Styles.filterTabTitle, Styles.mr8]}>
								{ data.filter.brand.selected
									? `Brand: ${data.filter.brand.selected}`
									: 'Brands'
								}
							</GeomanistBit>
							<IconBit name="dropdown"/>
						</BoxBit>
						{ data.filter.brand.show && (
							<ShadowBit y={ 0 } blur={ 24 } style={ Styles.filterTabItems }>
								{ data.filter.brand.list.map(list => {
									return this.filterItemRenderer(list, 'brand', brandSelected)
								}) }
							</ShadowBit>
						) }
					</TouchableBit>
					{/* <TouchableBit unflex style={ Styles.filterTab } onPress={() => this.onShowFilterItem('price')}>
						<BoxBit unflex row style={ Styles.filterTabTitleContainer }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={[Styles.filterTabTitle, Styles.mr8]}>
								{ this.state.filter.price.min
									? `Price: Min ${this.state.filter.price.min}${this.state.filter.price.max && ` - Max ${this.state.filter.price.max}`}`
									: 'Price'
								}
							</GeomanistBit>
							<IconBit name="dropdown"/>
						</BoxBit>
						{ this.state.filter.price.show && (
							<ShadowBit y={ 0 } blur={ 24 } style={[Styles.filterTabItems, Styles.filterTabItemsFit]}>
								<BoxBit row style={ Styles.mb8 }>
									<BoxBit style={ Styles.mr8 }>
										<GeomanistBit type={ GeomanistBit.TYPES.CAPTION_1 } weight="medium">
											Min
										</GeomanistBit>
										<TextInputBit
											defaultValue={ this.state.filter.price.min }
											onChange={ data => this.onChangePrice(data, 'min') }
											inputStyle={ Styles.filterTabItemPriceInput }
										/>
									</BoxBit>
									<BoxBit>
										<GeomanistBit type={ GeomanistBit.TYPES.CAPTION_1 } weight="medium">
											Max
										</GeomanistBit>
										<TextInputBit
											defaultValue={ this.state.filter.price.max }
											onChange={ data => this.onChangePrice(data, 'max') }
											inputStyle={ Styles.filterTabItemPriceInput }
										/>
									</BoxBit>
								</BoxBit>
							</ShadowBit>
						) }
					</TouchableBit> */}
				</BoxBit>
			)
		}

		recommendationsRenderer = () => {
			const data = !this.state.tabMethodIndex ? this.state.data.slice(1) : this.state.manualData.slice(1)
			const categories = !this.state.tabMethodIndex ? this.state.categories : this.state.manualCategories

			return (
				<BoxBit row style={ Styles.recommendWrapper }>
					<BoxBit unflex style={[Styles.recommendLeftSide, Styles.prl32]}>
						<TabLego
							activeIndex={ this.state.tabMethodIndex }
							tabs={[{
								title: 'Auto',
								onPress: this.onChangeMethodTab.bind(this, 0),
							}, {
								title: 'Manual',
								onPress: this.onChangeMethodTab.bind(this, 1),
							}]}
							style={ Styles.methodTab }
						/>
						<TouchableBit unflex onPress={ this.state.tabMethodIndex === 0 ? {} : () => this.toCropImage( 'http://res.cloudinary.com/dh56t7y6j/image/upload/v1621844304/' + this.props.image) } style={ Styles.mb16 }>
							<ImageBit
								// source={ '//' + this.props.image }
								key={ this.state.tabMethodIndex }
								source={ this.state.tabMethodIndex === 0
									? {url: this.state.data[0]}
									: data.length
										? {url: this.state.manualData[0]}
										: '//' + this.props.image
								}
								transform={{crop: 'fit'}}
								style={ Styles.imageTarget }
							/>
						</TouchableBit>
						{ this.state.tabMethodIndex === 1 && this.state.croppedImages.length === 0 &&
							<BoxBit unflex style={[Styles.caption, Styles.prl16]}>
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
									Click on the image and move your cursor to select the object. Sizing handles indicate that an object has been selected.
								</GeomanistBit>
							</BoxBit>
						}
						{ this.state.tabMethodIndex === 1 &&
							<BoxBit unflex>
								{ !!this.state.croppedImages.length > 0 &&
									this.state.croppedImages.map(this.croppedRenderer)
								}
							</BoxBit>
						}
						{ this.state.tabMethodIndex === 1 && this.state.croppedImages.length > 0 &&
							<BoxBit row style={{justifyContent: 'space-between', paddingBottom: 15}}>
								<ButtonBit
									title="Reset"
									theme={ButtonBit.TYPES.THEMES.WHITE}
									// state={ this.props.isResettable ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
									onPress={this.onReset}
								/>
								<ButtonBit
									title="Show Result"
									theme={ButtonBit.TYPES.THEMES.BLACK}
									// state={ this.props.isResettable ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
									onPress={this.onShowResult}
								/>
							</BoxBit>
						}
					</BoxBit>
					<BoxBit>
						{ data && data.length ? (<>
							<BoxBit unflex style={[Styles.categoryTabs, Styles.prl32]}>
								<TabLego
									activeIndex={ this.state.tabIndex }
									tabs={categories.map((category, i) => {
										return {
											title: category,
											onPress: this.onChangeTab.bind(this, i),
										}
									})}
									style={ Styles.categoryTab }
								/>
							</BoxBit>
							<BoxBit style={[Styles.containerScroll, Styles.prl16]} key={ this.state.tabIndex }>
								<BoxBit unflex row style={ !!this.state.tabMethodIndex ? Styles.containerHeaderItems : {} }>
									{ !!this.state.tabMethodIndex &&
										this.filterRenderer(this.state.croppedImages[this.state.tabIndex])
									}
									<BoxBit unflex style={[Styles.itemsAdded, Styles.prl16]}>
										<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium">
											{ StringHelper.pluralize(this.state.itemCount, 'item', '0 item', 's') } added
										</GeomanistBit>
									</BoxBit>
								</BoxBit>
								<BoxBit row style={ Styles.containerCardItems }>
									{ data[this.state.tabIndex].length
										? data.filter((prod, i) => i === this.state.tabIndex)[0].map(this.cardRecommendationRenderer)
										: (
											<BoxBit centering>
												<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
													{ !this.state.tabMethodIndex
														? 'Cannot find similar variant. Try using another image.'
														// this above is rare case. /predict/auto return no data
														
														: 'Cannot find similar variant with those filter.'
													}
												</GeomanistBit>
											</BoxBit>
										)
									}
								</BoxBit>
							</BoxBit>
						</>) : !!this.state.tabMethodIndex ? (
							<BoxBit centering>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
									No object selected
								</GeomanistBit>
							</BoxBit>
						) : (
							<BoxBit centering>
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
									An error occurred. Try again.
								</GeomanistBit>
							</BoxBit>
						) }
					</BoxBit>
				</BoxBit>
			)
		}

		cardRecommendationRenderer = (data) => {
			return (
				<CardVariantComponent
					key={ data.variant_id }
					id={ data.variant_id }
					stylecardId={ this.props.stylecardId }
					isAddable={ this.props.stylecardStatus === 'STYLING'}
					onAddItem={ this.props.onAddItem }
					onAddItemRec={ this.onAddItemRec }
					style={[Styles.cardItem, Styles.prl16]}
				/>
			)
		}

		loadingRenderer() {
			return (
				<BoxBit centering style={Styles.loadingContainer}>
					<SpectralBit>
						Analyzing image
					</SpectralBit>
					<LoaderBit simple/>
				</BoxBit>
			)
		}


		view() {
			return (
				<ModalClosablePagelet
					small={ false }
					title={ 'Recommendations' }
					onClose={ this.props.onClose }
					style={Styles.contentContainer}
					contentContainerStyle={ Styles.noPadding }
				>
					{/* {
						this.state.isLoading
							? this.loadingRenderer()
							: this.state.tabMethodIndex === 0
								? this.recommendationsRenderer(this.state.data.slice(1), this.state.categories)
								: this.recommendationsRenderer(this.state.manualData.slice(1), this.state.manualCategories)
					} */}
					{ this.state.isLoading ? this.loadingRenderer() : this.recommendationsRenderer() }
					
				</ModalClosablePagelet>
			)
		}
	}
)
