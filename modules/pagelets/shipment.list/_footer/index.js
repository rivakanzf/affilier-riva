import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';

import SelectStatusLego from 'modules/legos/select.status';

import Styles from './style';


export default ConnectHelper(
	class ShipmentListComponent extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				disabled: PropTypes.bool,
				count: PropTypes.number,
				total: PropTypes.number,
				isSelected: PropTypes.bool,
				onCreateShipment: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static defaultProps = {
			count: 0,
			total: 0,
		}

		constructor(p) {
			super(p, {
				isSendable: false,
				updater: 1,
			})

			this._method = undefined
		}

		onSelectShipmentMethod = method => {
			this._method = method

			if(!this.state.isSendable) {
				this.setState({
					isSendable: true,
				})
			}
		}

		onCreateShipment = () => {
			if(this._method) {
				this.props.onCreateShipment &&
				this.props.onCreateShipment(this._method)

				this._method = undefined

				this.setState({
					isSendable: false,
					updater: this.state.updater + 1,
				})
			}
		}

		view() {
			return (
				<BoxBit row unflex style={ Styles.footer }>
					<BoxBit unflex style={Styles.note}>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.info} >
							{ this.props.count } of { this.props.total } items shipped
						</GeomanistBit>
					</BoxBit>
					{ !this.props.disabled ? (
						<BoxBit row>
							<SelectStatusLego key={ this.state.updater } isRequired
								type={ SelectStatusLego.TYPES.SHIPMENTS }
								placeholder={ 'Select shipping provider' }
								onChange={ this.onSelectShipmentMethod }
								style={ Styles.dropdown }
							/>
							<ButtonBit
								title="Ship"
								weight="medium"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								state={ this.state.isSendable && this.props.isSelected ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								onPress={ this.onCreateShipment }
							/>
						</BoxBit>
					) : false }
				</BoxBit>
			)
		}
	}
)
