import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';
import PageContext from 'coeur/contexts/page';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';

import BoxLego from 'modules/legos/box';
import SelectionShipmentLego from 'modules/legos/selection.shipment';

import AddressListPagelet from '../address.list';
import ShipmentUpdatePagelet from '../shipment.update';

import FooterPart from './_footer'

import Styles from './style';

import { last, without } from 'lodash';


export default ConnectHelper(
	class ShipmentListPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				orderId: PropTypes.id,
				addable: PropTypes.bool,
				editable: PropTypes.bool,
				details: PropTypes.array, // order details
				addresses: PropTypes.array,
				shipments: PropTypes.array,
				onUpdate: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
			PageContext,
		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(nP.details.length !== nS.len || nP.shipments.length !== nS.shipmentLen) {
				const details = nP.details.map(detail => {
					return {
						...detail,
						// TODO: disable selection if packet is already shipped
						selectable: nP.addable && detail.status === 'PRIMED',
						shipments: detail.shipment_ids.map(sId => nP.shipments.find(s => s.id === sId)).sort((a, b) => +a.id - b.id),
					}
				})

				return {
					len: nP.details.length,
					shipmentLen: nP.shipments.length,
					selectable: nP.addable && details.reduce((sum, detail) => !sum ? detail.selectable : true, false),
					details,
					shippedItemCount: details
						.map(detail => last(detail.shipments))
						.map(shipment => shipment && shipment.status)
						.filter(status => status === 'DELIVERED' || status === 'DELIVERY_CONFIRMED').length,
				}
			}

			return null
		}

		static defaultProps = {
			details: [],
			shipments: [],
		}

		constructor(p) {
			super(p, {
				len: 0,
				shipmentLen: 0,
				selectable: false,
				selectedIds: [],
				details: [],
				shippedItemCount: 0,
			})
		}

		// onChangeAddress = address => {
		// 	this.defaultAddress = {
		// 		...this.defaultAddress,
		// 		id: address.id,
		// 		receiver: address.receiver,
		// 		phone: address.phone,
		// 		address: address.address,
		// 		district: address.district,
		// 		postal: address.postal,
		// 		metadata: address.metadata,
		// 	}
		// }

		address = addressId => {
			const address = this.props.addresses.find(a => a.id === addressId)

			return `Deliver to: #AD-${address.id} ${address.district} – ${address.postal}`
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onCreateShipment = (method, selectedIds, address) => {
			// Address is order address
			this.onModalRequestClose()

			this.props.utilities.alert.modal({
				component: (
					<ShipmentUpdatePagelet
						method={ method }
						orderId={ this.props.orderId }
						shipment={{
							destination: address,
							orderDetails: selectedIds.map(id => this.props.details.find(d => d.id === id)),
						}}
						onShipmentCreated={ this.onShipmentCreated }
					/>
				),
			})
		}

		onShipmentCreated = (rawShipment, orderDetails) => {
			const shipment = {
				id: rawShipment.id,
				amount: rawShipment.amount,
				awb: rawShipment.awb,
				courier: rawShipment.courier,
				created_at: rawShipment.created_at,
				destination_id: rawShipment.destination_id,
				is_facade: rawShipment.is_facade,
				metadata: rawShipment.metadata || {},
				note: rawShipment.note,
				origin_id: rawShipment.origin_id,
				packet_id: rawShipment.packet_id,
				prices: rawShipment.prices || {},
				service: rawShipment.service,
				status: rawShipment.status,
				type: rawShipment.type,
				url: rawShipment.url,
			}

			this.props.shipments.push(shipment)

			orderDetails.forEach(orderDetail => {
				const detail = this.props.details.find(d => d.id === orderDetail.id)

				detail.shipment_ids.push(shipment.id)
			})

			this.forceUpdate()

			this.props.onUpdate &&
			this.props.onUpdate()
		}

		onSelectAddress = method => {
			const selectedIds = this.state.selectedIds

			this.setState({
				selectedIds: [],
			}, () => {
				// Select Address
				const selectedDetail = selectedIds.length === 1 ? this.props.details.find(d => d.id === selectedIds[0]) : null
				const selectedAddressId = selectedDetail ? selectedDetail.address_id : undefined

				this.props.utilities.alert.modal({
					component: (
						<AddressListPagelet
							selectable addable
							type="order"
							typeId={ this.props.orderId }
							selectedId={ selectedAddressId }
							addresses={ this.props.addresses }
							onClose={ this.onModalRequestClose }
							onConfirm={ this.onCreateShipment.bind(this, method, selectedIds) }
						/>
					),
				})
			})
		}

		onToggleSelect = id => {
			if(this.state.selectedIds.indexOf(id) > -1) {
				this.setState({
					selectedIds: without(this.state.selectedIds, id),
				})
			} else {
				this.setState({
					selectedIds: [...this.state.selectedIds, id],
				})
			}
		}

		onEditShipment = shipment => {
			this.props.utilities.alert.modal({
				component: (
					<ShipmentUpdatePagelet
						id={ shipment.id }
						orderId={ this.props.orderId }
						editable={ this.props.editable && !shipment.is_return }
						onShipmentCreated={this.onShipmentEditted}
					/>
				),
			})
		}

		onShipmentEditted = (isUpdated, orderDetails, id, data) => {
			if(isUpdated) {
				const shipment = this.props.shipments.find(s => s.id === id)

				shipment.amount = data.amount
				shipment.awb = data.awb
				shipment.courier = data.courier
				shipment.created_at = data.createdAt
				shipment.metadata = data.metadata || {}
				shipment.service = data.service
				shipment.status = data.status
				shipment.url = data.url

				this.forceUpdate()

				this.props.onUpdate &&
				this.props.onUpdate()
			} else {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Cannot update shipment data. Something went wrong, please try again later.',
				})
			}
		}

		shipmentRenderer = orderDetail => {
			return (
				<SelectionShipmentLego
					key={ orderDetail.id }
					id={ orderDetail.id }
					// TODO: add exchange marking if this order is an exchange
					title={ `${orderDetail.id ? `#OD-${orderDetail.id} ` : ''}${orderDetail.title}` }
					description={ this.address(orderDetail.address_id) }
					note={ `Schedule: ${ TimeHelper.format(orderDetail.shipment_at, 'DD MMM YY') }` }
					isSelected={ this.state.selectedIds.indexOf(orderDetail.id) > -1 }
					selectable={ orderDetail.selectable }
					editable={ this.props.editable }
					shipments={ orderDetail.shipments }
					onSelect={ this.onToggleSelect.bind(this, orderDetail.id) }
					onEdit={ this.onEditShipment }
				/>
			)
		}

		emptyRenderer() {
			return (
				<BoxBit unflex centering style={ Styles.empty }>
					<GeomanistBit centering type={GeomanistBit.TYPES.PARAGRAPH_3} style={ Styles.info }>
						No items found
					</GeomanistBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxLego title="Shipment" contentContainerStyle={Styles.container}>
					{ this.state.len === 0 ? this.emptyRenderer() : this.state.details.map(this.shipmentRenderer) }
					<FooterPart disabled={ !this.state.selectable }
						isSelected={ !!this.state.selectedIds.length }
						count={ this.state.shippedItemCount }
						total={ this.state.len }
						onCreateShipment={ this.onSelectAddress }
					/>
				</BoxLego>
			)
		}
	}
)
