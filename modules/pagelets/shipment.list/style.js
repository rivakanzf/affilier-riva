import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
	},

	content: {
		paddingLeft: 16,
		paddingRight: 16,
		paddingTop: 5,
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},
	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},

	primary: {
		color: Colors.primary,
	},

	padder: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		alignItems: 'center',
	},

	padder16: {
		paddingBottom: 16,
	},

	list: {
		padding: 16,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	divider: {
		width: 1,
		height: '100%',
		marginLeft: 8,
		marginRight: 7,
		backgroundColor: Colors.black.palette(2, .1),
	},

	check: {
		paddingLeft: 8,
		paddingTop: 8,
		paddingBottom: 8,
		paddingRight: 8,
	},

	empty: {
		backgroundColor: Colors.solid.grey.palette(4),
		height: 92,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	info: {
		color: Colors.black.palette(2, .6),
	},

	footer: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		alignItems: 'center',
	},

	dropdown: {
		marginLeft: 8,
		marginRight: 4,
	},

})
