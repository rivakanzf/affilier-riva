import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import UtilitiesContext from 'coeur/contexts/utilities';

import ShipmentService from 'app/services/shipment';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import InputCheckboxBit from 'modules/bits/input.checkbox';
import TextBit from 'modules/bits/text';

import CellLego from 'modules/legos/cell';
import LoaderLego from 'modules/legos/loader';
import RowsLego from 'modules/legos/rows';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import GetterPacketComponent from 'modules/components/getter.packet';
import AddressesComponent from 'modules/components/addresses';

import ModalClosableConfirmationPagelet from '../modal.closable.confirmation';

import DetailPart from './_detail'

import Styles from './style';

import { isEmpty, uniq } from 'lodash'


export default ConnectHelper(
	class ShipmentMultiplePagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				details: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					title: PropTypes.string,
					orderNumber: PropTypes.string,
					type: PropTypes.oneOf([
						'MANUAL',
						'TIKI',
						'NINJA',
					]),
					destination: PropTypes.shape({
						location_id: PropTypes.id.isRequired,
						title: PropTypes.string,
						receiver: PropTypes.string,
						phone: PropTypes.string,
						address: PropTypes.string.isRequired,
						district: PropTypes.string,
						postal: PropTypes.string.isRequired,
					}),
				})),
				onShipmentCreated: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			if (oP.details) {
				return Promise.all(oP.details.map(detail => {
					return ShipmentService.getRates([detail.id], detail.destination, detail.type, state.me.token).then(ratesAndPacket => {
						return {
							...ratesAndPacket,
							detail,
						}
					})
				}))
			} else {
				return null
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static getDerivedStateFromProps(nP, nS) {
			if(!isEmpty(nP.data) && !nS.isLoaded) {
				return {
					isLoaded: true,
					details: nP.data,
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				isLoaded: false,
				details: [],
			})
		}

		headers = [{
			title: 'Product',
			width: 2,
		}, {
			title: 'Dest.',
			width: 1,
		}, {
			title: 'Shipment',
			width: 4,
		}, {
			title: '',
			align: 'right',
			width: .5,
		}]

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onCreateShipment = () => {
			if((this.props.id && this.props.editable) || !this.props.id) {
				this.setState({
					isSaving: true,
				}, () => {
					Promise.resolve().then(() => {
						return ShipmentService.createShipment({
							method: this.props.method,
							packet: this.state.packet,
							order_detail_ids: this.state.details.map(d => d.id),
							destination: this.state.address,

							amount: this.state.data.amount,
							status: this.state.data.status,
							awb: this.state.data.awb,
							url: this.state.data.url,
							courier: this.state.data.courier,
							service: this.state.data.service,
							metadata: this.state.data.metadata,
							created_at: this.state.data.createdAt,
							shouldSendEmail: this.state.shouldSendEmail,
						}, this.props.token)
					}).then(shipmentOrBoolean => {
						this.props.utilities.notification.show({
							title: 'Yeay!',
							message: 'Shipment created / editted',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.props.onShipmentCreated &&
						this.props.onShipmentCreated(shipmentOrBoolean, this.state.details, this.props.id, this.state.data)

						this.onClose()
					}).catch(err => {
						this.warn(err)

						this.props.utilities.notification.show({
							title: 'Oops…',
							message: 'Something went wrong. Please try again later.',
						})
					}).finally(() => {
						this.setState({
							isSaving: false,
						})
					})
				})
			} else {
				this.onClose()
			}
		}

		onChange = (id, key, value) => {
			if(this.state.data[id] === undefined) {
				this.state.data[id] = {}
			}

			this.state.data[id][key] = value

			if (!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			} else {
				this.forceUpdate()
			}
		}

		onToggleShouldSendEmail = (id, value) => {
			if (this.state.data[id] === undefined) {
				this.state.data[id] = {}
			}

			this.state.data[id].shouldSendEmail = value
		}

		title() {
			// eslint-disable-next-line no-nested-ternary
			return this.props.id
				? this.props.editable
					? 'Edit Shipment'
					: 'Shipment Detail'
				: 'Create Shipment'
		}

		confirm() {
			// eslint-disable-next-line no-nested-ternary
			return this.props.id
				? this.props.editable
					? 'Save Change'
					: 'Okay'
				: 'Ship'
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return this.state.isLoaded ? (
				<ModalClosableConfirmationPagelet
					header={ (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							Total: <TextBit weight="semibold">IDR{ FormatHelper.currency(0) }</TextBit>
						</GeomanistBit>
					) }
					loading={ this.state.isSaving }
					onClose={ this.onClose }
					onCancel={ this.onClose }
					onConfirm={ this.onCreateShipment }
					confirm={ 'Create' }
					title={ 'Create Multiple Shipment' }
				>
					<AddressesComponent data={[this.state.address]} >
						<CellLego title="Shipment Status">
							<SelectStatusLego isRequired unflex disabled={ !this.state.isEditable || (!this.props.id && this.props.method !== 'MANUAL') }
								type={ SelectStatusLego.TYPES.SHIPMENT_STATUSES }
								status={ this.state.data.status }
								onChange={ this.onChange.bind(this, 'status') }
							/>
							<InputCheckboxBit disabled={ !this.state.isEditable } title="Send email to customer" value={ this.state.shouldSendEmail } onChange={ this.onToggleShouldSendEmail } style={Styles.checkbox} />
						</CellLego>
					</AddressesComponent>

					<RowsLego
						data={[{
							data: [{
								title: '',
							}],
						}]}
					/>

					<TableLego
						headers={ this.tableHeader }
						rows={ this.state.details.map(this.productRenderer) }
						style={ Styles.table }
					/>

					<DetailPart
						key={ this.state.isLoaded }
						editable={ this.state.isEditable }
						id={ this.props.id }
						data={ this.state.data }
						packet={ this.state.packet }
						couriers={ this.state.couriers }
						rates={ this.state.rates }
						onChange={ this.onChange }
					/>

				</ModalClosableConfirmationPagelet>
			) : this.viewOnLoading()
		}
	}
)
