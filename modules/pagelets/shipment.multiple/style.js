import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	title: {},

	checkbox: {
		marginTop: 8,
	},

	table: {
		marginTop: 16,
		marginLeft: -Sizes.margin.default,
		marginRight: -Sizes.margin.default,
	},
})
