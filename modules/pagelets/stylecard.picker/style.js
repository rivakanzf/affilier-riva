import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'
// import Styles from 'coeur/constants/style'


export default StyleSheet.create({

	container: {
		width: 948,
	},

	content: {
		paddingTop: 8,
		paddingLeft: 12,
		paddingRight: 12,
		paddingBottom: 16,
		backgroundColor: Colors.grey.palette(2),
		flexDirection: 'row',
		flexWrap: 'wrap',
	},

	item: {
		marginLeft: 4,
		marginRight: 4,
		marginTop: 8,
	},
})
