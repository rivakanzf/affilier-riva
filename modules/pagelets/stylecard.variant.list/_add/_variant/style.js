import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	content: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	loader: {
		marginTop: 8,
		color: Colors.black.palette(2, .6),
	},

})
