import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities'
import PageContext from 'coeur/contexts/page';


import StylecardService from 'app/services/stylecard';

import BoxBit from 'modules/bits/box';
import TouchableBit from 'modules/bits/touchable';
import RadioBit from 'modules/bits/radio';
import GeomanistBit from 'modules/bits/geomanist';
import TextInputBit from 'modules/bits/text.input';
import ButtonBit from 'modules/bits/button'

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'

import VariantPart from './_variant'

import Styles from './style';

import { debounce } from 'lodash';


export default ConnectHelper(
	class AddPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				search: PropTypes.string,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
			PageContext,

		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static defaultProps = {
			search: undefined,
		}

		constructor(p) {
			super(p, {
				index: 0,
				isLoading: false,
				selectedId: null,
				purchase: {},
				isAvailable: true,
				search: p.search,
			})
		}

		onSearch = debounce((e, val) => {
			this.setState({
				search: val,
			})
		}, 300)

		onAddItems = () => {
			this.setState({
				isLoading: true,
			}, () => {
				StylecardService.putVariantIntoStylecard(this.props.id, {
					id: this.state.selectedId,
				}, this.props.token).then(this.onSuccess).catch(this.onError)
			})
		}

		onSuccess = res => {
			this.props.utilities.notification.show({
				title: 'Success',
				message: `Inventory added to stylesheet #${this.props.id}`,
				type: 'SUCCESS',
			})

			this.props.onUpdate &&
			this.props.onUpdate(res.id)

			this.onClose()
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onSelect = id => {
			this.setState({
				selectedId: id,
			})
		}

		onChangeAvailableItem = () => {
			this.setState({
				isAvailable: !this.state.isAvailable,
			})
		}

		onNavigateToCreateProduct = () => {
			this.props.onNavigateToCreateProduct()
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Add New Item"
					subheader={(
						<BoxBit unflex style={ Styles.header }>
							<TextInputBit
								placeholder={ 'Search variant…' }
								onChange={ this.onSearch }
							/>
							<BoxBit unflex style={Styles.unitContainer}>
								<GeomanistBit type={GeomanistBit.TYPES.NOTE_2} style={Styles.unit}>max 80 items will be shown</GeomanistBit>
							</BoxBit>
							<ButtonBit
								title={'Create new product'}
								onPress={ this.onNavigateToCreateProduct }
							/>
						</BoxBit>
					)}
					loading={ this.state.isLoading }
					disabled={ this.state.selectedId === null }
					confirm={ 'Select Item' }
					onCancel={ this.onClose }
					onConfirm={ this.onAddItems }
					contentContainerStyle={ Styles.content }
				>
					<TouchableBit row style={Styles.radio} onPress={ this.onChangeAvailableItem }>
						<BoxBit unflex row>
							<RadioBit dumb isActive={ this.state.isAvailable } onPress={ this.onChangeAvailableItem }/>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.text}>Available Item Only</GeomanistBit>
						</BoxBit>
					</TouchableBit>
					<VariantPart key={this.state.isAvailable} keyword={ this.state.search } isAvailable={ this.state.isAvailable } onSelect={ this.onSelect } />
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
