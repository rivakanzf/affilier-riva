import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	header: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	content: {
		height: 560 - 46 - 77 - StyleSheet.hairlineWidth,
		// paddingLeft: 0,
		// paddingRight: 0,
		// paddingBottom: 0,
	},

	radio: {
		marginTop: Sizes.margin.default,
	},

	text: {
		marginLeft: Sizes.margin.default,
	},
	
	unitContainer: {
		marginTop: 6,
		marginLeft: Sizes.margin.default,
	},

	unit: {
		color: Colors.primary,
	},
})
