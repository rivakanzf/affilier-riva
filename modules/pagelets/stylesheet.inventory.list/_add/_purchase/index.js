import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import BoxBit from 'modules/bits/box'
import InputCurrencyBit from 'modules/bits/input.currency';
import InputValidatedBit from 'modules/bits/input.validated';
import TextInputBit from 'modules/bits/text.input';

import RowBrandLego from 'modules/legos/row.brand';
import RowCategoryLego from 'modules/legos/row.category';
import SelectSizeLego from 'modules/legos/select.size';
import SelectColorLego from 'modules/legos/select.color';

import GetterCategoryComponent from 'modules/components/getter.category';

import RowsLego from 'modules/legos/rows';

import Styles from './style';

export default ConnectHelper(
	class PurchasePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.shape({
					variant_id: PropTypes.id,
					url: PropTypes.string, // ok
					seller_id: PropTypes.id,
					brand_id: PropTypes.id, // ok
					title: PropTypes.string, // ok
					category_id: PropTypes.id, // ok
					color_id: PropTypes.id, // ok
					size_id: PropTypes.id, // ok
					price: PropTypes.number,
					retail_price: PropTypes.number,
					note: PropTypes.string,
				}),
				onUpdate: PropTypes.func,
			}
		}

		static defaultProps = {
			data: {},
		}

		constructor(p) {
			super(p, {
				categoryId: null,
			})
		}

		shouldComponentUpdate(nP, nS) {
			// When ReactDOM checks to see if a field is controlled, it checks to see if value != null (note that it's !=, not !==), and since undefined == null in JavaScript, it decides that it's uncontrolled. because '' != null
			// Solution: do not update, use default value
			return nS.categoryId !== this.state.categoryId
		}

		onChange(id, val) {
			if(id === 'category_id') {
				this.setState({
					categoryId: val,
				})
			}

			this.onUpdateInput(id, null, val)
		}

		onUpdateInput = (key, e, val) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, val)
		}

		onUpdateNumber = (key, e, val) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, parseInt(val, 10))
		}

		sizeRenderer = ({ categoryId }) => {
			return (
				<SelectSizeLego unflex disabled={ !categoryId }
					categoryId={ categoryId }
					sizeId={ this.props.data.size_id }
					onChange={ this.onChange.bind(this, 'size_id') }
				/>
			)
		}

		view() {
			return (
				<RowsLego
					data={[{
						data: [{
							title: 'URL',
							children: (
								<TextInputBit
									placeholder="Input here…"
									defaultValue={ this.props.data.url }
									onChange={ this.onUpdateInput.bind(this, 'url') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Product Name',
							children: (
								<TextInputBit
									placeholder="Input here…"
									defaultValue={this.props.data.title}
									onChange={this.onUpdateInput.bind(this, 'title')}
								/>
							),
						}],
					}, {
						row: false,
						children: (
							<RowBrandLego
								title="Seller"
								brandId={ this.props.data.seller_id }
								onChange={ this.onChange.bind(this, 'seller_id') }
							/>
						),
					}, {
						row: false,
						children: (
							<RowBrandLego
								brandId={ this.props.data.brand_id }
								onChange={ this.onChange.bind(this, 'brand_id') }
							/>
						),
					}, {
						row: false,
						children: (
							<RowCategoryLego
								categoryId={ this.props.data.category_id }
								onChange={ this.onChange.bind(this, 'category_id') }
							/>
						),
					}, {
						data: [{
							title: 'Color',
							children: (
								<SelectColorLego unflex
									colorId={ this.props.data.color_id }
									onChange={ this.onChange.bind(this, 'color_id') }
								/>
							),
						}, {
							title: 'Size',
							children: (
								<GetterCategoryComponent key={ this.state.categoryId } categoryId={ this.state.categoryId } children={ this.sizeRenderer } />
							),
						}],
						style: Styles.row,
					}, {
						data: [{
							title: 'Retail Price',
							children: (
								<InputCurrencyBit
									placeholder="Input here…"
									value={ this.props.data.retail_price }
									onChange={ this.onChange.bind(this, 'retail_price') }
								/>
							),
						}, {
							title: 'Discounted Price',
							children: (
								<InputCurrencyBit
									placeholder="Input here…"
									value={ this.props.data.price }
									onChange={ this.onChange.bind(this, 'price') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Note (optional)',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.TEXTAREA }
									placeholder="Input here…"
									value={ this.props.data.note }
									onChange={ this.onChange.bind(this, 'note') }
								/>
							),
							description: 'Place your note about this request (optional).',
						}],
					}]}
					style={ Styles.container }
				/>
			)
		}
	}
)
