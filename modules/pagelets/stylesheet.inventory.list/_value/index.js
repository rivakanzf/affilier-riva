import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
// import ProgressBit from 'modules/bits/progress';
import TextBit from 'modules/bits/text';

import HintLego from 'modules/legos/hint';

import Styles from './style';

import { debounce } from 'lodash'


export default ConnectHelper(
	class ValuePart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				value: PropTypes.number,
				minValue: PropTypes.number,
				realValue: PropTypes.number,
				currentValue: PropTypes.number,
				currentRealValue: PropTypes.number,
				// onUpdate: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				statusBar: 0,
				statusNumber: 0,
				// STANDS FOR
				// 0 => price not eligible	// grey
				// 1 => careful				// yellow
				// 2 => good				// green
				// 3 => over price 			// red
				point: 0,
			})
		}

		componentDidMount() {
			this.recalculate()
		}

		componentDidUpdate(pP) {
			if( pP.currentValue !== this.props.currentValue
				|| pP.currentRealValue !== this.props.currentRealValue
			) {
				this.recalculate()
			}
		}

		recalculate = debounce(() => {
			let statusNumber = 0
			let statusBar = 0

			if(this.props.currentValue <= this.props.value) {
				if(this.props.currentValue >= this.props.minValue) {
					statusNumber = 1
				} else {
					statusNumber = 3
				}
			} else {
				statusNumber = 2
			}

			if(this.props.currentRealValue <= this.props.realValue) {
				if(this.props.currentRealValue >= this.props.realValue * .85) {
					statusBar = 1
				} else {
					statusBar = 2
				}
			} else {
				statusBar = 3
			}

			this.setState({
				statusNumber,
				statusBar,
				point: Math.min(100, Math.floor(100 * this.props.currentRealValue / this.props.realValue)),
			}, () => {
				this.props.onUpdate &&
				this.props.onUpdate({
					totalRetailValue: this.props.currentRealValue,
					totalRealValue: this.props.currentValue,
					isValid: statusNumber !== 3 && statusBar !== 3,
				})
			})
		}, 100)

		colorStyle(type = 'bar', status) {
			switch(status) {
			case 1 :
				return type === 'bar' ? Styles.careful : Styles.colorCareful
			case 2 :
				return type === 'bar' ? Styles.good : Styles.colorGood
			case 3 :
				return type === 'bar' ? Styles.overPrice : Styles.colorOverprice
			default:
				return type === 'bar' ? Styles.notEligible : Styles.colorNotEligble
			}
		}

		view() {
			return (
				<BoxBit row style={[Styles.container, this.props.style]}>
					<BoxBit>
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} weight="normal" style={Styles.text}>
							Total Purchase Value <TextBit weight="medium" style={ this.colorStyle('number', this.state.statusBar) }>IDR { FormatHelper.currencyFormat(this.props.currentRealValue) || '0' }</TextBit> / max. { FormatHelper.currencyFormat(this.props.realValue) || '0' } { this.state.statusBar === 3 ? (
								<TextBit style={ this.colorStyle('number', this.state.statusBar) }>({ this.props.realValue - this.props.currentRealValue > 0 ? '+' : '-' }IDR { FormatHelper.currencyFormat(Math.abs(this.props.realValue - this.props.currentRealValue)) || '0' })</TextBit>
							) : false }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} weight="normal" style={Styles.text}>
							Total Retail Value <TextBit weight="medium" style={ this.colorStyle('number', this.state.statusNumber) }>IDR { FormatHelper.currencyFormat(this.props.currentValue) || '0' }</TextBit> / ±{ FormatHelper.currencyFormat(this.props.value) || '0' } { this.state.statusNumber === 3 ? (
								<TextBit style={ this.colorStyle('number', this.state.statusNumber) }>({ this.props.currentValue - this.props.value > 0 ? '+' : '-' }IDR { FormatHelper.currencyFormat(Math.abs(this.props.currentValue - this.props.value)) || '0' })</TextBit>
							) : false }
						</GeomanistBit>
					</BoxBit>
					{/* <BoxBit /> */}
					{/* <ProgressBit
						progress={this.state.point}
						style={Styles.progress}
						barStyle={this.colorStyle('bar', this.state.statusBar)}
					/> */}
					<HintLego title={`Total retail value ± IDR ${FormatHelper.currencyFormat(this.props.minValue) || '-'} Purchase max IDR ${FormatHelper.currencyFormat(this.props.realValue) || '-'}`} />
				</BoxBit>
			)
		}
	}
)
