import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		alignItems: 'center',
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		// backgroundColor: Colors.grey.palette(5),
	},

	text: {
		color: Colors.black.palette(1, .7),
		marginRight: 8,
	},

	total: {
		paddingLeft: 8,
	},

	progress: {
		width: 135,
		borderRadius: 2,
		marginRight: 16,
	},

	good: {
		backgroundColor: Colors.green.palette(1),
	},
	overPrice: {
		backgroundColor: Colors.red.palette(7),
	},
	careful: {
		backgroundColor: Colors.yellow.palette(1),
	},
	notEligible: {
		backgroundColor: Colors.black.palette(2, .6),
	},

	colorGood: {
		color: Colors.green.palette(2),
	},
	colorCareful: {
		color: Colors.yellow.palette(2),
	},
	colorOverprice: {
		color: Colors.red.palette(3),
	},
	colorNotEligible: {
		backgroundColor: Colors.black.palette(2, .9),
	},
})
