import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	content: {
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
	},

	empty: {
		height: 88,
	},

	text: {
		color: Colors.black.palette(2, .6),
	},

	title: {
		color: Colors.black.palette(2),
	},

	header: {
		alignItems: 'center',
	},

	button: {
		marginTop: -6,
		marginBottom: -6,
		marginLeft: Sizes.margin.default,
	},

	refresh: {
		marginLeft: 8,
		alignItems: 'flex-end',
	},

})
