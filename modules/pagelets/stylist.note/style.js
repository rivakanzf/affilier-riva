import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		paddingRight: 0,
		paddingLeft: 0,
		paddingBottom: 0,
	},

	content: {
		borderWidth: 0,
	},

	note: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
	},

	textarea: {
		minHeight: 280,
		backgroundColor: Colors.grey.palette(2),
		fontStyle: 'italic',
	},

	reply: {
		color: Colors.black.palette(2, .6),
		paddingLeft: Sizes.margin.default,
		paddingTop: Sizes.margin.default,
		backgroundColor: Colors.grey.palette(2),
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	text: {
		color: Colors.black.palette(2, .6),
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		backgroundColor: Colors.grey.palette(2),
	},
})
