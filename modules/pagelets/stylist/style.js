import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	content: {
		paddingTop: 12,
		paddingBottom: 12,
	},

})
