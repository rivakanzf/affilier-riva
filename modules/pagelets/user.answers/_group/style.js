import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


function getItemWidth(num = 4) {
	return (Sizes.screen.width - ((Sizes.margin.default * 2) + (Sizes.margin.default * (num - 1))) - 4) / num
}

export default StyleSheet.create({

	box: {
		backgroundColor: Colors.white.primary,
		padding: 16,
		marginBottom: 16,
		width: getItemWidth(4),
	},

	header: {
		paddingTop: 4,
		paddingBottom: 4,
		color: Colors.black.palette(2),
	},

	questions: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	question: {
		justifyContent: 'space-between',
		paddingTop: 8,
		paddingBottom: 8,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	title: {
		color: Colors.black.palette(2, .6),
	},

	images: {
		flexWrap: 'wrap',
		marginLeft: -4,
		marginRight: -4,
		marginTop: 8,
		marginBottom: -4,
	},

	media: {
		width: (getItemWidth(4) - (Sizes.margin.default * 2) - 8) / 2,
		height: (getItemWidth(4) - (Sizes.margin.default * 2) - 8) / 2,
		marginLeft: 4,
		marginRight: 4,
		marginTop: 4,
		marginBottom: 4,
		borderRadius: 4,
		overflow: 'hidden',
	},

	url: {
		alignSelf: 'stretch',
	},

	link: {
		wordBreak: 'break-all',
	},

})
