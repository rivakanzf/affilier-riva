import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	notification: {
		marginLeft: -Sizes.margin.default,
		marginRight: -Sizes.margin.default,
		marginTop: -24,
	},

	padder: {
		marginRight: Sizes.margin.default,
	},
	
	reason: {
		marginBottom: 4,
	},

	pad: {
		marginBottom: Sizes.margin.default,
	},
	
	note: {
		color: Colors.black.palette(2, .6),
	},

	content: {
		marginTop: Sizes.margin.default,
		marginLeft: 0,
		marginRight: 0,
	},

	empty: {
		marginTop: 24,
		marginBottom: 12,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},
})
