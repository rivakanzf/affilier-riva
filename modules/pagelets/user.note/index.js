import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import AuthenticationHelper from 'utils/helpers/authentication';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities'

import UserService from 'app/services/user.new';

import Masonry from 'react-masonry-component';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';

import InitialLego from 'modules/legos/initial';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import NotFoundPagelet from 'modules/pagelets/not.found';

import Styles from './style';

import { isEmpty, without } from 'lodash';


export default ConnectHelper(
	class UserNotePagelet extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				isStylist: AuthenticationHelper.isAuthorized(state.me.roles, 'stylist'),
				isFulfillment: AuthenticationHelper.isAuthorized(state.me.roles, 'fulfillment'),
				myId: state.me.id,
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return UserService.getNotes(oP.id, state.me.token)
		}

		static getDerivedStateFromProps(nP, nS) {
			if(!nS.isLoaded && !isEmpty(nP.data)) {
				return {
					isLoaded: true,
					notes: nP.data,
				}
			}

			return null
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoaded: false,
				isCreating: false,
				updatingIds: [],
				notes: [],
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onCreateOrEditNote = note => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet note
						title={ note ? 'Edit Note' : 'Add Note'}
						message={ note ? 'What would you like to edit?' : 'Create a note about this client.' }
						value={ note ? note.note : undefined }
						placeholder={ 'Input here' }
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmEditting.bind(this, note ? note.id : null) }
					/>
				),
			})
		}

		onDeleteNote = id => {
			this.props.utilities.alert.show({
				title: 'Delete Note',
				message: 'Are you sure want to delete this note?',
				actions: [{
					title: 'Yes',
					onPress: this.onConfirmDeletion.bind(this, id),
				}, {
					title: 'Cancel',
				}],
			})
		}

		onConfirmEditting = (id, note) => {
			if(id === null) {
				// creating
				this.setState({
					isCreating: true,
				}, () => {
					UserService.createNote(this.props.id, note, this.props.token).then(_note => {
						return UserService.getNote(this.props.id, _note.id, this.props.token).then(newNote => {
							this.setState({
								isCreating: false,
								notes: [...this.state.notes, newNote],
							})
						})
					}).catch(this.onError)
				})
			} else {
				this.setState({
					updatingIds: [...this.state.updatingIds, id],
				}, () => {
					UserService.updateNote(this.props.id, id, note, this.props.token).then(isUpdated => {
						if(isUpdated) {
							const _note = this.state.notes.find(n => n.id === id)
							// force update the note
							_note.note = note
							_note.edited = true

							this.setState({
								updatingIds: without(this.state.updatingIds, id),
							})
						} else {
							throw new Error('Update failed')
						}
					}).catch(this.onError)
				})
			}
		}

		onConfirmDeletion = id => {
			this.setState({
				updatingIds: [...this.state.updatingIds, id],
			}, () => {
				UserService.deleteNote(this.props.id, id, this.props.token).then(isDeleted => {
					if (isDeleted) {
						this.setState({
							updatingIds: without(this.state.updatingIds, id),
							notes: this.state.notes.filter(n => n.id !== id),
						})
					} else {
						throw new Error('Delete failed')
					}
				}).catch(this.onError)
			})
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})
		}

		noteRenderer = data => {
			return (
				<BoxBit key={ data.id } unflex style={[Styles.notes, this.state.updatingIds.indexOf(data.id) > -1 ? Styles.updating : undefined ]}>
					<BoxBit row>
						{ data.image ? (
							<ImageBit resizeMode={ ImageBit.TYPES.COVER } source={ data.image } style={Styles.image} />
						) : (
							<InitialLego name={data.creator} style={Styles.image} />
						) }
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } weight="medium" style={Styles.creator}>
								{ data.creator }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } style={Styles.date}>
								{ TimeHelper.format(data.created_at, 'DD MMMM YYYY – HH:mm') }
							</GeomanistBit>
						</BoxBit>
						{ (this.props.isStylist || this.props.isFulfillment) || this.props.myId === data.creator_id ? (
							<BoxBit unflex style={Styles.icon}>
								<TouchableBit unflex enlargeHitSlop onPress={ this.onDeleteNote.bind(this, data.id) }>
									<IconBit
										name="trash"
										color={ Colors.red.palette(3) }
										size={ 18 }
									/>
								</TouchableBit>
								<TouchableBit unflex enlargeHitSlop onPress={this.onCreateOrEditNote.bind(this, data)} style={Styles.edit}>
									<IconBit
										name="edit-2"
										color={Colors.black.palette(2, .3)}
										size={18}
									/>
								</TouchableBit>
							</BoxBit>
						) : null }
					</BoxBit>
					<GeomanistBit italic type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={Styles.note}>
						“{ data.note }”
					</GeomanistBit>
					{ data.edited ? (
						<BoxBit unflex style={Styles.mark}>
							<GeomanistBit italic type={GeomanistBit.TYPES.NOTE_2} style={Styles.edited}>
								edited — { TimeHelper.ago(data.updated_at) }
							</GeomanistBit>
						</BoxBit>
					) : false }
				</BoxBit>
			)
		}

		addNoteRenderer = truthy => {
			return (this.props.isStylist || this.props.isFulfillment) ? (
				<BoxBit unflex style={[Styles.notes, Styles.adder]}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
						{ truthy ? 'Wanna tell more about this user?' : 'Oops… No notes found for this user. Wanna add one?' }
					</GeomanistBit>
					<ButtonBit
						title="Add Note"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						size={ ButtonBit.TYPES.SIZES.SMALL }
						state={ this.state.isCreating ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
						width={ ButtonBit.TYPES.WIDTHS.FIT }
						onPress={ this.onCreateOrEditNote.bind(this, null) }
						style={ Styles.button }
					/>
				</BoxBit>
			) : false
		}

		viewOnError = err => {
			this.warn(err)

			return (!this.props.isStylist && !this.props.isFulfillment) && !this.state.notes.length ? (
				<NotFoundPagelet description="Someone forgot to take notes…" />
			) : (
				<BoxBit style={[Styles.container, this.props.style]}>
					<Masonry>
						{ this.state.notes.length ? this.state.notes.map(this.noteRenderer) : false }
						{ this.addNoteRenderer(!!this.state.notes.length) }
					</Masonry>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<NotFoundPagelet loading />
			)
		}

		view() {
			return(
				<BoxBit style={[Styles.container, this.props.style]}>
					{ (!this.props.isStylist && !this.props.isFulfillment) && !this.state.notes.length ? (
						<NotFoundPagelet description="Someone forgot to take notes…" />
					) : (
						<Masonry>
							{ this.state.notes.length ? this.state.notes.map(this.noteRenderer) : false }
							{ this.addNoteRenderer(!!this.state.notes.length) }
						</Masonry>
					) }
				</BoxBit>
			)
		}
	}
)
