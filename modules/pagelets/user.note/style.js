import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


const col = Math.floor(Sizes.screen.width / 280)

export default StyleSheet.create({
	container: {
		// flexWrap: 'wrap',
		// alignItems: 'flex-start',
		// justifyContent: 'flex-start',
		marginTop: - Sizes.margin.default / 4,
		marginLeft: - Sizes.margin.default / 4,
		marginRight: - Sizes.margin.default / 4,
		marginBottom: - Sizes.margin.default / 4,
		display: 'block',
		// height: 280,
	},
	// masonry: {
	// 	flexShrink: 0,
	// },
	notes: {
		width: (Sizes.screen.width - (Sizes.margin.default * 2) - ((4 * 2) * (col - 1)) - 4) / col,
		backgroundColor: Colors.white.primary,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		marginTop: Sizes.margin.default / 4,
		marginLeft: Sizes.margin.default / 4,
		marginRight: Sizes.margin.default / 4,
		marginBottom: Sizes.margin.default / 4,
	},
	adder: {
		height: 180,
		justifyContent: 'space-between',
	},
	empty: {
		height: '100%',
	},
	updating: {
		opacity: .6,
	},
	image: {
		marginTop: -6,
		borderWidth: 6,
		borderColor: Colors.solid.grey.palette(1),
		width: 60,
		height: 60,
		borderRadius: 30,
		marginRight: Sizes.margin.default,
	},
	creator: {
		marginBottom: 4,
	},
	date: {
		color: Colors.black.palette(2, .6),
	},
	note: {
		marginTop: 8,
		color: Colors.black.palette(2, .8),
		// marginBottom: 4,
	},
	button: {
		marginTop: 8,
	},
	icon: {
		marginTop: -8,
		marginRight: -8,
	},
	edit: {
		marginTop: 8,
	},
	// mark: {
	// 	backgroundColor: Colors.solid.grey.palette(1),
	// 	borderRadius: 5,
	// 	alignSelf: 'flex-start',
	// 	paddingTop: 2,
	// 	paddingBottom: 2,
	// 	paddingLeft: 4,
	// 	paddingRight: 4,
	// },
	edited: {
		marginTop: 8,
		color: Colors.black.palette(2, .4),
	},
})
