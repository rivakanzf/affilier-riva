import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';

import UserService from 'app/services/user.new';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import NavTextBit from 'modules/bits/nav.text';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status'
import TableLego from 'modules/legos/table';

import QuickViewItemPagelet from 'modules/pagelets/quick.view.item';
import NotFoundPagelet from 'modules/pagelets/not.found';

import Styles from './style';

import { isEmpty } from 'lodash'


export default ConnectHelper(
	class UserStylesheetPagelet extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				style: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			return UserService.getStyleHistory(oP.id, state.me.token).catch(() => [])
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		header = [{
			title: 'Stylist',
			width: 1.2,
		}, {
			title: 'Product',
			width: 2.8,
		}, {
			title: 'Status',
			width: .8,
		}, {
			title: 'Feedback',
			width: .7,
		}, {
			title: 'Exchange',
			width: .7,
		}, {
			title: 'Items',
			width: 3.5,
		}]

		onQuickview = inventory => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewItemPagelet
						type={ inventory.inventory_id ? 'INVENTORY' : 'PURCHASE_REQUEST' }
						id={ inventory.inventory_id ? inventory.inventory_id : inventory.purchase_request_id }
					/>
				),
			})
		}

		onNavigateToStylesheet = id => {
			this.props.page.navigator.navigate(`stylesheet/${id}`)
		}

		imageRenderer = inventory => {
			return (
				<TouchableBit key={ inventory.id } unflex onPress={ this.onQuickview.bind(this, inventory) } style={Styles.container}>
					<ImageBit overlay unflex
						resizeMode={ ImageBit.TYPES.COVER }
						broken={isEmpty(inventory.image)}
						style={ Styles.image }
						source={ inventory.image || undefined }
					/>
				</TouchableBit>
			)
		}

		rowRenderer = stylesheet => {
			return {
				data: [{
					children: (
						<React.Fragment>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{ stylesheet.stylist }
							</GeomanistBit>
							<NavTextBit
								title={ `#US-${stylesheet.stylist_id}` }
								link={ 'View Stylist' }
								href={ `client/${stylesheet.stylist_id}` }
								textStyle={ Styles.note }
							/>
						</React.Fragment>
					),
				}, {
					children: (
						<React.Fragment>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{ stylesheet.title }
							</GeomanistBit>
							<NavTextBit
								title={ stylesheet.type === 'EXCHANGE' ? 'Exchange' : `Order Date: ${ stylesheet.order_date ? TimeHelper.format(stylesheet.order_date, 'DD MMM YYYY') : '-' }` }
								link={ 'View Order' }
								href={ `order/${stylesheet.order_id}` }
								textStyle={ Styles.note }
							/>
						</React.Fragment>
					),
				}, {
					children: (
						<BadgeStatusLego status={ stylesheet.status } />
					),
				}, {
					children: (
						<IconBit
							name={ stylesheet.feedback ? 'circle-checkmark' : 'circle-null' }
							color={ stylesheet.feedback ? Colors.green.palette(1) : Colors.grey.palette(6) }
						/>
					),
				}, {
					children: (
						<IconBit
							name={ stylesheet.exchange ? 'circle-checkmark' : 'circle-null' }
							color={ stylesheet.exchange ? Colors.grey.palette(6) : Colors.green.palette(1) }
						/>
					),
				}, {
					children: (
						<BoxBit row>
							{ stylesheet.inventories.map(this.imageRenderer) }
						</BoxBit>
					),
				}],
				onPress: this.onNavigateToStylesheet.bind(this, stylesheet.id),
			}
		}

		viewOnLoading() {
			return (
				<NotFoundPagelet loading />
			)
		}

		view() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					{ this.props.data.length ? (
						<TableLego
							headers={ this.header }
							rows={ this.props.data.map(this.rowRenderer) }
						/>
					) : (
						<NotFoundPagelet description="Looks like someone is getting obsolete…" />
					) }
				</BoxBit>
			)
		}
	}
)
