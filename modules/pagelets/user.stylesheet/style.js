import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		marginRight: 8,
	},

	image: {
		width: 72,
		height: 96,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},
})
