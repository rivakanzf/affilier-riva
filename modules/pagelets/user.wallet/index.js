import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import AuthenticationHelper from 'utils/helpers/authentication'

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';

import WalletService from 'app/services/wallet';
import PointService from 'app/services/point';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
import NavTextTitleBit from 'modules/bits/nav.text.title';
// import TouchableBit from 'modules/bits/touchable';

import BoxRowLego from 'modules/legos/box.row';
// import BadgeStatusLego from 'modules/legos/badge.status';
import NotificationLego from 'modules/legos/notification';
import TableLego from 'modules/legos/table';
import TabLego from 'modules/legos/tab';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import NotFoundPagelet from 'modules/pagelets/not.found';


import Styles from './style';

// import { isEmpty } from 'lodash'


export default ConnectHelper(
	class UserStylesheetPagelet extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				isFinance: AuthenticationHelper.isAuthorized(state.me.roles, 'finance'),
				isSuperAdmin: AuthenticationHelper.isAuthorized(state.me.roles, 'superadmin'),
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {

			return Promise.all([WalletService.getDetailByUser(oP.id, state.me.token), PointService.getUserPoint(oP.id, state.me.token)]).then(res => {
				return {
					wallet: {
						...res[0],
						transactions: res[0].transactions.length ? res[0].transactions.concat([{
							id: null,
							amount: res[0].transaction_total,
						}]) : res[0].transactions,
					},
					point: {
						...res[1],
						transactions: res[1].transactions.length ? res[1].transactions.concat([{
							id: null,
							amount: res[1].transaction_total,
						}]) : res[1].transactions,
					},
				
				}
			})
			// return WalletService.getDetailByUser(oP.id, state.me.token).then(data => {
			// 	return {
			// 		...data,
			// 		transactions: data.transactions.length ? data.transactions.concat([{
			// 			id: null,
			// 			amount: data.transaction_total,
			// 		}]) : data.transactions,
			// 	}
			// })
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				isLoadingPoint: false,
				tabIndex: 0,
			})
		}

		tabs = [{
			title: 'Wallet',
			onPress: () => {
				this.setState({
					tabIndex: 0,
				})
			},
		}, {
			title: 'Point',
			onPress: () => {
				this.setState({
					tabIndex: 1,
				})
			},
		}]

		header = [{
			title: 'Date',
			width: .8,
		}, {
			title: 'Reference',
			width: 3.5,
		}, {
			title: 'Note',
			align: 'left',
			width: 3.5,
		},
		{
			title: '',
			align: 'right',
			width: .4,
		}, {
			title: 'Amount',
			align: 'right',
			width: 1.1,
		}]

		link = reference => {
			switch(reference.type) {
			case 'REQUEST':
			default:
				return undefined
			case 'ORDER':
				return 'View Order'
			case 'STYLESHEET_INVENTORY':
				return undefined
			case 'EXCHANGE':
				return 'View Order'
			case 'USER':
				return 'View Client'
			}
		}

		href = reference => {
			switch(reference.type) {
			case 'REQUEST':
			default:
				return undefined
			case 'ORDER':
				return `order/${ reference.id }`
			case 'STYLESHEET_INVENTORY':
				return undefined
			case 'EXCHANGE':
				return `order/${ reference.id }`
			case 'USER':
				return `client/${ reference.id }`
			}
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onAddToWallet = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Deposit"
						message="Specify how much (in IDR) you want to deposit into this wallet."
						placeholder="Input without comma or dot…"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmAdding }
					/>
				),
			})
		}

		onAddToPoint = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Deposit"
						message="Specify how much (in IDR) you want to deposit into this wallet."
						placeholder="Input without comma or dot…"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmAddingPoint }
					/>
				),
			})
		}

		onDeductWallet = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Deduct"
						message="Specify how much (in IDR) you want to deduct from this wallet."
						placeholder="Input without comma or dot…"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmDeducting }
					/>
				),
			})
		}

		onDeductPoint = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Deduct"
						message="Specify how much (in IDR) you want to deduct from this point."
						placeholder="Input without comma or dot…"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmDeductingPoint }
					/>
				),
			})
		}

		onConfirmAdding = amount => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Note"
						message="Why?"
						placeholder="Why?"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmNote.bind(this, amount) }
					/>
				),
			})
		}

		onConfirmAddingPoint = amount => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Note"
						message="Why?"
						placeholder="Why?"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmNotePoint.bind(this, amount) }
					/>
				),
			})
		}

		onConfirmNote = (amount, note) => {
			if (!note.replace(/\s/g, '').length) {
				this.onError('Note tidak boleh kosong')
			} else {
				this.setState({
					isLoading: 'adding',
				}, () => {
					WalletService.depositIntoUser(this.props.id, parseInt(amount, 10), note, this.props.token).then(() => {
						this.props.utilities.notification.show({
							message: `Success adding IDR ${ FormatHelper.currency(amount) } into wallet.`,
							type: 'SUCCESS',
						})

						this.setState({
							isLoading: false,
						})
		
						this.props.refresh()
					}).catch(this.onError).finally(() => {
						this.setState({
							isLoading: false,
						})
					})
				})
			}

			return null

		}

		onConfirmNotePoint = (amount, note) => {
			if (!note.replace(/\s/g, '').length) {
				this.onError('Note tidak boleh kosong')
			} else {
				this.setState({
					isLoadingPoint: 'adding',
				}, () => {
					PointService.depositIntoUser(this.props.id, parseInt(amount, 10), note, this.props.token).then(() => {
						this.setState({
							isLoadingPoint: false,
						})
						
						this.props.utilities.notification.show({
							message: `Success adding IDR ${ FormatHelper.currency(amount) } into point.`,
							type: 'SUCCESS',
						})
		
						this.props.refresh()
					}).catch(this.onError).finally(() => {
						this.setState({
							isLoading: false,
						})
					})
				})
			}

			return null

		}

		onConfirmDeducting = amount => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Note"
						message="Why?"
						placeholder="Why?"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmNoteDeduct.bind(this, amount) }
					/>
				),
			})

		}

		onConfirmDeductingPoint = amount => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Note"
						message="Why?"
						placeholder="Why?"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmNoteDeductPoint.bind(this, amount) }
					/>
				),
			})

		}

		onConfirmNoteDeduct = (amount, note) => {
			if (!note.replace(/\s/g, '').length) {
				this.onError('Note tidak boleh kosong')
			} else {
				this.setState({
					isLoading: 'deducting',
				}, () => {
					WalletService.deductUser(this.props.id, parseInt(amount, 10), note, this.props.token).then(() => {
						this.props.utilities.notification.show({
							message: `Success deducting IDR ${FormatHelper.currency(amount)} from wallet.`,
							type: 'SUCCESS',
						})
		
						this.props.refresh()
					}).catch(this.onError).finally(() => {
						this.setState({
							isLoading: false,
						})
					})
				})
			}
			return null
		}

		onConfirmNoteDeductPoint = (amount, note) => {
			if (!note.replace(/\s/g, '').length) {
				this.onError('Note tidak boleh kosong')
			} else {
				this.setState({
					isLoading: 'deducting',
				}, () => {
					PointService.deductUser(this.props.id, parseInt(amount, 10), note, this.props.token).then(() => {
						this.props.utilities.notification.show({
							message: `Success deducting IDR ${FormatHelper.currency(amount)} from point.`,
							type: 'SUCCESS',
						})
		
						this.props.refresh()
					}).catch(this.onError).finally(() => {
						this.setState({
							isLoading: false,
						})
					})
				})
			}
			return null
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				message: err ? err.detail || err.message || err : 'Oops… Something went wrong. Please try again later.',
			})
		}

		bankRenderer = bank => {
			return {
				data: [{
					title: 'Bank',
					content: bank.name,
				}, {
					title: 'Account',
					children: (
						<NavTextTitleBit
							title={ bank.account_number }
							description={ bank.account_name }
						/>
					),
				}],
			}
		}

		rowRenderer = transaction => {
			return {
				data: [{
					title: '',
					children: transaction.id ? (
						<React.Fragment>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.format(transaction.created_at, 'DD/MM/YY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 } style={ Styles.note }>
								{ TimeHelper.format(transaction.created_at, 'HH:mm') }
							</GeomanistBit>
						</React.Fragment>
					) : undefined,
				}, {
					title: '',
					children: transaction.id ? (
						<NavTextTitleBit
							title={ transaction.reference.title }
							description={ transaction.reference.description }
							link={ this.link(transaction.reference) }
							href={ this.href(transaction.reference) }
						/>
					) : undefined,
				}, {
					title: '',
					children: transaction.note
						? (
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } align="left">
								{transaction.note}
							</GeomanistBit>
						)
						: undefined,
				}, {
					title: 'IDR',
					children: !transaction.id ? (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="semibold" align="right">
							IDR
						</GeomanistBit>
					) : undefined,
				}, {
					title: transaction.type === 'DEBIT' ? `-${ FormatHelper.currency( transaction.amount ) }` : FormatHelper.currency( transaction.amount ),
					children: !transaction.id ? (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="semibold" align="right">
							{ FormatHelper.currency( transaction.amount ) }
						</GeomanistBit>
					) : undefined,
				}],
			}
		}

		walletRenderer = () => {
			return (
				<BoxRowLego
					title={ 'Wallet' }
					data={[ this.props.data.wallet.balanced ? null : {
						data: [{
							children: (
								<NotificationLego type={ NotificationLego.TYPES.ERROR } message={ 'This wallet is not balanced.' } />
							),
							style: Styles.notification,
						}],
					}, {
						data: [{
							title: 'Balance',
							children: (
								<GeomanistBit type={ GeomanistBit.TYPES.HEADER_3 } weight="medium">
									{ `IDR ${ FormatHelper.currency(this.props.data.wallet.balance) }` }
								</GeomanistBit>
							),
						}, {
							title: 'Last update',
							content: this.props.data.wallet.updated_at ? TimeHelper.format(this.props.data.wallet.updated_at, 'DD MMMM YYYY HH:mm') : '-',
						}],
					}, {
						data: [{
							title: 'Actions',
							children: (
								<ButtonBit
									title="Deposit"
									weight="medium"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									// eslint-disable-next-line no-nested-ternary
									state={ this.state.isLoading === 'adding' ? ButtonBit.TYPES.STATES.LOADING : this.state.isLoading || !this.props.isSuperAdmin ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
									// width={ ButtonBit.TYPES.WIDTHS.FIT }
									// size={ ButtonBit.TYPES.SIZES.TINY }
									onPress={ this.onAddToWallet }
								/>
							),
						}, {
							blank: true,
							children: (
								<ButtonBit
									title="Deduct"
									weight="medium"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									theme={ ButtonBit.TYPES.THEMES.SECONDARY }
									// eslint-disable-next-line no-nested-ternary
									state={ this.state.isLoading === 'deducting' ? ButtonBit.TYPES.STATES.LOADING : this.state.isLoading || !this.props.isSuperAdmin ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
									// width={ ButtonBit.TYPES.WIDTHS.FIT }
									// size={ ButtonBit.TYPES.SIZES.TINY }
									onPress={ this.onDeductWallet }
									style={ Styles.button }
								/>
							),
						}],
					}]}
					style={ Styles.pad }
				/>
			)
		}

		pointRenderer = () => {
			return (
				<BoxRowLego
					title={ 'Point' }
					data={[ this.props.data.point.balanced ? null : {
						data: [{
							children: (
								<NotificationLego type={ NotificationLego.TYPES.ERROR } message={ 'This point is not balanced.' } />
							),
							style: Styles.notification,
						}],
					}, {
						data: [{
							title: 'Balance',
							children: (
								<GeomanistBit type={ GeomanistBit.TYPES.HEADER_3 } weight="medium">
									{ `IDR ${ FormatHelper.currency(this.props.data.point.balance) }` }
								</GeomanistBit>
							),
						}],
					}, {
						data: [{
							title: 'Actions',
							children: (
								<ButtonBit
									title="Deposit"
									weight="medium"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									// eslint-disable-next-line no-nested-ternary
									state={ this.state.isLoadingPoint === 'adding' ? ButtonBit.TYPES.STATES.LOADING : this.state.isLoadingPoint || !this.props.isSuperAdmin ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
									// width={ ButtonBit.TYPES.WIDTHS.FIT }
									// size={ ButtonBit.TYPES.SIZES.TINY }
									onPress={ this.onAddToPoint }
								/>
							),
						}, {
							blank: true,
							children: (
								<ButtonBit
									title="Deduct"
									weight="medium"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									theme={ ButtonBit.TYPES.THEMES.SECONDARY }
									// eslint-disable-next-line no-nested-ternary
									state={ this.state.isLoading === 'deducting' ? ButtonBit.TYPES.STATES.LOADING : this.state.isLoading || !this.props.isSuperAdmin ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
									// width={ ButtonBit.TYPES.WIDTHS.FIT }
									// size={ ButtonBit.TYPES.SIZES.TINY }
									onPress={ this.onDeductPoint }
									style={ Styles.button }
								/>
							),
						}],
					}]}
					style={ Styles.pad }
				/>
			)
		}

		transactionHistoryRenderer = () => {
			if(this.state.tabIndex === 0) {
				return (
					<TableLego
						headers={ this.header }
						rows={ this.props.data.wallet.transactions.map(this.rowRenderer) }
					/>
				)
			}else {
				return (
					<TableLego
						headers={ this.header }
						rows={ this.props.data.point.transactions.map(this.rowRenderer) }
					/>
				)
			}
		}

		viewOnError() {
			return !this.props.isSuperAdmin ? (
				<NotFoundPagelet
					title="Sorry"
					description="You are not authorized to view this page."
				/>
			) : (
				<NotFoundPagelet
					title="There's an error"
					description="Contact tech team to look for"
				/>
			)
		}

		viewOnLoading() {
			return (
				<NotFoundPagelet loading />
			)
		}

		view() {
			return !this.props.isFinance ? (
				<NotFoundPagelet
					title="Oops…"
					description="You are not authorized to view this page."
				/>
			) : (
				<BoxBit row style={[Styles.container, this.props.style]}>
					<BoxBit  style={ Styles.padder }>
						{this.walletRenderer()}
						{this.pointRenderer()}

						<BoxRowLego
							title={ 'Bank Accounts' }
							data={ this.props.data.wallet.banks.length ? this.props.data.wallet.banks.map(this.bankRenderer) : [{
								data: [{
									headerless: true,
									children: (
										<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
											{ 'User have not set up their bank account yet.' }
										</GeomanistBit>
									),
									style: Styles.empty,
								}],
							}] }
						/>
					</BoxBit>
					<BoxBit>
						<BoxRowLego title={ 'Transaction History' } contentContainerStyle={ Styles.content }>
							<TabLego
								tabs={ this.tabs }
								activeIndex={ this.state.tabIndex }
							/>
							{ this.transactionHistoryRenderer() }
						</BoxRowLego>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
