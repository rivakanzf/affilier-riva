import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import SelectionBit from 'modules/bits/selection';

import BoxRowLego from 'modules/legos/box.row';

import Styles from './style';


export default ConnectHelper(
	class PaymentLinkActionsPart extends StatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
			}, [
			])
		}

		view() {
			return (
				<BoxRowLego
					title="Payment Link Actions"
					data={[{
						data: [{
							title: 'Status',
							children: (
								<BoxBit unflex>
									<SelectionBit
										options={[]}
									/>
									<GeomanistBit
										type={GeomanistBit.TYPES.NOTE_1}
										style={Styles.lastUpdate}
									>
										Last updated: -
									</GeomanistBit>
								</BoxBit>
							),
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									title="CREATE PAYMENT LINK"
									weight="medium"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={ButtonBit.TYPES.STATES.DISABLED}
								/>
							),
						}],
					}]}
					style={Styles.container}
				/>
			)
		}
	}
)
