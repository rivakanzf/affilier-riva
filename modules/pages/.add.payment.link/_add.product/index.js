import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import TextInputBit from 'modules/bits/text.input';

import BoxLego from 'modules/legos/box';
import RowLego from 'modules/legos/row';

import Styles from './style';


export default ConnectHelper(
	class AddProductPart extends StatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
			}, [
				'footerRenderer',
				'onClose',
			])
		}

		onClose() {
			this.props.utilities.alert.hide()
		}

		footerRenderer(i) {
			return (
				<BoxBit unflex key={i} row style={Styles.footer}>
					<ButtonBit
						title="Cancel"
						weight="medium"
						type={ButtonBit.TYPES.SHAPES.RECTANGLE}
						size={ButtonBit.TYPES.SIZES.SMALL}
						onPress={this.onClose}
					/>
					<ButtonBit
						title="Add"
						weight="medium"
						type={ButtonBit.TYPES.SHAPES.PROGRESS}
						size={ButtonBit.TYPES.SIZES.SMALL}
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxLego
					title="Add Product"
					style={Styles.container}
					contentContainerStyle={Styles.content}>
					<BoxBit style={Styles.padder}>
						<RowLego
							data={[{
								title: 'Product ID',
								children: (
									<TextInputBit
										placeholder="Enter product ID"
									/>
								),
							}, {
								title: 'Product Name',
								children: (
									<TextInputBit
										placeholder="Enter product name"
									/>
								),
							}]}
						/>
						<RowLego
							data={[{
								title: 'Price',
								children: (
									<BoxBit row unflex>
										<BoxBit unflex style={Styles.idr}>
											<GeomanistBit
												type={GeomanistBit.TYPES.PARAGRAPH_3}
												weight="medium"
												style={Styles.darkGrey60}
											>
												IDR
											</GeomanistBit>
										</BoxBit>
										<BoxBit>
											<TextInputBit
												placeholder="Enter price"
											/>
										</BoxBit>
									</BoxBit>
								),
							}, {
								title: 'Quantity',
								children: (
									<TextInputBit
										type={TextInputBit.TYPES.NUMBER}
									/>
								),
							}]}
						/>
					</BoxBit>
					{ this.footerRenderer() }
				</BoxLego>
			)
		}
	}
)
