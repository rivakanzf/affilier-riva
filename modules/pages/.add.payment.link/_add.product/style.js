import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		width: '41.1vw',
		borderRadius: 4,
	},

	content: {
		margin: 0,
	},

	padder: {
		paddingLeft: 16,
		paddingRight: 16,
		height: '46.9vh',
	},

	idr: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: 16,
		paddingRight: 16,
		backgroundColor: Colors.solid.grey.palette(1),
		borderRadius: 2,
		marginRight: 4,
	},

	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},

	footer: {
		justifyContent: 'space-between',
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},
})
