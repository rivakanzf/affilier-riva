import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TextInputBit from 'modules/bits/text.input';

import BoxRowLego from 'modules/legos/box.row';

import ProductDetailPart from '../_product.detail';

import Styles from './style';


export default ConnectHelper(
	class ProductDataPart extends StatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		view() {
			return (
				<BoxBit>
					<BoxRowLego
						title="Payment Link Detail"
						data={[{
							data: [{
								title: 'Date Created',
								children: (
									<BoxBit row style={Styles.center}>
										<BoxBit>
											<TextInputBit
												postfix={(
													<IconBit
														name="calendar"
														size={20}
														color={Colors.black.palette(2, .8)}
														style={{paddingRight: 8}}
													/>
												)}
												inputStyle={Styles.inputDate}
											/>
										</BoxBit>
										<GeomanistBit
											type={GeomanistBit.TYPES.PARAGRAPH_3}
											align="center"
											style={Styles.text}
										>
											at
										</GeomanistBit>
										<TextInputBit
											inputStyle={Styles.time}
											type={TextInputBit.TYPES.TEL}
											maxlength={2}
										/>
										<GeomanistBit
											type={GeomanistBit.TYPES.PARAGRAPH_3}
											align="center"
											style={{width: 8}}
										>
											:
										</GeomanistBit>
										<TextInputBit
											inputStyle={Styles.time}
											type={TextInputBit.TYPES.TEL}
											maxlength={2}
										/>
									</BoxBit>
								),
							}, {
								title: 'Expiry',
								children: (
									<BoxBit unflex row>
										<BoxBit>
											<TextInputBit inputStyle={Styles.expiry} />
										</BoxBit>
										<BoxBit unflex style={Styles.hours}>
											<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" >
												hours
											</GeomanistBit>
										</BoxBit>
									</BoxBit>
								),
							}],
						}, {
							data: [{
								title: 'Client First Name',
								children: (
									<TextInputBit placeholder="Enter first name" />
								),
							}, {
								title: 'Client Last Name',
								children: (
									<TextInputBit placeholder="Enter last name" />
								),
							}],
						}, {
							data: [{
								title: 'Client Email Address',
								children: (
									<TextInputBit placeholder="Enter email address" />
								),
							}, {
								blank: true,
							}],
						}]}
						contentContainerStyle={Styles.container}
					/>
					<ProductDetailPart />
				</BoxBit>
			)
		}
	}
)
