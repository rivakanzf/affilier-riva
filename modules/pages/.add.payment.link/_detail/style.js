import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	time: {
		width: 48,
	},

	text: {
		width: 24,
	},

	input: {
		paddingRight: 8,
	},

	inputDate: {
		width: 0,
	},

	expiry: {
		width: 180,
	},

	center: {
		alignItems: 'center',
	},

	space: {
		justifyContent: 'space-between',
	},

	hours: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: 16,
		paddingRight: 16,
		marginLeft: 4,
		backgroundColor: Colors.solid.grey.palette(1),
	},

	padder: {
		paddingLeft: 16,
	},

	form: {
		flexDirection: 'row',
		flexWrap: 'wrap',
	},
	row: {
		flex: 1,
		flexGrow: 1,
	},
	singleRow: {
		flexGrow: 0,
	},
})
