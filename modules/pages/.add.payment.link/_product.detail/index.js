import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import TableLego from 'modules/legos/table';

import AddProductPart from '../_add.product';
import Styles from './style';


export default ConnectHelper(
	class ProductDetailPart extends StatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
			}, [
				'onAddProduct',
			])
		}

		onAddProduct() {
			this.props.utilities.alert.modal({
				component: (
					<AddProductPart />
				),
			})
		}

		view() {
			return (
				<BoxBit style={Styles.container}>
					<BoxBit unflex>
						<TableLego
							headers={[{
								title: 'Product ID',
								width: 2,
							}, {
								title: 'Product Name',
								width: 4,
							}, {
								title: 'Qty',
								width: .5,
							}, {
								title: 'Price',
								width: 3,
								align: 'right',
							}, {
								title: '',
								width: .5,
								align: 'right',
							}]}
							rows={Array(4).fill({
								data: [{
									title: '01',
								}, {
									title: 'Enchantment Jumpsuit',
								}, {
									title: '1',
								}, {
									title: 'IDR 899,000',
								}, {
									children: (
										<IconBit
											name="edit"
											size={20}
											color={Colors.primary}
										/>
									),
								}],
								onPress: this.onAddProduct,
							})}
							style={Styles.table}
						/>
					</BoxBit>
					<BoxBit row style={Styles.padder}>
						<BoxBit style={Styles.padder8} />
						<BoxBit>
							<BoxBit row style={Styles.total}>
								<GeomanistBit
									type={GeomanistBit.TYPES.PARAGRAPH_3}
									weight="semibold"
									style={Styles.darkGrey}
								>
									TOTAL
								</GeomanistBit>
								<GeomanistBit
									type={GeomanistBit.TYPES.PARAGRAPH_3}
									align="right"
									weight="medium"
									style={Styles.price}
								>
									IDR 1,147,000
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>
					<BoxBit row style={Styles.footer}>
						<BoxBit />
						<ButtonBit
							title="Add Product"
							weight="medium"
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							size={ButtonBit.TYPES.SIZES.SMALL}
							width={ButtonBit.TYPES.WIDTHS.FIT}
							onPress={this.onAddProduct}
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
