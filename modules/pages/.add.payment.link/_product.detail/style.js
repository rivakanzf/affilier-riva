import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.white.primary,
	},

	table: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},

	padder: {
		paddingLeft: Sizes.margin.default,
	},

	padder8: {
		paddingRight: 8,
	},

	total: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: 8,
		paddingRight: Sizes.margin.default,
	},

	price: {
		flexGrow: 1,
		color: Colors.black.palette(2),
	},

	footer: {
		paddingTop: 9,
		paddingBottom: 10,
		paddingRight: Sizes.margin.default,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},
})
