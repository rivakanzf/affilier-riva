import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import BoxBit from 'modules/bits/box';
import PageBit from 'modules/bits/page';
import ScrollViewBit from 'modules/bits/scroll.view';

import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
import HeaderLoggedLego from 'modules/legos/header.logged';

import SidebarNavigationComponent from 'modules/components/sidebar.navigation';

import ActionsPart from './_actions';
import DetailPart from './_detail';
import Styles from './style';


export default ConnectHelper(
	class PaymentLinksPage extends PageModel {

		static routeName = 'add.payment.link'

		constructor(p) {
			super(p, {
				// initial state
				user: {},
				address: {},
			}, [
				// autobinds
				'onUpdate',
				'onBack',
			]);
		}

		onUpdate(key, data) {
			this.setState({
				[key.toLowerCase()]: data,
			})
		}

		onBack() {
			this.navigator.back()
		}

		view() {
			return (
				<PageBit scroll={false} header={( <HeaderLoggedLego /> )}>
					<BoxBit row style={Styles.wrapper}>
						<SidebarNavigationComponent />
						<BoxBit style={Styles.container}>
							<HeaderBreadcrumbLego
								paths={[{
									title: 'PARMENT LINKS',
									onPress: this.onBack,
								}, {
									title: 'CREATE PAYMENT LINK',
								}, {
									title: 'Create Payment Link',
								}]}
							/>
							<ScrollViewBit>
								<BoxBit unflex row>
									<BoxBit style={{paddingRight: 16}}>
										<DetailPart onUpdate={ this.onUpdate }/>
									</BoxBit>
									<BoxBit>
										<ActionsPart />
									</BoxBit>
								</BoxBit>
							</ScrollViewBit>
						</BoxBit>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
