import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

    container: {
        backgroundColor: Colors.solid.grey.palette(1),
        paddingLeft: 32,
        paddingRight: 32,
        paddingTop: Sizes.margin.default,
        paddingBottom: Sizes.margin.default,
	},

    '@media (min-width: 1920px)': {
		page: {
			width: 1920,
			alignSelf: 'center',
			// alignItems: 'center',
		},
	},

	'@media (max-width: 1430px)': {
		wrapper: {
			flexDirection: 'column',
		},
	},
})
