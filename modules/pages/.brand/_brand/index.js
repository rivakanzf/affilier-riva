import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BrandManager from 'app/managers/brand';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';

import BreadcrumbLego from 'modules/legos/breadcrumb';
import GroupLego from 'modules/legos/group';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

import { debounce } from 'lodash';


export default ConnectHelper(
	class BrandPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				onRequestClose: PropTypes.func.isRequired,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, brand = BrandManager.get(id)

			return {
				brand,
			}
		}

		constructor(p) {
			super(p, {
				data: {
					title: p.pickupPoint.title,
				},
				isDone: false,
				isLoading: false,
			}, [
				'validateAndRetrieveArea',
				'saveDistrict',
				'onUpdate',
				'onUpdateAddress',
				'onSubmit',
				'onSuccess',
				'onError',
				'onCancel',
			])

			this.validateAndRetrieveArea = debounce(this.validateAndRetrieveArea, 300)
		}

		componentDidMount() {
		}

		onUpdate(isDone, {
			title,
		}) {
			this.setState({
				isDone,
				data: {
					...this.state.data,
					title: title.value,
				},
			})
		}

		onSubmit() {
			this.setState({
				isLoading: true,
			}, () => {
				const {
					title,
				} = this.state.data

				if(this.props.id) {
					// UPDATE
					BrandManager.updateBrand({
						title,
					}).then(this.onSuccess).catch(this.onError)
				} else {
					// CREATE
					BrandManager.createBrand({
						title,
					}).then(this.onSuccess).catch(this.onError)
				}
			})
		}

		onSuccess() {
			this.props.utilities.notification.show({
				title: 'Congrats',
				message: 'Brand updated.',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})

			this.setState({
				isLoading: false,
			})

			this.props.onRequestClose &&
			this.props.onRequestClose()
		}

		onError(err) {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})

			this.setState({
				isLoading: false,
			})
		}

		onCancel() {
			this.props.onRequestClose &&
			this.props.onRequestClose()
		}

		view() {
			return (
				<PageBit contentContainerStyle={Styles.container}>
					<BreadcrumbLego
						paths={[{
							title: 'Brand',
							onPress: this.props.onRequestClose,
						}, {
							title: 'Brand Detail',
						}]}
						style={Styles.breadcrumb}
					/>
					<GeomanistBit
						type={GeomanistBit.TYPES.HEADER_4}
						weight="medium"
						style={Styles.title}>
						{ this.props.pickupPoint.title }
					</GeomanistBit>
					<FormPagelet
						config={[{
							title: 'Title',
							isRequired: true,
							id: 'title',
							type: FormPagelet.TYPES.TITLE,
							value: this.state.data.title,
							testOnMount: true,
						}]}
						onUpdate={ this.onUpdate }
					/>
					<GroupLego title="Pickup Point" style={Styles.address}>
						{ this.state.isGettingAddress ? (
							<BoxBit type={BoxBit.TYPES.ALL_THICK} centering>
								<LoaderBit />
							</BoxBit>
						) : (
							<FormPagelet ignoreNull
								updater={ this.state.data.district || this.invalidPostalLength() }
								config={[{
									title: 'Receiver Name',
									id: 'pic',
									type: FormPagelet.TYPES.NAME,
									value: this.state.data.pic,
									placeholder: this.state.data.title,
									style: Styles.firstInput,
									testOnMount: true,
								}, {
									title: 'Phone',
									id: 'phone',
									type: FormPagelet.TYPES.PHONE,
									value: this.state.data.phone,
									inputContainerStyle: Styles.inputContainerPhone,
									inputStyle: Styles.inputPhone,
									testOnMount: true,
								}, {
									title: 'Address',
									isRequired: true,
									id: 'address',
									type: FormPagelet.TYPES.ADDRESS,
									value: this.state.data.address,
									testOnMount: true,
								}, {
									title: 'District (Kecamatan / Kelurahan)',
									id: 'district',
									isRequired: true,
									disabled: true,
									placeholder: 'Autofill',
									type: FormPagelet.TYPES.TITLE,
									value: this.state.data.district,
									testOnMount: true,
								}, {
									title: 'Postal Code',
									id: 'postal',
									isRequired: true,
									updater: this.invalidPostalLength,
									type: FormPagelet.TYPES.POSTAL,
									validator: this.checkInvalidPostal,
									testOnMount: true,
									value: this.state.data.postal,
								}]}
								onUpdate={ this.onUpdateAddress }
								onSubmit={ this.onSubmit }
							/>
						) }
					</GroupLego>
					<BoxBit unflex row>
						<ButtonBit
							title={ 'CANCEL' }
							type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
							width={ ButtonBit.TYPES.WIDTHS.FIT }
							state={ this.state.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
							onPress={ this.onCancel }
							style={ Styles.button }
						/>
						<ButtonBit
							title={ 'SAVE' }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							width={ ButtonBit.TYPES.WIDTHS.BLOCK }
							state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.state.isDone.address && this.state.isDone.pickup && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
							onPress={ this.onSubmit }
							style={ [Styles.button, Styles.rightButton] }
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
