import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BrandManager from 'app/managers/brand';
import PickupPointManager from 'app/managers/pickup.point';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';

import BoxLego from 'modules/legos/box';
import BoxRowLego from 'modules/legos/box.row';
import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
import RowLego from 'modules/legos/row';

import DetailPagelet from 'modules/pagelets/detail';
import EditBrandPagelet from 'modules/pagelets/edit.brand';
import EditPickupPointPagelet from 'modules/pagelets/edit.pickup.point';

import Styles from './style';

import { chunk } from 'lodash';


export default ConnectHelper(
	class DetailPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				onRequestClose: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state, oP) {
			const id = oP.id
				, brand = BrandManager.get(id)
				, pickupPoints = brand.pickupPointIds.map(pId => PickupPointManager.get(pId))

			return {
				brand,
				pickupPoints,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
			}, [
				'onRequestCloseAll',
				'onEditBrand',
				'onEditPickupPoint',
				// 'onDoneEditPickupPoint',
			])

			this._updates = {}
		}

		componentDidMount() {
		}

		onRequestCloseAll() {
			this.props.onRequestClose()
			this.setTimeout(this.props.onRequestClose, 10)
		}

		onEditBrand() {
			this.props.utilities.alert.modal({
				component: (
					<EditBrandPagelet
						id={this.props.brand.id}
						header={ this.brandHeaderRenderer(this.props.brand.title) }
						onRequestClose={ this.props.onRequestClose }
					/>
				),
			})
		}

		onEditPickupPoint(pickupPointId, title) {
			this.props.utilities.alert.modal({
				component: (
					<EditPickupPointPagelet
						id={typeof pickupPointId === 'number' ? pickupPointId : undefined}
						brandId={ this.props.brand.id }
						header={ this.headerRenderer(title) }
						onRequestClose={ this.props.onRequestClose }
					/>
				),
			})
		}

		brandHeaderRenderer(title) {
			const paths = [{
				title: 'Brands',
				onPress: this.props.onRequestClose,
			}, {
				title: 'Brand Detail',
			}]

			if(title) {
				paths.push({
					title,
				})
			}

			return (
				<HeaderBreadcrumbLego
					paths={paths}
					style={ Styles.main }
				/>
			)
		}

		headerRenderer(title) {
			const paths = [{
				title: 'Brands',
			}, {
				title: 'Brand Detail',
				onPress: this.props.onRequestClose,
			}, {
				title: 'Pickup Point',
			}]

			if(title) {
				paths.push({
					title,
				})
			}

			return (
				<HeaderBreadcrumbLego
					paths={paths}
					style={ Styles.main }
				/>
			)
		}

		// onDoneEditPickupPoint(pickupPointId) {
		// 	if(this.state.edittingIds.indexOf(pickupPointId) > -1) {
		// 		this.setState({
		// 			edittingIds: this.state.edittingIds.filter(id => id !== pickupPointId),
		// 		})
		// 	}
		// }

		view() {
			return (
				<DetailPagelet
					paths={[{
						title: 'Brands',
						onPress: this.props.onRequestClose,
					}, {
						title: 'Brand Detail',
					}, {
						title: this.props.brand.title,
					}]}
				>
					<BoxRowLego
						title="General"
						data={[{
							data: [{
								title: 'Name',
								editable: true,
								onPress: this.onEditBrand,
								content: this.props.brand.title,
							}],
						}]}
						contentContainerStyle={Styles.box}
					/>
					<BoxLego
						title="Pickup Points"
						contentContainerStyle={Styles.box}>
						{ chunk(this.props.pickupPoints.concat(new Array(this.props.pickupPoints.length % 2).fill({})), 2).map((pickupPoints, index) => {
							return (
								<RowLego
									key={ index }
									data={pickupPoints.map(pickupPoint => {
										return {
											title: pickupPoint.title,
											editable: pickupPoint.title ? true : false,
											onPress: this.onEditPickupPoint.bind(this, pickupPoint.id, pickupPoint.title),
											content: pickupPoint.description,
										}
									})}
								/>
							)
						}) }
						<BoxBit unflex>
							<ButtonBit
								title="Add Pickup Point"
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								onPress={ this.onEditPickupPoint }
							/>
						</BoxBit>
					</BoxLego>
				</DetailPagelet>
				// <BoxBit style={Styles.main}>
				// 	<HeaderBreadcrumbLego
				// 		paths={[{
				// 			title: 'Brands',
				// 			onPress: this.props.onRequestClose,
				// 		}, {
				// 			title: 'Brand Detail',
				// 		}, {
				// 			title: this.props.brand.title,
				// 		}]}
				// 	/>
				// 	<ScrollViewBit>
				// 		<BoxLego title="General" contentContainerStyle={Styles.box}>
				// 			<RowLego
				// 				data={[{
				// 					title: 'Name',
				// 					editable: true,
				// 					onPress: this.onEditBrand,
				// 					content: this.props.brand.title,
				// 				}]}
				// 			/>
				// 		</BoxLego>
				// 		<BoxLego title="Pickup Points" contentContainerStyle={Styles.box}>
				// 			{ _.chunk(this.props.pickupPoints.concat(new Array(this.props.pickupPoints.length % 2).fill({})), 2).map((pickupPoints, index) => {
				// 				return (
				// 					<RowLego
				// 						key={ index }
				// 						data={pickupPoints.map(pickupPoint => {
				// 							return {
				// 								title: pickupPoint.title,
				// 								editable: pickupPoint.title ? true : false,
				// 								onPress: this.onEditPickupPoint.bind(this, pickupPoint.id, pickupPoint.title),
				// 								content: pickupPoint.description,
				// 							}
				// 						})}
				// 					/>
				// 				)
				// 			}) }
				// 			<BoxBit unflex>
				// 				<ButtonBit
				// 					title="Add Pickup Point"
				// 					width={ ButtonBit.TYPES.WIDTHS.FIT }
				// 					onPress={ this.onEditPickupPoint }
				// 				/>
				// 			</BoxBit>
				// 		</BoxLego>
				// 	</ScrollViewBit>
				// </BoxBit>
			)
		}
	}
)
