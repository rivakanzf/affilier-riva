import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: 32,
		paddingRight: 32,
	},

	firstColumn: {
		marginRight: 16,
	},

	mediaContainer: {
		flexWrap: 'wrap',
	},

	media: {
		width: 128,
		height: 128,
		margin: 6,
	},

	box: {
		marginBottom: 16,
	},

	input: {
		marginTop: 8,
	},
})
