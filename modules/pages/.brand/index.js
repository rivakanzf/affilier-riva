import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import BrandManager from 'app/managers/brand';
import MeManager from 'app/managers/me';

// import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';
// import ScrollViewBit from 'modules/bits/scroll.view';
// import SelectionBit from 'modules/bits/selection';
// import TextInputBit from 'modules/bits/text.input';
// import AddressPagelet from '@yuna/modules.react/pagelets/address';

// import BreadcrumbLego from 'modules/legos/breadcrumb';
import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
import TableLego from 'modules/legos/table';

import SidebarNavigationComponent from 'modules/components/sidebar.navigation';

import EditBrandPagelet from 'modules/pagelets/edit.brand';

import DetailPart from './_detail';

import Styles from './style';

import _ from 'lodash'


export default ConnectHelper(
	class BrandPage extends PageModel {

		static routeName = 'brand'

		static stateToProps(state) {
			return {
				me: state.me,
				brandIds: state.brands.sortBy(b => b.title).keySeq().toJS(),
				isAuthorized: !!_.intersection(state.me.roles, ['inventory']).length,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				offset: 0,
				count: Infinity,
				search: undefined,
			}, [
				'onGoToFirst',
				'onGoToLast',
				'onGoToPrev',
				'onGoToNext',
				'onModalRequestClose',
				'onGoToDetail',
				'onAddBrand',
				'onSearchHandler',
			])
		}

		componentDidMount() {
			this.getData()
		}

		componentDidUpdate(pP, pS) {
			if(
				pS.search !== this.state.search
				|| pS.offset !== this.state.offset
			) {
				this.getData()
			}
		}

		getData() {
			if( this.props.isAuthorized ) {
				this.setState({
					isLoading: true,
				}, () => {
					BrandManager.getAll({
						search: this.state.search,
						offset: this.state.offset,
						count: this.state.count,
					}).then(res => {
						this.setState({
							isLoading: false,
							total: res.count,
							brandId: res.data.map(b => b.id),
						})
					}).catch(err => {
						if(err && err.code === '031') {
							this.utilities.notification.show({
								title: 'Sorry',
								message: 'You\'ve been logged out. Please log in again.',
							})

							this.navigator.navigate('login')

							MeManager.logout();
						}
					})
				})
			}
		}

		onGoToFirst() {
			this.setState({
				offset: 0,
			})
		}

		onGoToPrev() {
			this.setState({
				offset: Math.max(0, this.state.offset - this.state.count),
			})
		}

		onGoToNext() {
			this.setState({
				offset: Math.min(this.state.total, this.state.offset + this.state.count),
			})
		}

		onGoToLast() {
			this.setState({
				offset: this.state.total - this.state.count,
			})
		}

		onModalRequestClose() {
			this.utilities.alert.hide()
		}

		onGoToDetail(id) {
			this.utilities.alert.modal({
				component: (
					<DetailPart id={id} onRequestClose={this.onModalRequestClose} />
				),
				wrapperStyle: Styles.modal,
				onOverlayPress: this.onModalRequestClose,
			})
		}

		onSearchHandler(searchValue) {
			// TODO
			this.setState({
				search: searchValue,
			})
		}

		onAddBrand() {
			this.utilities.alert.modal({
				component: (
					<EditBrandPagelet paths={[{
						title: 'Brands',
						onPress: this.onModalRequestClose,
					}, {
						title: 'Add Brand',
					}]} />
				),
				wrapperStyle: Styles.modal,
				onOverlayPress: this.onModalRequestClose,
			})
		}

		view() {
			return this.props.me.token ? (
				<PageBit scroll={false} style={Styles.page}>
					<BoxBit row style={Styles.wrapper}>
						<SidebarNavigationComponent />
						{ this.props.isAuthorized ? (
							<BoxBit style={Styles.main}>
								<BoxBit unflex row>
									<HeaderBreadcrumbLego
										paths={[{
											title: 'Brand',
										}]}
									/>
									<BoxBit />
									<ButtonBit
										icon="expand"
										title="Add Brand"
										type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
										width={ ButtonBit.TYPES.WIDTHS.FIT }
										onPress={ this.onAddBrand }
									/>
								</BoxBit>
								<TableLego paging
									current={ this.state.offset }
									count={ this.state.count }
									total={ this.state.total }
									onGoToFirst={ this.onGoToFirst }
									onGoToPrev={ this.onGoToPrev }
									onGoToNext={ this.onGoToNext }
									onGoToLast={ this.onGoToLast }

									searchable
									isSearching={ this.state.isLoading }
									onSearch={ this.onSearchHandler }

									headers={[{
										title: 'Name',
									}]}
									rows={this.props.brandIds.map(brandId => {
										const brand = BrandManager.get(brandId)

										return {
											data: [{
												title: brand.title || '-',
											}],
											onPress: this.onGoToDetail.bind(this, brandId),
										}
									})}
									style={Styles.padder}
								/>
							</BoxBit>
						) : (
							<BoxBit centering>
								<GeomanistBit>Sorry, you are not authorized to access this page</GeomanistBit>
							</BoxBit>
						) }
					</BoxBit>
				</PageBit>
			) : false
		}
	}
)
