import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	page: {},
	wrapper: {},

	modal: {
		width: '90%',
	},

	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		// paddingBottom: 32,
		paddingLeft: 32,
		paddingRight: 32,
	},

	padder: {
		marginBottom: 16,
	},

	selection: {
		borderWidth: 1,
		borderColor: Colors.grey.palette(1, .2),
		borderStyle: 'solid',
		backgroundColor: Colors.white.primary,
		borderRadius: 4,
		paddingRight: 12,
	},

	padderRight: {
		marginRight: 16,
	},

	search: {
		paddingLeft: 16,
		backgroundColor: Colors.white.primary,
		borderRadius: 4,
		overflow: 'hidden',
	},

	footer: {
		marginTop: 16,
		justifyContent: 'flex-end',
	},

	'@media (min-width: 1920px)': {
		page: {
			width: 1920,
			alignSelf: 'center',
			// alignItems: 'center',
		},
	},

	'@media (max-width: 1366px)': {
		wrapper: {
			flexDirection: 'column',
		},
		modal: {
			width: '100%',
		},
	},
})
