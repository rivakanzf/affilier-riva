import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import CouponManager from 'app/managers/coupon';
import MeManager from 'app/managers/me';
// import OrderDetailManager from 'app/managers/order.detail';
// import UserManager from 'app/managers/user';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
// import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';

import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
import HeaderLoggedLego from 'modules/legos/header.logged';
import TableLego from 'modules/legos/table';

import SidebarNavigationComponent from 'modules/components/sidebar.navigation';

import EditCouponPagelet from 'modules/pagelets/edit.coupon';

// import AddPart from './_add';
import Styles from './style';

import _ from 'lodash'


export default ConnectHelper(
	class CouponsPage extends PageModel {

		static routeName = 'coupons'

		static stateToProps(state) {
			return {
				me: state.me,
				coupons: state.coupons,
				isAuthorized: !!_.intersection(state.me.roles, ['superadmin']).length,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				search: '',
				offset: 0,
				count: 30,
				total: 0,
				couponIds: [],
			}, [
				'getData',
				'onGoToFirst',
				'onGoToLast',
				'onGoToPrev',
				'onGoToNext',
				'onSearchHandler',
				'onNavigateToCouponDetail',
			])
		}

		componentDidMount() {
			this.getData()
		}

		componentDidUpdate(pP, pS) {
			if(
				pS.search !== this.state.search
				|| pS.offset !== this.state.offset
			) {
				this.getData()
			}
		}

		getData() {
			if( this.props.isAuthorized ) {
				this.setState({
					isLoading: true,
				}, () => {
					CouponManager.getAll({
						title: this.state.search,
						offset: this.state.offset,
						count: this.state.count,
					}, this.props.me.token).then(res => {
						this.setState({
							isLoading: false,
							total: res.count,
							couponIds: res.data.map(coupon => coupon.id),
						})
					}).catch(err => {
						if(err && err.code === '031') {
							this.utilities.notification.show({
								title: 'Sorry',
								message: 'You\'ve been logged out. Please log in again.',
							})

							// this.navigator.navigate('login')

							// MeManager.logout();
						}
					})
				})
			}
		}

		onSearchHandler(searchValue) {
			this.setState({
				search: searchValue,
			})
		}

		onGoToFirst() {
			this.setState({
				offset: 0,
			})
		}

		onGoToPrev() {
			this.setState({
				offset: Math.max(0, this.state.offset - this.state.count),
			})
		}

		onGoToNext() {
			this.setState({
				offset: Math.min(this.state.total, this.state.offset + this.state.count),
			})
		}

		onGoToLast() {
			this.setState({
				offset: this.state.total - this.state.count,
			})
		}

		// onAddCoupons() {
		// 	// TODO
		// 	this.utilities.alert.modal({
		// 		component: (
		// 			<AddPart />
		// 		),
		// 	})
		// }

		onNavigateToCouponDetail(e, couponId) {
			// TODO
			this.utilities.alert.modal({
				component: (
					<EditCouponPagelet paths={[{
						title: 'Coupons',
					}, {
						title: couponId ? 'Edit Coupon' : 'Add Coupon',
					}]} id={couponId} onUpdate={ this.getData } />
				),
			})
		}

		getTitle(type) {
			switch(type) {
			case 'shipping':
				return 'Shipping Amount';
			case 'item':
				return 'Each Applicable Item';
			case 'total':
				return 'Total Amount';
			case 'percent':
				return 'Percentage';
			default:
				return type
			}
		}

		view() {
			return this.props.me.token ? (
				<PageBit scroll={false} style={Styles.page} header={( <HeaderLoggedLego /> )}>
					<BoxBit row style={Styles.wrapper}>
						<SidebarNavigationComponent />
						{ this.props.isAuthorized ? (
							<BoxBit style={Styles.main}>
								<BoxBit unflex row>
									<HeaderBreadcrumbLego
										paths={[{
											title: 'Coupons',
										}]}
									/>
									<BoxBit />
									<ButtonBit
										icon="expand"
										title="Add Coupon"
										type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
										width={ ButtonBit.TYPES.WIDTHS.FIT }
										onPress={ this.onNavigateToCouponDetail }
									/>
								</BoxBit>
								<TableLego
									searchable
									isSearching={ this.state.isLoading }
									onSearch={ this.onSearchHandler }

									paging
									current={ this.state.offset }
									total={ this.state.total }
									onGoToFirst={ this.onGoToFirst }
									onGoToPrev={ this.onGoToPrev }
									onGoToNext={ this.onGoToNext }
									onGoToLast={ this.onGoToLast }

									headers={[{
										title: 'Title',
										width: 5,
									}, {
										title: 'Type',
										width: 5,
									}, {
										title: 'Amount',
										width: 5,
									}, {
										title: 'Published At',
										width: 5,
									}, {
										title: 'Expired At',
										width: 5,
									}]}
									rows={ this.state.couponIds.map(couponId => {
										const coupon = CouponManager.get(couponId)

										return coupon._isGetting ? {
											data: [{
												title: '-',
											}, {
												title: '-',
											}, {
												title: '-',
											}, {
												title: '-',
											}, {
												title: '-',
											}],
										} : {
											data: [{
												title: coupon.title,
											}, {
												title: this.getTitle(coupon.type),
											}, {
												title: coupon.type === 'percent' ? `${coupon.amount}%` : `IDR ${FormatHelper.currencyFormat(coupon.amount)}`,
											}, {
												title: TimeHelper.moment(coupon.createdAt).format('DD MMMM YYYY [at] HH:mm'),
											}, {
												title: coupon.expiry && TimeHelper.moment(coupon.expiry).format('DD MMMM YYYY [at] HH:mm') || '-',
											}],
											onPress: this.onNavigateToCouponDetail.bind(this, undefined, couponId),
										}
									}) }
									style={Styles.padder}
								/>
							</BoxBit>
						) : (
							<BoxBit centering>
								<GeomanistBit>Sorry, you are not authorized to access this page</GeomanistBit>
							</BoxBit>
						) }
					</BoxBit>
				</PageBit>
			) : false
		}
	}
)
