import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},
	wrapper: {
		height: '100%',
	},

	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		paddingBottom: 32,
		paddingLeft: 32,
		paddingRight: 32,
	},

	padder: {
		marginBottom: 16,
	},

	'@media (min-width: 1920px)': {
		page: {
			width: 1920,
			alignSelf: 'center',
			// alignItems: 'center',
		},
	},

	'@media (max-width: 1366px)': {
		wrapper: {
			flexDirection: 'column',
		},
		modal: {
			width: '100%',
		},
	},
	'@media (max-width: 1430px)': {
		wrapper: {
			flexDirection: 'column',
		},
	},
})
