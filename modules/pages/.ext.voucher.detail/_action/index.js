import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import BoxRowLego from 'modules/legos/box.row';
import DropdownLego from 'modules/legos/dropdown';
import HintLego from 'modules/legos/hint';

import Styles from './style';


export default ConnectHelper(
	class ActionPart extends StatefulModel {

		static contexts = [
			UtilitiesContext,
		]

		statusRenderer() {
			return (
				<BoxBit unflex>
					<BoxBit row>
						<BoxBit>
							<DropdownLego
								placeholder="Select status"
								children={(
									<BoxBit unflex style={Styles.dropdown}>
										<TouchableBit unflex row style={Styles.padder}>
											<GeomanistBit
												type={GeomanistBit.TYPES.PARAGRAPH_3}
												style={Styles.darkGrey}
											>
												Approved
											</GeomanistBit>
											<BoxBit />
											<RadioBit style={Styles.radio} />
										</TouchableBit>
										<TouchableBit unflex row>
											<GeomanistBit
												type={GeomanistBit.TYPES.PARAGRAPH_3}
												style={Styles.darkGrey}
											>
												Rejected
											</GeomanistBit>
											<BoxBit />
											<RadioBit style={Styles.radio} />
										</TouchableBit>
									</BoxBit>
								)}
							/>
						</BoxBit>
						{ this.orderCreated && (
							<ButtonBit
								title="Update"
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								width={ButtonBit.TYPES.WIDTHS.FIT}
								state={ButtonBit.TYPES.STATES.DISABLED}
								style={Styles.button}
							/>
						) }
					</BoxBit>
					{ this.updated && (
						<GeomanistBit
							type={GeomanistBit.TYPES.NOTE_1}
							style={Styles.footNote}
						>
							{ this.updated && 'Last updated: 23/10/208 10:36\nBy: Ghina Amania' }
						</GeomanistBit>
					) }
				</BoxBit>
			)
		}

		relatedRenderer() {
			return (
				<BoxBit unflex>
					{ this.orderCreated ? (
						<ButtonBit
							title="View Order"
							type={ButtonBit.TYPES.SHAPES.RECTANGLE}
							theme={ButtonBit.TYPES.THEMES.WHITE_PRIMARY}
						/>
					) : (
						<ButtonBit
							title="Create Order"
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							state={ButtonBit.TYPES.STATES.DISABLED}
						/>
					) }
					<GeomanistBit
						type={GeomanistBit.TYPES.NOTE_1}
						style={Styles.footNote}
					>
						{ this.statusApproved && 'No orders have been made' || this.orderCreated && 'Created on: 24/10/2018 18:45' || 'Requires approval' }
					</GeomanistBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxRowLego
					title="External Voucher Actions"
					header={( <HintLego title="Action must be taken before response deadline." /> )}
					data={[{
						data: [{
							title: 'Status',
							children: this.statusRenderer(),
						}, {
							title: 'Related Order',
							children: this.relatedRenderer(),
						}],
					}]}
					style={Styles.container}
				/>
			)
		}
	}
)
