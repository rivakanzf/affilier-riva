import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		marginLeft: 16,
	},

	dropdown: {
		paddingLeft: 16,
		paddingRight: 16,
		paddingBottom: 16,
	},

	padder: {
		paddingBottom: 16,
		paddingTop: 3,
	},

	radio: {
		height: 20,
		width: 20,
	},

	button: {
		marginLeft: 4,
	},

	footNote: {
		color: Colors.black.palette(2, .6),
		paddingTop: 8,
	},

})
