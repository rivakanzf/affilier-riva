import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'utils/constants/color'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import BoxRowLego from 'modules/legos/box.row';

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends StatefulModel {

		clientRenderer() {
			return (
				<BoxBit unflex>
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_3}
						style={Styles.darkGrey}
					>
						Maharani Angelica
					</GeomanistBit>
					<TouchableBit row>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							weight="medium"
							style={Styles.viewDetail}
						>
							View profile
						</GeomanistBit>
						<IconBit
							name="arrow-right-tailed"
							size={20}
							color={Colors.primary}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		emailRenderer() {
			return (
				<BoxBit unflex>
					<GeomanistBit
						type={GeomanistBit.TYPES.PARAGRAPH_3}
						style={Styles.darkGrey}
					>
						maharani.angelica@gmail.com
					</GeomanistBit>
					<TouchableBit row>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							weight="medium"
							style={Styles.viewDetail}
						>
							View other orders
						</GeomanistBit>
						<IconBit
							name="arrow-right-tailed"
							size={20}
							color={Colors.primary}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxRowLego
					title="INV/20190421/XIX/IV/301733848"
					data={[{
						data: [{
							title: 'Client',
							children: this.clientRenderer(),
						}, {
							title: 'Email Address',
							children: this.emailRenderer(),
						}],
					}, {
						data: [{
							title: 'Entry Date',
							content: '14/10/18 18:45',
						}, {
							title: 'Response Deadline',
							content: '15/10/18 18:45',
						}],
					}]}
				/>
			)
		}
	}
)
