import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	viewDetail: {
		color: Colors.primary,
		paddingRight: 4,
		lineHeight: 20,
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},
})
