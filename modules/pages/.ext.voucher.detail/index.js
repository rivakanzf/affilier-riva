import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import PageBit from 'modules/bits/page';

import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
import HeaderLoggedLego from 'modules/legos/header.logged';

import SidebarNavigationComponent from 'modules/components/sidebar.navigation';

import ActionPart from './_action';
import DetailPart from './_detail';

import Styles from './style';

// import _ from 'lodash'


export default ConnectHelper(
	class ExternalVoucherDetailPage extends PageModel {

		static routeName = 'external-voucher.detail'

		static stateToProps(state) {
			return {
				me: state.me,
				// isAuthorized: !!_.intersection(state.me.roles, ['invertory']).length,
			}
		}

		constructor(p) {
			super(p, {
			}, [
				'onNavigateToExtVoucher',
			])
		}

		onNavigateToExtVoucher() {
			this.navigator.navigate('external-voucher')
		}

		view() {
			return this.props.me.token ? (
				<PageBit style={Styles.page} header={( <HeaderLoggedLego /> )}>
					<SidebarNavigationComponent />
					<BoxBit style={Styles.main}>
						<BoxBit unflex row>
							<HeaderBreadcrumbLego
								paths={[{
									title: 'EXT. VOUCHERS',
									onPress: this.onNavigateToExtVoucher,
								}, {
									title: 'INV/20190421/XIX/IV/301733848',
								}, {
									title: 'Edit External Voucher',
								}]}
							/>
						</BoxBit>
						<BoxBit row>
							<BoxBit>
								<DetailPart />
							</BoxBit>
							<BoxBit>
								<ActionPart />
							</BoxBit>
						</BoxBit>
					</BoxBit>
					{/* { this.props.isAuthorized ? (
					) : (
						<BoxBit centering>
							<GeomanistBit>Sorry, you are not authorized to access this page</GeomanistBit>
						</BoxBit>
					) } */}
				</PageBit>
			) : false
		}
	}
)
