import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';
import PageContext from 'coeur/contexts/page';

import Colors from 'utils/constants/color'

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import BoxLego from 'modules/legos/box';
import BadgeStatusLego from 'modules/legos/badge.status';
import RowLego from 'modules/legos/row';

import Styles from './style';


export default ConnectHelper(
	class QuickViewPart extends StatefulModel {

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
			}, [
				'onclose',
				'onNavigateToDetail',
			])
		}

		onclose() {
			this.props.utilities.alert.hide()
		}

		onNavigateToDetail() {
			this.props.page.navigator.navigate('order')
		}

		view() {
			return (
				<BoxLego
					title="INV/20190421/XIX/IV/301733848"
					header={(
						<BoxBit row unflex>
							<BadgeStatusLego />
							<TouchableBit unflex onPress={this.onclose} style={Styles.close}>
								<IconBit
									name="close"
									color={Colors.black.palette(2, .8)}
								/>
							</TouchableBit>
						</BoxBit>
					)}
					style={Styles.container}
					contentContainerStyle={Styles.content}
				>
					<BoxBit style={Styles.padder}>
						<RowLego
							data={[{
								title: 'Client',
								content: 'Maharani Angelica',
							}, {
								title: 'Email Address',
								content: 'maharani.angelica@gmail.com',
							}]}
						/>
						<RowLego
							data={[{
								title: 'Entry Date',
								content: '14/10/2018 18:45',
							}, {
								title: 'Response Deadline',
								content: '14/10/2018 18:45',
							}]}
						/>
						<RowLego
							data={[{
								title: 'Status',
								content: 'Pending Response',
							}, {
								title: 'Order',
								children: (
									<BoxBit row>
										<IconBit
											name="null"
											size={20}
											color={Colors.solid.grey.palette(5)}
										/>
										<GeomanistBit
											type={GeomanistBit.TYPES.PARAGRAPH_3}
											style={Styles.order}
										>
											Has not been created
										</GeomanistBit>
									</BoxBit>
								),
							}]}
						/>
					</BoxBit>
					<BoxBit unflex row style={Styles.footer}>
						<BoxBit />
						<ButtonBit
							title="Edit External Voucher"
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							size={ButtonBit.TYPES.SIZES.SMALL}
							width={ ButtonBit.TYPES.WIDTHS.FIT }
							onPress={ this.onNavigateToDetail }
						/>
					</BoxBit>
				</BoxLego>
			)
		}
	}
)
