import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		width: '41.1vw',
		borderRadius: 4,
		overflow: 'hidden',
	},

	content: {
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
		height: '46vh',
	},

	padder: {
		paddingLeft: 16,
		paddingRight: 16,
		paddingBottom: 16,
	},

	close: {
		paddingLeft: 16,
	},

	order: {
		color: Colors.black.palette(2, .6),
		paddingLeft: 4,
	},

	ellipsis: {
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		textOverflow: 'ellipsis',
	},

	footer: {
		borderTopWidth: 1,
		borderColor: Colors.black.palette(2, .2),
		paddingTop: 10,
		paddingBottom: 10,
		paddingRight: 16,
		paddingLeft: 16,
	},

})
