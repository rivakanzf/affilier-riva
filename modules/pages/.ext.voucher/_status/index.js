import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import Colors from 'utils/constants/color'

import IconBit from 'modules/bits/icon';
import SelectionBit from 'modules/bits/selection';
import {orderBy} from 'lodash';

export default ConnectHelper(
	class StatusPart extends StatefulModel {

		view() {
			return (
				<SelectionBit
					placeholder="Filter by status"
					options={[]}
					style={this.props.style}
					inputStyle={this.props.inputStyle}
				/>
			)
		}
	}
)
