import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import PageBit from 'modules/bits/page';

import BadgeStatusLego from 'modules/legos/badge.status';
import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
import HeaderLoggedLego from 'modules/legos/header.logged';
import TableLego from 'modules/legos/table';

import SidebarNavigationComponent from 'modules/components/sidebar.navigation';

import DatePart from './_date';
import QuickViewPart from './_quick.view';
import StatusPart from './_status';

import Styles from './style';

// import _ from 'lodash'


export default ConnectHelper(
	class ExternalVouchersPage extends PageModel {

		static routeName = 'external-voucher'

		static stateToProps(state) {
			return {
				me: state.me,
				// isAuthorized: !!_.intersection(state.me.roles, ['invertory']).length,
			}
		}

		constructor(p) {
			super(p, {
				// isLoading: true,
				search: undefined,
				status: 'null',
				shipmentDate: 'null',
				date: 'null',
				data: [],
				total: 0,
				offset: 0,
				count: 40,
			}, [
				'onGoToFirst',
				'onGoToLast',
				'onGoToPrev',
				'onGoToNext',
				'onSearchHandler',
				'onQuickView',
				'onNavigateToDetail',
			])
		}

		onSearchHandler(searchValue) {
			// TODO
			this.setState({
				search: searchValue,
			})
		}

		onGoToFirst() {
			this.setState({
				offset: 0,
			})
		}

		onGoToPrev() {
			this.setState({
				offset: Math.max(0, this.state.offset - this.state.count),
			})
		}

		onGoToNext() {
			this.setState({
				offset: Math.min(this.state.total, this.state.offset + this.state.count),
			})
		}

		onGoToLast() {
			this.setState({
				offset: this.state.total - this.state.count,
			})
		}

		onQuickView() {
			this.utilities.alert.modal({
				component: (
					<QuickViewPart />
				),
			})
		}

		onNavigateToDetail() {
			this.navigator.navigate('external-voucher/detail')
		}

		view() {
			return this.props.me.token ? (
				<PageBit scroll={false} style={Styles.page} header={( <HeaderLoggedLego /> )}>
					<BoxBit row style={Styles.wrapper}>
						<SidebarNavigationComponent />
						<BoxBit style={Styles.main}>
							<BoxBit unflex row>
								<HeaderBreadcrumbLego
									paths={[{
										title: 'EXT. VOUCHERS',
									}, {
										title: 'External Vouchers',
									}]}
								/>
							</BoxBit>
							<TableLego paging
								// isLoading={ this.state.isLoading }
								onGoToFirst={ this.onGoToFirst }
								onGoToLast={ this.onGoToLast}
								onGoToNext={ this.onGoToNext }
								onGoToPrev={ this.onGoToPrev }
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								searchable
								// isSearching={ this.state.isLoading }
								onSearch={ this.onSearchHandler }
								action={(
									<BoxBit row>
										<BoxBit style={Styles.button}>
											<StatusPart />
										</BoxBit>
										<BoxBit style={Styles.button}>
											<DatePart />
										</BoxBit>
										<BoxBit />
										<BoxBit />
									</BoxBit>
								)}
								headers={[{
									title: 'External Voucher',
									width: 4,
								}, {
									title: 'Client',
									width: 3,
								}, {
									title: 'Entry Date',
									width: 2,
								}, {
									title: 'Response Deadline',
									width: 2.5,
								}, {
									title: 'Approval',
									width: 2.5,
								}, {
									title: 'Order',
									width: 1,
								}, {
									title: '',
									width: 1,
								}]}
								rows={Array(10).fill({
									data: [{
										title: 'INV/20190421/XIX/IV/301733848',
									}, {
										title: 'Chandravadan Karmakar',
									}, {
										title: '16/10/2018 14:58',
									}, {
										title: '17/10/2018 14:58',
									}, {
										children: (
											<TouchableBit unflex row centering>
												<BoxBit unflex>
													<BadgeStatusLego
														status={BadgeStatusLego.TYPES.UNPAID}
													/>
												</BoxBit>
												<BoxBit />
												<IconBit
													name="dropdown"
													size={20}
													color={Colors.black.palette(2, .8)}
												/>
											</TouchableBit>
										),
									}, {
										children: (
											<BoxBit unflex row>
												<IconBit
													name="approved"
													size={20}
													color={Colors.green.primary}
												/>
												<BoxBit />
											</BoxBit>
										),
									}, {
										children: (
											<BoxBit unflex row>
												<BoxBit />
												<TouchableBit unflex onPress={this.onQuickView}>
													<IconBit
														name="quick-view"
														size={20}
														color={Colors.primary}
													/>
												</TouchableBit>
											</BoxBit>
										),
									}],
									onPress: this.onNavigateToDetail,
								})}
								style={Styles.padder}
							/>
						</BoxBit>
						{/* { this.props.isAuthorized ? (
						) : (
							<BoxBit centering>
								<GeomanistBit>Sorry, you are not authorized to access this page</GeomanistBit>
							</BoxBit>
						) } */}
					</BoxBit>
				</PageBit>
			) : false
		}
	}
)
