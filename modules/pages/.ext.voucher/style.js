import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	wrapper: {
		height: '100%',
	},

	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	title: {
		paddingTop: 12,
		paddingBottom: 12,
	},

	button: {
		marginRight: 16,
	},

	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: 32,
		paddingRight: 32,
	},

	padder: {
		marginBottom: 16,
	},

	padderRight8: {
		paddingRight: 8,
	},

	padderRight: {
		marginRight: 16,
	},

	search: {
		paddingLeft: 16,
		backgroundColor: Colors.white.primary,
		borderRadius: 4,
		overflow: 'hidden',
	},

	footer: {
		marginTop: 16,
		justifyContent: 'flex-end',
	},

	capital: {
		textTransform: 'capitalize',
	},

	'@media (min-width: 1920px)': {
		page: {
			width: 1920,
			alignSelf: 'center',
			// alignItems: 'center',
		},
	},

	'@media (max-width: 1430px)': {
		wrapper: {
			flexDirection: 'column',
		},
	},
})
