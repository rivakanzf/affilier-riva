import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import PageBit from 'modules/bits/page';

import BadgeStatusLego from 'modules/legos/badge.status';
import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
import HeaderLoggedLego from 'modules/legos/header.logged';
import TableLego from 'modules/legos/table';

import SidebarNavigationComponent from 'modules/components/sidebar.navigation';

import ClientPart from './_client';
import DatePart from './_date';
import QuickViewPart from 'modules/pages/order/_quick.view';
import StatusPart from './_status';

import Styles from './style';

import _ from 'lodash'


export default ConnectHelper(
	class PaymentLinksPage extends PageModel {

		static routeName = 'payment-link'

		static stateToProps(state) {
			return {
				me: state.me,
				isAuthorized: !!_.intersection(state.me.roles, ['order']).length,
			}
		}

		constructor(p) {
			super(p, {
				// isLoading: true,
				search: undefined,
				status: 'null',
				shipmentDate: 'null',
				date: 'null',
				data: [],
				total: 0,
				offset: 0,
				count: 40,
			}, [
				'onGoToFirst',
				'onGoToLast',
				'onGoToPrev',
				'onGoToNext',
				'onSearchHandler',
				'onNavigateToCreatePaymentLink',
				'onQuickView',
			])
		}

		onSearchHandler(searchValue) {
			// TODO
			this.setState({
				search: searchValue,
			})
		}

		onGoToFirst() {
			this.setState({
				offset: 0,
			})
		}

		onGoToPrev() {
			this.setState({
				offset: Math.max(0, this.state.offset - this.state.count),
			})
		}

		onGoToNext() {
			this.setState({
				offset: Math.min(this.state.total, this.state.offset + this.state.count),
			})
		}

		onGoToLast() {
			this.setState({
				offset: this.state.total - this.state.count,
			})
		}

		onNavigateToCreatePaymentLink() {
			this.navigator.navigate('payment-links/create')
		}

		onQuickView() {
			this.utilities.alert.modal({
				component: (
					<QuickViewPart />
				),
			})
		}

		view() {
			return this.props.me.token ? (
				<PageBit style={Styles.page} header={( <HeaderLoggedLego /> )}>
					<SidebarNavigationComponent />
					{ this.props.isAuthorized ? (
						<BoxBit style={Styles.main}>
							<BoxBit unflex row>
								<HeaderBreadcrumbLego
									paths={[{
										title: 'PAYMENT LINKS',
									}, {
										title: 'Payment Links',
									}]}
								/>
							</BoxBit>
							<TableLego paging
								// isLoading={ this.state.isLoading }
								onGoToFirst={ this.onGoToFirst }
								onGoToLast={ this.onGoToLast}
								onGoToNext={ this.onGoToNext }
								onGoToPrev={ this.onGoToPrev }
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								searchable
								// isSearching={ this.state.isLoading }
								onSearch={ this.onSearchHandler }
								filter={(
									<ButtonBit
										title="Create Payment Link"
										type={ ButtonBit.TYPES.SHAPES.PROGRESS }
										width={ ButtonBit.TYPES.WIDTHS.FIT }
										onPress={ this.onNavigateToCreatePaymentLink }
										style={Styles.button}
									/>
								)}
								action={(
									<BoxBit row>
										<BoxBit style={Styles.button}>
											<StatusPart />
										</BoxBit>
										<BoxBit style={Styles.button}>
											<DatePart />
										</BoxBit>
										<BoxBit>
											<ClientPart />
										</BoxBit>
										<BoxBit />
									</BoxBit>
								)}
								headers={[{
									title: 'Link ID',
									width: 2,
								}, {
									title: 'Date Created',
									width: 1.5,
								}, {
									title: 'Expiry Date',
									width: 1.5,
								}, {
									title: 'Client',
									width: 2.5,
								}, {
									title: 'Status',
									width: 2,
								}, {
									title: '',
									width: 2,
									align: 'right',
								}]}
								rows={Array(4).fill({
									data: [{
										title: 'YCP1809140014814',
									}, {
										title: '18/05/2018 21:25',
									}, {
										title: '16/10/18 — 25/10/18',
									}, {
										title: 'Loise Pasaribu',
									}, {
										children: (
											<BoxBit unflex row>
												<BoxBit unflex>
													<BadgeStatusLego
														status={BadgeStatusLego.TYPES.UNPAID}
													/>
												</BoxBit>
												<BoxBit />
												<IconBit
													name="dropdown"
												/>
											</BoxBit>
										),
									}, {
										children: (
											<BoxBit unflex row>
												<BoxBit />
												<TouchableBit unflex onPress={this.onQuickView}>
													<IconBit
														name="quick-view"
														size={20}
														color={Colors.primary}
													/>
												</TouchableBit>
											</BoxBit>
										),
									}],
								})}
								style={Styles.padder}
							/>
						</BoxBit>
					) : (
						<BoxBit centering>
							<GeomanistBit>Sorry, you are not authorized to access this page</GeomanistBit>
						</BoxBit>
					) }
				</PageBit>
			) : false
		}
	}
)
