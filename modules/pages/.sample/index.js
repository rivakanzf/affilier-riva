import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import BoxBit from 'modules/bits/box';
import PageBit from 'modules/bits/page';
import TextBit from 'modules/bits/text';


export default ConnectHelper(
	class SamplePage extends PageModel {

		static routeName = 'sample'

		constructor(p) {
			super(p, {
				// initial state
			}, [
				// autobinds
			]);
		}

		view() {
			return (
				<PageBit scrollRef={ this.bindScroller }>
					<BoxBit centering>
						<TextBit>{ this.routeName }</TextBit>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
