import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import CaveatBit from 'modules/bits/caveat';
import SpectralBit from 'modules/bits/spectral';

import Styles from './style';


export default ConnectHelper(
	class GiftCardCenter extends PageModel {

		constructor(p) {
			super(p, {
			}, [
			])
		}

		view() {
			return (
				<BoxBit unflex style={Styles.content}>
					<CaveatBit type={CaveatBit.TYPES.NOTE_1} style={Styles.padder}>
						Dear
					</CaveatBit>
					<CaveatBit type={CaveatBit.TYPES.SUBHEADER_2} weight="bold">
						Anggi Permata Sari
					</CaveatBit>
					<BoxBit unflex style={Styles.message}>
						<CaveatBit type={CaveatBit.TYPES.SUBHEADER_3}>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</CaveatBit>
					</BoxBit>
					<BoxBit unflex>
						<CaveatBit type={CaveatBit.TYPES.SUBHEADER_3} weight="bold" align="right">
							Nova Matunry
						</CaveatBit>
						<SpectralBit type={SpectralBit.TYPES.FOOT_NOTE} weight="normal" style={Styles.orderNumber} weight="medium" align="right">
							12345
						</SpectralBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
