import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	page: {
		width: 793.7007874,
		height: 1122.519685,
	},

	wrap: {
		flexWrap: 'wrap',
	},

	content: {
		width: 793.7007874 / 2,
		height: 1122.519685 / 2,
		paddingTop: 64,
		paddingBottom: 64,
		paddingLeft: 46,
		paddingRight: 46,
	},
	
	message: {
		paddingTop: 22,
		paddingBottom: 26,
	},

	padder: {
		paddingBottom: 2,
	},
})
