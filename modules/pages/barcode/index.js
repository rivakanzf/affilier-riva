import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import ShipmentService from 'app/services/shipment';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import CheckboxBit from 'modules/bits/checkbox';
import TouchableBit from 'modules/bits/touchable';

import TableLego from 'modules/legos/table';
import BadgeStatusLego from 'modules/legos/badge.status';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import Styles from './style';

import { without } from 'lodash'


export default ConnectHelper(
	class PrintPage extends PageModel {
		static TYPES = {
			ADDRESS: 'ADDRESS',
			GIFT: 'GIFT',
			STYLIST_NOTE: 'STYLIST_NOTE',
		}

		static routeName = 'barcode'

		constructor(p) {
			super(p, {
				search: p.search || null,
				isLoading: true,
				status: null,
				count: 40,
				offset: 0,
				total: 0,

				data: [],
				selectedIds: [],

				token: undefined,
			})
		}

		componentDidUpdate(pP, pS) {
			if(
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.status !== this.state.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		tableHeader = [{
			title: '',
			width: .5,
		}, {
			title: 'Order ID',
			width: 2,
		}, {
			title: 'Courier',
			width: 2,
		}, {
			title: 'Receiver',
			width: 2,
		}, {
			title: 'Status',
			width: 1.5,
		}, {
			width: .5,
		}]

		convertTitle = val => {
			switch(val) {
			case this.TYPES.ADDRESS:
				return 'Address'
			case this.TYPES.STYLIST_NOTE:
				return 'Stylist Note'
			case this.TYPES.GIFT:
				return 'Voucher / Gift'
			default:
				return val
			}
		}

		getData = () => {
			const {
				offset,
				count,
				search,
			} = this.state

			this.setState({
				isLoading: true,
			}, () => {
				ShipmentService.getShipment({
					offset,
					search,
					limit: count,
					type: 'SICEPAT',
					status: 'PENDING',
					with_order: true,
				}, this.state.token)
					.then(res => {
						this.setState({
							isLoading: false,
							total: res.count,
							data: res.data,
						})
					})
					.catch(this.onError)
			})
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong.',
				})
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onFilterType = type => {
			switch(type) {
			case this.TYPES.INVOICE:
			case this.TYPES.GIFT:
				return true
			default:
				return false
			}
		}

		onChangeFilter = (key, val) => {
			this.setState({
				[key]: val,
				offset: 0,
			})
		}

		onCheck = id => {
			if (this.state.selectedIds.indexOf(id) > -1) {
				this.setState({
					selectedIds: without(this.state.selectedIds, id),
				})
			} else {
				this.setState({
					selectedIds: [...this.state.selectedIds, id],
				})
			}
		}

		onNavigateToPrint = () => {
			this.navigator.navigate('shipping/' + this.state.selectedIds + '/print/barcode')
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onNavigateToBack = () => {
			this.navigator.back()
		}

		onNavigate = (link, param) => {
			this.navigator.navigate(link, param)
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onSearchHandler = searchValue => {
			this.setState({
				search: searchValue,
				offset: 0,
			})
		}

		rowRenderer = stylesheet => {
			return {
				data: [{
					children: (
						<CheckboxBit
							dumb
							isActive={this.state.selectedIds.indexOf(stylesheet.id) > -1}
							onPress={this.onCheck.bind(this, stylesheet.id)}
						/>
					),
				}, {
					title: stylesheet.order_details[0].order.number || '-',
				}, {
					title: stylesheet.courier || '-',
				}, {
					title: stylesheet.destination.receiver || '-',
				}, {
					children: (
						<BadgeStatusLego status={stylesheet.status || undefined}/>
					),
				}, {
					children: (
						<BoxBit row style={ Styles.icons }>
							<TouchableBit style={ Styles.icon } centering row>
								<BoxBit/>
							</TouchableBit>
							<TouchableBit style={ Styles.icon } centering row>
								<BoxBit/>
							</TouchableBit>
						</BoxBit>
					),
				}],
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles="packing"
					onAuthorized={ this.onAuthorized }
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Print Barcode"
								paths={[{ title: 'PRINT' }]}
								search={ this.state.search }
								searchPlaceholder="Find YCO"
								onSearch={ this.onSearchHandler }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							>
								<ButtonBit
									title="Print"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={ this.state.selectedIds.length ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
									onPress={this.onNavigateToPrint}
								/>
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={Styles.padder}
							/>
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange}
							/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
