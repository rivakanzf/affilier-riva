/* eslint-disable no-nested-ternary */
import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import InputDateTimeBit from 'modules/bits/input.datetime';
import SwitchBit from 'modules/bits/switch';

import BoxRowLego from 'modules/legos/box.row';


import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				onChange: PropTypes.func,

				published_at: PropTypes.date,
				expired_at: PropTypes.date,
				featured: PropTypes.bool,
				onSave: PropTypes.func,
				isLoading: PropTypes.bool,
			}
		}

		view() {
			return (
				<BoxBit>
					<BoxRowLego
						title="Message Actions"
						data={[{
							data: [{
								title: 'Save Changes',
								children: (
									<ButtonBit
										key={ this.props.canSave }

										weight="medium"
										title="CREATE &amp; SAVE MESSAGE"
										type={ ButtonBit.TYPES.SHAPES.PROGRESS }
										onPress={ this.props.onSave }
										state={ this.props.isLoading
											? ButtonBit.TYPES.STATES.LOADING : this.props.canSave
												? ButtonBit.TYPES.STATES.NORMAL
												: ButtonBit.TYPES.STATES.DISABLED }
									/>
								),
							}],
						}, {
							data: [{
								headerless: true,
								children: (
									<InputDateTimeBit
										key={ this.props.published_at }
										title="Publish Date"
										style={Styles.inputDate}
										date={ this.props.published_at }
										onChange={ this.props.onChange.bind(this, 'published_at') }
									/>
								),
							}, {
								headerless: true,
								children: (
									<InputDateTimeBit
										key={ this.props.expired_at }
										title="End Date"
										date={ this.props.expired_at }

										style={Styles.inputDate}
										onChange={ this.props.onChange.bind(this, 'expired_at') }
									/>
								),
							}],
						}, {
							data: [{
								children: (
									<BoxBit key={ this.props.featured } row style={Styles.containerFeature}>
										<BoxBit unflex>
											<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="semibold">
												Featured
											</GeomanistBit>
											<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
												Display on homepage array.
											</GeomanistBit>
										</BoxBit>
										<SwitchBit
											tintColor={Colors.green.primary}
											style={Styles.switch}
											onChange={this.props.onChange.bind(this, 'featured')}
											value={ this.props.featured }
										/>
									</BoxBit>
								),
							}, {
								title: '',
							}],
						}]}
					/>
				</BoxBit>
			)
		}
	}
)
