import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';


import BoxLego from 'modules/legos/box';
import RowsLego from 'modules/legos/rows';

import RowRedirectBroadcast from 'modules/legos/row.redirect.broadcast';
import RowSummaryBroadcast from 'modules/legos/row.summary.broadcast';
import RowTitleBroadcast from 'modules/legos/row.title.broadcast';
import RowSlugBroadcast from 'modules/legos/row.slug.broadcast';

import RowUrlBroadcast from 'modules/legos/row.url.broadcast';
import SelectTypeLego from 'modules/legos/select.type';

import PagePart from '../_page';

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends PromiseStatefulModel {

		constructor(p) {
			super(p, {
				activeIndex: p.redirect === 'link' ? 0 : 1,
			});

			this._options = [{
				key: 'PROMO',
				title: 'Promotion',
			}, {
				key: 'INFO',
				title: 'Information',
			}]
		}

		static propTypes(PropTypes) {
			return {
				onChange: PropTypes.func,
				onChangeTextArea: PropTypes.func,
				onChangeCTA: PropTypes.func,
				onImageLoaded: PropTypes.func,
				onImageRemoved: PropTypes.func,
				
				onChangeCoupon: PropTypes.func,


				type: PropTypes.string,
				title: PropTypes.string,
				summary: PropTypes.string,
				redirect: PropTypes.string,
				link: PropTypes.string,
				message: PropTypes.string,
				tnc: PropTypes.string,
				use_promo: PropTypes.bool,
				couponIds: PropTypes.arrayOf(PropTypes.id),
				assetId: PropTypes.arrayOf(PropTypes.id),
				image: PropTypes.image,
				cta: PropTypes.object,

			}
		}

		onSwitch = (index) => {
			this.setState({
				activeIndex: index,
			}, () => {
				this.props.onChange('redirect', index === 0 ? 'link' : 'page')
			})
		}

		view() {
			return (
				<BoxLego
					title="Message Detail"
					contentContainerStyle={ Styles.container }
				>
					<RowsLego
						data={[{
							data: [{
								title: 'Type',
								children: (
									<SelectTypeLego
										key={ this.props.type }
										onChange={this.props.onChange.bind(this, 'type')}
										selected={ this.props.type}
										values={ this._options.map(option => {
											return {
												key: option.key,
												title: option.title,
												selected: option.key === this.props.type ? true : false,
											}
										}) }
									/>
								),
							}, {
								data: [{
									title: '',
								}],
							}],
						}, {
							data: [{
								headerless: true,
								children: (
									<RowSlugBroadcast
										key={ this.props.slug }
										value={ this.props.slug }
										onChange={this.props.onChange.bind(this, 'slug')}
									/>
								),
							}],
						}, {
							data: [{
								headerless: true,
								children: (
									<RowTitleBroadcast
										key={ this.props.title }
										value={ this.props.title }
										onChange={this.props.onChange.bind(this, 'title')}
									/>
								),
							}],
						}, {
							data: [{
								headerless: true,
								children: (
									<RowSummaryBroadcast
										key={ this.props.summary }
										value={ this.props.summary }
										onChange={this.props.onChange.bind(this, 'summary')}
									/>
								),
							}],
						}, {
							data: [{
								title: 'Redirect To',
								children: (
									<RowRedirectBroadcast
										data={[{
											title: 'URL Link',
										}, {
											title: 'Page',
										}]}
										activeIndex={ this.state.activeIndex }
										onPress={this.onSwitch}
									/>
								),
							}],
						}]}
						style={ Styles.content }
					/>
					<RowsLego
						data={[{
							data: [{
								headerless: true,
								children: this.state.activeIndex === 0 ? (
									<RowUrlBroadcast
										key={ this.props.link }
										onChange={this.props.onChange.bind(this, 'link')}
										value={ this.props.link}
									/>
								) : (
									<PagePart
										key={ `${this.props.redirect}_${this.props.cta.type}` }
										onChange={ this.props.onChange }
										onChangeCoupon={ this.props.onChangeCoupon }
										onChangeCTA={ this.props.onChangeCTA }
										onChangeTextArea={ this.props.onChangeTextArea }
										onImageLoaded={ this.props.onImageLoaded }
										onImageRemoved={ this.props.onImageRemoved }

										message={ this.props.message }
										tnc={ this.props.tnc }
										cta={ this.props.cta }
										use_promo={ this.props.use_promo }
										couponIds={ this.props.couponIds }
										assetId={ this.props.assetId }
										image={ this.props.image }
									/>
								),
							}],
						}]}
					/>
				</BoxLego>
			)
		}
	}
)
