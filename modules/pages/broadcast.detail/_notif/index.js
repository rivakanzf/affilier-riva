import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import InputDateTimeBit from 'modules/bits/input.datetime';
import SwitchBit from 'modules/bits/switch';
import TouchableBit from 'modules/bits/touchable';

import BoxRowLego from 'modules/legos/box.row';


import Styles from './style';


export default ConnectHelper(
	class NotifPart extends ConnectedStatefulModel {

		constructor(p) {
			super(p, {
				activeIndex: 0,
			});
		}

		static propTypes(PropTypes) {
			return {
				onChange: PropTypes.func,
				schedulePush: PropTypes.bool,
				schedulePush_at: PropTypes.date,
			}
		}

		onSwitch = (index) => {
			this.setState({
				activeIndex: index,
			}, () => {
				this.props.onChange('schedulePush', index === 0 ? true : false)
			})
		}

		scheduleRenderer = () => {
			return (
				<BoxBit unflex>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.mt8}>
						Push at the same time as Publish Date
					</GeomanistBit>
					<BoxBit unflex row style={Styles.containerChoice}>
						<TouchableBit unflex style={[Styles.btn, this.state.activeIndex === 0 ? Styles.btnActive : false]} onPress={this.onSwitch.bind(this, 0)}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={this.state.activeIndex === 0 ? Styles.white : undefined}>
								Yes
							</GeomanistBit>
						</TouchableBit>
						<BoxBit unflex style={Styles.divider} />
						<TouchableBit unflex style={[Styles.btn, this.state.activeIndex === 1 ? Styles.btnActive : false]} onPress={this.onSwitch.bind(this, 1)}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={this.state.activeIndex === 1 ? Styles.white : undefined}>
								No
							</GeomanistBit>
						</TouchableBit>
					</BoxBit>
					{ this.state.activeIndex === 0 ? (
						false
					) : (
						<InputDateTimeBit title=" " style={Styles.inputdate}
							onChange={ this.props.onChange.bind(this, 'scheduleTime') }
						/>
					) }
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit style={Styles.container}>
					<BoxRowLego
						title="Push Notification"
						header={(
							<SwitchBit
								tintColor={Colors.green.primary}
								value={ this.props.schedulePush}
								onChange={this.props.onChange.bind(this, 'pushNotification')}
							/>
						)}
						data={[{
							data: [{
								title: 'Schedule Push',
								children: (
									this.scheduleRenderer()
								),
							}, {
								title: '',
							}],
						}]}
					/>
				</BoxBit>
			)
		}
	}
)
