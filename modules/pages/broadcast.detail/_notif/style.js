import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		marginTop: 16,
	},

	containerFeature: {
		alignItems: 'center',
	},

	containerChoice: {
		marginTop: 8,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 2,
		alignSelf: 'flex-start',
	},

	btn: {
		paddingTop: 9,
		paddingLeft: 17,
		paddingRight: 16,
		paddingBottom: 7,
	},

	btnActive: {
		backgroundColor: Colors.primary,
	},

	white: {
		color: Colors.white.primary,
	},

	mt8: {
		marginTop: 8,
	},

	inputdate: {
		marginTop: -12,
	},

})
