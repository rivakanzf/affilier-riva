import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import InputFileBit from 'modules/bits/input.file';
import TextAreaBit from 'modules/bits/text.area';
import TouchableBit from 'modules/bits/touchable';
import TextInputBit from 'modules/bits/text.input'

import SelectTypeLego from 'modules/legos/select.type';
import RowsLego from 'modules/legos/rows';
import RowRedirectBroadcastLego from 'modules/legos/row.redirect.broadcast'
import LoaderBit from 'modules/bits/loader'

import UserService from 'app/services/user.new'
import CouponService from 'app/services/coupon'


import Styles from './style';


export default ConnectHelper(
	class DetailPart extends PromiseStatefulModel {

		static stateToProps(state) {
			return {
				token: state.me.token,
				id: state.me.id,
			}
		}

		static propTypes(PropTypes) {
			return {
				onChange: PropTypes.func,
				onChangeTextArea: PropTypes.func,
				onChangeCTA: PropTypes.func,
				onImageLoaded: PropTypes.func,
				onImageRemoved: PropTypes.func,
				onChangeCoupon: PropTypes.func,

				message: PropTypes.string,
				cta: PropTypes.object,
				tnc: PropTypes.string,
				use_promo: PropTypes.bool,
				couponIds: PropTypes.arrayOf(PropTypes.id),
				assetId: PropTypes.arrayOf(PropTypes.id),
				image: PropTypes.image,

			}
		}

		constructor(p) {
			super(p, {
				isUploading: false,
				activeIndex: p.use_promo ? 0 : 1,
				codeCount: p.couponIds.length,
				addable: true,
				cta: {
					isActive: p.cta.isActive,
					type: p.cta.type,
					list: [{
						key: 'page',
						title: 'Page',
					}, {
						key: 'link',
						title: 'Link',
					}],
					data: {
						url: p.cta.url,
						route: p.cta.route,
						title: p.cta.title,
						parameter: p.cta.parameter,
					},
					pages: ['Matchbox', 'Session', 'Prestyled', 'Inbox', 'Signup', 'Login'],
				},
				coupons:[],
				image: p.image,
			});
		}

		componentDidMount() {
			this.getCouponData()
		}

		getCouponData = () => {
			return CouponService.getCoupons({expired:false, is_public: true}, this.props.token).then(res => {
				this.setState({
					coupons: res.data,
				})
			})
		}

		onSwitch = (index) => {
			this.setState({
				activeIndex: index,
			}, () => {
				this.props.onChange('use_promo', index === 0 ? true : false)
			})
		}

		onDataLoaded = (id, image) => {
			if(id === 'loader' && image === undefined) {
				this.setState({image: undefined}, () => {
					this.props.onImageRemoved()
				})
			} else if(id === 'adder') {
				this.setState({isUploading: true}, () => {
					UserService.upload(this.props.id, {
						image,
					}, this.props.token).then(res => {
	
						this.props.onImageLoaded
						&& this.props.onImageLoaded(res)
	
						this.setState({image: res.url, isUploading: false})
					})
				})
			}
		}

		onAddNewCode = () => {
			this.setState({
				codeCount: this.state.codeCount + 1,
			})
		}

		onChangeCTAType = (key, index) => {
			this.onChangeCTA(key, this.state.cta.list[index].key)
		}

		onChangeCTA = (key, value) => {
			if(key === 'type' || key === 'isActive') {
				this.state.cta[key] = value

				this.props.onChangeCTA &&
				this.props.onChangeCTA(key, value)
				
				this.forceUpdate()
				
			} else {
				this.props.onChangeCTA &&
				this.props.onChangeCTA(key, value)
			}
		}

		onUpdateField = (key, e, value) => {
			this.state.cta.data[key] = value

			this.onChangeCTA(key, value)
		}

		codeRenderer = (i) => {
			return (
				<SelectTypeLego
					key={ i }
					style={Styles.mt8}
					placeholder={'Select Coupon'}
					onChange={this.props.onChangeCoupon.bind(this, i)}
					values={ this.state.coupons.map( coupon => {
						return {
							key: coupon.id,
							title: coupon.title,
							selected: coupon.id === this.props.couponIds[i],
						}
					})}
				/>
			)
		}
		
		btnRenderer = () => {
			return (
				<TouchableBit unflex row centering style={Styles.containerAdd} onPress={ this.onAddNewCode }>
					<IconBit name="add-square" size={16} />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.add}>
						Add more promo code
					</GeomanistBit>
				</TouchableBit>
			)
		}

		promoRenderer = () => {
			return (
				<BoxBit unflex>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
						Use promo code.
					</GeomanistBit>
					<BoxBit unflex row style={Styles.containerChoice}>
						<TouchableBit unflex style={[Styles.btn, this.state.activeIndex === 0 ? Styles.btnActive : {}]} onPress={this.onSwitch.bind(this, 0)}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={this.state.activeIndex === 0 ? Styles.white : {}}>
								Yes
							</GeomanistBit>
						</TouchableBit>
						<BoxBit unflex style={Styles.divider} />
						<TouchableBit unflex style={[Styles.btn, this.state.activeIndex === 1 ? Styles.btnActive : {}]} onPress={this.onSwitch.bind(this, 1)}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={this.state.activeIndex === 1 ? Styles.white : {}}>
								No
							</GeomanistBit>
						</TouchableBit>
					</BoxBit>
					{ this.state.activeIndex === 0 ? (
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
								Select from existing promo code. Dates will inherit from promo code.
							</GeomanistBit>
							{ this.state.coupons.length !== 0 && Array.from(Array(this.state.codeCount ).keys()).map(this.codeRenderer) }
							{ this.btnRenderer() }
						</BoxBit>
					) : (
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="semibold" style={Styles.header}>
								Default Message
							</GeomanistBit>
							<BoxBit style={Styles.containerDescriptionCode}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									Otomatis diterapkan pada saat checkout tanpa kode promo.
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					) }
				</BoxBit>
			)
		}

		ctaRenderer = () => {
			return (
				<BoxBit unflex>
					<BoxBit unflex row style={Styles.containerChoice}>
						<TouchableBit unflex style={[Styles.btn, this.state.cta.isActive === false ? Styles.btnActive : {}]} onPress={ this.onChangeCTA.bind(this, 'isActive', false) }>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={this.state.cta.isActive === false ? Styles.white : {}}>
								No
							</GeomanistBit>
						</TouchableBit>
						<BoxBit unflex style={Styles.divider} />
						<TouchableBit unflex style={[Styles.btn, this.state.cta.isActive === true ? Styles.btnActive : {}]} onPress={ this.onChangeCTA.bind(this, 'isActive', true) }>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={this.state.cta.isActive === true ? Styles.white : {}}>
								Yes
							</GeomanistBit>
						</TouchableBit>
					</BoxBit>

					{ this.state.cta.isActive && (
						<BoxBit key={this.state.cta.isActive}>
							<RowRedirectBroadcastLego
								activeIndex={ this.state.cta.list.findIndex(item => item.key === this.state.cta.type) }
								data={ this.state.cta.list }
								onPress={ this.onChangeCTAType.bind(this, 'type') }
							/>
							<BoxBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.mt8}>
									Button Label
								</GeomanistBit>
								<TextInputBit
									key={ `title_${ this.state.cta.data.title }` }
									placeholder={ this.state.cta.data.title || 'e.g Go To Matchbox' }
									// value={ this.state.cta.data.title }
									onChange={ this.onUpdateField.bind(this, 'title') }
									style={Styles.mt8}
								/>
							</BoxBit>
							{ this.state.cta.type === 'page' && (
								<BoxBit key={this.state.cta.type}>
									<BoxBit>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.mt8}>
											Route
										</GeomanistBit>
										<TextInputBit
											key={`route_${ this.state.cta.data.route }`}
											placeholder={this.state.cta.data.route || 'e.g matchbox'}
											onChange={ this.onUpdateField.bind(this, 'route') }
											style={Styles.mt8}
										/>
									</BoxBit>
									<BoxBit>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.mt8}>
											Parameter (e.g: slug of a product)
										</GeomanistBit>
										<TextInputBit
											key={`parameter_${ this.state.cta.data.parameter }`}
											placeholder={this.state.cta.data.parameter || 'e.g basic | mini'}
											onChange={ this.onUpdateField.bind(this, 'parameter') }
											style={Styles.mt8}
										/>
									</BoxBit>
								</BoxBit>
							)}
							{ this.state.cta.type === 'link' && (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.mt8}>
										URL
									</GeomanistBit>
									<TextInputBit
										key={`url_${ this.state.cta.data.url }`}
										placeholder={this.state.cta.data.url || 'https://tokopedia.com/yuna'}
										onChange={ this.onUpdateField.bind(this, 'url') }
										style={Styles.mt8}
									/>
								</BoxBit>
							)}
						</BoxBit>
					) }
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit style={ Styles.container }>
					<RowsLego
						data={[{
							data: [{
								title: 'Message Content',
								children: (
									<TextAreaBit
										key={ this.props.message }
										style={Styles.message}
										onChange={ this.props.onChangeTextArea.bind(this, 'message') }
										defaultValue={ this.props.message }
									/>
								),
							}, {
								title: 'Terms and Conditions',
								children: (
									<TextAreaBit
										key={ this.props.tnc }
										style={Styles.terms}
										onChange={ this.props.onChangeTextArea.bind(this, 'tnc') }
										defaultValue={ this.props.tnc }
									/>
								),
								description: 'Enter override terms and conditions. Displayed in bullet points. Press enter to create new bullet point.',
							}],
						}, {
							data: [{
								title: 'Promo Code',
								children: (
									this.promoRenderer()
								),
							}, {
								title: 'Cover Image',
								children: this.state.isUploading ? <LoaderBit/> : (
									<InputFileBit
										id={this.state.image ? 'loader' : 'adder'}
										maxSize={3}
										value={ this.state.image && `//${this.state.image}` }
										onDataLoaded={this.onDataLoaded}
									/>
								),
								description: 'Image ratio 16:9. Recommended size is 1920x1080 or 1280x720 (px).',
							}],
						}, {
							data: [{
								title: 'Action Call',
								children: (
									this.ctaRenderer()
								),
							}, (this.state.cta.isActive && this.state.cta.type === 'page' ? {
								title: 'Supported Page',
								children: (
									<BoxBit>
										{ this.state.cta.pages.map((page, i) => {
											return (
												<BoxBit key={i} row unflex style={Styles.supported}>
													<IconBit name="circle-checkmark" size={16} color={Colors.primary} />
													<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.supportedText}>{ page }</GeomanistBit>
												</BoxBit>
											)
										})}
									</BoxBit>
								),
							} : {})],
							style: Styles.ctaDivider,
						}]}
						style={ Styles.content }
					/>
				</BoxBit>
			)
		}
	}
)
