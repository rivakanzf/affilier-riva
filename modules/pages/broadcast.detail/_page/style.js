import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
	},

	content: {
	},

	note: {
		color: Colors.black.palette(2, .6),
		paddingTop: 8,
	},

	containerChoice: {
		marginTop: 8,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 2,
		alignSelf: 'flex-start',
	},

	ctaDivider: {
		marginTop: Sizes.margin.default,
		borderTopWidth: 1,
		borderColor: Colors.black.palette(2, .2),
	},

	btn: {
		paddingTop: 9,
		paddingLeft: 17,
		paddingRight: 16,
		paddingBottom: 7,
	},

	btnActive: {
		backgroundColor: Colors.primary,
	},
	
	supported: {
		marginBottom: 4,
		alignItems: 'center',
	},

	supportedText: {
		marginLeft: 6,
	},

	white: {
		color: Colors.white.primary,
	},

	divider: {
		width: StyleSheet.hairlineWidth,
		backgroundColor: Colors.black.palette(2, .2),
	},

	mt8: {
		marginTop: 8,
	},

	header: {
		paddingTop: 4,
		paddingBottom: 4,
		marginTop: 16,
	},

	containerDescriptionCode: {
		paddingTop: 12,
		paddingLeft: 16,
		paddingRight: 16,
		paddingBottom: 12,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 2,
	},

	containerAdd: {
		alignSelf: 'flex-start',
		marginTop: 17,
	},

	add: {
		marginLeft: 8,
		textDecoration: 'underline',
	},

	message: {
		height: 144,
	},

	terms: {
		height: 124,
	},

	inputFile: {
		width: '100%',
		height: 144,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
	},

	image: {
		height: 128,
		width: 96,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .16),
	},

	loading: {
		width: 100,
	}

})
