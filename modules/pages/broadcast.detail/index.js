import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';
import CommonHelper from 'coeur/helpers/common';

import BoxBit from 'modules/bits/box';

import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import NotificationService from 'app/services/notification'
import ActionsPart from './_actions';
import DetailPart from './_detail';
import NotifPart from './_notif';

import Styles from './style';

export default ConnectHelper(
	class BroadcastDetailPage extends PageModel {

		static routeName = 'broadcast.detail'

		static stateToProps(state) {
			return {
				token: state.me.token,
				userId: state.me.id,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isChanged: true,
			})

			this.data = {
				// detail state
				type: '',
				slug: '',
				title: '',
				summary: '',
				redirect: 'link',

				// detail -> link state
				link: null,

				// detail -> page state
				message: null,
				tnc: null,
				assetId: [],
				use_promo: true,
				couponIds: [],
				cta: {
					isActive: false,
					type: p.type || 'page',
					title: null,
					route: null,
					url: null,
					parameter: null,
				},
				
				// actions state
				published_at: null,
				expired_at: null,
				featured: false,

				// push notification state
				// pushNotification: false,
				schedulePush: false,
				scheduleTime: null,
			}
		}

		getData = () => {
			if(this.state.token && this.props.id && !this.state.isLoading) {
				this.setState({
					isLoading: true,
				}, () => {
					NotificationService.getNotification(this.props.id, this.state.token).then(res => {
						this.data.type = res.type
						this.data.slug = res.slug
						this.data.title = res.title
						this.data.summary = res.summary
						this.data.redirect = res.url === null ? 'page' : 'link'

						this.data.link = res.url

						this.data.message = res.description
						this.data.tnc = res.terms
						this.data.image = res.image
						this.data.use_promo = res.coupons.length > 0 ? true : false
						this.data.couponIds = res.coupons.length > 0
							? res.coupons.map(coupon => {
								return coupon.id
							})
							: []

						this.data.published_at = res.published_at
						this.data.expired_at = res.expired_at
						this.data.featured = res.is_featured
						
						this.data.pushNotification = res.push_notification
						this.data.schedulePush = res.pushed_at === null ? false : true
						this.data.scheduleTime = res.pushed_at
						
						if(!!res.metadata.call_to_action) {
							this.data.cta.isActive = true
							this.data.cta.type = res.metadata.call_to_action.type
							this.data.cta.url = res.metadata.call_to_action.url
							this.data.cta.title = res.metadata.call_to_action.title
							this.data.cta.route = res.metadata.call_to_action.route
							this.data.cta.parameter = res.metadata.call_to_action.params
						}

						this.setState({
							redirect: res.url === null ? 'page' : 'link',
							isLoading: false,
							isChanged: false,

						})
					}).catch(() => {
						this.setState({
							isLoading: false,
						})
					})
				})
			}
		}

		prepareData = () => {
			const ctaData = {
				type: this.data.cta.type,
				title: this.data.cta.title,
				...(this.data.cta.type === 'link' ? {
					url: this.data.cta.url,
				} : {
					title: this.data.cta.title,
					route: this.data.cta.route,
					params: this.data.cta.parameter,
				}),
			}

			return CommonHelper.stripUndefined(
				{
					slug: this.data.slug,
					type : this.data.type,
					title : this.data.title,
					description : this.data.message,

					url: this.data.link,

					summary: this.data.summary,
					terms: this.data.tnc,
					coupon_ids: this.data.couponIds ? this.data.couponIds : [],
					image: this.data.image,

					published_at : this.data.published_at,
					expired_at : this.data.expired_at,
					is_featured: this.data.featured,
					...(this.data.cta.isActive ? {
						call_to_action: ctaData,
					} : {}),

					// push_notification : this.data.pushNotification,
					// pushed_at: this.data.scheduleTime,
				}
			)
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		onNavigateToBroadcast = () => this.navigator.navigate('broadcast')

		onNavigateToBroadcastDetail = id => {
			this.navigator.navigate(`broadcast/detail/${id}`)
		}

		onSave = () => {
			if (
				this.data.type !== '' &&
				this.data.title !== '' &&
				this.data.message !== '' &&
				this.data.slug !== ''
			) {
				this.setState({
					isLoading: true,
				}, () => {
					if(this.props.id) {
						NotificationService.updateNotification(this.props.id, this.prepareData(), this.state.token).then( ()=> {
							this.utilities.notification.show({
								title: 'Yeay…',
								message: 'Notification Updated',
								type: this.utilities.notification.TYPES.SUCCESS,
							})
							this.setState({isLoading: false})
						}).catch(err => {
							this.onError(err)
						})
	
					} else {
						NotificationService.addNotification(this.prepareData(), this.state.token).then( res => {
							this.utilities.notification.show({
								title: 'Yeay…',
								message: 'Notification Saved',
								type: this.utilities.notification.TYPES.SUCCESS,
							})
							this.setState({isLoading: false}, ()=> {
								this.onNavigateToBroadcastDetail(res.id)

							})
						}).catch(err => {
							this.onError(err)
						})
	
					}
				})
			
				
			} else {
				this.utilities.notification.show({
					title: 'Oops…',
					message: 'Either type or title or slug or message is not defined.',
				})
			}
		}

		onError = (err) => {
			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops…',
					message: err.message,
				})
			})
		}
	
		onChange = (key, val) => {
			this.data[key] = val
			if (this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onChangeCTA = (key, val) => {
			this.data.cta[key] = val

			if(this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onChangeCoupon = (index, val) => {
			const newIds = this.data.couponIds.slice()
			newIds[index] = val
			this.data.couponIds = newIds

			if (this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}

		}

		onChangeTextArea = (key, e, val) => {
			this.data[key] = val

			if (this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onImageLoaded = (img) => {
			if(img.length !== 0) {
				// save image
				this.data.assetId = [img.id]
				this.data.image = img.url
			} else {
				// delete image
				this.data.assetId = []
				this.data.image = ''
			}

			if (this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}

		}

		onImageRemoved = () => {
			this.data.image = undefined
			this.data.assetId = []

			if (this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					onAuthorized={ this.onAuthorized }
					paths={[{
						title: 'Broadcast',
						onPress: this.onNavigateToBroadcast,
					}, {
						title: this.props.id,
					}]}
				>
					{() => (
						<BoxBit style={Styles.container}>
							<HeaderPageComponent
								title="Create Message"
								// paths={[{
								// 	title: 'BROADCAST',
								// }]}
							/>
							<BoxBit row>
								<BoxBit style={Styles.padder}>
									<DetailPart
										onChange={ this.onChange }
										onChangeTextArea={ this.onChangeTextArea }
										onChangeCTA={ this.onChangeCTA }
										onImageLoaded={ this.onImageLoaded }
										onImageRemoved={ this.onImageRemoved }

										onChangeCoupon={ this.onChangeCoupon}
										type={ this.data.type }
										slug={ this.data.slug }
										title={ this.data.title }
										summary={ this.data.summary }
										redirect={ this.state.redirect }
										link={ this.data.link }
										message={ this.data.message }
										tnc={ this.data.tnc }
										use_promo={ this.data.use_promo }
										couponIds={ this.data.couponIds }
										assetId={ this.data.assetId}
										image={ this.data.image }
										cta={ this.data.cta }
									/>
								</BoxBit>
								<BoxBit>
									<ActionsPart
										canSave={ this.state.isChanged}
										onChange={ this.onChange }
										published_at={ this.data.published_at }
										expired_at={ this.data.expired_at }
										featured={ this.data.featured }
										onSave={ this.onSave }
										isLoading={ this.state.isLoading }
									/>
									<NotifPart
										onChange={ this.onChange }
										schedulePush={ this.data.schedulePush }
										schedulePush_at={ this.data.scheduleTime }
									/>
								</BoxBit>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
