import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingLeft: 32,
		paddingRight: 32,
		paddingBottom: Sizes.margin.default,
		paddingTop: Sizes.margin.default,
	},

	padder: {
		marginRight: Sizes.margin.default,
	},

	tab: {
		marginBottom: Sizes.margin.default,
	},

})
