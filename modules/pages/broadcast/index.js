import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
// import AuthenticationHelper from 'utils/helpers/authentication'
// import CommonHelper from 'coeur/helpers/common';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import InputValidatedBit from 'modules/bits/input.validated'
import ButtonBit from 'modules/bits/button'
import LoaderBit from 'modules/bits/loader'


import TableLego from 'modules/legos/table';
// import SelectDateLego from 'modules/legos/select.date';
// import SelectStatusBadgeLego from 'modules/legos/select.status.badge';
import SelectTypeLego from 'modules/legos/select.type';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import NotificationService from 'app/services/notification'

import TimeHelper from 'coeur/helpers/time';

import { isEmpty, orderBy } from 'lodash'


import Styles from './style';
// import notification from '../../../app/services/notification';


export default ConnectHelper(
	class BroadcastPage extends PageModel {

		static routeName = 'broadcast'

		constructor(p) {

			// const query = CommonHelper.getQueryString()

			super(p, {
				isLoading: false,
				total: 0,
				offset: 0,
				count: 20,
				status: null,
				search: null,
				token: undefined,
				isUpdating: false,
				updatedId: null,
				ordered: null,
			})
		}

		componentDidUpdate(pP, pS) {
			if(
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.status !== this.state.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
					|| pS.type !== this.state.type
				)
			) {
				this.getData()
			}
		}


		roles = ['fulfillment', 'stylist', 'finance', 'packing', 'analyst']

		tableHeader = [{
			title: 'ID',
			width: 1,
		}, {
			title: 'Type',
			width: 1,
		}, {
			title: 'Title',
			width: 3.5,
		}, {
			title: 'Publish Date',
			width: 1,
		}, {
			title: 'Push',
			width: .5,
		}, {
			title: 'Featured',
			width: 1,
		}, {
			title: 'Urutan',
			width: 2,
		}]

		getData = () => {
			const {
				offset,
				count,
				search,
				type,
			} = this.state
			this.setState({isLoading: true}, () => {
				NotificationService.getNotifications(this.state.token,
					{
						offset,
						limit: count,
						search,
						type,
					})
					.then(res => {
						this.setState({
							data: orderBy(res.data, ['order']),
							total: res.count,
							isLoading: false,
							isUpdating: false,
						})
					})
			})
			
		}

		// eslint-disable-next-line no-shadow
		rowRenderer = notification => {
			return {
				data: [{
					title: notification.id,
				}, {
					title: notification.type,
				}, {
					title: notification.title,
				}, {
					title: notification.published_at
						? TimeHelper.moment(notification.published_at).format('DD MMM YY')
						: '-',
				}, {
					title: notification.push_notification,
				}, {
					children: (
						<BoxBit unflex style={{alignItems: 'flex-start'}}>
							<IconBit
								name={ notification.is_featured ? 'circle-checkmark' : 'circle-null' }
								color={ notification.is_featured ? Colors.green.palette(1) : Colors.grey.palette(6) }
							/>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit row unflex style={{alignItems: 'flex-start'}}>
							<InputValidatedBit type={'NUMBER'} value={notification.order} onChange={this.onChangeOrder.bind(this, notification.id)}/>
							{
								this.state.isUpdating && notification.id === this.state.updatedId
									? (
										<BoxBit centering>
											<LoaderBit simple/>
										</BoxBit>
									) : (
										<ButtonBit
											title="Ubah"
											onPress={ this.onUpdateOrder.bind(this, notification.id, notification.order)}
										/>
									)
							}
						</BoxBit>
					),
				}],
				onPress: this.onNavigateToBroadcastDetail.bind(this, notification.id),
			}
		}

		onChangeOrder = (id, val) => {
			// (id, val, isValid)
			this.setState({
				updatedId: id,
				ordered: val,
			})
		}

		onUpdateOrder = (id, oldOrder) => {
			if(this.state.ordered !== null) {
				this.setState({
					isUpdating: true,
				})
	
				const temp = this.state.data.filter(data => data.order === parseInt(this.state.ordered, 10))[0]
	
				if(isEmpty(temp)) {
					NotificationService.updateNotification(id, {order: parseInt(this.state.ordered, 10)}, this.state.token).then(() => {
						this.getData()
					})
				} else {
					NotificationService.updateNotification(id, {order: parseInt(this.state.ordered, 10)}, this.state.token).then(() => {
						NotificationService.updateNotification(temp.id, {order: parseInt(oldOrder, 0)}, this.state.token).then(() => {
							this.getData()
						})
					})
				}

			}
		}

		onChange = (key) => {
			this.setState({
				type: key,
			})
		}

		onNavigateToBroadcastDetail = id => {
			this.navigator.navigate(`broadcast/detail/${id}`)

		}

		onCreateMessage = () => {
			this.navigator.navigate('broadcast/detail')
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onSearchHandler = searchValue => {
			this.setState({
				search: searchValue,
				offset: 0,
			})
		}

		loadingRenderer = () => {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Broadcast' }]}
				>
					{() => (
						<BoxBit centering>
							<LoaderBit simple/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}

		view() {
			if(this.state.isLoading) {
				return this.loadingRenderer()
			}
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Broadcast' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Broadcast"
								// paths={[{ title: 'BROADCAST' }]}
								search={ this.state.search }
								searchPlaceholder="Search"
								buttonPlaceholder="Create Message"
								onSearch={ this.onSearchHandler }
								onPress={ this.onCreateMessage }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							>
								<SelectTypeLego
									values={ [
										{key: 'INFO', title: 'INFO'},
										{key: 'PROMO', title: 'PROMO'},
									] }
									onChange={ this.onChange.bind(this)}
									placeholder="Filter by type"
								/>
								{/* <SelectDateLego /> */}
							</HeaderFilterComponent>
							<TableLego
								isLoading = { this.state.isLoading }
								headers={ this.tableHeader }
								rows={this.state.data && this.state.data.map(this.rowRenderer)}
								style={Styles.padder}
							/>
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange}
							/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
