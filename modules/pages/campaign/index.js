import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'
import CommonHelper from 'coeur/helpers/common'
import TimeHelper from 'coeur/helpers/time'
import Colors from 'coeur/constants/color'

import CampaignService from 'app/services/campaign'

import BoxBit from 'modules/bits/box'
import SelectionBit from 'modules/bits/selection'
import GeomanistBit from 'modules/bits/geomanist'
import IconBit from 'modules/bits/icon'

import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import TableLego from 'modules/legos/table'

import { isEmpty } from 'lodash'

import Styles from './style'

export default ConnectHelper(
	class Campaign extends PageModel {

		static routeName = 'campaign'

		constructor(p) {
			const query = CommonHelper.getQueryString()
			super(p, {
				token: null,

				offset: 0,
				limit: 30,
				filter: {
					// search: query.search || null,
					status: query.status || null,
				},

				isLoading: true,
				data: [],
			})
		}

		headers = [{
			title: 'Campaign Name',
			width: 2,
		}, {
			title: 'Active',
			align: 'center',
			width: 1,
		}, {
			title: 'Published Date',
			width: 1,
		}, {
			title: 'End Date',
			width: 1,
		}]

		statuses = [{
			key: null,
			title: 'All status',
		}, {
			key: 'active',
			title: 'Active',
		}, {
			key: 'inactive',
			title: 'Inactive',
		}]

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		componentDidUpdate(prevProps, prevState) {
			if(
				prevState.offset !== this.state.offset ||
				// prevState.filter.search !== this.state.filter.search ||
				prevState.filter.status !== this.state.filter.status
			) {
				const query = {
					// ...(!!this.state.filter.search && { search: this.state.filter.search }),
					...(!!this.state.filter.status && { status: this.state.filter.status }),
				}
				window.history.pushState(
					{},
					'Campaign',
					isEmpty(query)
						? '/campaign'
						: `/campaign?${Object.keys(query).map(key => `${key}=${query[key]}`).join('&')}`
				)
				this.getData()
			}
		}

		getData() {
			const { filter } = this.state

			this.setState({
				isLoading: true,
			}, () => {
				CampaignService.getCampaigns({
					offset: this.state.offset,
					limit: this.state.limit,
					...(!!filter.search && { search: filter.search }),
					...(!!filter.status && { status: filter.status }),
				}, this.state.token).then(({data}) => {
					if(!!data.length) {
						this.setState({
							isLoading: false,
							data: !filter.status
								? data
								: data.filter(d => {
									return filter.status === 'active'
										? d.is_active
										: !d.is_active
								}),
						})
					} else this.setNoData()
				}).catch(this.setNoData)
			})
		}

		setNoData = () => this.setState({ isLoading: false })

		onChangeFilter = (status, key) => {
			this.setState(state => ({
				filter: {
					...state.filter,
					[key]: status,
				},
			}))
		}

		rowRenderer(campaign) {
			return {
				data: [{
					children: (<>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium">
							{ campaign.title }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
							{ campaign.description }
						</GeomanistBit>
					</>),
				}, {
					children: (
						<IconBit
							name={ campaign.is_active ? 'circle-checkmark' : 'circle-null' }
							color={ campaign.is_active ? Colors.green.palette(1) : Colors.grey.palette(6) }
        				/>
					),
				}, {
					children: campaign.publish_date ? (<>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							{ TimeHelper.moment(campaign.publish_date).format('DD/MM/YYYY — HH:mm') }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.dateNote }>
							{ TimeHelper.moment(campaign.publish_date).fromNow() }
						</GeomanistBit>
					</>) : false,
				}, {
					children: campaign.end_date ? (<>
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							{ TimeHelper.moment(campaign.end_date).format('DD/MM/YYYY — HH:mm') }
						</GeomanistBit>
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.dateNote }>
							{ TimeHelper.moment(campaign.end_date).fromNow() }
						</GeomanistBit>
					</>) : false,
				}],
				// onPress: () => this.onNavigateToCampaignDetail(campaign.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent roles={['marketing', 'analyst']} onAuthorized={ this.onAuthorized } paths={ [{ title: 'Campaigns' }] }>{ () => (
					<BoxBit style={Styles.main}>
						<HeaderPageComponent
							title="Campaigns"
							buttonPlaceholder="Create Campaign"
							onPress={ this.onNavigateToCreate }
							searchPlaceholder={ 'ID, Campaign Name' }
							search={ this.state.search }
							onSearch={ this.onSearch }
						/>
						<HeaderFilterComponent
							current={ this.state.offset }
							count={ this.state.data.length }
							total={ this.state.data.length }
							onNavigationChange={ this.onNavigationChange }
						>
							<SelectionBit
								placeholder="Status Campaign"
								value={ this.state.filter.status }
								options={ this.statuses }
								onChange={ val => this.onChangeFilter(val, 'status') }
							/>
						</HeaderFilterComponent>
						<TableLego
							isLoading={ this.state.isLoading }
							headers={ this.headers }
							rows={ this.state.data.map(this.rowRenderer) }
							style={ Styles.padder }
						/>
					</BoxBit>
				) }</PageAuthorizedComponent>
        	)
		}

	}
)
