import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UserService from 'app/services/user.new'

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';

import BoxRowLego from 'modules/legos/box.row';


import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				isManager: PropTypes.bool,
				loading: PropTypes.bool,
				disabled: PropTypes.bool,
				updatedAt: PropTypes.date,
				onPress: PropTypes.func,
				onNavigateToOrder: PropTypes.func,
			}
		}

		static propsToPromise(state, oP) {
			return UserService.getStyleProfileStatus(oP.id, state.me.token)
		}

		viewOnLoading() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Style Profile Quiz',
							children: (
								<BoxBit unflex row style={Styles.spq}>
									<LoaderBit simple />
								</BoxBit>
							),
							description: `Last update: ${TimeHelper.format(this.props.updatedAt, 'DD MMM YYYY HH:mm')}`,
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ 'UPDATE USER' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.props.loading ? ButtonBit.TYPES.STATES.LOADING : this.props.disabled && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL }
									onPress={ this.props.onPress }
								/>
							),
						}],
					}, {
						data: [{
							headerless: true,
							children: (
								<ButtonBit
									title={'VIEW CLIENT\'S ORDER'}
									theme={ ButtonBit.TYPES.THEMES.SECONDARY }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									onPress={ this.props.onNavigateToOrder }
								/>
							),
							style: Styles.header,
						}, {
							blank: true,
						}],
					}]}
				/>
			)
		}

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Style Profile Quiz',
							children: (
								<BoxBit unflex row style={Styles.spq}>
									<IconBit
										size={ 22 }
										name={ this.props.data ? 'circle-checkmark' : 'circle-null' }
										color={ this.props.data ? Colors.green.palette(1) : Colors.red.palette(3) }
										style={ Styles.icon }
									/>
									<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>{ this.props.data ? 'Completed' : 'Incomplete' }</GeomanistBit>
								</BoxBit>
							),
							description: `Last update: ${TimeHelper.format(this.props.updatedAt, 'DD MMM YYYY HH:mm')}`,
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ 'UPDATE USER' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.props.loading ? ButtonBit.TYPES.STATES.LOADING : this.props.disabled && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL }
									onPress={ this.props.onPress }
								/>
							),
						}],
					}, {
						data: [{
							headerless: true,
							children: (
								<ButtonBit
									title={'VIEW CLIENT\'S ORDER'}
									theme={ ButtonBit.TYPES.THEMES.SECONDARY }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									onPress={ this.props.onNavigateToOrder }
								/>
							),
							style: Styles.header,
						}, {
							blank: true,
						}],
					}]}
				/>
			)
		}
	}
)
