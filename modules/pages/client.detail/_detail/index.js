import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import PromiseStatefulModel from 'coeur/models/promise.stateful'

import TimeHelper from 'coeur/helpers/time';
import UtilitiesContext from 'coeur/contexts/utilities';

import UserService from 'app/services/user.new';
import CerveauService from 'app/services/cerveau'


import InputDatetimeBit from 'modules/bits/input.datetime';
import InputValidatedBit from 'modules/bits/input.validated';
import SelectionBit from 'modules/bits/selection';
// import TextInputBit from 'modules/bits/text.input';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';


import BoxRowLego from 'modules/legos/box.row';
import SelectStylistLego from 'modules/legos/select.stylist';

import AddressesComponent from 'modules/components/addresses'

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				image: PropTypes.object,
				firstName: PropTypes.string,
				lastName: PropTypes.string,
				email: PropTypes.string,
				phone: PropTypes.string,
				isVerified: PropTypes.bool,
				hasPayment: PropTypes.bool,
				loggedAt: PropTypes.date,
				createdAt: PropTypes.date,
				address: PropTypes.object,
				addresses: PropTypes.arrayOf(PropTypes.object),
				stylist: PropTypes.string,
				isManager: PropTypes.bool,
				onUpdate: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static propsToPromise(state, oP) {
			return CerveauService.getUserCluster(oP.id).catch(err => {
				console.warn(err)
			})

		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			addresses: [],
		}

		constructor(p) {
			super(p, {
				isUpdatingStylist: false,
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onChange = (key, val) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, val)
		}

		onConfirmChangeStylist = (key, val, reason) => {
			UserService.setStylist(this.props.id, key, reason, this.props.token).then(isUpdated => {
				if (isUpdated) {
					this.props.utilities.notification.show({
						title: 'Success',
						message: 'Stylist assigned.',
						type: 'SUCCESS',
					})

					this.props.onUpdateStylist &&
					this.props.onUpdateStylist(val)
				} else {
					throw new Error('Update false')
				}
			}).catch(err => {
				this.warn(err)

				this.props.utilities.notification.show({
					title: 'Oops',
					message: 'Something went wrong with your request, please try again later',
				})
			}).finally(() => {
				this.onModalRequestClose()
				this.setState({
					isUpdatingStylist: false,
				})
			})

		}

		onChangeStylist = (key, val) => {
			this.setState({
				isUpdatingStylist: true,
			}, () => {
				this.props.utilities.alert.modal({
					component: (
						<ModalPromptPagelet
							showInput={false}
							title={'handover or assign'}
							cancel={'assign'}
							confirm={'handover'}
							placeholder={'handover / assign'}
							onCancel={ this.onConfirmChangeStylist.bind(this, key, val, 'assign') }
							onConfirm={ this.onConfirmChangeStylist.bind(this, key, val, 'handover') }
							closeAfterSave={ false }
						/>
					),
				})
			})
		}

		view() {
			return (
				<React.Fragment>
					<BoxRowLego
						title={'General'}
						data={[{
							data: [{
								title: 'First Name',
								content: this.props.firstName,
								children: this.props.isManager ? (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.NAME }
										placeholder={ 'Input here…' }
										onChange={ this.onChange.bind(this, 'first_name') }
										value={ this.props.firstName }
									/>
								) : undefined,
							}, {
								title: 'Last Name',
								content: this.props.lastName,
								children: this.props.isManager ? (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.NAME }
										placeholder={ 'Input here…' }
										onChange={ this.onChange.bind(this, 'last_name') }
										value={ this.props.lastName }
									/>
								) : undefined,
							}],
						}, this.props.data && {
							data: [{
								title: 'Cluster Type',
								content: this.props.data.cluster_type,
								children: this.props.data ? (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.NAME }
										value={ this.props.data.cluster_type }
										readonly
									/>
								) : undefined,
							}, {
								title: 'Cluster No',
								content: this.props.data.cluster_no,
								children: this.props.data ? (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.NAME }
										value={ this.props.data.cluster_no }
										readonly
									/>
								) : undefined,
							}],
						}, {
							data: [{
								headerless: this.props.isManager,
								title: 'Email Address',
								content: this.props.email,
								children: this.props.isManager ? (
									<InputValidatedBit title="Email Address" type={InputValidatedBit.TYPES.EMAIL}
										onChange={this.onChange.bind(this, 'email')}
										value={this.props.email}
									/>
								) : undefined,
								style: this.props.isManager ? Styles.top : undefined,
							}, {
								headerless: this.props.isManager,
								title: 'Phone',
								content: '-',
								children: this.props.isManager ? (
									<InputValidatedBit title="Phone" type={InputValidatedBit.TYPES.PHONE}
										onChange={this.onChange.bind(this, 'phone')}
										value={this.props.phone}
									/>
								) : undefined,
								style: this.props.isManager ? Styles.top : undefined,
							}],
						}, {
							data: [{
								title: 'Is Email Verified',
								content: this.props.isVerified ? 'Yes' : 'No',
								children: this.props.isManager ? (
									<SelectionBit options={[{
										key: true,
										title: 'Yes',
										selected: this.props.isVerified === true,
									}, {
										key: false,
										title: 'No',
										selected: this.props.isVerified === false,
									}]} onChange={this.onChange.bind(this, 'is_verified')} />
								) : undefined,
							}, {
								// title: 'Has Payment',
								// content: this.props.hasPayment ? 'Yes' : 'No',
								// children: this.props.isManager ? (
								// 	<TextInputBit unflex={false} disabled
								// 		defaultValue={ this.props.hasPayment ? 'Yes' : 'No' }
								// 	/>
								// ) : undefined,
								title: 'Stylist',
								content: this.props.stylist,
								children: this.props.isManager ? (
									<SelectStylistLego stylist={ this.props.stylist } onChange={ this.onChangeStylist } />
								) : undefined,
							}],
						}, {
							data: [{
								headerless: this.props.isManager,
								title: 'Join Date',
								content: TimeHelper.format(this.props.createdAt, 'DD MMM YYYY HH:mm'),
								children: this.props.isManager ? (
									<InputDatetimeBit title="Join Date" readonly date={ this.props.createdAt } onChange={ this.onChange.bind(this, 'created_at') } />
								) : undefined,
								style: this.props.isManager ? Styles.date : undefined,
							}, {
								headerless: this.props.isManager,
								title: 'Last Login Date',
								content: TimeHelper.format(this.props.loggedAt, 'DD MMM YYYY HH:mm'),
								children: this.props.isManager ? (
									<InputDatetimeBit title="Last Login Date" readonly date={this.props.createdAt} onChange={this.onChange.bind(this, 'logged_at')} />
								) : undefined,
								style: this.props.isManager ? Styles.date : undefined,
							}],
						// eslint-disable-next-line no-nested-ternary
						}, this.props.address ? {
							children: (
								<AddressesComponent title="Main Address" data={ [this.props.address] } children={ this.props.addresses.length > 1 ? (
									<AddressesComponent title="Other Addresses" data={ this.props.addresses.filter(address => address.id !== this.props.address.id) } />
								) : undefined } />
							),
						} : this.props.addresses.length ? {
							children: (
								<AddressesComponent title="User Addresses" data={this.props.addresses} />
							),
						} : null]}
						style={ this.props.style }
					/>
				</React.Fragment>
			)
		}
	}
)
