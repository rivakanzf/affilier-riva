import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	image: {
		width: 120,
		height: 120,
		borderRadius: 60,
		marginTop: 16,
		overflow: 'hidden',
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},
	date: {
		marginTop: Sizes.margin.default,
	},
	top: {
		marginTop: Sizes.margin.default,
	},
})
