import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import UserService from 'app/services/user.new'

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import InputFileBit from 'modules/bits/input.file'
// import InputValidatedBit from 'modules/bits/input.validated';
import LoaderBit from 'modules/bits/loader';

import InitialLego from 'modules/legos/initial';

import Styles from './style';


export default ConnectHelper(
	class ImagePart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				firstName: PropTypes.string,
				lastName: PropTypes.string,
				profile: PropTypes.image,
				cover: PropTypes.image,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				uploading: {
					profile: false,
					cover: false,
				},
				profile: p.profile,
				cover: p.cover,
			})
		}

		onChange = (id, image) => {
			if (image === undefined && this.state[id] && this.state[id].id) {
				// removing
				this.setState({
					uploading: {
						...this.state.uploading,
						[id]: true,
					},
				}, () => {
					UserService.removeAsset(this.props.id, this.state[id].id, this.props.token).then(() => {
						this.setState({
							[id]: undefined,
							uploading: {
								...this.state.uploading,
								[id]: false,
							},
						})
					})
				})
			} else if(typeof image === 'string') {
				this.setState({
					uploading: {
						...this.state.uploading,
						[id]: true,
					},
				}, () => {
					UserService.upload(this.props.id, {
						type: id,
						image,
					}, this.props.token).then(asset => {
						this.setState({
							[id]: asset,
							uploading: {
								...this.state.uploading,
								[id]: false,
							},
						})
					})
				})
			}
		}

		view() {
			return (
				<BoxBit centering unflex style={[ Styles.container, this.props.style ]}>
					{ this.state.cover ? (
						<ImageBit source={ this.state.cover } resizeMode={ ImageBit.TYPES.COVER } style={[Styles.cover, Styles.border]} />
					) : false }
					<BoxBit style={ Styles.button }>
						<ButtonBit
							title="Change Cover"
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							theme={ ButtonBit.TYPES.THEMES.SECONDARY }
							size={ ButtonBit.TYPES.SIZES.MIDGET }
							state={ this.state.uploading.cover ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
						/>
						<InputFileBit
							id="cover"
							key={ this.state.uploading.cover }
							maxSize={3}
							onDataLoaded={ this.onChange }
							style={ [Styles.cover, Styles.hide] }
						/>
					</BoxBit>
					{ this.state.uploading.profile ? (
						<BoxBit centering unflex style={ Styles.profile }>
							<LoaderBit simple />
						</BoxBit>
					) : (
						<BoxBit centering unflex style={ Styles.profile }>
							{ this.state.profile ? (
								<ImageBit source={ this.state.profile } resizeMode={ ImageBit.TYPES.COVER } style={ Styles.cover } />
							) : (
								<InitialLego
									accessible={ false }
									size={ 52 }
									name={`${this.props.firstName} ${this.props.lastName}`}
									style={Styles.cover}
								/>
							) }
							<IconBit
								name="edit"
								color={ Colors.white.primary }
								style={ Styles.edit }
							/>
							<InputFileBit
								id="profile"
								key={this.state.uploading.profile}
								maxSize={3}
								onDataLoaded={this.onChange}
								style={[Styles.cover, Styles.hide]}
							/>
						</BoxBit>
					) }
				</BoxBit>
			)
		}
	}
)
