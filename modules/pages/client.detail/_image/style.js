import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		height: 280,
		backgroundColor: Colors.grey.palette(5),
	},
	profile: {
		width: 120,
		height: 120,
		borderRadius: 60,
		overflow: 'hidden',
		backgroundColor: Colors.grey.palette(2),
		boxShadow: `${ Colors.black.palette(2, .3) } 0px 1px 3px`,
	},
	border: {
		borderRadius: 2,
		overflow: 'hidden',
	},
	cover: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		width: 'auto',
		height: 'auto',
	},
	hide: {
		opacity: 0,
	},
	edit: {
		position: 'absolute',
		left: 48,
		top: 87,
	},
	button: {
		position: 'absolute',
		right: Sizes.margin.default,
		bottom: Sizes.margin.default,
	},
})
