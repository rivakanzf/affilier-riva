import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color'

import UserService from 'app/services/user.new'

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import BoxRowLego from 'modules/legos/box.row';
import SelectRoleLego from 'modules/legos/select.role';


import Styles from './style';

import { capitalize } from 'lodash';


export default ConnectHelper(
	class RolesPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				isManager: PropTypes.bool,
				roles: PropTypes.array,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			roles: [],
		}

		constructor(p) {
			super(p, {
				isUpdating: false,
				selectedRole: null,
				roles: p.roles,
			})
		}

		onChangeSelectedRole = role => {
			this.setState({
				selectedRole: role,
			})
		}

		onRemove = role => {
			this.setState({
				isUpdating: true,
			}, () => {
				UserService.removeRole(this.props.id, role, this.props.token).then(() => {
					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: 'Role removed from user.',
						type: 'SUCCESS',
					})

					this.setState({
						roles: this.state.roles.filter(r => r.role !== role),
						isUpdating: false,
					})
				}).catch(this.onError)
			})
		}

		onAdd = () => {
			this.setState({
				isUpdating: true,
			}, () => {
				UserService.addRole(this.props.id, this.state.selectedRole, this.props.token).then(() => {
					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: 'Role granted to user.',
						type: 'SUCCESS',
					})

					this.setState({
						roles: [...this.state.roles, { role: this.state.selectedRole }],
						isUpdating: false,
					})
				}).catch(this.onError)
			})
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isUpdating: false,
			})

			this.props.utilities.notification.show({
				title: 'Oops',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
			})
		}

		view() {
			return (
				<BoxRowLego
					title="Roles"
					data={[ ...this.state.roles.map((role, i) => {
						return {
							data: [{
								title: i === 0 ? 'Role' : '',
								headerless: i !== 0,
								style: i !== 0 ? Styles.row : undefined,
								children: (
									<TextInputBit key={ role.role } readonly defaultValue={ capitalize(role.role) } />
								),
							}, {
								title: i === 0 ? 'Granted On' : '',
								headerless: i !== 0,
								style: i !== 0 ? Styles.row : undefined,
								children: (
									<BoxBit row style={Styles.note}>
										<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
											{ TimeHelper.format(role.created_at, 'DD/MM/YYYY HH:mm') }
										</GeomanistBit>
										<TouchableBit accessible={ this.props.isManager && !this.state.isUpdating } unflex centering style={ Styles.icon } onPress={ this.onRemove.bind(this, role.role) }>
											<IconBit
												name="trash"
												color={ !this.props.isManager || this.state.isUpdating ? Colors.solid.grey.palette(5) : Colors.primary }
												size={ 24 }
											/>
										</TouchableBit>
									</BoxBit>
								),
							}],
						}
					}), this.state.roles.length ? null : {
						data: [{
							title: 'Role',
							content: '-',
						}, {
							title: 'Granted On',
							content: '-',
						}],
					}, this.props.isManager ? {
						data: [{
							children: (
								<SelectRoleLego isRequired placeholder="Select role" disabled={ this.state.isUpdating } key={this.state.roles.length} disabledRoles={this.state.roles.map(r => r.role)} onChange={ this.onChangeSelectedRole } />
							),
						}, {
							children: (
								<ButtonBit
									// weight="medium"
									title={ 'Add Role' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.state.isUpdating ? ButtonBit.TYPES.STATES.LOADING : this.state.selectedRole === null && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL }
									onPress={ this.onAdd }
								/>
							),
						}],
					} : null]}
					style={ this.props.style }
				/>
			)
		}
	}
)
