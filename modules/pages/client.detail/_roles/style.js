import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	note: {
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	icon: {
		width: 44,
		height: 44,
	},

	empty: {
		justifyContent: 'center',
		height: 72,
	},

	row: {
		marginTop: 8,
	},

})
