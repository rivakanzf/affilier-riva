import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import UserService from 'app/services/user.new'

import BoxBit from 'modules/bits/box';
import InputFileBit from 'modules/bits/input.file'
import InputValidatedBit from 'modules/bits/input.validated';
import LoaderBit from 'modules/bits/loader';

import BoxRowLego from 'modules/legos/box.row';

import Styles from './style';


export default ConnectHelper(
	class StylistPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				stageName: PropTypes.string,
				bio: PropTypes.string,
				signature: PropTypes.image,
				onUpdate: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isUploading: false,
				signature: p.signature,
			})
		}

		onChange = (key, val) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, val)
		}

		onChangeSignature = (id, image) => {
			if (image === undefined && this.state.signature && this.state.signature.id) {
				// removing
				this.setState({
					isUploading: true,
				}, () => {
					UserService.removeAsset(this.props.id, this.state.signature.id, this.props.token).then(() => {
						this.setState({
							signature: undefined,
							isUploading: false,
						})
					})
				})
			} else if(typeof image === 'string') {
				this.setState({
					isUploading: true,
				}, () => {
					UserService.upload(this.props.id, {
						type: 'signature',
						image,
					}, this.props.token).then(asset => {
						this.setState({
							signature: asset,
							isUploading: false,
						})
					})
				})
			}
		}

		view() {
			return (
				<React.Fragment>
					<BoxRowLego
						title={'Stylist Profile'}
						data={[{
							data: [{
								title: 'Nick Name',
								children: (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.NAME }
										placeholder={ 'Input here…' }
										onChange={ this.onChange.bind(this, 'stage_name') }
										value={ this.props.stageName }
									/>
								),
								description: 'Your clients will refer you as this instead of your real name.',
							}],
						}, {
							data: [{
								title: 'Biography',
								children: (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.TEXTAREA }
										placeholder={ 'Input here…' }
										onChange={ this.onChange.bind(this, 'bio') }
										value={ this.props.bio }
									/>
								),
							}],
						}, {
							data: [{
								title: 'Signature',
								children: this.state.isUploading ? (
									<BoxBit unflex style={Styles.image} centering>
										<LoaderBit simple />
									</BoxBit>
								) : (
									<InputFileBit
										key="unchanging"
										id="signature"
										maxSize={3}
										value={ this.state.signature }
										onDataLoaded={ this.onChangeSignature }
									/>
								),
							}],
						}]}
						style={ this.props.style }
					/>
				</React.Fragment>
			)
		}
	}
)
