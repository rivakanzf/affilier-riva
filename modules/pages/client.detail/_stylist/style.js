import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	image: {
		width: 100,
		height: 100,
		backgroundColor: Colors.grey.palette(2),
	},
})
