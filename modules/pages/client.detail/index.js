import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'

import UserService from 'app/services/user.new';

// import UserManager from 'app/managers/user';
// import OrderDetailManager from 'app/managers/order.detail';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';

import TabLego from 'modules/legos/tab';

import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import ImagesUserPagelet from 'modules/pagelets/images.user';
import UserAnswersPagelet from 'modules/pagelets/user.answers';
import UserNotePagelet from 'modules/pagelets/user.note';
import UserStylesheetPagelet from 'modules/pagelets/user.stylesheet';
import UserWalletPagelet from 'modules/pagelets/user.wallet';
import UserFeedbackLike from 'modules/pagelets/user.feedback.like'

import ActionsPart from './_actions'
import DetailPart from './_detail'
import ImagePart from './_image'
import RolesPart from './_roles'
import StylistPart from './_stylist'
// import StyleHistoryPart from 'modules/pages/user.detail/_style.history';
// import {
// 	StyleProfilePart,
// } from 'modules/pages/user.detail';

import Styles from './style';

export default ConnectHelper(
	class StyleSheetsDetailPage extends PageModel {

		static routeName = 'client.detail'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'superadmin'),
				isFulfillment: AuthenticationHelper.isAuthorized(state.me.roles, 'fulfillment'),
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				isChanged: false,
				isUpdating: false,
				tabIndex: p.tabIndex || 0,
				token: undefined,
				data: {
					addresses: [],
				},
				changes: {},
			})
		}

		roles = ['stylist', 'headstylist', 'analyst', 'finance', 'fulfillment', 'purchasing']

		tabs = [{
			title: 'General',
			icon: 'account',
			onPress: this.onChangeTab.bind(this, 0),
		}, {
			title: 'Style Profile',
			icon: 'style-profile',
			onPress: this.onChangeTab.bind(this, 1),
		}, {
			title: 'Style History',
			icon: 'style-sheet',
			onPress: this.onChangeTab.bind(this, 2),
		}, {
			title: 'Notes',
			icon: 'edit',
			onPress: this.onChangeTab.bind(this, 3),
		}, {
			title: 'Images',
			icon: 'images',
			onPress: this.onChangeTab.bind(this, 4),
		}, {
			title: 'Wallet',
			icon: 'wallet',
			onPress: this.onChangeTab.bind(this, 5),
		}, {
			title: 'User Like/Dislike',
			icon: 'style-profile',
			onPress: this.onChangeTab.bind(this, 6),
		}]

		onAuthorized = token => {
			this.setState({
				token,
				isLoading: true,
			}, () => {
				UserService.getUser(this.props.id, this.state.token).then(res => {
					this.setState({
						isLoading: false,
						data: res,
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			})
		}

		onNavigateToClients = () => {
			this.navigator.navigate('client')
		}

		onNavigateToOrder = () => {
			this.navigator.navigate('order', {
				search: this.state.data.email,
			})
		}

		onUpdate = (key, val) => {
			this.state.changes[key] = val

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onUpdateStylist = val => {
			this.state.data.stylist = val
		}

		onSaveChanges = () => {
			this.setState({
				isUpdating: true,
			}, () => {
				UserService.update(this.props.id, this.state.changes, this.props.token)
					.then(() => {
						this.utilities.notification.show({
							title: 'Yeay!',
							message: 'User data updated',
							type: 'SUCCESS',
						})

						this.setState({
							changes: {},
							isChanged: false,
							isUpdating: false,
						})
					}).catch(err => {
						this.warn(err)

						this.utilities.notification.show({
							title: 'Oops…',
							message: err.detail || err.message || 'Something went wrong.',
						})
					})
			})
		}

		contentRenderer = index => {
			switch (index) {
			case 0:
			default:
				return (
					<BoxBit unflex row>
						<BoxBit style={Styles.padder}>
							<ImagePart
								id={ this.props.id }
								firstName={ this.state.data.first_name }
								lastName={ this.state.data.last_name }
								profile={ this.state.data.image }
								cover={ this.state.data.cover }
							/>
							<DetailPart
								id={ this.props.id }
								image={ this.state.data.image }
								firstName={ this.state.data.first_name }
								lastName={ this.state.data.last_name }
								email={ this.state.data.email }
								phone={ this.state.data.phone }
								isVerified={ this.state.data.is_verified }
								hasPayment={ this.state.data.has_payment }
								loggedAt={ this.state.data.logged_at }
								createdAt={ this.state.data.created_at }
								address={ this.state.data.address }
								addresses={ this.state.data.addresses }
								stylist={ this.state.data.stylist }
								isManager={ this.props.isManager }
								onUpdate={ this.onUpdate }
								onUpdateStylist={ this.onUpdateStylist }
								style={ Styles.pad }
							/>
							{ this.state.data.roles.map(r => r.role).indexOf('stylist') > -1 ? (
								<StylistPart
									id={ this.props.id }
									stageName={ this.state.data.stage_name }
									bio={ this.state.data.bio }
									signature={ this.state.data.signature }
									onUpdate={ this.onUpdate }
									style={ Styles.pad }
								/>
							) : false }
						</BoxBit>
						<BoxBit>
							<ActionsPart
								id={ this.props.id }
								isManager={ this.props.isManager }
								loading={ this.state.isUpdating }
								disabled={ !this.state.isChanged }
								// spq={ this.state.data.spq }
								updatedAt={ this.state.data.spq_updated_at }
								onPress={ this.onSaveChanges }
								onNavigateToOrder={ this.onNavigateToOrder }
							/>
							<RolesPart
								id={ this.props.id }
								isManager={ this.props.isManager }
								roles={ this.state.data.roles }
								style={ Styles.pad }
							/>
						</BoxBit>
					</BoxBit>
				)
			case 1:
				return (
					<UserAnswersPagelet
						id={ this.props.id }
						date={ this.state.data.spq_updated_at } />
				)
			case 2:
				return (
					<UserStylesheetPagelet id={ this.props.id } />
				)
			case 3:
				return (
					<UserNotePagelet id={ this.props.id } />
				)
			case 4:
				return (
					<ImagesUserPagelet id={ this.props.id } />
				)
			case 5:
				return (
					<UserWalletPagelet id={ this.props.id } />
				)
			case 6:
				return (
					<UserFeedbackLike id={ this.props.id } />
				)
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={this.onAuthorized}
					paths={[{
						title: 'Clients',
						onPress: this.onNavigateToClients,
					}, {
						title: this.props.id || 'Client Detail',
					}]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title={ this.state.data.first_name ? `${this.state.data.first_name} ${this.state.data.last_name}` : 'Client Detail' }
							/>
							<TabLego activeIndex={this.state.tabIndex} tabs={this.tabs} style={Styles.tab} />
							{ this.state.isLoading ? (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							) : this.contentRenderer(this.state.tabIndex) }
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
