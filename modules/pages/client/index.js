import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';
import AuthenticationHelper from 'utils/helpers/authentication'

import UtilitiesContext from 'coeur/contexts/utilities';


import UserService from 'app/services/user.new';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import SelectionBit from 'modules/bits/selection';

import SelectRoleLego from 'modules/legos/select.role';
import SelectDateLego from 'modules/legos/select.date';
import SelectStylistLego from 'modules/legos/select.stylist';
import TableLego from 'modules/legos/table';

import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';


import Styles from './style';

import { isEqual } from 'lodash';


export default ConnectHelper(
	class ClientPage extends PageModel {

		static routeName = 'client'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist'),
				isAnalyst: AuthenticationHelper.isAuthorized(state.me.roles, 'analyst'),
				isFulfillment: AuthenticationHelper.isAuthorized(state.me.roles, 'fulfillment'),
				isMarketing: AuthenticationHelper.isAuthorized(state.me.roles, 'marketing'),
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				isUpdatingData: [],
				offset: 0,
				count: 160,
				total: 0,
				search: p.search || undefined,
				users: [],
				filter: {
					role: undefined,
					spq: '',
					order: '',
					stylist: '',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		roles = ['stylist', 'headstylist', 'analyst', 'fulfillment', 'purchasing', 'marketing']

		tableHeader = [{
			title: 'No.',
			width: .5,
		}, {
			title: 'User ID',
			width: .8,
		}, {
			title: 'Name',
			width: 2,
		}, {
			title: 'Email Address',
			width: 2.8,
		}, {
			title: 'Roles',
			width: 1,
		}, {
			title: 'Stylist',
			width: 1.5,
		}, {
			title: 'Join date',
			width: 1.1,
		}, {
			title: 'SPQ',
			width: .5,
			align: 'right',
		}]

		spqs = [{
			key: '',
			title: 'All SPQ status',
		}, {
			key: 'complete',
			title: 'Completed SPQ',
		}, {
			key: 'incomplete',
			title: 'Incomplete SPQ',
		}]

		orders = [{
			key: '',
			title: 'All order status',
		}, {
			key: true,
			title: 'Have order',
		}, {
			key: false,
			title: 'Doesn\'t have order',
		}]

		stylist = [{
			key: '',
			title: 'All stylist status',
		}, {
			key: true,
			title: 'Have stylist',
		}, {
			key: false,
			title: 'Doesn\'t have stylist',
		}]
		

		componentDidUpdate(pP, pS) {
			if (
				this.state.token && (
					!isEqual(pS.offset, this.state.offset)
					|| !isEqual(pS.search, this.state.search)
					|| !isEqual(pS.filter.role, this.state.filter.role)
					|| !isEqual(pS.filter.spq, this.state.filter.spq)
					|| !isEqual(pS.filter.order, this.state.filter.order)
					|| !isEqual(pS.filter.stylist, this.state.filter.stylist)
					|| !isEqual(pS.filter.date, this.state.filter.date)
				)
			) {
				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				UserService.getUsers({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(filter.role ? { role: filter.role } : {}),
					...(filter.spq !== '' ? { spq: filter.spq } : {}),
					...(filter.order !== '' ? { have_order: filter.order } : {}),
					...(filter.stylist !== '' ? { have_stylist: filter.stylist } : {}),
					...(filter.date !== '' ? { date: filter.date } : {}),
					...(this.props.isManager || this.props.isAnalyst || this.props.isFulfillment || this.props.isMarketing ? {} : { mine: true }),
				}, this.state.token).then(res => {
					if(id === this.getterId) {
						this.setState({
							isLoading: false,
							total: res.count,
							users: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			this.setState({
				offset: 0,
				filter: {
					...this.state.filter,
					[key]: val,
				},
			})
		}

		onConfirmChangeStylist = (user, key, val, reason) => {
			UserService.setStylist(user.id, key, reason, this.state.token).then(isUpdated => {
				if (isUpdated) {
					user.stylist = val

					this.utilities.notification.show({
						title: 'Success',
						message: `Stylist assigned to user #${user.id}`,
						type: 'SUCCESS',
					})
				} else {
					throw new Error('Update false')
				}
			}).catch(err => {
				this.warn(err)

				this.utilities.notification.show({
					title: 'Oops',
					message: 'Something went wrong with your request, please try again later',
				})
			}).finally(() => {
				this.setState({
					isUpdatingData: this.state.isUpdatingData.filter(id => id !== user.id),
				})
				this.onModalRequestClose()

			})
		}

		onChangeStylist = (user, key, val) => {
			this.setState({
				isUpdatingData: [...this.state.isUpdatingData, user.id],
			}, () => {
				this.props.utilities.alert.modal({
					component: (
						<ModalPromptPagelet
							showInput={false}
							title={'handover or assign'}
							cancel={'assign'}
							confirm={'handover'}
							placeholder={'handover / assign'}
							onCancel={ this.onConfirmChangeStylist.bind(this, user, key, val, 'assign') }
							onConfirm={ this.onConfirmChangeStylist.bind(this, user, key, val, 'handover') }
							closeAfterSave={ false }
						/>
					),
				})
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToDetail = id => {
			this.navigator.navigate(`client/${id}`)
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		rowRenderer = (user, i) => {
			return {
				data: [{
					title: i + 1,
				}, {
					title: `#US-${user.id}`,
				}, {
					title: user.name,
				}, {
					title: user.email,
				}, {
					title: user.roles || 'Client',
				}, {
					title: user.stylist,
					// eslint-disable-next-line no-nested-ternary
					children: this.props.isManager
						? this.state.isUpdatingData.indexOf(user.id) === -1
							? (
								<SelectStylistLego isRequired
									placeholder="-"
									stylist={ user.stylist }
									onChange={ this.onChangeStylist.bind(this, user) }
									style={ Styles.stylist }
								/>
							)
							: (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							)
						: undefined,
				}, {
					title: TimeHelper.format(user.created_at, 'DD MMM YYYY HH:mm'),
				}, {
					children: user.spq ? (
						<BoxBit style={Styles.right}>
							<IconBit
								name="circle-checkmark"
								color={ Colors.green.palette(1) }
								size={20}
							/>
						</BoxBit>
					) : (
						<BoxBit style={Styles.right}>
							<IconBit
								name="circle-null"
								color={Colors.solid.grey.palette(5)}
								size={20}
							/>
						</BoxBit>
					),
				}],
				onPress: this.onGoToDetail.bind(this, user.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Clients' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Clients"
								searchPlaceholder={ 'ID, Name, Email, or Stylist' }
								search={ this.state.search }
								onSearch={this.onSearch} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} >
								<SelectRoleLego
									role={ this.state.filter.role }
									onChange={ this.onChangeFilter.bind(this, 'role') } />
								<SelectionBit
									placeholder={ 'All SPQ status' }
									options={ this.spqs }
									onChange={ this.onChangeFilter.bind(this, 'spq') }
								/>
								<SelectionBit
									placeholder={ 'All order status' }
									options={ this.orders }
									onChange={ this.onChangeFilter.bind(this, 'order') }
								/>
								<SelectionBit
									placeholder={'All stylist status'}
									options={this.stylist}
									onChange={this.onChangeFilter.bind(this, 'stylist')}
								/>
								<SelectDateLego
									status={ this.state.filter.date }
									onChange={this.onChangeFilter.bind(this, 'date')}
								/>
							</HeaderFilterComponent>
							<TableLego isLoading={this.state.isLoading}
								headers={this.tableHeader}
								rows={this.state.users.map(this.rowRenderer)}
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
