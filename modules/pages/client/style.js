import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	padder: {
		paddingBottom: 16,
	},

	stylist: {
		backgroundColor: 'transparent',
		borderWidth: 0,
		paddingLeft: 0,
	},

	right: {
		alignItems: 'flex-end',
		justifyContent: 'center',
	},
})
