import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import StylecardService from 'app/services/stylecard';

import ButtonBit from 'modules/bits/button';


import BoxRowLego from 'modules/legos/box.row';

export default ConnectHelper(
	class ActionsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				clusterId: PropTypes.id,
			}
		}

		static stateToProps(state) {
			return {
				stylistId: state.me.id,
				me: state.me.fullName,
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isChanging: false,
			})
		}


		buttonState() {
			if (this.state.isChanging) {
				return ButtonBit.TYPES.STATES.LOADING
			} else if (this.props.disabled && this.props.status === this.state.status) {
				return ButtonBit.TYPES.STATES.DISABLED
			} else {
				return ButtonBit.TYPES.STATES.NORMAL
			}
		}

		onCreateStylecard = () => {
			this.setState({
				isChanging: true,
			})

			StylecardService.create({
				matchbox_id: 12,
				status: 'PENDING',
				stylist_id: this.props.stylistId,
				cluster_id: this.props.clusterId,
				is_public: true,
				is_recomendation: true,
				count: 5,
			}, this.props.token).then( () => {
				this.props.utilities.alert.show({
					title: 'Stylecard created',
					message: 'Berhasil membuat stylecard!',
					actions: [{
						title: 'Reload',
						onPress: () => {
							window.location.reload()
						},
					}],
				})
			}).catch(err => {
				this.warn(err)
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong with your request. Please try again.',
				})
				this.setState({
					isChanging: false,
				})
			})
		}
	
		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				timeout: 10000,
			})

			this.setState({
				isChanging: false,
			})
		}

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							children: (
								<ButtonBit
									weight="medium"
									title={ 'ADD STYLECARD' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									onPress={ this.onCreateStylecard }
									state={ this.buttonState() }
								/>
							),
						}],
					}]}
				/>
			)
		}
	}
)
