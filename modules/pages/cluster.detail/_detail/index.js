import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import LoaderBit from 'modules/bits/loader';


// import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';


import Styles from './style';


export default ConnectHelper(
	class DetailPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				isLoading: PropTypes.bool,
				id: PropTypes.id,
				
				clusterType: PropTypes.string,
				clusterNo: PropTypes.number,
				bodyCategory: PropTypes.string,
				looks: PropTypes.string,
				topFit: PropTypes.string,
				botFit: PropTypes.string,
				colorPalettes: PropTypes.string,
				styleSet: PropTypes.string,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isUpdating: false,
				isUploading: false,
				image: p.image,
				isLoaded: false,
			})
		}

		formatString = (str) => {
			return str.replaceAll(' - ', '\n')
		}

		viewOnLoading() {
			return (
				<BoxRowLego
					title={`Cluster-${ this.props.id }`}
					data={[{
						data: [{
							children: (
								<LoaderBit simple />
							),
						}],
					}]}
					contentContainerStyle={ Styles.padder }
				/>
			)
		}

		view() {
			return (
				<React.Fragment>
					<BoxRowLego
						title={`Cluster / ${ this.props.clusterType } - ${ this.props.clusterNo }`}
						contentContainerStyle={ Styles.padder }
					/>
					<BoxRowLego
						title={'Detail'}
						data={[ {
							data:[{
								title: 'Cluster Type',
								content: this.props.clusterType.replace('_', ' '),
							}, {
								title: 'Cluster No',
								content: this.props.clusterNo,
							}],
						}, {
							data: [{
								title: 'Body Category',
								content: this.props.bodyCategory,
							}, {
								title: 'Preferred Style',
								content: this.formatString(this.props.looks),
							}],
						}, {
							data: [{
								title: 'Preferred Top Fit',
								content: this.formatString(this.props.topFit),
							}, {
								title: 'Preferred Bottom Fit',
								content: this.formatString(this.props.botFit),
							}],
						}, {
							data: [{
								title: 'Preferred Color Palettes',
								content: this.formatString(this.props.colorPalettes),
							}, {
								title: 'Preferred Set',
								content: this.props.styleSet,
							}],
						}, {
							data: [{
								title: 'Cover',
								content: this.props.cover,
							}]
						}]
						}
						contentContainerStyle={ Styles.padder }
						style={ Styles.marginTop }
					/>
				</React.Fragment>
			)
		}
	}
)
