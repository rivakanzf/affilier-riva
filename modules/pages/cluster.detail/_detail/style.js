import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	padder: {
		marginBottom: 16,
		paddingBottom: 0,
	},
	marginTop: {
		marginTop: 16,
	},
	note: {
		color: Colors.black.palette(2, .6),
	},
	notes: {
		// backgroundColor: Colors.grey.palette(1, .04),
		// paddingLeft: Sizes.margin.default,
		// paddingRight: Sizes.margin.default,
		// paddingTop: 12,
		// paddingBottom: 12,
		minHeight: 88,
	},
	image: {
		width: '100%',
		height: 240,
	},
})
