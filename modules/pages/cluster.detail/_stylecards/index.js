import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box'
import LoaderBit from 'modules/bits/loader'
import RadioBit from 'modules/bits/radio'

import SummaryPart from '../_summary'

import Styles from './style';

import StylecardService from 'app/services/stylecard'

export default ConnectHelper(
	class StylecardsPart extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				displayRadio: PropTypes.bool,
				isActive: PropTypes.bool,
				onSelectStylecard: PropTypes.func,
			}
		}

		static propsToPromise(state, oP) {
			return StylecardService.getStylecardDetailed(oP.id, state.me.token)
		}

		static getDerivedStateFromProps(nP) {
			return {
				stylecards: {
					...nP.data,
				},
			}
		}

		onSelectStylecard = () => {
			this.props.onSelectStylecard &&
			this.props.displayRadio &&
			this.props.onSelectStylecard()
		}

		radioRenderer = () => {
			return (
				<BoxBit style={Styles.radioContainer}>
					<RadioBit
						dumb
						isActive={ this.props.isActive }
						style={Styles.radio}
						onPress={ this.onSelectStylecard }
					/>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit simple/>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit>
					{ this.props.displayRadio && this.radioRenderer() }
					
					<SummaryPart stylecards={[this.state.stylecards]} />
				</BoxBit>
			)
		}
	}
)
