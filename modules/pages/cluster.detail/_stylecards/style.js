import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	radioContainer: {
		marginTop: 24,
		marginLeft: 12,
	},
})
