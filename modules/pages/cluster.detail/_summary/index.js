import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import StylecardSummaryComponent from 'modules/components/stylecard.summary';

// import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';

import Styles from './style';

import BoxBit from 'modules/bits/box';
import style from '../_detail/style';

export default ConnectHelper(
	class SummaryPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				stylecards: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					title: PropTypes.string,
					inventories: PropTypes.array,
					stylistNote: PropTypes.string,
					count: PropTypes.number,
					status: PropTypes.string,
					image: PropTypes.image,
				})),
				style: PropTypes.style,
			}
		}

		contentRenderer = (stylecard, i) => {
			return (
				<StylecardSummaryComponent
					key={ i }
					index={ i }
					id={ stylecard.id }
					title={ stylecard.title }
					featuredImage={ stylecard.image }
					variants={ stylecard.variants }
					stylistNote={ stylecard.stylist_note }
					count={ stylecard.count }
					value={ stylecard.value }
					minValue={ stylecard.min_value }
					realValue={ stylecard.real_value }
					status={ stylecard.status }
					{ ...(stylecard.status !== 'PUBLISHED' ? {} : {
						publishedAt: stylecard.updated_at,
					})}
					showLink
					isRecommendation
				/>
			)
		}

		view() {
			return (
				<BoxBit>
					{ this.props.stylecards.map(this.contentRenderer) }
				</BoxBit>
			)
		}
	}
)
