import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'
import CommonHelper from 'coeur/helpers/common';

import ClusterService from 'app/services/cluster';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
import ButtonBit from 'modules/bits/button'

import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import ActionsPart from './_actions'
import DetailPart from './_detail'
import StylecardsPart from './_stylecards'

import BoxRowLego from 'modules/legos/box.row';
import TabLego from 'modules/legos/tab';

import Styles from './style';

import { sortBy } from 'lodash';

export default ConnectHelper(
	class StylecardDetailPage extends PageModel {

		static routeName = 'cluster.detail'

		static stateToProps(state) {
			return {
				isHeadOrStylist: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist', 'stylist'),
				token: state.me.token,
			}
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
			}
		}

		constructor(p) {
			const query = CommonHelper.getQueryString()

			super(p, {
				isPublishing: false,
				isLoading: true,
				isChanged: false,
				tabIndex: query.tab || p.tabIndex || 0,
				token: undefined,
				data: {},
				changes: {},
				selectedStylecard: [],
				offset: 0,
				limit: 10,
				firstLoad: true,
				isFetchedAll: false,
			})
		}
		
		filter = ['PENDING', 'STYLING', 'PUBLISHED']
		roles = ['headstylist', 'stylist', 'analyst', 'fulfillment']

		tabs = [{
			title: 'General',
			onPress: this.onChangeTab.bind(this, 0),
		}]

		getData = token => {
			this.setState({
				token,
				isLoading: this.state.firstLoad ? true : false,
				isFetchingMore: true,
			}, () => {
				ClusterService.getClusterDetail(this.props.id, {offset: this.state.offset, limit: this.state.limit}, this.state.token).then(res => {
					if(this.state.firstLoad) {
						this.setState({
							isLoading: false,
							isFetchingMore: false,
							data: res,
							firstLoad: false,
						})
					} else {
						this.setState({
							isLoading: false,
							isFetchingMore: false,
							data: {
								...this.state.data,
								stylecards: this.state.data.stylecards.concat(res.stylecards),
							},
						})
					}
				}).catch(err => {
					if(err.code === 'ERR_101') {
						this.setState({
							isFetchedAll: true,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops',
								message: 'tidak ada stylecard lagi',
							})
						})
					} else {
						this.warn(err)
	
						this.setState({
							isLoading: false,
							isFetchingMore: false,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops',
								message: 'Something went wrong with your request, please try again later',
							})
						})
					}
				})
			})
		}

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			}, () => {
				window.history.replaceState({}, 'ClusterDetail', `/cluster/${this.props.id}?tab=${this.state.tabIndex}`)

			})
		}

		onNavigateToClusters = () => {
			this.navigator.navigate('cluster')
		}

		onSelectStylecard = (id) => {
			if(this.state.selectedStylecard.indexOf(id) === -1) {
				this.setState({
					selectedStylecard: [...this.state.selectedStylecard, id],
				})
			} else {
				this.setState({
					selectedStylecard: this.state.selectedStylecard.filter(sc => sc !== id),
				})

			}
		}

		onPublishSelected = () => {
			this.setState({
				isPublishing: true,
			})

			ClusterService.publishMultiple(parseInt(this.props.id, 10), this.state.selectedStylecard, this.state.token).then(() => {
				this.setState({
					isPublishing:  false,
				})
				window.location.reload()
			}).catch((err) => {
				this.setState({
					isPublishing: false,
				})
				this.utilities.notification.show({
					title: 'Oops',
					message: err.detail,
				})
			})
		}

		onFetchMore = () => {
			this.setState({
				isFetchingMore: true,
				offset: this.state.offset + 10,
			}, () => {
				this.getData(this.props.token)
			})
		}


		stylecardsRenderer = (stylecard, i) => {
			const filter = this.filter[this.state.tabIndex]
			if( stylecard.status === filter) {
				return (
					<StylecardsPart
						key={i}
						id={ stylecard.id }
						displayRadio={ filter === 'STYLING' }
						isActive={ this.state.selectedStylecard.indexOf(stylecard.id) !== -1 }
						onSelectStylecard={ this.onSelectStylecard.bind(this, stylecard.id)}
					/>
				)
			}
			return false
		}

		publishButtonRenderer = () => {
			return (
				<ButtonBit
					type={ ButtonBit.TYPES.SHAPES.PROGRESS }
					title={ 'Publish Selected' }
					style={Styles.publishButton}
					weight={ 'medium' }
					onPress={ this.onPublishSelected }
					state={ this.state.isPublishing ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }

				/>
			)
		}

		contentRenderer = index => {
			switch (index) {
			case 0:
			default:
				return (
					<BoxBit unflex row>
						<BoxBit style={ Styles.padder }>
							<DetailPart
								isLoading={ this.state.isLoading }
								id={ this.props.id }

								clusterType={ this.state.data.type }
								clusterNo={ this.state.data.type_no }
								bodyCategory={ this.state.data.body_category }
								looks={ this.state.data.looks }
								topFit={ this.state.data.top_fit }
								botFit={ this.state.data.bottom_fit }
								colorPalettes={ this.state.data.color_palettes }
								styleSet={ this.state.data.style_set }
								cover={ this.state.data.cover }
							/>
						</BoxBit>
						<BoxBit>
							<ActionsPart
								clusterId={ this.props.id }
							/>
							<BoxRowLego
								title={'Style Cards'}
							>
								<TabLego
									capsule
									activeIndex={ this.state.tabIndex }
									tabs={ this.filter.map((tab, i) => {
										return {
											title: `${tab} (${this.state.data.stylecards.filter(sc => sc.status === tab).length})`,
											onPress: this.onChangeTab.bind(this, i),
										}
									})}
									style={Styles.stylecardsFilter}
								/>
								{/* { this.stylecardsFilterRenderer() } */}
								<BoxBit>
									{
										this.state.selectedStylecard.length > 0
										&& this.filter[this.state.tabIndex] === 'STYLING'
										&& this.publishButtonRenderer()
									}

									{ sortBy(this.state.data.stylecards, ['desc']).map(this.stylecardsRenderer) }
								</BoxBit>
								
								{
									!this.state.isFetchedAll &&
									<ButtonBit
										title={'Load More'}
										style={Styles.fetchButton}
										type={ ButtonBit.TYPES.SHAPES.PROGRESS }
										theme={ ButtonBit.TYPES.THEMES.SECONDARY }
										state={ this.state.isFetchingMore ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
										onPress={ this.onFetchMore }
									/>
								}
								
							</BoxRowLego>
						</BoxBit>
					</BoxBit>
				)
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.getData }
					paths={[{
						title: 'Cluster',
						onPress: this.onNavigateToClusters,
					}, {
						title: this.props.id || 'Cluster Detail',
					}]}
				>
					{() => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent title="Cluster Detail"/>
							{/* <TabLego activeIndex={ this.state.tabIndex } tabs={ this.tabs } style={ Styles.tab } /> */}
							{ this.state.isLoading ? (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							) : this.contentRenderer(this.state.tabIndex) }
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
