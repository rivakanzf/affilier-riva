import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	tab: {
		marginBottom: Sizes.margin.default,
	},

	padder: {
		marginRight: Sizes.margin.default,
	},

	pad: {
		marginTop: Sizes.margin.default,
	},

	stylecardsFilter: {
		marginTop: 12,
	},

	filterText: {
		marginLeft: 12,
		marginRight: 12,
	},

	publishButton: {
		marginTop: 12,
	},

	fetchButton: {
		marginTop: 12,
	}

})
