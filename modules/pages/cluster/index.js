import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'

import ClusterService from 'app/services/cluster';


import BoxBit from 'modules/bits/box';

import TableLego from 'modules/legos/table';

import HeaderPageComponent from 'modules/components/header.page'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import Styles from './style';

import { orderBy } from 'lodash';


export default ConnectHelper(
	class StylecardPage extends PageModel {

		static routeName = 'cluster'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist', 'stylist'),
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				
				offset: 0,
				count: 160,
				total: 0,
				clusters: [],
				
				token: undefined,
			})

			this.getterId = 1
		}

		tableHeader = [{
			title: 'Type',
			width: 1,
		}, {
			title: 'No',
			width: 0.5,
		}, {
			title: 'Body Type',
			width: 1.5,
		}, {
			title: 'Preferred Style',
			width: 2.5,
		}, {
			title: 'Preferred Top Fit',
			width: 2.5,
		}, {
			title: 'Preferred Bottom Fit',
			width: 2.5,
		}, {
			title: 'Preferred Color Palettes',
			width: 2.5,
		}]

		formatString = (str) => {
			return str.replaceAll(' - ', '\n')
		}

		onAuthorized = token => {
			this.setState({
				token,
			},
			this.getData
			)
		}

		getData = () => {
			this.getterId++;

			this.setState({
				isLoading: true,
			}, () => {
				ClusterService.getClusters(this.state.token).then(res => {

					this.setState({
						isLoading: false,
						count: res.count,
						clusters: orderBy(res.data, ['type', 'type_no']),
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			this.setState({
				offset: 0,
				filter: {
					...this.state.filter,
					[key]: val,
				},
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToDetail = id => {
			this.navigator.navigate(`cluster/${id}`)
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		rowRenderer = cluster => {
			return {
				data: [{
					title: cluster.type.replace('_', ' '),
				}, {
					title: cluster.type_no,
				}, {
					title: cluster.body_category,
				}, {
					title: this.formatString(cluster.looks),
				}, {
					title: this.formatString(cluster.top_fit),
				}, {
					title: this.formatString(cluster.bottom_fit),
				}, {
					title: this.formatString(cluster.color_palettes),
				}],
				onPress: this.onGoToDetail.bind(this, cluster.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={['stylist', 'headstylist', 'fulfillment', 'analyst']}
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Cluster' }]}
				>
					{() => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent
								title="Cluster"
								// searchPlaceholder={ 'ID or Stylist' }
								// search={ this.state.search }
								// onSearch={ this.onSearch }
								// buttonPlaceholder={ 'Create Style Card' }
								// onPress={ this.onCreateStylecard }
							/>
							{/* <HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								<SelectStatusLego emptyTitle="All Status" type={ SelectStatusLego.TYPES.STYLESHEET_STATUSES } status={ this.state.filter.status } onChange={ this.onChangeFilter.bind(this, 'status') } />
								<BoxBit style={ Styles.center }>
									<InputCheckboxBit title="Prestyled" description="Display have collection only" value={ this.state.filter.collection } onChange={ this.onChangeFilter.bind(this, 'collection') } />
								</BoxBit>
								<BoxBit style={ Styles.center }>
									<InputCheckboxBit title="Public" description="Display public style cards only" value={ this.state.filter.public } onChange={ this.onChangeFilter.bind(this, 'public') } />
								</BoxBit>
								<BoxBit style={ Styles.center }>
									<InputCheckboxBit title="Master" description="Display master style cards only" value={ this.state.filter.master } onChange={ this.onChangeFilter.bind(this, 'master') } />
								</BoxBit>
								<BoxBit style={ Styles.center }>
									<InputCheckboxBit title="Weekly Recommendation" description="Display weekly recommendation only" value={ this.state.filter.is_recomendation } onChange={ this.onChangeFilter.bind(this, 'is_recomendation') } />
								</BoxBit>
							</HeaderFilterComponent> */}
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.clusters.map(this.rowRenderer) }
								style={ Styles.padder }
							/>
							{/* <HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange } /> */}
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
