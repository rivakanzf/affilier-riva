import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import ButtonBit from 'modules/bits/button';
import InputDateTimeBit from 'modules/bits/input.datetime';

import BoxRowLego from 'modules/legos/box.row';

export default ConnectHelper(
	class ActionPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				loading: PropTypes.bool,
				onChangeDate: PropTypes.func,
				isChanged: PropTypes.bool,
				metadata: PropTypes.object,
				onSave: PropTypes.func,
				style: PropTypes.style,
			}
		}

		onChange = (key, value) => {
			this.props.onChangeDate &&
			this.props.onChangeDate(key, value)
		}

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ 'Save Collection' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.props.loading
										? ButtonBit.TYPES.STATES.LOADING
										: this.props.disabled || !this.props.isChanged
											&& ButtonBit.TYPES.STATES.DISABLED
											|| ButtonBit.TYPES.STATES.NORMAL
									}
									onPress={ this.props.onSave }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Publish Date',
							children: (
								<InputDateTimeBit unflex={ false }
									title={ '' }
									date={ this.props.metadata.date.publish_date }
									onChange={ this.onChange.bind(this, 'publish_date') }
								/>
							),
						}, {
							title: 'End Date',
							children: (
								<InputDateTimeBit unflex={ false }
									title={ '' }
									date={ this.props.metadata.date.end_date }
									onChange={ this.onChange.bind(this, 'end_date') }
								/>
							),
						}],
					}]}
					style={ this.props.style }
				/>
			)
		}
	}
)
