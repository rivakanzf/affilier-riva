import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import InputValidatedBit from 'modules/bits/input.validated';
import InputFileBit from 'modules/bits/input.file';
import LoaderBit from 'modules/bits/loader'
import InputDateTimeBit from 'modules/bits/input.datetime';
import ButtonBit from 'modules/bits/button';
import BoxBit from 'modules/bits/box'
import TextBit from 'modules/bits/text';

import BoxRowLego from 'modules/legos/box.row'

import UserService from 'app/services/user.new'

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends PromiseStatefulModel {

		static stateToProps(state) {
			return {
				token: state.me.token,
				id: state.me.id,
			}
		}

		static propTypes(PropTypes) {
			return {
				// readonly: PropTypes.bool,
				collectionId: PropTypes.id,
				title: PropTypes.string,
				slug: PropTypes.string,
				products: PropTypes.string,
				coupon_id: PropTypes.number,
				onChange: PropTypes.func,
				usePromo: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				isUploading: {
					banner: false,
					banner_mobile: false,
				},
				banner: p.banner,
				banner_mobile: p.banner_mobile,
			})
		}

		onChange = (key, value) => {
			this.props.onChange &&
			this.props.onChange(key, value)
		}

		onDataLoaded = (id, image) => {
			if(id === 'loader' && image === undefined) {
				this.setState({banner: undefined}, () => {
					this.onImageRemoved('banner')
				})
			} else if(id === 'adder') {
				this.setState({
					isUploading: {
						banner: true,
					},
				}, () => {
					UserService.upload(this.props.id, {
						image,
					}, this.props.token).then(res => {
	
						this.props.onChange &&
						this.props.onChange('banner', res.url)
	
						this.setState({
							banner: res.url,
							isUploading: {
								banner: false,
							},
						})
					})
				})
			}
		}

		onDataLoadedMobile = (id, image) => {
			if(id === 'loader' && image === undefined) {
				this.setState({banner_mobile: undefined}, () => {
					this.onImageRemoved('banner_mobile')
				})
			} else if(id === 'adder') {
				this.setState({
					isUploading: {
						banner_mobile: true,
					},
				}, () => {
					UserService.upload(this.props.id, {
						image,
					}, this.props.token).then(res => {
	
						this.props.onChange &&
						this.props.onChange('banner_mobile', res.url)
	
						this.setState({
							banner_mobile: res.url,
							isUploading: {
								banner_mobile: false,
							},
						})
					})
				})
			}
		}

		onImageRemoved = (type) => {
			if (type === 'banner') {
				this.state.banner = undefined
			} else {
				this.state.banner_mobile = undefined
			}

			if (this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}
		}

		view() {
			return (
				<BoxRowLego
					title={ this.props.collectionId ? `Collection #${this.props.collectionId}` : 'Detail'}
					data={[{
						data: [{
							title: 'Slug',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here"
									value={ this.props.slug }
									onChange={ this.onChange.bind(this, 'slug') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Collection Name',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here"
									value={ this.props.title }
									onChange={ this.onChange.bind(this, 'title') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Top Banner (Desktop)',
							children: this.state.isUploading.banner ? <LoaderBit/> : (
								<InputFileBit
									id={this.state.banner ? 'loader' : 'adder'}
									maxSize={3}
									value={ this.state.banner && `//${this.state.banner}` }
									onDataLoaded={this.onDataLoaded}
								/>
							),
							description: 'Image ratio 32:9. Recommended size is 1280x360 (px).',
						}, {
							title: 'Top Banner (Mobile)',
							children: this.state.isUploading.banner_mobile ? <LoaderBit/> : (
								<InputFileBit
									id={this.state.banner_mobile ? 'loader' : 'adder'}
									maxSize={3}
									value={ this.state.banner_mobile && `//${this.state.banner_mobile}` }
									onDataLoaded={this.onDataLoadedMobile}
								/>
							),
							description: 'Image ratio 16:9. Recommended size is 1280x720 (px).',
						}],
					}]}
					style={ this.props.style }
				/>
			)
		}
	}
)
