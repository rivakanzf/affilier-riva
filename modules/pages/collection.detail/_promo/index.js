import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import InputValidatedBit from 'modules/bits/input.validated';
import SwitchBit from 'modules/bits/switch'

import BoxRowLego from 'modules/legos/box.row'

import Styles from './style';


export default ConnectHelper(
	class PromoPart extends PromiseStatefulModel {

		static stateToProps(state) {
			return {
				token: state.me.token,
				id: state.me.id,
			}
		}

		static propTypes(PropTypes) {
			return {
				collectionId: PropTypes.id,
				title: PropTypes.string,
				caption: PropTypes.string,
				onChangePromo: PropTypes.func,
				onSwitch: PropTypes.func,
				usePromo: PropTypes.bool,
				metadata: PropTypes.object,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				// usePromo: false,
			})
		}

		onChange = (key, value) => {
			// console.log(key, value)
			this.props.onChangePromo &&
			this.props.onChangePromo(key, value)
		}

		view() {
			return (
				<BoxRowLego
					title="Promo Box"
					useSwitch
					switchValue={ this.props.usePromo }
					switchOnChange={ this.props.onSwitch }
					data={[this.props.usePromo ? {
						data: [{
							title: 'Title',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here"
									value={ this.props.metadata.promo.title }
									onChange={ this.onChange.bind(this, 'title') }
								/>
							),
						}],
					} : false, this.props.usePromo ? {
						data: [{
							title: 'Caption',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here"
									value={ this.props.metadata.promo.caption }
									onChange={ this.onChange.bind(this, 'caption') }
								/>
							),
						}],
					} : false]}
					style={ this.props.style }
				/>
			)
		}
	}
)
