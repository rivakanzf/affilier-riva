import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import Colors from 'coeur/constants/color';
import UtilitiesContext from 'coeur/contexts/utilities'

import InputDateTimeBit from 'modules/bits/input.datetime';
import TouchableBit from 'modules/bits/touchable';
import IconBit from 'modules/bits/icon';
import ButtonBit from 'modules/bits/button';
import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'

import BoxRowLego from 'modules/legos/box.row'

import CollectionService from 'app/services/collection';

import Styles from './style';

export default ConnectHelper(
	class SchedulePart extends PromiseStatefulModel {

		static stateToProps(state) {
			return {
				token: state.me.token,
				id: state.me.id,
			}
		}

		static propTypes(PropTypes) {
			return {
				collectionId: PropTypes.id,
				schedules: PropTypes.array,
				onChange: PropTypes.func,
				onSwitchSchedule: PropTypes.func,
				useSchedule: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static contexts = [ UtilitiesContext ]

		// props
		constructor(p) {
			super(p, {
				schedules: p.schedules.sort((a, b) => new Date(a.start_date) - new Date(b.start_date)) || [],
			})
		}

		addMoreSchedule = () => {
			const newArr = this.state.schedules.concat({
				start_date: '',
				end_date: '',
			})
			this.setState({
				schedules: newArr,
			})
		}

		updateSchedule = () => {
			CollectionService.updateSchedule(this.props.collectionId, this.state.schedules, this.props.token)
				.then(() => {
					this.props.utilities.notification.show({
						title: 'Yeay…',
						message: 'Schedules saved.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})
				})
				.catch(err => {
					console.log(err)
				})
		}

		onChangeSchedule = (date, i, type) => {
			const tempSchedules = this.state.schedules
			if (type === 'start') {
				tempSchedules[i].start_date = date
				this.setState({
					schedules: tempSchedules,
				})
			} else {
				tempSchedules[i].end_date = date
				this.setState({
					schedules: tempSchedules,
				})
			}
			
		}

		removeSchedule = i => {
			const newArr = this.state.schedules.filter((sch, idx) => idx !== i)
			this.setState({
				schedules: newArr,
			})
		}

		schedulesRenderer = (sch, i) => {
			return(
				<BoxBit row key={i}>
					<BoxBit style={ Styles.schedule }>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>Start Date</GeomanistBit>
						<InputDateTimeBit unflex={ false }
							title={ '' }
							date={ sch.start_date }
							onChange={ (date) => this.onChangeSchedule(date, i, 'start')}
						/>
					</BoxBit>
					<BoxBit>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>End Date</GeomanistBit>
						<InputDateTimeBit unflex={ false }
							title={ '' }
							date={ sch.end_date }
							onChange={ (date) => this.onChangeSchedule(date, i)}
						/>
					</BoxBit>
					<TouchableBit unflex centering onPress={ this.removeSchedule.bind(this, i) } style={ Styles.trash }>
						<IconBit
							name="close-fat"
							size={ 20 }
							color={ Colors.red.palette(3) }
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		onChange = (key, value) => {
			this.props.onChange &&
			this.props.onChange(key, value)
		}

		view() {
			return (
				<BoxRowLego
					title="Schedule"
					useSwitch
					switchValue={ this.props.useSchedule }
					switchOnChange={ this.props.onSwitchSchedule }
					data={this.props.useSchedule ? ([{
						data: [{
							key: this.state.schedules.map(s => s.id).join('-'),
							children: this.state.schedules.map(this.schedulesRenderer),
						}],
					}, {
						data: [{
							children: (
								<BoxBit row>
									<BoxBit style={ Styles.schedule }>
										<ButtonBit
											style={ Styles.scheduleButton }
											title={'Add more schedule'}
											type={ButtonBit.TYPES.SHAPES.PROGRESS}
											size={ButtonBit.TYPES.SIZES.SMALL}
											weight="medium"
											onPress={ this.addMoreSchedule }
										/>
									</BoxBit>
									<BoxBit style={ Styles.schedule }>
										<ButtonBit
											style={ Styles.scheduleButton }
											title={'Save Schedules'}
											type={ButtonBit.TYPES.SHAPES.PROGRESS}
											size={ButtonBit.TYPES.SIZES.SMALL}
											weight="medium"
											onPress={ this.updateSchedule }
										/>
									</BoxBit>
								</BoxBit>
							),
						}],
					}]) : []}
					style={ this.props.style }
				/>
			)
		}
	}
)
