import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	scheduleButton: {
		marginTop: Sizes.margin.default,
	},

	schedule: {
		marginRight: Sizes.margin.default,
	},

	scheduleBox: {
		marginBottom: Sizes.margin.default,
	},

	trash: {
		alignItems: 'center',
	},
})
