import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import InputValidatedBit from 'modules/bits/input.validated';
import InputFileBit from 'modules/bits/input.file';
import LoaderBit from 'modules/bits/loader'
import SwitchBit from 'modules/bits/switch'

import BoxRowLego from 'modules/legos/box.row'

import UserService from 'app/services/user.new'

import Styles from './style';


export default ConnectHelper(
	class ShowPart extends PromiseStatefulModel {

		static stateToProps(state) {
			return {
				token: state.me.token,
				id: state.me.id,
			}
		}

		static propTypes(PropTypes) {
			return {
				collectionId: PropTypes.id,
				metadata: PropTypes.object,
				subbanner: PropTypes.string,
				onChange: PropTypes.func,
				onChangeCta: PropTypes.func,
				onSwitchShow: PropTypes.func,
				showOnPage: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				isUploading: {
					subbanner: false,
				},
				subbanner: p.subbanner,
			})
		}

		onChange = (key, value) => {
			// console.log(key, value)
			this.props.onChange &&
			this.props.onChange(key, value)
		}

		onChangeCta = (key, value) => {
			this.props.onChangeCta &&
			this.props.onChangeCta(key, value)
		}

		onDataLoadedCover = (id, image) => {
			if(id === 'loader' && image === undefined) {
				this.setState({subbanner: undefined}, () => {
					this.onImageRemoved()
				})
			} else if(id === 'adder') {
				this.setState({
					isUploading: {
						subbanner: true,
					},
				}, () => {
					UserService.upload(this.props.id, {
						image,
					}, this.props.token).then(res => {
	
						this.props.onChange &&
						this.props.onChange('subbanner', res.url)
	
						this.setState({
							subbanner: res.url,
							isUploading: {
								subbanner: false,
							},
						})
					})
				})
			}
		}

		onImageRemoved = () => {
			this.state.subbanner = undefined

			if (this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}
		}

		view() {
			return (
				<BoxRowLego
					title="Show On Page"
					useSwitch
					switchValue={ this.props.showOnPage }
					switchOnChange={ this.props.onSwitchShow }
					data={[this.props.showOnPage ? {
						data: [{
							title: 'Button Label',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here"
									value={ this.props.metadata.cta.button_label }
									onChange={ this.onChangeCta.bind(this, 'button_label') }
								/>
							),
						}, this.props.showOnPage ? {
							title: 'Cover Banner',
							children: this.state.isUploading.subbanner ? <LoaderBit/> : (
								<InputFileBit
									id={this.state.subbanner ? 'loader' : 'adder'}
									maxSize={3}
									value={ this.state.subbanner && `//${this.state.subbanner}` }
									onDataLoaded={this.onDataLoadedCover}
								/>
							),
							description: 'Image ratio 4:3. Recommended size is 1280x960 (px).',
						} : false],
					} : false]}
					style={ this.props.style }
				/>
			)
		}
	}
)
