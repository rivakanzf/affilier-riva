/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import CollectionService from 'app/services/collection';

import BoxBit from 'modules/bits/box';

import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import CollectionItemsPagelet from 'modules/pagelets/collection.items';

import ActionPart from './_action';
import DetailPart from './_detail';
import PromoPart from './_promo';
import ShowPart from './_show';
import SchedulePart from './_schedule'

import Styles from './style';

import { isEmpty } from 'lodash'

export default ConnectHelper(
	class CollectionDetailPage extends PageModel {

		static routeName = 'collection.detail'

		constructor(p) {
			super(p, {
				isLoading: true,
				isUploading: false,
				uploader: 1,
				isChanged: false,
				token: undefined,
				data: {
					metadata: {},
				},
				changes: {},
				usePromo: false,
				showOnPage: false,
				useSchedule: false,
			}, [])

			this.metadata = {
				cta: {},
				promo: {},
				switch: {},
				date: {},
			}
		}

		isNumeric(num) {
			return /^-{0,1}\d+$/.test(num);
		}

		onNavigateToCollection = () => {
			this.navigator.navigate('collection')
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData = () => {
			if(this.props.id) {
				this.setState({
					isLoading: true,
				}, () => {
					CollectionService.getDetail(this.props.id, {}, this.state.token).then(res => {
						this.setState({
							data: res,
							usePromo: res.use_promo,
							showOnPage: res.show_on_page,
							useSchedule: res.metadata.switch ? res.metadata.switch.schedule : false,
							isLoading: false,
						})
						if (res.metadata.cta) {
							this.metadata.cta = res.metadata.cta
						}
						if (res.metadata.promo) {
							this.metadata.promo = res.metadata.promo
						}
						if (res.metadata.switch) {
							this.metadata.switch = res.metadata.switch
						}
						if (res.metadata.date) {
							this.metadata.date = res.metadata.date
						}
					}).catch(this.onError)
				})
			} else {
				// creating
				this.setState({
					isLoading: false,
				})
			}
		}

		onSave = () => {
			if(
				(!this.props.id &&
				(isEmpty(this.state.changes.slug)))
			) {
				this.utilities.alert.show({
					title: 'Data not complete',
					message: 'Slug must be filled',
					actions: [{
						title: 'Close',
						type: 'CANCEL',
					}],
				})
			} else {
				this.setState({
					isLoading: true,
				}, () => {
					if(this.props.id) {
						CollectionService.updateCollection(this.props.id, this.state.changes, this.state.token)
							.then(() => {
								// window.location.reload()
								this.setState({
									isLoading: false,
									data: {
										...this.state.data,
										...this.state.changes,
									},
									changes: {},
								})

								this.utilities.notification.show({
									title: 'Yeay…',
									message: 'Collection saved.',
									type: this.utilities.notification.TYPES.SUCCESS,
								})
							})
							.catch(this.onError)
					} else {
						CollectionService.createCollection(this.state.changes, this.state.token)
							.then((res) => {
								this.setState({
									isLoading: false,
									data: {
										...this.state.data,
										...this.state.changes,
									},
									changes: {},
								})

								this.utilities.notification.show({
									title: 'Yeay…',
									message: 'Collection created.',
									type: this.utilities.notification.TYPES.SUCCESS,
								})
								this.navigator.navigate(`collection/${res.id}`)
							})
							.catch(this.onError)
					}
				})
			}
		}

		onError = err => {
			this.warn(err);

			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong with your request, please try again later',
				})
			})
		}

		onChange = (key, val) => {
			this.state.changes[key] = this.isNumeric(val) ? parseInt(val, 10) : val

			// KEPAKE PADA SAAT UNCHECK ITEM (EDITTING)
			if(this.props.id && (key === 'valid_for' || key === 'valid_for_all')) {
				this.forceUpdate()
			}

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onChangeCta = (key, val) => {
			this.metadata.cta[key] = val

			if(this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}

			this.onChange &&
			this.onChange('metadata', this.metadata)
		}

		onChangePromo = (key, val) => {
			this.metadata.promo[key] = val

			if(this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}

			this.onChange &&
			this.onChange('metadata', this.metadata)
		}

		onChangeDate = (key, val) => {
			this.metadata.date[key] = val

			if(this.state.isChanged === false) {
				this.setState({
					isChanged: true,
				})
			}

			this.onChange &&
			this.onChange('metadata', this.metadata)
		}

		onSwitch = () => {
			this.onChange &&
			this.onChange('use_promo', !this.state.usePromo)
			this.setState({
				usePromo: !this.state.usePromo,
			}, () => {
				// console.log(this.state.usePromo)
			})
		}

		onSwitchShow = () => {
			this.onChange &&
			this.onChange('show_on_page', !this.state.showOnPage)
			this.setState({
				showOnPage: !this.state.showOnPage,
			}, () => {
				// console.log(this.state.showOnPage)
			})
		}

		onSwitchSchedule = () => {
			this.metadata.switch.schedule = !this.state.useSchedule
			this.setState({
				useSchedule: !this.state.useSchedule,
			})

			this.onChange &&
			this.onChange('metadata', this.metadata)
		}

		onUploadDataLoaded = (id, data) => {
			this.setState({
				isUploading: true,
			}, () => {
				CollectionService.uploadCSV( this.state.data.id, data, this.state.token ).then(() => {
					this.setState({
						isUploading: false,
						uploader: this.state.uploader + 1,
					}, () => {
						this.utilities.notification.show({
							message: 'Success uploading CSV. Reloading',
							type: this.utilities.notification.TYPES.SUCCESS,
						})
	
						window.location.reload()
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isUploading: false,
						uploader: this.state.uploader + 1,
					}, () => {
						this.utilities.notification.show({
							message: err ? err.detail && err.message || err : 'Oops… Something went wrong. Please try again later.',
						})
					})

				})
			})
		}

		onUploadDataError = () => {
			this.setState({
				uploader: this.state.uploader + 1,
			})
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={'marketing'}
					onAuthorized={this.onAuthorized}
					paths={[{
						title: 'Collections',
						onPress: this.onNavigateToCollection,
					}, {
						title: this.props.id
							? this.state.isLoading
								? 'Loading...'
								: this.state.data.title
							: 'New Collection',
					}]}
				>
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title={ this.props.id ? 'Collection Detail' : 'Add New Collection' }
							/>
							<BoxBit row>
								<BoxBit style={ Styles.padder }>
									<DetailPart
										key={ `detail_${this.state.isLoading}` }
										collectionId={ this.props.id }
										slug={ this.state.data.slug }
										title={ this.state.data.title }
										banner={ this.state.data.banner}
										banner_mobile={ this.state.data.banner_mobile }
										schedules={ this.state.data.schedules }
										coupon_id={ this.state.data.coupon_id }
										onChange={ this.onChange }
										// onSwitch={ this.onSwitch }
										showOnPage={ this.state.showOnPage }
										usePromo={ this.state.usePromo }
										onImageLoaded={ this.onImageLoaded }
										onImageRemoved={ this.onImageRemoved }
										style={ Styles.box }
									/>
									<PromoPart
										title={ this.state.data.title }
										banner={ this.state.data.banner}
										onChangePromo={ this.onChangePromo }
										onSwitch={ this.onSwitch }
										usePromo={ this.state.usePromo }
										metadata={ this.state.data.metadata.cta ? this.state.data.metadata : this.metadata }
										style={ Styles.box }
									/>
									{ this.state.usePromo &&
										<CollectionItemsPagelet
											key={ 'COUPON' }
											type={ 'COUPON' }
											id={ this.props.id }
											onChange={ this.onChange }
											isValidForAll={ this.state.changes.valid_for_all === undefined ? this.state.data.valid_for_all : this.state.changes.valid_for_all }
											style={ Styles.box }
										/>
									}
									{ (this.state.usePromo && this.props.id) &&
										<SchedulePart
											collectionId={ this.props.id }
											schedules={ this.state.data.schedules ? this.state.data.schedules : false }
											onChange={ this.onChange }
											onSwitchSchedule={ this.onSwitchSchedule }
											useSchedule={ this.state.useSchedule }
											metadata={ this.state.data.metadata.switch ? this.state.data.metadata : this.metadata }
										/>
									}
								</BoxBit>
								<BoxBit>
									<ActionPart
										key={ this.state.isLoading }
										isLoading={ this.state.isLoading }
										// showOnPage={ this.state.showOnPage }
										// usePromo={ this.state.usePromo }
										metadata={ this.state.data.metadata.date ? this.state.data.metadata : this.metadata }
										isChanged={ this.state.isChanged }
										onChangeDate={ this.onChangeDate }
										onSave={ this.onSave }
										style={ Styles.box }
									/>
									<ShowPart
										key={ `detail_${this.state.isLoading}` }
										onChange={ this.onChange }
										onChangeCta={ this.onChangeCta }
										onSwitchShow={ this.onSwitchShow }
										metadata={ this.state.data.metadata.cta ? this.state.data.metadata : this.metadata }
										showOnPage={ this.state.showOnPage }
										subbanner={ this.state.data.subbanner }
										onImageRemoved={ this.onImageRemoved }
										style={ Styles.box }
									/>
									{ this.props.id &&
										<CollectionItemsPagelet
											key={ 'VARIANT' }
											type={ 'VARIANT' }
											id={ this.props.id }
											isValidForAll={ this.state.changes.valid_for_all === undefined ? this.state.data.valid_for_all : this.state.changes.valid_for_all }
											style={ Styles.box }
											onUploadDataLoaded={ this.onUploadDataLoaded}
											onUploadDataError={this.onUploadDataError}
											isUploading={ this.state.isUploading }
											inputFileKey={ this.state.uploader }
										/>
									}
									{/* { this.props.id && (this.state.changes.valid_for || this.state.data.valid_for) ? (this.state.changes.valid_for || this.state.data.valid_for).map(this.couponItemRenderer) : false } */}
								</BoxBit>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
