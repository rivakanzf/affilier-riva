/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';
import Colors from 'coeur/constants/color';

import CollectionService from 'app/services/collection';

import BoxBit from 'modules/bits/box';
import SelectionBit from 'modules/bits/selection';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';

import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import TableLego from 'modules/legos/table';

import QuickViewPart from './_quick.view';
import Styles from './style';

import { isEqual, camelCase, mapKeys } from 'lodash'
export {
	QuickViewPart,
}

export default ConnectHelper(

	class CollectionsPage extends PageModel {

        static routeName = 'collections'

        constructor(p) {
        	super(p, {
        		isLoading: true,
        		isUpdatingData: [],
        		offset: 0,
        		count: 30,
        		total: 0,
        		search: p.search || undefined,
        		collections: [],
        		filter: {
        			status: undefined,
        		},
        		token: undefined,
        	}, [])

        	this.getterId = 1

        }

        headers = [{
        	title: 'Collection Name',
        	width: 1,
        }, {
        	title: 'Show On Page',
        	align: 'center',
        	width: 1,
        }, {
        	title: 'Use Coupon',
        	align: 'center',
        	width: 1,
        }, {
        	title: 'Use Schedule',
        	align: 'center',
        	width: 1,
        }, {
        	title: 'Publish Date',
        	align: 'center',
        	width: 1,
        }, {
        	title: 'End Date',
        	align: 'center',
        	width: 1,
        }]

		statuses = [{
			key: '',
			title: 'All status',
		}, {
			key: 'scheduled:true',
			title: 'Scheduled',
		}, {
			key: 'scheduled:false',
			title: 'Not Scheduled',
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token && (
					!isEqual(pS.offset, this.state.offset)
					|| !isEqual(pS.limit, this.state.limit)
					|| !isEqual(pS.search, this.state.search)
					|| !isEqual(pS.filter.status, this.state.filter.status)
				)
			) {
				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData() {
			const {
				offset,
				count,
				search,
				filter,
			} = this.state;

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				CollectionService.getCollections({
					offset: offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(!!filter.status ? { [filter.status.split(':')[0]]: filter.status.split(':')[1] } : {}),
				}, this.state.token).then(res => {
					if(id === this.getterId) {
						this.setState({
							isLoading: false,
							// total: res.count,
							collections: res.data.map(collection => mapKeys(collection, (val, key) => camelCase(key))),
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

        onSearch = val => {
        	this.setState({
        		offset: 0,
        		search: val,
        	})
        }

		onNavigateToCollectionDetail = collectionId => {
			this.navigator.navigate(`collection/${collectionId}`)
		}

		onNavigateToCreate = () => {
			this.navigator.navigate('collection/create')
		}

        onChangeFilter = (key, val) => {
        	this.setState({
        		offset: 0,
        		filter: {
        			...this.state.filter,
        			[key]: val,
        		},
        	})
        }

        onNavigationChange = (offset, count) => {
        	this.setState({
        		offset,
        		count,
        	})
        }

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

        rowRenderer = collection => {
        	return {
        		data: [{
        			children: (
        				<BoxBit>
        					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium">{ collection.slug }</GeomanistBit>
        				</BoxBit>
        			),
        		}, {
        			children: (
        				<IconBit
        					name={ collection.showOnPage ? 'circle-checkmark' : 'circle-null' }
        					color={ collection.showOnPage ? Colors.green.palette(1) : Colors.grey.palette(6) }
        				/>
        			),
        		}, {
        			children: (
        				<IconBit
        					name={ collection.usePromo ? 'circle-checkmark' : 'circle-null' }
        					color={ collection.usePromo ? Colors.green.palette(1) : Colors.grey.palette(6) }
        				/>
        			),
        		}, {
        			// children: (
        			// 	<IconBit
        			// 		name={ collection.metadata.switch.schedule && collection.usePromo ? 'circle-checkmark' : 'circle-null' }
        			// 		color={ collection.metadata.switch.schedule && collection.usePromo ? Colors.green.palette(1) : Colors.grey.palette(6) }
        			// 	/>
        			// ),
        		}, {
        			children: (
        				<BoxBit>{ !!collection.metadata.date && (<>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>{ TimeHelper.moment(collection.metadata.date.publish_date).format('DD/MM/YYYY — HH:mm') }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>{ TimeHelper.moment(collection.metadata.date.publish_date).fromNow() }</GeomanistBit>
						</>) }</BoxBit>
        			),
        		}, {
        			children: (
        				<BoxBit>{ !!collection.metadata.date && (<>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>{ TimeHelper.moment(collection.metadata.date.end_date).format('DD/MM/YYYY — HH:mm') }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>{ TimeHelper.moment(collection.metadata.date.end_date).fromNow() }</GeomanistBit>
						</>) }</BoxBit>
        			),
        		}],
        		onPress: this.onNavigateToCollectionDetail.bind(this, collection.id),
        	}
        }

        view() {
        	return (
        		<PageAuthorizedComponent
        			roles={'marketing'}
        			onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Collections' }]}
        		>
        			{ () => (
        				<BoxBit style={Styles.main}>
        					<HeaderPageComponent
        						title="Collections"
        						buttonPlaceholder="Create Collection"
        						onPress={this.onNavigateToCreate}
        						searchPlaceholder={ 'ID, Collection Name' }
        						search={ this.state.search }
        						onSearch={this.onSearch} />
        					<HeaderFilterComponent
        						current={this.state.offset}
        						count={this.state.count}
        						total={this.state.total}
        						onNavigationChange={this.onNavigationChange} >
        						<SelectionBit
        							placeholder={'All status'}
        							options={this.statuses}
        							onChange={this.onChangeFilter.bind(this, 'status')}
        						/>
        					</HeaderFilterComponent>
        					<TableLego
        						isLoading={this.state.isLoading}
        						headers={this.headers}
        						rows={this.state.collections.map(this.rowRenderer)}
        						style={Styles.padder}
        					/>
        				</BoxBit>
        			)}
        		</PageAuthorizedComponent>
        	)
        }
	}
)
