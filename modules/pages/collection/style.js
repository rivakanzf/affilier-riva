import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},
	wrapper: {
		height: '100%',
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		paddingBottom: 32,
		paddingLeft: 32,
		paddingRight: 32,
	},

	padder: {
		marginBottom: 16,
	},
})
