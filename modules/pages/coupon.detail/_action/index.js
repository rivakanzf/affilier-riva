import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';

import BoxRowLego from 'modules/legos/box.row';

import Styles from './style';


export default ConnectHelper(
	class ActionPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				count: PropTypes.number,
				paidCount: PropTypes.number,
				loading: PropTypes.bool,
				isChanged: PropTypes.bool,
				createdAt: PropTypes.date,
				publishedAt: PropTypes.date,
				expiredAt: PropTypes.date,

				onSave: PropTypes.func,
				style: PropTypes.style,
			}
		}

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Usage',
							content: '-',
							children: this.props.count ? (
								<BoxBit unflex row>
									<GeomanistBit weight="normal" type={ GeomanistBit.TYPES.HEADER_5 }>
										{ this.props.paidCount }/{ this.props.count }
									</GeomanistBit>
									{ this.props.publishedAt && this.props.expiredAt ? (
										<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.note }>
											{ ' ' } in { TimeHelper.moment(this.props.expiredAt).diff(this.props.publishedAt, 'd') } days
										</GeomanistBit>
									) : false }
								</BoxBit>
							) : undefined,
							description: `Created at: ${ TimeHelper.format(this.props.createdAt, 'DD MMM YYYY HH:mm') }`,
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ 'Save Coupon' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.props.loading
										? ButtonBit.TYPES.STATES.LOADING
										: this.props.disabled || !this.props.isChanged
											&& ButtonBit.TYPES.STATES.DISABLED
											|| ButtonBit.TYPES.STATES.NORMAL
									}
									onPress={ this.props.onSave }
								/>
							),
						}],
					}]}
					style={ this.props.style }
				/>
			)
		}
	}
)
