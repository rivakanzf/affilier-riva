import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import InputDateTimeBit from 'modules/bits/input.datetime';
import InputValidatedBit from 'modules/bits/input.validated';
import InputCheckboxBit from 'modules/bits/input.checkbox';

import BoxRowLego from 'modules/legos/box.row'
import SelectCouponTypeLego from 'modules/legos/select.coupon.type';

// import Styles from './style';


export default ConnectHelper(
	class DetailPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				// readonly: PropTypes.bool,
				id: PropTypes.id,
				title: PropTypes.string,
				caption: PropTypes.string,
				description: PropTypes.string,
				terms: PropTypes.string,
				type: PropTypes.string,
				amount: PropTypes.number,
				asCashback: PropTypes.bool,
				isRefundable: PropTypes.bool,
				publishedAt: PropTypes.date,
				expiredAt: PropTypes.date,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		onChange = (key, value) => {
			this.props.onChange &&
			this.props.onChange(key, value)
		}

		view() {
			return (
				<BoxRowLego
					title={ this.props.id ? `Coupon #CO-${ this.props.id }` : 'General' }
					data={[{
						data: [{
							title: 'Coupon Code',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here"
									value={ this.props.title }
									onChange={ this.onChange.bind(this, 'title') }
								/>
							),
							description: 'Please use capital letters.',
						}, {
							title: 'Caption',
							children: (
								<InputValidatedBit disabled
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here"
									value={ this.props.caption }
									onChange={ this.onChange.bind(this, 'caption') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Description',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.INPUT }
									placeholder="Input here"
									value={ this.props.description }
									onChange={ this.onChange.bind(this, 'description') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Terms',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.TEXTAREA }
									placeholder="Input here"
									value={ this.props.terms }
									onChange={ this.onChange.bind(this, 'terms') }
								/>
							),
							description: 'Readable terms for users to read.',
						}],
					}, {
						data: [{
							title: 'Type',
							children: (
								<SelectCouponTypeLego
									value={ this.props.type }
									onChange={ this.onChange.bind(this, 'type') }
								/>
							),
						}, {
							title: 'Amount',
							children: (
								<InputValidatedBit
									type={ InputValidatedBit.TYPES.CURRENCY }
									placeholder="Input here"
									value={ this.props.amount }
									onChange={ this.onChange.bind(this, 'amount') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'As Cashback',
							children: (
								<InputCheckboxBit
									value={ this.props.asCashback }
									onChange={ this.onChange.bind(this, 'as_cashback') }
								/>
							),
						}, {
							title: 'Refundable',
							children: (
								<InputCheckboxBit
									value={ this.props.isRefundable }
									onChange={ this.onChange.bind(this, 'is_refundable') }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Publish Date',
							children: (
								<InputDateTimeBit unflex={ false }
									title={ '' }
									date={ this.props.publishedAt }
									onChange={ this.onChange.bind(this, 'published_at') }
								/>
							),
						}, {
							title: 'Expire Date',
							children: (
								<InputDateTimeBit unflex={ false }
									title={ '' }
									date={ this.props.expiredAt }
									onChange={ this.onChange.bind(this, 'expired_at') }
								/>
							),
						}],
					}]}
					style={ this.props.style }
				/>
			)
		}
	}
)
