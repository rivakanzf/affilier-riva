import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BoxBit from 'modules/bits/box';
import InputCheckboxBit from 'modules/bits/input.checkbox';
import InputCurrencyBit from 'modules/bits/input.currency';
import InputValidatedBit from 'modules/bits/input.validated';

import BoxRowLego from 'modules/legos/box.row';
import SelectCouponValidityLego from 'modules/legos/select.coupon.validity';

// import Styles from './style';


export default ConnectHelper(
	class RulesPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				isPublic: PropTypes.bool,
				validFor: PropTypes.arrayOf(PropTypes.string),
				validForAll: PropTypes.bool,
				freeShipping: PropTypes.bool,
				firstPurchaseOnly: PropTypes.bool,
				internalProductOnly: PropTypes.bool,
				minimumSpend: PropTypes.number,
				maximumDiscount: PropTypes.number,
				usageLimit: PropTypes.number,
				usageLimitPerUser: PropTypes.number,
				forRecommendation: PropTypes.bool,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		onChange = (key, val) => {
			this.props.onChange &&
			this.props.onChange(key, val)
		}

		view() {
			return (
				<BoxRowLego
					title={ 'Rules' }
					data={[{
						data: [{
							title: 'Valid For',
							children: (
								<SelectCouponValidityLego
									value={ this.props.validFor }
									onChange={ this.onChange.bind(this, 'valid_for') }
								/>
							),
							description: 'Made this coupon only valid for certain user or item type.',
						}],
					}, {
						data: [{
							title: 'Minimum Spend',
							children: (
								<InputCurrencyBit value={ this.props.minimumSpend } onChange={ this.onChange.bind(this, 'minimum_spend') } />
							),
							description: 'Minimum purchase amount before this coupon made valid.',
						}, {
							title: 'Maximum Discount',
							children: (
								<InputCurrencyBit value={ this.props.maximumDiscount } onChange={ this.onChange.bind(this, 'maximum_discount')} />
							),
							description: 'Maximum discount for total transaction',
						}],
					}, {
						data: [{
							title: 'Usage Limit',
							children: (
								<InputValidatedBit type={ InputValidatedBit.TYPES.CURRENCY } value={ this.props.usageLimit } onChange={ this.onChange.bind(this, 'usage_limit') } />
							),
							description: 'Maximum coupon usage. Input -1 to set no usage limit.',
						}, {
							title: 'Usage Limit Per User',
							children: (
								<InputValidatedBit type={ InputValidatedBit.TYPES.CURRENCY } value={ this.props.usageLimitPerUser } onChange={ this.onChange.bind(this, 'usage_limit_per_user') } />
							),
							description: 'Maximum coupon usage per user. Input -1 to set no usage limit.',
						}],
					}, {
						data: [{
							title: 'Additional Rules',
							children: (
								<BoxBit unflex>
									<InputCheckboxBit
										value={ this.props.internalProductOnly }
										title="Internal product only"
										description={ 'Only valid for internal product' }
										onChange={ this.onChange.bind(this, 'internal_product_only') }
									/>
									<InputCheckboxBit
										value={ this.props.forRecommendation }
										title="For All Recommendation (Board)"
										description={ 'Coupon will show on boards page' }
										onChange={ this.onChange.bind(this, 'for_all_recommendation') }
									/>
									<InputCheckboxBit
										value={ this.props.isPublic }
										title="Public Coupon"
										description={ 'Coupon will appear on coupon recommendations.' }
										onChange={ this.onChange.bind(this, 'is_public') }
									/>
									<InputCheckboxBit
										value={ this.props.validForAll }
										title="Valid For All Item"
										description={ 'Except for users, enabling this will make all item that is included in "Valid For" eligible for coupon usage.' }
										onChange={ this.onChange.bind(this, 'valid_for_all') }
									/>
									<InputCheckboxBit
										value={ this.props.freeShipping }
										title="Free Shipping"
										description="Coupon usage will make applicable shipment price reduced to zero."
										onChange={ this.onChange.bind(this, 'free_shipping') }
									/>
									<InputCheckboxBit
										value={ this.props.firstPurchaseOnly }
										title="First Purchase Only"
										description="Coupon will be applicable only for users that has no paid order."
										onChange={ this.onChange.bind(this, 'first_purchase_only') }
									/>
								</BoxBit>
							),
						}],
					}]}
					style={ this.props.style }
				/>
			)
		}
	}
)
