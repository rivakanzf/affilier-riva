import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingBottom: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
	},
	timeStyle: {
		marginBottom: 0,
	},
	inputDate: {
		flex: 1,
	},
})
