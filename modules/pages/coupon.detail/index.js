/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import CouponService from 'app/services/coupon';

import BoxBit from 'modules/bits/box';

import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import CouponItemsPagelet from 'modules/pagelets/coupon.items';

import ActionPart from './_action';
import DetailPart from './_detail';
import RulesPart from './_rules';

import Styles from './style';

import { isEmpty, has } from 'lodash'


export default ConnectHelper(
	class CouponDetailPage extends PageModel {

		static routeName = 'coupon.detail'

		constructor(p) {
			super(p, {
				isLoading: true,
				isChanged: false,
				token: undefined,
				data: {},
				changes: {},
			}, [])
		}

		isNumeric(num) {
			return /^-{0,1}\d+$/.test(num);
		}

		onNavigateToCoupons = () => {
			this.navigator.navigate('coupons')
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData = () => {
			if(this.props.id) {
				this.setState({
					isLoading: true,
				}, () => {
					CouponService.getDetail(this.props.id, {}, this.state.token).then(res => {
						this.setState({
							data: res,
							isLoading: false,
						})
					}).catch(this.onError)
				})
			} else {
				// creating
				this.setState({
					isLoading: false,
				})
			}
		}

		onSave = () => {
			if(this.state.changes.amount < 0) {
				this.utilities.alert.show({
					message: 'Please insert amount greater than or equal to 0',
					actions: [{
						title: 'Close',
						type: 'CANCEL',
					}],
				})
			// } else if(
			// 	!this.props.id &&
			// 	(isEmpty(this.state.changes.title)
			// 		|| isEmpty(this.state.changes.type)
			// 		|| isEmpty(this.state.changes.valid_for)
			// 		|| this.state.changes.amount
			// 	)
			// ) {
			// 	this.utilities.alert.show({
			// 		title: 'Data not complete',
			// 		message: 'Code, Type, Items, and Amount must be filled',
			// 		actions: [{
			// 			title: 'Close',
			// 			type: 'CANCEL',
			// 		}],
			// 	})
			} else {
				this.setState({
					isLoading: true,
				}, () => {
					if(this.props.id) {
						CouponService.updateCoupon(this.props.id, this.state.changes, this.state.token)
							.then(() => {
								// window.location.reload()
								this.setState({
									isLoading: false,
									data: {
										...this.state.data,
										...this.state.changes,
									},
									changes: {},
								})

								this.utilities.notification.show({
									title: 'Yeay…',
									message: 'Coupon saved.',
									type: this.utilities.notification.TYPES.SUCCESS,
								})
							})
							.catch(this.onError)
					} else {
						CouponService.createCoupon(this.state.changes, this.state.token)
							.then(() => {
								this.setState({
									isLoading: false,
									data: {
										...this.state.data,
										...this.state.changes,
									},
									changes: {},
								})

								this.utilities.notification.show({
									title: 'Yeay…',
									message: 'Coupon created.',
									type: this.utilities.notification.TYPES.SUCCESS,
								})
							})
							.catch(this.onError)
					}
				})
			}
		}

		onError = err => {
			this.warn(err);

			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong with your request, please try again later',
				})
			})
		}

		onChange = (key, val) => {
			this.state.changes[key] = this.isNumeric(val) ? parseInt(val, 10) : val

			// KEPAKE PADA SAAT UNCHECK ITEM (EDITTING)
			if(this.props.id && (key === 'valid_for' || key === 'valid_for_all')) {
				this.forceUpdate()
			}

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		couponItemRenderer = type => {
			return this.props.id ? (
				<React.Fragment>
					<CouponItemsPagelet
						key={ type }
						type={ type }
						id={ this.props.id }
						// eslint-disable-next-line no-nested-ternary
						isValidForAll={ type === 'USER' ? false : this.state.changes.valid_for_all === undefined ? this.state.data.valid_for_all : this.state.changes.valid_for_all }
						style={ Styles.box }
					/>
					{ type === 'PRODUCT' ? (
						<CouponItemsPagelet
							key={ 'VARIANT' }
							type={ 'VARIANT' }
							id={ this.props.id }
							isValidForAll={ this.state.changes.valid_for_all === undefined ? this.state.data.valid_for_all : this.state.changes.valid_for_all }
							style={ Styles.box }
						/>
					) : undefined }
				</React.Fragment>
			) : false
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={'marketing'}
					onAuthorized={this.onAuthorized}
					paths={[{
						title: 'Coupons',
						onPress: this.onNavigateToCoupons,
					}, {
						title: this.props.id
							? this.state.isLoading
								? 'Loading...'
								: this.state.data.title
							: 'New Coupon',
					}]}
				>
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title={ this.props.id ? 'Coupon Detail' : 'Add New Coupon' }
								// paths={[{
								// 	title: 'COUPON',
								// 	onPress: this.onNavigateToBack,
								// }, {
								// 	title: `${this.props.id ? this.props.id : 'NEW COUPON'}`,
								// }]}
							/>
							<BoxBit row>
								<BoxBit style={ Styles.padder }>
									<DetailPart
										key={ `detail_${this.state.isLoading}` }
										id={ this.props.id }
										title={ this.state.data.title }
										caption={ this.state.data.caption }
										description={ this.state.data.description }
										terms={ this.state.data.terms }
										type={ this.state.data.type }
										amount={ this.state.data.amount }
										asCashback={ this.state.data.as_cashback}
										publishedAt={ this.state.data.published_at }
										expiredAt={ this.state.data.expired_at }
										asCashback={ this.state.data.as_cashback}
										isRefundable={ this.state.data.is_refundable}
										onChange={ this.onChange }
										style={ Styles.box }
									/>
									<RulesPart
										key={ `rules_${this.state.isLoading}` }
										isPublic={ this.state.data.is_public }
										maximumDiscount={ this.state.data.maximum_discount }
										validFor={ this.state.data.valid_for }
										validForAll={ this.state.data.valid_for_all }
										freeShipping={ this.state.data.free_shipping }
										firstPurchaseOnly={ this.state.data.first_purchase_only }
										minimumSpend={ this.state.data.minimum_spend }
										maximumSpend={ this.state.data.maximum_spend }
										internalProductOnly={ has(this.state.data.metadata, 'internal_product_only') ? this.state.data.metadata.internal_product_only : false}
										usageLimit={ this.state.data.usage_limit }
										usageLimitPerUser={ this.state.data.usage_limit_per_user }
										forRecommendation={ has(this.state.data.metadata, 'for_all_recommendation') ? this.state.data.metadata.for_all_recommendation : false }
										onChange={ this.onChange }
										style={ Styles.box }
									/>
								</BoxBit>
								<BoxBit>
									<ActionPart
										count={ this.state.data.count }
										paidCount={ this.state.data.paid_count }
										createdAt={ this.state.data.created_at }
										publishedAt={this.state.data.published_at}
										expiredAt={this.state.data.expired_at}
										loading={ this.state.isLoading }
										isChanged={ this.state.isChanged }
										onSave={ this.onSave }
										style={ Styles.box }
									/>
									{ this.props.id && (this.state.changes.valid_for || this.state.data.valid_for) ? (this.state.changes.valid_for || this.state.data.valid_for).map(this.couponItemRenderer) : false }
								</BoxBit>
							</BoxBit>

						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
