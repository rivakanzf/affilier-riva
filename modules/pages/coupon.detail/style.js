import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	padder: {
		marginRight: Sizes.margin.default,
	},

	box: {
		marginBottom: Sizes.margin.default,
	},
})
