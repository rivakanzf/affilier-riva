/* eslint-disable no-nested-ternary */
import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import CouponService from 'app/services/coupon';

import Colors from 'utils/constants/color'


import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';

import BadgeStatusLego from 'modules/legos/badge.status';
import LoaderLego from 'modules/legos/loader';
import RowsLego from 'modules/legos/rows';
import TableLego from 'modules/legos/table';

// import AddressesComponent from 'modules/components/addresses';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';

import { capitalize } from 'lodash';


export default ConnectHelper(
	class QuickViewPart extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				// couponId: PropTypes.number,
				id: PropTypes.number,
				onClose: PropTypes.func,
				onNavigateToCouponDetail: PropTypes.func,
			}
		}

		static propsToPromise(state, oP) {
			return CouponService.getDetail(oP.id, {
				detailed: true,
			}, state.me.token)
		}

		headers = [{
			title: 'Valid For',
			width: 4,
		}, {
			title: 'Type',
			width: 1,
			align: 'right',
		}]

		itemRenderer = item => {
			return {
				data: [{
					title: `${ item.id ? `[#${ item.id }] ` : '' }${ item.title }`,
				}, {
					title: capitalize(item.type),
				}],
			}
		}

		viewOnError() {
			return (
				<BoxBit row centering style={Styles.empty}>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.error}>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			const publishedAt = TimeHelper.moment(this.props.data.published_at)
			const isActive = +publishedAt < Date.now() && (
				this.props.data.expired_at ? +TimeHelper.moment(this.props.data.expired_at) > +publishedAt : true
			)

			return (
				<ModalClosableConfirmationPagelet
					title={ `Coupon Preview — ${this.props.data.title}` }
					header={( <BadgeStatusLego status={ isActive ? 'ACTIVE' : 'INACTIVE' } /> )}
					confirm="Edit Coupon"
					onClose={ this.props.onClose }
					onConfirm={ this.props.onNavigateToCouponDetail }
					contentContainerStyle={ Styles.container }
				>
					<RowsLego
						data={[{
							data: [{
								title: 'Description',
								content: this.props.data.description || '-',
							}, {
								title: 'Usage',
								content: this.props.data.count ? `${ this.props.data.paid_count }/${ this.props.data.count }` : '-',
							}],
						}, {
							data: [{
								title: 'Type',
								content: this.props.data.type || '-',
							}, {
								title: 'Amount',
								content: this.props.data.type === 'PERCENT' ? `${this.props.data.amount}%` : `IDR ${FormatHelper.currency(this.props.data.amount)}`,
							}],
						}, {
							data: [{
								title: 'Publish Date',
								content: this.props.data.published_at ? TimeHelper.format(this.props.data.published_at, 'DD/MM/YYYY HH:mm') : '-',
							}, {
								title: 'Expire Date',
								content: this.props.data.expired_at ? TimeHelper.format(this.props.data.expired_at, 'DD/MM/YYYY HH:mm') : '-',
							}],
						}, {
							data: [{
								title: 'Free Shipping',
								content: this.props.data.free_shipping ? 'Yes' : 'No',
								description: this.props.data.free_shipping ? 'Coupon usage will make applicable shipment price reduced to zero.' : undefined,
							}, {
								title: 'First Purchase Only',
								content: this.props.data.first_purchase_only ? 'Yes' : 'No',
								description: this.props.data.first_purchase_only ? 'Coupon will be applicable only for users that has no order.' : undefined,
							}],
						}, {
							data: [{
								title: 'Minimum Spend',
								content: this.props.data.minimum_spend ? `IDR ${ FormatHelper.currency(this.props.data.minimum_spend) }` : 'Not applied',
							}, {
								title: 'Maximum Spend',
								content: this.props.data.maximum_spend !== -1 ? `IDR ${ FormatHelper.currency(this.props.data.maximum_spend) }` : 'Not applied',
							}],
						}, {
							data: [{
								title: 'Usage Limit',
								content: this.props.data.usage_limit === -1 ? 'Not applied' : this.props.data.usage_limit,
							}, {
								title: 'Usage Limit Per User',
								content: this.props.data.usage_limit_per_user === -1 ? 'Not applied' : this.props.data.usage_limit_per_user,
							}],
						}]}
					/>
					<TableLego
						headers={ this.headers }
						rows={ []
							.concat(this.props.data.valid_for.indexOf('MATCHBOX') > -1
								? this.props.data.valid_for_all ? [{ type: 'MATCHBOX', title: 'All Matchboxes' }] : this.props.data.matchboxes
								: [])
							.concat(this.props.data.valid_for.indexOf('PRODUCT') > -1
								? this.props.data.valid_for_all ? [{ type: 'PRODUCT', title: 'All Products & Variants' }] : [].concat(this.props.data.products).concat(this.props.data.variants)
								: [])
							.concat(this.props.data.valid_for.indexOf('SERVICE') > -1
								? this.props.data.valid_for_all ? [{ type: 'SERVICE', title: 'All Services' }] : this.props.data.services
								: [])
							.concat(this.props.data.valid_for.indexOf('USER') > -1
								? this.props.data.users
								: [])
							.map(this.itemRenderer)
						}
						style={ Styles.table }
					/>
					{/* <BoxBit unflex style={ Styles.rules }>
						{ <GeomanistBit type={ GeomanistBit.TYPES.HEADER_5 }>
							Rules
						</GeomanistBit> }
						<RowsLego
							data={[{
								// data: [{
								// 	title: 'Valid For',
								// 	content: this.props.data.valid_for.map(v => capitalize(v)).join(', '),
								// 	// description: 'Type of product that this coupon is applicable for.',
								// }],
							}
								null, ]}
							style={ Styles.content }
						/>
					</BoxBit> */}
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
