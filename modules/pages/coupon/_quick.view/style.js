import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	empty: {
		paddingTop: Sizes.margin.default,
		minHeight: 300,
	},

	error: {
		color: Colors.primary,
		marginLeft: 8,
	},

	container: {
		// paddingBottom: 0,
		// paddingLeft: 0,
		// paddingRight: 0,
	},

	rules: {
		marginTop: Sizes.margin.default,
	},

	content: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		// marginTop: Sizes.margin.default,
		// marginLeft: -Sizes.margin.default,
		// marginRight: -Sizes.margin.default,
		// borderRadius: 2,
		// backgroundColor: Colors.grey.palette(2),
		// paddingLeft: Sizes.margin.default,
		// paddingRight: Sizes.margin.default,
		// paddingBottom: Sizes.margin.default,
	},

	table: {
		marginTop: Sizes.margin.default,
		// borderTopWidth: StyleSheet.hairlineWidth,
		// borderColor: Colors.black.palette(2, .16),
		// marginLeft: -Sizes.margin.default,
		// marginRight: -Sizes.margin.default,
	},

	applications: {
		// marginTop: Sizes.margin.default,
	},

	item: {
		backgroundColor: Colors.grey.palette(2),
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingTop: 8,
		paddingBottom: 8,
		alignItems: 'center',
		marginBottom: 4,
	},

	// image: {
	// 	height: 32,
	// 	width: 32,
	// 	marginRight: Sizes.margin.default,
	// },

	id: {
		marginRight: 8,
	},

})
