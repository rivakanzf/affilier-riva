/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';
import Colors from 'coeur/constants/color';

import CouponService from 'app/services/coupon';


import BoxBit from 'modules/bits/box';
import SelectionBit from 'modules/bits/selection';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';
import IconBit from 'modules/bits/icon';


import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import TableLego from 'modules/legos/table';

import QuickViewPart from './_quick.view';
import Styles from './style';

import { isEqual, camelCase, mapKeys } from 'lodash'
export {
	QuickViewPart,
}

export default ConnectHelper(
	class CouponsPage extends PageModel {

		static routeName = 'coupons'

		constructor(p) {
			super(p, {
				isLoading: true,
				isUpdatingData: [],
				offset: 0,
				count: 30,
				total: 0,
				search: p.search || undefined,
				coupons: [],
				filter: {
					status: undefined,
				},
				token: undefined,
			}, [])

			this.getterId = 1
		}

		headers = [{
			title: 'Title',
			width: 5,
		}, {
			title: 'Amount',
			width: 2,
		}, {
			title: 'Is Active',
			align: 'center',
			width: 1,
		}, {
			title: 'As Cashback',
			align: 'center',
			width: 2,
		}, {
			title: 'Usage',
			width: 1,
		}, {
			title: 'Published At',
			width: 2,
		}, {
			title: 'Expired At',
			width: 2,
		}, {
			title: '',
			width: .5,
		}]

		statuses = [{
			key: '',
			title: 'All status',
		}, {
			key: 'expired:true',
			title: 'Expired',
		}, {
			key: 'expired:false',
			title: 'Not Expired',
		}, {
			key: 'published:true',
			title: 'Published',
		}, {
			key: 'published:false',
			title: 'Not Published',
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token && (
					!isEqual(pS.offset, this.state.offset)
					|| !isEqual(pS.limit, this.state.limit)
					|| !isEqual(pS.search, this.state.search)
					|| !isEqual(pS.filter.status, this.state.filter.status)
				)
			) {
				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData() {
			const {
				offset,
				count,
				search,
				filter,
			} = this.state;

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				CouponService.getCoupons({
					offset: offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(!!filter.status ? { [filter.status.split(':')[0]]: filter.status.split(':')[1] } : {}),
				}, this.state.token).then(res => {
					if(id === this.getterId) {
						this.setState({
							isLoading: false,
							total: res.count,
							coupons: res.data.map(coupon => mapKeys(coupon, (val, key) => camelCase(key))),
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigateToCouponDetail = couponId => {
			this.navigator.navigate(`coupon/${couponId}`)
		}

		onNavigateToCreate = () => {
			this.navigator.navigate('coupon/create')
		}


		onChangeFilter = (key, val) => {
			this.setState({
				offset: 0,
				filter: {
					...this.state.filter,
					[key]: val,
				},
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onQuickView = id => {
			this.utilities.alert.modal({
				component: (
					<QuickViewPart
						id={ id }
						onNavigateToCouponDetail={ this.onNavigateToCouponDetail.bind(this, id) }
						onClose={ this.onModalRequestClose }
					/>
				),
			})
		}

		rowRenderer = coupon => {
			// const isActive = +publishedAt < Date.now() && (
			// 	coupon.expiredAt ? +TimeHelper.moment(coupon.expiredAt) > +publishedAt : true
			// )

			const isActive = !!coupon.expiredAt
				? TimeHelper.moment(new Date()).isAfter(coupon.publishedAt) && TimeHelper.moment(new Date()).isBefore(coupon.expiredAt)
				: !!coupon.publishedAt
					? TimeHelper.moment(new Date()).isAfter(coupon.publishedAt)
					: false

			return {
				data: [{
					children: (
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium">{ coupon.title }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>{ coupon.description }</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: coupon.type === 'PERCENT' ? `${coupon.amount}%` : `IDR ${FormatHelper.currencyFormat(coupon.amount)}`,
				}, {
					children: (
						<IconBit
							name={ isActive ? 'circle-checkmark' : 'circle-null' }
							color={ isActive ? Colors.green.palette(1) : Colors.grey.palette(6) }
						/>
					),
				}, {
					children: (
						<IconBit
							name={ coupon.asCashback ? 'circle-checkmark' : 'circle-null' }
							color={ coupon.asCashback ? Colors.green.palette(1) : Colors.grey.palette(6) }
						/>
					),
				}, {
					title: '-',
					children: coupon.count || coupon.paid_count ? (
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>{ coupon.paidCount }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>of { coupon.count }</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					children: (
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>{ TimeHelper.moment(coupon.publishedAt).format('DD/MM/YYYY — HH:mm') }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>{ TimeHelper.moment(coupon.publishedAt).fromNow() }</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: '-',
					children: coupon.expiredAt ? (
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>{ TimeHelper.moment(coupon.expiredAt).format('DD/MM/YYYY — HH:mm') }</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>{ TimeHelper.moment(coupon.expiredAt).fromNow() }</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					children: (
						<TouchableBit centering row onPress={this.onQuickView.bind(this, coupon.id)}>
							<BoxBit />
							<IconBit
								name="quick-view"
								color={Colors.primary}
							/>
						</TouchableBit>
					),
				}],
				onPress: this.onNavigateToCouponDetail.bind(this, coupon.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={'marketing'}
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Coupons' }]}
				>
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Coupons"
								buttonPlaceholder="Create Coupon"
								onPress={this.onNavigateToCreate}
								// paths={[{ title: 'COUPONS' }]}
								searchPlaceholder={ 'ID, Code' }
								search={ this.state.search }
								onSearch={this.onSearch} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} >
								<SelectionBit
									placeholder={'All status'}
									options={this.statuses}
									onChange={this.onChangeFilter.bind(this, 'status')}
								/>
							</HeaderFilterComponent>
							<TableLego
								isLoading={this.state.isLoading}
								headers={this.headers}
								rows={this.state.coupons.map(this.rowRenderer)}
								style={Styles.padder}
							/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
