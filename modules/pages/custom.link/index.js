import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'
import InputValidateBit from 'modules/bits/input.validated'
import IconBit from 'modules/bits/icon'
import TouchableBit from 'modules/bits/touchable'

import PageAuthorizedComponent from 'modules/components/page.authorized'

import BoxRowLego from 'modules/legos/box.row'

import ModalQuickViewPagelet from 'modules/pagelets/modal.quick.view'

import { isEmpty } from 'lodash'

import Styles from './style'

export default ConnectHelper(
	class CustomLinkPage extends PageModel {
		static routeName = 'custom.link'

		constructor(p) {
			super(p, {
				isLoading: false,
			})
		}

		static stateToProps(state) {
			return {
				firstName: state.me.firstName,
				userId: state.me.id,
			}
		}

		getUrl = () => {
			let { url } = {...this.state}

			url = url + '?tr_source=affiliate&affiliate=' + this.props.userId

			const tags = [this.state.tag1, this.state.tag2, this.state.tag3, this.state.tag4, this.state.tag5]

			for(let i = 1; i <= tags.length; i++) {
				if(!isEmpty(tags[i-1])) {
					url = url + '&tag' + i + '=' + tags[i - 1]
				}
			}

			this.onShowModal(url)
		}

		onClose = () => {
			this.props.utilities.alert.close()
		}

		onCopyLink = url => {
			navigator.clipboard.writeText(url)

			this.props.utilities.notification.show({
				message: 'Copied to clipboard',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})
		}

		onShowModal = url => {
			this.props.utilities.alert.modal({
				component: (
					<ModalQuickViewPagelet unflex
						layout="free"
						onClose={this.onClose}
						style={Styles.page}
					>
						<BoxBit unflex centering>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Link Affiliate
							</GeomanistBit>

							<BoxBit row>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									{ url }
								</GeomanistBit>
								<TouchableBit unflex onPress={ this.onCopyLink.bind(this, url) } style={ Styles.copyIcon }>
									<IconBit name="copy" size={16}/>
								</TouchableBit>
							</BoxBit>
						</BoxBit>
					</ModalQuickViewPagelet>
				),
			})
		}

		onChange = (key, value) => {
			this.setState({
				[key]: value,
			})
		}

		view() {
			return (
				<PageAuthorizedComponent
					paths={[{ title: 'Custom Link' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<BoxBit unflex>
								<BoxRowLego
									title="Custom Link"
									data={[{
										data: [{
											title: 'Create a custom link for a specific product',
										}],
									}, {
										data: [{
											title: 'Insert Url',
											children: (
												<InputValidateBit
													type={InputValidateBit.TYPES.INPUT}
													placeholder="Example: https://helloyuna.io/products/variants/:id"
													onChange={this.onChange.bind(this, 'url')}
												/>
											),
										}],
									}, {
										data: [{
											title: 'Tag Link 1',
											children: (
												<BoxBit unflex>
													<InputValidateBit
														type={InputValidateBit.TYPES.INPUT}
														placeholder="Example: Skirts"
														onChange={this.onChange.bind(this, 'tag1')}
													/>
												</BoxBit>
											),
										}],
									}, {
										data: [{
											title: 'Tag Link 2',
											children: (
												<BoxBit unflex>
													<InputValidateBit
														type={InputValidateBit.TYPES.INPUT}
														placeholder="Example: Feeds"
														onChange={this.onChange.bind(this, 'tag2')}
													/>
												</BoxBit>
											),
										}],
									}, {
										data: [{
											title: 'Tag Link 3',
											children: (
												<BoxBit unflex>
													<InputValidateBit
														type={InputValidateBit.TYPES.INPUT}
														placeholder="Example: 1212Sale"
														onChange={this.onChange.bind(this, 'tag3')}
													/>
												</BoxBit>
											),
										}],
									}, {
										data: [{
											title: 'Tag Link 4',
											children: (
												<BoxBit unflex>
													<InputValidateBit
														type={InputValidateBit.TYPES.INPUT}
														onChange={this.onChange.bind(this, 'tag4')}
													/>
												</BoxBit>
											),
										}],
									}, {
										data: [{
											title: 'Tag Link 5',
											children: (
												<BoxBit unflex>
													<InputValidateBit
														type={InputValidateBit.TYPES.INPUT}
														onChange={this.onChange.bind(this, 'tag5')}
													/>
												</BoxBit>
											),
										}],
									}, {
										data: [{
											title: '',
											children: (
												<ButtonBit
													title="Get URL"
													weight="medium"
													type={ ButtonBit.TYPES.SHAPES.PROGRESS }
													width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
													state={ ButtonBit.TYPES.STATES.NORMAL }
													onPress= {this.getUrl }
													style={Styles.button}
												/>
											),
										}],
									}]}
								/>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
