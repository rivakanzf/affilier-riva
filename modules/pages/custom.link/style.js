import StyleSheet from "coeur/libs/style.sheet"
import Sizes from 'coeur/constants/size'
import Colors from 'utils/constants/color'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		padding: Sizes.margin.default,
	},

	button: {
		maxWidth: 200,
	},

	page: {
		padding: Sizes.margin.default,
		maxWidth: 560,
	},

	copyIcon: {
		paddingLeft: 6,
	},
})
