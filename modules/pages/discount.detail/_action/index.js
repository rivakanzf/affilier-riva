import React from 'react'
import ConnectedStatefulModel from 'coeur/models/connected.stateful'
import ConnectHelper from 'coeur/helpers/connect'
import TimeHelper from 'coeur/helpers/time'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'

import BoxRowLego from 'modules/legos/box.row'

import Styles from './style'

export default ConnectHelper(
	class ActionPart extends ConnectedStatefulModel {
		
		static propTypes(PropTypes) {
			return {
				loading: PropTypes.bool,
				isChanged: PropTypes.bool,
				createdAt: PropTypes.date,
				publishedAt: PropTypes.date,
				expiredAt: PropTypes.date,

				onSave: PropTypes.func,
				style: PropTypes.style,
			}
		}

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: this.props.createdAt ? 'Save Changes' : 'Create Promo',
							children: (
								<ButtonBit
									weight="medium"
									title={this.props.createdAt ? 'Save Changes' : 'Create Promo'}
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={this.props.loading
										? ButtonBit.TYPES.STATES.LOADING
										: this.props.disabled || this.props.isChanged
								&& ButtonBit.TYPES.STATES.DISABLED
								|| ButtonBit.TYPES.STATES.NORMAL
									}
									onPress={this.props.onSave}
								/>
							),
							description: `Created at: ${ TimeHelper.format(this.props.createdAt, 'DD MMM YYYY HH:mm') }`,
						}],
					}]}
				/>
			)
		}

	}
)
