import React from 'react'
import StatefulModel from 'coeur/models/stateful'
import ConnectHelper from 'coeur/helpers/connect'

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import InputDateTimeBit from 'modules/bits/input.datetime';
import InputValidatedBit from 'modules/bits/input.validated';
import InputCheckboxBit from 'modules/bits/input.checkbox';

import BoxRowLego from 'modules/legos/box.row'

// import Styles from './style'

export default ConnectHelper(
	class DetailPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				title: PropTypes.string,
				publishedAt: PropTypes.date,
				expiredAt: PropTypes.date,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		onChange = (key, value) => {
			this.props.onChange &&
			this.props.onChange(key, value)
		}

		view() {
			return (
				<BoxRowLego
					title={this.props.id ? `Discount #P0-${this.props.id}` : 'General'}
					data={[{
						data: [{
							title: 'Discount Name',
							children: (
								<InputValidatedBit
									type={InputValidatedBit.TYPES.INPUT}
									placeholder="Input here"
									value={this.props.title}
									onChange={this.onChange.bind(this, 'title')}
								/>
							),
						}],
					}, {
						data: [{
							title: 'Periode Discount',
							children: (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										Start Date
									</GeomanistBit>
									<InputDateTimeBit unflex={false}
										title={''}
										date={this.props.publishedAt}
										onChange={this.onChange.bind(this, 'published_at')}
									/>
								</BoxBit>
							),
						}, {
							title: ' ',
							children: (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										End Date
									</GeomanistBit>
									<InputDateTimeBit unflex={false}
										title={''}
										date={this.props.expiredAt}
										onChange={this.onChange.bind(this, 'expired_at')}
									/>
								</BoxBit>
							),
						}],
					}]}
					style={this.props.style}
				/>
			)
		}
	}
)
