/* eslint-disable no-nested-ternary */
import React from 'react'
import ConnectedStatefulModel from 'coeur/models/connected.stateful'
import ConnectHelper from 'coeur/helpers/connect'
import AutheticationHelper from 'utils/helpers/authentication'
import UtilitiesContext from 'coeur/contexts/utilities'

import ItemService from 'app/services/item'

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import LoaderBit from 'modules/bits/loader'
import RadioBit from 'modules/bits/radio'
import TextBit from 'modules/bits/text'
import TextInputBit from 'modules/bits/text.input'
import TouchableBit from 'modules/bits/touchable'

import RowLego from 'modules/legos/row'

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'

import { debounce, isEqual } from 'lodash'

import Styles from './style'

export default ConnectHelper(
	class AddPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				// title: PropTypes.string,
				type: PropTypes.string,
				// onUpdate: PropTypes.func,
			}
		}

		static contexts = [ UtilitiesContext ]

		static stateToProps(state) {
			return {
				token: state.me.token,
				isManager: AutheticationHelper.isAuthorized(state.me.token, 'headstylist', 'stylist'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isAdding: false,
				search: null,
				selectedId: -1,
				data: [],
			})
		}

		componentDidMount() {
			if(this.props.type && this.state.search) {
				this.getData()
			}
		}

		componentDidUpdate(nP, nS) {
			if(!isEqual(nS.search, this.state.search)) {
				this.getData()
			}
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				ItemService.getAllItem({
					search: this.state.search,
					type: this.props.type,
				}, this.props.token)
					.then(res => {
						this.log('RES ===>>> ', res)
						this.setState({
							data: res,
							isLoading: false,
						})
					})
					.catch(() => {
						this.setState({
							data: [],
							isLoading: false,
						})
					})
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onSearch = debounce((e, val) => {
			this.setState({
				search: val,
			})
		}, 300)

		listRenderer = item => {
			return (
				<TouchableBit key={item.id} unflex row
					style={[Styles.item, this.state.selectedId === item.id && Styles.selected]}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
						<TextBit style={Styles.id}>[#{item.id}]</TextBit> { item.title || '-' }
					</GeomanistBit>
					<BoxBit/>
					<RadioBit dumb isActive={this.state.selectedId === item.id} style={Styles.radio}/>
				</TouchableBit>
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Add New Products"
					subheader={(
						<RowLego data={[{
							title: 'Product Title',
							children: (
								<TextInputBit defaultValue={this.state.search} placeholder="Input here..." onChange={this.onSearch} />
							),
						}, {
							blank: true,
						}]} style={Styles.header}/>
					)}
					loading={this.state.isLoading}
					disabled={this.state.selectedId === -1}
					confirm={'Add to promo'}
					onCancel={this.onClose}
					onConfirm={() => this.log('CONFIRM ADD PRODUCT')}
					// style={Styles.addModal}
					contentContainerStyle={Styles.content}
				>
					{this.state.isLoading ? (
						<BoxBit centering>
							<LoaderBit simple />
						</BoxBit>
					) : this.state.data.length ? this.state.data.map(this.listRenderer) : (
						<BoxBit centering>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								No item found
							</GeomanistBit>
						</BoxBit>
					)}
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
