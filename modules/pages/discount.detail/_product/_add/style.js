import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	header: {
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	item: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingRight: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		marginLeft: -Sizes.margin.default,
		marginRight: -Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	selected: {
		backgroundColor: Colors.grey.palette(2),
	},

	id: {
		color: Colors.primary,
	},

	note: {
		color: Colors.black.palette(2, .16),
	},

	radio: {
		marginRight: 8,
		marginLeft: 8,
	},

	content: {
		minHeight: 450,
	},

	empty: {
		height: 96,
	},

	addModal: {
		width: '50%',
		maxWidth: 800,
	}
})
