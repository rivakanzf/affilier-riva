/* eslint-disable no-nested-ternary */
import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import StatefulModel from 'coeur/models/stateful'

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import LoaderBit from 'modules/bits/loader'
import ScrollViewBit from 'modules/bits/scroll.view'
import ListLego from 'modules/legos/list'

import Styles from './styles'

export default ConnectHelper(
	class TablePart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				isLoading: PropTypes.bool,
				unflex: PropTypes.bool,
				headers: PropTypes.array,
				headerless: PropTypes.bool,
				bodyless: PropTypes.bool,
				footers: PropTypes.array,
				productsRows: PropTypes.array.isRequired,
				variantsRows: PropTypes.array.isRequired,
				headerKey: PropTypes.updater,
				lastBorder: PropTypes.bool,
				alternating: PropTypes.bool,
				style: PropTypes.style,
				contentStyle: PropTypes.style,
			}
		}

		static defaultProps = {
			lastBorder: true,
			alternating: false,
		}

		constructor(p) {
			super(p, {
				productRowLen: -1,
				variantRowLen: -1,
			})
		}

		getFlex(width) {
			return width > 0 ? `${width * 100}%` : 'auto'
		}

		productRowRenderer = ({
			key,
			data,
			children,
			disabled,
			onPress,
			style,
		}, index) => {
			return (
				<ListLego index={ index } key={ key || index } onPress={ onPress } disabled={ disabled } style={[Styles.body, Styles.mB, style]} borderless={this.props.lastBorder ? false : index === this.state.productRowLen } alternating={ this.props.alternating }>
					<BoxBit unflex row>
						{ data.map((row, i) => {
							return (
								<BoxBit key={i} style={[Styles.row, {
									flexBasis: this.getFlex(this.props.headers[i].width),
								}]}>
									{ row.children ? row.children : (
										<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} align={this.props.headers[i].align} style={row.style}>
											{row.title}
										</GeomanistBit>
									)}
								</BoxBit>
							)
						})}
					</BoxBit>
					{/* { children } */}
					{ this.props.variantsRows[index].map(this.variantRowRenderer) }
				</ListLego>
			)
		}

		variantRowRenderer = ({
			key,
			data,
			children,
			disabled,
			onPress,
			style,
		}, index) => {
			return (
				<ListLego index={ index } key={ key || index } onPress={ onPress } disabled={ disabled } style={[Styles.variantBody, style]} borderless alternating={ this.props.alternating }>
					<BoxBit unflex row>
						{ data.map((row, i) => {
							return (
								<BoxBit key={i} style={[Styles.row, {
									flexBasis: this.getFlex(this.props.headers[i].width),
								}]}>
									{ row.children ? row.children : (
										<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} align={this.props.headers[i].align} style={row.style}>
											{row.title}
										</GeomanistBit>
									)}
								</BoxBit>
							)
						})}
					</BoxBit>
					{ children }
				</ListLego>
			)
		}

		view() {
			this.state.variantRowLen = this.props.variantsRows.length - 1
			this.state.productRowLen = this.props.productsRows.length - 1

			return (
				<BoxBit unflex={this.props.unflex} style={this.props.style}>
					{ this.props.headerless ? false : (
						<ListLego key={this.props.headerKey} isHeader row style={Styles.header}>
							{ this.props.headers.map((header, i) => {
								return (
									<BoxBit key={i} style={[Styles.row, {
										flexBasis: this.getFlex(header.width),
									}]}>
										{ header.children ? header.children : (
											<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} align={this.props.headers[i].align} weight="semibold">
												{ header.title }
											</GeomanistBit>
										) }
									</BoxBit>
								)
							}) }
						</ListLego>
					) }
					<ScrollViewBit unflex={ this.props.unflex } style={this.props.contentStyle}>
						{ this.props.isLoading ? (
							<ListLego index={1} unflex={false}>
								<BoxBit centering>
									<LoaderBit simple/>
								</BoxBit>
							</ListLego>
						) : !this.props.variantsRows.length ? !this.props.bodyless ? (
							<ListLego index={1} unflex={false} style={Styles.empty}>
								<BoxBit centering>
									<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
										No Data
									</GeomanistBit>
								</BoxBit>
							</ListLego>
						) : false : this.props.productsRows.map(this.productRowRenderer)}
					</ScrollViewBit>
					{ this.props.footers && (
						<ListLego row isHeader style={Styles.footer}>
							{ this.props.footers.map((footer, i) => {
								return (
									<BoxBit key={i} style={[Styles.row, {
										flexBasis: this.getFlex(this.props.headers[i].width),
									}]}>
										{ footer.children ? footer.children : (
											<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} align={this.props.headers[i].align} weight="semibold">
												{ footer.title }
											</GeomanistBit>
										) }
									</BoxBit>
								)
							}) }
						</ListLego>
					) }
				</BoxBit>
			)
		}
	}
)
