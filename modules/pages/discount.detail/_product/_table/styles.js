import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	header: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingRight: 8,
		paddingLeft: 8,
	},

	mB: {
		// marginTop: 8,
		marginBottom: 16,
	},

	footer: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingRight: 8,
		paddingLeft: 8,
	},

	body: {
		paddingLeft: 8,
		paddingRight: 8,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	variantBody: {
		paddingTop: 4,
		paddingBottom: 4,
		paddingLeft: 0,
		paddingRight: 0,
	},

	row: {
		justifyContent: 'center',
		wordBreak: 'break-all',
		paddingLeft: 8,
		paddingRight: 8,
		overflow: 'hidden',
	},

	empty: {
		backgroundColor: Colors.solid.grey.palette(4),
	},

	note: {
		color: Colors.black.palette(2, .6),
	},
})
