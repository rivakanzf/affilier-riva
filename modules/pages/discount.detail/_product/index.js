import React from 'react'
import ConnectedStatefulModel from 'coeur/models/connected.stateful'
import ConnectHelper from 'coeur/helpers/connect'
import FormatHelper from 'coeur/helpers/format'

import PageContext from 'coeur/contexts/page'
import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box'
import IconBit from 'modules/bits/icon'
import BoxImageBit from 'modules/bits/box.image'
import SwitchBit from 'modules/bits/switch'
import ButtonBit from 'modules/bits/button'
import CheckboxBit from 'modules/bits/checkbox'
import GeomanistBit from 'modules/bits/geomanist'
import TouchableBit from 'modules/bits/touchable'
import InputCurrencyBit from 'modules/bits/input.currency'
import InputValidatedBit from 'modules/bits/input.validated'

import BoxLego from 'modules/legos/box'
import TableLego from 'modules/legos/table'

import AddPart from './_add'
import TablePart from './_table'

import { isEmpty, orderBy, without, isEqual } from 'lodash'

import Styles from './style'

const data = {
	id: 1,
	title: 'Diskon Ramune',
	createdAt: '2021-08-18T09:58:23.056Z',
	publishedAt: '2021-08-16T16:59:00.000Z',
	expiredAt: '2021-08-23T16:59:00.000Z',
	metadata: {},
	description: 'Diskon Produk Ramune',
	products: [{
		brand: 'RAMUNE',
		title: 'Hara Cropped Sweater',
		id: 12348,
		variants: [{
			assets: [{
				id: 62092,
				url: 'production/variant/18493/mj3phowcg4ykcjhxwyxf.jpg',
			}, {
				id: 62093,
				url: 'production/variant/18493/hal2fquaaljacttgjknq.jpg',
			}, {
				id: 46856,
				url: 'production/variant/18493/fyvcp6rpgbqazc4vmrf5.jpg',
			}, {
				id: 46857,
				url: 'production/variant/18493/deuszv1wijfrdqkd8kog.jpg',
			}, {
				id: 46858,
				url: 'production/variant/18493/tuf69hyjr8acbaeel6ds.jpg',
			}],
			colors: 'Nude',
			id: 18493,
			retailPrice: 275000,
			product_id: 12348,
			quantity: 5,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 62094,
				url: 'production/variant/18494/gmaw3rxdntrv77znxfrc.jpg',
			}, {
				id: 62095,
				url: 'production/variant/18494/xr5jqvxctdsbofsjuaf0.jpg',
			}, {
				id: 47086,
				url: 'production/variant/18494/setempbnq6yoirmsbabk.jpg',
			}, {
				id: 47087,
				url: 'production/variant/18494/wbrmiq2lqqoigcugvac8.jpg',
			}, {
				id: 47088,
				url: 'production/variant/18494/pdjx2puotiwau8zhptny.jpg',
			}],
			colors: 'Nude',
			id: 18494,
			retailPrice: 275000,
			product_id: 12348,
			quantity: 3,
			size: {
				id: 8,
				title: 'M',
			},
		}],
	}, {
		brand: 'RAMUNE',
		title: 'Mia Loose Dress',
		id: 9763,
		variants: [{
			assets: [{
				id: 30831,
				url: 'production/variant/13123/gmbr0tbzhzn54k9s38hi.jpg',
			}, {
				id: 30832,
				url: 'production/variant/13123/hqxccfz29u4xjfhgsr9r.jpg',
			}, {
				id: 30833,
				url: 'production/variant/13123/lox80pldq1ugabq7cwfl.jpg',
			}, {
				id: 30834,
				url: 'production/variant/13123/dwehrzkghqcgdly6lm98.jpg',
			}, {
				id: 30835,
				url: 'production/variant/13123/k1zvl3fhbxno3dr6o2fx.jpg',
			}],
			colors: 'white',
			id: 13123,
			retailPrice: 339000,
			quantity: 1,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 30816,
				url: 'production/variant/13120/kzpsxalilsl5wboeaiun.jpg',
			}, {
				id: 30817,
				url: 'production/variant/13120/l9ussqjpbfwxzo5ly2g5.jpg',
			}, {
				id: 30818,
				url: 'production/variant/13120/gn35e6daqg40qp5vao98.jpg',
			}, {
				id: 30819,
				url: 'production/variant/13120/sog6k6ctmen3qdefvikw.jpg',
			}, {
				id: 30820,
				url: 'production/variant/13120/mhlb8eqmlqby1xkkmqvh.jpg',
			}],
			colors: 'Black',
			id: 13120,
			retailPrice: 339000,
			quantity: 1,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 30821,
				url: 'production/variant/13121/g1wjunsrbunxv6y0myvf.jpg', 
			}, {
				id: 30822,
				url: 'production/variant/13121/k5bd0r5ecfyvy99uudfj.jpg',
			}, {
				id: 30823,
				url: 'production/variant/13121/shnfrnrajrpdkenocr44.jpg',
			}, {
				id: 30824,
				url: 'production/variant/13121/rlvpuftbfm5y8iablihf.jpg',
			}, {
				id: 30825,
				url: 'production/variant/13121/mdcotvgm8wxtogshxdg8.jpg',
			}],
			colors: 'Black',
			id: 13121,
			retailPrice: 339000,
			quantity: 0,
			size: {
				id: 8,
				title: 'M',
			},
		}, {
			assets: [{
				id: 30836,
				url: 'production/variant/13124/oxxqp3acszubypbpb0zb.jpg',
			}, {
				id: 30837,
				url: 'production/variant/13124/fh7u4qyyf5qksqoclixs.jpg',
			}, {
				id: 30838,
				url: 'production/variant/13124/mlacsaq2rsfsmq9ixchl.jpg',
			}, {
				id: 30839,
				url: 'production/variant/13124/rkdmvhno0oatjmhwzwsq.jpg',
			}, {
				id: 30840,
				url: 'production/variant/13124/yuudcrvdqw0hlwa4dg5u.jpg',
			}],
			colors: 'White',
			id: 13124,
			retailPrice: 339000,
			quantity: 1,
			size: {
				id: 8,
				title: 'M',
			},
		}, {
			assets: [{
				id: 30841,
				url: 'production/variant/13125/r6otpd6nqyowd9tvxenh.jpg',
			}, {
				id: 30842,
				url: 'production/variant/13125/fejuhcblb0etmthxxr2x.jpg',
			}, {
				id: 30843,
				url: 'production/variant/13125/sn5ht5n1adhhj2dsyx2u.jpg',
			}, {
				id: 30844,
				url: 'production/variant/13125/xaidtzzk6njoyb0rp9f5.jpg',
			}, {
				id: 30845,
				url: 'production/variant/13125/xiomvi78acvovi72rowu.jpg',
			}],
			colors: 'White',
			id: 13125,
			retailPrice: 339000,
			quantity: 0,
			size: {
				id: 9,
				title: 'L',
			},
		}, {
			assets: [{
				id: 30826,
				url: 'production/variant/13122/ywzbvady3vxj2jy4ywg3.jpg',
			}, {
				id: 30827,
				url: 'production/variant/13122/ajfjn13xt7vf3ysnpd1u.jpg',
			}, {
				id: 30828,
				url: 'production/variant/13122/q0nz5j1lyol4t61gbjea.jpg',
			}, {
				id: 30829,
				url: 'production/variant/13122/zv6wh5t5z8akx17zxbrm.jpg',
			}, {
				id: 30830,
				url: 'production/variant/13122/lgmlei5fa3je5qzywqzu.jpg',
			}],
			colors: 'Black',
			id: 13122,
			retailPrice: 339000,
			quantity: 0,
			size: {
				id: 9,
				title: 'L',
			},
		}],
	}],
}

export default ConnectHelper(
	class ProductsPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				productId: PropTypes.id,
				products: PropTypes.array,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
				userId: state.me.id,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				updater: 0,
				isLoading: true,
				isChanged: false,
				isSaving: false,
				selectedProductIds: [],
				selectedVariantIds: [],
				variants: [],

				modified: {},
			})
		}

		dataVariant = [{
			discount_price: null,
			discount_percent: null,
			have_promo: true,
		}]

		headerMainTable = [{
			title: '',
			width: .5,
			align: 'center',
		}, {
			title: 'Product Name',
			width: 3,
			align: 'center',
		}, {
			title: 'Retail Price',
			width: 1.5,
			align: 'center',
		}, {
			title: 'Discount Price',
			width: 2,
			align: 'center',
		}, {
			title: 'Discount Percent',
			width: 2.2,
			align: 'center',
		}, {
			title: 'Quantity',
			width: .8,
			align: 'center',
		}, {
			title: 'Add to Discount',
			width: 1,
			align: 'center',
		}, {
			title: '',
			width: 1,
			align: 'center',
		}]

		headerToggleTable = [{
			title: '',
			width: .5,
		}, {
			title: '',
			width: 3,
		}, {
			title: '',
			width: 1.5,
		}, {
			title: '',
			width: 2,
		}, {
			title: '',
			width: 2.2,
		}, {
			title: '',
			width: .8,
		}, {
			title: '',
			width: 1,
		}, {
			title: '',
			width: 1,
		}]

		// onChangeShallow = (ids, key, value) => {
		// 	this.setState({
		// 		modified: {

		// 		}
		// 	})
		// }

		onAdd = () => {
			this.props.utilities.alert.modal({
				component: (
					<AddPart
						id={this.props.id}
						type="PRODUCT"
					/>
				),
			})
		}

		onSelect = id => {
			if (this.state.selectedProductIds.indexOf(id) > -1) {
				this.setState({
					selectedProductIds: without(this.state.selectedProductIds, id),
				})
			} else {
				this.setState({
					selectedProductIds: [...this.state.selectedProductIds, id],
				})
			}
		}

		onSelectAll = id => {
			const variantSelectedIds = [...this.props.products.map(p => p.variants.map(v => v.id))]
			this.log(variantSelectedIds)

			if (this.state.selectedProductIds.length === id.length) {
				this.setState({
					selectedProductIds: [],
					selectedVariantIds: variantSelectedIds,
				})
			} else {
				this.setState({
					selectedProductIds: id,
					selectedVariantIds: variantSelectedIds,
				})
			}

			this.log(this.state.selectedProductIds)
		}

		onChangeDiscount = (ids, retailPrice, key1, key2, val, isValid) => {
			let val2 = 0
			if(isValid) {
				if(key1 === 'discount_price') {
					val2 = Math.round((retailPrice - val) / retailPrice * 100)
				} else {
					val2 = Math.round(retailPrice - (retailPrice * val / 100))
				}
				this.setState({
					modified: {
						...this.state.modified,
						[ids]: {
							[key1]: val,
							[key2]: val2,
						},
					},
				})
				this.forceUpdate()
			}

			// this.log(this.state.modified)
		}

		productRowRenderer = product => {
			return {
				data: [{
					children: (
						<CheckboxBit dumb
							isActive={ this.state.selectedProductIds.indexOf(product.id) > -1 }
							onPress={ this.onSelect.bind(this, product.id)}
							// onPress={() => this.log(this.state.selectedProductIds.indexOf(product.id) > -1)}
						/>
					),
				}, {
					children: (
						<BoxBit row>
							<BoxImageBit unflex
								source={product.variants[0].assets[0]}
								transform={{crop: 'fit'}}
								broken={!product.variants[0].assets[0]}
								style={Styles.image}
							/>
							<BoxBit style={Styles.titleBox}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									{product.brand}
								</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									{product.title}
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					),
				}, {}, {}, {}, {}, {}, {
					children: (
						<BoxBit row centering>
							<TouchableBit unflex onPress={() => this.log('REMOVE PRODUCT')}>
								<IconBit name="trash"/>
							</TouchableBit>
						</BoxBit>
					),
				}],
			}
		}

		variantRowRenderer = variant => {
			const quantity = variant.quantity === 0
			return {
				id: variant.id,
				data: [{}, {
					children: (
						<BoxBit style={Styles.titleBox}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={quantity && Styles.red || {}}>
								{variant.colors}-{variant.size.title}
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					// Retail Price
					children: (
						<GeomanistBit align="center" type={GeomanistBit.TYPES.PARAGRAPH_3} style={quantity && Styles.red || {}}>
							{ FormatHelper.currencyFormat(variant.retailPrice) }
						</GeomanistBit>
					),
				}, {
					// Discount Price
					children: (
						<InputCurrencyBit
							title="IDR"
							value={variant.discountPrice}
							onChange={this.onChangeDiscount.bind(this, variant.id, variant.retailPrice, 'discount_price', 'discount_percent')}
							style={Styles.center}
						/>
					),
				}, {
					// Discount Percentage
					children: (
						<InputCurrencyBit
							title="%"
							value={variant.discountPercent}
							onChange={this.onChangeDiscount.bind(this, variant.id, variant.retailPrice, 'discount_percent', 'discount_price')}
							style={Styles.center}
						/>
					),
				}, {
					// Quantity
					children: (
						<GeomanistBit align="center" type={GeomanistBit.TYPES.PARAGRAPH_3} style={quantity && Styles.red || {}}>
							{variant.quantity}
						</GeomanistBit>
					),
				}, {
					// Add to Discount
					children: (
						<BoxBit centering>
							<SwitchBit
								style={Styles.switch}
								value={variant.included_in_promo}
							/>
						</BoxBit>
					),
				}, {}],
			}
		}

		toggleTableRenderer = productIds => {
			return (
				<TableLego
					headers={this.headerToggleTable}
					rows={[{
						data: [{
							children: (
								<CheckboxBit dumb
									isActive={ !productIds.length ? false : this.state.selectedProductIds.length === productIds.length }
									disabled={ !productIds.length }
									onPress={ this.onSelectAll.bind(this, productIds) }
								/>
							),
						}, {
							title: 'Select All',
						}, {}, {
							title: 'Discount Price',
							children: (
								<InputCurrencyBit
									// value={this.state.modified.discount_price}
									placeholder="Discount Price"
									style={Styles.center}
								/>
							),
						}, {
							title: 'Discount Percent',
							children: (
								<InputCurrencyBit
									title="%"
									// value={this.state.modified.discount_percent}
									placeholder="Discount Percent"
									style={Styles.center}
								/>
							),
						}, {}, {
							// Hapus Product
							children: (
								<ButtonBit
									title="Delete"
									weight="medium"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									size={ButtonBit.TYPES.SIZES.SMALL}
									state={ !!this.state.selectedProductIds.length
										? ButtonBit.TYPES.STATES.NORMAL
										: ButtonBit.TYPES.STATES.DISABLED }
									onPress={() => this.log('DELETE ALL PRODUCTS')}
								/>
							),
						}, {
							children: (
								<ButtonBit
									title="Apply"
									weight="medium"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									size={ButtonBit.TYPES.SIZES.SMALL}
									state={ !!this.state.selectedProductIds.length
										? ButtonBit.TYPES.STATES.NORMAL
										: ButtonBit.TYPES.STATES.DISABLED }
									onPress={() => this.log('APPLY CHANGES TO ALL')}
								/>
							),
						}],
					}]}
				/>
			)
		}
		
		mainTableRenderer = () => {
			const variant = [...this.props.products.map(product => product.variants.map(v => v))]

			return (
				// <TableLego
				// 	headers={this.headerMainTable}
				// 	rows={productVariant.map(this.variantRowRenderer)}
				// 	contentStyle={Styles.list}
				// />
				<TablePart
					headers={this.headerMainTable}
					productsRows={this.props.products.map(this.productRowRenderer)}
					variantsRows={variant.map(v => v.map(this.variantRowRenderer))}
					contentStyle={Styles.list}
				/>
			)
		}

		footerRenderer = () => {
			return (
				<BoxBit unflex row style={Styles.footer}>
					<BoxBit/>
					<ButtonBit
						title="Save"
						weight="medium"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						state={ ButtonBit.TYPES.STATES.DISABLED }
						size={ ButtonBit.TYPES.SIZES.SMALL }
					/>
				</BoxBit>
			)
		}

		view() {
			const productIds = [ ...this.props.products.map(p => p.id)]
			// this.log(this.props.products)
			// this.log(productIds)
			this.log(this.state.selectedProductIds)
			this.log('STATE ===>>> ', this.state)
			return (
				<BoxLego
					title="Products"
					header={(
						<BoxBit row unflex>
							<ButtonBit
								title="Add Products"
								weight="medium"
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								state={ButtonBit.TYPES.STATES.NORMAL}
								size={ButtonBit.TYPES.SIZES.SMALL}
								onPress={ this.onAdd }
							/>
						</BoxBit>
					)}
					style={Styles.container}
					contentContainerStyle={Styles.padder}>
					{ this.toggleTableRenderer(productIds) }
					{ this.mainTableRenderer() }
					{ this.footerRenderer() }
				</BoxLego>
			)
		}
	}
)
