import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		marginTop: 16,
	},

	padder: {
		margin: 0,
	},

	list: {
		maxHeight: 315 * 1.5,
		minHeight: 105 * 1.25,
	},

	image: {
		width: 40,
		height: 40 * 4 / 3,
	},

	center: {
		alignItems: 'center',
	},

	titleBox: {
		paddingLeft: 8,
		justifyContent: 'center',
	},

	footer: {
		paddingTop: 9,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	editBtn: {
		marginRight: 6,
	},

	disabled: {
		backgroundColor: Colors.grey.palette(2, .4),
	},

	red: {
		color: Colors.red.palette(2),
	},
})
