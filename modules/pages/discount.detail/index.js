/* eslint-disable no-nested-ternary */
import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'

import BoxBit from 'modules/bits/box'

import PageAuthorizedComponent from 'modules/components/page.authorized'
import HeaderPageComponent from 'modules/components/header.page'

import DetailPart from './_detail'
import ActionPart from './_action'
import ProductPart from './_product'

import Styles from './style'

const data = {
	id: 1,
	title: 'Diskon Ramune',
	createdAt: '2021-08-18T09:58:23.056Z',
	publishedAt: '2021-08-16T16:59:00.000Z',
	expiredAt: '2021-08-23T16:59:00.000Z',
	metadata: {},
	description: 'Diskon Produk Ramune',
	products: [{
		brand: 'RAMUNE',
		title: 'Hara Cropped Sweater',
		id: 12348,
		variants: [{
			assets: [{
				id: 62092,
				url: 'production/variant/18493/mj3phowcg4ykcjhxwyxf.jpg',
			}, {
				id: 62093,
				url: 'production/variant/18493/hal2fquaaljacttgjknq.jpg',
			}, {
				id: 46856,
				url: 'production/variant/18493/fyvcp6rpgbqazc4vmrf5.jpg',
			}, {
				id: 46857,
				url: 'production/variant/18493/deuszv1wijfrdqkd8kog.jpg',
			}, {
				id: 46858,
				url: 'production/variant/18493/tuf69hyjr8acbaeel6ds.jpg',
			}],
			colors: 'Nude',
			id: 18493,
			retailPrice: 275000,
			discountPrice: 0,
			discountPercent: 0,
			product_id: 12348,
			quantity: 5,
			included_in_promo: true,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 62094,
				url: 'production/variant/18494/gmaw3rxdntrv77znxfrc.jpg',
			}, {
				id: 62095,
				url: 'production/variant/18494/xr5jqvxctdsbofsjuaf0.jpg',
			}, {
				id: 47086,
				url: 'production/variant/18494/setempbnq6yoirmsbabk.jpg',
			}, {
				id: 47087,
				url: 'production/variant/18494/wbrmiq2lqqoigcugvac8.jpg',
			}, {
				id: 47088,
				url: 'production/variant/18494/pdjx2puotiwau8zhptny.jpg',
			}],
			colors: 'Nude',
			id: 18494,
			retailPrice: 275000,
			discountPrice: 0,
			discountPercent: 0,
			product_id: 12348,
			quantity: 3,
			included_in_promo: true,
			size: {
				id: 8,
				title: 'M',
			},
		}],
	}, {
		brand: 'RAMUNE',
		title: 'Mia Loose Dress',
		id: 9763,
		variants: [{
			assets: [{
				id: 30831,
				url: 'production/variant/13123/gmbr0tbzhzn54k9s38hi.jpg',
			}, {
				id: 30832,
				url: 'production/variant/13123/hqxccfz29u4xjfhgsr9r.jpg',
			}, {
				id: 30833,
				url: 'production/variant/13123/lox80pldq1ugabq7cwfl.jpg',
			}, {
				id: 30834,
				url: 'production/variant/13123/dwehrzkghqcgdly6lm98.jpg',
			}, {
				id: 30835,
				url: 'production/variant/13123/k1zvl3fhbxno3dr6o2fx.jpg',
			}],
			colors: 'White',
			id: 13123,
			retailPrice: 339000,
			discountPrice: 0,
			discountPercent: 0,
			quantity: 1,
			included_in_promo: true,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 30816,
				url: 'production/variant/13120/kzpsxalilsl5wboeaiun.jpg',
			}, {
				id: 30817,
				url: 'production/variant/13120/l9ussqjpbfwxzo5ly2g5.jpg',
			}, {
				id: 30818,
				url: 'production/variant/13120/gn35e6daqg40qp5vao98.jpg',
			}, {
				id: 30819,
				url: 'production/variant/13120/sog6k6ctmen3qdefvikw.jpg',
			}, {
				id: 30820,
				url: 'production/variant/13120/mhlb8eqmlqby1xkkmqvh.jpg',
			}],
			colors: 'Black',
			id: 13120,
			retailPrice: 339000,
			discountPrice: 0,
			discountPercent: 0,
			quantity: 1,
			included_in_promo: true,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 30821,
				url: 'production/variant/13121/g1wjunsrbunxv6y0myvf.jpg', 
			}, {
				id: 30822,
				url: 'production/variant/13121/k5bd0r5ecfyvy99uudfj.jpg',
			}, {
				id: 30823,
				url: 'production/variant/13121/shnfrnrajrpdkenocr44.jpg',
			}, {
				id: 30824,
				url: 'production/variant/13121/rlvpuftbfm5y8iablihf.jpg',
			}, {
				id: 30825,
				url: 'production/variant/13121/mdcotvgm8wxtogshxdg8.jpg',
			}],
			colors: 'Black',
			id: 13121,
			retailPrice: 339000,
			discountPrice: 0,
			discountPercent: 0,
			quantity: 0,
			included_in_promo: true,
			size: {
				id: 8,
				title: 'M',
			},
		}, {
			assets: [{
				id: 30836,
				url: 'production/variant/13124/oxxqp3acszubypbpb0zb.jpg',
			}, {
				id: 30837,
				url: 'production/variant/13124/fh7u4qyyf5qksqoclixs.jpg',
			}, {
				id: 30838,
				url: 'production/variant/13124/mlacsaq2rsfsmq9ixchl.jpg',
			}, {
				id: 30839,
				url: 'production/variant/13124/rkdmvhno0oatjmhwzwsq.jpg',
			}, {
				id: 30840,
				url: 'production/variant/13124/yuudcrvdqw0hlwa4dg5u.jpg',
			}],
			colors: 'White',
			id: 13124,
			retailPrice: 339000,
			discountPrice: 0,
			discountPercent: 0,
			quantity: 1,
			included_in_promo: true,
			size: {
				id: 8,
				title: 'M',
			},
		}, {
			assets: [{
				id: 30841,
				url: 'production/variant/13125/r6otpd6nqyowd9tvxenh.jpg',
			}, {
				id: 30842,
				url: 'production/variant/13125/fejuhcblb0etmthxxr2x.jpg',
			}, {
				id: 30843,
				url: 'production/variant/13125/sn5ht5n1adhhj2dsyx2u.jpg',
			}, {
				id: 30844,
				url: 'production/variant/13125/xaidtzzk6njoyb0rp9f5.jpg',
			}, {
				id: 30845,
				url: 'production/variant/13125/xiomvi78acvovi72rowu.jpg',
			}],
			colors: 'White',
			id: 13125,
			retailPrice: 339000,
			discountPrice: 0,
			discountPercent: 0,
			quantity: 0,
			included_in_promo: true,
			size: {
				id: 9,
				title: 'L',
			},
		}, {
			assets: [{
				id: 30826,
				url: 'production/variant/13122/ywzbvady3vxj2jy4ywg3.jpg',
			}, {
				id: 30827,
				url: 'production/variant/13122/ajfjn13xt7vf3ysnpd1u.jpg',
			}, {
				id: 30828,
				url: 'production/variant/13122/q0nz5j1lyol4t61gbjea.jpg',
			}, {
				id: 30829,
				url: 'production/variant/13122/zv6wh5t5z8akx17zxbrm.jpg',
			}, {
				id: 30830,
				url: 'production/variant/13122/lgmlei5fa3je5qzywqzu.jpg',
			}],
			colors: 'Black',
			id: 13122,
			retailPrice: 339000,
			discountPrice: 0,
			discountPercent: 0,
			quantity: 0,
			included_in_promo: true,
			size: {
				id: 9,
				title: 'L',
			},
		}],
	}],
}

export default ConnectHelper(
	class DiscountDetailPage extends PageModel {
		
		static routeName = 'discount.detail'

		constructor(p) {
			console.log(p)
			super(p, {
				isLoading: true,
				isChanged: false,
				token: undefined,
				data: {},
				changes: {},
			}, [])
		}

		isNumeric(num) {
			return /^-{0,1}\d+$/.test(num)
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData = () => {
			if(this.props.id) {
				this.setState({
					isLoading: true,
				}, () => {
					this.setState({
						data: data,
						isLoading: false,
					})
				})
			} else {
				// Create New Promo
				this.setState({
					isLoading: false,
				})
			}
		}

		onChange = (key, val) => {
			this.state.changes[key] = this.isNumeric(val) ? parseInt(val, 10) : val
		}

		onNavigateToDiscounts = () => {
			this.navigator.navigate('discounts')

			if(!this.state.isChanged) {
				this.setState({
					isChange: true,
				})
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={'marketing'}
					onAuthorized={ this.onAuthorized }
					paths={[{
						title: 'Discounts',
						onPress: this.onNavigateToDiscounts,
					}, {
						title: this.props.id
							? this.state.isLoading
								? 'Loading...'
								: this.state.data.title
							: 'New Discount',
					}]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title={this.props.id ? 'Discount Detail' : 'Add New Discount'}
							/>
							<BoxBit unflex row>
								<BoxBit style={Styles.padder}>
									<DetailPart
										key={ `detail_${this.state.isLoading}` }
										id={ this.props.id }
										title={ this.state.data.title }
										publishedAt={ this.state.data.publishedAt }
										expiredAt={ this.state.data.expiredAt }
										onChange={ this.onChange }
										style={ Styles.box }
									/>
								</BoxBit>
								<BoxBit>
									<ActionPart
										createdAt={ this.state.data.createdAt }
										publishedAt={ this.state.data.publishedAt }
										expiredAt={ this.state.data.expiredAt }
										isChanged={ this.state.isChanged }
										loading={ this.state.isLoading }
										style={ Styles.box}
									/>
								</BoxBit>
							</BoxBit>
							<ProductPart
								id={this.state.data.id}
								products={this.state.data.products}
							/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
