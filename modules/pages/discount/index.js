/* eslint-disable no-nested-ternary */
import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'
import TimeHelper from 'coeur/helpers/time'
import Colors from 'coeur/constants/color'


import BoxBit from 'modules/bits/box'
import SelectionBit from 'modules/bits/selection'
import GeomanistBit from 'modules/bits/geomanist'
import TouchableBit from 'modules/bits/touchable'
import IconBit from 'modules/bits/icon'
import BoxImageBit from 'modules/bits/box.image'

import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import TableLego from 'modules/legos/table'

import Styles from './style'

// DUMMY DATA
const data = [{
	id: 1,
	title: 'Diskon Ramune',
	createdAt: '2021-08-18T09:58:23.056Z',
	publishedAt: '2021-08-16T16:59:00.000Z',
	expiredAt: '2021-08-23T16:59:00.000Z',
	metadata: {},
	description: 'Diskon Produk Ramune',
	products: [{
		brand: 'RAMUNE',
		title: 'Hara Cropped Sweater',
		id: 12348,
		variants: [{
			assets: [{
				id: 62092,
				url: 'production/variant/18493/mj3phowcg4ykcjhxwyxf.jpg',
			}, {
				id: 62093,
				url: 'production/variant/18493/hal2fquaaljacttgjknq.jpg',
			}, {
				id: 46856,
				url: 'production/variant/18493/fyvcp6rpgbqazc4vmrf5.jpg',
			}, {
				id: 46857,
				url: 'production/variant/18493/deuszv1wijfrdqkd8kog.jpg',
			}, {
				id: 46858,
				url: 'production/variant/18493/tuf69hyjr8acbaeel6ds.jpg',
			}],
			colors: [{
				hex: '#E3BC9A',
				image: null,
				title: 'Nude',
			}],
			id: 18493,
			retailPrice: 275000,
			product_id: 12348,
			quantity: 5,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 62094,
				url: 'production/variant/18494/gmaw3rxdntrv77znxfrc.jpg',
			}, {
				id: 62095,
				url: 'production/variant/18494/xr5jqvxctdsbofsjuaf0.jpg',
			}, {
				id: 47086,
				url: 'production/variant/18494/setempbnq6yoirmsbabk.jpg',
			}, {
				id: 47087,
				url: 'production/variant/18494/wbrmiq2lqqoigcugvac8.jpg',
			}, {
				id: 47088,
				url: 'production/variant/18494/pdjx2puotiwau8zhptny.jpg',
			}],
			colors: [{
				hex: '#E3BC9A',
				image: null,
				title: 'Nude',
			}],
			id: 18494,
			retailPrice: 275000,
			product_id: 12348,
			quantity: 3,
			size: {
				id: 8,
				title: 'M',
			},
		}],
	}, {
		brand: 'RAMUNE',
		title: 'Mia Loose Dress',
		id: 9763,
		variants: [{
			assets: [{
				id: 30831,
				url: 'production/variant/13123/gmbr0tbzhzn54k9s38hi.jpg',
			}, {
				id: 30832,
				url: 'production/variant/13123/hqxccfz29u4xjfhgsr9r.jpg',
			}, {
				id: 30833,
				url: 'production/variant/13123/lox80pldq1ugabq7cwfl.jpg',
			}, {
				id: 30834,
				url: 'production/variant/13123/dwehrzkghqcgdly6lm98.jpg',
			}, {
				id: 30835,
				url: 'production/variant/13123/k1zvl3fhbxno3dr6o2fx.jpg',
			}],
			colors: [{
				hex: '#F8F8F8',
				image: '//question/colour-11-white.jpg',
				title: 'white',
			}],
			id: 13123,
			retailPrice: 339000,
			quantity: 1,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 30816,
				url: 'production/variant/13120/kzpsxalilsl5wboeaiun.jpg',
			}, {
				id: 30817,
				url: 'production/variant/13120/l9ussqjpbfwxzo5ly2g5.jpg',
			}, {
				id: 30818,
				url: 'production/variant/13120/gn35e6daqg40qp5vao98.jpg',
			}, {
				id: 30819,
				url: 'production/variant/13120/sog6k6ctmen3qdefvikw.jpg',
			}, {
				id: 30820,
				url: 'production/variant/13120/mhlb8eqmlqby1xkkmqvh.jpg',
			}],
			colors: [{
				hex: '#000000',
				image: '//question/colour-13-black.jpg',
				title: 'black',
			}],
			id: 13120,
			retailPrice: 339000,
			quantity: 1,
			size: {
				id: 7,
				title: 'S',
			},
		}, {
			assets: [{
				id: 30821,
				url: 'production/variant/13121/g1wjunsrbunxv6y0myvf.jpg', 
			}, {
				id: 30822,
				url: 'production/variant/13121/k5bd0r5ecfyvy99uudfj.jpg',
			}, {
				id: 30823,
				url: 'production/variant/13121/shnfrnrajrpdkenocr44.jpg',
			}, {
				id: 30824,
				url: 'production/variant/13121/rlvpuftbfm5y8iablihf.jpg',
			}, {
				id: 30825,
				url: 'production/variant/13121/mdcotvgm8wxtogshxdg8.jpg',
			}],
			colors: [{
				hex: '#000000',
				image: '//question/colour-13-black.jpg',
				title: 'black',
			}],
			id: 13121,
			retailPrice: 339000,
			quantity: 0,
			size: {
				id: 8,
				title: 'M',
			},
		}, {
			assets: [{
				id: 30836,
				url: 'production/variant/13124/oxxqp3acszubypbpb0zb.jpg',
			}, {
				id: 30837,
				url: 'production/variant/13124/fh7u4qyyf5qksqoclixs.jpg',
			}, {
				id: 30838,
				url: 'production/variant/13124/mlacsaq2rsfsmq9ixchl.jpg',
			}, {
				id: 30839,
				url: 'production/variant/13124/rkdmvhno0oatjmhwzwsq.jpg',
			}, {
				id: 30840,
				url: 'production/variant/13124/yuudcrvdqw0hlwa4dg5u.jpg',
			}],
			colors: [{
				hex: '#F8F8F8',
				image: '//question/colour-11-white.jpg',
				title: 'white',
			}],
			id: 13124,
			retailPrice: 339000,
			quantity: 1,
			size: {
				id: 8,
				title: 'M',
			},
		}, {
			assets: [{
				id: 30841,
				url: 'production/variant/13125/r6otpd6nqyowd9tvxenh.jpg',
			}, {
				id: 30842,
				url: 'production/variant/13125/fejuhcblb0etmthxxr2x.jpg',
			}, {
				id: 30843,
				url: 'production/variant/13125/sn5ht5n1adhhj2dsyx2u.jpg',
			}, {
				id: 30844,
				url: 'production/variant/13125/xaidtzzk6njoyb0rp9f5.jpg',
			}, {
				id: 30845,
				url: 'production/variant/13125/xiomvi78acvovi72rowu.jpg',
			}],
			colors: [{
				hex: '#F8F8F8',
				image: '//question/colour-11-white.jpg',
				title: 'white',
			}],
			id: 13125,
			retailPrice: 339000,
			quantity: 0,
			size: {
				id: 9,
				title: 'L',
			},
		}, {
			assets: [{
				id: 30826,
				url: 'production/variant/13122/ywzbvady3vxj2jy4ywg3.jpg',
			}, {
				id: 30827,
				url: 'production/variant/13122/ajfjn13xt7vf3ysnpd1u.jpg',
			}, {
				id: 30828,
				url: 'production/variant/13122/q0nz5j1lyol4t61gbjea.jpg',
			}, {
				id: 30829,
				url: 'production/variant/13122/zv6wh5t5z8akx17zxbrm.jpg',
			}, {
				id: 30830,
				url: 'production/variant/13122/lgmlei5fa3je5qzywqzu.jpg',
			}],
			colors: [{
				hex: '#000000',
				image: '//question/colour-13-black.jpg',
				title: 'black',
			}],
			id: 13122,
			retailPrice: 339000,
			quantity: 0,
			size: {
				id: 9,
				title: 'L',
			},
		}],
	}],
}, {
	id: 2,
	title: 'Diskon 25 Agustus',
	createdAt: '2021-08-18T09:58:23.056Z',
	publishedAt: '2021-08-16T16:59:00.000Z',
	expiredAt: '2021-08-23T16:59:00.000Z',
	metadat: {},
	description: 'Diskon tanggal 25 Agustus',
	products: [{
		brand: 'Aevydae',
		title: 'Trinidad Dress',
		id: 14496,
		variants: [{
			assets: [{
				id: 63251,
				url: 'production/variant/23901/ywfjh5zpzd3vcgneuciu.jpg',
			}, {
				id: 63252,
				url: 'production/variant/23901/jayvcutfytgtuef7asna.jpg',
			}, {
				id: 63253,
				url: 'production/variant/23901/lrj3mq2tpypdjczgrb8r.jpg',
			}, {
				id: 63254,
				url: 'production/variant/23901/odcy3e2lmdskce92ztzn.jpg',
			}],
			colors: [{
				hex: '#FFB2B2',
				image: '/question/colour-2-pink.jpg',
				title: 'pink',
			}],
			id: 23901,
			retailPrice: 139000,
			quantity: 2,
			size: {
				id: 14,
				title: 'All Size',
			},
		}, {
			assets: [{
				id: 63247,
				url: 'production/variant/23900/y2bpcktjypmsrjnbf9ms.jpg',
			}, {
				id: 63249,
				url: 'production/variant/23900/dhdfiorakh3pezc7p7z0.jpg',
			}, {
				id: 63250,
				url: 'production/variant/23900/ona42isgvknpjzm0u8g1.jpg',
			}, {
				id: 63248,
				url: 'production/variant/23900/ga5bk6j4uiaycemtef2j.jpg',
			}],
			colors: [{
				hex: '#257BCD',
				image: '//question/colour-8-blue.jpg',
				title: 'blue',
			}],
			id: 23900,
			retailPrice: 139000,
			quantity: 2,
			size: {
				id: 14,
				title: 'All Size',
			},
		}],
	}],
}]

export default ConnectHelper(
	class DiscountPage extends PageModel {
		static routeName = 'discounts'
		

		static stateToProps(state) {
			return {
				me: state.me,
				// isAuthorized:
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isUpdatingData: [],
				search: p.search || undefined,
				offset: 0,
				count: 30,
				total: 0,
				promos: data || [],
				filter: {
					status: undefined,
				},
				token: undefined,
			})
		}

		headers = [{
			title: 'Title',
			width: 5,
		}, {
			title: 'Product',
			width: 5,
		}, {
			title: 'Is Active',
			width: 1,
		}, {
			title: 'Published At',
			width: 2,
		}, {
			title: 'Expired At',
			width: 2,
		}, {
			title: '',
			width: .5,
		}]

		statuses = [{
			key: '',
			title: 'All Status',
		}, {
			key: 'expired:true',
			title: 'Expired',
		}, {
			key:' expired:false',
			title: 'Not Expired',
		}, {
			key: 'published:true',
			title: 'Published',
		}, {
			key: 'published:false',
			title: 'Not Published',
		}]

		onNavigateToPromoDetail = promoId => {
			this.navigator.navigate(`discount/${promoId}`)
		}

		imageRenderer = (length, assets, i) => {
			return i < 6 ? (
				i === 5 ? (
					<BoxBit key={i} unflex>
						<BoxImageBit unflex
							source={assets}
							transform={{crop: 'fit'}}
							broken={!assets}
							style={Styles.image}
						/>
						<BoxBit unflex centering style={ Styles.countBox}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.count}>
								+{length - i - 1}
							</GeomanistBit>
						</BoxBit>
					</BoxBit>
				) : (
					<BoxBit key={i} unflex>
						<BoxImageBit unflex
							source={assets}
							transform={{crop: 'fit'}}
							broken={!assets}
							style={Styles.image}
						/>
					</BoxBit>
				)
			) : undefined
		}

		rowRenderer = promo => {
			const image = []
			
			promo.products.map(products => products.variants.map(variants => variants.assets.map(assets => image.push(assets))))

			const isActive = !!promo.expiredAt
				? TimeHelper.moment(new Date()).isAfter(promo.publishedAt) && TimeHelper.moment(new Date()).isBefore(promo.expiredAt)
				: !!promo.publishedAt
					? TimeHelper.moment(new Date()).isAfter(promo.publishedAt)
					: false
			
			return {
				data: [{
					children: (
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium">
								{promo.title}
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
								{promo.description}
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					// Render Image
					// children: (
					// 	<BoxBit>
					// 		<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium">
					// 			THIS IS WHERE VARIANT IMAGE WILL BE RENDER
					// 		</GeomanistBit>
					// 	</BoxBit>
					// ),
					children: (
						<BoxBit row style={Styles.imageBox}>
							{image.map(this.imageRenderer.bind(this, image.length))}
						</BoxBit>
					),
				}, {
					children: (
						<IconBit
							name={isActive ? 'circle-checkmark' : 'circle-null'}
							color={isActive ? Colors.green.palette(1) : Colors.grey.palette(6)}
						/>
					),
				}, {
					children: (
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{TimeHelper.moment(promo.publishedAt).format('DD/MM/YYYY — HH:mm')}
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
								{TimeHelper.moment(promo.publishedAt).fromNow()}
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: '-',
					children: promo.expiredAt ? (
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{TimeHelper.moment(promo.expiredAt).format('DD/MM/YYYY — HH:mm')}
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
								{TimeHelper.moment(promo.expiredAt).fromNow()}
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					children: (
						<TouchableBit centering row onPress={() => this.log('SHOULD SHOW QUICK VIEW OR NOT ?')}>
							<BoxBit />
							<IconBit
								name="quick-view"
								color={Colors.primary}
							/>
						</TouchableBit>
					),
				}],
				onPress: this.onNavigateToPromoDetail.bind(this, promo.id),
			}
		}

		view() {
			this.log(this.state.promos)
			return (
				<PageAuthorizedComponent
					roles={'marketing'}
					// onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Discounts' }]}
				>
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Discount"
								buttonPlaceholder="Add New Discount"
								onPress={() => this.log('Add New Discount')}
								searchPlaceholder={'ID, Code'}
								search={this.state.search}
								onSearch={this.onSearch}
							/>
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange}>
								<SelectionBit
									placeholder={'All Status'}
									options={this.statuses}
									onChange={() => this.log('CHANGE STATUS')}
								/>
							</HeaderFilterComponent>
							<TableLego
								isLoading={this.state.isLoading}
								headers={this.headers}
								rows={this.state.promos.map(this.rowRenderer)}
								style={Styles.padder}
							/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}

	}
)
