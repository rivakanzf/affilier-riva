import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		paddingBottom: 32,
		paddingLeft: 32,
		paddingRight: 32,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},
	
	padder: {
		marginBottom: 16,
	},

	image: {
		width: 60,
		height: 60 * 4 / 3,
	},

	countBox: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		backgroundColor: Colors.black.palette(2, .4),
	},

	count: {
		color: Colors.white.primary,
	},

	imageBox: {
		justifyContent: 'space-around',
	},
})
