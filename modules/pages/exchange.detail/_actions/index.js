import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import ButtonBit from 'modules/bits/button';
import ClipboardBit from 'modules/bits/clipboard';

import BoxRowLego from 'modules/legos/box.row';
import SelectStatusLego from 'modules/legos/select.status';


export default ConnectHelper(
	class ActionsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				note: PropTypes.string,
				status: PropTypes.string,
				createdAt: PropTypes.date,
				onUpdated: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isSaving: false,
				changes: {
					note: undefined,
					details: {},
				},
			})
		}

		// onApproveReject = () => {
		// 	// TODO
		// 	// check if all is not rejected
		// 	this.setState({
		// 		isSaving: true,
		// 	}, () => {
		// 		Promise.resolve().then(() => {
		// 			const count = this.props.details.length

		// 			if (!count) {
		// 				throw new Error('There is no details to approve / reject.')
		// 			}

		// 			if (!this.state.changes.note) {
		// 				throw new Error('Approval / Rejection note must be filled.')
		// 			}

		// 			if (count !== Object.keys(this.state.changes.details).length) {
		// 				throw new Error('You need to fill in all of details note and status.')
		// 			}

		// 			let haveRejectedAndNoNote = false
		// 			const {
		// 				isAllRejected,
		// 				isAllApproved,
		// 			} = this.props.details.reduce((sum, detail) => {
		// 				const update = this.state.changes.details[detail.id]

		// 				if (!!update && update.status === 'FAILED' && !update.note) {
		// 					haveRejectedAndNoNote = true
		// 				}
		// 				return {
		// 					isAllApproved: sum.isAllApproved ? !!update && update.status === 'SUCCESS' : false,
		// 					isAllRejected: sum.isAllRejected ? !!update && update.status === 'FAILED' : false,
		// 				}
		// 			}, {
		// 				isAllRejected: true,
		// 				isAllApproved: true,
		// 			})

		// 			if(haveRejectedAndNoNote) {
		// 				throw new Error('You need to specify a note when rejecting a detail.')
		// 			}

		// 			const details = Object.keys(this.state.changes.details).map(k => {
		// 				return {
		// 					exchange_detail_id: parseInt(k, 10),
		// 					status: this.state.changes.details[k].status,
		// 					note: this.state.changes.details[k].note,
		// 				}
		// 			})

		// 			if(isAllRejected) {
		// 				// Reject
		// 				return ExchangeService.reject(this.props.id, {
		// 					note: this.state.changes.note,
		// 					details,
		// 				}, this.props.token).then(() => ['FAILED', details])
		// 			} else {
		// 				// Approved or Exception
		// 				return ExchangeService.approve(this.props.id, {
		// 					note: this.state.changes.note,
		// 					details,
		// 				}, this.props.token).then(() => [isAllApproved ? 'SUCCESS' : 'EXCEPTION', details])
		// 			}
		// 		}).then(([status, details]) => {
		// 			// updated
		// 			this.props.onUpdated &&
		// 			this.props.onUpdated(status, this.state.changes.note, details)

		// 			this.setState({
		// 				isSaving: false,
		// 			})

		// 			this.props.utilities.notification.show({
		// 				title: 'Yeay!',
		// 				message: `Success ${ status === 'FAILED' ? 'rejecting' : 'approving' } exchange request.`,
		// 			})
		// 		}).catch(err => {
		// 			this.setState({
		// 				isSaving: false,
		// 			})

		// 			this.props.utilities.notification.show({
		// 				title: 'Oops…',
		// 				message: err ? err.detail || err.message || err : 'Something went wrong.',
		// 			})
		// 		})
		// 	})
		// }

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Status',
							children: (
								<SelectStatusLego nullAsPending disabled unflex
									type={ SelectStatusLego.TYPES.CODES }
									status={ this.props.status }
								/>
							),
							description: `Exchange date: ${TimeHelper.format(this.props.updatedAt, 'DD MMMM YYYY [at] HH:mm')}`,
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ this.state.isUpdatable ? 'APPROVE / REJECT' : 'UPDATE EXCHANGE' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.state.isSaving ? ButtonBit.TYPES.STATES.LOADING : this.state.isChanged && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
									onPress={ this.onApproveReject }
								/>
							),
						}],
					}, {
						data: [{
							title: `Note${ this.state.isUpdatable ? '*' : '' }`,
							children: (
								<ClipboardBit autogrow={44} value={ this.props.note || '-' } />
							),
						}],
					}]}
				/>
			)
		}
	}
)
