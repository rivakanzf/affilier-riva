import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LinkBit from 'modules/bits/link';
import NavTextBit from 'modules/bits/nav.text';

import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';

import ShipmentPart from '../_shipment'

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				orderId: PropTypes.id,
				orderNumber: PropTypes.string,
				orderDate: PropTypes.date,
				user: PropTypes.string,
				email: PropTypes.string,
				userId: PropTypes.id,
				children: PropTypes.node,
				onChange: PropTypes.func,
				loading: PropTypes.bool,
				disable: PropTypes.bool,
				shipment: PropTypes.shape({
					awb: PropTypes.string,
					courier: PropTypes.string,
					id: PropTypes.id,
					status: PropTypes.string,
					type: PropTypes.string,
				}),
			}
		}

		view() {
			return (
				<React.Fragment>
					<BoxRowLego
						title={`Exchange #EX-${this.props.id}`}
						data={[{
							data: [{
								title: 'Order',
								children: (
									<NavTextBit
										title={ this.props.orderNumber }
										link={ 'View Order' }
										href={ `order/${ this.props.orderId }` }
									/>
								),
							}, {
								title: 'Client',
								children: (
									<NavTextBit
										title={ this.props.user }
										link={ 'View Client' }
										href={ `client/${ this.props.userId }` }
									/>
								),
							}],
						}]}
					/>
					<BoxRowLego
						title="Note*"
						data={[{
							title: '',
							children: (
								<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3}>Pengisian resi shipping secara manual sudah tidak dibutuhkan lagi dengan sistem yang baru (18/03/2020)</GeomanistBit>
							),
						}]}
					/>
					{ this.props.children }
					{ this.props.shipment ? (
						<BoxRowLego
							title={'Return Shipment'}
							data={[{
								data: [{
									title: 'Courier',
									content: `${ this.props.shipment.courier } 'Reguler' }`,
								}, {
									title: 'Status',
									children: (
										<BoxBit row unflex>
											<BadgeStatusLego status={ this.props.shipment.status } />
										</BoxBit>
									),
								}],
							}, {
								data: [{
									title: 'AWB',
									content: this.props.shipment.awb,
								},
									// {
									// 	title: 'URL',
									// 	content: '-',
									// 	children: this.props.shipment.url ? (
									// 		<LinkBit underline href={this.props.shipment.url} target={ LinkBit.TYPES.BLANK }>
									// 			{ this.props.shipment.url }
									// 		</LinkBit>
									// 	) : undefined,
									// },
								],
							},
								// {
								// 	data: [{
								// 		title: 'Manifest Date',
								// 		content: TimeHelper.format(this.props.shipment.created_at, 'DD MMMM YYYY [—] HH:mm'),
								// 	}, {
								// 		title: 'Est. Delivery Date',
								// 		content: TimeHelper.getRange(TimeHelper.moment(this.props.shipment.created_at).add(this.props.shipment.metadata.min_delivery_day, 'd'), TimeHelper.moment(this.props.shipment.created_at).add(this.props.shipment.metadata.max_delivery_day, 'd'), 'DD', 'MMMM'),
								// 	}],
								// },
							]}
							style={ Styles.padder }
						/>
					) : false }
				</React.Fragment>
			)
		}
	}
)
