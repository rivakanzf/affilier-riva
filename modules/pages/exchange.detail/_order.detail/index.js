import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import UtilitiesContext from 'coeur/contexts/utilities'

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
import NavTextBit from 'modules/bits/nav.text';
import NavTextTitleBit from 'modules/bits/nav.text.title';

import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';

import QuickViewItemPagelet from 'modules/pagelets/quick.view.item'

import Styles from './style';

import { capitalize } from 'lodash'


export default ConnectHelper(
	class DetailPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				header: PropTypes.string,
				id: PropTypes.id,
				title: PropTypes.string,
				description: PropTypes.string,
				status: PropTypes.string,
				type: PropTypes.string,
				ref_id: PropTypes.id,
				stylesheet: PropTypes.shape({
					id: PropTypes.id,
					stylist_id: PropTypes.id,
					stylist: PropTypes.string,
					image: PropTypes.object,
				}),
				style: PropTypes.style,
			}
		}
		
		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			stylesheet: {},
		}

		onNavigateToDetail = () => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewItemPagelet type={ this.props.type } id={ this.props.ref_id } />
				),
			})
		}

		productRenderer = () => {
			switch(this.props.type) {
			case 'STYLESHEET':
				return  [{
					data: [{
						title: 'Stylesheet',
						children: (
							<BoxBit row>
								<ImageBit source={ this.props.stylesheet.image } overlay resizeMode={ ImageBit.TYPES.COVER } broken={ !this.props.stylesheet.image } style={Styles.image} />
								<NavTextTitleBit
									title={ this.props.title }
									description={ this.props.description }
									link={ 'View Stylesheet' }
									href={ `stylesheet/${ this.props.stylesheet.id }` }
								/>
							</BoxBit>
						),
					}],
				}, {
					data: [{
						title: 'Stylist',
						children: (
							<NavTextBit
								title={ this.props.stylesheet.stylist }
								link={ 'View Stylist' }
								href={ `client/${ this.props.stylesheet.stylist_id }` }
							/>
						),
					}, {
						title: 'Status',
						children: (
							<BoxBit row unflex>
								<BadgeStatusLego status={ this.props.status } />
							</BoxBit>
						),
					}],
				}]
			case 'INVENTORY':
			case 'SERVICE':
			case 'VOUCHER':
			default:
				return [{
					data: [{
						title: capitalize(this.props.type),
						children: (
							<NavTextBit
								title={ this.props.title }
								link={ 'View Inventory' }
								href={ `inventory/${ this.props.ref_id }` }
								onPress={ this.onNavigateToDetail }
							/>
						),
					}, {
						title: 'Status',
						children: (
							<BoxBit row unflex>
								<BadgeStatusLego status={ this.props.status } />
							</BoxBit>
						),
					}],
				}]
			}
		}

		view() {
			return (
				<BoxRowLego
					title={ this.props.header }
					data={ this.productRenderer() }
					style={ [this.props.style, Styles.padder] }
				/>
			)
		}
	}
)
