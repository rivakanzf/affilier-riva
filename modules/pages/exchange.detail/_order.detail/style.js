import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	padder: {
		marginTop: Sizes.margin.default,
	},
	image: {
		width: 60,
		height: 60,
		// borderWidth: StyleSheet.hairlineWidth,
		// borderColor: Colors.black.palette(2, .16),
		marginRight: Sizes.margin.default / 2,
	},
})
