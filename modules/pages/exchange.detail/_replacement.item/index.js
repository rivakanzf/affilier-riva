import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';
import FormatHelper from 'coeur/helpers/format';


import UtilitiesContext from 'coeur/contexts/utilities';

import ButtonBit from 'modules/bits/button';
import ClipboardBit from 'modules/bits/clipboard';

import BoxRowLego from 'modules/legos/box.row';
import BoxLego from 'modules/legos/box.row';
import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import ImageBit from 'modules/bits/image'
import TextBit from 'modules/bits/text'

import AddressListPagelet from 'modules/pagelets/address.list'
import ShipmentUpdatePagelet from 'modules/pagelets/shipment.update'

import ShipmentListLego from 'modules/legos/shipment.list'
import SelectStatusLego from 'modules/legos/select.status';

import Styles from './style';
import { isEmpty } from 'lodash';

export default ConnectHelper(
	class ReplacementItemPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				exchangeId: PropTypes.number,
				data: PropTypes.array,
				exchangeDetailIds: PropTypes.array,
				addresses: PropTypes.arrayOf(PropTypes.shape({
					address: PropTypes.string,
					id: PropTypes.number,
					location_id: PropTypes.number,
					phone: PropTypes.string,
					postal: PropTypes.string,
					receiver: PropTypes.string,
					title: PropTypes.string,
				})),
				replacementShipments: PropTypes.arrayOf(PropTypes.shape({
					address: PropTypes.shape({
						address: PropTypes.string,
						id: PropTypes.number,
						location_id: PropTypes.number,
						phone: PropTypes.string,
						postal: PropTypes.string,
						receiver: PropTypes.string,
						title: PropTypes.string,
					}),
					awb: PropTypes.string,
					courier: PropTypes.string,
					id: PropTypes.number,
					status: PropTypes.string,
					type: PropTypes.string,
				})),
				onRefresh: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onShowShipmentDetail = () => {
			this.props.utilities.alert.modal({
				component: (
					<ShipmentUpdatePagelet
						id={this.props.replacementShipments[0].id}
						editable
					/>
				),
			})
		}

		onCreateShipment = (selectedAddress) => {			
			this.onModalRequestClose()

			this.props.utilities.alert.modal({
				component: (
					<ShipmentUpdatePagelet
						isExchange
						method={ 'MANUAL' }
						exchangeId={ this.props.exchangeId }
						exchangeDetailIds={ this.props.exchangeDetailIds }
						shipment={{
							destination: selectedAddress,
						}}
						onShipmentCreated={ this.props.onRefresh }
					/>
				),
			})
		}

		onSelectAddress = () => {
			this.props.utilities.alert.modal({
				component: (
					<AddressListPagelet
						selectable addable
						type="order"
						typeId={ this.props.orderId }
						// selectedId={ selectedAddressId }
						addresses={ this.props.addresses }
						onClose={ this.onModalRequestClose }
						onConfirm={ this.onCreateShipment }
					/>
				),
			})
		}

		shipmentRenderer = (shipment) => {
			return (
				<ShipmentListLego
					isRetrun
					id={ shipment.id }
					courier={ shipment.courier }
					service={ shipment.service }
					awb={ shipment.awb }
					status={ shipment.status }
					onPress={ this.onShowShipmentDetail }
				/>
			)
		}

		itemRenderer = (item, i) => {
			return (
				<BoxBit key={  `key_${i}` } unflex style={[Styles.item]}>
					<BoxBit unflex row>
						<ImageBit overlay resizeMode={ ImageBit.TYPES.COVER } source={ '//' + item.image.url } style={ Styles.image } />
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
								{ item.brand }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium">
								{ item.product }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.discounted } >
								IDR { FormatHelper.currency(item.price) }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ item.size } - {item.colors[0].title}
							</GeomanistBit>
							<BoxBit />
						</BoxBit>
					</BoxBit>
				</BoxBit>
			)
		}

		view() {
			return (!isEmpty(this.props.data) || !isEmpty(this.props.replacementShipments)) && (
				<BoxLego title="Replacement Items">
					{ this.props.data && this.props.data.map(this.itemRenderer) }
					{ this.props.replacementShipments &&  this.props.replacementShipments.map(this.shipmentRenderer) }
				</BoxLego>
			)
		}
	}
)
