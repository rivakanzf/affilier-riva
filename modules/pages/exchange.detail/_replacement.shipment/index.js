import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';
import FormatHelper from 'coeur/helpers/format';


import UtilitiesContext from 'coeur/contexts/utilities';

import ButtonBit from 'modules/bits/button';
import ClipboardBit from 'modules/bits/clipboard';

import BoxRowLego from 'modules/legos/box.row';
import BoxLego from 'modules/legos/box.row';
import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import ImageBit from 'modules/bits/image'
import TextBit from 'modules/bits/text'

import SelectStatusLego from 'modules/legos/select.status';

import ShipmentListLego from 'modules/legos/shipment.list'

import ShipmentListPagelet from 'modules/pagelets/shipment.list'

import Styles from './style';

export default ConnectHelper(
	class ReplacementShipmentPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.array,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				addresses: [],
			})
		}

		static getDerivedStateFromProps(nP, nS) {
			const addresses = nP.user.addresses.map(addr => addr.id)
			const shipmentIds = nP.data.map(shipment => shipment.id)
			return {
				...nS,
				addresses,
				shipmentIds,
			}
		}

		shipmentRenderer = (shipment) => {
			return (
				<ShipmentListLego
					isRetrun
					id={ shipment.id }
					courier={ shipment.courier }
					service={ shipment.service }
					awb={ shipment.awb }
					status={ shipment.status }
				/>
			)
		}

		view() {
			return (
				<BoxLego
					title="Replacement Shipments"
				>
					<ShipmentListPagelet
						editable
						shipments={this.props.data}
						details={[{shipment_ids: this.state.shipmentIds, address_id: this.props.data[0].address.id}]}
						addresses={this.state.addresses}
					/>
				</BoxLego>
			)
		}
	}
)
