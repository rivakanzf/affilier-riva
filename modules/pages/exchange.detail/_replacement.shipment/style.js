import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
    
	item: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.black.palette(2, .16),
	},

	image: {
		width: 60,
		height: 78,
		marginRight: Sizes.margin.default,
	},

	discounted: {
		color: Colors.red.palette(7),
	},

	retail: {
		color: Colors.black.palette(2),
		textDecoration: 'line-through',
	},
	
	note: {
		color: Colors.black.palette(2, .6),
	},


})
