import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import ButtonBit from 'modules/bits/button'
import TextInputBit from 'modules/bits/text.input'
import GeomanistBit from 'modules/bits/geomanist'

import BoxRowLego from 'modules/legos/box.row'
import { isEmpty } from 'lodash'

export default ConnectHelper(
	class ShipmentPart extends ConnectedStatefulModel {
		
		static propTypes(PropTypes) {
			return {
				onChange: PropTypes.func,
				loading: PropTypes.bool,
				disabled: PropTypes.bool,
				shipment: PropTypes.shape({
					courier: PropTypes.string,
					service: PropTypes.string,
					status: PropTypes.string,
					awb: PropTypes.string,
					url: PropTypes.string,
					created_at: PropTypes.date,
					metadata: PropTypes.object,
				}),
				onSubmit: PropTypes.func,
			}
		}

		static defaultProps = {
			shipment: {},
		}

		constructor(p) {
			super(p, {
				isChanged: false,
				awb: !isEmpty(p.shipment) ? p.shipment.awb : undefined,
				courier: !isEmpty(p.shipment) ? p.shipment.courier : undefined,
			})
		}

		onChange = (key, e, val) => {
			if(!this.state.isChanged) {
				this.state.isChanged = true
			}

			this.state[key] = val
			
			this.forceUpdate()
		}

		onSubmit = () => {
			this.props.onSubmit &&
			this.props.onSubmit(this.state.awb, this.state.courier)
		}

		view() {
			return (
				// this.props.shipment ? (
				// 	<BoxRowLego
				// 		title="Shipment"
				// 		data={[{
				// 			data: [{
				// 				title: 'Nomor Resi',
				// 				children: (
				// 					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
				// 						{ this.state.awb }
				// 					</GeomanistBit>
				// 				),
				// 			}, {
				// 				title: 'Courier',
				// 				children: (
				// 					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
				// 						{ this.props.shipment.courier }
				// 					</GeomanistBit>
				// 				),
				// 			}],
				// 		}]}
				// 	/>
				// ) : (
				<BoxRowLego
					title="Shipment"
					data={[{
						data: [{
							title: 'Nomor Resi',
							children: (
								<TextInputBit
									placeholder="Masukkan nomor resi"
									defaultValue={ this.state.awb }
									onChange={ this.onChange.bind(this, 'awb')}
								/>
							),
						}, {
							title: ' ',
							children: (
								<ButtonBit
									weight="medium"
									title="Update Shipment"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={this.props.loading ? ButtonBit.TYPES.STATES.LOADING : !this.state.isChanged && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL}
									onPress={ this.onSubmit }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Courier',
							children: (
								<TextInputBit
									placeholder="Masukkan pilihan courier pengiriman"
									defaultValue={ this.state.courier }
									onChange={this.onChange.bind(this, 'courier')}
								/>
							),
						}],
					}]}
				/>
			)
			// )
		}
	}
)
