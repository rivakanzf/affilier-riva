import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import ExchangeService from 'app/services/exchange';
// import OrderDetailManager from 'app/managers/order.detail';

import BoxBit from 'modules/bits/box';

import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import ExchangeDetailsPagelet from 'modules/pagelets/exchange.details';
import NotFoundPagelet from 'modules/pagelets/not.found';

import ActionsPart from './_actions'
import DetailPart from './_detail'
import ReplacementItemPart from './_replacement.item'
// import OrderDetailPart from './_order.detail'
// import ReplacementShipmentPart from './_replacement.shipment'

import Styles from './style';

export default ConnectHelper(
	class ExchangeDetailPage extends PageModel {

		static routeName = 'exchange.detail'

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				isChanged: false,
				isUpdating: false,
				token: undefined,
				data: {
					order_detail: {},
					details: [],
				},
			})
		}

		roles = ['fulfillment']

		onAuthorized = token => {
			this.setState({
				token,
				isLoading: true,
			}, () => {
				ExchangeService.getDetail(this.props.id, this.state.token).then(res => {
					this.setState({
						isLoading: false,
						data: res,
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onRefresh = () => {
			this.utilities.alert.hide()

			this.onAuthorized(this.state.token)
		}

		onSubmitAWB = (awb, courier) => {
			this.setState({
				isLoading: true,
			}, () => {
				ExchangeService.updateResi(this.props.id, {
					user_id: this.state.data.user_id,
					awb,
					courier,
				}, this.state.token).then(() => {
					this.utilities.notification.show({
						type: 'SUCCESS',
						title: 'Yeayy',
						message: 'AWB Successfuly Updated',
					})
					
					this.onAuthorized(this.state.token)
				}).catch(err => {
					this.warn(err)
					
					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Ooops',
							message: 'Update resi failed',
						})
					})
				})
			})
		}

		onNavigateToExchange = () => {
			this.navigator.navigate('exchange')
		}

		onChange = () => {
			// this.state.changes[key] = val

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onUpdated = (status, note, details) => {
			this.state.data.status = status
			this.state.data.note = note

			this.state.data.details.forEach(detail => {
				const update = details.find(d => d.exchange_detail_id === detail.id)

				if(update) {
					detail.note = update.note
					detail.status = update.status
				}
			})

			this.forceUpdate()
		}

		viewOnLoading() {
			return (
				<NotFoundPagelet loading />
			)
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={this.onAuthorized}
					paths={[{
						title: 'Exchange',
						onPress: this.onNavigateToExchange,
					}, {
						title: this.state.isLoading
							? 'Loading...'
							: `EX-${this.props.id}`,
					}]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title={ 'Exchange Detail' }
							/>
							{ this.state.isLoading ? this.viewOnLoading() : (
								<BoxBit unflex row>
									<BoxBit style={Styles.padder}>
										<DetailPart
											id={ this.props.id }
											orderId={ this.state.data.details[0].order.id }
											orderNumber={ this.state.data.details[0].order.number }
											orderDate={ this.state.data.details[0].order.created_at }
											user={ `${this.state.data.user.firstname} ${this.state.data.user.lastname}` }
											email={ this.state.data.user.email }
											userId={ this.state.data.user.id }
											shipment={ this.state.data.return_shipment }
											loading={this.state.isUpdating}
											disabled={!this.state.isChanged}
											onChange={ this.onChange }
											onSubmitAWB={ this.onSubmitAWB }
										>
											{/* <OrderDetailPart header={ 'Exchanged Product' } { ...this.state.data.order_detail } /> */}
										</DetailPart>
										{ this.state.data.replacement_items ? (
											<ReplacementItemPart
												exchangeId={ parseInt(this.props.id, 10) }
												exchangeDetailIds={ this.state.data.details.map(detail => detail.id)}
												data={this.state.data.replacement_items}
												addresses={ this.state.data.user.addresses }
												replacementShipments={ this.state.data.replacement_shipments}
												onRefresh={ this.onRefresh }
											/>
										) : false }
									</BoxBit>
									<BoxBit>
										<ActionsPart
											key={ this.state.data.status }
											id={ this.props.id }
											note={ this.state.data.note }
											status={ this.state.data.status }
											createdAt={ this.state.data.created_at }
											details={ this.state.data.details }
											replacement={ this.state.data.replacement }
											onUpdated={ this.onUpdated }
										/>
										<ExchangeDetailsPagelet details={this.state.data.details} id={ this.props.id } title={ 'Exchange Items' } onUpdated={ this.onRefresh } style={ Styles.pad } />
									</BoxBit>
								</BoxBit>
							) }
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
