import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import Colors from 'utils/constants/color'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LinkBit from 'modules/bits/link';
import IconBit from 'modules/bits/icon';

import RowsLego from 'modules/legos/rows';
import BadgeStatusLego from 'modules/legos/badge.status';
import LoaderLego from 'modules/legos/loader';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';


export default ConnectHelper(
	class QuickViewPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				order_number: PropTypes.string,
				order_date: PropTypes.date,
				user: PropTypes.string,
				email: PropTypes.string,

				order_detail_title: PropTypes.string,
				order_detail_description: PropTypes.string,

				replacement_id: PropTypes.id,
				replacement_title: PropTypes.string,
				replacement_description: PropTypes.string,

				shipment: PropTypes.object,
				status: PropTypes.string,
				note: PropTypes.string,
				created_at: PropTypes.date,
				onClose: PropTypes.func,
				onNavigateToExchange: PropTypes.func,
			}
		}

		onNavigateToDetail = () => {
			this.props.onClose()
			this.props.onNavigateToExchange()
		}

		viewOnError() {
			return (
				<BoxBit row centering style={Styles.empty}>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.error}>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ `Exchange #${ this.props.id }` }
					header={( <BadgeStatusLego status={this.props.status} /> )}
					confirm="View Exchange"
					onClose={ this.props.onClose }
					onConfirm={ this.onNavigateToDetail }
					contentContainerStyle={ Styles.container }
				>
					<RowsLego
						data={[{
							data: [{
								title: 'Order',
								children: (
									<React.Fragment>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
											{ this.props.order_number }
										</GeomanistBit>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
											Order date: { TimeHelper.format(this.props.order_date, 'DD MMMM YYYY HH:mm') }
										</GeomanistBit>
									</React.Fragment>
								),
							}, {
								title: 'Client',
								children: (
									<React.Fragment>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
											{ this.props.user }
										</GeomanistBit>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
											{ this.props.email }
										</GeomanistBit>
									</React.Fragment>
								),
							}],
						}, {
							data: [{
								title: 'Product',
								children: (
									<React.Fragment>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
											{ this.props.order_detail_title }
										</GeomanistBit>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
											{ this.props.order_detail_description }
										</GeomanistBit>
									</React.Fragment>
								),
							}, {
								title: 'Replacement',
								content: '-',
								children: this.props.replacement_id ? (
									<React.Fragment>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
											{ this.props.replacement_title }
										</GeomanistBit>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
											{ this.props.replacement_description }
										</GeomanistBit>
									</React.Fragment>
								) : undefined,
							}],
						}, {
							data: [{
								title: 'Exchange Date',
								content: TimeHelper.format(this.props.created_at, 'DD MMMM YYYY HH:mm'),
							}, {
								title: 'Exchange Note',
								content: this.props.note,
							}],
						}, {
							data: [{
								title: 'Return Shipment',
								content: '-',
								children: this.props.shipment ? (
									<React.Fragment>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
											{ this.props.shipment.awb }
										</GeomanistBit>
										<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
											<LinkBit target={LinkBit.TYPES.BLANK} href={ this.props.shipment.url } underline>
												{ this.props.shipment.url }
											</LinkBit>
										</GeomanistBit>
									</React.Fragment>
								) : undefined,
							}, {
								blank: true,
							}],
						}]}
					/>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
