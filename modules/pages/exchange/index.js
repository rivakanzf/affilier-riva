import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';

import AuthenticationHelper from 'utils/helpers/authentication';

import ExchangeService from 'app/services/exchange';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import NavTextBit from 'modules/bits/nav.text';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import QuickViewPart from './_quick.view'

import Styles from './style';

import { capitalize, orderBy } from 'lodash'


export default ConnectHelper(
	class ExchangePage extends PageModel {

		static routeName = 'exchange'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'fulfillment'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				exchanges: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					status: 'PENDING',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		roles = ['fulfillment']

		tableHeader = [{
			title: 'Order',
			width: 2.1,
		}, {
			title: 'Product',
			width: 2.9,
		}, {
			title: 'User',
			width: 2.5,
		}, {
			title: 'Status',
			width: 1,
		}, {
			title: 'Exc. Note',
			width: 2.4,
		}, {
			title: '',
			width: .5,
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.filter.status !== this.state.filter.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				ExchangeService.filter({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(!!filter.status ? { status: filter.status } : {}),
				}, this.state.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								exchanges: res.data,
							})
						}
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops',
								message: 'Something went wrong with your request, please try again later',
							})
						})
					})
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToDetail = id => {
			this.navigator.navigate(`exchange/${id}`)
		}

		onQuickView = exchange => {
			this.utilities.alert.modal({
				component: (
					<QuickViewPart { ...exchange } status={ exchange.status }
						onNavigateToExchange={ this.onGoToDetail.bind(this, exchange.id) }
						onClose={ this.onModalRequestClose } />
				),
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		rowRenderer = exchange => {
			return {
				data: [{
					children: (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.title }>
								{ exchange.order_number }
							</GeomanistBit>
							<NavTextBit
								title={`Order date: ${ TimeHelper.format(exchange.order_date, 'DD MMM \'YY') }`}
								link={'View Order'}
								href={`order/${exchange.order_id}`}
							/>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.title }>
								{ exchange.order_detail_title }
							</GeomanistBit>
							<NavTextBit
								title={`${capitalize(exchange.order_detail_type)} #ST-${exchange.order_detail_ref_id}`}
								link={'View Stylesheet'}
								href={`stylesheet/${exchange.order_detail_ref_id}`}
							/>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.title }>
								{ exchange.user }
							</GeomanistBit>
							<NavTextBit
								title={ exchange.email }
								link={'View Client'}
								href={`client/${exchange.user_id}`}
							/>
						</BoxBit>
					),
				}, {
					children: (
						<BadgeStatusLego status={ exchange.status || 'PENDING' } />
					),
				}, {
					title: exchange.note || '-',
					// children: (
					// 	<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.title }>
					// 		{ exchange.note }
					// 	</GeomanistBit>
					// ),
				}, {
					children: (
						<TouchableBit centering onPress={ this.onQuickView.bind(this, exchange) }>
							<IconBit
								name="quick-view"
								color={ Colors.primary }
							/>
						</TouchableBit>
					),
				}],
				onPress: this.onGoToDetail.bind(this, exchange.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={this.roles}
					onAuthorized={this.onAuthorized}
					paths={[{ title: 'Exchange' }]}
				>
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Exchange Request"
								searchPlaceholder={ 'User or order number' }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								<SelectStatusLego type={SelectStatusLego.TYPES.CODES} status={ this.state.filter.status } onChange={this.onChangeFilter.bind(this, 'status')} />
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ orderBy(this.state.exchanges, ['id'], ['desc']).map(this.rowRenderer) }
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
