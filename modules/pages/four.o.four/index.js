import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import FourOFourPagelet from 'modules/pagelets/four.o.four'


export default ConnectHelper(
	class FourOFourPage extends PageModel {

		static routeName = '404'

		view() {
			return (
				<FourOFourPagelet />
			)
		}
	}
)
