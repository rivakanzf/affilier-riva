import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'utils/constants/color'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import InputCurrencyBit from 'modules/bits/input.currency';

import RowsLego from 'modules/legos/rows';
import BadgeStatusLego from 'modules/legos/badge.status';
import LoaderLego from 'modules/legos/loader';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';

import { capitalize } from 'lodash';


export default ConnectHelper(
	class QuickViewPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.shape({
					id: PropTypes.id,
					variant_id: PropTypes.id,
					product_id: PropTypes.id,
					brand: PropTypes.string,
					title: PropTypes.string,
					image: PropTypes.object,
					category: PropTypes.string,
					colors: PropTypes.arrayOf(PropTypes.object),
					size: PropTypes.title,
					retail_price: PropTypes.number,
					price: PropTypes.number,
					rack: PropTypes.string,
					note: PropTypes.string,
					booking_note: PropTypes.string,
					status: PropTypes.string,
				}),
				onClose: PropTypes.func,
			}
		}

		viewOnError() {
			return (
				<BoxBit row centering style={Styles.empty}>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.error}>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ 'Inventory Preview' }
					header={( <BadgeStatusLego status={this.props.data.status} /> )}
					confirm="Done"
					onClose={ this.props.onClose }
					onConfirm={ this.props.onClose }
					contentContainerStyle={ Styles.container }
				>
					<RowsLego
						data={[{
							children: (
								<ImageBit source={ this.props.data.image } overlay resizeMode={ ImageBit.TYPES.COVER } style={ Styles.image } />
							),
						}, {
							data: [{
								title: 'ID',
								content: `#${this.props.data.id}`,
							}, {
								title: 'Title',
								content: this.props.data.title,
							}],
						}, {
							data: [{
								title: 'Brand',
								content: this.props.data.brand,
							}, {
								title: 'Category',
								content: this.props.data.category,
							}],
						}, {
							data: [{
								title: 'Color',
								content: this.props.data.colors.map(c => capitalize(c.title)).join('/'),
							}, {
								title: 'Size',
								content: this.props.data.size,
							}],
						}, {
							data: [{
								title: 'Retail Price',
								children: (
									<InputCurrencyBit value={ this.props.data.retail_price } readonly />
								),
							}, {
								title: 'Discounted Price',
								children: (
									<InputCurrencyBit value={ this.props.data.price } readonly />
								),
							}],
						}, {
							data: [{
								title: 'Rack',
								content: this.props.data.rack || '-',
							}, {
								title: 'Note',
								content: `${ this.props.data.note || '-' }`,
								description: this.props.data.booking_note,
							}],
						}]}
					/>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
