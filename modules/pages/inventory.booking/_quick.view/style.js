import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	empty: {
		paddingTop: Sizes.margin.default,
		minHeight: 300,
	},

	error: {
		color: Colors.primary,
		marginLeft: 8,
	},

	container: {
		// paddingBottom: 0,
		// paddingLeft: 0,
		// paddingRight: 0,
		paddingTop: Sizes.margin.default,
	},

	image: {
		height: 128,
		width: 96,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .16),
		marginRight: Sizes.margin.default,
	},

	table: {
		marginTop: Sizes.margin.default,
		marginBottom: - StyleSheet.hairlineWidth,
	},

})
