import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import Colors from 'coeur/constants/color';

import AuthenticationHelper from 'utils/helpers/authentication';
import InventoryService from 'app/services/inventory';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import ColorLego from 'modules/legos/color';
import SelectBrandLego from 'modules/legos/select.brand';
import SelectCategoryLego from 'modules/legos/select.category';
import SelectDateLego from 'modules/legos/select.date';
import SelectSizeLego from 'modules/legos/select.size';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import InventoryStatusPagelet from 'modules/pagelets/inventory.status';

import QuickViewPart from './_quick.view'

import Styles from './style';


export default ConnectHelper(
	class InventoryBookingPage extends PageModel {

		static routeName = 'inventory.booking'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'inventory', 'fulfillment'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				inventories: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					status: 'BOOKED',
					date: undefined,
					brandId: undefined,
					categoryId: undefined,
					sizeId: undefined,
				},
				token: undefined,
			})

			this.getterId = 1
		}

		roles = ['inventory', 'fulfillment', 'stylist']

		tableHeader = [{
			title: 'Product',
			width: 4.5,
		}, {
			title: 'Category',
			width: 1.5,
		}, {
			title: 'Color',
			width: 1,
		}, {
			title: 'Size',
			width: 1,
		}, {
			title: 'Status',
			width: 1.5,
		}, {
			title: 'Note',
			width: 3,
		}, {
			title: '',
			width: .5,
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.filter.date !== this.state.filter.date
					|| pS.filter.status !== this.state.filter.status
					|| pS.filter.brandId !== this.state.filter.brandId
					|| pS.filter.categoryId !== this.state.filter.categoryId
					|| pS.filter.sizeId !== this.state.filter.sizeId
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				InventoryService.filter({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(filter.date ? { date: filter.date } : {}),
					status: filter.status,
					...(filter.brandId ? { brand_ids: filter.brandId } : {}),
					...(filter.categoryId ? { category_ids: filter.categoryId } : {}),
					...(filter.sizeId ? { size_ids: filter.sizeId } : {}),
					...( this.props.isManager ? {} : { mine: true }),
				}, this.state.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								inventories: res.data,
							})
						}
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops',
								message: 'Something went wrong with your request, please try again later',
							})
						})
					})
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onChangeStatus = inventory => {
			this.utilities.alert.modal({
				component: (
					<InventoryStatusPagelet force
						id={ inventory.id }
						status={ inventory.status }
						note={ inventory.note }
						onChange={ this.onStatusChanged.bind(this, inventory) }
					/>
				),
			})
		}

		onStatusChanged = (inventory, status, note) => {
			inventory.status = status
			if(status === 'BOOKED') {
				inventory.booking_note = note
			} else {
				inventory.note = note
			}

			this.forceUpdate()
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToDetail = id => {
			this.navigator.navigate(`product/${id}`)
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onQuickView = inventory => {
			this.utilities.alert.modal({
				component: (
					<QuickViewPart
						data={ inventory }
						onClose={ this.onModalRequestClose }
					/>
				),
			})
		}

		rowRenderer = inventory => {
			return {
				data: [{
					children: (
						<BoxBit row style={Styles.product}>
							<ImageBit source={ inventory.image } style={ Styles.image } />
							<BoxBit style={Styles.box}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium">
									#IN-{ inventory.id }
								</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title}>
									{ `${inventory.brand} – ${inventory.title}`}
								</GeomanistBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
									Seller: { inventory.seller }
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					),
				}, {
					title: inventory.category,
				}, {
					children: (
						<ColorLego colors={ inventory.colors } size={ 24 } radius={ 12 } border={ 0 } />
					),
				}, {
					title: inventory.size,
				}, {
					children: (
						<BadgeStatusLego
							status={ inventory.status }
							onPress={ this.onChangeStatus.bind(this, inventory) }
						/>
					),
				}, {
					children: (
						<BoxBit style={Styles.center}>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1}>
								{ inventory.note || '-' }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_2}>
								Rack: { inventory.rack || '-' }
							</GeomanistBit>
							{ inventory.booking_note ? (
								<GeomanistBit type={GeomanistBit.TYPES.NOTE_2} style={ Styles.note }>
									— { inventory.booking_note }
								</GeomanistBit>
							) : false }
						</BoxBit>
					),
				}, {
					children: (
						<TouchableBit centering style={Styles.icon} onPress={ this.onQuickView.bind(this, inventory) }>
							<IconBit
								name="quick-view"
								color={ Colors.primary }
							/>
						</TouchableBit>
					),
				}],
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={this.roles}
					onAuthorized={this.onAuthorized}
					paths={[{ title: 'Bookings' }]}
				>
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Inventory Booking"
								searchPlaceholder={ this.props.isManager ? 'User, brand, product' : 'Brand or product name' }
								onSearch={this.onSearch} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} >
								{ this.props.isManager ? (
									<SelectStatusLego type={SelectStatusLego.TYPES.INVENTORIES} status={ this.state.filter.status } onChange={this.onChangeFilter.bind(this, 'status')} />
								) : false }
								<SelectDateLego status={ this.state.filter.date } onChange={ this.onChangeFilter.bind(this, 'date') } />
								<SelectBrandLego brandId={ this.state.filter.brandId } onChange={ this.onChangeFilter.bind(this, 'brandId') } />
								<SelectCategoryLego categoryId={ this.state.filter.categoryId } onChange={ this.onChangeFilter.bind(this, 'categoryId') } />
								<SelectSizeLego categoryId={ this.state.filter.categoryId } sizeId={ this.state.filter.sizeId } onChange={ this.onChangeFilter.bind(this, 'sizeId') } />
							</HeaderFilterComponent>
							<TableLego isLoading={this.state.isLoading}
								headers={this.tableHeader}
								rows={this.state.inventories.map(this.rowRenderer)}
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
