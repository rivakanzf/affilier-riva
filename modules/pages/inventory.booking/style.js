import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'utils/constants/color'


export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	padder: {
		marginBottom: 16,
	},

	product: {
		justifyContent: 'flex-start',
		alignItems: 'center',
	},

	image: {
		width: 60,
		height: 60,
		marginRight: 16,
	},

	box: {
		overflow: 'hidden',
	},

	title: {
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	center: {
		justifyContent: 'center',
	},

	icon: {
		alignItems: 'flex-end',
	},

})
