import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import BrandManager from 'app/managers/brand';

import UtilitiesContext from 'coeur/contexts/utilities';

import ButtonBit from 'modules/bits/button';
import InputValidatedBit from 'modules/bits/input.validated';
import ScrollViewBit from 'modules/bits/scroll.view';

import BoxLego from 'modules/legos/box';
import TableLego from 'modules/legos/table';

import EditBrandPagelet from 'modules/pagelets/edit.brand';
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

// import _ from 'lodash';


export default ConnectHelper(
	class AddBrandPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				onUpdate: PropTypes.func.isRequired,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				brands: state.brands,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				total: 0,
				selectedBrandId: undefined,
				brandIds: [],
			}, [
				'getData',
				'onSelectBrand',
				'onAddNewBrand',
				'onModalRequestClose',
				'onUpdateAndSelect',
			])
		}

		// componentWillMount() {
		// 	this.setState({
		// 		isLoading: true,
		// 	}, () => {
		// 		BrandManager.getAll().then(result => {
		// 			this.setState({
		// 				total: result.count,
		// 				brandIds: result.data.map(brand => brand.id),
		// 				isLoading: false,
		// 			})
		// 		})
		// 	})
		// }

		getData(title) {
			if(!this.state.isLoading) {
				return new Promise(res => {
					this.setState({
						isLoading: true,
					}, () => {
						BrandManager.searchByTitle({
							title,
						}).then(result => {
							this.setState({
								total: result.count,
								brandIds: result.data.map(brand => brand.id),
								isLoading: false,
							})

							res(result)
						})
					})
				})
			}

			return Promise.reject(false)
		}

		onSelectBrand(brandId) {
			this.setState({
				selectedBrandId: brandId,
			}, () => {
				this.props.onUpdate(brandId, true)
			})
		}

		onUpdateAndSelect(brand) {
			this.getData().then(() => {
				this.onSelectBrand(brand.id)
			})
		}

		onAddNewBrand() {
			this.props.utilities.alert.modal({
				component: (
					<EditBrandPagelet
						paths={[{
							title: 'Inventory',
							onPress: this.onModalRequestClose,
						}, {
							title: 'Add Brand',
						}]}
						onUpdate={ this.onUpdateAndSelect }
						onRequestClose={ this.onModalRequestClose }
					/>
				),
				closeOnOverlayPress: true,
			})
		}

		onModalRequestClose() {
			this.props.utilities.alert.hide()
		}

		view() {
			return (
				<ScrollViewBit unflex style={this.props.style} contentContainerStyle={ Styles.content } style={Styles.container}>
					<BoxLego title="Select Brand" contentContainerStyle={Styles.box}>
						<InputValidatedBit
							disabled isRequired
							updater={ this.state.selectedBrandId }
							id={ 'brandId' }
							title={ 'Brand ID' }
							description={ 'Select one of the brands below' }
							type={ FormPagelet.TYPES.TITLE }
							value={ this.state.selectedBrandId ? this.state.selectedBrandId + '' : undefined }
						/>
						<ButtonBit
							icon="expand"
							title={ 'Add Brand' }
							type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
							width={ ButtonBit.TYPES.WIDTHS.FIT }
							onPress={ this.onAddNewBrand }
							style={ Styles.button }
						/>
					</BoxLego>
					<TableLego paging searchable
						onSearch={ this.getData }
						isSearching={ this.state.isLoading }
						// current={}
						total={ this.state.total }
						// onGoToFirst: PropTypes.func,
						// onGoToPrev: PropTypes.func,
						// onGoToNext: PropTypes.func,
						// onGoToLast: PropTypes.func,
						isLoading={ this.state.isLoading }
						headers={[{
							width: 1,
							title: 'ID',
						}, {
							width: 9,
							title: 'Brand Name',
						}]}
						rows={this.state.brandIds.map(brandId => {
							const brand = BrandManager.get(brandId)

							return {
								data: [{
									title: brandId,
								}, {
									title: brand.title,
								}],
								onPress: this.onSelectBrand.bind(this, brandId),
								style: this.state.selectedBrandId === brandId ? Styles.rowSelected : undefined,
							}
						})}
					/>
				</ScrollViewBit>
			)
		}
	}
)
