import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import PickupPointManager from 'app/managers/pickup.point';

import UtilitiesContext from 'coeur/contexts/utilities';

import ButtonBit from 'modules/bits/button';
import InputValidatedBit from 'modules/bits/input.validated';
import ScrollViewBit from 'modules/bits/scroll.view';

import BoxLego from 'modules/legos/box';
import TableLego from 'modules/legos/table';

import EditPickupPointPagelet from 'modules/pagelets/edit.pickup.point';
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class AddPickupPointPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				brandId: PropTypes.id,
				onUpdate: PropTypes.func.isRequired,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				pickuppoints: state.pickuppoints,
			}
		}

		static defaultProps = {
			brandId: 1,
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				selectedPickupPointId: undefined,
				pickupPointIds: [],
			}, [
				'onSelectPickupPoint',
				'onAddNewPickupPoint',
				'onModalRequestClose',
				'onUpdateAndSelect',
			])
		}

		componentWillMount() {
			if(this.props.brandId) {
				this.setState({
					isLoading: true,
				}, () => {
					PickupPointManager.searchByBrandId({
						brandId: this.props.brandId,
					}).then(pickupPoints => {
						this.setState({
							pickupPointIds: pickupPoints.map(pickupPoint => pickupPoint.id),
							isLoading: false,
						})
					})
				})
			}
		}

		onSelectPickupPoint(pickupPointId) {
			this.setState({
				selectedPickupPointId: pickupPointId,
			}, () => {
				this.props.onUpdate(pickupPointId, true)
			})
		}

		onUpdateAndSelect(pickupPoint) {
			this.getData().then(() => {
				this.onSelectPickupPoint(pickupPoint.id)
			})
		}

		onAddNewPickupPoint() {
			this.props.utilities.alert.modal({
				component: (
					<EditPickupPointPagelet
						brandId={ this.props.brandId }
						paths={[{
							title: 'Inventory',
							onPress: this.onModalRequestClose,
						}, {
							title: 'Add Pickup Point',
						}]}
						onUpdate={ this.onUpdateAndSelect }
						onRequestClose={ this.onModalRequestClose }
					/>
				),
				closeOnOverlayPress: true,
			})
		}

		onModalRequestClose() {
			this.props.utilities.alert.hide()
		}

		view() {
			return (
				<ScrollViewBit unflex style={this.props.style} contentContainerStyle={ Styles.content }>
					<BoxLego title="Select Pickup Point" contentContainerStyle={Styles.box}>
						<InputValidatedBit
							disabled isRequired
							updater={ this.state.selectedPickupPointId }
							id={ 'pickupPointId' }
							title={ 'Pickup Point ID' }
							description={ 'Select one of the pickup points below' }
							type={ FormPagelet.TYPES.TITLE }
							value={ this.state.selectedPickupPointId ? this.state.selectedPickupPointId + '' : undefined }
						/>
						<ButtonBit
							icon="expand"
							title={ 'Add Pickup Point' }
							type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
							width={ ButtonBit.TYPES.WIDTHS.FIT }
							onPress={ this.onAddNewPickupPoint }
							style={ Styles.button }
						/>
					</BoxLego>
					<TableLego
						isLoading={ this.state.isLoading }
						headers={[{
							width: 1,
							title: 'ID',
						}, {
							width: 5,
							title: 'Title',
						}, {
							width: 5,
							title: 'Description',
						}]}
						rows={this.state.pickupPointIds.map(pickupPointId => {
							const pickupPoint = PickupPointManager.get(pickupPointId)

							return {
								data: [{
									title: pickupPointId,
								}, {
									title: pickupPoint.title,
								}, {
									title: pickupPoint.description,
								}],
								onPress: this.onSelectPickupPoint.bind(this, pickupPointId),
								style: this.state.selectedPickupPointId === pickupPointId ? Styles.rowSelected : undefined,
							}
						})}
					/>
				</ScrollViewBit>
			)
		}
	}
)
