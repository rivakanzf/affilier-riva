import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import ProductManager from 'app/managers/product';

import UtilitiesContext from 'coeur/contexts/utilities';

import StringHelper from 'coeur/helpers/string';
import ButtonBit from 'modules/bits/button';
import LinkBit from 'modules/bits/link';
import InputValidatedBit from 'modules/bits/input.validated';
import ScrollViewBit from 'modules/bits/scroll.view';

import BoxLego from 'modules/legos/box';
import TableLego from 'modules/legos/table';

import EditProductPagelet from 'modules/pagelets/edit.product';
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class AddProductPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				brandId: PropTypes.id,
				onUpdate: PropTypes.func.isRequired,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				products: state.products,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				total: 0,
				selectedProductId: undefined,
				productIds: [],
			}, [
				'getData',
				'onSelectProduct',
				'onAddNewProduct',
				'onModalRequestClose',
				'onUpdateAndSelect',
			])
		}

		componentWillMount() {
			if(this.props.brandId) {
				this.setState({
					isLoading: true,
				}, () => {
					ProductManager.searchByTitle({
						brandId: this.props.brandId,
					}).then(result => {
						this.setState({
							total: result.count,
							productIds: result.data.map(product => product.id),
							isLoading: false,
						})
					})
				})
			}
		}

		getData(title) {
			if(!this.state.isLoading) {
				return new Promise(res => {
					this.setState({
						isLoading: true,
					}, () => {
						ProductManager.searchByTitle({
							title,
							brandId: this.props.brandId,
						}).then(result => {
							this.setState({
								total: result.count,
								productIds: result.data.map(product => product.id),
								isLoading: false,
							})

							res(result)
						})
					})
				})
			}

			return Promise.reject(false)
		}

		onSelectProduct(productId) {
			this.setState({
				selectedProductId: productId,
			}, () => {
				this.props.onUpdate(productId, true)
			})
		}

		onUpdateAndSelect(product) {
			this.getData().then(() => {
				this.onSelectProduct(product.id)
			})
		}

		onAddNewProduct() {
			this.props.utilities.alert.modal({
				component: (
					<EditProductPagelet
						brandId={ this.props.brandId }
						paths={[{
							title: 'Inventory',
							onPress: this.onModalRequestClose,
						}, {
							title: 'Add Product',
						}]}
						onUpdate={ this.onUpdateAndSelect }
						onRequestClose={ this.onModalRequestClose }
					/>
				),
				closeOnOverlayPress: true,
			})
		}

		onModalRequestClose() {
			this.props.utilities.alert.hide()
		}

		view() {
			return (
				<ScrollViewBit unflex style={this.props.style} contentContainerStyle={ Styles.content } style={ Styles.container }>
					<BoxLego title="Select Product" contentContainerStyle={Styles.box}>
						<InputValidatedBit
							disabled isRequired
							updater={ this.state.selectedProductId }
							id={ 'productId' }
							title={ 'Product ID' }
							description={ 'Select one of the products below' }
							type={ FormPagelet.TYPES.TITLE }
							value={ this.state.selectedProductId ? this.state.selectedProductId + '' : undefined }
						/>
						<ButtonBit
							icon="expand"
							title={ 'Add Product' }
							type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
							width={ ButtonBit.TYPES.WIDTHS.FIT }
							onPress={ this.onAddNewProduct }
							style={ Styles.button }
						/>
					</BoxLego>
					<TableLego paging searchable
						onSearch={ this.getData }
						isSearching={ this.state.isLoading }
						// current={}
						total={ this.state.total }
						// onGoToFirst: PropTypes.func,
						// onGoToPrev: PropTypes.func,
						// onGoToNext: PropTypes.func,
						// onGoToLast: PropTypes.func,
						isLoading={ this.state.isLoading }
						headers={[{
							width: 1,
							title: 'ID',
						}, {
							width: 9,
							title: 'Product Name',
						}, {
							width: 6,
							title: 'URL',
						}]}
						rows={this.state.productIds.map(productId => {
							const product = ProductManager.get(productId)

							return {
								data: [{
									title: productId,
								}, {
									title: product.title,
								}, product.url ? {
									children: (
										<LinkBit target={ LinkBit.TYPES.BLANK } href={ product.url }>
											{ StringHelper.ellipsize(product.url, 50) }
										</LinkBit>
									),
								} : {
									title: '-',
								}],
								onPress: this.onSelectProduct.bind(this, productId),
								style: this.state.selectedProductId === productId ? Styles.rowSelected : undefined,
							}
						})}
					/>
				</ScrollViewBit>
			)
		}
	}
)
