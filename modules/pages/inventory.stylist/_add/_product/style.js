import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	content: {
		paddingTop: 16,
		paddingBottom: 0,
		paddingLeft: 32,
		paddingRight: 32,
		flexShrink: 1,
		alignSelf: 'flex-end',
		width: '100vw',
	},

	container: {
		width: '100vw',
	},

	box: {
		marginBottom: 16,
	},

	rowSelected: {
		backgroundColor: Colors.yellow.primary,
	},
})
