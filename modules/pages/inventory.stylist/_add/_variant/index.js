import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import VariantManager from 'app/managers/variant';

import UtilitiesContext from 'coeur/contexts/utilities';

import FormatHelper from 'coeur/helpers/format';

import ButtonBit from 'modules/bits/button';
import InputValidatedBit from 'modules/bits/input.validated';
import ScrollViewBit from 'modules/bits/scroll.view';

import BoxLego from 'modules/legos/box';
import TableLego from 'modules/legos/table';

import EditVariantPagelet from 'modules/pagelets/edit.variant';
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class AddVariantPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				productId: PropTypes.id,
				onUpdate: PropTypes.func.isRequired,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				variants: state.variants,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				total: 0,
				selectedVariantId: undefined,
				variantIds: [],
			}, [
				'getData',
				'onSelectVariant',
				'onAddNewVariant',
				'onModalRequestClose',
				'onUpdateAndSelect',
			])
		}

		componentWillMount() {
			if(this.props.productId) {
				this.setState({
					isLoading: true,
				}, () => {
					VariantManager.searchByProductId({
						productId: this.props.productId,
					}).then(result => {
						this.setState({
							total: result.count,
							variantIds: result.data.map(variant => variant.id),
							isLoading: false,
						})
					})
				})
			}
		}

		getData(search) {
			if(!this.state.isLoading) {
				return new Promise(res => {
					this.setState({
						isLoading: true,
					}, () => {
						VariantManager.searchByProductId({
							search,
							productId: this.props.productId,
						}).then(result => {
							this.setState({
								total: result.count,
								variantIds: result.data.map(variant => variant.id),
								isLoading: false,
							})

							res(result)
						})
					})
				})
			}

			return Promise.reject(false)
		}

		onSelectVariant(variantId) {
			this.setState({
				selectedVariantId: variantId,
			}, () => {
				this.props.onUpdate(variantId, true)
			})
		}

		onUpdateAndSelect(variant) {
			this.getData().then(() => {
				this.onSelectVariant(variant.id)
			})
		}

		onAddNewVariant() {
			this.props.utilities.alert.modal({
				component: (
					<EditVariantPagelet
						productId={ this.props.productId }
						paths={[{
							title: 'Inventory',
							onPress: this.onModalRequestClose,
						}, {
							title: 'Add Variant',
						}]}
						onUpdate={ this.onUpdateAndSelect }
						onRequestClose={ this.onModalRequestClose }
					/>
				),
				closeOnOverlayPress: true,
			})
		}

		onModalRequestClose() {
			this.props.utilities.alert.hide()
		}

		view() {
			return (
				<ScrollViewBit unflex style={this.props.style} contentContainerStyle={ Styles.content }>
					<BoxLego title="Select Variant" contentContainerStyle={Styles.box}>
						<InputValidatedBit
							disabled isRequired
							updater={ this.state.selectedVariantId }
							id={ 'variantId' }
							title={ 'Variant ID' }
							description={ 'Select one of the variants below' }
							type={ FormPagelet.TYPES.TITLE }
							value={ this.state.selectedVariantId ? this.state.selectedVariantId + '' : undefined }
						/>
						<ButtonBit
							icon="expand"
							title={ 'Add Variant' }
							type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
							width={ ButtonBit.TYPES.WIDTHS.FIT }
							onPress={ this.onAddNewVariant }
							style={ Styles.button }
						/>
					</BoxLego>
					<TableLego paging searchable
						onSearch={ this.getData }
						isSearching={ this.state.isLoading }
						// current={}
						total={ this.state.total }
						// onGoToFirst: PropTypes.func,
						// onGoToPrev: PropTypes.func,
						// onGoToNext: PropTypes.func,
						// onGoToLast: PropTypes.func,
						isLoading={ this.state.isLoading }
						headers={[{
							width: 1,
							title: 'ID',
						}, {
							width: 5,
							title: 'Color',
						}, {
							width: 5,
							title: 'Size',
						}, {
							width: 3,
							title: 'Price',
						}]}
						rows={this.state.variantIds.map(variantId => {
							const variant = VariantManager.get(variantId)

							return {
								data: [{
									title: variantId,
								}, {
									title: variant.color,
								}, {
									title: variant.size,
								}, {
									title: FormatHelper.currencyFormat(variant.price),
								}],
								onPress: this.onSelectVariant.bind(this, variantId),
								style: this.state.selectedVariantId === variantId ? Styles.rowSelected : undefined,
							}
						})}
					/>
				</ScrollViewBit>
			)
		}
	}
)
