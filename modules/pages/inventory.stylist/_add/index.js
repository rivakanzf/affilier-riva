import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ScrollSnapBit from 'modules/bits/scroll.snap';

import DetailPagelet from 'modules/pagelets/detail'
import EditInventoryPagelet from 'modules/pagelets/edit.inventory';

import AddBrandPart from './_brand';
import AddPickupPointPart from './_pickup.point';
import AddProductPart from './_product';
import AddVariantPart from './_variant';

import Styles from './style';


export default ConnectHelper(
	class AddPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				onRequestClose: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				brands: state.brands,
				products: state.products,
				variants: state.variants,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				index: 0,
				data: {
					brandId: undefined,
					productId: undefined,
					variantId: undefined,
				},
				isDone: {
					'0': false,
					'1': false,
					'2': false,
					'3': false,
				},
			}, [
				'onUpdatePickupPoint',
				'onUpdateBrand',
				'onUpdateProduct',
				'onUpdateVariant',
				'onUpdateInventory',
				'onBack',
				'onNext',
				// 'onEditBrand',
				// 'onEditPickupPoint',
				// 'onDoneEditPickupPoint',
			])

			this._updates = {}
		}

		componentWillMount() {
		}

		canNext() {
			return !!this.state.isDone[this.state.index]
		}

		canSave() {
			return Object.values(this.state.isDone).findIndex(isDone => !isDone) === -1
		}

		onUpdatePickupPoint(pickupPointId, isDone) {
			this.setState({
				data: {
					...this.state.data,
					pickupPointId,
				},
				isDone: {
					...this.state.isDone,
					[ this.state.index ]: isDone,
				},
			})
		}

		onUpdateBrand(brandId, isDone) {
			this.setState({
				data: {
					...this.state.data,
					brandId,
				},
				isDone: {
					...this.state.isDone,
					[ this.state.index ]: isDone,
				},
			})
		}

		onUpdateProduct(productId, isDone) {
			this.setState({
				data: {
					...this.state.data,
					productId,
				},
				isDone: {
					...this.state.isDone,
					[ this.state.index ]: isDone,
				},
			})
		}

		onUpdateVariant(variantId, isDone) {
			this.setState({
				data: {
					...this.state.data,
					variantId,
				},
				isDone: {
					...this.state.isDone,
					[ this.state.index ]: isDone,
				},
			})
		}

		onUpdateInventory(inventoryOrInventories) {
			//
			// this.props.utilities.notification.show({
			// 	title: '👌🏻',
			// 	message: 'Inventory created!',
			// 	type: this.props.utilities.notification.TYPES.SUCCESS,
			// })
			//
			this.props.utilities.alert.hide()
		}

		onBack() {
			this.setState({
				index: this.state.index - 1,
			})
		}

		onNext() {
			if(this.canSave() && this.state.index === 3) {
				this.props.utilities.alert.modal({
					component: (
						<EditInventoryPagelet
							paths={[{
								title: 'Inventory',
							}, {
								title: 'Add Inventory',
							}]}
							variantId={ this.state.data.variantId }
							pickupPointId={ this.state.data.pickupPointId }
							onUpdate={ this.onUpdateInventory }
						/>
					),
				})
			} else {
				this.setState({
					index: this.state.index + 1,
				})
			}
		}

		footerRenderer() {
			return (
				<BoxBit unflex row>
					<ButtonBit
						title={ 'BACK' }
						type={ ButtonBit.TYPES.SHAPES.RECTANGLE }
						width={ ButtonBit.TYPES.WIDTHS.FIT }
						state={ this.state.index === 0 ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onBack }
					/>
					<ButtonBit
						title={ 'NEXT' }
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
						state={ this.state.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.canNext() && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
						onPress={ this.onNext }
						style={ Styles.button }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<DetailPagelet
					paths={[{
						title: 'Inventories',
						onPress: this.props.onRequestClose,
					}, {
						title: 'Add Inventory',
					}]}
					style={Styles.content}
					footer={ this.footerRenderer() }
					contentContainerStyle={ Styles.container }
				>
					<ScrollSnapBit index={ this.state.index } contentContainerStyle={Styles.scroller} disableGesture>
						<AddPickupPointPart
							onUpdate={ this.onUpdatePickupPoint }
							style={ Styles.content }
						/>
						<AddBrandPart
							onUpdate={ this.onUpdateBrand }
							style={ Styles.content }
						/>
						<AddProductPart
							key={ this.state.data.brandId }
							brandId={ this.state.data.brandId }
							onUpdate={ this.onUpdateProduct }
							style={ Styles.content }
						/>
						<AddVariantPart
							key={ this.state.data.productId }
							productId={ this.state.data.productId }
							onUpdate={ this.onUpdateVariant }
							style={ Styles.content }
						/>
					</ScrollSnapBit>
				</DetailPagelet>
			)
		}
	}
)
