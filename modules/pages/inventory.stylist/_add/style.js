import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'
// import Styles from 'coeur/constants/style'

export default StyleSheet.create({
	container: {
		padding: 0,
	},

	content: {
		width: '100vw',
	},

	scroller: {
		flexShrink: 1,
	},

	button: {
		marginLeft: 16,
	},

	'@media (max-width: 1366px)': {
		content: {
			width: '100vw',
		},
	},
})
