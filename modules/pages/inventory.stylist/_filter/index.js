import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';

import Colors from 'coeur/constants/color';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

import { isEqual, isEmpty, difference, capitalize } from 'lodash';

function toMap(sum, current) {
	return {
		...sum,
		[current.id]: current,
	}
}

function modifyFilter(filters, type, newIds) {
	const _filters = filters.map(f => {
		if(f.type !== type) {
			return f
		} else if(f.type === type && newIds.indexOf(f.value) === -1) {
			return false
		} else {
			return f
		}
	}).filter(f => f !== false)

	return _filters.concat(difference(newIds, _filters.filter(f => f.type === type).map(f => f.value)).map(id => {
		return {
			type,
			value: id,
		}
	}))
}

let cache = null

export default ConnectHelper(
	class FilterPart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				price: PropTypes.arrayOf(PropTypes.number),
				brandIds: PropTypes.arrayOf(PropTypes.id),
				colorIds: PropTypes.arrayOf(PropTypes.id),
				sizeIds: PropTypes.arrayOf(PropTypes.id),
				onRemoveFilter: PropTypes.func,
				style: PropTypes.style,
			};
		}

		static propsToQuery(state) {
			if(cache !== null) {
				return null
			} else {
				return [`query {
					colorsList {
						id
						title
						hex
						image
					}
					sizesList {
						id
						title
					}
					brandsList {
						id
						title
					}
				}`, {}, state.me.token]
			}
		}

		static getDerivedStateFromProps(nP, nS) {

			// maybe because cache
			if(isEmpty(nP.data) && cache === null) {
				return null
			} else if(cache === null) {
				cache = nP.data
			}

			const isPriceChanged = !isEqual(nP.filter.price, nS.lastFilterProps.price)
			const isBrandChanged = !isEqual(nP.filter.brandIds, nS.lastFilterProps.brandIds)
			const isColorChanged = !isEqual(nP.filter.colorIds, nS.lastFilterProps.colorIds)
			const isSizeChanged  = !isEqual(nP.filter.sizeIds, nS.lastFilterProps.sizeIds)

			if(
				isPriceChanged
				|| isBrandChanged
				|| isColorChanged
				|| isSizeChanged
			) {
				// find changes
				let filters = nS.filters.slice()

				if(isPriceChanged) {
					filters.splice(filters.findIndex(f => f.type === 'price'), 1)

					if(nP.filter.price.length) {
						filters.push({
							type: 'price',
							value: nP.filter.price,
						})
					}
				}

				if(isBrandChanged) {
					filters = modifyFilter(filters, 'brand', nP.filter.brandIds)
				}

				if(isColorChanged) {
					filters = modifyFilter(filters, 'color', nP.filter.colorIds)
				}

				if(isSizeChanged) {
					filters = modifyFilter(filters, 'size', nP.filter.sizeIds)
				}

				return {
					lastFilterProps: nP.filter,
					filters,
					...(isEmpty(nS.brands) ? {
						brands: cache.brandsList.reduce(toMap, {}),
						colors: cache.colorsList.reduce(toMap, {}),
						sizes: cache.sizesList.reduce(toMap, {}),
					} : {}),
				}
			} else {
				return null
			}
		}

		constructor(p) {
			super(p, {
				brands: {},
				colors: {},
				sizes: {},
				lastFilterProps: {},
				filters: [],
			});
		}

		tagRenderer = (title, type, id, index) => {
			return (
				<BoxBit key={index} unflex row style={Styles.tag}>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.title} >
						{ title }
					</GeomanistBit>
					<TouchableBit unflex onPress={this.props.onRemoveFilter && this.props.onRemoveFilter.bind(this, type, id)}>
						<IconBit
							name="close"
							size={16}
							color={Colors.primary}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		filterRenderer = ({type, value}, i) => {
			switch(type) {
			case 'price':
				return this.tagRenderer(value.length > 1 ? `IDR ${value.map(v => FormatHelper.currency(v)).join(' - ')}` : `> IDR ${value[0]}`, 'price', value, i)
			case 'brand':
				return this.tagRenderer(this.state.brands[value].title, 'brandIds', value, i)
			case 'color':
				return this.tagRenderer(capitalize(this.state.colors[value].title), 'colorIds', value, i)
			case 'size':
			default:
				return this.tagRenderer(this.state.sizes[value].title, 'sizeIds', value, i)
			}
		}

		view() {
			return (
				<BoxBit row style={[Styles.wrap, this.props.style]}>
					{ this.state.filters.length ? this.state.filters.map(this.filterRenderer) : (
						<GeomanistBit weight="normal" type={GeomanistBit.TYPES.SUBHEADER_1} style={Styles.empty}>No filter selected</GeomanistBit>
					) }
				</BoxBit>
			);
		}
	}
);
