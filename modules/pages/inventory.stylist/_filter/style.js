import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	wrap: {
		flexWrap: 'wrap',
		margin: -2,
	},
	tag: {
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.primary,
		alignItems: 'center',
		paddingLeft: 12,
		paddingRight: 8,
		paddingTop: 4,
		paddingBottom: 4,
		borderRadius: 2,
		margin: 2,
	},
	title: {
		color: Colors.primary,
		paddingRight: 6,
		margin: 2,
	},
	empty: {
		color: Colors.black.palette(1, .4),
	},
})
