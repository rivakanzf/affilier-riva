import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';
// import Sizes from 'coeur/constants/size';
// import Colors from 'coeur/constants/color';

// import MatchboxManager from 'app/managers/matchbox';
// import VolatileManager from 'app/managers/volatile';
import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import ScrollViewBit from 'modules/bits/scroll.view';
// import ShadowBit from 'modules/bits/shadow';
import TouchableBit from 'modules/bits/touchable';

// import LoaderBit from 'modules/bits/loader';
import CardMediaComponent from 'modules/components/card.media';

import Styles from './style';

export default ConnectHelper(
	class ItemProduct extends QueryStatefulModel {
		static propsToQuery(state, p) {
			return [`query {
				productById(id: ${p.id}) {
					id
					title
					variants {
						id,
						price,
						variantAssets {
							id
							url
						}
						variantColors {
							id
							title
							hex
							color {
								id
								title
								hex
							}
						}
					}
					brand {
						title
					}
				}
			}`, {}, state.me.token]
		}

		static getDerivedStateFromProps(nP) {
			return !nP.isLoading ? ({
				product: nP.data.productById,
			}) : null
		}

		constructor(p) {
			super(p, {
				product: {},
				activeVariant: '',
			}, [
				'onPress',
				'productRenderer',
				'colorRenderer',
			])
		}

		onPress(id) {
			this.props.onGoToDetail &&
			this.props.onGoToDetail(id)
		}

		colorRenderer({id, hex, color}) {
			return (
				<BoxBit key={id} style={[{ backgroundColor: color.hex || hex }]}/>
			);
		}

		productRenderer({id, price, size}) {
			return (
				<BoxBit unflex key={id}>
					<BoxBit unflex row style={Styles.padder}>
						{/* <GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal">
							IDR { FormatHelper.currencyFormat(basePrice) }
						</GeomanistBit>
						<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal">
							{base}
						</GeomanistBit> */}

						{/* { basePrice !== price && ( */}
						<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal" style={Styles.discount}>
							IDR { FormatHelper.currencyFormat(price) }
						</GeomanistBit>
						{/* )} */}

					</BoxBit>

					{ !!size && (
						<BoxBit unflex row centering style={[Styles.space, Styles.padder]}>
							<GeomanistBit type={GeomanistBit.TYPES.SUBHEADER_1} weight="normal" style={Styles.size}>
								{ size.title && size.title.toUpperCase() }
							</GeomanistBit>
						</BoxBit>
					)}
				</BoxBit>
			);
		}

		view() {
			return !this.props.isLoading ? (
				<ScrollViewBit>
					{ !!this.state.product && (
						<TouchableBit unflex style={[Styles.container, this.props.style]} onPress={ this.onPress.bind(this, this.props.id)}>
							{ !!this.state.product.id && (
								<CardMediaComponent
									id={ this.state.product.variants[0].variantAssets[0] && this.state.product.variants[0].variantAssets[0].id}
									style={ Styles.image }
								/>
							)}
							<BoxBit unflex style={Styles.content}>
								{ !!this.state.product.brand && (
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.darkGrey80}>
										{ this.state.product.brand.title }
									</GeomanistBit>
								)}

								{ !!this.state.product.title && (
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.darkGrey80}>
										{ this.state.product.title }
									</GeomanistBit>
								)}
								{ this.state.activeVariant && this.productRenderer(this.state.activeVariant, this.state.base) }
							</BoxBit>

							<BoxBit unflex row style={Styles.containerColor}>
								{ this.state.product.variants && this.state.product.variants.map(({id, variantColors}) => {
									return (
										<BoxBit unflex key={id} unflex style={Styles.contentColor}>
											{ variantColors.map(this.colorRenderer) }
										</BoxBit>
									)
								})}
							</BoxBit>
						</TouchableBit>
					)}
				</ScrollViewBit>
			) : null
		}
	}
);
