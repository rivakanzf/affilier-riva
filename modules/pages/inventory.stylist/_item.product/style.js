import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'utils/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	// container: {
	// 	width: 252,
	// },

	image: {
		height: 336,
	},

	content: {
		paddingTop: 8,
	},

	darkGrey80: {
		color: Colors.black.palette(2, .8),
		wordBreak: 'break-all',
	},

	space: {
		justifyContent: 'space-between',
	},

	size: {
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: 12,
		paddingRight: 12,
		borderWidth: 1,
		borderRadius: 2,
		borderStyle: 'solid',
		borderColor: Colors.grey.palette(1, .2),
	},

	discount: {
		marginLeft: 8,
		color: Colors.red.primary,
	},

	padder: {
		paddingTop: 8,
	},

	padder2: {
		paddingTop: 2,
	},
	containerColor: {
		paddingTop: 12,
		paddingBottom: 8,
	},
	contentColor: {
		width: 24,
		height: 24,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 12,
		marginRight: 8,
		overflow: 'hidden',
		transform: 'rotate(-35deg)',
	},
})
