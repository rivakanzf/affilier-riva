import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color'

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class PaginationPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				current: PropTypes.number,
				count: PropTypes.number,
				total: PropTypes.number,
				onGoToPrevPress: PropTypes.func,
				onGoToNextPress: PropTypes.func,
			}
		}

		static defaultProps = {
			current: 0,
			count: Infinity,
			total: Infinity,
		}

		view() {
			const end = Math.ceil(this.props.total / this.props.count)
			const current = Math.floor(this.props.current / this.props.count) + 1

			return end > 1 ? (
				<BoxBit unflex row centering style={this.props.style}>
					<TouchableBit unflex
						onPress={this.props.onGoToPrevPress}
						accessible={ this.props.current === 0 ? false : true }
					>
						<IconBit
							name="arrow-left-large"
							size={24}
							color={ this.props.current === 0 ? Colors.black.palette(2, .2) : Colors.black.palette(2, .8)}
						/>
					</TouchableBit>
					<GeomanistBit
						type={GeomanistBit.TYPES.SUBHEADER_1}
						weight="normal"
						style={Styles.text}
					>
						{`${current} / ${end}`}
					</GeomanistBit>
					<TouchableBit unflex
						onPress={this.props.onGoToNextPress}
						accessible={ current === end ? false : true }
					>
						<IconBit
							name="arrow-right-large"
							color={ current === end ? Colors.black.palette(2, .2) : Colors.black.palette(2, .8)}
						/>
					</TouchableBit>
				</BoxBit>
			) : false
		}
	}
)
