import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	button: {
		marginLeft: 8,
		marginRight: 8,
	},

	first: {
		marginLeft: 0,
	},

	last: {
		marginRight: 0,
	},

	text: {
		color: Colors.black.palette(1, .7),
		paddingLeft: 6,
		paddingRight: 6,
	},

})
