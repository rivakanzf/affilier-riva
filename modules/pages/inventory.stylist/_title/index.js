import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';

import Styles from './style';

import { isEmpty, capitalize } from 'lodash';

const cache = {}


export default ConnectHelper(
	class TitlePart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				categoryId: PropTypes.id,
				result: PropTypes.number,
			}
		}

		static propsToQuery(state, oP) {
			if(oP.categoryId) {
				return cache[oP.categoryId] ? null : [`query {
					categoryById(id: ${oP.categoryId}) {
						title
					}
				}`, {
					fetchPolicy: 'no-cache',
				}, state.me.token]
			} else {
				return null
			}
		}

		static getDerivedStateFromProps(nP) {
			if (!nP.categoryId) {
				return null
			} else if (!isEmpty(nP.data)) {
				cache[nP.categoryId] = nP.data
			} else if (cache[nP.categoryId]) {
				// check for cache
			} else {
				return null
			}

			return {
				title: cache[nP.categoryId].categoryById.title,
			}
		}

		static defaultProps = {
			result: 0,
		}

		constructor(p) {
			super(p, {
				title: '',
			})
		}

		viewOnLoading() {
			return this.viewRenderer(true)
		}

		viewRenderer(isLoading = false) {
			return isLoading ? (
				<BoxBit row centering unflex>
					<LoaderBit simple />
					<BoxBit />
				</BoxBit >
			) : (
				<BoxBit row unflex>
					<GeomanistBit type={GeomanistBit.TYPES.HEADER_4} style={Styles.title}>
						{ capitalize(this.state.title) }
					</GeomanistBit>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.result}>
						{ this.props.result } results
					</GeomanistBit>
				</BoxBit>
			)
		}

		view() {
			return (this.viewRenderer())
		}
	}
)
