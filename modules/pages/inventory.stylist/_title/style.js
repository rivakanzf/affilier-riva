import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	title: {
		color: Colors.black.palette(1, .9),
	},

	result: {
		color: Colors.black.palette(2, .8),
		paddingLeft: Sizes.margin.default,
		paddingTop: Sizes.margin.default / 2,
	},

})
