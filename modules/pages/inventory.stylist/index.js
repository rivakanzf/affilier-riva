import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'

import CommonHelper from 'coeur/helpers/common'

import ProductService from 'app/services/product'

import Sizes from 'coeur/constants/size'

import BoxBit from 'modules/bits/box'
import CheckboxBit from 'modules/bits/checkbox'
import GeomanistBit from 'modules/bits/geomanist'
import TextInputBit from 'modules/bits/text.input'
import TouchableBit from 'modules/bits/touchable'
import SelectionBit from 'modules/bits/selection'

import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb'
import HeaderStylistLego from 'modules/legos/header.stylist'
import TableGalleryLego from 'modules/legos/table.gallery'

import CardProductStylistComponent from 'modules/components/card.product.stylist'
import PageAuthorizedComponent from 'modules/components/page.authorized'
import SidebarFilterComponent from 'modules/components/sidebar.filter'

import QuickViewProductPagelet from 'modules/pagelets/quick.view.product'

import FilterPart from './_filter'
import PaginationPart from './_pagination'
import TitlePart from './_title'

import Styles from './style'

import { isEqual, isEmpty, without } from 'lodash'


export function getItemWidth(num = 5) {
	return (Sizes.app.width - (240 + Sizes.margin.default + Sizes.margin.default + (Sizes.margin.default * (num - 1)))) / num
}

export default ConnectHelper(
	class InventoryStylistPage extends PageModel {

		static routeName = 'inventory.stylist'

		constructor(p) {
			let column = 4

			if(Sizes.screen.width <= 768) {
				column = 3
			} else if(Sizes.screen.width > 1366) {
				column = 5
			} else if(Sizes.screen.width >= 1920) {
				column = 7
			}

			const query = CommonHelper.getQueryString()

			super(p, {
				isMenuVisible: false,
				isLoading: false,
				offset: query.offset || 0,
				count: column * 8,
				total: 0,
				search: query.search || undefined,
				categoryId: query.categoryId || null,
				column,
				products: [],
				filter: {
					price: query.price ? CommonHelper.asArray(query.price) : [],
					colorIds: query.colorIds ? CommonHelper.asArray(query.colorIds) : [],
					brandIds: query.brandIds ? CommonHelper.asArray(query.brandIds) : [],
					sizeIds: query.sizeIds ? CommonHelper.asArray(query.sizeIds) : [],
				},
				token: undefined,
				available: true,
				sortBy: {},
			})

			this.sortOptions = [{
				title: 'SORT BY',
				key: 'default',
				selected: true,
			}, {
				title: 'PRICE - High to Low',
				key: {sort_by_price: 'DESC'},
			}, {
				title: 'PRICE - Low to High',
				key: {sort_by_price: 'ASC'},
			}, {
				title: 'DATE - Newest to Oldest',
				key: {sort_by_created_at: 'DESC'},
			}, {
				title: 'DATE - Oldest to Newest',
				key: {sort_by_created_at: 'ASC'},
			}]

			this.getterId = 1
			this.itemWidth = getItemWidth(column)
			this.categoryChildren = {}
			this.que = false
		}

		componentDidUpdate(pP, pS) {
			if (
				this.state.token && (
					pS.token !== this.state.token
					|| !isEqual(pS.categoryId, this.state.categoryId)
					|| !isEqual(pS.offset, this.state.offset)
					|| !isEqual(pS.count, this.state.count)
					|| !isEqual(pS.search, this.state.search)
					|| !isEqual(pS.filter.price, this.state.filter.price)
					|| !isEqual(pS.filter.colorIds, this.state.filter.colorIds)
					|| !isEqual(pS.filter.brandIds, this.state.filter.brandIds)
					|| !isEqual(pS.filter.sizeIds, this.state.filter.sizeIds)
					|| !isEqual(pS.filter.merchants, this.state.filter.merchants)
					|| pS.available !== this.state.available
				)
			) {
				const query = CommonHelper.stripUndefined({
					categoryId: this.state.categoryId || undefined,
					offset: this.state.offset || undefined,
					count: this.state.count || undefined,
					search: this.state.search || undefined,
					price: this.state.filter.price.join(',') || undefined,
					colorIds: this.state.filter.colorIds.join(',') || undefined,
					brandIds: this.state.filter.brandIds.join(',') || undefined,
					sizeIds: this.state.filter.sizeIds.join(',') || undefined,
				})

				window.history.replaceState({}, 'Inventory', isEmpty(query) ? '/stylist-inventory' : `/stylist-inventory?${Object.keys(query).map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		getData = () => {
			if(!isEmpty(this.categoryChildren) && this.state.categoryId) {
				const {
					offset,
					categoryId,
					filter,
					search,
					sortBy,
				} = this.state

				this.getterId++;

				const id = this.getterId

				this.setState({
					isLoading: true,
				}, () => {
					ProductService.getAllProduct({
						limit: this.state.count,
						offset,
						...(!!search ? { search } : {} ),
						...sortBy,
						category_ids: (this.categoryChildren[categoryId] || [categoryId]).join(','),
						...(filter.price.length ? { price: filter.price.join('-') } : {}),
						...(filter.colorIds.length ? { color_ids: filter.colorIds.join(',') } : {}),
						...(filter.brandIds.length ? { brand_ids: filter.brandIds.join(',') } : {}),
						...(filter.sizeIds.length ? { size_ids: filter.sizeIds.join(',') } : {}),
						...(filter.merchants ? { merchants: filter.merchants.join(',') } : {}),

						inventory: this.state.available,
					}, this.state.token)
						.then(res => {
							if (id === this.getterId) {
								this.setState({
									isLoading: false,
									total: res.count,
									products: res.data,
								})
							}
						})
						.catch(err => {
							this.warn(err)

							this.setState({
								isLoading: false,
							}, () => {
								this.utilities.notification.show({
									title: 'Oops',
									message: 'Something went wrong, please try again later',
								})
							})
						})
				})
			} else {
				this.que = true
			}
		}

		onNavigateToDetail = (productId, variantId) => {
			this.utilities.alert.modal({
				component: (
					<QuickViewProductPagelet
						id={ productId }
						variantId={ variantId }
						includeZeroInventory
						mayBook
						mayAddToStylesheet
						mayAddToStylecard
					/>
				),
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onResetFilter = () => {
			this.setState({
				offset: 0,
				filter: {
					price: [],
					colorIds: [],
					brandIds: [],
					sizeIds: [],
				},
			})
		}

		onToggleAvailability = () => {
			this.setState({
				available: !this.state.available,
			})
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onRemoveFilter = (key, id) => {
			const filter = { ...this.state.filter }

			if(key === 'price') {
				filter[key] = []
			} else {
				filter[key] = without(filter[key], id)
			}

			this.setState({
				offset: 0,
				filter,
			})
		}

		onCategoryChildrenLoaded = (categoryChildren) => {
			this.categoryChildren = categoryChildren

			if(this.que && this.state.token) {
				this.que = false
				this.getData()
			}
		}

		onGoToPrev = () => {
			this.setState({
				offset: Math.max(0, this.state.offset - this.state.count),
			})
		}

		onGoToNext = () => {
			this.setState({
				offset: Math.min(this.state.total, this.state.offset + this.state.count),
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeCategory = id => {
			this.setState({
				offset: 0,
				categoryId: id,
			})
		}

		onToggleMenuVisibility = () => {
			this.setState({
				isMenuVisible: !this.state.isMenuVisible,
			})
		}

		onChangeSort = (key) => {
			if(key !== 'default') {
				this.setState({
					sortBy: key,
				}, () => {
					this.getData()
				})
			} else {
				this.setState({
					sortBy: false,
				})
			}
		}

		itemRenderer = item => {
			if(item.variants.length > 0) {
				return (
					<CardProductStylistComponent
						id={ item.id }
						key={ item.id }
						categoryId={ item.category_id }
						width={ this.itemWidth }
						brand={ item.brand }
						title={ item.title }
						description={ item.description }
						colors={ item.colors }
						sizes={ item.sizes }
						variants={ item.variants }
						onPress={ this.onNavigateToDetail }
						haveConsignmentStock={ item.variants && item.variants[0].have_consignment_stock}
					/>
				)
			}
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			}, this.getData)
		}

		view() {
			return (
				<PageAuthorizedComponent
					isMenuVisible={ this.state.isMenuVisible }
					header={(
						<HeaderStylistLego
							categoryId={ this.state.categoryId }
							search={ this.state.search }
							closer={ this.state.isMenuVisible }
							onSearch={ this.onSearch }
							onShowMenu={ this.onToggleMenuVisibility }
							onChangeCategory={ this.onChangeCategory }
						/>
					)}
					roles="stylist"
					onAuthorized={ this.onAuthorized }
				>
					{ () => (
						<BoxBit style={ Styles.main }>
							<SidebarFilterComponent
								key={ `${ this.routeName }|${ this.state.available }` }
								header={(
									<React.Fragment>
										<HeaderBreadcrumbLego
											paths={[{
												title: 'STYLIST INVENTORY',
											}, {
												title: 'Inventory',
											}]}
										/>
										<TouchableBit unflex row onPress={ this.onToggleAvailability } style={ Styles.checker }>
											<CheckboxBit dumb onPress={ this.onToggleAvailability } isActive={ this.state.available } />
											<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.check }>Available Only</GeomanistBit>
										</TouchableBit>
										<TextInputBit
											placeholder="Search product"
											onSubmitEditing={ this.onSearch }
											style={ Styles.input }
										/>
										<SelectionBit
											options={this.sortOptions}
											style={Styles.sortBy}
											onChange={ this.onChangeSort }
										/>
									</React.Fragment>
								)}
								filter={ this.state.filter }
								categoryId={ this.state.categoryId }
								onChangeCategory={ this.onChangeCategory }
								onChangeFilter={ this.onChangeFilter }
								onResetFilter={ this.onResetFilter }
								onCategoryChildrenLoaded={ this.onCategoryChildrenLoaded }
							/>
							<TitlePart categoryId={ this.state.categoryId } result={ this.state.total } />
							<BoxBit unflex row style={ Styles.padder }>
								<FilterPart filter={ this.state.filter } onRemoveFilter={ this.onRemoveFilter } style={ Styles.tags } />
								<PaginationPart
									current={ this.state.offset }
									count={ this.state.count }
									total={ this.state.total }
									onGoToPrevPress={ this.onGoToPrev }
									onGoToNextPress={ this.onGoToNext }
								/>
							</BoxBit>
							<TableGalleryLego
								isSearching={ this.state.isLoading }
								column={ this.state.column }
								rows={ this.state.products }
								itemRenderer={ this.itemRenderer }
							/>
							<BoxBit unflex row style={ Styles.padder }>
								<BoxBit />
								<PaginationPart
									current={ this.state.offset }
									count={ this.state.count }
									total={ this.state.total }
									onGoToPrevPress={ this.onGoToPrev }
									onGoToNextPress={ this.onGoToNext }
								/>
							</BoxBit>
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
