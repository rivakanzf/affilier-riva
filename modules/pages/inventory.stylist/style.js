import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	main: {
		marginTop: 53,
		paddingLeft: 240 + Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		justifyContent: 'space-between',
	},

	item: {
		marginBottom: Sizes.margin.default,
	},

	padder: {
		marginTop: Sizes.margin.default,
		marginBottom: 16,
	},

	tags: {
		marginRight: Sizes.margin.default,
	},

	input: {
		marginTop: 4,
		marginBottom: Sizes.margin.default,
	},

	checker: {
		alignItems: 'center',
		marginTop: 8,
		marginBottom: 8,
	},

	check: {
		marginLeft: 8,
	},

	sortBy: {
		marginBottom: 16,
	}
})
