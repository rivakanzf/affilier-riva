import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';

import Linking from 'coeur/libs/linking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
// import InputValidatedBit from 'modules/bits/input.validated';
import PageBit from 'modules/bits/page';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class LoginPage extends PageModel {

		static routeName = 'login'

		constructor(p) {
			super(p, {
				// initial state
			});

			this.userData = {}
			this.formConfig = [{
				id: 'email',
				required: true,
				type: FormPagelet.TYPES.EMAIL,
				// placeholder: 'email@example.com',
			}, {
				id: 'password',
				required: true,
				type: FormPagelet.TYPES.PASSWORD,
				// placeholder: 'Your secret',
			}]
		}

		onUpdate = (isAllValid, data) => {
			if(isAllValid !== this.state.isAllValid) {
				this.setState({
					isAllValid,
					isError: false,
				})
			} else if(this.state.isError === true) {
				this.setState({
					isError: false,
				})
			}

			this.userData = data
		}

		onLogin = () => {
			// TODO
			this.setState({
				isLoading: true,
			}, () => {
				MeManager
					.login({
						email: this.userData.email.value,
						password: this.userData.password.value,
					})
					.then(() => {
						if(this.props.redirect) {
							this.navigator.navigate(this.props.redirect)
						} else {
							this.navigator.top()
						}
					})
					.catch((err) => {

						this.setState({
							isLoading: false,
							isError: true,
						})

						this.warn(err)

						if (err && err.code === '018') {
							this.utilities.notification.show({
								title: 'Oops!',
								message: 'Either your email or password is invalid.',
								key: Date.now(),
							})
						} else if (err && err.code === '019') {
							this.navigator.navigate('verify', {
								email: err.detail,
								description: `We noticed that you have completed your Style Profile (${err.detail}) before, please verify your email to update it here.`,
							})
						} else {
							this.utilities.notification.show({
								title: 'Oops!',
								message: err.message || err,
								key: Date.now(),
							})
						}
					})
			})
		}

		onNavigateToForgotPassword = () => {
			Linking.open('https://helloyuna.io/forgot')
		}

		view() {
			return (
				<PageBit scrollRef={ this.bindScroller } contentContainerStyle={[Styles.content, Styles.container]}>
					<BoxBit type={BoxBit.TYPES.ALL_THICK} unflex style={Styles.box}>
						<IconBit
							name="yuna"
							style={Styles.marginButton}
							size={32}
						/>

						<GeomanistBit type={GeomanistBit.TYPES.HEADER_4} weight="medium" style={Styles.login}>
							Login
						</GeomanistBit>

						<FormPagelet
							config={this.formConfig}
							onUpdate={ this.onUpdate }
							onSubmit={ this.onLogin }
						/>

						<ButtonBit
							title="LOGIN"
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ !this.state.isAllValid ? ButtonBit.TYPES.STATES.DISABLED : this.state.isLoading && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL }
							style={ Styles.button }
							onPress={ this.onLogin }
						/>

						<ButtonBit
							theme={ ButtonBit.TYPES.THEMES.SECONDARY }
							type={ ButtonBit.TYPES.SHAPES.GHOST }
							size={ ButtonBit.TYPES.SIZES.SMALL }
							title={ 'Forgot password?' }
							align="center"
							onPress={ this.onNavigateToForgotPassword }
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
