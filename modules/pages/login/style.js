import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	content: {
		alignItems: 'center',
		justifyContent: 'center',
	},

	button: {
		marginTop: 24,
		marginBottom: 8,
	},

	login: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomStyle: 'solid',
		borderColor: Colors.black.palette(2, .2),
		paddingBottom: 8,
	},

	box: {
		width: 320,
		backgroundColor: Colors.white.primary,
		borderWidth: StyleSheet.hairlineWidth,
		borderRadius: 4,
		borderColor: Colors.black.palette(2, .2),
	},
})
