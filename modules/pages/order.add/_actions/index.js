import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';

import BoxRowLego from 'modules/legos/box.row';
import SelectStatusLego from 'modules/legos/select.status';


import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				isValid: PropTypes.bool,
				isSaving: PropTypes.bool,
				onSave: PropTypes.func,
			}
		}

		static contexts = [ UtilitiesContext ]

		constructor(p) {
			super(p, {
				orderStatus: null,
				isLoading: false,
			}, [
			])
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onPress = () => {
			this.props.utilities.alert.show({
				title: 'Are you sure want to create this order',
				message: 'Please check again',
				actions: [{
					title: 'OK',
					type: 'OK',
					onPress: this.props.onSave,
				}, {
					title: 'Cancel',
					type: 'CANCEL',
				}],
			})
		}

		view() {
			return (
				<BoxRowLego
					title="Order Actions"
					data={[{
						data: [{
							title: 'Order Status',
							children: (
								<BoxBit unflex>
									<SelectStatusLego
										disabled
										type={ SelectStatusLego.TYPES.ORDERS }
										status="PENDING_PAYMENT"
										style={Styles.status}
									/>
									<GeomanistBit
										type={GeomanistBit.TYPES.NOTE_1}
										style={Styles.lastUpdate}
									>
										Last updated: -
									</GeomanistBit>
								</BoxBit>
							),
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title="CREATE &amp; SAVE ORDER"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={ this.props.isSaving
										? ButtonBit.TYPES.STATES.LOADING
										: this.props.isValid
											&& ButtonBit.TYPES.STATES.NORMAL
											|| ButtonBit.TYPES.STATES.DISABLED
									}
									onPress={this.onPress}
								/>
							),
						}],
					}]}
					style={Styles.container}
				/>
			)
		}
	}
)
