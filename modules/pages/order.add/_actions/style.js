import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		marginBottom: Sizes.margin.default,
	},

	lastUpdate: {
		paddingTop: 8,
		color: Colors.black.palette(2, .6),
	},

	status: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: 16,
	}
})
