import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UserService from 'app/services/user.new';
import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';
import IconBit from 'modules/bits/icon';

import AddressList from 'modules/pagelets/address.list';

import { isEqual } from 'lodash';
import Styles from './style';

export default ConnectHelper(
	class AddressPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,

				title: PropTypes.string,
				placeholder: PropTypes.string,

				onSelect: PropTypes.func,
			}
		}

		static contexts = [ UtilitiesContext ]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				placeholder: p.placeholder,
				isLoading: false,
				selectedId: null,
				data: [],
			}, [])
		}

		componentDidMount() {
			if(this.props.id) {
				this.getData()
			}
		}
		
		componentDidUpdate(pP) {
			if(!isEqual(pP.id, this.props.id)) {
				this.getData()
			}
		}

		getData() {
			this.setState({
				isLoading: true,
			}, () => {
				UserService.getUserAddresses(this.props.id, this.props.token).then(res => {
					this.setState({
						isLoading: false,
						selectedId: res.default_id,
						data: res.addresses.map(result => {
							return {
								selectable: true,
								...result,
							}
						}),
					})
				}).catch(err => {
					this.warn(err)
					
					if(err && err.code !== 'ERR_101') {
						this.props.utilities.notification.show({
							title: 'Oops,',
							message: 'Get address list failed, try again later',
						})
					}

					this.setState({ isLoading: false })
				})
			})
		}

		onConfirm = (address) => {
			this.setState({
				placeholder: `${address.receiver} - ${address.address} - ${address.phone}`,
			}, () => {
				this.props.onSelect &&
				this.props.onSelect(address)
	
				this.onClose()
			})
		}
		
		onPress = () => {
			this.props.utilities.alert.modal({
				component: (
					<AddressList
						addable
						selectable
						onClose={this.onClose}
						onConfirm={this.onConfirm}
						addresses={this.state.data}
						selectedId={this.state.selectedId}
						isSelected={this.state.selectedId}
					/>
				),
			})
		}
		onClose = () => {
			this.props.utilities.alert.hide()
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit />
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit unflex style={this.props.style}>
					{ !!this.props.title && (
						<GeomanistBit weight="semibold"
							type={this.props.small
								? GeomanistBit.TYPES.PARAGRAPH_3
								: GeomanistBit.TYPES.HEADER_5
							} style={Styles.title}
						>
							{ this.props.title }
						</GeomanistBit>
					)}
					<TouchableBit unflex row style={[Styles.input, Styles.border]} onPress={ this.onPress }>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={Styles.darkGrey60}
						>
							{ this.state.isLoading ? 'Fetching result ...' : this.state.placeholder }
						</GeomanistBit>
						<IconBit
							name="dropdown"
							size={20}
							color={Colors.black.palette(2, .8)}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
