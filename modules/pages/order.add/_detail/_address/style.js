import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		position: 'absolute',
		zIndex: 2,
		backgroundColor: Colors.white.primary,
		width: '100%',
		borderRadius: 2,
		overflow: 'hidden',
	},

	title: {
		color: Colors.black.palette(2),
		marginTop: 4,
		marginBottom: 4,
	},

	padder: {
		marginLeft: 16,
		marginRight: 16,
		marginBottom: 8,
	},
	padder16: {
		marginLeft: Sizes.margin.default,
		marginRight: Sizes.margin.default,
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},
	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},


	paddingRight: {
		paddingRight: 12,
	},

	isSearching: {
		paddingTop: 8,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		color: Colors.black.palette(2, .6),
	},

	listContainer: {
		maxHeight: 132,
		marginTop: 8,
	},

	list: {
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		justifyContent: 'space-between',
	},
	select: {
		paddingTop: 3,
		paddingBottom: 3,
		paddingLeft: 8,
		paddingRight: 8,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.white.primary,
		borderRadius: 2,
	},

	isSelected: {
		backgroundColor: Colors.primary,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	listContent: {
		backgroundColor: Colors.solid.grey.palette(4),
		paddingTop: 12,
		paddingBottom: 11,
		paddingLeft: 8,
		paddingRight: 8,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		alignItems: 'center',
	},
	image: {
		height: 36,
		width: 36,
	},

	input: {
		justifyContent: 'space-between',
		paddingLeft: 16,
		paddingRight: 8,
		paddingBottom: 11,
		paddingTop: 11,
	},

	border: {
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
	},
})
