import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UserService from 'app/services/user.new';

import BoxBit from 'modules/bits/box';
import TextInputBit from 'modules/bits/text.input';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import ScrollViewBit from 'modules/bits/scroll.view';
import TouchableBit from 'modules/bits/touchable';

import InitialLego from 'modules/legos/initial';
import ListLego from 'modules/legos/list';

import Styles from './style';


export default ConnectHelper(
	class ResultPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				// INHERITED FROM PARENT
				onSelect: PropTypes.func,
				onHide: PropTypes.func,
			}
		}
		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				users: [],
			}, [
				'onPress',
				'resultRenderer',
				'loadingRenderer',
				'onMouseEnter',
				'onMouseLeave',
			])
		}

		componentDidMount() {
			this.getData()
		}

		componentDidUpdate(pP) {
			if(pP.search !== this.props.search) {
				this.getData()
			}
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				UserService.getUsers({
					search: this.props.search,
				}, this.props.token).then(res => {
					this.setState({
						isLoading: false,
						users: res.data,
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
						users: [],
					})
				})
			})
		}

		loadingRenderer() {
			return (
				<BoxBit unflex row style={Styles.loader}>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={[Styles.darkGrey60, Styles.padderRight]}>
						Fetching result...
					</GeomanistBit>
					<LoaderBit simple size={20} />
				</BoxBit>
			);
		}

		emptyRenderer() {
			return (
				<BoxBit unflex row style={Styles.loader}>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.darkGrey60}>
						Currently have no data recorded.
					</GeomanistBit>
				</BoxBit>
			);
		}

		onPress(user) {
			this.props.onSelect &&
			this.props.onSelect(user, true, `${user.id} - ${user.name} - ${user.email}`)

			this.props.onHide &&
			this.props.onHide()
		}

		onMouseEnter(i) {
			this.setState({
				hoverIndex: i,
			})
		}

		onMouseLeave() {
			this.setState({
				hoverIndex: undefined,
			})
		}

		resultRenderer(user, i) {
			return (
				<TouchableBit key={i} row
					onMouseEnter={ this.onMouseEnter.bind(this, i) }
					onMouseLeave={ this.onMouseLeave.bind(this, i) }
					style={[i === this.state.hoverIndex ? Styles.isHovered : Styles.listContent, Styles.list]}
					onPress={ this.onPress.bind(this, user) }
				>
					<BoxBit unflex row style={Styles.desc}>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={i === this.state.hoverIndex ? Styles.white : Styles.darkGrey}
						>
							#{user.id} —
						</GeomanistBit>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={i === this.state.hoverIndex ? Styles.white : Styles.darkGrey}
						>
							{user.name} -
						</GeomanistBit>
						<GeomanistBit
							type={GeomanistBit.TYPES.PARAGRAPH_3}
							style={i === this.state.hoverIndex ? Styles.white : Styles.darkGrey}
						>
							{user.email}
						</GeomanistBit>
					</BoxBit>
					{ i === this.state.hoverIndex && (
						<TouchableBit unflex style={Styles.select}>
							<GeomanistBit
								type={GeomanistBit.TYPES.NOTE_1}
								weight="medium"
								style={Styles.white}
							>
								Select
							</GeomanistBit>
						</TouchableBit>
					) }
				</TouchableBit>
			)
		}

		view() {
			return this.state.isLoading ? this.loadingRenderer() : (
				<ScrollViewBit style={ Styles.searchBar }>
					{ this.state.users.length === 0
						? this.emptyRenderer()
						: this.state.users.map(this.resultRenderer)
					}
				</ScrollViewBit>
			)
		}
	}
)
