import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities';
// import PageContext from 'coeur/contexts/page';

import InputDateTimeBit from 'modules/bits/input.datetime';

import BoxLego from 'modules/legos/box';
import DropdownSearchLego from 'modules/legos/dropdown.search';


import ResultClientPart from './_result.client';
import AddressPart from './_address';

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				userId: PropTypes.id,
				shipmentAddressId: PropTypes.id,
				orderDate: PropTypes.string,

				onChangeUser: PropTypes.func,
				onChangeAddress: PropTypes.func,
				onChangeDate: PropTypes.func,
			}
		}

		static contexts = [ UtilitiesContext ]

		constructor(p) {
			super(p, {
				search: '',
			}, [
				'onViewOrders',
				'onViewProfile',
			])
		}

		onViewProfile() {
			this.props.page.navigator.navigate(`user/detail/${this.props.userId}`)
		}

		onViewOrders() {
			this.props.page.navigator.navigate(`order/searchBy/${this.props.name}`)
		}
		
		view() {
			return (
				<BoxLego
					unflex
					title="Order Detail"
					contentContainerStyle={Styles.padder}
				>
					<InputDateTimeBit
						unflex
						title="Order date"
						onChange={ this.props.onChangeDate }
						style={Styles.padderVertical}
					/>

					<DropdownSearchLego
						small
						title="Client"
						placeholder="Search client by ID, name or email address"
						onSelect={ this.props.onChangeUser }
						children={(
							<ResultClientPart
								search={ this.state.search }
								{ ...this.props }
							/>
						)}
						style={Styles.padderVertical}
					/>

					<AddressPart
						small
						id={this.props.userId}
						title="Shipping address"
						placeholder="Select shipping address"
						style={[Styles.noPadder, Styles.padderVertical]}
						onSelect={ this.props.onChangeAddress }
					/>
				</BoxLego>
			)

		}
	}
)
