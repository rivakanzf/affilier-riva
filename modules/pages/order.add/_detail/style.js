import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	padder: {
		marginBottom: Sizes.margin.default,
	},

	time: {
		width: 48,
	},

	text: {
		width: 24,
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},
	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},
	white: {
		color: Colors.white.primary,
	},
	primary: {
		color: Colors.primary,
		paddingRight: 4,
	},

	search: {
		paddingTop: 9,
		borderRadius: 2,
		overflow: 'hidden',
		position: 'absolute',
		zIndex: 1,
		backgroundColor: Colors.white.primary,
		width: '100%',
	},

	space: {
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: 8,
	},

	padder16: {
		marginLeft: Sizes.margin.default,
		marginRight: Sizes.margin.default,
	},
	padderInput: {
		paddingRight: 8,
	},

	paddingRight: {
		paddingRight: 12,
	},

	isSearching: {
		paddingTop: 8,
		color: Colors.black.palette(2, .6),
	},

	input: {
		paddingRight: 8,
	},

	inputDate: {
		width: 0,
	},

	listContainer: {
		maxHeight: 132,
		marginTop: 8,
	},

	center: {
		alignItems: 'center',
	},

	list: {
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
	},
	select: {
		paddingTop: 3,
		paddingBottom: 3,
		paddingLeft: 8,
		paddingRight: 8,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.white.primary,
		borderRadius: 2,
	},

	isSelected: {
		backgroundColor: Colors.primary,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	client: {
		paddingTop: 11,
		paddingBottom: 11,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 2,
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingLeft: Sizes.margin.default,
		paddingRight: 8,
	},

	noPadder: {
		paddingLeft: 0,
		paddingRight: 0,
	},
	padderVertical: {
		marginTop: Sizes.margin.default,
		marginBottom: 4,
	},

	content: {
		color: Colors.black.palette(2),
	},
})
