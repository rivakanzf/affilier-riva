import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class InputPromoPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onSubmitEditing: PropTypes.func,
				isLoading: PropTypes.bool,
				isValid: PropTypes.bool,
				value: PropTypes.string,
			}
		}

		constructor(p) {
			super(p, {
			}, [
				'onChange',
				'onSubmitEditing',
			])

			this._data = undefined
		}

		onChange(e, val) {
			this._data = val
		}

		onSubmitEditing() {
			this.props.onSubmitEditing &&
			this.props.onSubmitEditing(this._data)
		}

		view() {
			return (
				<BoxBit unflex row style={Styles.container} onPress={this.onPress}>
					<BoxBit>
						<TextInputBit
							key={ this.props.value }
							// disabled={ this.props.disabled || false}
							defaultValue={ this.props.value }
							placeholder="Add promo code"
							onChange={ this.onChange }
							onSubmitEditing={ this.onSubmitEditing }
							style={Styles.input}
							postfix={ this.props.isLoading ? (
								<BoxBit unflex centering>
									<LoaderBit simple style={Styles.loader} size={16} />
								</BoxBit>
							) : this.props.isValid && (
								<BoxBit unflex centering>
									<IconBit
										name="checkmark"
										size={20}
										color={ Colors.green.primary }
									/>
								</BoxBit>
							) }
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
