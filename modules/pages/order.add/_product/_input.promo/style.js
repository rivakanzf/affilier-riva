import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	// container: {
	// 	paddingTop: 16,
	// 	paddingBottom: 16,
	// 	paddingLeft: Sizes.margin.thick,
	// 	paddingRight: Sizes.margin.thick,
	// 	backgroundColor: Colors.white.primary,
	// 	borderColor: Colors.black.palette(2, .2),
	// 	borderWidth: 0,
	// 	borderTopWidth: StyleSheet.hairlineWidth,
	// 	borderBottomWidth: StyleSheet.hairlineWidth,
	// },

	input: {
		paddingRight: 8,
	},

	loader: {
		height: 24,
	},

	colorBlack: {
		color: Colors.black.palette(1, .7),
		marginRight: 16,
	},

	colorRed: {
		color: Colors.red.primary,
		marginRight: 16,
	},
})
