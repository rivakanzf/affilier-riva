import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import TableLego from 'modules/legos/table';
import PricesLego from 'modules/legos/prices';
import BoxLego from 'modules/legos/box';
import ProductFormLego from 'modules/legos/product.form';

import InputPromoPart from './_input.promo';
import Styles from './style';

export default ConnectHelper(
	class ProductDetailPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				userId: PropTypes.id,
				isCouponValid: PropTypes.bool,
				isUpdating: PropTypes.bool,

				prices: PropTypes.object,

				onValidateCoupon: PropTypes.func,
				onChangePrice: PropTypes.func,
			}
		}

		static contexts = [ UtilitiesContext ]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isCouponValid: false,
				isUpdating: false,

				products: [],
				promoCode: null,
			})
		}

		getSourceAsset(asset) {
			if(Array.isArray(asset)) {
				return asset[0]
			}

			return asset
		}

		onPress = () => {
			this.props.utilities.alert.modal({
				component: (
					<ProductFormLego
						title="Add Product"
						userId={this.props.userId}

						onCancel={this.onClose}
						onAdd={ this.onAdd }
					/>
				),
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onEditProduct(i) {
			const {
				metadata,
				...product
			} = this.state.products[i]
			
			this.props.utilities.alert.modal({
				component: (
					<ProductFormLego
						userId={this.props.userId}
						title="Edit Product"

						isEditing
						item={product}
						metadata={metadata}
						isPhysical={metadata.isPhysical}
						isGift={product.asVoucher}

						onCancel={this.onClose}
						onAdd={this.onModify.bind(this, i)}
					/>
				),
			})
		}

		onAdd = item => {
			const cpyProduct = this.state.products.slice();

			let qty = item.qty;

			while(qty > 0) {
				cpyProduct.push(item);
				qty--;
			}

			this.setState({
				products: cpyProduct,
			}, () => {
				this.props.onChangeProduct &&
				this.props.onChangeProduct(cpyProduct)

				this.onClose()
			})

		}

		onModify = (i, item) => {
			const modifyProduct = this.state.products.slice()
			modifyProduct.splice(i, 1, item)

			this.setState({
				products: modifyProduct,
			}, () => {
				this.props.onChangeProduct &&
				this.props.onChangeProduct(modifyProduct)
			})

			this.onClose()
		}

		onRemove = i => {
			const rmvProduct = this.state.products.slice();

			rmvProduct.splice(i, 1)

			this.setState({
				products: rmvProduct,
			}, () => {
				this.props.onChangeProduct &&
				this.props.onChangeProduct(rmvProduct)
			})
		}

		view() {
			return (
				<BoxBit>
					<TableLego
						headers={[{
							title: 'Product',
							width: 2.8,
						}, {
							title: 'Qty',
							width: .7,
							align: 'center',
						}, {
							title: 'Price',
							width: 1.2,
							align: 'right',
						}, {
							title: '',
							width: .5,
							align: 'right',
						}]}
						rows={ this.state.products.map((product, i) => {
							return {
								data: [{
									children: (
										<BoxBit row unflex>
											<ImageBit
												broken={!product.image}
												source={this.getSourceAsset(product.image) || undefined}
												style={Styles.image}
											/>
											<BoxBit unflex>
												<GeomanistBit ellipsis
													type={GeomanistBit.TYPES.PARAGRAPH_3}
													style={Styles.darkGrey}
												>
													{ product.title } (#{product.id})
												</GeomanistBit>

												<TouchableBit unflex onPress={ this.onEditProduct.bind(this, i) }>
													<GeomanistBit
														type={GeomanistBit.TYPES.PARAGRAPH_3}
														weight="medium"
														style={Styles.primary}
													>
														Edit Matchbox Order Detail
													</GeomanistBit>
												</TouchableBit>
											</BoxBit>
										</BoxBit>
									),
								}, {
									title: 1,
								}, {
									title: `IDR ${product.price ? FormatHelper.currencyFormat(product.price) : 0
									}`,
								}, {
									children: (
										<TouchableBit unflex onPress={ this.onRemove.bind(this, i) }>
											<IconBit
												name="trash"
												size={20}
												color={Colors.black.palette(2, .6)}
											/>
										</TouchableBit>
									),
								}],
							}
						})}
						footers={[{
							children: (
								<BoxBit
								>
									<PricesLego
										coupons={(
											<InputPromoPart
												isValid={this.props.isCouponValid}
												isLoading={this.props.isUpdating}
												onSubmitEditing={this.props.onValidateCoupon}
												style={{ color: Colors.green.primary }}
											/>
										)}
										prices={this.props.prices}
										style={Styles.price}
									/>
								</BoxBit>
							),
						}]}
						style={Styles.table}
					/>
					<BoxBit unflex style={Styles.footer}>
						<ButtonBit
							title="Add Product"
							weight="medium"
							width={ButtonBit.TYPES.WIDTHS.FIT}
							state={this.props.isUpdating ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							size={ButtonBit.TYPES.SIZES.SMALL}
							onPress={this.onPress }
							style={{ alignSelf: 'flex-end' }}
						/>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
