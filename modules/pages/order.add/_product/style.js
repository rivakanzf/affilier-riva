import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.white.primary,
		paddingTop: 16,
	},

	center: {
		alignItems: 'center',
	},
	image: {
		height: 40,
		width: 40,
		marginRight: 8,
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},

	padder: {
		paddingLeft: Sizes.margin.default,
	},

	padder8: {
		paddingRight: 8,
	},

	table: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},
	capital: {
		textTransform: 'capitalize',
	},

	primary: {
		color: Colors.primary,
	},

	head: {
		paddingTop: 6,
		paddingBottom: 6,
	},
	buttton: {
		alignItems: 'flex-end'
	},

	headBorder: {
		paddingTop: 6,
		paddingBottom: 6,
		paddingRight: Sizes.margin.default,
		paddingLeft: 8,
	},

	total: {
		paddingTop: 12,
		paddingBottom: 12,
		paddingLeft: 8,
		paddingRight: Sizes.margin.default,
	},

	price: {
		paddingLeft: 0,
		paddingRight: 0,
	},

	borderBottom: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	promo: {
		paddingLeft: 16,
		paddingRight: 16,
		paddingTop: 7,
		paddingBottom: 7,
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		borderStyle: 'solid',
		borderRadius: 2,
	},

	footer: {
		paddingTop: 10,
		paddingBottom: 10,
		marginTop: -12,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
		paddingRight: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
	},
})
