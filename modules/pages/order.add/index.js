import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import OrderService from 'app/services/new.order';
import BoxBit from 'modules/bits/box';
import PageAuthorizedComponent from 'modules/components/page.authorized';
import MultipleAreaAddressComponent from 'modules/components/multiple.area.address';

// import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';

import { isEqual, sortBy, isEmpty } from 'lodash';

import ProductPart from './_product';
import ActionsPart from './_actions';
import DetailPart from './_detail';
import Styles from './style';

export default ConnectHelper(
	class OrderAddPage extends PageModel {
		static routeName = 'order.add'

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isValid: false,
				isUpdating: false,
				isCouponValid: false,

				user: {},
				address: {},
				orderDate: null,
				couponCode: null,
				promoCode: null,
				
				products: [],
				price: {
					subtotal: 0,
					shipping: 0,
					discount: 0,
					total	: 0,
				},
			}, [
				'onSave',
				'onError',

				'onNavigateToOrders',
			]);
		}

		componentDidUpdate(pP, pS) {
			if(
				!isEqual(pS.address.id, this.state.address.id)
				|| !isEqual(sortBy(pS.products, 'id'), sortBy(this.state.products, 'id'))
			) {
				if(this.shouldCheckPrice(this.state.address.id, this.state.products)) {
					this.getOrderDetail()
				}
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		shouldCheckPrice = (address, products) => {
			return address && !isEmpty(products)
		}

		onUpdateProductAddress = () => {
			this.onModalRequestClose()

			this.getOrderDetail()
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onNavigateToOrders() {
			this.navigator.navigate('order')
		}

		onChange(key, val) {
			this.setState({
				[key]: val,
			})
		}

		onSave() {
			if ( !isEmpty(this.state.address) && !isEmpty(this.state.products)) {
				this.setState({
					isLoading: true,
				}, () => {
					OrderService.craeteOrMockOrder({
						order_date: this.state.orderDate,
						user_id: this.state.user.id,
						address_id: this.state.address.id,
						...(this.state.promoCode ? {
							coupon_code: this.state.promoCode,
						} : {}),
						products: this.state.products.map(({ id, type, metadata, qty, asVoucher }) => {
							return {
								id,
								qty,
								type,
								metadata,
								as_voucher: asVoucher,
							}
						}),
						is_facade: false,
					}, this.props.token).then(res => {
						if(res) {
							this.setState({
								isLoading: false,
							}, () => {
								this.onNavigateToOrder()
	
								this.utilities.notification.show({
									title: 'Yeay!',
									message: 'Order successfully created',
									type: this.utilities.notification.TYPES.SUCCESS,
									timeout: 3000,
								})
							})
						}
					}).catch(this.onError)
				})
			} else {
				this.utilities.notification.show({
					title: 'Oops…',
					message: 'Either Address or Product not defined.',
				})
			}
		}

		onValidateCoupon = code => {
			this.setState({
				promoCode: code,
			}, () => {
				this.getOrderDetail(true)
			})
		}

		getOrderDetail = validatingCoupon => {
			this.setState({
				isUpdating: true,
			}, () => {
				OrderService.craeteOrMockOrder({
					order_date: this.state.orderDate,
					address_id: this.state.address.id,
					user_id: this.state.user.id,
					...(this.state.promoCode ? {
						coupon_code: this.state.promoCode,
					} : {}),
					products: this.state.products.map(({
						id, type, metadata, qty, asVoucher,
					}) => {
						return {
							id: id,
							type: type,
							metadata,
							qty,
							as_voucher: asVoucher,
						}
					}),
					is_facade: true,
				}, this.props.token).then(({
					coupons,
					price,
					campaigns,
				}) => {
					this.setTimeout(() => {
						if (validatingCoupon) {
							if(this.state.promoCode.length) {
								if (coupons.length) {
									this.utilities.notification.show({
										title: 'Yeay!',
										message: 'Coupon successfully applied',
										type: this.utilities.notification.TYPES.SUCCESS,
										timeout: 3000,
									})
								} else {
									this.utilities.notification.show({
										title: 'Oops…',
										message: 'Coupon isn\'t found',
										timeout: 3000,
									})
								}
							}
						}

						this.setState({
							isUpdating: false,
							isValid: true,
							isCouponValid: !!(coupons.length && coupons.find(c => c.discount > 0)),
							price: {
								...price,
								subtotal: price.subtotal,
								total: price.total,
								shipping: price.shipping,
								discount: price.discount,
								additional: campaigns.length ? price.subtotal - campaigns.find(c => c.discount).discount : null,
							},
							couponCode: coupons.length && coupons.find(c => c.discount > 0).code,
						})
					}, 2000)
				}).catch(err => {
					this.warn(err)
					if(err.code === 'ERR_101') {
						if(this.state.promoCode.length) {
							this.utilities.notification.show({
								message: 'whether code or product not found',
							})
						}
					} else if(err.code === 'ERR_107') {
						this.utilities.alert.modal({
							component: (
								<MultipleAreaAddressComponent
									data={ err.detail.tikiAreas }
									selectedAddressId={this.state.address.id}
									onSubmit={ this.onUpdateProductAddress }
									onRequestClose={this.onModalRequestClose}
									token={this.props.token}
									onError={this.onError}
								/>
							),
						})
					} else if(err.code === 'ERR_103' || err.code === 'ERR_100' || err.code === 'ERR_099') {
						this.utilities.notification.show({
							title: 'Oops, to get detail ...',
							message: '' + err.detail,
						})

						this.setState({
							promoCode: null,
							isCouponValid: false,
							isValid: false,
						}, () => {
							this.props.onChangePrice &&
							this.props.onChangePrice({
								total: 0,
								subtotal: 0,
								discount: 0,
								shipping: 0,
							})
						})
					} else {
						this.utilities.notification.show({
							message: 'Oops… There\'s something wrong.',
						})
					}

					this.setState({
						isUpdating: false,
						isValid: false,
					})
				})
			})
		}

		onError(err) {
			this.warn(err)

			this.setState({
				isLoading: false,
				// isValid: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops',
					message: 'Something went wrong, please try again later',
				})
			})
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles="admin"
					onAuthorized={this.onAuthorized}
					paths={[{
						title: 'Orders',
						onPress: this.onNavigateToOrders,
					}, {
						title: 'Add Order',
					}]}
				>
					{() => (
						<BoxBit style={Styles.container}>
							<BoxBit unflex row>
								<BoxBit style={Styles.padder}>
									<DetailPart
										userId={this.state.user.id}

										onChangeUser={this.onChange.bind(this, 'user')}
										onChangeDate={this.onChange.bind(this, 'orderDate')}
										onChangeAddress={this.onChange.bind(this, 'address')}
									/>

									<ProductPart
										userId={this.state.user.id}
										
										isCouponValid={this.state.isCouponValid}
										isUpdating={this.state.isUpdating}
										prices={this.state.price}

										onValidateCoupon={this.onValidateCoupon}
										onChangeProduct={this.onChange.bind(this, 'products')}
									/>
								</BoxBit>

								<BoxBit style={Styles.padder}>
									<ActionsPart
										isValid={this.state.isValid}
										isSaving={this.state.isLoading}
										onSave={this.onSave}
									/>
								</BoxBit>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
