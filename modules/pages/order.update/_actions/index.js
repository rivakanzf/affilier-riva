import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import OrderService from 'app/services/new.order';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ClipboardBit from 'modules/bits/clipboard';
import GeomanistBit from 'modules/bits/geomanist';
import InputCheckboxBit from 'modules/bits/input.checkbox';

import BoxRowLego from 'modules/legos/box.row';
import RowNotificationLego from 'modules/legos/row.notification';
import SelectStatusLego from 'modules/legos/select.status';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';

import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				orderId: PropTypes.id,
				userId: PropTypes.id,
				shipmentIds: PropTypes.arrayOf(PropTypes.id),
				status: PropTypes.string,
				styleProfileCompleted: PropTypes.bool,
				paymentLink: PropTypes.string,
				log: PropTypes.string,
				updatedAt: PropTypes.string,
				onChangeStatus: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			shipmentIds: [],
		}

		constructor(p) {
			super(p, {
				isSending: false,
				isChanging: false,
				updater: 1,
			})

			this._data = {
				status: p.status,
				readable: '',
				shouldSendEmail: false,
			}
		}

		disabledStatuses = () => {
			switch(this.props.status) {
			case 'PENDING_PAYMENT':
				return [							'PAID_WITH_EXCEPTION', 'PROCESSING',	'RESOLVED']
			case 'PAID':
				return ['PENDING_PAYMENT',			'PAID_WITH_EXCEPTION',					'RESOLVED', 'EXPIRED']
			case 'PAID_WITH_EXCEPTION':
				return ['PENDING_PAYMENT', 'PAID',											'RESOLVED', 'EXPIRED']
			case 'PROCESSING':
				return ['PENDING_PAYMENT', 'PAID',	'PAID_WITH_EXCEPTION',							 	'EXPIRED']
			case 'RESOLVED':
				return ['PENDING_PAYMENT', 'PAID',	'PAID_WITH_EXCEPTION',							 	'EXPIRED', 'EXCEPTION']
			case 'EXPIRED':
				return ['PENDING_PAYMENT',			'PAID_WITH_EXCEPTION', 'PROCESSING', 	'RESOLVED', 'EXPIRED', 'EXCEPTION']
			case 'EXCEPTION':
				return ['PENDING_PAYMENT',			'PAID_WITH_EXCEPTION', 'PROCESSING',	'RESOLVED', 'EXPIRED']
			default:
				return []
			}
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onChangeStatus = (status, readable) => {
			this._data = {
				status,
				readable,
				shouldSendEmail: false,
			}

			this.forceUpdate()
		}

		onChangeSendEmail = value => {
			this._data.shouldSendEmail = value
		}

		onUpdate = () => {
			this._data.shouldSendEmail = false

			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="You might want to add a note regarding this change."
						placeholder="Input here"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmChangeStatus }
					>
						<InputCheckboxBit value={ this._data.shouldSendEmail } title="Email customer about this change" onChange={ this.onChangeSendEmail } style={Styles.input} />
					</ModalPromptPagelet>
				),
			})
		}

		onConfirmChangeStatus = note => {
			this.setState({
				isChanging: true,
			}, () => {
				return Promise.resolve().then(() => {
					switch(this._data.status) {
					case 'PAID':
						return OrderService.payOrder(this.props.orderId, {
							note,
							metadata: {},
							payment: 'SUCCESS',
							shouldSendEmail: this._data.shouldSendEmail,
						}, this.props.token)
					case 'PROCESSING':
						return OrderService.processOrder(this.props.orderId, {
							note,
							shouldSendEmail: this._data.shouldSendEmail,
						}, this.props.token)
					case 'RESOLVED':
						return OrderService.resolveOrder(this.props.orderId, {
							note,
							shouldSendEmail: this._data.shouldSendEmail,
						}, this.props.token)
					case 'EXPIRED':
						return OrderService.expireOrder(this.props.orderId, {
							note,
							shouldSendEmail: this._data.shouldSendEmail,
						}, this.props.token)
					case 'EXCEPTION':
						return OrderService.exceptOrder(this.props.orderId, {
							note,
							shouldSendEmail: this._data.shouldSendEmail,
						}, this.props.token)
					default:
						throw new Error('Cannot change to this status')
					}
				}).then(() => {
					this.props.utilities.notification.show({
						title: 'Status changed',
						message: `Successfully change status to ${this._data.readable}`,
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.setState({
						isChanging: false,
					}, () => {
						this.props.onChangeStatus &&
						this.props.onChangeStatus(this._data.status)
					})
				}).catch(err => {
					this.warn(err)

					this._data.status = this.props.status

					this.props.utilities.notification.show({
						title: 'Error!',
						message: `Error when changing status, ${err && (err.detail || err.message) || err}`,
						timeout: 10000,
					})

					this.setState({
						isChanging: false,
						updater: this.state.updater + 1,
					})
				})
			})
		}

		view() {
			return (
				<BoxRowLego
					title="Order Actions"
					data={[{
						data: [{
							title: 'Order Status',
							children: (
								<BoxBit unflex>
									<SelectStatusLego key={ this.state.updater } unflex isRequired
										type={ SelectStatusLego.TYPES.ORDERS }
										status={ this._data.status}
										disabledStatuses={ this.disabledStatuses() }
										onChange={ this.onChangeStatus }
									/>
									<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.time }>
										Last updated: { this.props.updatedAt
											? TimeHelper.format(this.props.updatedAt, 'DD MMMM YYYY HH:mm')
											: '-'
										}
									</GeomanistBit>
								</BoxBit>
							),
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ 'UPDATE STATUS' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.state.isChanging
										? ButtonBit.TYPES.STATES.LOADING
										: this.props.status === this._data.status && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL
									}
									onPress={ this.onUpdate }
								/>
							),
						}],
					}, this.props.log ? {
						data: [{
							title: 'Exception Note',
							content: this.props.log || '-',
						}],
					} : null, {
						data: [{
							title: 'Payment Link',
							children: (
								<ClipboardBit autogrow={44} value={ this.props.paymentLink || '-' } />
							),
						}],
					}, {
						data: [{
							children: (
								<RowNotificationLego title="Email Notification" type={ RowNotificationLego.TYPES.PAYMENT } id={ this.props.orderId } disabled={ this.props.status !== 'PENDING_PAYMENT' } />
							),
							headerStyle: Styles.button,
						}],
					}, {
						data: [{
							children: (
								<RowNotificationLego type={ RowNotificationLego.TYPES.STYLE_PROFILE } id={ this.props.userId } disabled={ this.props.styleProfileCompleted } />
							),
							headerStyle: Styles.button,
						}],
					}, {
						data: [{
							children: (
								<RowNotificationLego type={ RowNotificationLego.TYPES.SHIPMENT } id={ this.props.shipmentIds } disabled={ !this.props.shipmentIds.length } />
							),
							headerStyle: Styles.button,
						}],
					}, {
						data: [{
							children: (
								<RowNotificationLego type={ RowNotificationLego.TYPES.FEEDBACK } id={ this.props.userId } />
							),
							headerStyle: Styles.button,
						}],
					}]}
					style={ Styles.container }
				/>
			)
		}
	}
)
