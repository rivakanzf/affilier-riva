import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import TimeHelper from 'coeur/helpers/time';

import OrderService from 'app/services/new.order';

import InputDatetimeBit from 'modules/bits/input.datetime';

import BadgeStatusLego from 'modules/legos/badge.status';
import BoxLego from 'modules/legos/box';
import NotificationLego from 'modules/legos/notification';
import RowClientLego from 'modules/legos/row.client';
import RowsLego from 'modules/legos/rows';

import AddressesComponent from 'modules/components/addresses';

import Styles from './style';

import { isEmpty } from 'lodash';


export default ConnectHelper(
	class DetailPart extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				orderId: PropTypes.id,
				orderDetailIds: PropTypes.arrayOf(PropTypes.id),
				orderNumber: PropTypes.string,
				userId: PropTypes.id,
				userName: PropTypes.string,
				userEmail: PropTypes.string,
				orderDate: PropTypes.string,
				paymentDate: PropTypes.string,
				cancellationDate: PropTypes.string,
				// shipmentAt: PropTypes.array,
				// addresses: PropTypes.array,
			}
		}

		static propsToPromise(state, oP) {
			if(oP.cancellationDate) {
				return Promise.all([
					OrderService.getOrderHistory([oP.orderId], state.me.token),
					OrderService.getOrderDetailHistory([oP.orderDetailIds], state.me.token),
				]).then(([orderHistories]) => {
					return {
						order: orderHistories.pop() || null,
						orderDetailHistories: [],
						// orderHistories: orderDetailHistories.pop() || null,
					}
				})
			} else {
				return null
			}
		}

		static getDerivedStateFromProps(nP) {
			if(!nP.isLoaded && !isEmpty(nP.data)) {
				return {
					isLoaded: true,
					orderHistory: nP.data.order,
					orderDetailHistories: nP.data.orderDetailHistories,
				}
			}

			return null
		}

		constructor(p) {
			super(p, {
				isLoaded: false,
				orderHistory: null,
				orderDetailHistories: [],
			})
		}

		view() {
			return (
				<BoxLego
					title={ `Order #${ this.props.orderNumber }` }
					header={ !this.props.cancellationDate ? undefined : (
						<BadgeStatusLego status="CANCELLED" />
					) }
					contentContainerStyle={ Styles.container }
				>
					{ this.state.orderHistory ? (
						<NotificationLego
							message={`Order already cancelled: ${ this.state.orderHistory.note }${
								this.state.orderDetailHistories.length ? this.state.orderDetailHistories.map(orderDetailHistory => {
									return `\n#${ orderDetailHistory.order_detail_id }: ${ orderDetailHistory.note }`
								}).join('') : ''
							}`}
							type="WARNING"
						/>
					) : false }
					<RowsLego
						data={[{
							data: [{
								title: 'Short Id',
								content: `#OR-${ this.props.orderId }`,
							}],
						}, {
							data: [{
								headerless: true,
								children: (
									<RowClientLego
										userId={this.props.userId}
										userName={this.props.userName}
										userEmail={this.props.userEmail}
									/>
								),
							}],
						}, {
							data: [{
								headerless: true,
								children: (
									<InputDatetimeBit title="Order Date" date={this.props.orderDate} readonly />
								),
							}, this.props.paymentDate ? {
								headerless: true,
								children: (
									<InputDatetimeBit title="Payment Date" date={this.props.paymentDate} readonly />
								),
							} : {
								title: 'Payment Date',
								content: '-',
								headerStyle: Styles.top0,
							}],
							style: Styles.margin,
						}, {
							data: [{
								headerless: true,
								children: (
									<AddressesComponent addable
										type="order"
										typeId={ this.props.orderId }
										data={ this.props.addresses }
									/>
								),
							}],
						}]}
						style={ Styles.content }
					/>
				</BoxLego>
			)
		}
	}
)
