import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	container: {
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
	},

	content: {
		marginLeft: Sizes.margin.default,
		marginRight: Sizes.margin.default,
		marginBottom: Sizes.margin.default,
	},

	margin: {
		marginTop: 16,
	},

	top0: {
		marginTop: 0,
	},

})
