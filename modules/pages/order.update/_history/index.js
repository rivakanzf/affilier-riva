import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import TimeHelper from 'coeur/helpers/time';

import OrderService from 'app/services/new.order';

import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import NavTextBit from 'modules/bits/nav.text';

import TableLego from 'modules/legos/table';

import Styles from './style';
import geomanist from '../../../bits/geomanist';


export default ConnectHelper(
	class HistoryPart extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				// shipmentAt: PropTypes.array,
				// addresses: PropTypes.array,
			}
		}

		static propsToPromise(state, oP) {
			return OrderService.getOrderHistories(oP.id, state.me.token)
		}

		header = [{
			title: 'No.',
			width: .5,
		}, {
			title: 'Changer',
			width: 1,
		}, {
			title: 'Type',
			width: 1,
		}, {
			title: 'Change Log',
			width: 5,
		}, {
			title: 'Date',
			width: 1,
		}]

		logRenderer = (log, i) => {
			return  (
				<BoxBit key={ i } unflex style={ i === 0 ? undefined : Styles.log }>
					<GeomanistBit type={ geomanist.TYPES.NOTE_3 } weight="semibold" style={ Styles.note }>
						{ log[0] }
					</GeomanistBit>
					<GeomanistBit type={ geomanist.TYPES.CAPTION_1 } weight="normal">
						{ log[1] }
					</GeomanistBit>
				</BoxBit>
			)
		}

		rowRenderer = (history, i) => {
			return {
				data: [{
					title: i + 1,
				}, {
					title: '-',
					children: history.changer ? (
						<NavTextBit
							title={ `#US-${ history.changer }`}
							link="View user"
							href={ `client/${ history.changer }` }
						/>
					) : undefined,
				}, {
					title: history.type,
					children: history.ref_id ? (
						<BoxBit unflex>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ history.type }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.CAPTION_1 } style={ Styles.note }>
								#{ history.ref_id }
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					title: typeof history.log === 'string' ? history.log : undefined,
					children: Array.isArray(history.log) ? history.log.map(this.logRenderer) : undefined,
				}, {
					children: (
						<BoxBit unflex>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.format(history.date, 'DD MMM YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.CAPTION_1 } style={ Styles.note }>
								{ TimeHelper.format(history.date, 'HH:mm:ss') }
							</GeomanistBit>
						</BoxBit>
					),
				}],
			}
		}

		viewOnLoading() {
			return (
				<TableLego isLoading
					headers={ this.header }
					rows={ [] }
				/>
			)
		}

		view() {
			return (
				<TableLego
					headers={ this.header }
					rows={ this.props.data.map(this.rowRenderer) }
				/>
			)
		}
	}
)
