import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({

	note: {
		color: Colors.black.palette(2, .6),
	},

	log: {
		marginTop: 8,
	},

})
