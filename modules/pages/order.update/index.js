import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import UserService from 'app/services/user.new';
import OrderService from 'app/services/new.order';

import LoaderBit from 'modules/bits/loader';
import BoxBit from 'modules/bits/box';
// import InputDateTimeBit from 'modules/bits/input.datetime';
// import PageBit from 'modules/bits/page';
// import ScrollViewBit from 'modules/bits/scroll.view';

// import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
// import HeaderLoggedLego from 'modules/legos/header.logged';
import PricesLego from 'modules/legos/prices';
import TabLego from 'modules/legos/tab';
import TableProductLego from 'modules/legos/table.product';

// import AddressUpdateComponent from 'modules/components/address.update';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';
// import SidebarNavigationComponent from 'modules/components/sidebar.navigation';
// import MultipleAddress from 'modules/pagelets/multiple.address';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import ShipmentListPagelet from 'modules/pagelets/shipment.list';
import QuickViewOrderDetailPagelet from 'modules/pagelets/quick.view.order.detail';

import ActionsPart from './_actions';
import DetailPart from './_detail';
import HistoryPart from './_history';

import Styles from './style';


export default ConnectHelper(
	class OrderUpdatePage extends PageModel {

		static routeName = 'order.update'

		constructor(p) {
			super(p, {
				tabIndex: 0,
				isLoading: true,
				order: {},
				token: undefined,
				updater: 1,
			});
		}

		roles = ['fulfillment', 'finance', 'packing', 'analyst']

		tabs = [{
			title: 'General',
			icon: 'account',
			onPress: this.onChangeTab.bind(this, 0),
		}, {
			title: 'History',
			icon: 'purchases',
			onPress: this.onChangeTab.bind(this, 1),
		}]

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			})
		}

		onAuthorized = token => {
			this.setState({
				isLoading: true,
				token,
			}, () => {
				OrderService.getOrderDetail(this.props.id, token).then(order => {
					OrderService.getOrderAddresses(this.props.id, token).then(dataAddresses => {
						const addresses = dataAddresses.filter(address => {
							return order.addresses.findIndex(original => original.location_id === address.location_id)
						})
						order.addresses = order.addresses.concat(addresses)
						this.setState({
							isLoading: false,
							order,
						})
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: err ? err.detail || err.message || err : 'Something went wrong, please try again later',
						})
					})
				})
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeStatus = () => {
			this.onAuthorized(this.state.token)
		}

		onChangeOrderDetailStatus = (id, status) => {
			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						// eslint-disable-next-line no-nested-ternary
						message={ status === 'PRIMED'
							? 'Important: this action will make all stylesheet inventories be marked as unavailable.'
							: status === 'RESOLVED'
								? 'Important: this action make all stylesheet non exchangeable.'
								: 'You might want to add a note for this change.' }
						placeholder="Input here"
						onCancel={this.onModalRequestClose}
						onConfirm={this.onConfirmChangeOrderDetailStatus.bind(this, id, status)}
					/>
				),
			})
		}

		onConfirmChangeOrderDetailStatus = (id, status, note) => {
			OrderService.setOrderDetailStatus(id, {
				code: status,
				note,
			}, this.state.token).then(isUpdated => {
				if(isUpdated) {
					this.utilities.notification.show({
						title: 'Yeay!',
						message: 'Order detail status updated.',
						type: this.utilities.notification.TYPES.SUCCESS,
					})

					const detail = this.state.order.details.find(d => d.id === id)
					detail.status = status

					this.setState({
						updater: this.state.updater + 1,
					})
				} else {
					throw new Error('Data not updated.')
				}
			}).catch(err => {
				this.warn(err)

				this.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later',
				})
			})
		}

		onNavigate = (route) => {
			this.navigator.navigate(route)
		}

		onNavigateToOrders = () => {
			this.navigator.navigate('order')
		}

		onNavigateToOrderDetail = id => {
			this.utilities.alert.modal({
				component: (
					<QuickViewOrderDetailPagelet
						id={ id }
						onNavigate={ this.onNavigate }
					/>
				),
			})

			return
		}

		onShipmentUpdate = () => {
			this.forceUpdate()
		}

		contentRenderer() {
			switch(this.state.tabIndex) {
			case 0:
				return (
					<BoxBit unflex row>
						<BoxBit style={ Styles.padder }>
							<DetailPart
								orderId={ this.state.order.id }
								orderNumber={ this.state.order.number }
								userId={ this.state.order.user_id }
								userName={ this.state.order.user_name }
								userEmail={ this.state.order.user_email }
								orderDate={ this.state.order.created_at }
								paymentDate={ this.state.order.payment_at }
								cancellationDate={ this.state.order.cancelled_at }
								addresses={ this.state.order.addresses }
								// shipmentAt={ this.state.order.shipment_at }
							/>
							<TableProductLego unflex
								orderDetails={ this.state.order.details }
								onNavigateToOrderDetail={ this.onNavigateToOrderDetail }
								onChangeStatus={ this.onChangeOrderDetailStatus }
							/>
							<PricesLego
								coupons={ this.state.order.coupons }
								campaigns={ this.state.order.campaigns }
								shipments={ this.state.order.facade_shipments }
								prices={ this.state.order.prices }
							/>
						</BoxBit>
						<BoxBit>
							<ActionsPart
								orderId={ this.props.id }
								userId={ this.state.order.user_id || null }
								shipmentIds={ this.state.order.shipments.filter(s => s.status === 'PROCESSING' || s.status === 'DELIVERED' || s.status === 'DELIVERY_CONFIRMED').map(s => s.id) }
								status={ this.state.order.status }
								styleProfileCompleted={ this.state.order.user_spq_completed }
								paymentLink={ this.state.order.payment_link }
								log={ this.state.order.log }
								updatedAt={ this.state.order.updated_at }
								onChangeStatus={ this.onChangeStatus }
							/>
							<ShipmentListPagelet addable editable
								key={ this.state.updater }
								orderId={ this.state.order.id }
								addresses={ this.state.order.addresses }
								shipments={ this.state.order.shipments }
								details={ this.state.order.details }
								onUpdate={ this.onShipmentUpdate }
								style={ Styles.box }
							/>
						</BoxBit>
					</BoxBit>
				)
			case 1:
				return (
					<HistoryPart id={ this.props.id } />
				)
			default:
				return false
			}
		}

		loadingRenderer() {
			return (
				<BoxBit centering>
					<LoaderBit />
				</BoxBit>
			);
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					style={ Styles.page }
					paths={[{
						title: 'Orders',
						onPress: this.onNavigateToOrders,
					}, {
						title: this.state.isLoading
							? 'Loading...'
							: `#${this.state.order.number || this.props.id}`,
					}]}
				>
					{() => (
						<BoxBit style={Styles.container}>
							<HeaderPageComponent
								title="Orders"
							/>
							<TabLego activeIndex={ this.state.tabIndex } tabs={ this.tabs } style={ Styles.tab } />
							{ this.state.isLoading ? this.loadingRenderer() : this.contentRenderer() }
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
