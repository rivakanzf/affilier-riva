import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	tab: {
		marginBottom: Sizes.margin.default,
	},

	box: {
		marginTop: Sizes.margin.default,
	},

	padder: {
		marginRight: Sizes.margin.default,
	},

})
