import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'
import CommonHelper from 'coeur/helpers/common';

import OrderService from 'app/services/new.order';
import MeManager from 'app/managers/me';

import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import InputCheckboxBit from 'modules/bits/input.checkbox';
import InputDateBit from 'modules/bits/input.date';
import LoaderBit from 'modules/bits/loader';
import NavTextTitleBit from 'modules/bits/nav.text.title';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import TableLego from 'modules/legos/table';
// import SelectDateLego from 'modules/legos/select.date';
import SelectStatusLego from 'modules/legos/select.status';
import SelectStatusBadgeLego from 'modules/legos/select.status.badge';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import QuickViewOrderPagelet from 'modules/pagelets/quick.view.order';
import QuickViewVoucherPagelet from 'modules/pagelets/quick.view.voucher';

import Styles from './style';

import { isEmpty } from 'lodash';


export default ConnectHelper(
	class OrderPage extends PageModel {

		static routeName = 'order'

		static stateToProps(state) {
			return {
				isStylist: AuthenticationHelper.isAuthorized(state.me.roles, 'stylist'),
				isFulfillment: AuthenticationHelper.isAuthorized(state.me.roles, 'fulfillment', 'finance'),
			}
		}

		constructor(p) {

			const query = CommonHelper.getQueryString()

			super(p, {
				isLoading: true,

				search: query.search || p.search || null,
				status: query.status || null,
				date: query.date || null,
				shipmentDate: query.shipmentDate || null,

				data: [],
				total: 0,
				offset: query.offset || 0,
				count: query.count || 40,

				token: undefined,
			})

			this._shouldChangeEmail = false
		}

		componentDidUpdate(pP, pS) {
			if(
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.date !== this.state.date
					|| pS.status !== this.state.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
					|| pS.shipmentDate !== this.state.shipmentDate
				)
			) {
				const query = CommonHelper.stripUndefined({
					offset: this.state.offset || undefined,
					count: this.state.count || undefined,
					search: this.state.search || undefined,
					status: this.state.status || undefined,
					date: this.state.date || undefined,
					shipmentDate: this.state.shipmentDate || undefined,
				})

				window.history.replaceState({}, 'Orders', isEmpty(query) ? '/order' : `/order?${Object.keys(query).map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}

		roles = ['fulfillment', 'stylist', 'finance', 'packing', 'analyst']

		tableHeader = [{
			title: 'Order ID',
			width: 1.5,
		}, {
			title: 'Order Date',
			width: 1,
		}, {
			title: 'Ship Sched.',
			width: 1,
		}, {
			title: 'Products',
			width: 2.5,
		}, {
			title: 'Client Name',
			width: 2,
		}, {
			title: 'Order Status',
			width: 1.2,
		}, {
			width: .3,
			children: (
				<BoxBit unflex style={Styles.quickViewHeader} />
			),
		}]

		disabledStatuses = status => {
			switch (status) {
			case 'PENDING_PAYMENT':
				return ['PAID_WITH_EXCEPTION', 'PROCESSING', 'RESOLVED']
			case 'PAID':
				return ['PENDING_PAYMENT', 'PAID_WITH_EXCEPTION', 'RESOLVED', 'EXPIRED']
			case 'PAID_WITH_EXCEPTION':
				return ['PENDING_PAYMENT', 'PAID', 'RESOLVED', 'EXPIRED']
			case 'PROCESSING':
				return ['PENDING_PAYMENT', 'PAID', 'PAID_WITH_EXCEPTION', 'EXPIRED']
			case 'RESOLVED':
				return ['PENDING_PAYMENT', 'PAID', 'PAID_WITH_EXCEPTION', 'EXPIRED', 'EXCEPTION']
			case 'EXPIRED':
				return ['PENDING_PAYMENT', 'PAID_WITH_EXCEPTION', 'PROCESSING', 'RESOLVED', 'EXPIRED', 'EXCEPTION']
			case 'EXCEPTION':
				return ['PENDING_PAYMENT', 'PAID', 'PAID_WITH_EXCEPTION', 'PROCESSING', 'RESOLVED', 'EXPIRED']
			default:
				return []
			}
		}

		prefix = type => {
			if (type) {
				switch(type) {
				case 'INVENTORY':
					return 'IN'
				case 'SERVICE':
					return 'SE'
				case 'STYLESHEET':
					return 'ST'
				case 'STYLEBOARD':
					return 'SB'
				case 'VOUCHER':
					return 'VO'
				case 'WALLET_TRANSACTION':
					return 'WT'
				default:
					return type.substring(0, 2)
				}
			} else {
				return 'OD'
			}
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				OrderService.getOrderList({
					limit: this.state.count,
					offset: this.state.offset,
					search: this.state.search,
					status: this.state.status || null,
					date: this.state.date,
					shipment_date: this.state.shipmentDate,
				}, this.state.token).then(res => {
					this.setState({
						isLoading: false,
						total: res.count,
						data: res.data,
					})
				}).catch(err => {
					this.setState({
						isLoading: false,
					}, () => {
						if (err && err.code === '031') {
							this.utilities.notification.show({
								title: 'Sorry',
								message: 'You\'ve been logged out. Please log in again.',
							})

							this.navigator.navigate('login')

							MeManager.logout();
						} else {
							this.props.utilities.notification.show({
								title: 'Oops',
								message: 'Something went wrong, please try again later',
							})
						}
					})
				})
			})
		}

		onAddNewOrder = () => {
			this.navigator.navigate('order/add')
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onChangeFilter = (key, val) => {
			if((key === 'date' || key === 'shipmentDate') && val) {
				this.setState({
					[key]: TimeHelper.moment(val).toISOString(),
					offset: 0,
				})
			} else {
				this.setState({
					[key]: val,
					offset: 0,
				})
			}
		}

		onChangeSendEmail = value => {
			this._shouldSendEmail = value
		}

		onChangeStatus = (order, key, value) => {
			this._shouldSendEmail = false

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="You might want to add a note for this change."
						placeholder="Input here"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmChangeStatus.bind(this, order, key, value) }
					>
						<InputCheckboxBit value={ this._shouldSendEmail } title="Email customer about this change" onChange={ this.onChangeSendEmail } style={ Styles.input } />
					</ModalPromptPagelet>
				),
			})
		}

		onConfirmChangeStatus = (order, key, value, note) => {
			Promise.resolve().then(() => {
				switch(key) {
				case 'PAID':
					return OrderService.payOrder(order.id, {
						note,
						metadata: {},
						payment: 'SUCCESS',
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				case 'PROCESSING':
					return OrderService.processOrder(order.id, {
						note,
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				case 'RESOLVED':
					return OrderService.resolveOrder(order.id, {
						note,
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				case 'EXPIRED':
					return OrderService.expireOrder(order.id, {
						note,
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				case 'EXCEPTION':
					return OrderService.exceptOrder(order.id, {
						note,
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				default:
					throw new Error('Cannot change to this status')
				}
			}).then(() => {
				this.utilities.notification.show({
					title: 'Status changed',
					message: `Successfully change status to ${value}`,
					type: this.utilities.notification.TYPES.SUCCESS,
				})

				order.status = key

				this.forceUpdate()
			}).catch(err => {
				this.warn(err)

				if(key === 'RESOLVED' && err && err.code === 'ERR_104') {
					this.utilities.alert.show({
						title: 'Are you sure?',
						message: 'This order still have unresolved order details. Continue?',
						actions: [{
							title: 'Yes',
							onPress: () => {
								return OrderService.resolveOrder(order.id, {
									note,
									shouldSendEmail: this._shouldSendEmail,
									force: true,
								}, this.state.token).then(() => {
									this.utilities.notification.show({
										title: 'Status changed',
										message: `Successfully change status to ${value}`,
										type: this.utilities.notification.TYPES.SUCCESS,
									})

									order.status = key

									this.forceUpdate()
								}).catch(_err => {
									this.warn(_err)

									this.utilities.notification.show({
										title: 'Error!',
										message: `Error when changing status, ${_err && _err.message || _err}`,
									})
								})
							},
							type: 'OK',
						}, {
							title: 'Cancel',
							onPress: this.onModalRequestClose,
							type: 'CANCEL',
						}],
					})
				} else {
					this.utilities.notification.show({
						title: 'Error!',
						message: `Error when changing status, ${err && err.message || err}`,
					})
				}
			})
		}

		onNavigateToOrder = orderId => {
			this.navigator.navigate(`order/${orderId}`)
		}

		onNavigateToOrderDetail = (id, type, refId) => {
			if(type === 'VOUCHER') {
				this.utilities.alert.modal({
					component: (
						<QuickViewVoucherPagelet
							id={ refId }
						/>
					),
				})
			} else {
				this.navigator.navigate(`${type.toLowerCase()}/${refId}`)
			}
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onNavigateToBack = () => {
			this.navigator.back()
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onSearchHandler = searchValue => {
			this.setState({
				search: searchValue,
				offset: 0,
			})
		}

		onQuickView = orderId => {
			this.utilities.alert.modal({
				component: (
					<QuickViewOrderPagelet
						id={ orderId }
						onNavigateToOrder={ this.onNavigateToOrder.bind(this, orderId) }
						onNavigateToOrderDetail={ this.onNavigateToOrderDetail }
						onClose={ this.onModalRequestClose }
					/>
				),
			})
		}

		rowRenderer = order => {
			return {
				data: [{
					title: order.number || '-',
					children: (
						<NavTextTitleBit
							title={ order.number }
							description={ `#OR-${ order.id }` }
						/>
					),
				}, {
					title: order.order_date
						? TimeHelper.moment(order.order_date).format('DD MMM YY')
						: '-',
				}, {
					title: order.schedule
						? TimeHelper.getRange(...order.schedule, 'DD', 'MMM', 'YY')
						: '-',
				}, {
					children: (
						<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
							{ order.products.map((p, i) => {
								return (
									<TextBit key={ i }><TextBit style={ Styles.id }>[#{ p.ref_id ? this.prefix(p.type) : 'OD' }-{ p.ref_id || p.id }]</TextBit> { p.title }{ i === order.products.length - 1 ? '' : ', ' }</TextBit>
								)
							}) }
						</GeomanistBit>
					),
				}, {
					title: order.user || '-',
					style: Styles.capital,
				}, {
					children: order.loading ? (
						<BoxBit centering>
							<LoaderBit simple />
						</BoxBit>
					) : (
						<SelectStatusBadgeLego
							type={ SelectStatusBadgeLego.TYPES.ORDERS }
							status={ order.status }
							disabledStatuses={ this.disabledStatuses(order.status) }
							onChange={ this.onChangeStatus.bind(this, order) }
						/>
					),
				}, {
					children: (
						<TouchableBit centering row onPress={this.onQuickView.bind(this, order.id)}>
							<BoxBit />
							<IconBit
								name="quick-view"
								size={20}
								color={Colors.primary}
							/>
						</TouchableBit>
					),
				}],
				onPress: this.onNavigateToOrder.bind(this, order.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Orders' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Orders"
								search={ this.state.search }
								searchPlaceholder="Id, name, or email"
								buttonPlaceholder="Create New Order"
								onSearch={ this.onSearchHandler }
								onPress={ this.onAddNewOrder }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							>
								<SelectStatusLego status={ this.state.status } type={ SelectStatusLego.TYPES.ORDERS } onChange={this.onChangeFilter.bind(this, 'status')} />
								{/* <SelectDateLego onChange={this.onChangeFilter.bind(this, 'date')} /> */}
								<InputDateBit date={ this.state.date } inside title="Order Date" onChange={this.onChangeFilter.bind(this, 'date')}/>
								<InputDateBit date={ this.state.shipmentDate } inside title="Shipment Date" onChange={this.onChangeFilter.bind(this, 'shipmentDate')}/>
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={ Styles.padder }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
