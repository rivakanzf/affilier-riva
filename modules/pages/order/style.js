import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingBottom: 16,
		paddingTop: 16,
		paddingLeft: 32,
		paddingRight: 32,
	},

	padder: {
		marginBottom: 16,
	},

	capital: {
		textTransform: 'capitalize',
	},

	quickViewHeader: {
		width: 20,
	},

	input: {
		marginTop: 8,
	},

	id: {
		color: Colors.primary,
	},
})
