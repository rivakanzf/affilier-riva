import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import StylesheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
import SpectralBit from 'modules/bits/spectral';

import Styles from './style';

import { chunk } from 'lodash';


export default ConnectHelper(
	class PrintAddressPage extends PageModel {

		static routeName = 'print.address'

		static propsToPromise(state, oP) {
			return oP.id ? StylesheetService.getAddresses(oP.id, state.me.token) : null
		}

		constructor(p) {
			super(p, {
			}, [
			])

			this._datas = Array(10).fill({
				id: '123',
				name: 'nuel',
				phone: '089664066157',
				address: 'Jl. Tanjung Duren Raya no.103. Jl. Tanjung Duren Raya no.103. Jl. Tanjung Duren Raya no.103. Jl. Tanjung Duren Raya no.103. Jl. Tanjung Duren Raya no.103.',
			})
		}

		fillArray = (addresses) => {
			const arr = []
			addresses.map((address) => {
				arr.push(...Array(Math.floor(10 / addresses.length)).fill({
					id: address.id,
					receiver: address.receiver,
					phone: address.phone,
					address: address.address,
					district: address.district,
					postal: address.postal,
				}))
			})
			return arr
		}

		pageRenderer = (data, i) => {
			return (
				<BoxBit unflex style={ Styles.page } key={ i }>
					<BoxBit unflex style={Styles.content}>
						{ data.map(this.contentRenderer) }
					</BoxBit>
				</BoxBit>
			)
		}

		contentRenderer({
			id,
			receiver,
			phone,
			address,
			district,
			postal,
		}, i) {
			return (
				<BoxBit unflex style={ Styles.address } key={ i }>
					<SpectralBit type={SpectralBit.TYPES.FOOT_NOTE_1}>
						{ id }
					</SpectralBit>
					<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
						{ receiver }
					</SpectralBit>
					<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
						{ phone }
					</SpectralBit>
					<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
						{ address }
					</SpectralBit>
					<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
						{ district }
					</SpectralBit>
					<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
						{ postal }
					</SpectralBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		viewOnError() {
			return (
				<BoxBit centering style={ Styles.container }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.title }>
						Oops… Something went wrong
					</GeomanistBit>
					<ButtonBit
						title="Retry"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						onPress={ () => {
							this.props.refresh()
						} }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit unflex style={Styles.container}>
					{ chunk(this.fillArray(this.props.data), 10).map(this.pageRenderer) }
				</BoxBit>
			)
		}
	}
)
