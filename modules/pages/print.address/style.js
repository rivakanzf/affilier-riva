import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.white.primary,
		// width: '100vw',
		// height: '29cm',
	},

	page: {
		width: '100vw',
		height: '100vh',
		paddingLeft: 118,
		paddingRight: 90,
		paddingTop: '.4cm',
		paddingBottom: '4cm',
	},

	address: {
		width: '50%',
		height: '20%',
		paddingBottom: '.2cm',
		paddingRight: 26,
		overflow: 'hidden',
		justifyContent: 'center',
	},

	content: {
		height: '20.5cm',
		flexWrap: 'wrap',
	},
})
