import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import ShipmentService from 'app/services/shipment';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import LoaderBit from 'modules/bits/loader';
import SpectralBit from 'modules/bits/spectral';

import Barcode from 'react-barcode'

import Styles from './style';

import { chunk, isEmpty } from 'lodash';


export default ConnectHelper(
	class PrintAddressPage extends PageModel {

		static routeName = 'print.barcode'

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return oP.id ? ShipmentService.getShipmentData(oP.id, state.me.token) : null
		}

		static getDerivedStateFromProps(nP, nS) {
			if(!isEmpty(nS)) {
				return nS
			} else {
				if(!isEmpty(nP.data)) {
					return {
						destination: nP.data.map(dt => {
							return {
								shippingId: dt.id,
								...dt.destination,
								awb: dt.awb,
							}
						}),
					}
				}
				return false
			}

		}

		
		constructor(p) {
			super(p, {
			}, [
			])

			this._datas = Array(10).fill({
				id: '123',
				name: 'nuel',
				phone: '089664066157',
				address: 'Jl. Tanjung Duren Raya no.103. Jl. Tanjung Duren Raya no.103. Jl. Tanjung Duren Raya no.103. Jl. Tanjung Duren Raya no.103. Jl. Tanjung Duren Raya no.103.',
			})
		}

		// componentDidMount() {
		// 	this.getData()
		// }

		// getData = () => {
		// 	this.setState({
		// 		isLoading: true,
		// 	}, () => {
		// 		ShipmentService.getShipmentData(this.props.id, this.props.token).then(res => {
		// 			res.map( dt => {
		// 				this.setState({
		// 					isLoading: true,
		// 					destination: [
		// 						...this.state.destination,
		// 						{
		// 							...dt.destination,
		// 							awb: ShipmentService.getSicepatWaybill(dt.id, this.props.token).then(() => {
		// 								this.setState({
		// 									isLoading:false,
		// 								})
		// 							}),
		// 						}],
		// 				})
		// 			})
		// 		})
			
		// 	})
			

		// }

		fillArray = (addresses) => {
			const arr = []
			addresses.map((address) => {
				arr.push(...Array(Math.floor(6 / addresses.length)).fill({
					id: address.id,
					receiver: address.receiver,
					phone: address.phone,
					address: address.address,
					district: address.district,
					postal: address.postal,
					awb: address.awb,
				}))
			})
			return arr
		}

		pageRenderer = (data, i) => {
			return (
				<BoxBit unflex style={ Styles.page } key={ i }>
					<BoxBit unflex style={Styles.content}>
						{ data.map(this.contentRenderer) }
					</BoxBit>
				</BoxBit>
			)
		}

		contentRenderer({
			destination,
			awb,
		}, i) {
			return (
				<BoxBit unflex row style={ [Styles.address] } key={ i }>
					<Barcode
						height={ 50 }
						width={ 2 }
						value={ awb }
					/>
					<BoxBit style={Styles.detail}>
						<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
							{ destination.receiver }
						</SpectralBit>
						<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
							{ destination.phone }
						</SpectralBit>
						<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
							{ destination.address }
						</SpectralBit>
						<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
							{ destination.district }
						</SpectralBit>
						<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4}>
							{ destination.postal }
						</SpectralBit>
					</BoxBit>
					
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		viewOnError() {
			return (
				<BoxBit centering style={ Styles.container }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.title }>
						Oops… Something went wrong
					</GeomanistBit>
					<ButtonBit
						title="Retry"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						onPress={ () => {
							this.props.refresh()
						} }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit unflex centering style={Styles.container}>
					{ chunk(this.props.data, 7).map(this.pageRenderer) }
					{/* { this.state.destination.map(this.contentRenderer) } */}
				</BoxBit>
			)
		}
	}
)
