import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		paddingTop: 12,
		backgroundColor: Colors.white.primary,
		// width: '100vw',
		// height: '29cm',
	},

	page: {
		width: '100vw',
		height: '100vh',
		paddingLeft: 118,
		paddingRight: 90,
		paddingTop: '.4cm',
		paddingBottom: '4cm',
	},

	address: {
		width: '14cm',
		height:'4cm',
		borderWidth: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: -1,
		// width: '50%',
		// height: '20%',
		// paddingRight: 26,
		// overflow: 'hidden',
	},

	barcode: {
		width:'5.5cm',
	},

	detail: {
		width:'18.5cm',

	},

	content: {
		height: '20.5cm',
	},

	hidden: {
		visibility: 'hidden',
	},

	'@media print': {
		page: {
			pageBreakAfter: 'always',
		},
	},
})
