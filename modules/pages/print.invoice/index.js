import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import StylesheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import TextBit from 'modules/bits/text';
import LoaderBit from 'modules/bits/loader';
import ImageBit from 'modules/bits/image';
import SpectralBit from 'modules/bits/spectral';
import GeomanistBit from 'modules/bits/geomanist';

import TableLego from 'modules/legos/table';

import Styles from './style';

const Logo = ImageBit.resolve('tile-wide.png')


export default ConnectHelper(
	class PrintInvoicePage extends PageModel {

		static routeName = 'print.invoice'

		static propsToPromise(state, oP) {
			return oP.id ? StylesheetService.getPrintInvoice(oP.id, {token: state.me.token}) : null
		}

		constructor(p) {
			super(p, {
			}, [
				'footerTable',
			])
			this._headerTable = [{
				children: (
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_5} weight="semibold">
						NO
					</GeomanistBit>
				),
				width: 1,
			}, {
				children: (
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_5} weight="semibold">
						BRAND
					</GeomanistBit>
				),
				width: 2,
			}, {
				children: (
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_5} weight="semibold">
						ITEM NAME
					</GeomanistBit>
				),
				width: 5,
			}, {
				children: (
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_5} align="center" weight="semibold">
						SIZE
					</GeomanistBit>
				),
				width: 1,
				align: 'center',
			}, {
				children: (
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_5} align="right" weight="semibold">
						PRICE(IDR)
					</GeomanistBit>
				),
				width: 2,
				align: 'right',
			}]

			this._footerTable = [{
				title: 'STYLIST PICKS TOTAL',
			}, {
				title: 'YOU SAVED',
			}, {
				title: 'YOU PAID',
				weight: 'semibold',
			}]
		}

		footerTable(price, {
			title,
			weight,
		}, i) {
			return (
				<BoxBit row unflex key={i} >
					<BoxBit style={{flexBasis: '600%'}} />
					<BoxBit style={[{flexBasis: '190%'}, Styles.item, i === 0 && Styles.border]}>
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_5} weight="semibold" align="right">
							{ title }
						</GeomanistBit>
					</BoxBit>
					<BoxBit style={[{flexBasis: '200%'}, Styles.padder16, Styles.item, i === 0 && Styles.border]}>
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_4} weight={weight} align="right">
							{ price[i]
								? `IDR ${FormatHelper.currencyFormat(price[i])}`
								: '-'
							}
						</GeomanistBit>
					</BoxBit>
				</BoxBit>
			)
		}

		itemRenderer(item, i) {
			return {
				data: [{
					children: (
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_4}>
							{ i + 1 }
						</GeomanistBit>
					),
				}, {
					children: (
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_4}>
							{ item.brand || '-' }
						</GeomanistBit>
					),
				}, {
					children: (
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_4}>
							{ item.title || '-' }
						</GeomanistBit>
					),
				}, {
					children: (
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_4} align="center">
							{ item.size || '-' }
						</GeomanistBit>
					),
				}, {
					children: (
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_4} align="right">
							{ item.price ? `IDR ${FormatHelper.currencyFormat(item.price)}` : '-' }
						</GeomanistBit>
					),
				}],
			}
		}

		contentRenderer = ({
			id,
			client,
			email,
			stylist,
			total,
			saved,
			paid,
			inventories,
		}, i) => {
			return (
				<BoxBit key={i} unflex style={Styles.content}>
					<ImageBit
						source={ Logo }
						style={Styles.logo}
					/>
					<BoxBit unflex row style={Styles.padder}>
						<SpectralBit type={SpectralBit.TYPES.SUBHEADER_3}>
							Hi {client},
						</SpectralBit>
						<BoxBit />
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_4}>
							{email}
						</GeomanistBit>
					</BoxBit>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_3}>
						Thanks for letting us style you. Enjoy.
					</GeomanistBit>
					<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4} style={Styles.padder32}>
						Hand-Picked for you by {stylist || '-'}
					</SpectralBit>
					<TableLego unflex compact
						headers={this._headerTable}
						rows={inventories.map(this.itemRenderer)}
						style={Styles.table}
					/>
					<BoxBit unflex>
						{ this._footerTable.map(this.footerTable.bind(this, [ total, saved, paid ])) }
						{/* { this.footerTable(saved, paid, total) } */}
					</BoxBit>
					<BoxBit unflex style={Styles.footer}>
						<GeomanistBit align="right" type={GeomanistBit.TYPES.NOTE_3} style={Styles.orderNumber}>
							{id || '-'}
						</GeomanistBit>
						<SpectralBit weight="bold" type={SpectralBit.TYPES.SUBHEADER_4} style={Styles.padder4}>
							Spread the heat, share the love.
						</SpectralBit>
						<GeomanistBit weight="normal" type={GeomanistBit.TYPES.NOTE_3}>
							Obsessed with your new look? Share it with us. We can't wait to see what you come up with. Make sure to <TextBit weight="semibold">#YunaMatchbox</TextBit> and tag <TextBit weight="semibold">@yunaandco</TextBit>
						</GeomanistBit>
					</BoxBit>
					<SpectralBit type={SpectralBit.TYPES.SUBHEADER_4} weight="bold" align="center">
						Thank You
					</SpectralBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit />
				</BoxBit>
			)
		}

		viewOnError() {
			return (
				<BoxBit centering>
					<GeomanistBit type={GeomanistBit.TYPES.NOTE_3}>
						No data found or received ...
					</GeomanistBit>
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
						Please check data compliance
					</GeomanistBit>

					<BoxBit unflex centering style={Styles.error}>
						<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
							If you are sure or this issue persisted, contact dev team
						</GeomanistBit>
					</BoxBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit style={Styles.page}>
					<BoxBit row style={Styles.container}>
						{ this.props.data.map(this.contentRenderer) }
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
