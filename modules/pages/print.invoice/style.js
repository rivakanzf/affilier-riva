import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	page: {
		// height: 793.7007874,
		// width: 1122.519685,
		backgroundColor: Colors.white.primary,
	},
	logo: {
		height: 66,
		width: 66,
		alignSelf: 'center',
		marginBottom: 32,
	},
	error: {
		marginTop: Sizes.margin.thick,
	},

	container: {
		// height: 793.7007874,
		width: 1122.519685,
		flexWrap: 'wrap',
	},

	content: {
		width: '50%',
		height: 793.7007874,
		paddingTop: 32,
		paddingBottom: 24,
		paddingLeft: 48,
		paddingRight: 48,
	},

	default: {
		paddingRight: 16,
	},

	table: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
		marginTop: 12,
	},
	padder16: {
		paddingRight: 16,
	},
	item: {
		paddingTop: 6,
		paddingBottom: 6,
	},

	border: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
	},
	noBorder: {
		borderBottom: 0,
	},

	footer: {
		paddingTop: 48,
		paddingBottom: 32,
	},

	orderNumber: {
		paddingBottom: 8,
		color: Colors.black.palette(2, .6),
	},

	padder: {
		paddingBottom: 4,
	},
	padder4: {
		paddingBottom: 4,
	},
	padder32: {
		paddingTop: 32,
	},

	// '@media print': {
	// 	'@page': {
	// 		size: 'A4 landscape',
	// 	},
	// },
})
