import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import StylesheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
// import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import SpectralBit from 'modules/bits/spectral';

import Styles from './style';

import { chunk } from 'lodash';


export default ConnectHelper(
	class PrintStylistNotePage extends PageModel {

		static routeName = 'print.stylist.note'

		static propsToPromise(state, oP) {
			return oP.id ? StylesheetService.getNotes(oP.id, state.me.token) : null
		}

		constructor(p) {
			super(p, {})

			this._imageTransformation = {
				crop: 'fit',
			}
		}

		pageRenderer = (notes, i) => {
			return (
				<BoxBit unflex style={ Styles.page } key={ i }>
					{ notes.map(this.contentRenderer) }
				</BoxBit>
			)
		}

		contentRenderer = ({
			id,
			note,
			client,
			signature,
		}, i) => {
			return (
				<BoxBit unflex style={ Styles.note } key={ i }>
					<SpectralBit type={ SpectralBit.TYPES.SUBHEADER_2 } style={ Styles.title }>
						Hello { client || '-' },
					</SpectralBit>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
						{ note }
					</GeomanistBit>
					<BoxBit/>
					<BoxBit unflex>
						<BoxBit row style={ Styles.tag }>
							<BoxBit unflex>
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_4 } style={ Styles.number }>
									{ id || '-' }
								</GeomanistBit>
								<ImageBit
									source="//app/logo/yuna.png"
									transform={{crop: 'fit'}}
									style={Styles.image}
								/>
							</BoxBit>
							<BoxBit unflex>
								{ !!signature ? (
									<ImageBit source={ signature } resizeMode={ImageBit.TYPES.CONTAIN} transform={ this._imageTransformation } style={Styles.signature} />
								) : false }
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_3 }>
									#YUNAMATCHBOX
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit centering>
					<LoaderBit simple />
				</BoxBit>
			)
		}

		viewOnError() {
			return (
				<BoxBit centering style={ Styles.container }>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.title }>
						Oops… Something went wrong
					</GeomanistBit>
					<ButtonBit
						title="Retry"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						onPress={ () => {
							this.props.refresh()
						} }
					/>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit unflex style={Styles.container}>
					{ chunk(this.props.data, 4).map(this.pageRenderer) }
				</BoxBit>
			)
		}
	}
)
