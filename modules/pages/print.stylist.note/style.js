import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.white.primary,
		// width: '100%',
		// height: '29cm',
	},

	page: {
		flexWrap: 'wrap',
		width: '100vw',
		height: '100vh',
	},

	note: {
		width: '50%',
		height: '50%',
		paddingTop: 40,
		paddingLeft: 24,
		paddingRight: 24,
		paddingBottom: 80,
		borderWidth: .5,
		borderColor: Colors.black.palette(2, .06),
	},

	signature: {
		height: 80,
		width: 80,
		marginBottom: 18,
	},

	title: {
		paddingBottom: 12,
	},

	number: {
		paddingBottom: 8,
		color: Colors.black.palette(2, .6),
	},

	tag: {
		justifyContent: 'space-between',
		alignItems: 'flex-end',
	},

	image: {
		width: 64,
		height: 20,
	},

	'@media print': {
		page: {
			pageBreakAfter: 'always',
		},
	},
})
