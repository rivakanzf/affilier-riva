import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import StylesheetService from 'app/services/style.sheets';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import TextBit from 'modules/bits/text';
import IconBit from 'modules/bits/icon';
import ButtonBit from 'modules/bits/button';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';

import TableLego from 'modules/legos/table';
import BadgeStatusLego from 'modules/legos/badge.status';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import StylistNotePagelet from 'modules/pagelets/stylist.note';
import QuickViewStylesheetPagelet from 'modules/pagelets/quick.view.stylesheet';

import Styles from './style';

import { without } from 'lodash'


export default ConnectHelper(
	class PrintPage extends PageModel {
		static TYPES = {
			ADDRESS: 'ADDRESS',
			GIFT: 'GIFT',
			STYLIST_NOTE: 'STYLIST_NOTE',
		}

		static routeName = 'print'

		constructor(p) {
			super(p, {
				search: p.search || null,
				isLoading: true,
				status: null,
				count: 40,
				offset: 0,
				total: 0,

				data: [],
				selectedIds: [],

				token: undefined,
			})
		}

		componentDidUpdate(pP, pS) {
			if(
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.status !== this.state.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		tableHeader = [{
			title: '',
			width: .5,
		}, {
			title: 'Order ID',
			width: 2,
		}, {
			title: 'Stylesheet ID',
			width: 2.5,
		}, {
			title: 'Stylist',
			width: 1.5,
		}, {
			title: 'Status',
			width: 1.5,
		}, {
			width: .5,
		}]

		convertTitle = val => {
			switch(val) {
			case this.TYPES.ADDRESS:
				return 'Address'
			case this.TYPES.STYLIST_NOTE:
				return 'Stylist Note'
			case this.TYPES.GIFT:
				return 'Voucher / Gift'
			default:
				return val
			}
		}

		getData = () => {
			const {
				offset,
				count,
				search,
			} = this.state

			this.setState({
				isLoading: true,
			}, () => {
				StylesheetService.getStyleSheets({
					offset,
					search,
					limit: count,
					no_shipment: 'true',
					status: 'PUBLISHED',
				}, this.state.token)
					.then(res => {
						this.setState({
							isLoading: false,
							total: res.count,
							data: res.data,
						})
					})
					.catch(this.onError)
			})
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong.',
				})
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onFilterType = type => {
			switch(type) {
			case this.TYPES.INVOICE:
			case this.TYPES.GIFT:
				return true
			default:
				return false
			}
		}

		onChangeFilter = (key, val) => {
			this.setState({
				[key]: val,
				offset: 0,
			})
		}

		onCheck = id => {
			if (this.state.selectedIds.indexOf(id) > -1) {
				this.setState({
					selectedIds: without(this.state.selectedIds, id),
				})
			} else {
				this.setState({
					selectedIds: [...this.state.selectedIds, id],
				})
			}
		}

		onSelectTemplate = key => {
			switch(key)	{
			case this.TYPES.ADDRESS:
				return window.open(`${window.location.origin}/stylesheet/${this.state.selectedIds.join(',')}/print/address`)
			case this.TYPES.STYLIST_NOTE:
				return window.open(`${window.location.origin}/stylesheet/${this.state.selectedIds.join(',')}/print/stylist/note`)
			default:
				return this.utilities.notification.show({
					title: 'Service not provided',
					// message: '',
				})
			}
		}

		onNavigateToDetail = (id, type, refId) => {
			this.onModalRequestClose()

			this.navigator.navigate(`${type.toLowerCase()}/${refId}`)
		}

		onNavigateToPrint = () => {
			this.utilities.menu.show({
				title: 'Template Type',
				actions: Object.values(this.TYPES).map(val => {
					return {
						title: this.convertTitle(val),
						disabled: this.onFilterType(val),
						isActive: val === this.state.template,
						onPress: this.onSelectTemplate.bind(this, val),
						type: this.props.utilities.menu.TYPES.ACTION.RADIO,
					}
				}),
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onNavigateToBack = () => {
			this.navigator.back()
		}

		onNavigateToStylesheet = id => {
			this.navigator.navigate('stylesheet/' + id)
		}

		onNavigate = (link, param) => {
			this.navigator.navigate(link, param)
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onSearchHandler = searchValue => {
			this.setState({
				search: searchValue,
				offset: 0,
			})
		}

		onEdit = stylesheet => {
			this.utilities.alert.modal({
				component: (
					<StylistNotePagelet
						note={ stylesheet.note }
						client={ stylesheet.client }
						value={ stylesheet.stylist_note }
						onClose={ this.onModalRequestClose }
						onSubmit={ this.onEditStylistNote.bind(this, stylesheet) }
					/>
				),
			})
		}

		onEditStylistNote = (stylesheet, val) => {
			return Promise.resolve().then(() => {
				return StylesheetService.publish(stylesheet.id, stylesheet.status, val, {}, this.state.token)
			}).then(() => {
				this.props.utilities.notification.show({
					title: 'Congratulations 🎉🎉🎉',
					message: 'One stylesheet down. Let\'s keep the spirit up!',
					type: this.props.utilities.notification.TYPES.SUCCESS,
				})

				stylesheet.stylist_note = val

				this.onModalRequestClose()
			}).catch(this.onError)
		}

		onQuickView = stylesheet => {
			this.utilities.alert.modal({
				component: (
					<QuickViewStylesheetPagelet
						id={ stylesheet.id }
						onNavigate={ this.onNavigate }
					/>
				),
			})
		}

		rowRenderer = stylesheet => {
			return {
				data: [{
					children: (
						<CheckboxBit
							dumb
							isActive={this.state.selectedIds.indexOf(stylesheet.id) > -1}
							onPress={this.onCheck.bind(this, stylesheet.id)}
						/>
					),
				}, {
					title: stylesheet.order || '-',
				}, {
					children: (
						<BoxBit style={ Styles.multiline }>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								<TextBit style={ Styles.id }>[#ST-{ stylesheet.id }]</TextBit> { stylesheet.title }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: stylesheet.stylist || '-',
				}, {
					children: (
						<BadgeStatusLego status={stylesheet.status || undefined}/>
					),
				}, {
					children: (
						<BoxBit row style={ Styles.icons }>
							<TouchableBit style={ Styles.icon } centering row onPress={this.onEdit.bind(this, stylesheet)}>
								<IconBit
									name="edit"
									size={20}
									color={Colors.primary}
								/>
							</TouchableBit>
							<TouchableBit style={ Styles.icon } centering row onPress={this.onQuickView.bind(this, stylesheet)}>
								<IconBit
									name="quick-view"
									size={20}
									color={Colors.primary}
								/>
							</TouchableBit>
						</BoxBit>
					),
				}],
				onPress: this.onCheck.bind(this, stylesheet.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles="packing"
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Print' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Print"
								search={ this.state.search }
								searchPlaceholder="Find YCO or Stylist"
								onSearch={ this.onSearchHandler }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							>
								<ButtonBit
									title="Print"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={ this.state.selectedIds.length ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
									onPress={this.onNavigateToPrint}
								/>
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={Styles.padder}
							/>
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange}
							/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
