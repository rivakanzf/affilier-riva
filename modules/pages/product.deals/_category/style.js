import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Defaults from 'coeur/constants/default'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingTop: Sizes.margin.default,
	},

	categoryGallery: {
		paddingTop: 8,
		paddingBottom: 8,
		width: 72,
	},

	image: {
		width: 72,
		height: 72,
	},

	containerText: {
		marginTop: 8,
		marginBottom: 8,
		textTransform: 'capitalize',
	},

	categoryList: {
		padding: 12,
		borderWidth: 2,
		borderColor: Colors.grey.palette(16),
		textTransform: 'capitalize',
	},

	categoryListActive: {
		borderColor: Colors.black.primary,
	},

	selection: {
		flexShrink: 0,
		flexBasis: '50%',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		textTransform: 'capitalize',
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .1),
	},

	selectionInLoading: {
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .1),
	},

	menuListContainer: {
		padding: 0,
		maxHeight: '70vh',
	},

	menuListHeader: {
		justifyContent: 'space-between',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: 1,
		borderColor: Colors.black.palette(3, .16),
	},

	menuListFooter: {
		paddingTop: 12,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: 8,
		backgroundColor: Colors.white.primary,
		borderTopWidth: 1,
		borderColor: Colors.black.palette(2, .1),
	},

	menuListFooterBtn: {
		padding: 8,
	},

	menuListItems: {
		minHeight: 300,
		height: '100%',
		paddingRight: Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		textTransform: 'capitalize',
	},

	menuListItemsCollapsible: {
		paddingLeft: Sizes.margin.default,
	},

	menuListItemDot: {
		width: 20,
		height: 20,
	},

	menuListItemDotInactive: {
		borderColor: Colors.solid.grey.palette(3),
		...(Defaults.PLATFORM === 'web' && {
			'&:hover': {
				borderColor: Colors.black.primary,
			},
			transition: 'border-color linear .07s',
		}),
	},

	menuListItem: {
		alignItems: 'center',
		marginBottom: Sizes.margin.default,
		textTransform: 'capitalize',
		...(Defaults.PLATFORM === 'web' && {
			'&:hover $menuListItemDotInactive': {
				borderColor: Colors.black.primary,
			},
		}),
	},

	menuListItemTitle: {
		marginLeft: Sizes.margin.default / 2,
	},

	// ...(Defaults.PLATFORM === 'web' ? {
	// } : {} ),

})
