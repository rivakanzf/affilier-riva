import React from 'react'
import QueryStatefulModel from 'coeur/models/query.stateful'
import Connect from 'coeur/helpers/connect'

import BoxBit from 'modules/bits/box'
import TouchableBit from 'modules/bits/touchable'
import ImageBit from 'modules/bits/image'
import GeomanistBit from 'modules/bits/geomanist'
import LoaderBit from 'modules/bits/loader'

import FilterBoxLego from 'modules/legos/filter.box'

import { isEmpty, capitalize, difference } from 'lodash'
import Styles from './style'

let cache = null

export default Connect(
	class ColorPart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				colorIds: PropTypes.arrayOf(PropTypes.id),
				onChangeColorIds: PropTypes.func,
				inFilterModal: PropTypes.bool,
			}
		}

		static propsToQuery(state) {
			return !cache ? [`query {
				colorsList {
					id
					title
					hex
					image
				}
			}`, {}, state.me.token] : null
		}

		static getDerivedStateFromProps(nP) {
			if (!isEmpty(nP.data)) {
				cache = nP.data.colorsList.filter(d => d.title !== 'Light Yellow' && d.title !== 'Navy' )
				// It should be done in backend. Light Yellow and Navy are not exist in SPQ Tagging anymore.
			} else if (cache) {
				// check for cache
			} else {
				return null
			}

			const compareStr = (str1, str2) => {
				return str1.toLowerCase() === str2
			}

			return {
				// colors: [].concat(orderBy(cache.colorsList.map(c => {
				// 	return {
				// 		key: c.id,
				// 		title: capitalize(c.title),
				// 		hex: c.hex,
				// 		image: c.image,
				// 	}
				// }), ['title'], 'asc')),
				colors: cache.reduce((acc, color) => {
					const { id, title, hex, image } = color
					if(
						compareStr(title, 'nude') ||
						compareStr(title, 'beige') ||
						compareStr(title, 'ecru')
					) {
						acc.Beige.ids.push(id)
						acc.Beige.hex = hex
						acc.Beige.image = image
					} else if(
						compareStr(title, 'teal') ||
						compareStr(title, 'turquoise') ||
						compareStr(title, 'dark blue') ||
						compareStr(title, 'blue') ||
						compareStr(title, 'light blue')
					) {
						acc.Blue.ids.push(id)
						acc.Blue.hex = hex
						acc.Blue.image = image
					} else if(
						compareStr(title, 'dark brown') ||
						compareStr(title, 'brown') ||
						compareStr(title, 'camel') ||
						compareStr(title, 'sand')
					) {
						acc.Brown.ids.push(id)
						acc.Brown.hex = hex
						acc.Brown.image = image
					} else if(
						compareStr(title, 'gold') ||
						compareStr(title, 'rose gold')
					) {
						acc.Gold.ids.push(id)
						if(compareStr(title, 'gold')) {
							acc.Gold.hex = hex
							acc.Gold.image = image
						}
					} else if(
						compareStr(title, 'khaki') ||
						compareStr(title, 'emerald') ||
						compareStr(title, 'olive') ||
						compareStr(title, 'green') ||
						compareStr(title, 'mint')
					) {
						acc.Green.ids.push(id)
						acc.Green.hex = hex
						acc.Green.image = image
					} else if(
						compareStr(title, 'grey') ||
						compareStr(title, 'dark grey')
					) {
						acc.Grey.ids.push(id)
						acc.Grey.hex = hex
						acc.Grey.image = image
					} else if(
						compareStr(title, 'fuschia') ||
						compareStr(title, 'pink') ||
						compareStr(title, 'dusty pink')
					) {
						acc.Pink.ids.push(id)
						acc.Pink.hex = hex
						acc.Pink.image = image
					} else if(
						compareStr(title, 'purple') ||
						compareStr(title, 'lilac')
					) {
						acc.Purple.ids.push(id)
						acc.Purple.hex = hex
						acc.Purple.image = image
					} else if(
						compareStr(title, 'maroon') ||
						compareStr(title, 'red')
					) {
						acc.Red.ids.push(id)
						acc.Red.hex = hex
						acc.Red.image = image
					} else if(
						compareStr(title, 'white') ||
						compareStr(title, 'off white')
					) {
						acc.White.ids.push(id)
						acc.White.hex = hex
						acc.White.image = image
					} else if(
						compareStr(title, 'mustard') ||
						compareStr(title, 'yellow') ||
						compareStr(title, 'cream')
					) {
						acc.Yellow.ids.push(id)
						if(compareStr(title, 'yellow')) {
							acc.Yellow.hex = hex
							acc.Yellow.image = image
						}
					} else {
						// black, orange, silver, multi color, no color
						acc[capitalize(title)].ids.push(id)
						acc[capitalize(title)].hex = hex
						acc[capitalize(title)].image = image
					}

					return acc
				}, {
					Beige: { ids: [], hex: null, image: null },
					Black: { ids: [], hex: null, image: null },
					Blue: { ids: [], hex: null, image: null },
					Brown: { ids: [], hex: null, image: null },
					Gold: { ids: [], hex: null, image: null },
					Green: { ids: [], hex: null, image: null },
					Grey: { ids: [], hex: null, image: null },
					'Multi color': { ids: [], hex: null, image: null },
					'No color': { ids: [], hex: null, image: null },
					Orange: { ids: [], hex: null, image: null },
					Pink: { ids: [], hex: null, image: null },
					Purple: { ids: [], hex: null, image: null },
					Red: { ids: [], hex: null, image: null },
					Silver: { ids: [], hex: null, image: null },
					White: { ids: [], hex: null, image: null },
					Yellow: { ids: [], hex: null, image: null },
				}),
				isLoading: false,
			}
		}

		static defaultProps = {
			colorIds: [],
		}

		constructor(p) {
			super(p, {
				colors: [],
				isLoading: true,
				colorIds: p.colorIds,
			})
		}

		onSelectColor = ids => {
			// const colorIds = this.state.colorIds.concat(ids)
			// this.setState({
			// 	colorIds: this.state.colorIds.indexOf(key) > -1
			// 		? [ ...(this.state.colorIds.filter(c => c !== key)) ]
			// 		: [ ...this.state.colorIds, key ],
			// }, () => this.props.onChangeColorIds(key))
			this.setState(state => ({
				colorIds: state.colorIds.indexOf(ids[0]) > -1
					? difference(state.colorIds, ids)
					: [ ...state.colorIds, ...ids ],
			}), () => {
				this.props.onChangeColorIds(this.state.colorIds)
			})
		}

		colorRenderer = (key, i) => {
			const { ids, hex, image } = this.state.colors[key]

			const { inFilterModal } = this.props
			const colorIds = inFilterModal ? this.state.colorIds : this.props.colorIds
			return (
				<TouchableBit
					unflex
					key={ i }
					row={ !inFilterModal }
					centering
					style={ [Styles.listColor, inFilterModal && Styles.listColorRow] }
					onPress={ () => this.onSelectColor(ids) }
				>
					<BoxBit unflex style={[
						Styles.listColorContainer,
						inFilterModal && Styles.listColorContainerBig,
						!isEmpty(colorIds) && colorIds.indexOf(ids[0]) > -1 && Styles.listColorContainerActive,
					]}>
						{ key === 'Multi color' ? (
							<ImageBit broken={ !image } source={ image } style={ Styles.listColorImage }/>
						) : (
							<BoxBit style={[Styles.listColorBg, { background: hex }]}/>
						) }
					</BoxBit>
					<GeomanistBit type={ inFilterModal ? GeomanistBit.TYPES.NOTE_1 : GeomanistBit.TYPES.PARAGRAPH_3 } style={[Styles.listColorTitle, inFilterModal && Styles.listColorTitleRow]}>
						{ key }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering style={ Styles.listCollapsible }>
					<LoaderBit simple/>
				</BoxBit>
			)
		}

		view() {
			if(this.state.isLoading) return this.viewOnLoading()
			return (
				<FilterBoxLego title="Warna" style={ this.props.inFilterModal && Styles.listCollapsible }>
					<BoxBit unflex row={ this.props.inFilterModal } style={ this.props.inFilterModal && Styles.listColorsRow }>
						{ Object.keys(this.state.colors).map(this.colorRenderer) }
					</BoxBit>
				</FilterBoxLego>
			)
		}
	}
)
