import React from 'react'
import QueryStatefulModel from 'coeur/models/query.stateful'
import Connect from 'coeur/helpers/connect'

import BoxBit from 'modules/bits/box'
import CheckboxBit from 'modules/bits/checkbox'
import TouchableBit from 'modules/bits/touchable'
import GeomanistBit from 'modules/bits/geomanist'
import LoaderBit from 'modules/bits/loader'

import FilterBoxLego from 'modules/legos/filter.box'

import { isEmpty, isNil } from 'lodash'

import Styles from './style'

let queryCache = null
let lastCategoryIds = []

const setSizes = ({sizes}) => {
	return {
		sizes,
		isLoading: false,
	}
}

const sizesListFilter = (categoriesList, sizesList, categoryIds) => {
	return sizesList.filter(s => {
		const picker = s.categorySizes.find(cs => cs.categoryId === categoryIds[0])
			|| (
				!isEmpty(categoriesList) && s.categorySizes.find(cs => cs.categoryId === categoriesList[0].category.id)
			)
		
		return picker
	})
}

export default Connect(
	class SizePart extends QueryStatefulModel {
		
		static propTypes(PropTypes) {
			return {
				categoryIds: PropTypes.arrayOf(PropTypes.id),
				sizeIds: PropTypes.arrayOf(PropTypes.id),
				onChangeSizeIds: PropTypes.func,
				inFilterModal: PropTypes.bool,
			}
		}

		constructor(p) {
			super(p, {
				sizes: [],
				isLoading: true,
				sizeIds: p.sizeIds,
			})
		}

		static propsToQuery(state, props) {
			const { categoryIds } = props
			return [`query {
				${!isEmpty(props.categoryIds) ? `categoriesList(filter: {
					id: {
						isNull: false
						equalTo: ${categoryIds[0]}
					}
				}) {
					id
					category {
						id
					}
				}` : ''}
				sizesList(filter: {
					sizeId: {
						isNull: true
					}
				}) {
					id
					title
					categorySizes {
						sizeId
						categoryId
					}
					sizes {
						id
						title
					}
				}
			}`, {
				skip: !!queryCache && categoryIds[0] === lastCategoryIds,
			}, state.me.token]
		}

		static getDerivedStateFromProps(nP) {
			if(!isEmpty(nP.data)) {
				const { sizesList } = nP.data
				if(nP.categoryIds) {
					const categoriesList = !isNil(nP.data.categoriesList) && !isNil(nP.data.categoriesList[0].category) ? nP.data.categoriesList : null
					queryCache = {
						categoriesList,
						sizesList,
						sizes: sizesListFilter(categoriesList, sizesList, nP.categoryIds),
					}
					lastCategoryIds = nP.categoryIds
					if(!isEmpty(queryCache.sizes)) return setSizes(queryCache)
					return {
						sizes: sizesList,
						isLoading: false,
					}
				}
				return {
					sizes: sizesList,
					isLoading: false,
				}
			} else if(!!queryCache) {
				if(nP.categoryIds) {
					lastCategoryIds = nP.categoryIds
					queryCache.sizes = sizesListFilter(queryCache.categoriesList, queryCache.sizesList, nP.categoryIds)
				} else {
					queryCache.sizes = queryCache.sizesList
				}
				return setSizes(queryCache)
			} else {
				return {
					sizes: [],
					isLoading: false,
				}
			}
		}

		onSelectListSize = id => {
			this.setState({
				...this.state,
				sizeIds: this.state.sizeIds.indexOf(id) > -1
					? [ ...(this.state.sizeIds.filter(s => s !== id)) ]
					: [ ...this.state.sizeIds, id ],
			}, () => this.props.onChangeSizeIds(id))
		}

		listSizesCheckboxRenderer = ({
			id,
			title,
		}) => {
			return (
				<TouchableBit key={ id } row style={ Styles.listSize } onPress={() => this.onSelectListSize(id)}>
					<CheckboxBit
						dumb
						isActive={ this.props.sizeIds.indexOf(id) > -1 }
						onPress={() => this.onSelectListSize(id)}
						style={ Styles.listSizeCheckbox }
					/>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.listSizeTitle }>
						{ title }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		listSizesBoxRenderer = ({
			id,
			title,
		}, index) => {
			return (
				<TouchableBit centering unflex key={ id } style={[Styles.listSizeBox, this.state.sizeIds.indexOf(id) > -1 && Styles.listSizeBoxActive, (index + 1) % 3 === 0 ? Styles.listSizeBox3N : {}]} onPress={() => this.onSelectListSize(id)}>
					<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="bold">
						{ title }
					</GeomanistBit>
				</TouchableBit>
			)
		}

		listSizeCategoriesRenderer = ({
			id,
			title,
			sizes,
		}) => {
			return (
				<BoxBit key={ id } unflex style={ Styles.listSizeCategories }>
					<GeomanistBit type={ GeomanistBit.TYPES.CAPTION_2 } style={ Styles.listSizeCategoriesTitle }>
						{ title }
					</GeomanistBit>
					<BoxBit unflex row={ !!this.props.inFilterModal } style={ !!this.props.inFilterModal ? Styles.listSizesBoxes : {} }>
						{ !this.props.inFilterModal
							? sizes.map(this.listSizesCheckboxRenderer)
							: sizes.map(this.listSizesBoxRenderer) }
					</BoxBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit unflex centering style={ Styles.listCollapsible }>
					<LoaderBit simple/>
				</BoxBit>
			)
		}

		view() {
			if(this.state.isLoading) return this.viewOnLoading()
			return (
				<FilterBoxLego title="Ukuran" style={ this.props.inFilterModal ? Styles.listCollapsible : {} }>
					<BoxBit unflex style={Styles.sizes}>
						{ this.state.sizes.map(this.listSizeCategoriesRenderer) }
					</BoxBit>
				</FilterBoxLego>
			)
		}
	}
)
