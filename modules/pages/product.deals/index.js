/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size';

import ProductService from 'app/services/product';

import AuthenticationHelper from 'utils/helpers/authentication'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import IconBit from 'modules/bits/icon'
import LoaderBit from 'modules/bits/loader'
import TextInputBit from 'modules/bits/text.input'
import TouchableBit from 'modules/bits/touchable'
import CheckboxBit from 'modules/bits/checkbox'
import GeomanistBit from 'modules/bits/geomanist'

// import HeaderBreadcrumbLego from 'modules/legos/header.breadcrumb';
import TableGalleryLego from 'modules/legos/table.gallery';

import CardProductComponent from 'modules/components/card.product';
import HeaderFilterComponent from 'modules/components/header.filter';
import PageAuthorizedComponent from 'modules/components/page.authorized';
import PaginationComponent from 'modules/components/pagination';
import SidebarFilterComponent from 'modules/components/sidebar.filter';
import ShareComponent from 'modules/components/share'

import QuickViewProductPagelet from 'modules/pagelets/quick.view.product';

import FilterPart from './_filter'
import CategoryPart from './_category'

import Styles from './style';

import { isEmpty, isEqual, isArray, isNil } from 'lodash';

function getItemWidth(num = 5) {
	return (Sizes.screen.width - (240 + Sizes.margin.default + Sizes.margin.default + (Sizes.margin.default * (num - 1)))) / num
}


export default ConnectHelper(
	class ProductDealsPage extends PageModel {

		static routeName = 'product.deals'

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			let column = 5

			if (Sizes.screen.width <= 768) {
				column = 4
			} else if (Sizes.screen.width > 1366) {
				column = 6
			} else if (Sizes.screen.width >= 1920) {
				column = 8
			}

			const query = CommonHelper.getQueryString()

			const page = query.page
				? query.page > 0 ? query.page : 1
				: 1

			const limit = query.limit
				? query.limit <= 100 ? query.limit : 100
				: 40

			super(p, {
				isLoading: true,
				page,
				limit,
				offset: page * limit - limit,
				// count: query.count || (column * 8),
				total: 0,
				column,
				products: [],
				categoryIds: query.categoryIds ? ( isArray(query.categoryIds) ? query.categoryIds : [query.categoryIds] ) : [],
				search: query.search || null,
				filter: {
					price: query.price ? query.price : '',
					colorIds: query.colorIds ? ( isArray(query.colorIds) ? query.colorIds : [query.colorIds] ) : [],
					brandIds: query.brandIds ? CommonHelper.asArray(query.brandIds) : [],
					sizeIds: query.sizeIds ? ( isArray(query.sizeIds) ? query.sizeIds : [query.sizeIds] ) : [],
					sort: query.sort_by_created_at
						? { sort_by_created_at: query.sort_by_created_at }
						: query.sort_by_price
							? { sort_by_price: query.sort_by_price }
							: { sort_by_created_at: 'DESC'},
				},
				token: undefined,
				available: true,
			})

			this.getterId = 1
			this.itemWidth = getItemWidth(column)
			this.categoryChildren = {}
			this.que = false
		}

		componentDidMount() {
			this.getData()
		}

		componentDidUpdate(pP, pS) {
			if ( !isEqual(pS.categoryIds, this.state.categoryIds)
				|| pS.page !== this.state.page
				|| !isEqual(pS.search, this.state.search)
				|| !isEqual(pS.filter, this.state.filter)
				// || pS.available !== this.state.available
			) {
				console.log('AAAAAAAA')
				const query = {
					page: this.state.page,
					...(!!this.state.search && { search: this.state.search }),
					...(!!this.state.limit && { limit: this.state.limit }),
					...(!isEmpty(this.state.categoryIds) && { categoryIds: this.state.categoryIds }),
					...(!isEmpty(this.state.filter.price) && { price: this.state.filter.price }),
					...(!isEmpty(this.state.filter.colorIds) && { colorIds: this.state.filter.colorIds }),
					...(!isEmpty(this.state.filter.brandIds) && { brandIds: this.state.filter.brandIds }),
					...(!isEmpty(this.state.filter.sizeIds) && { sizeIds: this.state.filter.sizeIds }),
					...(!isEmpty(this.state.filter.sort) &&
						( !isNil(this.state.filter.sort.sort_by_created_at)
							? { sort_by_created_at: this.state.filter.sort.sort_by_created_at }
							: { sort_by_price: this.state.filter.sort.sort_by_price } )),
				}

				window.history.replaceState({}, 'Product Deals', isEmpty(query) ? '/deals' : `/deals?${Object.keys(query).map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}
		
		getData = () => {
			const {
				offset,
				categoryIds,
				filter,
				search,
				limit,
			} = this.state

			this.setState({
				isLoading: true,
			}, () => {
				ProductService.getAllProduct({
					limit,
					offset,
					...(!!search ? { search } : {}),
					...(categoryIds.length ? { category_ids: categoryIds.join(',') } : {}),
					...(filter.price.length ? { price: filter.price } : {}),
					...(filter.colorIds.length ? { color_ids: filter.colorIds.join(',') } : {} ),
					...(filter.brandIds.length ? { brand_ids: filter.brandIds.join(',') } : {}),
					...(filter.sizeIds.length ? { size_ids: filter.sizeIds.join(',') } : {}),
					...(filter.sort.sort_by_created_at ? { sort_by_created_at: filter.sort.sort_by_created_at } : {}),
					...(filter.sort.sort_by_price ? { sort_by_price: filter.sort.sort_by_price } : {}),
					inventory: false,
				}, this.props.token)
					.then(res => {
						this.setState({
							isLoading: false,
							total: res.count,
							products: res.data,
						})
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops',
								message: 'Something went wrong with your request, please try again later',
							})
						})
					})
			})
		}

		onChangeFilter = (key, val, multiple) => {
			this.setState(state => ({
				offset: 0,
				page: 1,
				filter: {
					...state.filter,
					...(!isEmpty(key) ? {
						[key]: !!multiple
							? ( state.filter[key].indexOf(val) > -1
								? [ ...(state.filter[key].filter(d => d !== val)) ]
								: [ ...state.filter[key], val] )
							: isEqual(state.filter[key], val) ? '' : val,
					} : { // this below is for filter change in filter.modal by apply button
						// also color change by bulk update colorIds
						...val,
					}),
				},
			}))
		}

		onChangeCategoryIds = (id, multiple) => {
			this.setState(state => ({
				offset: 0,
				page: 1,
				categoryIds: multiple
					? ( state.categoryIds.indexOf(id) > -1
						? [ ...(state.categoryIds.filter(c => c !== id)) ]
						: [ ...state.categoryIds, id ] )
					: ( !isArray(id) ? [ id ] : id ),
			}))
		}

		onChangeSort = (key, val) => {
			this.setState(state => ({
				offset: 0,
				page: 1,
				filter: {
					...state.filter,
					sort: isNil(state.filter.sort[key])
						? { [key]: val }
						: state.filter.sort[key] !== val
							? { [key]: val }
							: {},
				},
				sort: false,
			}))
		}

		// onResetFilter = () => {
		// 	this.setState({
		// 		filter: {
		// 			price: [],
		// 			colorIds: [],
		// 			brandIds: [],
		// 			sizeIds: [],
		// 		},
		// 	})
		// }

		onResetFilter = () => {
			this.setState({
				offset: 0,
				page: 1,
				categoryIds: [],
				filter: {
					price: '',
					colorIds: [],
					brandIds: [],
					sizeIds: [],
					sort: { sort_by_created_at: 'DESC' },
				},
			})
		}

		onCategoryChildrenLoaded = categoryChildren => {
			this.categoryChildren = categoryChildren

			if(this.que) {
				this.que = false
				this.getData()
			}
		}

		onChangeCategory = categoryId => {
			this.setState({
				categoryId,
				offset: 0,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToPrev = () => {
			this.setState({
				offset: Math.max(0, this.state.offset - this.state.count),
			})
		}

		onGoToNext = () => {
			this.setState({
				offset: Math.min(this.state.total, this.state.offset + this.state.count),
			})
		}

		onAddProduct = () => {
			// TODO
			this.navigator.navigate('product/add/')
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onGoToDetail = id => {
			const product = this.state.products.find(prod => prod.id === id)

			if(product.variants.length > 0) {
				this.utilities.alert.modal({
					component: (
						<QuickViewProductPagelet
							id={ id }
							includeZeroInventory
							onNavigateToProduct={ this.props.isInventory ? () => {
								this.onModalRequestClose()
	
								this.navigator.navigate(`product/${id}`)
							} : undefined }
						/>
					),
				})
			} else {
				this.navigator.navigate(`product/${id}`)

			}
		}

		onClose = () => {
			this.props.utilities.alert.close()
		}

		onCopyLink = (url) => {
			navigator.clipboard.writeText(url)

			this.utilities.notification.show({
				message: 'Copied to clipboard',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})
		}

		onGetLink = (id) => {
			this.utilities.alert.modal({
				component: (
					<ShareComponent
						id={id}
						onClose={this.onClose}
						onCopyLink={this.onCopyLink}
					/>
				),
			})
		}

		onNavigationChange = offset => {
			this.setState(state => ({
				offset,
				page: offset / state.limit + 1,
			}))
		}

		itemRenderer = (item, i) => {
			if(item.variants.length > 0) {
				return (
					<CardProductComponent
						key={ item.id }
						id={ item.id }
						variantId={ item.variants[0].id }
						width={ this.itemWidth }
						image={ item.variants && item.variants[0].asset && item.variants[0].asset.url }
						brand={ item.brand }
						title={ item.title }
						price={ item.prices[1] }
						purchasePrice={ item.prices[0] }
						colors={ item.colors }
						imageStyle={Styles.productImage}
						style={Styles.product}
						onPress={ this.onGetLink }
					/>
				)
			}else {
				return (
					<CardProductComponent
						key={ i }
						id={ item.id }
						width={ this.itemWidth }
						brand={ item.brand }
						title={ item.title }
						onPress={ this.onGetLink }
					/>
				)
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					paths={[{ title: 'Products Deals' }]}
				>
					{() => (
						<BoxBit style={Styles.container}>

							<BoxBit style={[Styles.padder, Styles.pb48]}>
								<BoxBit row>
									<BoxBit unflex style={Styles.contentLeft}>
										<TextInputBit
											placeholder="Cari"
											prefix={ <IconBit name="search" size={ 16 } color={ Colors.grey.palette(12) } /> }
											defaultValue={ this.state.search }
											onSubmitEditing={ this.onSearch }
											style={ Styles.searchContainer }
											inputStyle={ Styles.searchInput }
										/>
										<FilterPart
											filter={this.state.filter}
											onChangeFilter={this.onChangeFilter}
											categoryIds={this.state.categoryIds}
											onChangeCategoryIds={this.onChangeCategoryIds}
											onResetFilter={this.onResetFilter}
										/>
									</BoxBit>
									<BoxBit style={Styles.content}>
										{ this.state.isLoading ? (
											<BoxBit centering style={Styles.wrap}>
												<LoaderBit simple/>
											</BoxBit>
										) : (
											<BoxBit row unflex style={Styles.wrap}>
												{ this.state.products.map(this.itemRenderer) }
											</BoxBit>
										) }
										<BoxBit unflex row>
											<BoxBit />
											<PaginationComponent
												current={this.state.offset}
												count={this.state.limit}
												total={this.state.total}
												onNavigationChange={this.onNavigationChange}
											/>
										</BoxBit>
									</BoxBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
