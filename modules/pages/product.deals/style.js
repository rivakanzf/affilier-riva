import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'utils/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 0,
		paddingLeft: 0,
		paddingRight: 0,
	},

	sidebarFilterTitle: {
		color: Colors.black.palette(2),
	},
	
	wrap: {
		flexWrap: 'wrap',
	},

	filter: {
		top: 80,
	},

	header: {
		marginTop: 16,
	},

	input: {
		marginTop: 4,
		marginBottom: Sizes.margin.default,
	},

	checker: {
		alignItems: 'center',
		marginTop: 8,
		marginBottom: 8,
	},

	check: {
		marginLeft: 8,
	},

	pb48: {
		paddingBottom: Sizes.margin.thick * 2,
	},

	padder: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
	},

	content: {
		paddingTop: Sizes.margin.default,
		paddingLeft: 0,
		paddingRight: 0,
		paddingBottom: Sizes.margin.default * 2,
	},
	
	contentLeft: {
		width: '15%',
		minWidth: 216,
		paddingTop: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingLeft: 8,
	},

	product: {
		width: '25%',
		paddingLeft: 8,
		paddingRight: 8,
	},

	productImage: {
		width: '100%',
	},

	searchContainer: {
		height: 36,
		backgroundColor: Colors.grey.palette(8),
		borderColor: Colors.solid.grey.palette(3),
		marginBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
	},

	searchInput: {
		backgroundColor: Colors.transparent,
		paddingLeft: 8,
	},
})
