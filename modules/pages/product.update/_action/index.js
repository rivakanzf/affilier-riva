import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import SwitchBit from 'modules/bits/switch';

import BoxRowLego from 'modules/legos/box.row';

import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				// id: PropTypes.id,
				// onRequestClose: PropTypes.func,
				onSave: PropTypes.func,
				canSave: PropTypes.bool,
				isLoading: PropTypes.bool,
				isPublished: PropTypes.bool,
				onToggleIsPublished: PropTypes.func,
				updatedAt: PropTypes.date,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Publishing',
							content: this.props.isPublished ? 'This product is published (default)' : 'This product is not publicly available',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: (
								<SwitchBit onChange={ this.props.onToggleIsPublished } value={ this.props.isPublished } style={ Styles.switch } />
							),
						}],
					}, {
						data: [{
							title: 'Save Changes',
							children: (
								<BoxBit unflex>
									<ButtonBit
										title="DELETE PRODUCT"
										theme={ ButtonBit.TYPES.THEMES.WHITE_PRIMARY }
										width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
										weight="medium"
										state={ ButtonBit.TYPES.STATES.DISABLED }
									/>
									<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.time }>
										Last updated: {
											this.props.updatedAt
												? TimeHelper.format(this.props.updatedAt, 'DD MMMM YYYY HH:mm')
												: '-'
										}
									</GeomanistBit>
								</BoxBit>
							),
						}, {
							blank: true,
							children: (
								<ButtonBit
									weight="medium"
									title={ 'UPDATE PRODUCT' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.props.isLoading ? ButtonBit.TYPES.STATES.LOADING : this.props.canSave && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
									onPress={ this.props.onSave }
								/>
							),
						}],
					}]}
				/>
			)
		}
	}
)
