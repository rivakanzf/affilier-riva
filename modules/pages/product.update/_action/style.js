import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	space: {
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingBottom: 16,
		paddingTop: 16,
	},

	last: {
		color: Colors.black.palette(2, .6),
		paddingTop: 8,
	},

	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},

	switch: {
		alignSelf: 'flex-end',
	},

	content: {
		opacity: .6,
	},

	time: {
		paddingTop: 8,
		color: Colors.black.palette(2, .6),
	},
})
