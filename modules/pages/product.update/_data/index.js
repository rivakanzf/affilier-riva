import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities'

// import GeomanistBit from 'modules/bits/geomanist';
import InputValidatedBit from 'modules/bits/input.validated';
import TextInputBit from 'modules/bits/text.input';
import SelectionBit from 'modules/bits/selection'
import BoxBit from 'modules/bits/box'
import TouchableBit from 'modules/bits/touchable'

import TabLego from 'modules/legos/tab'
import BoxLego from 'modules/legos/box';
import RowLego from 'modules/legos/row';
import HintLego from 'modules/legos/hint';
import RowBrandLego from 'modules/legos/row.brand';
import RowCategoryLego from 'modules/legos/row.category';

import Styles from './style';


export default ConnectHelper(
	class DataPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				brandId: PropTypes.number,
				categoryId: PropTypes.number,
				packetId: PropTypes.number,
				title: PropTypes.string,
				description: PropTypes.string,
				material: PropTypes.string,
				care: PropTypes.string,
				remarks: PropTypes.string,
				merchants: PropTypes.array,
				isLoading: PropTypes.bool,
				onChangeBrand: PropTypes.func,
				onChangeCategory: PropTypes.func,
				onChangeTitle: PropTypes.func,
				onChangeDescription: PropTypes.func,
				onChangeFit: PropTypes.func,
				onChangeCare: PropTypes.func,
				onChangeRemarks: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				merchants: p.merchants || [],
			})
		}

		merchantOptions = [{
			title: 'None',
			key: '',
		}, {
			title: 'Tokopedia',
			key: 'tokopedia',
		}, {
			title: 'Shopee',
			key: 'shopee',
		}, {
			title: 'blibli',
			key: 'blibli',
		}]

		static contexts = [
			UtilitiesContext,
		]

		onChangeTitle = (e, val) => {
			this.props.onChangeTitle &&
			this.props.onChangeTitle(val)
		}

		onChangeMerchant = (key) => {
			if(key === '') {
				this.setState({
					merchants: [],
				}, () => this.props.onChangeMerchant(this.state.merchants))

			}else {
				this.setState({
					merchants: [...this.state.merchants, key],
				}, () => this.props.onChangeMerchant(this.state.merchants))
			}


		}

		onRemoveMerchant = (index) => {
			const merchants = this.state.merchants.filter(merchant => merchant !== this.state.merchants[index])
			this.setState({
				merchants: merchants,
			}, () => this.props.onChangeMerchant(this.state.merchants))

		}

		merchantOptionsRenderer = () => {
			const options = this.merchantOptions.filter(option => !this.state.merchants.includes(option.key))
			return options.map(merchant => {
				return {
					...merchant,
					selected: this.props.merchant === merchant.key,
				}
			})
		}

		view() {
			return (
				<BoxLego
					title="Product Data"
					header={(
						<HintLego title="Category cannot be changed once product is created and saved." />
					)}
					contentContainerStyle={Styles.padder}
				>
					<RowBrandLego
						key={ `brand_${this.props.brandId}` }
						brandId={ this.props.brandId }
						isLoading={ this.props.isLoading }
						onChange={ this.props.onChangeBrand }
					/>

					<RowLego
						data={[{
							title: 'Product Name',
							children: (
								<TextInputBit key={ `title_${this.props.title}` }
									defaultValue={ this.props.title }
									placeholder="Input here…"
									onChange={ this.onChangeTitle }
								/>
							),
						}]}
					/>

					<RowCategoryLego
						disabled={ !!this.props.id }
						key={ `cat_${this.props.categoryId}` }
						categoryId={ this.props.categoryId }
						isLoading={ this.props.isLoading }
						onChange={ this.props.onChangeCategory }
					/>

					<RowLego
						data={[{
							title: 'Description',
							children: (
								<InputValidatedBit
									key={ `description_${this.props.description}` }
									type={ InputValidatedBit.TYPES.TEXTAREA }
									value={ this.props.description }
									placeholder="Input here…"
									onChange={ this.props.onChangeDescription }
								/>
							),
						}]}
					/>

					<RowLego
						data={[{
							title: 'Fit & Sizes',
							children: (
								<InputValidatedBit
									key={ `fit_${this.props.fit}` }
									type={ InputValidatedBit.TYPES.TEXTAREA }
									value={ this.props.fit }
									placeholder="Input here…"
									onChange={ this.props.onChangeFit }
								/>
							),
						}]}
					/>

					<RowLego
						data={[{
							title: 'Care',
							children: (
								<InputValidatedBit
									key={ `care_${this.props.care}` }
									type={ InputValidatedBit.TYPES.TEXTAREA }
									value={ this.props.care }
									placeholder="Input here…"
									onChange={ this.props.onChangeCare }
								/>
							),
						}]}
					/>

					<RowLego
						data={[{
							title: 'Remarks',
							children: (
								<InputValidatedBit
									key={ `remarks_${this.props.remarks}` }
									type={ InputValidatedBit.TYPES.TEXTAREA }
									value={ this.props.remarks }
									placeholder="Input here…"
									onChange={this.props.onChangeRemarks}
								/>
							),
						}]}
					/>

					<RowLego
						data={[{
							title: 'E-Commerce Merchant',
							children: (
								<SelectionBit
									options={this.merchantOptionsRenderer()}
									onChange={this.onChangeMerchant}
								/>
							),
						}]}
					/>

					<RowLego
						data={[{
							children: (
								<TabLego
									capsule
									static
									tabs={this.state.merchants.map((merchant, i) => {
										return {
											title: merchant,
											onPress: this.onRemoveMerchant.bind(this, i),
										}
									})}
								/>
							),
						}]}
					/>
					
				</BoxLego>
			)
		}
	}
)
