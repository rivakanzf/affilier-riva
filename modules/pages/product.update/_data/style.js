import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	padder: {
		marginBottom: 16,
		paddingBottom: 0,
	},
	// required: {
	// 	color: Colors.red.palette(3),
	// },
})
