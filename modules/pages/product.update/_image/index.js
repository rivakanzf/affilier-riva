import React from 'react';
import ConnectedStatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';
import ButtonBit from 'modules/bits/button'

import BoxLego from 'modules/legos/box';
import RowImageLego from 'modules/legos/row.image';

import Styles from './style';


export default ConnectHelper(
	class ImagePart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				userId: PropTypes.id,
				token: PropTypes.string,
				setImages: PropTypes.func.isRequired,
				onRefresh: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				images: [],
				isUploading: false,
			})
		}

		onClose = () => {
			this.props.utilities.alert.close()
		}

		onUpdateImage = () => {
			const images = this.state.images.map((img, i) => {
				return {
					image: img.data,
					order: i,
					index: i,
				}
			})
			
			this.props.setImages &&
			this.props.setImages(images)

			this.onClose()
		}

		header = () => {
			return (
				<BoxBit row unflex centering style={Styles.header}>
					<BoxBit />
					<TouchableBit unflex centering onPress={this.onClose} style={Styles.icon}>
						<IconBit name="close" />
					</TouchableBit>
				</BoxBit>
			)
		}

		onUpdate = images => {
			this.state.images = images
		}

		view() {
			return (
				<BoxBit style={Styles.container}>
					{ this.header() }
					<BoxLego
						title="Upload Images"
						contentContainerStyle={Styles.padder}
					>
						<RowImageLego
							headerless
							title=""
							addable
							max_attachment={ 5 }
							onUpdate={ this.onUpdate }
						/>
						<ButtonBit
							state={ this.state.isUploading ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL}
							title="OK"
							onPress={ this.onUpdateImage }
						/>
					</BoxLego>
				</BoxBit>
			)
		}
	}
)
