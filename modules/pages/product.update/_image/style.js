import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		width: '60%',
		justifyContent: 'center',
	},


	header: {
		backgroundColor: Colors.transparent,
		height: 48,
	},

	icon: {
		backgroundColor: Colors.white.primary,
		width: 36,
		height: 36,
		borderRadius: 36 / 2,
	},

	padder: {
		marginTop: 16,
	},
})
