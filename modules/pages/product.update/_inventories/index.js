import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'coeur/constants/color';

import VariantService from 'app/services/variant';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status'
import BoxLego from 'modules/legos/box';
import TableLego from 'modules/legos/table';

import PaginationComponent from 'modules/components/pagination'

import InventoryUpdatePagelet from 'modules/pagelets/inventory.update';
import InventoryStatusPagelet from 'modules/pagelets/inventory.status';

import Styles from './style';


export default ConnectHelper(
	class InventoriesPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				variantId: PropTypes.id,
				refresh: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				current: 0,
				count: 10,
				total: 0,
				inventories: [],
			})
		}

		headers = [{
			title: 'ID',
			width: 1.2,
		}, {
			title: 'Price',
			width: 1.5,
		}, {
			title: 'Note',
			width: 3,
		}, {
			title: 'Status',
			width: 2,
		}, {
			width: .5,
		}]

		componentDidMount() {
			this.getData()
		}

		getData = () => {
			if (this.props.variantId) {
				this.setState({
					isLoading: true,
				}, () => {
					VariantService.getVariantInventories(this.props.variantId, this.state.current, this.state.count, this.props.token).then(result => {
						this.setState({
							isLoading: false,
							total: result.count,
							inventories: result.data || [],
						})
					}).catch(err => {
						this.warn(err)

						this.props.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong, please try again later',
						})

						this.setState({
							isLoading: false,
						})
					})
				})
			}
		}

		onClose = () => {
			this.props.utilities.alert.close()
		}

		onUpdateInventory = inventory => {
			this.props.utilities.alert.modal({
				component: (
					<InventoryUpdatePagelet id={inventory ? inventory.id : undefined} variantId={ parseInt(this.props.variantId, 10) } onUpdate={this.onInventoryUpdated.bind(this, inventory)} onDuplicate={ this.onDuplicateInventory } />
				),
			})
		}

		onDuplicateInventory = data => {
			this.props.utilities.alert.modal({
				component: (
					<InventoryUpdatePagelet variantId={ parseInt(this.props.variantId, 10) } { ...data } onUpdate={this.onInventoryUpdated.bind(this, null)} />
				),
			})
		}

		onInventoryUpdated = (inventory, data) => {
			if(inventory) {
				inventory.price = data.price
				inventory.note = data.note
				inventory.status = data.status
				inventory.rack = data.rack

				this.props.refresh()
			} else {
				this.props.refresh()
			}
		}

		onNavigationChange = (current, count) => {
			this.setState({
				current,
				count,
			})
		}

		onChangeStatus = inventory => {
			this.props.utilities.alert.modal({
				component: (
					<InventoryStatusPagelet
						id={ inventory.id }
						status={ inventory.status }
						note={ inventory.note }
						onChange={ this.onStatusChange.bind(this, inventory) }
					/>
				),
			})
		}

		onStatusChange = (inventory, status, note) => {
			inventory.status = status

			if(status !== 'BOOKED') {
				inventory.note = note
			}

			this.props.refresh()
		}

		rowRenderer = inventory => {
			return {
				key: inventory.id,
				data: [{
					title: `#IN-${ inventory.id }`,
				}, {
					title: inventory.price ? `IDR ${FormatHelper.currency(inventory.price)}` : '-',
				}, {
					title: inventory.rack || inventory.note || '-',
					children: (
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ inventory.note || '-' }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								Rack: { inventory.rack || '-' }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					children: this.statusRenderer(inventory),
				}, {
					children: this.iconRenderer(),
				}],
				onPress: this.onUpdateInventory.bind(this, inventory),
			}
		}

		statusRenderer = inventory => {
			return (
				<BadgeStatusLego status={ inventory.status } onPress={ this.onChangeStatus.bind(this, inventory) } style={Styles.badge} />
			)
		}

		iconRenderer = () => {
			return (
				<BoxBit row centering>
					<BoxBit />
					<IconBit name="edit" size={20} color={Colors.black.palette(2, .8)} />
				</BoxBit>
			)
		}

		header = () => {
			return (
				<BoxBit row unflex centering style={Styles.header}>
					<BoxBit />
					<TouchableBit unflex centering onPress={this.onClose} style={Styles.icon}>
						<IconBit name="close" />
					</TouchableBit>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit style={Styles.container}>
					{ this.header() }
					<BoxLego
						title="Inventory Items"
						header={(
							<PaginationComponent
								current={this.state.current}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={ this.onNavigationChange }
							/>
						)}
						contentContainerStyle={Styles.content}
					>
						<TableLego
							isSearching={ this.state.isLoading }
							headers={ this.headers }
							rows={ this.state.inventories.map(this.rowRenderer) }
						/>

						<BoxBit row unflex type={BoxBit.TYPES.THIN} style={Styles.footer}>
							<BoxBit />
							<ButtonBit
								title="Add Item(s)"
								weight="medium"
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								state={!this.props.variantId && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL}
								size={ButtonBit.TYPES.SIZES.SMALL}
								onPress={this.onUpdateInventory.bind(this, undefined)}
							/>
						</BoxBit>
					</BoxLego>
				</BoxBit>
			)
		}
	}
)
