import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		width: '60%',
		justifyContent: 'center',
	},
	padderBottom: {
		paddingBottom: 10,
	},
	content: {
		marginLeft: 0,
		marginRight: 0,
		marginBottom: 0,
	},
	dark90: {
		color: Colors.black.palette(1, .9),
	},

	selection: {
		height: 36,
		width: 160,
	},
	selectionInput: {
		height: 'auto',
		width: 0,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	// footer: {
	// 	justifyContent: 'flex-end',
	// 	alignItems: 'center',
	// 	paddingTop: 12,
	// 	paddingBottom: 12,
	// 	paddingRight: 16,
	// },

	deleteButton: {
		marginLeft: 12,
	},

	total: {
		color: Colors.black.palette(2, .6),
		paddingRight: 16,
	},

	status: {
		padding: 9,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 2,
		borderStyle: 'solid',
		textAlign: 'center',
		backgroundColor: Colors.solid.grey.palette(1),
	},

	capital: {
		textTransform: 'capitalize',
	},
	checkbox: {
		alignSelf: 'center',
		paddingTop: 12,
		paddingBottom: 12,
		paddingRight: 12,
		paddingLeft: 12,
	},

	footer: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	pagination: {
		marginRight: 10,
	},

	row: {
		// fontSize: 14,
	},

	header: {
		backgroundColor: Colors.transparent,
		height: 48,
	},

	icon: {
		backgroundColor: Colors.white.primary,
		width: 36,
		height: 36,
		borderRadius: 36 / 2,
	},
})
