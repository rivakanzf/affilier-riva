import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities';

import VariantService from 'app/services/variant'

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';
import LoaderBit from 'modules/bits/loader'
import ButtonBit from 'modules/bits/button'

import MeasurementPagelet from 'modules/pagelets/measurement';

import Styles from './style';

import { isEmpty } from 'lodash'

export default ConnectHelper(
	class MeasurementPart extends PromiseStatefulModel {
		
		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				categoryId: PropTypes.id,
			}
		}
	
		static propsToPromise(state, oP) {
			return VariantService.getVariantMeasurements(oP.id, state.me.token).catch(err => {
				this.warn(err)
			})
		}

		static getDerivedStateFromProps(nP, nS) {
			if(!isEmpty(nP.data) && !nS.isLoaded) {
				const measurements = {}
				nP.data.map(m => {
					measurements[m.id] = m.measurementVariants[0].value
				})

				return {
					...nS,
					measurements,
					isLoaded: true,
				}
			} else {
				return {
					...nS,
					measurements: !isEmpty(nS.measurements) ? nS.measurements : {},
				}
			}
		}

		
		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isSaving: false,
				isLoaded: false,
			})
		}

		onChange = (value) => {
			this.setState({
				isChanged: true,
				measurements: value,
			})
		}

		onClose = () => {
			this.props.utilities.alert.close()
		}

		onSave = () => {
			this.setState({
				isSaving: true,
			})

			VariantService.updateVariant(this.props.id, {measurements: this.state.measurements}, this.props.token).finally(() => {
				this.setState({
					isSaving: false,
				})
			})
			
		}

		header = () => {
			return (
				<BoxBit row unflex centering style={Styles.header}>
					<BoxBit />
					<TouchableBit unflex centering onPress={this.onClose} style={Styles.icon}>
						<IconBit name="close" />
					</TouchableBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<BoxBit style={Styles.container} centering>
					<LoaderBit simple/>
				</BoxBit>
			)
		}

		view() {
			return (
				<BoxBit style={Styles.container}>
					{ this.header() }
					<MeasurementPagelet
						variantId={ this.props.id }
						key={`${this.props.categoryId}`}
						categoryId={this.props.categoryId}
						measurements={this.state.measurements}
						onChange={this.onChange}
						style={Styles.mt0}
					/>
					<ButtonBit
						title={ 'SAVE' }
						onPress={ this.onSave }
						state={ this.state.isSaving ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
					/>
				</BoxBit>
			)
		}
	}
)
