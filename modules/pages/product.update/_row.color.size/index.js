import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import ButtonBit from 'modules/bits/button'
import BoxBit from 'modules/bits/box'
import TextInputBit from 'modules/bits/text.input'

import SelectColorLego from 'modules/legos/select.color';
import RowLego from 'modules/legos/row';
import SelectSizeLego from 'modules/legos/select.size';

import GetterCategoryComponent from 'modules/components/getter.category'

import Styles from './style';

import { isEmpty } from 'lodash'


export default ConnectHelper(
	class ImagePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				colorIds: PropTypes.id,
				categoryId: PropTypes.id,
				sizeId: PropTypes.id,
				onChangeColor: PropTypes.func,
				onChangeSize: PropTypes.func,
				onAddNewVariant: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {
				colors: {
					primary: {},
					secondary: {},
				},
				size: {},
				url: null,
				sku: null,
			})
		}

		onChange = (key, e) => {
			this.setState({
				[key]: e.target.value,
			})
		}

		onChangeColor = (type, id, title) => {
			this.setState({
				colors: {
					...this.state.colors,
					[type]: {
						id,
						title,
					},
				},
			}, () => {
				// this.props.onChangeColor(type, id, title)
			})
		}

		onChangeSize = (id, title) => {
			this.setState({
				size: {
					id,
					title,
				},
			}, () => {
				// this.props.onChangeSize(id, title)
			})
		}

		sizeRenderer = ({categoryId}) => {
			return (
				<SelectSizeLego isRequired
					placeholder={ 'Select size' }
					sizeId={this.props.sizeId}
					categoryId={categoryId}
					onChange={this.onChangeSize}
				/>
			)
		}

		onAddNewVariant = () => {
			if(isEmpty(this.state.colors.primary) || isEmpty(this.state.size)) {
				this.props.onAddNewVariant &&
				this.props.onAddNewVariant(false, this.state)
			} else {
				this.props.onAddNewVariant &&
				this.props.onAddNewVariant(true, this.state)

			}
		}

		view() {
			return (
				<BoxBit>
					<RowLego
						// key={ this.props.categoryId }
						data={[{
							title: 'SKU',
							children: (
								<TextInputBit
									onChange={this.onChange.bind(this, 'sku')}
								/>
							),
						}, {
							title: 'URL',
							children: (
								<TextInputBit
									onChange={this.onChange.bind(this, 'url')}

								/>
							),
						}]}
						style={Styles.container}
					/>
					<RowLego
						key={ this.props.categoryId }
						data={[{
							title: 'Color',
							children: (
								<SelectColorLego
									headerless
									colorIds={ this.props.colorIds }
									onChange={ this.onChangeColor.bind(this, 'primary') }
								/>
							),
						}, {
							title: 'Size',
							children: (
								<GetterCategoryComponent
									categoryId={ this.props.categoryId }
									children={ this.sizeRenderer }
								/>
							),
						}]}
						style={Styles.container}
					/>
					{
						!isEmpty(this.state.colors.primary) &&
						<RowLego
							data={[{
								title: 'Secondary Color',
								children: (
									<SelectColorLego
										headerless
										colorIds={ this.props.colorIds }
										onChange={ this.onChangeColor.bind(this, 'secondary') }
									/>
								),
							}, {
								title: 'Actions',
								children: (
									<ButtonBit
										title={'ADD'}
										onPress={ this.onAddNewVariant }
									/>
								),
							}]}
							style={Styles.container}
						/>
					}
					
				</BoxBit>
			)
		}
	}
)
