import StyleSheet from 'coeur/libs/style.sheet'

export default StyleSheet.create({
	container: {
		flex: 1,
		padding: '0 16px',
	},
})
