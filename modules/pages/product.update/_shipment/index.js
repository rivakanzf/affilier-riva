import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

// import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities'

import BoxLego from 'modules/legos/box';
import RowPacketLego from 'modules/legos/row.packet';

import Styles from './style';


export default ConnectHelper(
	class ShipmentPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				packetId: PropTypes.number,
				onChangePacket: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		onChangeTitle = (e, val) => {
			this.props.onChangeTitle &&
			this.props.onChangeTitle(val)
		}

		view() {
			return (
				<BoxLego
					title="Shipment"
					style={Styles.container}
				>
					<RowPacketLego key={ `pack_${this.props.packetId}` } packetId={ this.props.packetId } onChange={this.props.onChangePacket} />
				</BoxLego>
			)
		}
	}
)
