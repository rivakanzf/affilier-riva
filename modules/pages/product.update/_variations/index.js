/* eslint-disable no-nested-ternary */
import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import ProductService from 'app/services/product';
import VariantService from 'app/services/variant';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import SwitchBit from 'modules/bits/switch';
import ButtonBit from 'modules/bits/button';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import TouchableBit from 'modules/bits/touchable';
import InputCurrencyBit from 'modules/bits/input.currency';

import BoxLego from 'modules/legos/box';
import TableLego from 'modules/legos/table';

import ImagePart from '../_image'
import MeasurementPart from '../_measurement'
import RowColorSizePart from '../_row.color.size'
import InventoriesPart from '../_inventories'

import { isEmpty, orderBy, without, isEqual } from 'lodash'

import Styles from './style';


export default ConnectHelper(
	class VariationsPart extends ConnectedStatefulModel {
		static propTypes(PropTypes) {
			return {
				productId: PropTypes.id,
				onChangeRetailPrice: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
				userId: state.me.id,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				updater: 0,
				isLoading: true,
				isChanged: false,
				isSaving: false,
				newVariantId: 1,
				selectedIds: [],
				variants: [], // initial data
				newVariants: [], // store new variant here

				changes: [], // store what changes in existing variant(s)

				images: {}, // store new image per variant including new variant that will be updated shallowly by Save button
				// { int/string(variantId): [] }

				modified: {}, // to store what change do you want to variants

				message: null,
			})
		}

		data = {
			isAvailable: true,
		}

		header = [{
			title: '',
			width: .2,
		}, {
			title: 'Color',
			width: .5,
		}, {
			title: 'Size',
			width: .5,
		}, {
			title: 'ID',
			width: .5,
		}, {
			title: 'Retail',
			width: 1.5,
		}, {
			title: 'Discounted',
			width: 1.5,
		}, {
			title: 'Photos',
			width: 2,
		}, {
			title: 'Overseas',
			width: 1,
		}, {
			title: 'Limited Stock',
			width: 1,
		}, {
			title: 'Published',
			width: 1,
		}, {
			title: 'Avalability',
			width: 1,
		}, {
			title: 'Consign',
			width: 1,
		}, {
			title: 'Stock',
			width: .5,
		}, {
			title: '',
			width: 0.5,
		}]

		componentDidMount() {
			if(this.props.productId) {
				this.getData()
			}
		}

		getData = () => {
			ProductService.getProductVariants(this.props.productId, this.props.token).then(variants => {
				this.data.isAvailable = variants.is_available

				this.setState({
					isLoading: false,
					variants: variants.data,
				})
			}).catch(err => {
				this.warn(err)

				this.setState({
					isLoading: false,
				})
			})
		}

		getImageButtonStates = () => {
			let disabled = true
			
			if(!!this.state.selectedIds.length) disabled = false

			return disabled ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL
		}

		setImages = images => {
			const selected = {}

			this.state.selectedIds.forEach(id => {
				selected[id] = images
			})

			this.setState({
				isChanged: true,
				images: { ...this.state.images, ...selected },
			})
		}

		onCreateNewVariants = () => {
			if(this.state.newVariants.length) {
				return VariantService.createVariant(this.state.newVariants.map(variant => {
					return {
						sku: variant.sku,
						url: variant.url,
						product_id: parseInt(this.props.productId, 10),
						color_ids: variant.color_ids,
						size_id: variant.size.id,
						limited_stock: variant.limited_stock,
						price: parseInt(variant.price, 10),
						retail_price: parseInt(variant.retail_price, 10),
						is_available: variant.is_available,
						is_published: variant.is_published,
						have_consignment_stock: variant.have_consignment_stock,
						overseas: variant.overseas,
					}
				}), this.props.token)
			} else {
				return Promise.resolve()
			}
		}

		onUpdateVariants = () => {
			return new Promise(resolve => {
				const result = []

				this.state.changes.forEach(variant => {
					VariantService.updateVariant(parseInt(variant.id, 10), variant, this.props.token)
						.then(res => {
							result.push(res)
						})
				})

				resolve(result)
			})
		}

		onUpdateVariantImage = (images, variantIds) => {
			return new Promise(resolve => {
				images.forEach((img, i) => {
					// if(!res) {
					// 	VariantService.updateVariantImage(
					// 		variantIds[i],
					// 		img,
					// 		{ replace: true, batch: true },
					// 		this.props.token,
					// 	).catch(err => {
					// 		this.warn(err)
					// 		res = `Failed to update variant image in one of variant id (${variantIds[i].slice()})`
					// 	})
					// 	return false
					// } else {
					// 	return true // stop the loop
					// }
					VariantService.updateVariantImage(
						variantIds[i],
						img,
						{ replace: true, batch: true },
						this.props.token,
					).then(response => {
						console.log('success update variantImage: ', response, img, variantIds[i])
						if(i === images.length - 1) resolve(null)
						return false
					}).catch(err => {
						this.warn(err)
						resolve(`Failed to update variant image in one of variant id (${variantIds[i].slice()})`)
						return true // stop the loop
					})
				})
			})
		}

		onNavigateToVariantDetail(id) {
			if(typeof(id) === 'string') {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'You need to save this variant first',
				})
			} else {
				this.props.page.navigator.navigate(`product/${this.props.productId}/variant/${id}`)
			}
		}

		onSaveUpdate = () => {
			this.setState({
				isSaving: true,
				message: null,
			}, () => {
				// kalau mau update foto, tapi gak ada yg dipilih
				// if(Object.keys(this.state.images).length && !this.state.selectedIds.length) {
				// 	return this.setState({
				// 		isSaving: false,
				// 		message: 'Centang variant yang ingin di update gambarnya',
				// 	})
				// }
				return Promise.all([this.onCreateNewVariants(), this.onUpdateVariants()]).then(([created]) => {
					const newVariants = []

					if(!!created) {
						created.forEach((newVariant, i) => {
							this.state.images[newVariant.id] = this.state.images[this.state.newVariants[i].id]
							newVariants.push(newVariants)
						})
					}

					// this below and images will be [[], []]
					const variantIdsGroupedToImageUpdate = []
					const images = Object.keys(this.state.images).reduce((acc, key) => {
						const parsedKey = parseInt(key, 10)
						if(!isNaN(parsedKey)) {
							if( isEmpty(acc) || !isEqual(acc[acc.length - 1], this.state.images[key]) ) {
								// acc[i] = this.state.images[key]
								// the property name doesn't matter
								acc.push(this.state.images[key])
								
								if(!!variantIdsGroupedToImageUpdate.length) {
									variantIdsGroupedToImageUpdate[acc.length - 1] = [parsedKey]
								} else {
									variantIdsGroupedToImageUpdate[0] = [parsedKey]
								}
							} else {
								variantIdsGroupedToImageUpdate[acc.length - 1].push(parsedKey)
							}
						}
						return acc
					}, [])

					this.onUpdateVariantImage(images, variantIdsGroupedToImageUpdate).catch((err) => {
						if(err !== 'ERR_101') {
							this.props.utilities.notification.show({
								message: 'Ada gambar yang tidak tersave',
							})
						}
					})

					this.setState({
						isLoading: true,
						isSaving: false,
						isChanged: false,
						message: null,
						newVariants: [],
						changes: [],
					}, () => this.getData())
				}).catch(err => {
					this.warn(err)
					
					this.setState({
						isSaving: false,
					}, () => {
						this.props.utilities.notification.show({
							title: 'Oops…',
							message: 'Cek kembali data yang diinputkan',
						})
					})
				})
			})
		}

		
		updateData = (id, type, val) => {
			if(typeof(id) === 'number') {
				// existing variant
				const index = this.state.variants.findIndex(variant => id === variant.id)
				this.state.variants[index] = {
					...this.state.variants[index],
					[type]: val,
				}

				const changes = this.state.changes.findIndex(variant => id === variant.id)
				if(changes === -1) {
					this.state.changes = [...this.state.changes, {id, [type]: val}]
				} else {
					this.state.changes[changes] = {
						...this.state.changes[changes],
						[type]: val,
					}
				}

				if(!this.state.isChanged) {
					this.setState({
						isChanged: true,
					})
				}
			} else {
				const index = this.state.newVariants.findIndex(variant => id === variant.id)
				this.state.newVariants[index] = {
					...this.state.newVariants[index],
					[type]: val,
				}
			}

		}

		onChange = (ids, key, value) => {
			this.setState({
				modified: {
					...this.state.modified,
					[key]: value,
				},
			})

			if(Array.isArray(ids)) {
				ids.map(id => this.updateData(id, key, value))
			} else {
				this.updateData(ids, key, value)

			}
		}

		onChangeShallow = (ids, key, value) => {
			this.setState({
				modified: {
					...this.state.modified,
					[key]: value,
				},
			})
		}

		onChangePriceShallow = (ids, key, val, isValid) => {
			if(isValid) {
				this.state.modified = {
					...this.state.modified,
					[key]: val,
				}
			}
		}

		onChangePrice = (ids, key, val, isValid) => {
			if (isValid) {
				this.state.modified = {
					...this.state.modified,
					[key]: val,
				}

				if(Array.isArray(ids)) {
					ids.map(id => this.updateData(id, key, val))
				} else {
					this.updateData(ids, key, val)
				}
			}

		}

		onSelect = id => {
			if (this.state.selectedIds.indexOf(id) > -1) {
				this.setState({
					selectedIds: without(this.state.selectedIds, id),
				})
			} else {
				this.setState({
					selectedIds: [...this.state.selectedIds, id],
				})
			}
		}

		onSelectAll = id => {
			if (this.state.selectedIds.length === id.length) {
				this.setState({
					selectedIds: [],
				})
			} else {
				this.setState({
					selectedIds: id,
				})
			}
		}

		onShowImage = () => {
			this.props.utilities.alert.modal({
				component: (
					<ImagePart
						userId={ this.props.userId }
						token={ this.props.token }
						setImages={ this.setImages }
						onRefresh={ this.getData }
					/>
				),
			})
		}

		onShowMeasurement = (variant) => {
			if(typeof(variant.id) === 'string') {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'You need to save this variant first',
				})
			} else {
				this.props.utilities.alert.modal({
					component: (
						<MeasurementPart id={variant.id} categoryId={variant.product.category_id} />
					),
				})
			}
		}

		onShowInventory = (variant) => {
			if(typeof(variant.id) === 'string') {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'You need to save this variant first',
				})
			} else {
				this.props.utilities.alert.modal({
					component: (
						<InventoriesPart variantId={variant.id} refresh={ this.getData }/>
					),
				})

			}
		}

		onUpdateShallow = () => {
			const changeKeys = Object.keys(this.state.modified)

			this.state.selectedIds.forEach(id => {
				changeKeys.forEach(key => {
					this.updateData(id, key, this.state.modified[key])
				})
			})
			this.setState({
				updater: this.state.updater + 1,
				selectedIds: [],
				modified: {},
			})

		}
		
		onAddNewVariant = (isValid, variant) => {
			if(!isValid) {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Size cannot be empty',
				})
			} else {
				const newVariant = {
					id: `new-${this.state.newVariantId}`,
					sku: variant.sku ? variant.sku : '',
					url: variant.url ? variant.url : '',
					color: `${variant.colors.primary.title}${!isEmpty(variant.colors.secondary) ? '/' + variant.colors.secondary.title : ''}`,
					color_ids: isEmpty(variant.colors.secondary) ? [variant.colors.primary.id] : [variant.colors.primary.id, variant.colors.secondary.id],
					size: {
						id: variant.size.id,
						title: variant.size.title,
					},
					assets: [],
					quantity_detail: {
						available: 0,
						total: 0,
					},
					price: '',
					retail_price: '',
					overseas: false,
					is_available: true,
					is_published: true,
					have_consignment_stock: false,
				}
	
				this.setState({
					isChanged: true,
					newVariants: [newVariant, ...this.state.newVariants],
					newVariantId: this.state.newVariantId + 1,
				}, () => {
					console.log('onaddnewvariants: ', this.state.newVariants)
				})
			}
		}

		createPartRenderer = () => {
			return (
				<RowColorSizePart
					onAddNewVariant={ this.onAddNewVariant }
					categoryId={ this.props.categoryId }
				/>
			)
		}

		imagesRenderer = (image, i) => {
			return (
				<ImageBit key={i} overlay style={Styles.image}
					source={ image.image ? image.image : image }
					broken={ !image }
					transform={{crop: 'fit'}}
				/>
			)
		}

		rowRenderer = (variant) => {
			return {
				id: variant.id,
				data: [{
					children: (
						<CheckboxBit dumb
							isActive={ this.state.selectedIds.indexOf(variant.id) > -1 }
							onPress={ this.onSelect.bind(this, variant.id) }
						/>
					),
				}, {
					title: variant.color || '-',
					style: Styles.capital,
				}, {
					title: variant.size.title || '-',
				}, {
					title: typeof(variant.id) === 'number' ? variant.id : 'new' || '-',
				}, {
					children: (
						<InputCurrencyBit
							title=""
							value={variant.retail_price}
							onChange={ this.onChangePrice.bind(this, variant.id, 'retail_price') }
							style={Styles.center}
						/>
					),
				}, {
					children: (
						<InputCurrencyBit
							title=""
							value={variant.price}
							onChange={ this.onChangePrice.bind(this, variant.id, 'price') }
							style={Styles.center}
						/>
					),
					
				}, {
					children: (
						<TouchableBit row style={Styles.wrap} onPress={ this.onNavigateToVariantDetail.bind(this, variant.id)}>
							{ this.state.images[variant.id]
								? this.state.images[variant.id].map(this.imagesRenderer)
								: variant.assets.map(this.imagesRenderer) }
						</TouchableBit>
					),
				}, {
					children: (
						<SwitchBit
							onChange={ this.onChange.bind(this, variant.id, 'overseas') }
							value={ variant.overseas }
							style={ Styles.switch }
						/>
					),
				}, {
					children: (
						<SwitchBit
							onChange={ this.onChange.bind(this, variant.id, 'limited_stock') }
							value={ variant.limited_stock }
							style={ Styles.switch }
						/>
					),
				}, {
					children: (
						<SwitchBit
							onChange={ this.onChange.bind(this, variant.id, 'is_published') }
							value={ variant.is_published }
							style={ Styles.switch }
						/>
					),
				}, {
					children: (
						<SwitchBit
							onChange={ this.onChange.bind(this, variant.id, 'is_available') }
							value={ variant.is_available }
							style={ Styles.switch }
						/>
					),
				}, {
					children: (
						<SwitchBit
							onChange={ this.onChange.bind(this, variant.id, 'have_consignment_stock') }
							value={ variant.have_consignment_stock }
							style={ Styles.switch }
						/>
					),
				}, {
					children: (
						<TouchableBit centering onPress={this.onShowInventory.bind(this, variant)}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.underline}>
								{variant.quantity_detail.available + '/' + variant.quantity_detail.total || '-'}
							</GeomanistBit>
						</TouchableBit>
					),
				}, {
					children: (
						<BoxBit row centering>
							<TouchableBit unflex onPress={this.onShowMeasurement.bind(this, variant)}>
								<IconBit name="edit" />
							</TouchableBit>
							<TouchableBit unflex onPress={ this.onNavigateToVariantDetail.bind(this, variant.id) } style={ Styles.ml4 }>
								<IconBit name="arrow-right"/>
							</TouchableBit>
						</BoxBit>
					),
				}],
			}
		}

		toggleTableRenderer = variantIds => {
			return (
				<TableLego
					headers={[{
						title: '',
						width: .2,
					}, {
						title: '',
						width: .5,
					}, {
						title: '',
						width: .5,
					}, {
						title: '',
						width: .5,
					}, {
						title: '',
						width: 1.5,
					}, {
						title: '',
						width: 1.5,
					}, {
						title: '',
						width: 2,
					}, {
						title: '',
						width: 1,
					}, {
						title: '',
						width: 1,
					}, {
						title: '',
						width: 1,
					}, {
						title: '',
						width: 1,
					}, {
						title: '',
						width: 1,
					}, {
						title: '',
						width: 1.2,
					}]}
					rows={[{
						id: 123,
						data: [{
							children: (
								<CheckboxBit dumb
									isActive={ !variantIds.length ? false : this.state.selectedIds.length === variantIds.length }
									disabled={ !variantIds.length }
									onPress={ this.onSelectAll.bind(this, variantIds) }
								/>
							),
						}, {
							title: 'Select All',
						}, {}, {}, {
							children: (
								<InputCurrencyBit
									title=""
									value={this.state.modified.retail_price}
									onChange={ this.onChangePriceShallow.bind(this, this.state.selectedIds, 'retail_price') }
									style={Styles.center}
								/>
							),
						}, {
							children: (
								<InputCurrencyBit
									title=""
									value={this.state.modified.price}
									onChange={ this.onChangePriceShallow.bind(this, this.state.selectedIds, 'price') }
									style={Styles.center}
								/>
							),
						}, {
							children: (
								<ButtonBit
									weight="medium"
									title="Add Image(s)"
									size={ ButtonBit.TYPES.SIZES.SMALL }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									onPress={ this.onShowImage }
									state={ this.getImageButtonStates() }
								/>
							),
						}, {
							children: (
								<SwitchBit
									onChange={ this.onChangeShallow.bind(this, this.state.selectedIds, 'overseas') }
									style={ Styles.switch }
								/>
							),
						}, {
							children: (
								<SwitchBit
									onChange={ this.onChangeShallow.bind(this, this.state.selectedIds, 'limited_stock') }
									style={ Styles.switch }
								/>
							),
						}, {
							children: (
								<SwitchBit
									onChange={ this.onChangeShallow.bind(this, this.state.selectedIds, 'is_published') }
									style={ Styles.switch }
								/>
							),
						}, {
							children: (
								<SwitchBit
									style={ Styles.switch }
									onChange={ this.onChangeShallow.bind(this, this.state.selectedIds, 'is_available') }

								/>
							),
						}, {
							children: (
								<SwitchBit
									style={ Styles.switch }
									onChange={ this.onChangeShallow.bind(this, this.state.selectedIds, 'have_consignment_stock') }

								/>
							),
						}, {
							children: (
								<ButtonBit
									title="Apply"
									weight="medium"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									size={ButtonBit.TYPES.SIZES.SMALL}
									state={ !!this.state.selectedIds.length
										? ButtonBit.TYPES.STATES.NORMAL
										: ButtonBit.TYPES.STATES.DISABLED
									}
									onPress={ this.onUpdateShallow }
								/>
							),
						}],
					}]}
				/>
			)
		}

		mainTableRenderer = () => {
			return (
				<TableLego
					key={ `${this.state.updater}-${this.state.newVariants}` }
					isLoading={ this.state.isLoading }
					headers={ this.header }
					rows={ orderBy([...this.state.newVariants, ...this.state.variants], ['id']).map(this.rowRenderer) }
					contentStyle={Styles.list}
				/>
			)
		}
		
		footerRenderer = () => {
			return (
				<BoxBit unflex row style={Styles.footer}>
					<BoxBit>
						{ !this.state.message ? false : (
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} weight="medium" style={Styles.tomato}>
								{ this.state.message }
							</GeomanistBit>
						) }
					</BoxBit>
					<ButtonBit
						title="Save"
						weight="medium"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						state={ this.state.isSaving
							? ButtonBit.TYPES.STATES.LOADING
							: !this.state.isChanged
								? ButtonBit.TYPES.STATES.DISABLED
								: ButtonBit.TYPES.STATES.NORMAL
						}
						size={ ButtonBit.TYPES.SIZES.SMALL }
						onPress={ this.onSaveUpdate }
					/>
				</BoxBit>
			)
		}

		view() {
			const variantIds = [ ...this.state.variants.map(v => v.id), ...this.state.newVariants.map(v => v.id) ]

			return (
				<BoxLego title="Variants" style={Styles.container} contentContainerStyle={Styles.padder}>
					{ this.createPartRenderer() }
					{ this.toggleTableRenderer(variantIds) }
					{ this.mainTableRenderer() }
					{ this.footerRenderer() }
				</BoxLego>

			)
		}
	}
)
