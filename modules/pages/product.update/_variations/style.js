import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		marginTop: 16,
	},

	padder: {
		margin: 0,
	},

	list: {
		maxHeight: 315 * 1.5,
		minHeight: 105 * 1.25,
	},

	wrap: {
		flexWrap: 'wrap',
	},

	capital: {
		textTransform: 'capitalize',
	},

	center: {
		alignItems: 'center',
	},

	footer: {
		paddingTop: 9,
		paddingBottom: 10,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
		alignItems: 'center',
		justifyContent: 'space-between',
	},

	tomato: {
		color: Colors.red.palette(3),
	},

	image: {
		width: 60,
		height: 60 * 4 / 3,
		marginRight: 4,
		marginBottom: 4,
	},

	containerImage: {
		marginRight: 4,
		marginBottom: 4,
	},

	underline: {
		textDecoration: 'underline',
	},

	ml4: {
		marginLeft: 4,
	},
})
