/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities';

import ProductService from 'app/services/product';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader'

import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import TagPagelet from 'modules/pagelets/tag';
import ModalPromptPagelet from 'modules/pagelets/modal.prompt';

import ActionPart from './_action';
import DataPart from './_data';
import ShipmentPart from './_shipment';
import VariationsPart from './_variations';

import Styles from './style';

import { isEmpty } from 'lodash';

export default ConnectHelper(
	class ProductUpdatePage extends PageModel {

		static routeName = 'product.update'

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isChanged: false,
				isLoading: false,
				token: undefined,
			})
		}

		data = {
			brandId: null,
			packetId: null,
			tagIds: [],
			packet: {},
			categoryId: null,
			title: null,
			description: null,
			fit: undefined,
			care: undefined,
			remarks: undefined,
			isPublished: true,
			updatedAt: null,
		}

		roles = ['inventory', 'stylist']

		getData = () => {
			if (this.state.token && this.props.id && !this.state.isLoading) {
				this.setState({
					isLoading: true,
				}, () => {
					ProductService.getProductById(this.props.id, this.state.token)
						.then(res => {
							this.data.brandId = res.brand_id
							// this.data.tagIds =
							this.data.packetId = res.packet_id
							this.data.categoryId = res.category_id
							this.data.title = res.title
							this.data.isPublished = res.is_published
							this.data.updatedAt = res.updated_at
							this.data.description = res.description
							this.data.fit = res.fit
							this.data.care = res.care
							this.data.remarks = res.remarks
							this.data.merchants = res.merchants

							this.setState({
								isLoading: false,
							})
						})
						.catch(this.onError)
				})
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		onNavigateToInventory = () => {
			this.navigator.navigate('inventory')
		}

		onNavigateToProductDetail = id => {
			this.navigator.navigate(`product/${ id }`)
		}

		onError = err => {
			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onChange = (key, value) => {
			this.data[key] = value

			if (!this.state.isChanged) {
				this.state.isChanged = true
				
				this.forceUpdate()
			} else if (key === 'categoryId' || key === 'isPublished') {
				this.forceUpdate()
			}
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onConfirmSave = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why you updated / changed this product (required)."
						onCancel={ this.onClose }
						onConfirm={ this.onSave }
					/>
				),
			})
		}

		onSave = (note) => {
			if (
				this.data.brandId !== null &&
				this.data.categoryId !== null &&
				this.data.tagIds.length > 0 &&
				this.data.title
			) {
				if (this.props.id) {
					this.setState({
						isLoading: true,
					}, () => {
						ProductService.updateProduct({
							id: parseInt(this.props.id, 10),
							brand_id: this.data.brandId,
							category_id: this.data.categoryId,
							tag_ids: this.data.tagIds,
							title: this.data.title,
							is_published: this.data.isPublished,
							description: this.data.description,
							fit: this.data.fit,
							care: this.data.care,
							remarks: this.data.remarks,
							note,
							...(isEmpty(this.data.merchants) ? {merchants: []} : { merchants: this.data.merchants }),
							...(isEmpty(this.data.packet) ? {} : { packet: this.data.packet }),

						}, this.state.token)
							.then(() => {
								this.utilities.notification.show({
									message: 'Yeay... Product saved',
									type: this.utilities.notification.TYPES.SUCCESS,
								})

								this.data.updatedAt = Date.now()

								this.setState({
									isLoading: false,
									isChanged: false,
								}, () => this.getData())
							})
							.catch(this.onError)
					})
				} else {
					this.setState({
						isLoading: true,
					}, () => {
						ProductService.createProduct({
							brand_id: this.data.brandId,
							category_id: this.data.categoryId,
							tag_ids: this.data.tagIds,
							title: this.data.title,
							is_published: this.data.isPublished,
							description: this.data.description,
							fit: this.data.fit,
							care: this.data.care,
							remarks: this.data.remarks,
							...(isEmpty(this.data.merchants) ? {merchants: []} : { merchants: this.data.merchants }),
							...(isEmpty(this.data.packet) ? {} : { packet: this.data.packet }),
						}, this.state.token)
							.then(res => {
								if (res.id) {
									this.setState({
										isLoading: false,
									}, this.onNavigateToProductDetail.bind(this, res.id));
								}
							})
							.catch(this.onError)
					})
				}
			} else {
				this.utilities.notification.show({
					title: 'Oops…',
					message: 'Either brand or category or title or tags is not defined.',
				})
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={this.onAuthorized}
					paths={[{
						title: 'Inventory',
						onPress: this.onNavigateToInventory,
					}, {
						title: this.props.id ? `Product ${this.props.id}` : 'New Product',
					}]}
				>
					{() => (
						<BoxBit style={Styles.container}>
							<HeaderPageComponent
								title={ `${this.props.id ? 'Edit' : 'Add New'} Product` }
							/>
							<BoxBit unflex row>
								<BoxBit style={ Styles.padder }>
									{ this.state.isLoading ? <LoaderBit simple/> : (
										<DataPart
											id={ this.props.id }
											title={ this.data.title }
											brandId={ this.data.brandId }
											categoryId={ this.data.categoryId }
											description={ this.data.description }
											fit={ this.data.fit }
											care={ this.data.care }
											remarks={ this.data.remarks }
											merchants={ this.data.merchants }
											onChangeBrand={ this.onChange.bind(this, 'brandId') }
											onChangeCategory={ this.onChange.bind(this, 'categoryId') }
											onChangeTitle={ this.onChange.bind(this, 'title') }
											onChangeDescription={ this.onChange.bind(this, 'description') }
											onChangeFit={ this.onChange.bind(this, 'fit') }
											onChangeCare={ this.onChange.bind(this, 'care') }
											onChangeRemarks={ this.onChange.bind(this, 'remarks') }
											onChangeMerchant={ this.onChange.bind(this, 'merchants') }
										/>
									)}
									
								</BoxBit>
								<BoxBit>
									<ActionPart
										isLoading={this.state.isLoading}
										isPublished={this.data.isPublished}
										canSave={this.state.isChanged}
										updatedAt={this.data.updatedAt}
										onToggleIsPublished={this.onChange.bind(this, 'isPublished')}
										// onSave={this.onSave}
										onSave={ this.onConfirmSave }
									/>
									<ShipmentPart
										packetId={ this.data.packetId }
										onChangePacket={ this.onChange.bind(this, 'packet') }
									/>
									{ this.data.categoryId ? (
										<TagPagelet
											key={`tag_${this.data.categoryId}`}
											categoryId={this.data.categoryId}
											productId={this.props.id}
											onChange={this.onChange.bind(this, 'tagIds')}
										/>
									) : false}
								</BoxBit>
							</BoxBit>
							<VariationsPart productId={this.props.id} categoryId={ this.data.categoryId }/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
