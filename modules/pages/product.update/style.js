import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingLeft: 32,
		paddingRight: 32,
		paddingBottom: 16,
		paddingTop: 16,
	},

	padder: {
		paddingRight: 16,
	},
})
