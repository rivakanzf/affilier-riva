import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
// import TouchableBit from 'modules/bits/touchable';
import SwitchBit from 'modules/bits/switch'

import BoxRowLego from 'modules/legos/box.row';

import Styles from './style';


export default ConnectHelper(
	class ActionPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				canSave: PropTypes.bool,
				isLoading: PropTypes.bool,
				isPublished: PropTypes.bool,
				onToggleIsPublished: PropTypes.func,
				onDelete: PropTypes.func,
				onSave: PropTypes.func,
				updatedAt: PropTypes.date,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Availability',
							content: this.props.isAvailable ? 'This variant is available (default)' : 'This variant is no longer available',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: (
								<SwitchBit onChange={ this.props.onToggleIsAvailable } value={ this.props.isAvailable } style={ Styles.switch } />
							),
						}],
					}, {
						data: [{
							title: 'Publishing',
							content: this.props.isPublished ? 'This variant is published' : 'This variant is not published',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: (
								<SwitchBit onChange={ this.props.onToggleIsPublished } value={ this.props.isPublished } style={ Styles.switch } />
							),
						}],
					}, {
						data: [{
							title: 'Overseas Purchase',
							content: this.props.overseas ? 'This variant is imported from overseas' : 'This variant available locally',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: (
								<SwitchBit onChange={ this.props.onToggleOverseas } value={ this.props.overseas } style={ Styles.switch } />
							),
						}],
					}, {
						data: [{
							title: 'Save Changes',
							children: (
								<BoxBit unflex>
									<ButtonBit
										title="DELETE VARIATION"
										theme={ ButtonBit.TYPES.THEMES.WHITE_PRIMARY }
										width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
										weight="medium"
										state={ this.props.isLoading ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
										onPress={ this.props.onDelete }
									/>
									<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.time }>
										Last updated: {
											this.props.updatedAt
												? TimeHelper.format(this.props.updatedAt, 'DD MMMM YYYY HH:mm')
												: '-'
										}
									</GeomanistBit>
								</BoxBit>
							),
						}, {
							blank: true,
							children: (
								<ButtonBit
									title="SAVE VARIATION"
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
									size={ ButtonBit.TYPES.SIZES.MEDIUM }
									weight="medium"
									state={ this.props.isLoading ? ButtonBit.TYPES.STATES.DISABLED : this.props.canSave && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
									onPress={ this.props.onSave }
								/>
							),
						}],
					}]}
					style={ Styles.padder }
				/>
			)
		}
	}
)
