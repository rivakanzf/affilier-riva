import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	space: {
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingTop: 16,
	},

	padder: {
		marginBottom: 16,
	},

	darkGrey60: {
		color: Colors.black.palette(2, .6),
	},

	last: {
		marginTop: 8,
		color: Colors.black.palette(2, .6),
	},

	spacer: {
		width: 16,
	},

	switch: {
		alignSelf: 'flex-end',
	},

	content: {
		opacity: .6,
	},

	time: {
		paddingTop: 8,
		color: Colors.black.palette(2, .6),
	},

})
