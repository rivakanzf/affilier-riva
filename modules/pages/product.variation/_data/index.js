import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import TextInputBit from 'modules/bits/text.input';
import InputCurrencyBit from 'modules/bits/input.currency';
import InputValidatedBit from 'modules/bits/input.validated';

import BoxLego from 'modules/legos/box';
import RowLego from 'modules/legos/row';
import RowImageVariantLego from 'modules/legos/row.image.variant';
import RowColorLego from 'modules/legos/row.color';
import RowPacketLego from 'modules/legos/row.packet';
import SelectSizeLego from 'modules/legos/select.size';

import GetterCategoryComponent from 'modules/components/getter.category'

import Styles from './style';


export default ConnectHelper(
	class DataPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				productId: PropTypes.id,
				packetId: PropTypes.id,
				sku: PropTypes.string,
				url: PropTypes.string,
				colorIds: PropTypes.arrayOf(PropTypes.id),
				sizeId: PropTypes.id,
				categoryId: PropTypes.id,
				price: PropTypes.number,
				retailPrice: PropTypes.number,
				note: PropTypes.string,
				remarks: PropTypes.string,
				onChangeUrl: PropTypes.func,
				onChangeColor: PropTypes.func,
				onChangeSize: PropTypes.func,
				onChangePrice: PropTypes.func,
				onChangePacket: PropTypes.func,
				onDeletePacket: PropTypes.func,
				onChangeNote: PropTypes.func,
				onChangeRemarks: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		onChangeUrl = (e, val) => {
			this.props.onChangeUrl &&
			this.props.onChangeUrl(val)
		}

		onChangePrice = (val, isValid) => {
			if(isValid) {
				this.props.onChangePrice &&
				this.props.onChangePrice(val)
			}
		}

		onChangeRetailPrice = (val, isValid) => {
			if (isValid) {
				this.props.onChangeRetailPrice &&
				this.props.onChangeRetailPrice(val)
			}
		}

		onChangeNote = val => {
			this.props.onChangeNote &&
			this.props.onChangeNote(val)
		}

		onChangeRemarks = val => {
			this.props.onChangeRemarks &&
			this.props.onChangeRemarks(val)
		}

		sizeRenderer = ({categoryId}) => {
			return (
				<SelectSizeLego isRequired
					placeholder={ 'Select size' }
					sizeId={this.props.sizeId}
					categoryId={categoryId}
					onChange={this.props.onChangeSize}
				/>
			)
		}

		view() {
			return (
				<BoxLego key={ this.props.note } title="Variation Data" contentContainerStyle={Styles.padder}>
					{ this.props.id ? (
						<RowImageVariantLego variantId={ this.props.id } />
					) : false }
					<RowLego
						data={[{
							title: 'SKU',
							children: (
								<TextInputBit disabled
									placeholder="Input here"
									defaultValue={ this.props.sku }
								/>
							),
							headerStyle: this.props.id ? Styles.top0 : undefined,
						}, {
							title: 'URL',
							children: (
								<TextInputBit
									placeholder="Input here"
									defaultValue={ this.props.url }
									onChange={ this.onChangeUrl }
								/>
							),
							headerStyle: this.props.id ? Styles.top0 : undefined,
						}]}
					/>
					<RowColorLego
						colorIds={ this.props.colorIds }
						onChange={ this.props.onChangeColor }
					/>
					<RowLego
						key={ this.props.categoryId }
						data={[{
							title: 'Size*',
							children: (
								<GetterCategoryComponent
									categoryId={ this.props.categoryId }
									children={ this.sizeRenderer }
								/>
							),
						}, {
							blank: true,
						}]}
						style={Styles.row}
					/>
					<RowLego
						data={[{
							children: (
								<InputValidatedBit showCounter
									title="Note"
									type={ InputValidatedBit.TYPES.TEXTAREA }
									maxlength={ 500 }
									value={ this.props.note }
									onChange={ this.onChangeNote }
								/>
							),
						}]}
					/>

					<RowLego
						data={[{
							children: (
								<InputValidatedBit showCounter
									title="Remarks"
									placeholder="Input here…"
									type={ InputValidatedBit.TYPES.TEXTAREA }
									maxlength={ 500 }
									value={ this.props.remarks }
									onChange={ this.onChangeRemarks }
								/>
							),
						}]}
					/>

					<RowPacketLego deletable={ !!this.props.packetId }
						packetId={ this.props.packetId }
						variantId={ this.props.id }
						productId={ this.props.productId }
						onChange={ this.props.onChangePacket }
						onDelete={ this.props.onDeletePacket }
					/>

					<RowLego
						data={[{
							title: 'Retail Price*',
							children: (
								<InputCurrencyBit
									value={ this.props.retailPrice }
									onChange={ this.onChangeRetailPrice }
								/>
							),
							description: ' ',
						}, {
							title: 'Discounted Price',
							children: (
								<InputCurrencyBit
									value={ this.props.price }
									onChange={ this.onChangePrice }
								/>
							),
							description: 'Input same value as retail price for no discount',
						}]}
					/>
				</BoxLego>
			)
		}
	}
)
