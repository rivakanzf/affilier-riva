import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	padder: {
		marginBottom: 16,
		paddingBottom: 0,
	},

	row: {
		height: 84,
	},

	wrap: {
		flexWrap: 'wrap',
	},

	length: {
		position: 'absolute',
		right: 0,
		bottom: 0,
		color: Colors.black.palette(2, .6),
		marginRight: 8,
		marginBottom: 8,
		backgroundColor: Colors.white.primary,
	},

	// required: {
	// 	color: Colors.red.palette(3),
	// },

	containerTextarea: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 16,
		paddingRight: 0,
	},

	input: {
		padding: 0,
		paddingRight: 16,
	},

	top0: {
		marginTop: 0,
	},
})
