import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
// import Colors from 'coeur/constants/color';
// import UtilitiesContext from 'coeur/contexts/utilities';

// import CommonHelper from 'coeur/helpers/common'

import VariantService from 'app/services/variant';
import ProductService from 'app/services/product';
import PacketService from 'app/services/packet';

import BoxBit from 'modules/bits/box';

import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import MeasurementPagelet from 'modules/pagelets/measurement';
import TagPagelet from 'modules/pagelets/tag';

import ActionsPart from './_action';
import DataPart from './_data';
import InventoriesPart from './_inventories';

import Styles from './style';

import { intersection, isEmpty } from 'lodash';


export default ConnectHelper(
	class ProductVariationPage extends PageModel {

		static routeName = 'variation.add'

		static stateToProps(state) {
			return {
				token: state.me.token,
				isStylist: !!intersection(state.me.roles, ['stylist']).length,
				isAuthorized: !!intersection(state.me.roles, ['inventory']).length,
			}
		}

		constructor(p) {
			super(p, {
				isChanged: false,
				isLoading: false,
				token: undefined,
			})
		}

		data = {
			sku: undefined,
			url: undefined,
			colorIds: [],
			tagIds: [],
			sizeId: null,
			packetId: null,
			categoryId: null,
			retail_price: undefined,
			price: undefined,
			note: undefined,
			isPublished: true,
			isAvailable: true,
			overseas: false,
			remarks: null,
			measurements: {},
			packet: {},
			updatedAt: undefined,
		}

		roles = ['inventory', 'stylist']

		onAuthorized = token => {
			this.setState({
				token,
			}, () => {
				this.getData()
			})
		}

		getData = () => {
			if(this.props.id) {
				this.setState({
					isLoading: true,
				}, () => {
					VariantService.getVariantById(this.props.id, {
					}, this.props.token)
						.then(res => {
							this.data.sku = res.sku
							this.data.url = res.url
							this.data.colorIds = [].concat(res.color_id || []).concat(...res.color_ids.filter(id => id !== res.color_id))
							this.data.sizeId = res.size_id
							this.data.categoryId = res.category_id
							this.data.retail_price = res.retail_price
							this.data.price = res.price
							this.data.note = res.note
							this.data.isPublished = res.is_published
							this.data.isAvailable = res.is_available
							this.data.overseas = res.overseas
							this.data.remarks = res.remarks
							this.data.packetId = res.packet_id
							this.packet = res.packet
							this.data.measurements = res.measurements.map(measurement => {
								return {
									[measurement.id]: measurement.value,
								}
							}).reduce((sum, curr) => {
								return {
									...sum,
									...curr,
								}
							}, {})
							this.data.updatedAt = res.updated_at

							this.setState({
								isLoading: false,
							})
						})
						.catch(this.onError)
				})
			} else if(this.props.productId) {
				this.setState({
					isLoading: true,
				}, () => {
					ProductService.getProductById(this.props.productId, this.props.token)
						.then(res => {
							this.data.categoryId = res.category_id

							this.setState({
								isLoading: false,
							})
						})
						.catch(this.onError)
				})
			}
		}

		onNavigateToInventory = () => {
			this.navigator.navigate('inventory')
		}

		onNavigateToProduct = () => {
			this.navigator.navigate(`product/${ this.props.productId }`)
		}

		onNavigateToVariant = id => {
			this.navigator.navigate(`product/${ this.props.productId }/variant/${ id }`)
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onChange = (key, value) => {
			this.data[key] = value

			if (key === 'isPublished' || key === 'isAvailable' || key === 'overseas') {
				this.forceUpdate()
			}

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onDeletePacket = () => {
			if(this.data.packetId) {
				this.setState({
					isLoading: true,
				}, () => {
					PacketService.deletePacket(this.data.packetId, this.props.token)
						.then(() => {
							this.data.packetId = null
							this.data.packet = {}

							this.utilities.notification.show({
								message: 'Packet deleted',
								type: this.utilities.notification.TYPES.SUCCESS,
							})

							this.setState({
								isLoading: false,
							})
						})
						.catch(this.onError)
				})
			}
		}

		onDeleteVariant = () => {
			this.setState({
				isLoading: true,
			}, () => {
				VariantService.deleteVariantFromProduct(this.props.id, this.props.token)
					.then(res => {
						if(res) {
							this.setState({
								isLoading: false,
							}, () => this.navigator.back())
						}
					})
					.catch( this.onError )
			})
		}

		onSaveVariant = () => {
			this.setState({
				isLoading: true,
			}, () => {
				if (this.props.id) {
					VariantService.updateVariant(typeof this.props.id === 'string' ? parseInt(this.props.id, 10) : this.props.id, {
						url: this.data.url,
						color_ids: this.data.colorIds,
						tag_ids: this.data.tagIds,
						size_id: this.data.sizeId,
						retail_price: this.data.retail_price,
						price: this.data.price,
						note: this.data.note,
						remarks: this.data.remarks,
						is_published: this.data.isPublished,
						is_available: this.data.isAvailable,
						overseas: this.data.overseas,
						measurements: this.data.measurements,
						...(isEmpty(this.data.packet) ? {} : { packet: this.data.packet }),
					}, this.props.token).then(() => {
						this.utilities.notification.show({
							message: 'Variant saved',
							type: this.utilities.notification.TYPES.SUCCESS,
						})

						this.data.updatedAt = Date.now()

						this.setState({
							isLoading: false,
							isChanged: false,
						}, () => this.getData())
					}).catch(this.onError)
				} else {
					if (
						this.data.colorIds.length &&
						this.data.sizeId &&
						this.data.retail_price
					) {
						VariantService.createVariant({
							product_id: typeof this.props.productId === 'string' ? parseInt(this.props.productId, 10) : this.props.productId,
							url: this.data.url,
							color_ids: this.data.colorIds,
							tag_ids: this.data.tagIds,
							size_id: this.data.sizeId,
							retail_price: this.data.retail_price,
							price: this.data.price,
							note: this.data.note,
							remarks: this.data.remarks,
							is_published: this.data.isPublished,
							is_available: this.data.isAvailable,
							overseas: this.data.overseas,
							measurements: this.data.measurements,
							...(isEmpty(this.data.packet) ? {} : { packet: this.data.packet }),
						}, this.props.token).then(res => {
							this.utilities.notification.show({
								message: 'Variant saved',
								type: this.utilities.notification.TYPES.SUCCESS,
							})

							this.data.updatedAt = Date.now()

							this.setState({
								isLoading: false,
								isChanged: false,
							}, this.onNavigateToVariant.bind(this, res.id))
						}).catch(this.onError)
					} else {
						this.setState({
							isLoading: false,
						})

						this.utilities.notification.show({
							title: 'Oops…',
							message: 'Either color, size or price is not defined.',
						})
					}
				}
			})
		}

		onRefresh = () => {
			this.forceUpdate()
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={this.onAuthorized}
					paths={[{
						title: 'Inventory',
						onPress: this.onNavigateToInventory,
					}, {
						title: `Product ${this.props.productId}`,
						onPress: this.onNavigateToProduct,
					}, {
						title: this.props.id ? `Variant ${this.props.id}` : 'New Variant',
					}]}
				>
					{() => (
						<BoxBit style={Styles.container}>
							<HeaderPageComponent
								title={`${this.props.id ? 'Edit' : 'Add New'} Variation`}
							/>
							<BoxBit unflex row>
								<BoxBit style={Styles.padder}>
									<DataPart
										key={ this.data.packetId }
										id={ this.props.id }
										productId={ this.props.productId }
										packetId={ this.data.packetId }
										sku={ this.data.sku }
										url={ this.data.url }
										colorIds={ this.data.colorIds }
										sizeId={ this.data.sizeId }
										categoryId={ this.data.categoryId }
										retailPrice={ this.data.retail_price }
										price={ this.data.price }
										note={ this.data.note }
										remarks={ this.data.remarks }
										onChangeUrl={ this.onChange.bind(this, 'url') }
										onChangeColor={ this.onChange.bind(this, 'colorIds') }
										onChangeSize={ this.onChange.bind(this, 'sizeId') }
										onChangeRetailPrice={ this.onChange.bind(this, 'retail_price') }
										onChangePrice={ this.onChange.bind(this, 'price') }
										onChangeNote={ this.onChange.bind(this, 'note') }
										onChangeRemarks={ this.onChange.bind(this, 'remarks') }
										onChangePacket={ this.onChange.bind(this, 'packet') }
										onDeletePacket={ this.onDeletePacket }
										onChangeOrder={ this.onChangeOrder }
									/>
									{ !this.props.isStylist && (
										<InventoriesPart variantId={this.props.id} />
									)}
								</BoxBit>
								<BoxBit>
									<ActionsPart
										canSave={ this.state.isChanged }
										isLoading={ this.state.isLoading }
										isPublished={ this.data.isPublished }
										isAvailable={ this.data.isAvailable }
										overseas={ this.data.overseas }
										updatedAt={ this.data.updatedAt }
										onToggleIsPublished={ this.onChange.bind(this, 'isPublished') }
										onToggleIsAvailable={ this.onChange.bind(this, 'isAvailable') }
										onToggleOverseas={ this.onChange.bind(this, 'overseas') }
										onDelete={ this.onDeleteVariant }
										onSave={ this.onSaveVariant }
									/>

									{ this.data.categoryId ? (
										<TagPagelet
											key={ `tag_${this.data.categoryId}` }
											categoryId={ this.data.categoryId }
											productId={ this.props.productId }
											variantId={ this.props.id }
											onChange={ this.onChange.bind(this, 'tagIds') }
										/>
									) : false }

									<MeasurementPagelet key={`${this.state.isLoading}${this.data.categoryId}`} categoryId={this.data.categoryId} measurements={this.data.measurements} onChange={this.onChange.bind(this, 'measurements')} />

								</BoxBit>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
