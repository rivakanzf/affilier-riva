import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingLeft: 32,
		paddingRight: 32,
		paddingTop: 16,
		paddingBottom: 16,
	},

	centering: {
		alignItems: 'center',
	},

	darkGrey: {
		color: Colors.black.palette(2),
	},

	padder: {
		paddingRight: 16,
	},

	title: {
		color: Colors.black.palette(2),
		paddingTop: 24,
		paddingBottom: 8,
	},

	'@media (min-width: 1920px)': {
		page: {
			width: 1920,
			alignSelf: 'center',
			// alignItems: 'center',
		},
	},

	'@media (max-width: 1446px)': {
		wrapper: {
			flexDirection: 'column',
			height: '100%',
		},
	},
})
