import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import AddVariantComponent from 'modules/pagelets/add.variant'

import ShipmentService from 'app/services/shipment'

import MeManager from 'app/managers/me';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';

import InitialLego from 'modules/legos/initial';

import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import Styles from './style';

import { capitalize } from 'lodash'

export default ConnectHelper(
	class ProfilePage extends PageModel {

		static routeName = 'profile'

		static stateToProps(state) {
			return {
				name: `${state.me.fullName}`,
				email: state.me.email,
				roles: state.me.roles,
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				// initial state
			}, [
				// 'onLogout',
			]);
		}

		onLogout = () => {
			this.navigator.navigate('login')
			MeManager.logout()
		}

		view() {
			ShipmentService.getShipment(this.props.token)
			return (
				<PageAuthorizedComponent roles="admin" paths={[{ title: 'Profile' }]}>
					{() => (
						<BoxBit style={Styles.page}>
							<HeaderPageComponent
								title="Profile"
								// paths={[{ title: 'PROFILE' }]}
							/>
							<BoxBit unflex style={Styles.container}>
								<BoxBit row>
									<InitialLego
										name={ this.props.name }
										style={Styles.initial}
									/>
									<BoxBit>
										<GeomanistBit
											type={GeomanistBit.TYPES.HEADER_4}
											style={Styles.name}
										>
											{ this.props.name }
										</GeomanistBit>
										<GeomanistBit
											type={GeomanistBit.TYPES.PARAGRAPH_3}
											weight="medium"
											style={Styles.role}
										>
											{ this.props.roles.map(role => {
												return capitalize(role)
											}).join(', ') }
										</GeomanistBit>
										<GeomanistBit
											type={GeomanistBit.TYPES.PARAGRAPH_3}
										>
											{ this.props.email }
										</GeomanistBit>
									</BoxBit>
								</BoxBit>
								<ButtonBit
									title="LOGOUT"
									// theme={ButtonBit.TYPES.THEMES.BLACK}
									width={ButtonBit.TYPES.WIDTHS.FIT}
									size={ButtonBit.TYPES.SIZES.SMALL}
									style={Styles.logout}
									onPress={this.onLogout}
								/>
							</BoxBit>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
