import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	page: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: 32,
		paddingRight: 32,
	},
	container: {
		backgroundColor: Colors.white.primary,
		padding: Sizes.margin.default,
		borderRadius: 2,
		width: '50%',
	},
	initial: {
		alignSelf: 'center',
		marginRight: Sizes.margin.default,
		height: 70,
		width: 70,
		borderRadius: 50,
	},
	header: {
		marginTop: 0,
	},
	name: {
		color: Colors.black.palette(2),
	},
	role: {
		color: Colors.black.palette(2, .6),
	},
	email: {
		color: Colors.black.palette(2),
	},
	logout: {
		width: 102,
		marginTop: 16,
		marginLeft: 86,
	},
})
