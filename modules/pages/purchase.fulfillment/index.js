import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import PurchaseService from 'app/services/purchase';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LinkBit from 'modules/bits/link';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import QuickViewPurchasePagelet from 'modules/pagelets/quick.view.purchase';


import Styles from './style';


export default ConnectHelper(
	class PurchaseFulfillmentPage extends PageModel {

		static routeName = 'purchase.fulfillment'

		constructor(p) {
			super(p, {
				isLoading: false,
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					status: 'PENDING',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		tableHeader = [{
			title: 'Seller',
			width: 1,
		}, {
			title: 'Product',
			width: 3.1,
		}, {
			title: 'URL',
			width: 4,
		}, {
			title: 'Req. Date',
			width: 1,
		}, {
			title: 'Status',
			width: 1,
		}, {
			title: 'Action',
			width: 1.8,
		}, {
			title: '',
			width: .5,
		}]

		paths = [{
			title: 'PURCHASE',
		}, {
			title: 'REQUEST',
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.filter.status !== this.state.filter.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.filterRequest({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(filter.status !== '' ? { status: filter.status } : {}),
					published: true,
				}, this.state.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								data: res.data,
							})
						}
					})
					.catch(this.onError)
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigateToClient = id => {
			this.navigator.navigate(`client/${id}`)
			this.props.utilities.alert.hide()
		}

		onNavigateToStylesheet = id => {
			this.navigator.navigate(`stylesheet/${id}`)
			this.props.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onApprove = purchase => {
			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Approve Request"
						message="Add a note regarding this approval (optional)."
						onConfirm={ this.onConfirmApproval.bind(this, purchase) }
						onCancel={ this.onModalRequestClose }
					/>
				),
			})
		}

		onReject = purchase => {
			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Reject Request*"
						message="Tell us why you reject this purchase request (required)."
						onConfirm={ this.onConfirmRejection.bind(this, purchase) }
						onCancel={ this.onModalRequestClose }
					/>
				),
			})
		}

		onConfirmApproval = (purchase, note) => {
			purchase.isLoading = 'approving'

			this.forceUpdate()

			PurchaseService.approveRequest(purchase.id, note, this.state.token).then(isUpdated => {
				if(isUpdated) {
					purchase.isDone = true
					purchase.isLoading = false
					purchase.status = 'SUCCESS'

					this.utilities.notification.show({
						title: 'Yeay!',
						message: 'Success approving request.',
						type: 'SUCCESS',
					})

					this.forceUpdate()
				} else {
					throw new Error('Approval failed.')
				}
			}).catch(err => {
				purchase.isLoading = false
				throw err
			}).catch(this.onError)

		}

		onConfirmRejection = (purchase, note) => {
			purchase.isLoading = 'rejecting'

			this.forceUpdate()

			PurchaseService.rejectRequest(purchase.id, note, this.state.token).then(isUpdated => {
				if (isUpdated) {
					purchase.isDone = true
					purchase.isLoading = false
					purchase.status = 'FAILED'

					this.utilities.notification.show({
						title: 'Yeay!',
						message: 'Success rejecting request.',
						type: 'SUCCESS',
					})

					this.forceUpdate()
				} else {
					throw new Error('Rejection failed.')
				}
			}).catch(err => {
				purchase.isLoading = false
				throw err
			}).catch(this.onError)
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong.',
				})
			})
		}

		onQuickView = purchase => {
			this.utilities.alert.modal({
				component: (
					<QuickViewPurchasePagelet
						id={ purchase.id }
						onClose={ this.onModalRequestClose }
						onNavigateToClient={ this.onNavigateToClient }
						onNavigateToStylesheet={ this.onNavigateToStylesheet }
						disabled={ purchase.status !== 'PENDING' }
						onApprove={ () => {
							this.onModalRequestClose()
							this.onApprove(purchase)
						} }
						onReject={ () => {
							this.onModalRequestClose()
							this.onReject(purchase)
						} }
					/>
				),
			})
		}

		rowRenderer = purchase => {
			return {
				data: [{
					title: purchase.seller,
				}, {
					children: (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } weight="medium" style={ Styles.title }>
								{ purchase.title }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.note }>
								{ purchase.description }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: '-',
					children: purchase.url ? (
						<LinkBit target={ LinkBit.TYPES.BLANK } underline href={ purchase.url }>
							{ purchase.url }
						</LinkBit>
					) : undefined,
				}, {
					children: (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.title }>
								{ TimeHelper.format(purchase.created_at, 'DD/MM/YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								— { TimeHelper.format(purchase.created_at, 'HH:mm') }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					children: (
						<BadgeStatusLego status={ purchase.status } />
					),
				}, {
					children: (
						<BoxBit accessible={ !purchase.isLoading && !purchase.isDone } row unflex style={Styles.action}>
							<ButtonBit
								title="Reject"
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								theme={ ButtonBit.TYPES.THEMES.SECONDARY }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								size={ ButtonBit.TYPES.SIZES.TINY }
								// eslint-disable-next-line no-nested-ternary
								state={ purchase.isLoading === 'rejecting' ? ButtonBit.TYPES.STATES.LOADING : purchase.isDone || purchase.status !== 'PENDING' ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onReject.bind(this, purchase) }
								style={ Styles.approve }
							/>
							<ButtonBit
								title="Approve"
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								// eslint-disable-next-line no-nested-ternary
								state={ purchase.isLoading === 'approving' ? ButtonBit.TYPES.STATES.LOADING : purchase.isDone || purchase.status !== 'PENDING' ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onApprove.bind(this, purchase) }
								size={ ButtonBit.TYPES.SIZES.TINY }
							/>
						</BoxBit>
					),
				}, {
					children: (
						<TouchableBit style={Styles.icon} centering onPress={ this.onQuickView.bind(this, purchase) }>
							<IconBit
								name={ 'quick-view' }
								color={ Colors.primary }
							/>
						</TouchableBit>
					),
				}],
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={'fulfillment'}
					onAuthorized={this.onAuthorized} >
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Purchase Request"
								paths={ this.paths }
								searchPlaceholder={ 'Product, brand, or category' }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								<SelectStatusLego type={SelectStatusLego.TYPES.PURCHASE_REQUESTS} status={ this.state.filter.status } onChange={this.onChangeFilter.bind(this, 'status')} />
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
