import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';

import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'utils/constants/color'

import PurchaseService from 'app/services/purchase';
import VariantService from 'app/services/variant';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import InputValidatedBit from 'modules/bits/input.validated';
import TextBit from 'modules/bits/text';

import LoaderLego from 'modules/legos/loader';
import NotificationLego from 'modules/legos/notification';
import RowsLego from 'modules/legos/rows';
// import RowPacketLego from 'modules/legos/row.packet';
import SelectBrandAddressLego from 'modules/legos/select.brand.address';
import SelectionVariantLego from 'modules/legos/selection.variant';
import TableLego from 'modules/legos/table';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';

import Styles from './style';


export default ConnectHelper(
	class ApprovePart extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				orderId: PropTypes.number,
				variantId: PropTypes.number,
				purchaseRequests: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					isMain: PropTypes.bool,
					quantity: PropTypes.number,
				})),
				onClose: PropTypes.func,
				onCancel: PropTypes.func,
				onApproved: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, p) {
			return Promise.all([
				PurchaseService.getOrder(p.orderId, {
					detail: true,
				}, state.me.token),
				VariantService.getVariantById(p.variantId, {
					item_detailed: true,
				}, state.me.token),
			]).then(([purchase, variant]) => {
				return {
					purchase,
					variant,
				}
			})
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				brand_address_id: undefined,
				note: undefined,
				rack: undefined,
			})
		}

		headers = [{
			title: 'ID',
			width: .4,
		}, {
			title: 'Title',
			width: 3,
		}, {
			title: 'Category',
			width: 1,
		}, {
			title: 'Color',
			width: 1,
		}, {
			title: 'Size',
			width: 1,
		}, {
			title: 'Qty',
			width: .5,
		}]

		onClose = () => {
			this.props.onClose &&
			this.props.onClose()

			this.props.utilities.alert.hide()
		}

		onChange = (key, val) => {
			this.state[key] = val
		}

		onConfirmApprove = () => {
			this.setState({
				isLoading: true,
			}, () => {
				this.props.purchaseRequests.reduce((p, pr) => {
					return p.then(() => {
						return PurchaseService.resolveRequest(pr.id, {
							variant_id: this.props.variantId,
							quantity: pr.quantity,
							main: pr.isMain,
							brand_address_id: this.state.brand_address_id || undefined,
							rack: this.state.rack,
							note: this.state.note,
						}, this.props.token)
					})
				}, Promise.resolve()).then(() => {
					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: 'Success resolving.',
						type: 'SUCCESS',
					})

					this.onClose()

					this.props.onApproved &&
					this.props.onApproved()
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					})

					this.props.utilities.notification.show({
						title: 'Oops…',
						message: err ? (err.detail && err.detail.message || err.detail) || err.message || err : 'Something went wrong.',
					})
				})
			})
		}

		rowRenderer = request => {
			const pr = this.props.data.purchase.requests.find(r => r.id === request.id)
			return {
				data: [{
					title: `#${ request.id }`,
				}, {
					children: (
						<BoxBit unflex>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
								<TextBit style={ Styles.seller }>[{ pr.seller }]</TextBit> { pr.brand } – { pr.title }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: pr.category,
				}, {
					title: pr.color,
				}, {
					title: pr.size,
				}, {
					title: `${request.quantity} / ${pr.quantity}`,
				}],
			}
		}

		viewOnError() {
			return (
				<BoxBit row centering style={Styles.empty}>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.error}>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title={ 'Approve Purchasing' }
					// header={( <BadgeStatusLego status={this.props.data.status} /> )}
					loading={ this.state.isLoading }
					confirm="Approve"
					cancel="Back"
					onClose={ this.onClose }
					onCancel={ this.props.onCancel }
					onConfirm={ this.onConfirmApprove }
					contentContainerStyle={ Styles.container }
				>
					<NotificationLego message="You are about to convert following purchase request(s) into inventories. Be warned that ALL of them will be converted into a single variant." />
					<SelectionVariantLego
						image={ this.props.data.variant.image }
						brand={ this.props.data.variant.brand }
						title={ this.props.data.variant.title }
						category={ this.props.data.variant.category }
						size={ this.props.data.variant.size }
						color={ this.props.data.variant.color }
						price={ this.props.data.variant.price }
						retailPrice={ this.props.data.variant.retail_price }
					/>
					<TableLego compact
						headers={ this.headers }
						rows={ this.props.purchaseRequests.map( this.rowRenderer ) }
						style={ Styles.table }
					/>
					<RowsLego
						data={[{
							data: [{
								title: 'Rack',
								children: (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.INPUT }
										placeholder="Input here…"
										onChange={ this.onChange.bind(this, 'rack') }
									/>
								),
							}, {
								title: 'Pickup Point',
								children: (
									<SelectBrandAddressLego onChange={ this.onChange.bind(this, 'brand_address_id') } />
								),
							}],
						}, {
							data: [{
								title: 'Note',
								children: (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.TEXTAREA }
										placeholder="Input here…"
										onChange={ this.onChange.bind(this, 'note') }
									/>
								),
							}],
						}]}
						style={ Styles.content }
					/>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
