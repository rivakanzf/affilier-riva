import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	content: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	empty: {
		paddingTop: Sizes.margin.default,
		minHeight: 300,
	},

	error: {
		color: Colors.primary,
		marginLeft: 8,
	},

	container: {
		paddingBottom: 0,
		paddingLeft: 0,
		paddingRight: 0,
	},

	table: {
		marginTop: Sizes.margin.default,
		marginBottom: - StyleSheet.hairlineWidth,
	},

	seller: {
		color: Colors.primary,
	},

})
