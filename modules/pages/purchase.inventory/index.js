import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import PurchaseService from 'app/services/purchase';

import Colors from 'coeur/constants/color';
import Linking from 'coeur/libs/linking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LinkBit from 'modules/bits/link';
import TextBit from 'modules/bits/text';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import ColorLego from 'modules/legos/color';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
// import PurchaseOrderPicker from 'modules/pagelets/purchase.order.picker';
import QuickViewPurchaseOrderPagelet from 'modules/pagelets/quick.view.purchase.order';

import ApprovePart from './_approve';
import VariantPickerPart from './_variant.picker';

import Styles from './style';

import { capitalize, isEmpty, without } from 'lodash'


export default ConnectHelper(
	class PurchasePurchasingPage extends PageModel {

		static routeName = 'purchase.purchasing'

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				activeId: -1,
				selectedIds: {},
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					// ordered: 'false',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		headers = [{
			title: '',
			width: .3,
		}, {
			title: 'Title',
			width: 2.5,
		}, {
			title: 'Note',
			width: 2,
		}, {
			title: 'Qty',
			width: 1,
		}, {
			title: 'Updated at',
			width: 1,
		}, {
			title: 'Actions',
			width: 1.5,
		}, {
			title: '',
			width: .5,
		}]

		paths = [{
			title: 'PURCHASE',
		}, {
			title: 'PURCHASING REQUEST',
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					// || pS.filter.ordered !== this.state.filter.ordered
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				// filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.filterOrder({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					status: 'PURCHASED',
					with_user: true,
					with_request: true,
				}, this.state.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								data: res.data,
							})
						}
					})
					.catch(this.onError)
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigateToInventory = () => {
			Linking.open(window.location.href.split('/')[0] + '/inventory')
		}

		onNavigateToStylesheet = id => {
			this.navigator.navigate('stylesheet/' + id)
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onToggleActive = id => {
			if(this.state.activeId !== id) {
				this.setState({
					activeId: id,
					selectedIds: {},
				})
			}
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong.',
				})
			})
		}

		onQuickView = id => {
			this.utilities.alert.modal({
				component: (
					<QuickViewPurchaseOrderPagelet
						id={ id }
						onClose={ this.onModalRequestClose }
						// onNavigateToPurchaseOrder={ this.onNavigateToClient }
					/>
				),
			})
		}

		onApproval = order => {
			order.isLoading = 'approving'

			this.forceUpdate()

			const id = Object.keys(this.state.selectedIds)[0]

			this.utilities.alert.modal({
				component: (
					<VariantPickerPart
						onClose={ () => {
							order.isLoading = false
							this.forceUpdate()
						} }
						search={ order.requests.find(r => {
							return r.id === parseInt(id, 10)
						}).title }
						onSelect={ this.onConfirmApproval.bind(this, order) }
						onNavigateToInventory={ this.onNavigateToInventory }
					/>
				),
			})
		}

		onConfirmApproval = (order, variantId) => {
			this.utilities.alert.modal({
				component: (
					<ApprovePart
						orderId={ order.id }
						variantId={ variantId }
						purchaseRequests={ Object.keys(this.state.selectedIds).map(id => {
							const request = order.requests.find(r => {
								return r.id === parseInt(id, 10)
							})

							return {
								id: parseInt(id, 10),
								isMain: request.stylesheet && this.state.selectedIds[id].indexOf(0) > -1,
								quantity: this.state.selectedIds[id].length,
							}
						}) }
						onClose={ () => {
							order.isLoading = false
							this.forceUpdate()
						} }
						onCancel={ this.onApproval.bind(this, order) }
						onApproved={ this.onUpdated }
					/>
				),
			})
		}

		onUpdated = () => {
			this.setState({
				selectedIds: {},
			}, this.getData)
			this.getData()
		}

		onRejection = order => {
			order.isLoading = 'rejecting'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Why?"
						message="Tell us why you rejected this item? (required)"
						onCancel={ () => {
							order.isLoading = false
							this.forceUpdate()

							this.onModalRequestClose()
						} }
						onConfirm={ this.onConfirmRejection.bind(this, order) }
					/>
				),
			})

			// TODO
			// PurchaseService.rejectRequest(purchase.id, note, this.state.token).then(isUpdated => {
			// 	if (isUpdated) {
			// 		purchase.isDone = true
			// 		purchase.isLoading = false
			// 		purchase.status = 'FAILED'

			// 		this.utilities.notification.show({
			// 			title: 'Yeay!',
			// 			message: 'Success rejecting request.',
			// 			type: 'SUCCESS',
			// 		})

			// 		this.forceUpdate()
			// 	} else {
			// 		throw new Error('Rejection failed.')
			// 	}
			// }).catch(err => {
			// 	purchase.isLoading = false
			// 	throw err
			// }).catch(this.onError)
		}

		onConfirmRejection = (order, note) => {
			Promise.all(Object.keys(this.state.selectedIds).map(id => {
				const request = order.requests.find(r => {
					return r.id === parseInt(id, 10)
				})

				return PurchaseService.despiseRequest(id, {
					quantity: this.state.selectedIds[id].length,
					main: request.stylesheet && this.state.selectedIds[id].indexOf(0) > -1,
					note,
				}, this.props.token)
			})).then(() => {
				this.utilities.notification.show({
					title: 'Yeay!',
					message: 'Success rejecting.',
					type: 'SUCCESS',
				})

				this.onUpdated()
			}).catch(err => {
				this.warn(err)

				order.isLoading = false
				this.forceUpdate()

				this.props.utilities.notification.show({
					title: 'Oops…',
					message: err ? (err.detail && err.detail.message || err.detail) || err.message || err : 'Something went wrong.',
				})
			})
		}

		onToggleSelect = (requestId, index) => {
			if(this.state.selectedIds[requestId]) {
				const _index = this.state.selectedIds[requestId].indexOf(index)
				if (_index > -1) {
					const selecteds = without([...this.state.selectedIds[requestId]], index)

					if(selecteds.length) {
						this.state.selectedIds[requestId] = selecteds
					} else {
						delete this.state.selectedIds[requestId]
					}
				} else {
					this.state.selectedIds[requestId] = [...this.state.selectedIds[requestId], index]
				}
			} else {
				this.state.selectedIds[requestId] = [index]
			}

			this.forceUpdate()
		}

		onSelectAll = order => {
			if(this.isAllSelected(order)) {
				order.requests.forEach(request => {
					delete this.state.selectedIds[request.id]
				})
			} else {
				order.requests.forEach(request => {
					this.state.selectedIds[request.id] = Array(request.quantity).fill(undefined).map((u, i) => i)
				})
			}

			this.forceUpdate()
		}

		isAllSelected = order => {
			return order.requests.findIndex(request => {
				return !this.state.selectedIds[request.id] || this.state.selectedIds[request.id].length !== request.quantity
			}) === -1
		}

		isAllPending = order => {
			return order.requests.findIndex(request => {
				return request.status === 'RESOLVED' || request.status === 'EXCEPTION'
			}) === -1
		}

		rowRenderer = order => {
			const isActive = this.state.activeId === order.id

			return {
				data: [{
					children: (
						<BoxBit centering>
							<RadioBit dumb isActive={ isActive } />
						</BoxBit>
					),
				}, {
					title: `${ order.user } / ${ order.title }`,
					// children: (
					// 	<BoxBit style={Styles.product}>
					// 		<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
					// 			{ order.title }
					// 		</GeomanistBit>
					// 		<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
					// 			Issued by: { order.user }
					// 		</GeomanistBit>
					// 	</BoxBit>
					// ),
				}, {
					title: order.note || '-',
				}, {
					title: order.quantity,

				}, {
					title: TimeHelper.format(order.updated_at, 'DD/MM/YY HH:mm'),
					// children: (
					// 	<BoxBit style={Styles.product}>
					// 		<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
					// 			{ TimeHelper.format(order.updated_at, 'DD/MM/YYYY') }
					// 		</GeomanistBit>
					// 		<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
					// 			— { TimeHelper.format(order.updated_at, 'HH:mm') }
					// 		</GeomanistBit>
					// 	</BoxBit>
					// ),
				}, {
					children: (
						<BoxBit row unflex style={ Styles.actions }>
							<ButtonBit
								title="Reject"
								weight="medium"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								theme={ ButtonBit.TYPES.THEMES.SECONDARY }
								// eslint-disable-next-line no-nested-ternary
								state={ order.isLoading === 'rejecting' ? ButtonBit.TYPES.STATES.LOADING : order.isDone || order.isLoading ? ButtonBit.TYPES.STATES.DISABLED : isActive && !isEmpty(this.state.selectedIds) ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								size={ ButtonBit.TYPES.SIZES.TINY }
								onPress={ this.onRejection.bind(this, order) }
								style={ Styles.button }
							/>
							<ButtonBit
								title="Approve"
								weight="medium"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								// eslint-disable-next-line no-nested-ternary
								state={ order.isLoading === 'approving' ? ButtonBit.TYPES.STATES.LOADING : order.isDone || order.isLoading ? ButtonBit.TYPES.STATES.DISABLED : isActive && !isEmpty(this.state.selectedIds) ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								size={ ButtonBit.TYPES.SIZES.TINY }
								onPress={ this.onApproval.bind(this, order) }
							/>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit row style={ Styles.icons }>
							<TouchableBit unflex centering onPress={ this.onQuickView.bind(this, order.id) }>
								<IconBit
									name={ 'quick-view' }
									color={ Colors.primary }
								/>
							</TouchableBit>
						</BoxBit>
					),
				}],
				children: isActive ? (
					<BoxBit unflex style={Styles.requests}>
						{ this.headerRenderer(order) }
						{ order.requests.map(this.requestRenderer) }
					</BoxBit>
				) : false,
				onPress: this.onToggleActive.bind(this, order.id),
				style: !!order.isLoading ? Styles.loading : undefined,
			}
		}

		headerRenderer = order => {
			return (
				<BoxBit unflex row style={[Styles.request, Styles.header]}>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } weight="medium" style={ Styles.wide }>
						Title
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } weight="medium" style={ Styles.row }>
						Category
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } weight="medium" style={ Styles.row }>
						Color
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } weight="medium" style={ Styles.tight }>
						Size
					</GeomanistBit>
					<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } align="center" weight="medium" style={ Styles.tight }>
						Stylesheet Id
					</GeomanistBit>
					<BoxBit style={Styles.tight}>
						{ this.isAllPending(order) ? (
							<CheckboxBit dumb isActive={ this.isAllSelected(order) } onPress={ this.onSelectAll.bind(this, order) } />
						) : false }
					</BoxBit>
				</BoxBit>
			)
		}

		requestRenderer = request => {
			return Array(request.quantity).fill(undefined).map((u, i) => {
				const isInStylesheet = request.stylesheet && i === 0
				const isRejected = request.status === 'EXCEPTION'
				const isApproved = request.status === 'RESOLVED'

				return (
					<TouchableBit key={`${request.id}|${i}`} activeOpacity={ isApproved || isRejected ? 1 : undefined } accessible={ !isApproved && !isRejected } unflex row onPress={this.onToggleSelect.bind(this, request.id, i)} style={[Styles.request, isApproved || isRejected ? Styles.inactive : undefined]}>
						<LinkBit target={LinkBit.TYPES.BLANK} href={ request.url } underline style={Styles.wide}>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1}>
								<TextBit style={Styles.seller}>[{ request.seller }]</TextBit> { request.brand } – { request.title }
							</GeomanistBit>
						</LinkBit>
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.row}>
							{ request.category }
						</GeomanistBit>
						<BoxBit row style={ Styles.row }>
							<ColorLego colors={[request.color]} size={ 16 } radius={ 8 } border={ 0 } style={Styles.color} />
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1}>
								{ capitalize(request.color.title) }
							</GeomanistBit>
						</BoxBit>
						<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.tight}>
							{ request.size }
						</GeomanistBit>
						<GeomanistBit align="center" type={GeomanistBit.TYPES.NOTE_1} style={Styles.stylesheet}>
							{ isInStylesheet ? (
								<LinkBit onPress={ this.onNavigateToStylesheet.bind(this, request.stylesheet_id) } underline>
									#ST-{request.stylesheet_id}
								</LinkBit>
							) : '-' }
						</GeomanistBit>
						<BoxBit style={Styles.tight}>
							{/* eslint-disable-next-line no-nested-ternary */}
							{ isApproved ? (
								<GeomanistBit type={GeomanistBit.TYPES.NOTE_1}>
									[APPROVED]
								</GeomanistBit>
							) : isRejected ? (
								<GeomanistBit type={GeomanistBit.TYPES.NOTE_1}>
									[REJECTED]
								</GeomanistBit>
							) : (
								<CheckboxBit dumb isActive={ this.state.selectedIds[request.id] && this.state.selectedIds[request.id].indexOf(i) > -1 } onPress={ this.onToggleSelect.bind(this, request.id, i) } />
							) }
						</BoxBit>
					</TouchableBit>
				)
			})
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={'inventory'}
					onAuthorized={this.onAuthorized} >
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Purchasing Request"
								paths={ this.paths }
								searchPlaceholder={ 'Product, brand, or category' }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								{/* <SelectionBit options={ this.order } onChange={ this.onChangeFilter.bind(this, 'ordered') } /> */}
								{/* <ButtonBit
									title={ 'Add to Purchase Order' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.state.selectedIds.length ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
									width={ ButtonBit.TYPES.WIDTHS.FIT }
									onPress={ this.onAddToPurchaseOrder }
								/> */}
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.headers }
								rows={ this.state.data.map(this.rowRenderer) }
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
