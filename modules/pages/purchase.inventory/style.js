import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	padder: {
		marginBottom: 16,
	},

	actions: {

	},

	icons: {
		justifyContent: 'flex-end',
	},

	button: {
		marginRight: 4,
	},

	row: {
		flexBasis: '120%',
	},

	wide: {
		flexBasis: '500%',
	},

	tight: {
		flexBasis: '70%',
		alignItems: 'flex-end',
		paddingRight: 4,
	},

	stylesheet: {
		flexBasis: '70%',
		alignItems: 'center',
	},

	loading: {
		transition: '1s all',
		filter: 'grayscale(.6)',
	},

	seller: {
		color: Colors.primary,
	},

	requests: {
		marginTop: 10,
		marginLeft: '4.5%',
	},

	request: {
		paddingLeft: 8,
		paddingRight: 8,
		paddingTop: 4,
		paddingBottom: 4,
		marginTop: 2,
		marginBottom: 2,
		backgroundColor: Colors.grey.palette(2),
		alignItems: 'center',
	},

	inactive: {
		opacity: .6,
		filter: 'grayscale(1)',
	},

	header: {
		backgroundColor: Colors.transparent,
	},

	color: {
		marginRight: 4,
	},

})
