import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import PurchaseService from 'app/services/purchase';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';

import BoxRowLego from 'modules/legos/box.row';
import SelectStatusLego from 'modules/legos/select.status';

import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				changeable: PropTypes.bool,

				disabled: PropTypes.bool,
				changeGetter: PropTypes.func,

				status: PropTypes.string,
				note: PropTypes.string,
				stylistNote: PropTypes.string,
				updatedAt: PropTypes.string,
				onChanged: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me.fullName,
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isChanging: false,
				status: p.status,
			})
		}

		disabledStatuses = () => {
			switch(this.props.status) {
			case 'PENDING':
				return [						'RESOLVED', 			 'EXCEPTION' ]
			case 'PURCHASED':
				return ['PENDING', 				'RESOLVED', 'CANCELLED', 'EXCEPTION']
			case 'RESOLVED':
				return ['PENDING', 'PURCHASED', 			'CANCELLED', 'EXCEPTION']
			case 'CANCELLED':
				return ['PENDING', 'PURCHASED', 'RESOLVED', 			 'EXCEPTION']
			case 'EXCEPTION':
				return ['PENDING', 'PURCHASED', 'RESOLVED', 'CANCELLED'				]
			default:
				return []
			}
		}

		buttonState() {
			if (this.state.isChanging) {
				return ButtonBit.TYPES.STATES.LOADING
			} else if (this.props.disabled && this.props.status === this.state.status) {
				return ButtonBit.TYPES.STATES.DISABLED
			} else if (this.props.status === 'PENDING') {
				return ButtonBit.TYPES.STATES.NORMAL
			} else {
				return ButtonBit.TYPES.STATES.DISABLED
			}
		}

		onModalRequestClose = () => {
			this.setState({
				isChanging: false,
			})

			this.props.utilities.alert.hide()
		}

		onChangeStatus = status => {
			this.setState({
				status,
			})
		}

		onUpdateData = () => {
			this.setState({
				isChanging: true,
			}, () => {
				return PurchaseService
					.updateOrder(this.props.id, {
						...this.props.changeGetter(),
						status: this.state.status,
					}, this.props.token)
					.then(() => {
						this.props.utilities.notification.show({
							title: 'Success',
							message: 'Data updated.',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.setState({
							isChanging: false,
						})
					})
					.catch(this.onError)
			})
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				timeout: 10000,
			})

			this.setState({
				isChanging: false,
			})
		}

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Purchase Order Status',
							children: (
								<BoxBit unflex>
									<SelectStatusLego key={this.state.updater} unflex isRequired
										type={ SelectStatusLego.TYPES.PURCHASES }
										status={ this.props.status }
										disabledStatuses={ this.disabledStatuses() }
										onChange={ this.onChangeStatus }
									/>
									<GeomanistBit
										type={GeomanistBit.TYPES.NOTE_1}
										style={Styles.time}
									>
										Last updated: {
											this.props.updatedAt
												? TimeHelper.format(this.props.updatedAt, 'DD MMMM YYYY HH:mm')
												: '-'
										}
									</GeomanistBit>
								</BoxBit>
							),
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ 'UPDATE PURCHASE ORDER' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.buttonState() }
									onPress={ this.onUpdateData }
								/>
							),
						}],
					}]}
				/>
			)
		}
	}
)
