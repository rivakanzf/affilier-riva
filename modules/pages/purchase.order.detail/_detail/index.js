import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

// import StyleSheetService from 'app/services/style.sheets';

// import BoxBit from 'modules/bits/box';
// import InputCurrencyBit from 'modules/bits/input.currency';
import InputValidatedBit from 'modules/bits/input.validated';
// import LoaderBit from 'modules/bits/loader';
// import NavTextBit from 'modules/bits/nav.text';
// import SelectionBit from 'modules/bits/selection';

// import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';
// import SelectBrandLego from 'modules/legos/select.brand';
// import SelectColorLego from 'modules/legos/select.color';
// import SelectSizeLego from 'modules/legos/select.size';
// import RowCategoryLego from 'modules/legos/row.category';
import RowPurchaseOrderLego from 'modules/legos/row.image.po';

// import GetterCategoryComponent from 'modules/components/getter.category';

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				title: PropTypes.string,
				retailPrice: PropTypes.number,
				price: PropTypes.number,
				quantity: PropTypes.number,
				note: PropTypes.string,
				createdAt: PropTypes.date,
				changeable: PropTypes.bool,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isUpdating: false,
			})
		}

		onChange = (key, value) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, +value ? +value : value)
		}

		view() {
			return (
				<React.Fragment>
					<BoxRowLego
						title={ this.props.title || `Purchase Order #${this.props.id}` }
						data={[{
							data: [{
								title: 'Total Retail Value',
								content: `IDR ${ FormatHelper.currency(this.props.retailPrice) }`,
							}, {
								title: 'Total Value',
								content: `IDR ${ FormatHelper.currency(this.props.price) }`,
							}],
						}, {
							data: [{
								title: 'Quantity',
								content: this.props.quantity || 0,
							}, {
								title: 'Created at',
								content: TimeHelper.format(this.props.createdAt, 'DD MMMM YYYY [—] HH:mm'),
							}],
						}, {
							data: [{
								title: 'Note',
								content: this.props.note || '-',
								children: this.props.changeable ? (
									<InputValidatedBit
										placeholder="Input your note here…"
										type={ InputValidatedBit.TYPES.TEXTAREA }
										value={ this.props.note }
										onChange={ this.onChange.bind(this, 'note') }
									/>
								) : undefined,
								description: 'A note regarding this purchase that inventory team should pay attention to.',
							}],
						}, {
							children: (
								<RowPurchaseOrderLego title="Proof of Transaction" purchaseOrderId={ this.props.id } addable={ this.props.changeable } />
							),
						}, {
							data: [{
								headerless: true,
								description: 'You are required to upload at least one proof of transaction here. (screen shoot will do it)',
							}],
						}]}
						contentContainerStyle={Styles.padder}
					/>
				</React.Fragment>
			)
		}
	}
)
