import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import UtilitiesContext from 'coeur/contexts/utilities'

import PurchaseService from 'app/services/purchase';

import ButtonBit from 'modules/bits/button';

import LoaderLego from 'modules/legos/loader'

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation'

import PurchasePart from './_purchase';

import { isEmpty } from 'lodash';


export default ConnectHelper(
	class EditPart extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				editable: PropTypes.bool,
				orderId: PropTypes.id,
				onUpdate: PropTypes.func,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, oP) {
			return oP.id ? PurchaseService.getDetail(oP.id, {
				basic: true,
			}, state.me.token) : null
		}

		static getDerivedStateFromProps(nP, nS) {
			if (!nS.isLoaded && !isEmpty(nP.data)) {
				return {
					isLoaded: true,
					purchase: nP.data,
				}
			}
			return null
		}

		constructor(p) {
			super(p, {
				isLoaded: false,
				isLoading: false,
				isChanged: false,
				purchase: {},
			})
		}

		onRemove = () => {
			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.removeRequest(this.props.orderId, this.props.id, this.props.token).then(() => {
					this.props.utilities.notification.show({
						title: 'Success',
						message: 'Purchase request removed from this order',
						type: 'SUCCESS',
					})

					this.props.onRemove &&
					this.props.onRemove(this.props.id)

					this.onClose()
				}).catch(this.onError)
			})
		}

		onUpdateRequest = () => {
			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.updateRequest(this.props.id, this.state.purchase, this.props.token).then(() => {
					this.props.utilities.notification.show({
						title: 'Success',
						message: 'Purchase request updated',
						type: 'SUCCESS',
					})

					this.props.onUpdate &&
					this.props.onUpdate(this.state.purchase)

					this.onClose()
				}).catch(this.onError)
			})
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.props.utilities.notification.show({
					title: 'Oops',
					message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				})
			})
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onUpdatePurchaseData = (key, value) => {
			this.state.purchase[key] = value

			this.setState({
				purchase: { ...this.state.purchase },
				isChanged: true,
			})
		}

		headerRenderer() {
			return (
				<ButtonBit
					title="Remove"
					type={ ButtonBit.TYPES.SHAPES.PROGRESS }
					size={ ButtonBit.TYPES.SIZES.SMALL }
					state={ !this.props.editable ? ButtonBit.TYPES.STATES.DISABLED : this.state.isLoading && ButtonBit.TYPES.STATES.DISABLED || ButtonBit.TYPES.STATES.NORMAL }
					onPress={ this.onRemove }
				/>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return (
				<ModalClosableConfirmationPagelet
					title="Edit Purchase Request"
					header={ this.headerRenderer() }
					loading={ this.state.isLoading }
					disabled={ this.props.editable && !this.state.isChanged }
					confirm={ this.props.editable ? 'Submit' : 'Done' }
					onCancel={ this.props.editable ? this.onClose : undefined }
					onConfirm={ this.props.editable ? this.onUpdateRequest : this.onClose }
				>
					<PurchasePart { ...this.state.purchase } editable={ this.props.editable } onUpdate={ this.onUpdatePurchaseData } />
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
