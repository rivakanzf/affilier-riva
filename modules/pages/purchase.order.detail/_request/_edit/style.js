import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	header: {
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .16),
	},

	content: {
		height: 560 - 46 - 104 - StyleSheet.hairlineWidth,
		// paddingLeft: 0,
		// paddingRight: 0,
		// paddingBottom: 0,
	},

	content2: {
		height: 560 - 46,
	},

	container: {
		width: 560,
		height: 650,
	},

	backgroundGrey: {
		backgroundColor: Colors.white.primary,
	},

	centering: {
		alignItems: 'center',
	},

	radio: {
		width: 24,
		height: 24,
		borderRadius: 12,
	},

	padderLeft16: {
		paddingLeft: 16,
	},
	padder48: {
		paddingRight: 48,
	},
	padderLeft: {
		paddingLeft: 8,
	},
	padderTop: {
		paddingTop: 16,
	},

	borderBottom: {
		paddingBottom: 16,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
	},

	dark70: {
		color: Colors.black.palette(1, .7),
	},
	darkGrey: {
		color: Colors.black.palette(2),
	},

	footer: {
		justifyContent: 'space-between',
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 16,
		paddingRight: 16,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.black.palette(2, .2),
	},

	itemsContainer: {
		padding: 16,
		borderWidth: 1,
		borderColor: Colors.black.palette(2, .2),
		borderRadius: 4,
		marginBottom: 16,
		height: 128,
	},
	image: {
		width: 96,
		height: 96,
		marginRight: 16,
	},
	discount: {
		color: Colors.red.palette(4),
	},
	retail: {
		marginRight: 8,
		color: Colors.black.palette(1, .7),
	},

	row: {
		height: 88,
	},
})
