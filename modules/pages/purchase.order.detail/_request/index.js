import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import FormatHelper from 'coeur/helpers/format';
import Colors from 'coeur/constants/color';

import UtilitiesContext from 'coeur/contexts/utilities';

import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

// import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';
import TableLego from 'modules/legos/table';

import EditPart from './_edit'

import Styles from './style';


export default ConnectHelper(
	class RequestPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				editable: PropTypes.bool,
				id: PropTypes.id,
				requests: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					title: PropTypes.string,
					description: PropTypes.string,
					price: PropTypes.number,
					retail_price: PropTypes.number,
					quantity: PropTypes.number,
					status: PropTypes.string,
				})),
				onRemoved: PropTypes.func,
				onChanged: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			requests: [],
		}

		static contexts = [
			UtilitiesContext,
		]

		headers = [{
			title: 'ID',
			width: .8,
		}, {
			title: 'Product',
			width: 4,
		}, {
			title: 'Price',
			width: 1.2,
		}, {
			title: 'Qty',
			width: .5,
		}, {
			title: 'Total',
			width: 1.2,
			align: 'right',
		}, {
			title: '',
			width: .5,
		}]

		total = () => {
			return this.props.requests.map(r => {
				return {
					quantity: r.quantity,
					retail_price: r.retail_price * r.quantity,
					price: r.price * r.quantity,
				}
			}).reduce((sum, curr) => {
				return {
					quantity: sum.quantity + curr.quantity,
					retail_price: sum.retail_price + curr.retail_price,
					price: sum.price + curr.price,
				}
			}, {
				quantity: 0,
				retail_price: 0,
				price: 0,
			})
		}

		onChange = val => {
			this.setState({
				length: val ? val.length : 0,
			})

			this.props.onChange &&
			this.props.onChange(val)
		}

		onEdit = request => {
			this.props.utilities.alert.modal({
				component: (
					<EditPart
						id={ request.id }
						orderId={ this.props.id }
						editable={ this.props.editable }
						onUpdate={ this.onUpdateRequest.bind(this, request) }
						onRemove={ this.onRemoveRequest }
					/>
				),
			})
		}

		onUpdateRequest = (request, update) => {
			Object.keys(update).forEach(key => {
				request[key] = update[key]
			})

			this.props.onChanged &&
			this.props.onChanged()
		}

		onRemoveRequest = id => {
			this.props.onRemoved &&
			this.props.onRemoved(id)
		}

		requestRenderer = request => {
			return {
				data: [{
					title: `#PR-${ request.id }`,
				}, {
					children: (
						<React.Fragment>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
								{ request.title }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ request.description }
							</GeomanistBit>
						</React.Fragment>
					),
				}, {
					children: request.retail_price !== request.price ? (
						<React.Fragment>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.retail }>
								IDR { FormatHelper.currency(request.retail_price) }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.discounted }>
								IDR { FormatHelper.currency(request.price) }
							</GeomanistBit>
						</React.Fragment>
					) : (
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
							IDR { FormatHelper.currency(request.price) }
						</GeomanistBit>
					),
				}, {
					children: (
						<GeomanistBit align="center" type={ GeomanistBit.TYPES.NOTE_1 }>
							{ request.quantity }
						</GeomanistBit>
					),
				}, {
					title: `IDR ${ FormatHelper.currency(request.price * request.quantity) }`,
				}, {
					children: (
						<TouchableBit onPress={ this.onEdit.bind(this, request) } style={ Styles.icon }>
							<IconBit
								name={ this.props.editable ? 'edit' : 'quick-view' }
								color={ Colors.primary }
								size={ 20 }
							/>
						</TouchableBit>
					),
				}],
			}
		}

		view() {
			return (
				<BoxRowLego
					title={'Purchase Requests'}
					style={ this.props.style }
					contentContainerStyle={Styles.padder} >
					<TableLego compact
						headers={ this.headers }
						rows={ this.props.requests.map( this.requestRenderer ) }
						footers={[{
							title: '',
						}, {
							title: '',
						}, {
							title: '',
						}, {
							title: 'Total',
						}, {
							children: (
								<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} weight="semibold" align="right">
									IDR { FormatHelper.currency(this.total().price) }
								</GeomanistBit>
							),
						}, {
							title: '',
						}]}
					/>
				</BoxRowLego>
			)
		}
	}
)
