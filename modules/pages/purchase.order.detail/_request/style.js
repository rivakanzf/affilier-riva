import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	padder: {
		marginBottom: 0,
		marginTop: 0,
		marginLeft: 0,
		marginRight: 0,
	},
	note: {
		color: Colors.black.palette(2, .6),
	},
	retail: {
		textDecoration: 'line-through',
	},
	discounted: {
		color: Colors.red.palette(7),
	},
	total: {
		paddingTop: 12,
		paddingBottom: 12,
	},
	icon: {
		justifyContent: 'center',
		alignItems: 'flex-end',
	},
})
