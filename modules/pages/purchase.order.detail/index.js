import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'

import PurchaseService from 'app/services/purchase';

// import UserManager from 'app/managers/user';
// import OrderDetailManager from 'app/managers/order.detail';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';

import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import ActionsPart from './_actions'
import DetailPart from './_detail'
import RequestPart from './_request'
// import StylistNotePart from './_stylist.note'
// import StyleHistoryPart from 'modules/pages/user.detail/_style.history';
// import {
// 	StyleProfilePart,
// } from 'modules/pages/user.detail';

import Styles from './style';
import {} from 'lodash';

export default ConnectHelper(
	class PurchaseOrderDetail extends PageModel {

		static routeName = 'purchase.order.detail'

		static stateToProps(state) {
			return {
				isPurchasing: AuthenticationHelper.isAuthorized(state.me.roles, 'purchasing'),
				isInventory: AuthenticationHelper.isAuthorized(state.me.roles, 'inventory'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				isChanged: false,
				token: undefined,
				data: {
					requests: [],
				},
				changes: {},
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, () => {
				this.setState({
					isLoading: true,
				}, () => {
					PurchaseService.getOrder(this.props.id, {}, this.state.token).then(res => {
						this.setState({
							isLoading: false,
							data: res,
						})
					}).catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops',
								message: err ? err.detail || err.message || err : 'Something went wrong.',
							})
						})
					})
				})
			})
		}

		onNavigateToPurchases = () => this.navigator.navigator('purchase')
		onNavigateToBack = () => this.navigator.back()

		onUpdateData = (key, value) => {
			this.state.changes[key] = value

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onChanged = () => {
			this.setState({
				data: {
					...this.state.data,
					updated_at: new Date().toISOString(),
					...this.getTotal(),
				},
			})
		}

		onRemoved = id => {
			const requests = this.state.data.requests.filter(r => {
				return r.id !== id
			})

			this.setState({
				data: {
					...this.state.data,
					requests,
					...this.getTotal(requests),
				},
			})
		}

		getTotal(requests = this.state.data.requests) {
			return requests.map(r => {
				return {
					retail_price: r.retail_price * r.quantity,
					price: r.price * r.quantity,
					quantity: r.quantity,
				}
			}).reduce((sum, curr) => {
				return {
					retail_price: sum.retail_price + curr.retail_price,
					price: sum.price + curr.price,
					quantity: sum.quantity + curr.quantity,
				}
			}, {
				retail_price: 0,
				price: 0,
				quantity: 0,
			})
		}

		getChanges = () => {
			return this.state.changes
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={['inventory', 'purchasing']}
					onAuthorized={this.onAuthorized}
					paths={[{
						title: 'Purchase',
						onPress: this.onNavigateToPurchases,
					}, {
						title: 'Order',
						onPress: this.onNavigateToBack,
					}, {
						title: this.props.id,
					}]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Purchase Order Detail"
							/>
							{ this.state.isLoading ? (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							) : (
								<BoxBit unflex row>
									<BoxBit style={Styles.padder}>
										<DetailPart
											id={ this.state.data.id }
											title={ this.state.data.title }
											retailPrice={ this.state.data.retail_price }
											price={ this.state.data.price }
											quantity={ this.state.data.quantity }
											note={ this.state.data.note }
											createdAt={ this.state.data.created_at }
											changeable={ this.state.data.status === 'PENDING' && this.props.isPurchasing }
											onUpdate={ this.onUpdateData }
										/>
									</BoxBit>
									<BoxBit>
										<ActionsPart
											key={ this.state.data.status }
											id={ this.props.id }
											disabled={ !this.state.isChanged }
											changeGetter={ this.getChanges }
											isPurchasing={ this.props.isPurchasing }
											isInventory={this.props.isInventory}
											status={ this.state.data.status }
											updatedAt={ this.state.data.updated_at }
											onChanged={ this.onChanged }
										/>
										<RequestPart
											id={ this.props.id }
											editable={ this.state.data.status === 'PENDING' && this.props.isPurchasing }
											requests={ this.state.data.requests }
											onRemoved={ this.onRemoved }
											onChanged={ this.onChanged }
											style={ Styles.pad }
										/>
									</BoxBit>
								</BoxBit>
							) }
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
