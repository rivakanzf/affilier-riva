import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import PurchaseService from 'app/services/purchase';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
// import SelectionBit from 'modules/bits/selection';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import QuickViewPurchaseOrderPagelet from 'modules/pagelets/quick.view.purchase.order';

import Styles from './style';

// import { without } from 'lodash';


export default ConnectHelper(
	class PurchaseOrderPage extends PageModel {

		static routeName = 'purchase.order'

		constructor(p) {
			super(p, {
				isLoading: false,
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					status: 'PENDING',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		tableHeader = [{
			title: 'ID',
			width: .5,
		}, {
			title: 'Title',
			width: 2,
		}, {
			title: 'Total Retail Value',
			width: 1,
		}, {
			title: 'Total Value',
			width: 1,
		}, {
			title: 'Qty',
			width: .3,
		}, {
			title: 'Status',
			width: .7,
		}, {
			title: '',
			width: .2,
		}]

		paths = [{
			title: 'PURCHASE',
		}, {
			title: 'ORDER',
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.filter.status !== this.state.filter.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.filterOrder({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					status: filter.status,
				}, this.state.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								data: res.data,
							})
						}
					})
					.catch(this.onError)
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onNavigateToDetail = id => {
			this.navigator.navigate(`purchase/order/${id}`)
			this.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong.',
				})
			})
		}

		onQuickView = id => {
			this.utilities.alert.modal({
				component: (
					<QuickViewPurchaseOrderPagelet
						id={ id }
						onClose={ this.onModalRequestClose }
						onNavigateToPurchaseOrder={ this.onNavigateToDetail.bind(this, id) }
					/>
				),
			})
		}

		rowRenderer = purchase => {
			return {
				data: [{
					title: `#PO-${ purchase.id }`,
				}, {
					title: purchase.title,
				}, {
					title: `IDR ${ FormatHelper.currency(purchase.retail_price) }`,
				}, {
					title: `IDR ${ FormatHelper.currency(purchase.price) }`,
				}, {
					title: purchase.quantity,
				}, {
					children: (
						<BadgeStatusLego status={ purchase.status } />
					),
				}, {
					children: (
						<BoxBit row style={ Styles.icons }>
							<TouchableBit unflex centering onPress={ this.onQuickView.bind(this, purchase.id) }>
								<IconBit
									name={ 'quick-view' }
									color={ Colors.primary }
								/>
							</TouchableBit>
						</BoxBit>
					),
				}],
				onPress: this.onNavigateToDetail.bind(this, purchase.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={'fulfillment'}
					onAuthorized={this.onAuthorized} >
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Purchase Order"
								paths={ this.paths }
								searchPlaceholder={ 'Product, brand, or category' }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								<SelectStatusLego type={SelectStatusLego.TYPES.PURCHASES} isRequired status={ this.state.filter.status  } onChange={this.onChangeFilter.bind(this, 'status')} />
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
