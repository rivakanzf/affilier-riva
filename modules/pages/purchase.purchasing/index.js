import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import PurchaseService from 'app/services/purchase';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LinkBit from 'modules/bits/link';
// import SelectionBit from 'modules/bits/selection';
import TouchableBit from 'modules/bits/touchable';

import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

// import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import PurchaseOrderPicker from 'modules/pagelets/purchase.order.picker';
import QuickViewPurchasePagelet from 'modules/pagelets/quick.view.purchase';


import Styles from './style';

import { without } from 'lodash'


export default ConnectHelper(
	class PurchasePurchasingPage extends PageModel {

		static routeName = 'purchase.purchasing'

		constructor(p) {
			super(p, {
				isLoading: false,
				selectedIds: [],
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					ordered: 'false',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		tableHeader = [{
			title: '',
			width: .4,
		}, {
			title: 'Seller',
			width: 1,
		}, {
			title: 'Product',
			width: 3,
		}, {
			title: 'Price',
			width: 1,
		}, {
			title: 'URL',
			width: 4,
		}, {
			title: 'Request Date',
			width: 1,
		}, {
			title: '',
			width: .5,
		}]

		paths = [{
			title: 'PURCHASE',
		}, {
			title: 'REQUEST',
		}]

		order = [{
			key: 'false',
			title: 'Do not have purchase order',
			selected: true,
		}, {
			key: 'true',
			title: 'Purchased',
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.filter.ordered !== this.state.filter.ordered
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.filterRequest({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					status: 'APPROVED',
					ordered: filter.ordered,
				}, this.state.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								data: res.data,
							})
						}
					})
					.catch(this.onError)
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigateToClient = id => {
			this.navigator.navigate(`client/${id}`)
			this.props.utilities.alert.hide()
		}

		onNavigateToStylesheet = id => {
			this.navigator.navigate(`stylesheet/${id}`)
			this.props.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onToggleActive = id => {
			const isSelected = this.state.selectedIds.indexOf(id) > -1

			if(isSelected) {
				// remove
				this.setState({
					selectedIds: without(this.state.selectedIds, id),
				})
			} else {
				// add
				this.setState({
					selectedIds: [...this.state.selectedIds, id],
				})
			}
		}

		onUpdate = () => {
			if(this.state.offset !== 0) {
				this.setState({
					offset: 0,
					selectedIds: [],
				})
			} else {
				this.setState({
					selectedIds: [],
				}, this.getData)
			}
		}

		onError = err => {
			this.warn(err)

			this.setState({
				isLoading: false,
			}, () => {
				this.utilities.notification.show({
					title: 'Oops…',
					message: err ? err.detail || err.message || err : 'Something went wrong.',
				})
			})
		}

		onQuickView = id => {
			this.utilities.alert.modal({
				component: (
					<QuickViewPurchasePagelet
						disabled
						id={ id }
						onClose={ this.onModalRequestClose }
						onNavigateToClient={ this.onNavigateToClient }
						onNavigateToStylesheet={ this.onNavigateToStylesheet }
					/>
				),
			})
		}

		onAddToPurchaseOrder = () => {
			this.utilities.alert.modal({
				component: (
					<PurchaseOrderPicker
						ids={ this.state.selectedIds }
						onUpdate={ this.onUpdate }
					/>
				),
			})
		}

		rowRenderer = purchase => {
			const isActive = this.state.selectedIds.indexOf(purchase.id) > -1

			return {
				data: [{
					children: (
						<CheckboxBit dumb
							isActive={ isActive }
							onPress={ this.onToggleActive.bind(this, purchase.id) }
						/>
					),
				}, {
					title: purchase.seller,
				}, {
					title: purchase.title,
					children: (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{ purchase.title }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.note}>
								{ purchase.description }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: `IDR ${FormatHelper.currency(purchase.retail_price)}`,
					children: purchase.price !== purchase.retail_price ? (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.retail}>
								IDR { FormatHelper.currency(purchase.retail_price) }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.discounted}>
								IDR { FormatHelper.currency(purchase.price) }
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					children: (
						<LinkBit href={ purchase.url } underline target={ LinkBit.TYPES.BLANK }>
							{ purchase.url }
						</LinkBit>
					),
				}, {
					children: (
						<BoxBit style={Styles.product}>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{ TimeHelper.format(purchase.created_at, 'DD/MM/YYYY') }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={Styles.note}>
								— { TimeHelper.format(purchase.created_at, 'HH:mm') }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit row style={Styles.icons}>
							<TouchableBit unflex centering onPress={ this.onQuickView.bind(this, purchase.id) }>
								<IconBit
									name={ 'quick-view' }
									color={ Colors.primary }
								/>
							</TouchableBit>
						</BoxBit>
					),
				}],
				onPress: this.onToggleActive.bind(this, purchase.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={'fulfillment'}
					onAuthorized={this.onAuthorized} >
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Purchase Request"
								paths={ this.paths }
								searchPlaceholder={ 'Product, brand, or category' }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								{/* <SelectionBit options={ this.order } onChange={ this.onChangeFilter.bind(this, 'ordered') } /> */}
								<ButtonBit
									title={ 'Add to Purchase Order' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.state.selectedIds.length ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
									width={ ButtonBit.TYPES.WIDTHS.FIT }
									onPress={ this.onAddToPurchaseOrder }
								/>
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
