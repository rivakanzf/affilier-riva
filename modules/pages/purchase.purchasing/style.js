import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	padder: {
		marginBottom: 16,
	},

	product: {
		justifyContent: 'center',
		alignItems: 'flex-start',
		overflow: 'hidden',
	},

	title: {
		alignSelf: 'normal',
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	approve: {
		marginRight: 4,
	},

	action: {
		alignItems: 'center',
	},

	icons: {
		justifyContent: 'flex-end',
	},

	add: {
		marginRight: 8,
	},

	retail: {
		textDecoration: 'line-through',
	},

	discounted: {
		color: Colors.red.palette(7),
	},

})
