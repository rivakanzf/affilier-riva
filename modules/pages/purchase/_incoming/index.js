import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
// import TimeHelper from 'coeur/helpers/time';

import AuthenticationHelper from 'utils/helpers/authentication';

import UtilitiesContext from 'coeur/contexts/utilities';
import PageContext from 'coeur/contexts/page';

import PurchaseService from 'app/services/purchase';

import Colors from 'coeur/constants/color';
import Linking from 'coeur/libs/linking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LinkBit from 'modules/bits/link';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status'
import BadgeStatusSmallLego from 'modules/legos/badge.status.small'
import ColorLego from 'modules/legos/color';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import PickerVariantPagelet from 'modules/pagelets/picker.variant';
import PurchaseRequestApprovePagelet from 'modules/pagelets/purchase.request.approve';
import QuickViewPurchaseOrderPagelet from 'modules/pagelets/quick.view.purchase.order';
import QuickViewItemPagelet from 'modules/pagelets/quick.view.item';
import QuickViewStylesheetPagelet from 'modules/pagelets/quick.view.stylesheet';

import Styles from './style';

import { capitalize, flatten } from 'lodash'


export default ConnectHelper(
	class PurchasePurchasingPage extends StatefulModel {

		static stateToProps(state) {
			return {
				isPacking: AuthenticationHelper.isAuthorized(state.me.roles, 'packing'),
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				activeId: -1,
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					// ordered: 'false',
				},
			})
		}

		getterId = 1

		headers = [{
			title: 'Title',
			width: 2.5,
		}, {
			title: 'Qty',
			align: 'center',
			width: .5,
		}, {
			title: 'Status',
			width: 1,
		}, {
			title: 'Note',
			width: 2.5,
		}, {
			title: '',
			width: .2,
		}, {
			title: '',
			width: .5,
		}, {
			title: '',
			width: .3,
		}]

		childHeaders = [{
			title: 'Product',
			width: 3.2,
		}, {
			title: 'Category',
			width: .7,
		}, {
			title: 'Color',
			width: .6,
		}, {
			title: 'Size',
			width: .3,
		}, {
			title: 'Note',
			width: 1,
		}, {
			title: '',
			width: .5,
		}, {
			title: 'Actions / Status',
			width: 1.4,
		}]

		componentDidMount() {
			if (this.props.token) {
				this.getData()
			}
		}

		componentDidUpdate(pP, pS) {
			if (
				this.props.token
				&& (
					pS.search !== this.state.search
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		changeable = request => {
			return request.purchase_order_status === 'PURCHASED'
				&& (
					request.status === 'APPROVED'
					|| request.status === 'RECEIVED'
				)
		}

		inactive = request => {
			return request.status !== 'RESOLVED'
				&& request.status !== 'EXCEPTION'
		}

		getData = () => {
			const {
				offset,
				count,
				// filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.filterOrder({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					status: 'PURCHASED',
					with_user: true,
					with_request: true,
				}, this.props.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								data: res.data.map(po => {
									return {
										...po,
										requests: flatten(po.requests.map(this.spreader.bind(this, po))),
									}
								}),
							})
						}
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.props.utilities.notification.show({
								title: 'Oops…',
								message: err ? err.detail || err.message || err : 'Something went wrong.',
							})
						})
					})
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigateToInventory = () => {
			Linking.open(window.location.href.split('/')[0] + '/inventory')
		}

		onNavigateToDetail = id => {
			this.props.page.navigator.navigate(`purchase/order/${id}`)
		}

		onNavigateToStylesheet = id => {
			this.props.page.navigator.navigate('stylesheet/' + id)
			this.onModalRequestClose()
		}

		onNavigate = (link, param) => {
			this.props.page.navigator.navigate(link, param)
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onToggleActive = id => {
			if(this.state.activeId !== id) {
				this.setState({
					activeId: id,
				})
			}
		}

		onQuickView = id => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewPurchaseOrderPagelet
						id={ id }
						onClose={ this.onModalRequestClose }
						onNavigateToPurchaseOrder={ this.onNavigateToDetail.bind(this, id) }
					/>
				),
			})
		}

		onQuickViewRequest = request => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewItemPagelet
						id={ request.id }
						type={ 'PURCHASE_REQUEST' }
						onClose={this.onModalRequestClose}
					/>
				),
			})
		}

		onQuickViewStylesheet = id => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewStylesheetPagelet
						id={ id }
						onNavigate={ this.onNavigate }
					/>
				),
			})
		}

		onReceive = request => {
			request.isLoading = 'receiving'

			this.forceUpdate()

			PurchaseService.updateRequest(request.id, {
				status: 'RECEIVED',
			}, this.props.token).then(isUpdated => {
				request.isLoading = false

				if (isUpdated) {
					request.status = 'RECEIVED'

					this.props.utilities.notification.show({
						title: 'Yeay!',
						message: 'Success receiving item.',
						type: 'SUCCESS',
					})

					this.forceUpdate()
				} else {
					throw new Error('Receiving failed.')
				}
			}).catch(err => {
				request.isLoading = false
				this.forceUpdate()

				throw err
			}).catch(this.onError)
		}

		onException = request => {
			request.isLoading = 'excepting'

			this.forceUpdate()

			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why this inventory exceptioned. *required"
						onCancel={ () => {
							request.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							PurchaseService.despiseRequest(request.id, {
								quantity: 1,
								main: true,
								note,
							}, this.props.token).then(isUpdated => {
								request.isLoading = false

								if(isUpdated) {
									request.status = 'EXCEPTION'

									this.props.utilities.notification.show({
										title: 'Yeay!',
										message: 'Success excepting request.',
										type: 'SUCCESS',
									})

									this.forceUpdate()
								} else {
									throw new Error('Excepting failed.')
								}
							}).catch(err => {
								request.isLoading = false
								this.forceUpdate()

								throw err
							}).catch(this.onError)
						} }
					/>
				),
			})
		}

		onAccepting = request => {
			request.isLoading = 'accepting'

			this.forceUpdate()

			this.props.utilities.alert.modal({
				component: (
					<PickerVariantPagelet
						onClose={ () => {
							request.isLoading = false
							this.forceUpdate()
						} }
						search={ request.title }
						onSelect={ this.onConfirmAccepting.bind(this, undefined, request) }
						onNavigateToInventory={ this.onNavigateToInventory }
					/>
				),
			})
		}

		onConfirmAccepting = (status, request, variantId) => {
			this.props.utilities.alert.modal({
				component: (
					<PurchaseRequestApprovePagelet
						status={ status }
						orderId={ request.purchase_order_id }
						variantId={ variantId }
						purchaseRequests={ [{
							id: request.id,
							isMain: request.isMain,
							quantity: 1,
						}] }
						onClose={ () => {
							request.isLoading = false
							this.forceUpdate()
						} }
						onCancel={ () => {
							if(status) {
								this.onDenying(request)
							} else {
								this.onAccepting(request)
							}
						} }
						onApproved={ () => {
							request.isLoading = false

							if(!status) {
								// accepting
								request.status = 'RESOLVED'
							} else {
								// denying
								request.status = 'EXCEPTION'
							}

							this.props.utilities.notification.show({
								title: 'Yeay!',
								message: 'Success accepting / denying item.',
								type: 'SUCCESS',
							})

							this.forceUpdate()
						} }
					/>
				),
			})
		}

		onDenying = request => {
			this.props.utilities.menu.show({
				title: 'Select reason',
				actions: [{
					title: 'Defect',
					type: this.props.utilities.menu.TYPES.SELECTION,
					onPress: this.onConfirmDenying.bind(this, 'DEFECT', request),
				}, {
					title: 'Exception',
					type: this.props.utilities.menu.TYPES.SELECTION,
					onPress: this.onConfirmDenying.bind(this, 'EXCEPTION', request),
				}],
			})
		}

		onConfirmDenying = (status, request) => {
			request.isLoading = 'denying'

			this.forceUpdate()

			this.props.utilities.alert.modal({
				component: (
					<PickerVariantPagelet
						onClose={ () => {
							request.isLoading = false
							this.forceUpdate()
						} }
						search={ request.title }
						onSelect={ this.onConfirmAccepting.bind(this, status, request) }
						onNavigateToInventory={ this.onNavigateToInventory }
					/>
				),
			})
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? (err.detail && err.detail.message || err.detail) || err.message || err : 'Something went wrong.',
			})
		}

		spreader = (order, request) => {
			return Array(request.quantity).fill(undefined).map((u, i) => {
				return {
					key: `${request.id}|${i}`,
					isMain: request.stylesheet && i === 0,
					...request,
					quantity: 1,
					purchase_order_id: order.id,
					purchase_order_status: order.status,
				}
			})
		}

		rowRenderer = order => {
			const isActive = this.state.activeId === order.id

			return {
				data: [{
					title: `${ order.user } / ${ order.title }`,
					children: (
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								<TextBit style={ Styles.seller }>[#PO-{ order.id }]</TextBit> { order.title }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ order.user }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: order.quantity,
				}, {
					children: (
						<BadgeStatusLego status={ order.status } />
					),
				}, {
					title: order.note || '-',
				}, {
					title: '',
				}, {
					children: (
						<BoxBit row style={ Styles.icons }>
							<TouchableBit unflex centering onPress={ this.onQuickView.bind(this, order.id) }>
								<IconBit
									name={ 'quick-view' }
									color={ Colors.primary }
								/>
							</TouchableBit>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit style={ Styles.dropdown }>
							<IconBit
								name={isActive ? 'dropdown-up' : 'dropdown'}
								size={20}
							/>
						</BoxBit>
					),
				}],
				children: isActive ? (
					<TableLego compact small isLoading={ this.state.isLoading }
						headers={ this.childHeaders }
						rows={ order.requests.map(this.childRenderer) }
						style={ Styles.table } />
				) : false,
				onPress: this.onToggleActive.bind(this, order.id),
			}
		}

		childRenderer = request => {
			// const actions = this.actions(request)
			const disabled = !this.changeable(request)
			const inactive = this.inactive(request)

			return {
				data: [{
					children: (
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.seller }>
								[{ request.seller }]
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
								<LinkBit target={ LinkBit.TYPES.BLANK } href={ request.url } underline>
									{ request.brand } – { request.title }
								</LinkBit>
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					// title: request.category,
					children: (
						<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.ellipsis } ellipsis numberOfLines={ 1 }>
							{ request.category }
						</GeomanistBit>
					),
				}, {
					children: (
						<BoxBit row style={ Styles.row }>
							<ColorLego colors={ [request.color] } size={ 16 } radius={ 8 } border={ 0 } style={ Styles.color } />
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
								{ capitalize(request.color.title) }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: request.size,
				}, {
					title: request.note || '-',
				}, {
					title: '-',
					children: request.isMain ? (
						<LinkBit onPress={ this.onQuickViewStylesheet.bind(this, request.stylesheet_id) } underline>
							#ST-{ request.stylesheet_id }
						</LinkBit>
					) : undefined,
				}, {
					// eslint-disable-next-line no-nested-ternary
					children: request.status === 'APPROVED' ? (
						<BoxBit row style={ Styles.actions }>
							<ButtonBit weight="medium"
								title={ 'Receive' }
								// eslint-disable-next-line no-nested-ternary
								state={ request.isLoading === 'receiving' ? ButtonBit.TYPES.STATES.LOADING : request.isLoading || disabled ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onReceive.bind(this, request) }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								size={ ButtonBit.TYPES.SIZES.MIDGET }
								style={ Styles.button }
							/>
							<ButtonBit weight="medium"
								title={ 'Exception' }
								theme={ ButtonBit.TYPES.THEMES.SECONDARY }
								// eslint-disable-next-line no-nested-ternary
								state={ request.isLoading === 'excepting' ? ButtonBit.TYPES.STATES.LOADING : request.isLoading || disabled ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onException.bind(this, request) }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								size={ ButtonBit.TYPES.SIZES.MIDGET }
								style={ Styles.button }
							/>
						</BoxBit>
					) : request.status === 'RECEIVED' ? (
						<BoxBit row style={ Styles.actions }>
							<ButtonBit weight="medium"
								title={ request.isMain ? 'Pack' : 'Accept' }
								// eslint-disable-next-line no-nested-ternary
								state={ request.isLoading === 'accepting' ? ButtonBit.TYPES.STATES.LOADING : request.isLoading || disabled ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onAccepting.bind(this, request) }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								size={ ButtonBit.TYPES.SIZES.MIDGET }
								style={ Styles.button }
							/>
							<ButtonBit weight="medium"
								title={ 'Deny' }
								theme={ ButtonBit.TYPES.THEMES.SECONDARY }
								// eslint-disable-next-line no-nested-ternary
								state={ request.isLoading === 'denying' ? ButtonBit.TYPES.STATES.LOADING : request.isLoading || disabled ? ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onDenying.bind(this, request) }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								size={ ButtonBit.TYPES.SIZES.MIDGET }
								style={ Styles.button }
							/>
						</BoxBit>
					) : (
						<BoxBit row style={ Styles.row }>
							<BadgeStatusSmallLego unflex status={ request.status } />
						</BoxBit>
					),
				}],
				onPress: disabled && inactive ? undefined : this.onQuickViewRequest.bind(this, request),
				style: disabled && inactive ? Styles.inactive : undefined,
			}
		}

		view() {
			return (
				<BoxBit>
					<HeaderFilterComponent
						current={ this.state.offset }
						count={ this.state.count }
						total={ this.state.total }
						onNavigationChange={ this.onNavigationChange } />
					<TableLego isLoading={ this.state.isLoading }
						headers={ this.headers }
						rows={ this.state.data.map(this.rowRenderer) }
						style={ Styles.padder } />
					<HeaderFilterComponent
						current={ this.state.offset }
						count={ this.state.count }
						total={ this.state.total }
						onNavigationChange={ this.onNavigationChange } />
				</BoxBit>
			)
		}
	}
)
