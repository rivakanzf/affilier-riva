import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	padder: {
		marginBottom: 16,
	},

	icons: {
		justifyContent: 'flex-end',
	},

	row: {
		alignItems: 'center',
	},

	ellipsis: {
		whiteSpace: 'nowrap',
	},

	seller: {
		color: Colors.primary,
	},

	color: {
		marginRight: 4,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	inactive: {
		filter: 'grayscale(.7)',
		opacity: .3,
	},

	dropdown: {
		justifyContent: 'center',
		alignItems: 'flex-end',
	},

	table: {
		marginTop: 8,
		marginBottom: -12,
		marginLeft: -8,
		marginRight: -8,
		paddingTop: 4,
		paddingBottom: 4,
	},

	actions: {
		flexWrap: 'wrap',
		marginBottom: -4,
	},

	button: {
		marginRight: 4,
		marginBottom: 4,
	},
})
