import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';

import UtilitiesContext from 'coeur/contexts/utilities';
import PageContext from 'coeur/contexts/page';

import PurchaseService from 'app/services/purchase';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import PriceLego from 'modules/legos/price';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';

import QuickViewPurchaseOrderPagelet from 'modules/pagelets/quick.view.purchase.order';

import Styles from './style';

// import { without } from 'lodash';


export default ConnectHelper(
	class OrderPart extends StatefulModel {

		static contexts = [
			UtilitiesContext,
			PageContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					status: 'PENDING',
				},
			})
		}

		getterId = 1

		tableHeader = [{
			title: 'ID',
			width: .5,
		}, {
			title: 'Title',
			width: 2.5,
		}, {
			title: 'Status',
			width: .7,
		}, {
			title: '',
			width: .5,
		}, {
			title: 'Qty',
			align: 'center',
			width: .5,
		}, {
			title: 'Total',
			align: 'right',
			width: 1,
		}, {
			title: '',
			width: .5,
		}, {
			title: '',
			width: .2,
		}]

		componentDidMount() {
			if (this.props.token) {
				this.getData()
			}
		}

		componentDidUpdate(pP, pS) {
			if (
				this.props.token
				&& (
					pS.search !== this.state.search
					|| pS.filter.status !== this.state.filter.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.filterOrder({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					status: filter.status,
					with_user: true,
				}, this.props.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								data: res.data,
							})
						}
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.props.utilities.notification.show({
								title: 'Oops…',
								message: err ? err.detail || err.message || err : 'Something went wrong.',
							})
						})
					})
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigateToDetail = id => {
			this.props.page.navigator.navigate(`purchase/order/${id}`)
			this.props.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onQuickView = id => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewPurchaseOrderPagelet
						id={ id }
						onClose={ this.onModalRequestClose }
						onNavigateToPurchaseOrder={ this.onNavigateToDetail.bind(this, id) }
					/>
				),
			})
		}

		rowRenderer = purchase => {
			return {
				data: [{
					title: `#PO-${ purchase.id }`,
				}, {
					children: (
						<BoxBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ purchase.title }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ purchase.user || '-' }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					children: (
						<BadgeStatusLego status={ purchase.status } />
					),
				}, {
					title: '',
				}, {
					title: purchase.quantity,
				}, {
					title: `IDR ${FormatHelper.currency(purchase.retail_price)}`,
					children: purchase.retail_price !== purchase.price ? (
						<PriceLego price={ purchase.price } retailPrice={ purchase.retail_price } />
					) : undefined,
				}, {
					title: '',
				}, {
					children: (
						<BoxBit row style={ Styles.icons }>
							<TouchableBit unflex centering onPress={ this.onQuickView.bind(this, purchase.id) }>
								<IconBit
									name={ 'quick-view' }
									color={ Colors.primary }
								/>
							</TouchableBit>
						</BoxBit>
					),
				}],
				onPress: this.onNavigateToDetail.bind(this, purchase.id),
			}
		}

		view() {
			return (
				<BoxBit>
					<HeaderFilterComponent
						current={ this.state.offset }
						count={ this.state.count }
						total={ this.state.total }
						onNavigationChange={ this.onNavigationChange }>
						<SelectStatusLego type={ SelectStatusLego.TYPES.PURCHASES } isRequired status={ this.state.filter.status  } onChange={ this.onChangeFilter.bind(this, 'status') } />
					</HeaderFilterComponent>
					<TableLego isLoading={ this.state.isLoading }
						headers={ this.tableHeader }
						rows={ this.state.data.map(this.rowRenderer) }
						style={ Styles.padder } />
					<HeaderFilterComponent
						current={ this.state.offset }
						count={ this.state.count }
						total={ this.state.total }
						onNavigationChange={ this.onNavigationChange } />
				</BoxBit>
			)
		}
	}
)
