import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import AuthenticationHelper from 'utils/helpers/authentication';

import UtilitiesContext from 'coeur/contexts/utilities';
import PageContext from 'coeur/contexts/page';

import PurchaseService from 'app/services/purchase';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LinkBit from 'modules/bits/link';
// import SelectionBit from 'modules/bits/selection';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import PriceLego from 'modules/legos/price';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';

import PurchaseRequestUpdatePagelet from 'modules/pagelets/purchase.request.update';
import PurchaseOrderPicker from 'modules/pagelets/purchase.order.picker';
import QuickViewPurchasePagelet from 'modules/pagelets/quick.view.purchase';

import Styles from './style';

import { without } from 'lodash'


export default ConnectHelper(
	class RequestPart extends ConnectedStatefulModel {

		static stateToProps(state) {
			return {
				isPurchasing: AuthenticationHelper.isAuthorized(state.me.roles, 'purchasing'),
			}
		}

		static contexts = [
			UtilitiesContext,
			PageContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				selectedIds: [],
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					ordered: 'false',
					status: 'APPROVED',
				},
			})
		}

		getterId = 1

		tableHeader = [{
			title: '',
			width: .2,
		}, {
			title: 'Date',
			width: .7,
		}, {
			title: 'Product',
			width: 3.3,
		}, {
			title: 'Requested By',
			width: 1.5,
		}, {
			title: 'Note',
			width: 1.1,
		}, {
			title: 'Price',
			width: 1,
		}, {
			title: 'Status',
			width: 1,
		}, {
			title: '',
			width: .2,
		}]

		componentDidMount() {
			if (this.props.token) {
				this.getData()
			}
		}

		componentDidUpdate(pP, pS) {
			if (
				this.props.token
				&& (
					pS.search !== this.state.search
					|| pS.filter.ordered !== this.state.filter.ordered
					|| pS.filter.status !== this.state.filter.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				PurchaseService.filterRequest({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					status: filter.status,
					ordered: filter.ordered,
				}, this.props.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								data: res.data,
							})
						}
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.props.utilities.notification.show({
								title: 'Oops…',
								message: err ? err.detail || err.message || err : 'Something went wrong.',
							})
						})
					})
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigateToClient = id => {
			this.props.page.navigator.navigate(`client/${id}`)
			this.props.utilities.alert.hide()
		}

		onNavigateToStylesheet = id => {
			this.props.page.navigator.navigate(`stylesheet/${id}`)
			this.props.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onToggleActive = id => {
			const isSelected = this.state.selectedIds.indexOf(id) > -1

			if(isSelected) {
				// remove
				this.setState({
					selectedIds: without(this.state.selectedIds, id),
				})
			} else {
				// add
				this.setState({
					selectedIds: [...this.state.selectedIds, id],
				})
			}
		}

		onUpdate = () => {
			if(this.state.offset !== 0) {
				this.setState({
					offset: 0,
					selectedIds: [],
				})
			} else {
				this.setState({
					selectedIds: [],
				}, this.getData)
			}
		}

		onQuickView = id => {
			this.props.utilities.alert.modal({
				component: this.props.isPurchasing ? (
					<PurchaseRequestUpdatePagelet editable
						id={ id } unremoveable pendingOnEdit={ false }
					/>
				) : (
					<QuickViewPurchasePagelet disabled
						id={id}
						onClose={this.onModalRequestClose}
						onNavigateToClient={this.onNavigateToClient}
						onNavigateToStylesheet={this.onNavigateToStylesheet}
					/>
				),
			})
		}

		onAddToPurchaseOrder = () => {
			this.props.utilities.alert.modal({
				component: (
					<PurchaseOrderPicker
						ids={ this.state.selectedIds }
						onUpdate={ this.onUpdate }
					/>
				),
			})
		}

		rowRenderer = purchase => {
			const isActive = this.state.selectedIds.indexOf(purchase.id) > -1

			return {
				data: [{
					children: (
						<CheckboxBit dumb disabled={ !this.props.isPurchasing }
							isActive={ isActive }
							onPress={ this.props.isPurchasing ? this.onToggleActive.bind(this, purchase.id) : undefined }
						/>
					),
				}, {
					title: TimeHelper.format(purchase.created_at, 'DD/MM/YY HH:mm'),
				}, {
					title: purchase.title,
					children: (
						<BoxBit style={ Styles.product }>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								<TextBit style={ Styles.seller }>[{ purchase.seller }]</TextBit> <LinkBit target={ LinkBit.TYPES.BLANK } href={ purchase.url } underline>
									{ purchase.title }
								</LinkBit>
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ purchase.description }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit style={ Styles.product }>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{ purchase.user }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ purchase.matchbox ? `[#ST-${ purchase.stylesheet_id }] ${ purchase.matchbox }` : '-' }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit style={ Styles.product }>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ purchase.note || '-' }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: `IDR ${FormatHelper.currency(purchase.retail_price)}`,
					children: purchase.price !== purchase.retail_price ? (
						<PriceLego align="left" price={ purchase.price } retailPrice={ purchase.retail_price } />
					) : undefined,
				}, {
					children: (
						<BadgeStatusLego status={ purchase.status } />
					),
				}, {
					children: (
						<BoxBit row style={Styles.icons}>
							<TouchableBit unflex centering onPress={ this.onQuickView.bind(this, purchase.id) }>
								<IconBit
									name={ this.props.isPurchasing ? 'edit' : 'quick-view' }
									color={ Colors.primary }
								/>
							</TouchableBit>
						</BoxBit>
					),
				}],
				onPress: this.props.isPurchasing ? this.onToggleActive.bind(this, purchase.id) : undefined,
			}
		}

		view() {
			return (
				<BoxBit>
					<HeaderFilterComponent
						current={ this.state.offset }
						count={ this.state.count }
						total={ this.state.total }
						onNavigationChange={ this.onNavigationChange }>
						<ButtonBit
							title={ 'Add to Purchase Order' }
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ this.state.selectedIds.length ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
							// width={ ButtonBit.TYPES.WIDTHS.FIT }
							onPress={ this.onAddToPurchaseOrder }
						/>
						<SelectStatusLego type={ SelectStatusLego.TYPES.PURCHASE_REQUESTS } status={ this.state.filter.status } onChange={ this.onChangeFilter.bind(this, 'status') } />
					</HeaderFilterComponent>
					<TableLego isLoading={ this.state.isLoading }
						headers={ this.tableHeader }
						rows={ this.state.data.map(this.rowRenderer) }
						style={ Styles.padder } />
					<HeaderFilterComponent
						current={ this.state.offset }
						count={ this.state.count }
						total={ this.state.total }
						onNavigationChange={ this.onNavigationChange } />
				</BoxBit>
			)
		}
	}
)
