import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({

	padder: {
		marginBottom: 16,
	},

	product: {
		justifyContent: 'center',
		alignItems: 'flex-start',
		overflow: 'hidden',
	},

	seller: {
		color: Colors.primary,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	icons: {
		justifyContent: 'flex-end',
	},

	retail: {
		textDecoration: 'line-through',
	},

	discounted: {
		color: Colors.red.palette(7),
	},

})
