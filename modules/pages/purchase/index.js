import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
// import FormatHelper from 'coeur/helpers/format';
// import TimeHelper from 'coeur/helpers/time';

// import PurchaseService from 'app/services/purchase';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';

import TabLego from 'modules/legos/tab';

import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import QuickCreatePurchaseRequestPagelet from 'modules/pagelets/quick.create.purchase.request';

import IncomingPart from './_incoming'
import OrderPart from './_order'
import RequestPart from './_request'

import Styles from './style';


export default ConnectHelper(
	class PurchasePurchasingPage extends PageModel {

		static routeName = 'purchase'

		constructor(p) {
			super(p, {
				activeIndex: 0,
				token: undefined,
				key: 1,
			})
		}

		tabs = [{
			title: 'Requests',
			icon: 'purchases',
			onPress: () => {
				this.onChangeTab(0)
			},
		}, {
			title: 'Order',
			icon: 'order',
			onPress: () => {
				this.onChangeTab(1)
			},
		},  {
			title: 'Incoming',
			icon: 'shipments',
			onPress: () => {
				this.onChangeTab(2)
			},
		}]

		paths = [{
			title: 'Purchases',
		}]

		roles = ['purchasing', 'inventory', 'packing']

		title = () => {
			switch(this.state.activeIndex) {
			case 0:
				return 'Purchase Requests'
			case 1:
				return 'Purchase Order'
			case 2:
				return 'Incoming Purchase Requests'
			default:
				return false
			}
		}

		onUpdate = () => {
			this.setState({
				key: this.state.key + 1,
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onChangeTab = index => {
			this.setState({
				activeIndex: index,
			})
		}

		onCreatePurchaseRequest = () => {
			this.utilities.alert.modal({
				component: (
					<QuickCreatePurchaseRequestPagelet
						approved // Make it auto approved
						onCreated={ this.onUpdate }
					/>
				),
			})
		}

		contentRenderer = index => {
			if(this.state.token) {
				switch(index) {
				case 0:
				default:
					return (
						<RequestPart key={ this.state.key } token={ this.state.token } />
					)
				case 1:
					return (
						<OrderPart key={ this.state.key } token={ this.state.token } />
					)
				case 2:
					return (
						<IncomingPart key={ this.state.key } token={ this.state.token } />
					)
				}
			} else {
				return false
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={ this.paths }
				>
					{ () => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent
								title={ this.title() }
								searchPlaceholder={ 'Product, brand, or category' }
								buttonPlaceholder={ 'Create Purchase Request' }
								onSearch={ this.onSearch }
								onPress={ this.onCreatePurchaseRequest }
							/>
							<TabLego activeIndex={ this.state.activeIndex } tabs={ this.tabs } style={ Styles.tab } />
							{ this.contentRenderer(this.state.activeIndex) }
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
