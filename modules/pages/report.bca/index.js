/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';
// import FormatHelper from 'coeur/helpers/format';
import Colors from 'coeur/constants/color'

// import OrderService from 'app/services/new.order';
// import InventoryService from 'app/services/inventory'
import ReportService from 'app/services/report'
import MeManager from 'app/managers/me';

import TimeHelper from 'coeur/helpers/time';


import BoxBit from 'modules/bits/box';
// import InputCheckboxBit from 'modules/bits/input.checkbox';
import ScrollViewBit from 'modules/bits/scroll.view'
import GeomanistBit from 'modules/bits/geomanist'
import IconBit from 'modules/bits/icon'
import ClipboardBit from 'modules/bits/clipboard'
import TouchableBit from 'modules/bits/touchable'
import LoaderBit from 'modules/bits/loader'


import SelectStatusBadgeLego from 'modules/legos/select.status.badge'

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

// import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
// import QuickViewOrderPagelet from 'modules/pagelets/quick.view.order';
// import QuickViewVoucherPagelet from 'modules/pagelets/quick.view.voucher';

import Styles from './style';

import { isEmpty, orderBy } from 'lodash';


export default ConnectHelper(
	class ReportInventoryPage extends PageModel {

		static routeName = 'report.inventory'

		constructor(p) {

			const query = CommonHelper.getQueryString()

			super(p, {
				isLoading: true,

				data: [],
				total: 0,
				offset: query.offset || 0,
				count: query.count || 40,
				search: query.search || p.search || null,
				orderBy: query.orderBy || p.orderBy || 'ASC',


				token: undefined,
			})
		}

		componentDidUpdate(pP, pS) {
			if(
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
					|| pS.search !== this.state.search
					|| pS.orderBy !== this.state.orderBy
				)
			) {
				const query = CommonHelper.stripUndefined({
					offset: this.state.offset || undefined,
					count: this.state.count || undefined,
					search: this.state.search || undefined,
					orderBy: this.state.orderBy || undefined,

				})

				window.history.replaceState({}, '', isEmpty(query) ? '/report/BCA' : `/report/BCA?${Object.keys(query).map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}

		roles = ['fulfillment', 'finance', 'packing', 'analyst', 'administrator']

		
		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				ReportService.getBCA({
					limit: this.state.count,
					offset: this.state.offset,
					search: this.state.search,
					sort_by_created_at: this.state.orderBy,
				}, this.state.token).then(res => {
					this.setState({
						isLoading: false,
						total: res.count,
						data: orderBy(res.data, 'id', ['desc']),
					})
				}).catch(err => {
					this.setState({
						isLoading: false,
					}, () => {
						if (err && err.code === '031') {
							this.utilities.notification.show({
								title: 'Sorry',
								message: 'You\'ve been logged out. Please log in again.',
							})

							this.navigator.navigate('login')

							MeManager.logout();
						} else {
							this.props.utilities.notification.show({
								title: 'Oops',
								message: 'Something went wrong, please try again later',
							})
						}
					})
				})
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onChangeFilter = (key, val) => {
			if((key === 'date' || key === 'shipmentDate') && val) {
				this.setState({
					[key]: TimeHelper.moment(val).toISOString(),
					offset: 0,
				})
			} else {
				this.setState({
					[key]: val,
					offset: 0,
				})
			}
		}

		onNavigateToBack = () => {
			this.navigator.back()
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onNavigateToOrderDetail = (orderId) => {
			this.navigator.navigate(`order/${orderId}`)
		}

		rowRenderer = data => {
			switch(data.renderMode) {
			case 'ICON':
				return (
					<IconBit
						name={data.title === 'YES' ? 'circle-checkmark' : 'circle-null'}
						color={ data.title === 'YES' ? Colors.green.palette(2) : Colors.red.palette(7) }
						size={24}
						style={ Styles.data }

					/>
				)
			case 'BADGE':
				return (
					<SelectStatusBadgeLego
						type={ data.type }
						status={ data.title }
						disabled
					/>
				)
			case 'CLIPBOARD':
				return (
					<BoxBit>
						<ClipboardBit
							// autogrow={ 44 }
							value={ data.title }
							style={ Styles.data }
						/>
					</BoxBit>
				)
			case 'CLICKABLE':
				return (
					<TouchableBit onPress={ this.onNavigateToOrderDetail.bind(this, data.title, data.orderId) }>
						<GeomanistBit style={[Styles.data, Styles.underline]} type={ GeomanistBit.TYPES.PARAGRAPH_3 } align={ data.align } weight={ data.weight }>
							{ data.title }
						</GeomanistBit>
					</TouchableBit>
				)
			default:
				return (
					<GeomanistBit style={Styles.data} type={ GeomanistBit.TYPES.PARAGRAPH_3 } align={ data.align } weight={ data.weight }>
						{ data.title }
					</GeomanistBit>
				)
			}
			
		}

		listRenderer = (data, i) => {
			return (
				<BoxBit key={ i } unflex style={[Styles.row, i & 1 ? Styles.even : undefined]}>
					{
						i !== 0
							? this.rowRenderer(data)
							: (
								<GeomanistBit style={Styles.data} type={ GeomanistBit.TYPES.PARAGRAPH_3 } align={ data.align } weight={ i === 0 ? 'bold' : data.weight }>
									{ data.title }
								</GeomanistBit>
							)
					}
				</BoxBit>
			)
		}

		orderIdRenderer = () => {
			return (
				<TouchableBit>
					{ ['Order ID', ...this.state.data.map(order => order.id)].map(label => {
						return {
							renderMode: 'CLICKABLE',
							title: label,
							orderId: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</TouchableBit>
			)
		}

		dateRenderer = () => {
			return (
				<TouchableBit>
					{ ['Date', ...this.state.data.map(order => order.created_at)].map((label, i) => {
						return {
							title: i === 0 ? label : TimeHelper.format(label),
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</TouchableBit>
			)
		}

		orderNumberRenderer = () => {
			return (
				<TouchableBit>
					{ ['Order Number', ...this.state.data.map(order => order.number)].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</TouchableBit>
			)
		}

		orderStatusRenderer = () => {
			return (
				<TouchableBit>
					{ ['Order Status', ...this.state.data.map(order => order.order_payment.status)].map(label => {
						return {
							renderMode: 'BADGE',
							type: SelectStatusBadgeLego.TYPES.ORDERS,
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</TouchableBit>
			)
		}

		paymentStatusRenderer = () => {
			return (
				<TouchableBit>
					{ ['Payment Status', ...this.state.data.map(order => order.order_payment.payment_status)].map(label => {
						return {
							renderMode: 'BADGE',
							type: SelectStatusBadgeLego.TYPES.PAYMENTS,
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</TouchableBit>
			)
		}

		emailRenderer = () => {
			return (
				<TouchableBit>
					{ ['Email', ...this.state.data.map(order => order.user.email)].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</TouchableBit>
			)
		}

		tableRenderer = () => {
			return (
				<ScrollViewBit unflex horizontal>
					{this.orderIdRenderer()}
					{this.dateRenderer()}
					{this.orderNumberRenderer()}
					{this.orderStatusRenderer()}
					{this.paymentStatusRenderer()}
					{this.emailRenderer()}
					{/* {this.variantIdRenderer()}
					{this.statusRenderer()}
					{this.skuRenderer()}
					{this.urlRenderer()}
					{this.brandRenderer()}
					{this.titleRenderer()}
					{this.colorRenderer()}
					{this.sizeRenderer()}
					{this.priceRenderer()}
					{this.retailPriceRenderer()}
					{this.purchasePriceRenderer()}
					{this.isAvailableRenderer()}
					{this.isPublishedRenderer()}
					{this.isOverseasRenderer()}
					{this.consignmentStockRenderer()} */}
				</ScrollViewBit>
			)
		}

		loadingRenderer = () => {
			return (
				<BoxBit centering>
					<LoaderBit simple/>
				</BoxBit>
			)
		}

		view() {
			return (
				<PageAuthorizedComponent
					// roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Inventory Report - BCA' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Inventory Report - BCA"
								// paths={[{ title: '' }]}
								// buttonPlaceholder="Create New Order"
								// onPress={ this.onAddNewOrder }
								search={ this.state.search }
								searchPlaceholder="Brand or Product Title"
								onSearch={ this.onSearchHandler }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							/>
							{
								this.state.isLoading
									? this.loadingRenderer()
									: this.tableRenderer()
							}
						
							{/* <TableLego
								compact
								isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={ Styles.padder }
							/> */}
							{/* <HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							/> */}
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
