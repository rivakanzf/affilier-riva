/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';
import FormatHelper from 'coeur/helpers/format';
import Colors from 'coeur/constants/color'

import OrderService from 'app/services/new.order';
// import InventoryService from 'app/services/inventory'
import ReportService from 'app/services/report'
import MeManager from 'app/managers/me';

import TimeHelper from 'coeur/helpers/time';


import BoxBit from 'modules/bits/box';
import InputCheckboxBit from 'modules/bits/input.checkbox';
import ScrollViewBit from 'modules/bits/scroll.view'
import GeomanistBit from 'modules/bits/geomanist'
import IconBit from 'modules/bits/icon'
import ClipboardBit from 'modules/bits/clipboard'
import TouchableBit from 'modules/bits/touchable'
import LoaderBit from 'modules/bits/loader'


import SelectStatusBadgeLego from 'modules/legos/select.status.badge'

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import QuickViewOrderPagelet from 'modules/pagelets/quick.view.order';
import QuickViewVoucherPagelet from 'modules/pagelets/quick.view.voucher';

import Styles from './style';

import { isEmpty } from 'lodash';


export default ConnectHelper(
	class ReportInventoryPage extends PageModel {

		static routeName = 'report.inventory'

		// static stateToProps(state) {
		// 	return {
		// 	}
		// }

		constructor(p) {

			const query = CommonHelper.getQueryString()

			super(p, {
				isLoading: true,

				data: [],
				total: 0,
				offset: query.offset || 0,
				count: query.count || 40,
				search: query.search || p.search || null,
				orderBy: query.orderBy || p.orderBy || 'ASC',


				token: undefined,
			})
		}

		componentDidUpdate(pP, pS) {
			if(
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
					|| pS.search !== this.state.search
					|| pS.orderBy !== this.state.orderBy
				)
			) {
				const query = CommonHelper.stripUndefined({
					offset: this.state.offset || undefined,
					count: this.state.count || undefined,
					search: this.state.search || undefined,
					orderBy: this.state.orderBy || undefined,

				})

				window.history.replaceState({}, '', isEmpty(query) ? '/report/inventory' : `/report/inventory?${Object.keys(query).map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}

		roles = ['fulfillment', 'stylist', 'finance', 'packing', 'analyst', 'administrator']

		tableHeader = [{
			title: 'INV ID',
			width: 0.5,
		}, {
			title: 'VAR ID',
			width: 0.5,
		}, {
			title: 'Brand',
			width: 1,
		}, {
			title: 'Title',
			width: 1,
		}, {
			title: 'Color',
			width: 1,
		}, {
			title: 'Size',
			width: 1,
		}, {
			title: 'Price',
			width: 1,
		}, {
			title: 'Retail price',
			width: 1,
		}, {
			title: 'URL',
			width: 2,
		}, {
			title: 'Available',
			width: 1,
		}, {
			title: 'Publish',
			width: 1,
		}, {
			title: 'Overseas',
			width: 1,
		}, {
			title: 'Consign',
			width: 1,
		}]
	
		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				ReportService.getInventory({
					limit: this.state.count,
					offset: this.state.offset,
					search: this.state.search,
					sort_by_created_at: this.state.orderBy,
				}, this.state.token).then(res => {
					this.setState({
						isLoading: false,
						total: res.count,
						data: res.data,
					})
				}).catch(err => {
					this.setState({
						isLoading: false,
					}, () => {
						if (err && err.code === '031') {
							this.utilities.notification.show({
								title: 'Sorry',
								message: 'You\'ve been logged out. Please log in again.',
							})

							this.navigator.navigate('login')

							MeManager.logout();
						} else {
							this.props.utilities.notification.show({
								title: 'Oops',
								message: 'Something went wrong, please try again later',
							})
						}
					})
				})
			})
		}

		onAddNewOrder = () => {
			this.navigator.navigate('order/add')
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onChangeFilter = (key, val) => {
			if((key === 'date' || key === 'shipmentDate') && val) {
				this.setState({
					[key]: TimeHelper.moment(val).toISOString(),
					offset: 0,
				})
			} else {
				this.setState({
					[key]: val,
					offset: 0,
				})
			}
		}

		onChangeSendEmail = value => {
			this._shouldSendEmail = value
		}

		onChangeStatus = (order, key, value) => {
			this._shouldSendEmail = false

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="You might want to add a note for this change."
						placeholder="Input here"
						onCancel={ this.onModalRequestClose }
						onConfirm={ this.onConfirmChangeStatus.bind(this, order, key, value) }
					>
						<InputCheckboxBit value={ this._shouldSendEmail } title="Email customer about this change" onChange={ this.onChangeSendEmail } style={ Styles.input } />
					</ModalPromptPagelet>
				),
			})
		}

		onConfirmChangeStatus = (order, key, value, note) => {
			Promise.resolve().then(() => {
				switch(key) {
				case 'PAID':
					return OrderService.payOrder(order.id, {
						note,
						metadata: {},
						payment: 'SUCCESS',
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				case 'PROCESSING':
					return OrderService.processOrder(order.id, {
						note,
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				case 'RESOLVED':
					return OrderService.resolveOrder(order.id, {
						note,
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				case 'EXPIRED':
					return OrderService.expireOrder(order.id, {
						note,
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				case 'EXCEPTION':
					return OrderService.exceptOrder(order.id, {
						note,
						shouldSendEmail: this._shouldSendEmail,
					}, this.state.token)
				default:
					throw new Error('Cannot change to this status')
				}
			}).then(() => {
				this.utilities.notification.show({
					title: 'Status changed',
					message: `Successfully change status to ${value}`,
					type: this.utilities.notification.TYPES.SUCCESS,
				})

				order.status = key

				this.forceUpdate()
			}).catch(err => {
				this.warn(err)

				if(key === 'RESOLVED' && err && err.code === 'ERR_104') {
					this.utilities.alert.show({
						title: 'Are you sure?',
						message: 'This order still have unresolved order details. Continue?',
						actions: [{
							title: 'Yes',
							onPress: () => {
								return OrderService.resolveOrder(order.id, {
									note,
									shouldSendEmail: this._shouldSendEmail,
									force: true,
								}, this.state.token).then(() => {
									this.utilities.notification.show({
										title: 'Status changed',
										message: `Successfully change status to ${value}`,
										type: this.utilities.notification.TYPES.SUCCESS,
									})

									order.status = key

									this.forceUpdate()
								}).catch(_err => {
									this.warn(_err)

									this.utilities.notification.show({
										title: 'Error!',
										message: `Error when changing status, ${_err && _err.message || _err}`,
									})
								})
							},
							type: 'OK',
						}, {
							title: 'Cancel',
							onPress: this.onModalRequestClose,
							type: 'CANCEL',
						}],
					})
				} else {
					this.utilities.notification.show({
						title: 'Error!',
						message: `Error when changing status, ${err && err.message || err}`,
					})
				}
			})
		}

		onNavigateToOrder = orderId => {
			this.navigator.navigate(`order/${orderId}`)
		}

		onNavigateToOrderDetail = (id, type, refId) => {
			if(type === 'VOUCHER') {
				this.utilities.alert.modal({
					component: (
						<QuickViewVoucherPagelet
							id={ refId }
						/>
					),
				})
			} else {
				this.navigator.navigate(`${type.toLowerCase()}/${refId}`)
			}
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onNavigateToBack = () => {
			this.navigator.back()
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onSearchHandler = searchValue => {
			this.setState({
				search: searchValue,
				offset: 0,
			})
		}

		onChangeOrderBy = () => {
			this.setState({
				orderBy: this.state.orderBy === 'ASC' ? 'DESC' : 'ASC',
			})
		}

		onQuickView = orderId => {
			this.utilities.alert.modal({
				component: (
					<QuickViewOrderPagelet
						id={ orderId }
						onNavigateToOrder={ this.onNavigateToOrder.bind(this, orderId) }
						onNavigateToOrderDetail={ this.onNavigateToOrderDetail }
						onClose={ this.onModalRequestClose }
					/>
				),
			})
		}

		onNavigateToVariant = (variantId, productId) => {
			this.navigator.navigate(`product/${productId}/variant/${variantId}`)
		}

		rowRenderer = data => {
			switch(data.renderMode) {
			case 'ICON':
				return (
					<IconBit
						name={data.title === 'YES' ? 'circle-checkmark' : 'circle-null'}
						color={ data.title === 'YES' ? Colors.green.palette(2) : Colors.red.palette(7) }
						size={24}
						style={ Styles.data }

					/>
				)
			case 'BADGE':
				return (
					<SelectStatusBadgeLego
						type={ data.type }
						status={ data.title }
						disabled
					/>
				)
			case 'CLIPBOARD':
				return (
					<BoxBit>
						<ClipboardBit
							// autogrow={ 44 }
							value={ data.title }
							style={ Styles.data }
						/>
					</BoxBit>
				)
			case 'CLICKABLE':
				return (
					<TouchableBit onPress={ this.onNavigateToVariant.bind(this, data.title, data.productId) }>
						<GeomanistBit style={[Styles.data, Styles.underline]} type={ GeomanistBit.TYPES.PARAGRAPH_3 } align={ data.align } weight={ data.weight }>
							{ data.title }
						</GeomanistBit>
					</TouchableBit>
				)
			default:
				return (
					<GeomanistBit style={Styles.data} type={ GeomanistBit.TYPES.PARAGRAPH_3 } align={ data.align } weight={ data.weight }>
						{ data.title }
					</GeomanistBit>
				)
			}
			
		}

		listRenderer = (data, i) => {
			return (
				<BoxBit key={ i } unflex style={[Styles.row, i & 1 ? Styles.even : undefined]}>
					{
						i !== 0
							? this.rowRenderer(data)
							: (
								<GeomanistBit style={Styles.data} type={ GeomanistBit.TYPES.PARAGRAPH_3 } align={ data.align } weight={ i === 0 ? 'bold' : data.weight }>
									{ data.title }
								</GeomanistBit>
							)
					}
				</BoxBit>
			)
		}

		inventoryIdRenderer = () => {
			return (
				<TouchableBit onPress={ this.onChangeOrderBy }>
					{ ['Inventory ID', ...this.state.data.map(variant => variant.id)].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</TouchableBit>
			)
		}

		variantIdRenderer = () => {
			return (
				<BoxBit>
					{ ['Variant ID', ...this.state.data.map(variant => {return {variantId: variant.variant.id, productId: variant.variant.product.id}})].map((label, i) => {
						return {
							renderMode: 'CLICKABLE',
							title: i === 0 ? 'Variant ID' : label.variantId,
							productId: label.productId,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		skuRenderer = () => {
			return (
				<BoxBit>
					{ ['SKU', ...this.state.data.map(variant => variant.variant.sku ? variant.variant.sku : '-' )].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		urlRenderer = () => {
			return (
				<BoxBit>
					{ ['URL', ...this.state.data.map(variant => variant.variant.url ? variant.variant.url : '-' )].map(label => {
						return {
							renderMode: 'CLIPBOARD',
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		brandRenderer = () => {
			return (
				<BoxBit>
					{ ['Brand', ...this.state.data.map(variant => variant.variant.brand)].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		titleRenderer = () => {
			return (
				<BoxBit>
					{ ['Title', ...this.state.data.map(variant => variant.variant.product.title)].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		colorRenderer = () => {
			return (
				<BoxBit>
					{ ['Color', ...this.state.data.map(variant => {
						return variant.variant.colors.length > 1
							? `${variant.variant.colors[0].title}/${variant.variant.colors[1].title}`
							: `${variant.variant.colors[0].title}`
					})].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		sizeRenderer = () => {
			return (
				<BoxBit>
					{ ['Size', ...this.state.data.map(variant => variant.variant.variant_size)].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		statusRenderer = () => {
			return (
				<BoxBit>
					{ ['Status', ...this.state.data.map(variant => variant.status)].map(label => {
						return {
							renderMode: 'BADGE',
							type: SelectStatusBadgeLego.TYPES.INVENTORIES,
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		priceRenderer = () => {
			return (
				<BoxBit>
					{ ['Price', ...this.state.data.map(variant => FormatHelper.currency(variant.variant.price))].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}
		
		retailPriceRenderer = () => {
			return (
				<BoxBit>
					{ ['Retail Price', ...this.state.data.map(variant => FormatHelper.currency(variant.variant.retail_price))].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		purchasePriceRenderer = () => {
			return (
				<BoxBit>
					{ ['Purchase Price', ...this.state.data.map(variant => FormatHelper.currency(variant.purchase_price))].map(label => {
						return {
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		isAvailableRenderer = () => {
			return (
				<BoxBit>
					{ ['Available', ...this.state.data.map(variant => variant.variant.is_available ? 'YES' : 'NO')].map(label => {
						return {
							renderMode: 'ICON',
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		isPublishedRenderer = () => {
			return (
				<BoxBit>
					{ ['Published', ...this.state.data.map(variant => variant.variant.is_published ? 'YES' : 'NO')].map(label => {
						return {
							renderMode: 'ICON',
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		consignmentStockRenderer = () => {
			return (
				<BoxBit>
					{ ['Have Consignment', ...this.state.data.map(variant => variant.variant.have_consignment_stock ? 'YES' : 'NO')].map(label => {
						return {
							renderMode: 'ICON',
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		isOverseasRenderer = () => {
			return (
				<BoxBit>
					{ ['Available', ...this.state.data.map(variant => variant.variant.overseas ? 'YES' : 'NO')].map(label => {
						return {
							renderMode: 'ICON',
							title: label,
							weight: 'medium',
						}
					}).map(this.listRenderer) }
				</BoxBit>
			)
		}

		tableRenderer = () => {
			return (
				<ScrollViewBit unflex horizontal>
					{this.inventoryIdRenderer()}
					{this.variantIdRenderer()}
					{this.statusRenderer()}
					{this.skuRenderer()}
					{this.urlRenderer()}
					{this.brandRenderer()}
					{this.titleRenderer()}
					{this.colorRenderer()}
					{this.sizeRenderer()}
					{this.priceRenderer()}
					{this.retailPriceRenderer()}
					{this.purchasePriceRenderer()}
					{this.isAvailableRenderer()}
					{this.isPublishedRenderer()}
					{this.isOverseasRenderer()}
					{this.consignmentStockRenderer()}
				</ScrollViewBit>
			)
		}

		loadingRenderer = () => {
			return (
				<BoxBit centering>
					<LoaderBit simple/>
				</BoxBit>
			)
		}

		view() {
			return (
				<PageAuthorizedComponent
					// roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Inventory Report' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Inventory Report"
								// paths={[{ title: '' }]}
								search={ this.state.search }
								searchPlaceholder="Brand or Product Title"
								// buttonPlaceholder="Create New Order"
								onSearch={ this.onSearchHandler }
								// onPress={ this.onAddNewOrder }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							/>
							{
								this.state.isLoading
									? this.loadingRenderer()
									: this.tableRenderer()
							}
						
							{/* <TableLego
								compact
								isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={ Styles.padder }
							/> */}
							{/* <HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							/> */}
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
