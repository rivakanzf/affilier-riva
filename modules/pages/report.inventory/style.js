import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: 32,
		paddingRight: 32,
	},

	padder: {
		marginBottom: 16,
	},

	capital: {
		textTransform: 'capitalize',
	},

	quickViewHeader: {
		width: 20,
	},

	input: {
		marginTop: 8,
	},

	id: {
		color: Colors.primary,
	},

	row: {
		paddingTop: 14,
		paddingBottom: 14,
		paddingLeft: 12,
		paddingRight: 12,
		height: 70,
	},

	even: {
		backgroundColor: Colors.solid.grey.palette(3),
	},

	data: {
		whiteSpace: 'nowrap',
  		overflow: 'hidden',
		marginTop: 'auto',
		marginBottom: 'auto',
	},

	underline: {
		textDecoration: 'underline',
	},
})
