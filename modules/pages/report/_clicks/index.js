import React from 'react'
import StatefulModel from 'coeur/models/stateful'
import ConnectHelper from 'coeur/helpers/connect'
import TimeHelper from 'coeur/helpers/time'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'
import InputDateBit from 'modules/bits/input.date'
import TextInputBit from 'modules/bits/text.input'

import BoxRowLego from 'modules/legos/box.row'
import TableLego from 'modules/legos/table'

import Styles from './style'

const data = [{
	id: 1,
	timestamp: '2021-09-16T06:50:11.628Z',
	region: 'Indonesia',
	tags: [
		'sale',
		'recommend',
	],
	url: 'https://helloyuna.io/product/variant/14045?tr_source=affiliate&affiliate=40230&tag_link1=sale&tag_link2=recommend',
}, {
	id: 2,
	timestamp: '2021-09-16T04:07:28.154Z',
	region: 'Indonesia',
	tags: [],
	url: 'https://helloyuna.io/product/variant/14044?tr_source=affiliate&affiliate=40230',
}, {
	id: 3,
	timestamp: '2021-09-10T16:52:26.962Z',
	region: 'Indonesia',
	tags: [
		'skirt',
	],
	url: 'https://helloyuna.io/product/variant/14044?tr_source=affiliate&affiliate=40230&tag_link1=skirt',
}]

export default ConnectHelper(
	class ClickPart extends StatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				timestamp: PropTypes.date,
				region: PropTypes.string,
				tags: PropTypes.arrayOf(PropTypes.string),
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				data: [],
				tags: '',
			})
		}

		header = [{
			title: 'ID',
			width: .2,
		}, {
			title: 'URL',
			width: 2.5,
		}, {
			title: 'Waktu Klik',
			width: 1,
		}, {
			title: 'Wilayah Klik',
			width: 1,
		}, {
			title: 'Tags',
			width: 1.5,
		}]
		
		componentDidMount() {
			this.getData()
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				this.setState({
					data: data,
					isLoading: false,
				})
			})
		}

		onChangeFromDate = val => {
			this.log(val)
		}

		onChangeToDate = val => {
			this.log(val)
		}

		onChangeId = (e, val) => {
			this.log(val)
		}

		onChangeTags = (e, val) => {
			this.log(val)
		}

		onChangeRegion = (e, val) => {
			this.log(val)
		}

		onSearch = () => {
			this.log('Search in database')
		}

		rowRenderer = history => {
			return {
				id: history.id,
				data: [{
					title: history.id,
				}, {
					title: history.url,
				}, {
					title: `${TimeHelper.format(history.timestamp, 'DD MMMM YYYY HH:mm')}`,
				}, {
					title: history.region,
				}, {
					title: history.tags.map(tags => tags + ', '),
				}],
			}
		}

		tagsRenderer = tag => {
			this.setState(state => ({
				tags: state.tags + ', ' + tag,
			}))

			return this.state.tags
		}

		clickHistoryRenderer = () => {
			return (
				<TableLego
					key={'Clicks'}
					isLoading={this.state.isLoading}
					headers={this.header}
					rows={this.state.data.map(this.rowRenderer)}
				/>
			)
		}

		view() {
			return (
				<BoxRowLego
					title="Click Report"
					data={[{
						data: [{
							title: 'Waktu Klik',
							children: (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										Dari
									</GeomanistBit>
									<InputDateBit
										date={null}
										onChange={this.onChangeFromDate}
									/>
								</BoxBit>
							),
						}, {
							title: ' ',
							children: (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										Sampai
									</GeomanistBit>
									<InputDateBit
										date={null}
										onChange={this.onChangeToDate}
									/>
								</BoxBit>
							),
						}],
					}, {
						data: [{
							title: 'Click ID',
							children: (
								<TextInputBit
									type={TextInputBit.TYPES.TEXT}
									placeholder="Search based on click id"
									onChange={this.onChangeId}
								/>
							),
						}],
					}, {
						data: [{
							title: 'Tag Link',
							children: (
								<TextInputBit
									type={TextInputBit.TYPES.TEXT}
									placeholder="Search based on tag link"
									onChange={this.onChangeTags}
								/>
							),
							description: 'Use commas to input several tags',
						}],
					}, {
						data: [{
							title: 'Region',
							children: (
								<TextInputBit
									type={TextInputBit.TYPES.TEXT}
									placeholder="Search based on region"
									onChange={this.onChangeRegion}
								/>
							),
						}],
					}, {
						data: [{
							title: '',
							children: (
								<ButtonBit
									weight="medium"
									title="Search"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={ButtonBit.TYPES.NORMAL}
									onPress={this.onSearch}
								/>
							),
						}],
					}, {
						data: [{
							title: ' ',
							children: (
								<>
									{this.clickHistoryRenderer()}
								</>
							),
						}],
					}]}
				/>
			)
		}
	}
)
