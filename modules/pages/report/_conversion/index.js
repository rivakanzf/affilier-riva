import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import PromiseStatefulModel from 'coeur/models/promise.stateful'
import TimeHelper from 'coeur/helpers/time'
import FormatHelper from 'coeur/helpers/format'

import BoxBit from 'modules/bits/box'
import ButtonBit from 'modules/bits/button'
import GeomanistBit from 'modules/bits/geomanist'
import InputDateBit from 'modules/bits/input.date'
import TextInputBit from 'modules/bits/text.input'

import BoxRowLego from 'modules/legos/box.row'
import TableLego from 'modules/legos/table'

import Styles from './style'

const data = [{
	timestamp: '2021-09-16T06:50:11.628Z',
	number: 'YCO2109160015006',
	product: {
		id: 13764,
		brand: 'Modemaya',
		title: 'Olive Shirt',
		price: [269910, 299900],
		commission: 20000,
	},
	quantity: 1,
}, {
	timestamp: '2021-09-16T04:07:28.154Z',
	number: 'YCO2109160010704',
	product: {
		id: 12347,
		brand: 'RAMUNE',
		title: 'Chaerin Frill Top',
		price: [191400, 319000],
		commission: 15000,
	},
	quantity: 3,
}, {
	timestamp: '2021-09-10T16:52:26.962Z',
	number: 'YCO2109100015216',
	product: {
		id: 14660,
		brand: 'RAMUNE',
		title: 'Haru Loose Top',
		price: [129000, 249000],
		commission: 18500,
	},
	quantity: 7,
}]

export default ConnectHelper(
	class ConversionPart extends PromiseStatefulModel {
		
		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				timestamp: PropTypes.date,
				productId: PropTypes.id,
				productBrand: PropTypes.string,
				productName: PropTypes.string,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				data: [],
			})
		}

		header = [{
			title: 'Waktu Pembelian',
			width: 2,
		}, {
			title: 'No Order',
			width: 2,
		}, {
			title: 'Produk',
			width: 2,
		}, {
			title: 'Quantity',
			width: 1,
		}, {
			title: 'Total Price',
			width: 1,
		}, {
			title: 'Total Commission',
			width: 1.3,
		}]

		componentDidMount() {
			this.getData()
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				this.setState({
					data: data,
					isLoading: false,
				})
			})
		}

		onChangeFromDate = (val) => {
			this.log(val)
		}

		onChangeToDate = val => {
			this.log(val)
		}

		onChangeId = (e, val) => {
			this.log(val)
		}

		onSearch = () => {
			this.log('Search in database')
		}

		rowRenderer = history => {
			return {
				id: history.product.id,
				data: [{
					title: `${TimeHelper.format(history.timestamp, 'DD MMMM YYYY HH:mm')}`,
				}, {
					title: history.number,
				}, {
					children: (
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								{history.product.brand} - {history.product.title}
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Product Id: {history.product.id}
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Commission: IDR {FormatHelper.currency(history.product.commission)}
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: history.quantity,
				}, {
					title: `IDR ${FormatHelper.currency(history.quantity * history.product.price[0])}`,
				}, {
					title: `IDR ${FormatHelper.currency(history.quantity * history.product.commission)}`,
				}],
			}
		}

		conversionHistoryRenderer = () => {
			return (
				<TableLego
					key={'Conversion'}
					isLoading={this.state.isLoading}
					headers={this.header}
					rows={this.state.data.map(this.rowRenderer)}
				/>
			)
		}

		view() {
			return (
				<BoxRowLego
					title="Conversion Report"
					data={[{
						data: [{
							title: 'Waktu Pembelian',
							children: (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										Dari
									</GeomanistBit>
									<InputDateBit
										date={null}
										onChange={this.onChangeFromDate}
									/>
								</BoxBit>
							),
						}, {
							title: ' ',
							children: (
								<BoxBit>
									<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
										Sampai
									</GeomanistBit>
									<InputDateBit
										date={null}
										onChange={this.onChangeToDate}
									/>
								</BoxBit>
							),
						}],
					}, {
						data: [{
							title: 'Product Id',
							children: (
								<TextInputBit
									type={TextInputBit.TYPES.TEXT}
									placeholder="Search based on product id"
									onChange={this.onChangeId}
								/>
							),
						}],
					}, {
						data: [{
							title: '',
							children: (
								<ButtonBit
									weight="medium"
									title="Search"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={ButtonBit.TYPES.NORMAL}
									onPress={this.onSearch}
								/>
							),
						}],
					}, {
						data: [{
							title: 'History',
							children: (<>
								{this.conversionHistoryRenderer()}
							</>),
						}],
					}]}
				/>
			)
		}
	}
)
