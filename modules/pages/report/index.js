import React from 'react'
import PageModel from 'coeur/models/page'
import ConnectHelper from 'coeur/helpers/connect'

import BoxBit from 'modules/bits/box'
import LoaderBit from 'modules/bits/loader'

import PageAuthorizedComponent from 'modules/components/page.authorized'
import HeaderPageComponent from 'modules/components/header.page'

import TabLego from 'modules/legos/tab'

import ClickPart from './_clicks'
import ConversionPart from './_conversion'

import Styles from './style'

export default ConnectHelper(
	class ReportPage extends PageModel {
		static routeName = 'report'
		
		constructor(p) {
			super(p, {
				isLoading: false,
				tabIndex: p.tabIndex || 0,
			})
		}

		roles = ['stylist', 'headstylist', 'analyst', 'finance', 'fulfillment', 'purchasing']

		tabs = [{
			title: 'Clicks Report',
			icon: 'track',
			onPress: this.onChangeTab.bind(this, 0),
		}, {
			title: 'Conversion Report',
			icon: 'quick-view',
			onPress: this.onChangeTab.bind(this, 1),
		}]

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			})
		}

		contentRenderer = index => {
			switch(index) {
			case 0:
			default:
				return (
					<ConversionPart/>
				)
			case 1:
				return (
					<ClickPart/>
				)
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={this.roles}
					paths={[{ title: 'Report' }]}
				>

					{() => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent
								title="Report"
							/>
							<TabLego activeIndex={this.state.tabIndex} tabs={this.tabs} style={Styles.tab}/>
							{ this.state.isLoading ? (
								<BoxBit centering>
									<LoaderBit simple/>
								</BoxBit>
							) : this.contentRenderer(this.state.tabIndex) }
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
