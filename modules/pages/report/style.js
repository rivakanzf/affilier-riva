import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingLeft: 32,
		paddingRight :32,
		paddingBottom: 16,
		paddingTop: 16,
	},
	
})
