/* eslint-disable no-nested-ternary */
import React from 'react';
import PromiseStatefulModel from 'coeur/models/promise.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import Colors from 'utils/constants/color'

import RequestService from 'app/services/request';
import UserService from 'app/services/user.new'

import BoxBit from 'modules/bits/box';
// import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
// import LoaderBit from 'modules/bits/loader';
import NavTextTitleBit from 'modules/bits/nav.text.title';
import InputFileBit from 'modules/bits/input.file'
import LoaderBit from 'modules/bits/loader'
import ImageBit from 'modules/bits/image'
import TouchableBit from 'modules/bits/touchable'

import RowsLego from 'modules/legos/rows';
import BadgeStatusLego from 'modules/legos/badge.status';
import LoaderLego from 'modules/legos/loader';
import SelectStylistLego from 'modules/legos/select.stylist';

// import AddressesComponent from 'modules/components/addresses';

import ModalClosableConfirmationPagelet from 'modules/pagelets/modal.closable.confirmation';
import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import ImagePagelet from 'modules/pagelets/image'

import Styles from './style';


export default ConnectHelper(
	class QuickViewPart extends PromiseStatefulModel {
		static propTypes(PropTypes) {
			return {
				id: PropTypes.number,
				onUpdateStatus: PropTypes.func,
				onNavigateToClientDetail: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToPromise(state, p) {
			return RequestService.getDetail(p.id, state.me.token)
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isLoading: false,
				stylist_id: undefined,
				image: null,
				isUploading: false,
			})
		}

		transform = { original: true }


		type = type => {
			switch (type) {
			case 'WITHDRAW':
				return 'Withdraw'
			case 'CHANGE_STYLIST':
				return 'Change Stylist'
			default:
				return type
			}
		}

		onClose = () => {
			this.props.utilities.alert.hide()
		}

		onChangeStylist = id => {
			this.state.stylistId = id
		}

		onRejection = () => {
			this.props.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why you rejected this request (required)."
						onCancel={ this.onClose }
						onConfirm={ this.onConfirmRejection }
					/>
				),
			})
		}

		onApproval = () => {

			if(this.props.data.type === 'WITHDRAW' && (!this.state.image || this.state.isUploading) ) {
				this.props.utilities.notification.show({
					message: 'Tolong upload bukti transfer untuk melanjutkan',
					type: this.props.utilities.notification.TYPES.WARNING,
				})
			} else {
				this.props.utilities.alert.modal({
					component: (
						<ModalPromptPagelet
							title="Add note"
							message="You might want to add note regarding this approval"
							onCancel={ this.onClose }
							onConfirm={ this.onConfirmApproval }
						/>
					),
				})
			}
			
		}

		onConfirmRejection = note => {
			this.setState({
				isLoading: true,
			}, () => {
				return RequestService.reject(this.props.id, note, this.props.token).then(() => {
					this.setState({
						isLoading: false,
					})

					this.onClose()

					this.props.utilities.notification.show({
						message: 'Request rejected.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.props.onUpdateStatus &&
					this.props.onUpdateStatus('FAILED')
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					})

					this.props.utilities.notification.show({
						message: err ? err.detail || err.message || err : 'Oops… Something went wrong',
					})
				})
			})
		}

		onConfirmApproval = note => {
			this.setState({
				isLoading: true,
			}, () => {
				return Promise.resolve().then(() => {
					switch(this.props.data.type) {
					case 'WITHDRAW':
						return RequestService.approveWithdraw(this.props.id, note, this.state.image.id, this.props.token)
					case 'CHANGE_STYLIST':
						if(this.state.stylistId) {
							return RequestService.approveChange(this.props.id, this.state.stylistId, note, this.props.token)
						} else {
							throw new Error('Replacement stylist not selected')
						}
					default:
						throw new Error('Type invalid')
					}
				}).then(() => {
					this.setState({
						isLoading: false,
					})

					this.onClose()

					this.props.utilities.notification.show({
						message: 'Yeay… Success approving request.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.props.onUpdateStatus &&
					this.props.onUpdateStatus('SUCCESS')
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					})

					this.props.utilities.notification.show({
						message: err ? err.detail || err.message || err : 'Oops… Something went wrong',
					})
				})
			})
		}

		onNavigateToClientDetail = href => {
			this.onClose()

			this.props.onNavigateToClientDetail &&
			this.props.onNavigateToClientDetail(href)
		}

		onDataLoaded = (id, image) => {
			if(id === 'loader' && image === undefined) {
				this.setState({image: undefined})
			} else if(id === 'adder') {
				this.setState({isUploading: true}, () => {
					UserService.upload(this.props.id, {
						image,
					}, this.props.token).then(res => {
						this.setState({image: res, isUploading: false})
					})
				})
			}

			// if(id === 'loader' && image === undefined) {
			// 	this.setState({image: undefined}, () => {
			// 		this.props.onImageRemoved()
			// 	})
			// } else if(id === 'adder') {
			// 	this.setState({isUploading: true}, () => {
			// 		UserService.upload(this.props.id, {
			// 			image,
			// 		}, this.props.token).then(res => {
	
			// 			this.props.onImageLoaded
			// 			&& this.props.onImageLoaded(res)
	
			// 			this.setState({image: res.url, isUploading: false})
			// 		})
			// 	})
			// }
		}

		onImagePopup = () => {
			this.props.utilities.alert.modal({
				component: (
					<ImagePagelet source={`//${this.props.data.receipt.url}`} onPress={this.onModalRequestClose} transform={this.transform}/>
				),
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		typeRenderer = type => {
			switch (type) {
			case 'WITHDRAW':
				return (
					<BoxBit unflex style={ Styles.content }>
						<RowsLego
							data={[{
								data: [{
									title: 'Withdraw Amount',
									content: `IDR ${FormatHelper.currency( this.props.data.metadata.amount )}`,
								}],
							}, {
								data: [{
									title: 'Bank',
									content: this.props.data.banks && this.props.data.banks[0] && this.props.data.banks[0].name,
								}],
							}, {
								data: [{
									title: 'Account Name',
									content: this.props.data.banks && this.props.data.banks[0] && this.props.data.banks[0].account_name,
								}, {
									title: 'Account Number',
									content: this.props.data.banks && this.props.data.banks[0] && this.props.data.banks[0].account_number,
								}],
							}, {
								data: [{
									title: 'Branch',
									content: this.props.data.banks && this.props.data.banks[0] && this.props.data.banks[0].branch,
								}, {
									title: 'City',
									content: this.props.data.banks && this.props.data.banks[0] && this.props.data.banks[0].city,
								}],
							},
							{
								data:[{
									title: 'Bukti Transfer',
									children: (
										this.props.data.status === 'SUCCESS' && this.props.data.receipt

											? <TouchableBit unflex onPress={this.onImagePopup}>
												<ImageBit source={`//${this.props.data.receipt.url}`}/>
											</TouchableBit>
											: this.state.isUploading
												? <LoaderBit/>
												: <InputFileBit
													id={this.state.image ? 'loader' : 'adder'}
													maxSize={3}
													value={ this.state.image && `//${this.state.image.url}` }
													onDataLoaded={this.onDataLoaded}
												/>
									),

								}],

							}]}
						/>
					</BoxBit>
				)
			case 'CHANGE_STYLIST':
				return (
					<BoxBit unflex style={ Styles.content }>
						<RowsLego
							data={[{
								data: [{
									title: 'Stylist',
									children: (
										<SelectStylistLego stylist={ this.props.data.stylist && this.props.data.stylist.name } onChange={ this.onChangeStylist } />
									),
								}, {
									blank: true,
								}],
							}]}
						/>
					</BoxBit>
				)
			default:
				return false
			}
		}

		viewOnError() {
			return (
				<BoxBit row centering style={Styles.empty}>
					<IconBit name="circle-info" color={ Colors.primary } />
					<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3} style={Styles.error}>Something went wrong when loading the data</GeomanistBit>
				</BoxBit>
			)
		}

		viewOnLoading() {
			return (
				<LoaderLego />
			)
		}

		view() {
			return this.state.isLoading ? this.viewOnLoading() : (
				<ModalClosableConfirmationPagelet
					title={ `Request #RE-${ this.props.id }` }
					header={( <BadgeStatusLego status={this.props.data.status} /> )}
					confirm="Approve"
					cancel="Reject"
					onClose={ this.onClose }
					onConfirm={ this.props.data.status === 'PENDING' ? this.onApproval : undefined }
					onCancel={ this.props.data.status === 'PENDING' ? this.onRejection : undefined }
					contentContainerStyle={ Styles.container }
				>
					<BoxBit unflex type={ BoxBit.TYPES.THIN }>
						<RowsLego
							data={[{
								data: [{
									title: 'Requested by',
									children: (
										<NavTextTitleBit
											title={ this.props.data.user_name }
											description={ this.props.data.user_email }
											link={ 'View Client' }
											href={ `client/${ this.props.data.user_id }` }
											onPress={ this.onNavigateToClientDetail }
										/>
									),
								}, {
									title: 'Request Date',
									content: TimeHelper.format(this.props.data.created_at, 'DD MMMM YYYY HH:mm'),
								}],
							}, {
								data: [{
									title: 'Type',
									content: this.type(this.props.data.type),
								}, {
									title: 'Reason',
									content: this.props.data.reason || this.props.data.metadata.reason || '-',
								}, {
									title: 'Note',
									content: this.props.data.note || this.props.data.metadata.note || '-',
								}],
							}]}
						/>
						{ this.typeRenderer( this.props.data.type ) }
					</BoxBit>
				</ModalClosableConfirmationPagelet>
			)
		}
	}
)
