import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		// paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default * 1.5,
		paddingLeft: 0,
		paddingRight: 0,
	},

	empty: {
		paddingTop: Sizes.margin.default,
		minHeight: 300,
	},

	error: {
		color: Colors.primary,
		marginLeft: 8,
	},

	content: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: Colors.black.palette(2, .16),
		marginTop: Sizes.margin.default,
		// paddingLeft: Sizes.margin.default,
		// paddingRight: Sizes.margin.default,
		// paddingBottom: Sizes.margin.default,
		// marginLeft: -Sizes.margin.default,
		// marginRight: -Sizes.margin.default,
		// backgroundColor: Colors.grey.palette(2),
	},

	actions: {},

	button: {
		marginRight: 8,
	},

})
