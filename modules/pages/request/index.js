import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';
import AuthenticationHelper from 'utils/helpers/authentication'

import RequestService from 'app/services/request';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
// import NavTextTitleBit from 'modules/bits/nav.text.title';

import BadgeStatusLego from 'modules/legos/badge.status';
import SelectStatusLego from 'modules/legos/select.status'
import TableLego from 'modules/legos/table';

import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import QuickViewPart from './_quick.view'

import Styles from './style';

import { isEqual } from 'lodash';


export default ConnectHelper(
	class RequestPage extends PageModel {

		static routeName = 'request'

		static stateToProps(state) {
			return {
				isSuperAdmin: AuthenticationHelper.isAuthorized(state.me.roles, 'superadmin'),
				isFinance: AuthenticationHelper.isAuthorized(state.me.roles, 'finance'),
				// isAnalyst: AuthenticationHelper.isAuthorized(state.me.roles, 'analyst'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				offset: 0,
				count: 160,
				total: 0,
				search: p.search || undefined,
				requests: [],
				filter: {
					type: undefined,
					status: 'PENDING',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		roles = ['headstylist', 'finance']

		tableHeader = [{
			title: 'Request ID',
			width: .8,
		}, {
			title: 'Requestor',
			width: 1.5,
		}, {
			title: 'Email',
			width: 2,
		}, {
			title: 'Type',
			width: 1.5,
		}, {
			title: 'Status',
			width: 1,
		}, {
			title: 'Req. Date',
			width: 1.1,
		}, {
			title: '',
			width: .5,
			align: 'right',
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token && (
					!isEqual(pS.offset, this.state.offset)
					|| !isEqual(pS.search, this.state.search)
					|| !isEqual(pS.filter.type, this.state.filter.type)
					|| !isEqual(pS.filter.status, this.state.filter.status)
				)
			) {
				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				RequestService.getRequests({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(filter.type ? {
						// eslint-disable-next-line no-nested-ternary
						type: this.props.isSuperAdmin
							? filter.type
							: this.props.isFinance
								? 'WITHDRAW'
								: 'CHANGE_STYLIST',
					} : {}),
					...(filter.status !== '' ? { status: filter.status } : {}),
				}, this.state.token).then(res => {
					if(id === this.getterId) {
						this.setState({
							isLoading: false,
							total: res.count,
							requests: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		type = type => {
			switch(type) {
			case 'WITHDRAW':
				return 'Withdraw'
			case 'CHANGE_STYLIST':
				return 'Change Stylist'
			default:
				return type
			}
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			this.setState({
				offset: 0,
				filter: {
					...this.state.filter,
					[key]: val,
				},
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToDetail = request => {
			this.utilities.alert.modal({
				component: (
					<QuickViewPart
						id={ request.id }
						onUpdateStatus={ status => {
							request.status = status
							this.forceUpdate()
						} }
						onNavigateToClientDetail={ href => {
							this.navigator.navigate(href)
						} }
					/>
				),
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		rowRenderer = request => {
			return {
				data: [{
					title: `#RE-${request.id}`,
				}, {
					title: request.user_name,
				}, {
					title: request.user_email,
				}, {
					title: this.type(request.type),
				}, {
					children: (
						<BoxBit row style={ Styles.status }>
							<BadgeStatusLego unflex status={ request.status } />
						</BoxBit>
					),
				}, {
					title: TimeHelper.format(request.created_at, 'DD MMM YYYY HH:mm'),
				}, {
					children: (
						<BoxBit style={ Styles.right }>
							<IconBit name="quick-view" color={ Colors.primary } />
						</BoxBit>
					),
				}],
				onPress: this.onGoToDetail.bind(this, request),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Requests' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Requests"
								searchPlaceholder={ 'ID or Requestor' }
								search={ this.state.search }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								<SelectStatusLego type={ SelectStatusLego.TYPES.CODES }
									status={ this.state.filter.status }
									onChange={ this.onChangeFilter.bind(this, 'status') }
								/>
								{ this.props.isSuperAdmin ? (
									<SelectStatusLego type={ SelectStatusLego.TYPES.USER_REQUESTS }
										placeholder={ 'Select type' }
										status={ this.state.filter.type }
										onChange={ this.onChangeFilter.bind(this, 'type') }
									/>
								) : (
									<BoxBit />
								) }
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.requests.map(this.rowRenderer) }
								style={ Styles.padder } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange } />
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
