import React from 'react';
import PageModel from 'coeur/models/page';
import Colors from 'coeur/constants/color';
import TimeHelper from 'coeur/helpers/time';
import ConnectHelper from 'coeur/helpers/connect';

import OrderDetailService from 'app/services/order.detail';
import ShipmentService from 'app/services/shipment';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import ButtonBit from 'modules/bits/button';
import CheckboxBit from 'modules/bits/checkbox';
import GeomanistBit from 'modules/bits/geomanist';
import InputFileCustomBit from 'modules/bits/input.file.custom';
// import NavTextBit from 'modules/bits/nav.text';
import NavTextTitleBit from 'modules/bits/nav.text.title';
import TouchableBit from 'modules/bits/touchable';

import TableLego from 'modules/legos/table';
// import BadgeStatusLego from 'modules/legos/badge.status';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import AddressUpdatePagelet from 'modules/pagelets/address.update';
import QuickViewItemPagelet from 'modules/pagelets/quick.view.item';
import QuickViewStylesheetPagelet from 'modules/pagelets/quick.view.stylesheet';

import Styles from './style';

import { without } from 'lodash'


export default ConnectHelper(
	class ShipmentPage extends PageModel {

		static routeName = 'shipment'

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				isUploading: false,
				uploader: 1,
				count: 20,
				offset: 0,
				total: 0,
				search: p.search || null,
				status: null,
				data: [],
				selectedIds: [],

				token: undefined,
			})
		}

		tableHeader = [{
			title: '',
			width: .5,
		}, {
			title: 'Order Number',
			width: 2,
		}, {
			title: 'Address',
			width: 3,
		}, {
			title: 'Shipment Date',
			width: 1,
		}, {
			width: .5,
			align: 'right',
		}]

		componentDidUpdate(pP, pS) {
			if(
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		getData = () => {
			const {
				offset,
				count,
				search,
			} = this.state

			this.setState({
				isLoading: true,
			}, () => {
				OrderDetailService.getPacked({
					offset,
					search,
					limit: count,
					status: 'PRIMED',
				}, this.state.token)
					.then(res => {
						this.setState({
							isLoading: false,
							data: res.data,
							total: res.count,
						})
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops…',
								message: err ? err.detail || err.message || err : 'Something went wrong.',
							})
						})
					})
			})
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onNavigateToBack = () => {
			this.navigator.back()
		}

		onNavigateToStylesheet = id => {
			this.onModalRequestClose()
			this.navigator.navigate(`stylesheet/${ id }`)
		}

		onNavigate = href => {
			this.navigator.navigate(href)
		}

		onChangeFilter = (key, val) => {
			this.setState({
				[key]: val,
				offset: 0,
			})
		}

		onSelect = id => {
			if (this.state.selectedIds.indexOf(id) > -1) {
				this.setState({
					selectedIds: without(this.state.selectedIds, id),
				})
			} else {
				this.setState({
					selectedIds: [...this.state.selectedIds, id],
				})
			}
		}

		onQuickView = (type, id) => {
			switch(type) {
			case 'STYLESHEET':
				this.utilities.alert.modal({
					component: (
						<QuickViewStylesheetPagelet
							id={ id }
							onNavigate={ this.onNavigate }
						/>
					),
				})
				break
			case 'INVENTORY':
				this.utilities.alert.modal({
					component: (
						<QuickViewItemPagelet
							type={ 'INVENTORY' }
							id={ id }
							onClose={ this.onModalRequestClose }
						/>
					),
				})
				break
			}
		}

		onEditAddress = detail => {
			this.utilities.alert.modal({
				component: (
					<AddressUpdatePagelet
						id={ detail.address.id }
						type={ 'order' }
						typeId={ detail.order_id }
						onClose={ this.onModalRequestClose }
						onConfirm={ this.onUpdateAddress.bind(this, detail) }
					/>
				),
			})
		}

		onUpdateAddress = (detail, address) => {
			detail.address = address

			this.onModalRequestClose()

			this.forceUpdate()
		}

		onShip = () => {
			this.utilities.menu.show({
				title: 'Select Shipping Method',
				actions: [],
			})
		}

		onCreateMultipleShipments = method => {
			// Address is order address
			this.onModalRequestClose()

			// this.props.utilities.alert.modal({
			// 	component: (
			// 		<ShipmentUpdatePagelet
			// 			method={method}
			// 			orderId={this.props.orderId}
			// 			shipment={{
			// 				destination: address,
			// 				orderDetails: selectedIds.map(id => this.props.details.find(d => d.id === id)),
			// 			}}
			// 			onShipmentCreated={this.onShipmentCreated}
			// 		/>
			// 	),
			// })
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onSearchHandler = searchValue => {
			this.setState({
				search: searchValue,
				offset: 0,
			})
		}

		onUploadDataLoaded = (id, data) => {
			this.setState({
				isUploading: true,
			}, () => {
				ShipmentService.uploadCSV( data, this.props.token ).then(() => {
					this.setState({
						isUploading: false,
						uploader: this.state.uploader + 1,
					})

					this.utilities.notification.show({
						message: 'Success uploading CSV. Reloading',
					})

					// window.location.reload()

				}).catch(err => {
					this.warn(err)

					this.setState({
						isUploading: false,
						uploader: this.state.uploader + 1,
					})

					this.utilities.notification.show({
						message: err ? err.detail || err.message || err : 'Oops… Something went wrong. Please try again later.',
					})
				})
			})
		}

		onUploadDataError = () => {
			this.setState({
				uploader: this.state.uploader + 1,
			})
		}

		rowRenderer = detail => {
			return {
				data: [{
					children: (
						<CheckboxBit dumb
							isActive={ this.state.selectedIds.indexOf(detail.id) > -1 }
							onPress={ this.onSelect.bind(this, detail.id) }
						/>
					),
				}, {
					children: (
						<NavTextTitleBit
							title={ detail.order_number }
							description={ `[#${ detail.type.substr(0, 2) }-${ detail.ref_id }] ${ detail.title }` }
							link={ 'View Order' }
							href={ `order/${ detail.order_id }` }
						/>
					),
				}, {
					children: (
						<NavTextTitleBit
							title={ `${ detail.address.receiver } (${ detail.address.phone })` }
							description={ `${ detail.address.address } – ${ detail.address.district } – ${ detail.address.postal }` }
							link={ 'Edit Address' }
							href={ `order/${ detail.order_id }` }
							onPress={ this.onEditAddress.bind(this, detail) }
						/>
					// 	<BoxBit unflex>
					// 		<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } weight="semibold">
					// 			{ detail.address.receiver } ({ detail.address.phone })
					// 		</GeomanistBit>
					// 		<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.address }>
					// 			{ detail.address.address } – { detail.address.district } – { detail.address.postal }
					// 		</GeomanistBit>
					// 	</BoxBit>
					),
				}, {
					title: '-',
					children: detail.shipment_at ? (
						<BoxBit unflex>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.format(detail.shipment_at, 'DD MMM YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 } style={ Styles.note }>
								{ `– ${TimeHelper.moment(detail.shipment_at).fromNow()}` }
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					children: (
						<TouchableBit unflex style={ Styles.quickview } onPress={ this.onQuickView.bind(this, detail.type, detail.ref_id) }>
							<IconBit
								name="quick-view"
								size={20}
								color={Colors.primary}
							/>
						</TouchableBit>
					),
				}],
				onPress: this.onSelect.bind(this, detail.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles="fulfillment"
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Shipments' }, { title: 'Create' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Create Shipments"
								search={ this.state.search }
								searchPlaceholder="Order Number"
								onSearch={ this.onSearchHandler }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }
							>
								<ButtonBit
									title="Ship"
									type={ButtonBit.TYPES.SHAPES.PROGRESS}
									state={ this.state.selectedIds.length ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
									onPress={ this.onShip }
								/>
								<InputFileCustomBit
									key={ this.state.uploader }
									title="Update via CSV"
									loading={ this.state.isUploading }
									onDataLoaded={ this.onUploadDataLoaded }
									onDataError={ this.onUploadDataError }
								/>
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.data.map(this.rowRenderer) }
								style={Styles.padder}
							/>
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange}
							/>
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
