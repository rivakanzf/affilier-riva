import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: 32,
		paddingRight: 32,
	},
	id: {
		color: Colors.primary,
	},
	padder: {
		marginBottom: 16,
	},
	note: {
		color: Colors.black.palette(2, .6),
	},
	address: {
		whiteSpace: 'nowrap',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},
	quickview: {
		alignItems: 'flex-end',
	},
})
