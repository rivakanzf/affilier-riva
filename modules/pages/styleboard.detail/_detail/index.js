import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import StyleBoardService from 'app/services/style.board';

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image'
import TouchableBit from 'modules/bits/touchable'
import ImagePagelet from 'modules/pagelets/image'
import InputValidatedBit from 'modules/bits/input.validated';
import LoaderBit from 'modules/bits/loader';
import NavTextBit from 'modules/bits/nav.text';

// import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';
import SelectStylistLego from 'modules/legos/select.stylist';

import ExchangeDetailsPagelet from 'modules/pagelets/exchange.details';
import ModalPromptPagelet from 'modules/pagelets/modal.prompt';


import Styles from './style';


export default ConnectHelper(
	class DetailPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				replacingId: PropTypes.id,
				userId: PropTypes.id,
				assetIds: PropTypes.arrayOf(PropTypes.id),
				changeable: PropTypes.bool,
				title: PropTypes.string,
				client: PropTypes.string,
				stylist: PropTypes.string,
				type: PropTypes.string,
				outfit: PropTypes.string,
				attire: PropTypes.string,
				product: PropTypes.string,
				note: PropTypes.string,
				assets: PropTypes.array,
				previewSchedule: PropTypes.string,
				onStylistChanged: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isUpdating: false,
			})
		}

		types = [{
			key: 'FIRST',
			title: 'First Stylesheet',
		}, {
			key: 'REPEAT',
			title: 'Repeat Stylesheet',
		}, {
			key: 'EXCHANGE',
			title: 'Exchange Stylesheet',
		}]

		lineups = [{
			key: null,
			title: 'None',
		}, {
			key: 'Clothing + Clothing + Clothing + Clothing',
			title: 'Clothing + Clothing + Clothing + Clothing',
		}, {
			key: 'Clothing + Clothing + Clothing + Footwear',
			title: 'Clothing + Clothing + Clothing + Footwear',
		}, {
			key: 'Clothing + Clothing + Clothing + Bag',
			title: 'Clothing + Clothing + Clothing + Bag',
		}, {
			key: 'Clothing + Clothing + Clothing + Accessory',
			title: 'Clothing + Clothing + Clothing + Accessory',
		}, {
			key: 'Clothing + Clothing + Bag + Accessory',
			title: 'Clothing + Clothing + Bag + Accessory',
		}, {
			key: 'Clothing + Clothing + Footwear + Accessory',
			title: 'Clothing + Clothing + Footwear + Accessory',
		}]

		validateNumber = input => {
			return !!+input
		}

		onChange = (key, value) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, +value ? +value : value)
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.close()
		}

		onImagePopUp = source => {
			this.props.utilities.alert.modal({
				component: (
					<ImagePagelet source={source} onPress={this.onModalRequestClose} transform={this.transform}/>
				),
			})
		}

		onChangeStylist = (key, val, note) => {
			this.setState({
				isUpdatingData: true,
			}, () => {
				StyleBoardService.assignStylist(this.props.id, key, this.props.token, note).then(isUpdated => {
					if (isUpdated) {
						this.props.utilities.notification.show({
							title: 'Success',
							message: `Stylist assigned to stylesheet #${this.props.id}`,
							type: 'SUCCESS',
						})

						this.props.onStylistChanged &&
						this.props.onStylistChanged(key, val)
					} else {
						throw new Error('Update false')
					}
				}).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops',
						message: 'Something went wrong with your request, please try again later',
					})
				}).finally(() => {
					this.setState({
						isUpdating: false,
					})
				})
			})
		}

		onShowModal = (key, val) => {
			return (
				this.props.utilities.alert.modal({
					component: (
						<ModalPromptPagelet
							title="Why?"
							message="Short, descriptive note on why you reopen this stylesheet."
							placeholder="Input here"
							onCancel={this.onModalRequestClose}
							onConfirm={this.onChangeStylist.bind(this, key, val)}
						/>
					),
				})
			)
		}

		view() {
			return (
				<React.Fragment>
					<BoxRowLego
						title={`Styleboard #SB-${this.props.id}`}
						data={[{
							data: [{
								title: 'Client',
								content: '-',
								children: this.props.client ? (
									<NavTextBit title={ this.props.client } link="View profile" href={`client/${this.props.userId}`} />
								) : undefined,
							}, {
								title: 'Stylist',
								content: '-',
								// eslint-disable-next-line no-nested-ternary
								children: this.props.changeable ? this.state.isUpdating ? (
									<BoxBit centering>
										<LoaderBit simple />
									</BoxBit>
								) : (
									<SelectStylistLego stylist={ this.props.stylist }
										// onChange={ this.onChangeStylist }
										onChange={ this.onShowModal }
									/>
								) : this.props.client ? (
									<NavTextBit title={this.props.stylist} link="View style sheets" href={'stylesheet'} param={{ search: this.props.client }} />
								) : undefined,
							}],
						}, {
							data: [{
								title: 'Product',
								content: 'Custom Styling', // this.props.title,
							}, {
								title: 'Preview Schedule',
								content: this.props.previewSchedule ? TimeHelper.format(this.props.previewSchedule, 'DD MMMM YYYY') : '-',
							}],
						}]}
						contentContainerStyle={Styles.padder}
					/>
					<BoxRowLego
						title={'Detail'}
						data={[{
							data: [{
								title: 'Title',
								content: this.props.title || '-',
								children: this.props.changeable ? (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.INPUT }
										value={ this.props.title }
										onChange={ this.onChange.bind(this, 'title') }
									/>
								) : undefined,
							}],
						}, {
							data: [{
								title: 'I\'m looking an outfit for:',
								content: this.props.outfit || '-',
							}, {
								title: 'With dresscode:',
								content: this.props.attire || '-',
							}],
						}, {
							data: [{
								title: 'I want to have this products:',
								content: this.props.product || '-',
							}],
						}, {
							data: [{
								title: 'Please note:',
								content: this.props.note || '-',
								contentStyle: Styles.notes,
							}],
						}, {
							data: [{
								title: 'Referensi',
								content: this.props.reference.length ? 'Seperti referensi dibawah ini ' : '-',
								children: this.props.reference && this.props.reference.length ? (
									<BoxBit unflex row>
										{ this.props.reference.map((file, i) => {
											return (
												<TouchableBit key={ i } unflex onPress={ this.onImagePopUp.bind(this, file) }>
													<ImageBit source={file} style={Styles.image} />
												</TouchableBit>
											)
										})}
									</BoxBit>
								) : false,
							}],
						}]}
						contentContainerStyle={Styles.padder}
						style={Styles.marginTop}
					/>
					{ this.props.replacingId ? (
						<ExchangeDetailsPagelet id={this.props.replacingId} readonly title="Previous Feedback" tab="exchanged" style={Styles.marginTop} />
					) : false }
				</React.Fragment>
			)
		}
	}
)
