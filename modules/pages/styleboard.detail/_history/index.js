import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import TimeHelper from 'coeur/helpers/time';
import UtilitiesContext from 'coeur/contexts/utilities'
import PageContext from 'coeur/contexts/page'

import Colors from 'utils/constants/color';

import IconBit from 'modules/bits/icon';
import NavTextBit from 'modules/bits/nav.text';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import TableLego from 'modules/legos/table';

import QuickViewStyleboardPagelet from 'modules/pagelets/quick.view.styleboard'

import Styles from './style';


export default ConnectHelper(
	class HistoryPart extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.array,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
			PageContext,
		]

		header = [{
			title: 'Changed By',
			width: 4,
			align: 'center',
		}, {
			title: 'Changed On',
			width: 5,
			align: 'center',
		}, {
			title: 'Changed To',
			width: 4,
			align: 'center',
		}, {
			title: 'Changed stylist to',
			width: 5,
			align: 'center',
		}, {
			title: 'Changelog',
			width: 5,
			align: 'center',
		}]

		onQuickView = id => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewStyleboardPagelet
						id={ id }
						onNavigate={ this.props.page.navigator.navigate }
					/>
				),
			})
		}

		rowRenderer = history => {
			return {
				data: [{
					title: history.changer_user_id,
				}, {
					title: TimeHelper.format(history.created_at, 'DD MMM YYYY HH:mm'),
				}, {
					children: (
						<BadgeStatusLego status={ history.status } />
					),
				}, {
					title: history.stylist,
				}, {
					title: history.note,
				}],
			}
		}

		view() {
			return (
				<TableLego isLoading={ this.state.isLoading }
					headers={ this.header }
					rows={ this.props.data.map(this.rowRenderer) }
					style={ Styles.padder } />
			)
		}
	}
)
