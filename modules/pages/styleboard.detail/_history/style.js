import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	padder: {
		marginRight: Sizes.margin.default,
	},

	image: {
		width: 40,
		height: 40,
		borderRadius: 20,
		overflow: 'hidden',
		borderWidth: 3,
		borderColor: Colors.white.primary,
		marginRight: 8,
	},
})
