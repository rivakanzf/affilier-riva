import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import GeomanistBit from 'modules/bits/geomanist';
import InputValidatedBit from 'modules/bits/input.validated';

// import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';

import Styles from './style';


export default ConnectHelper(
	class StylistNotePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				readonly: PropTypes.bool,
				value: PropTypes.string,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				value: p.value,
				length: p.value ? p.value.length : 0,
				max: 1500,
			})
		}

		headerRenderer() {
			return (
				<GeomanistBit type={GeomanistBit.TYPES.CAPTION_1} style={Styles.counter}>
					{ this.state.length } / { this.state.max }
				</GeomanistBit>
			)
		}

		onChange = val => {
			this.setState({
				length: val ? val.length : 0,
			})

			this.props.onChange &&
			this.props.onChange(val)
		}

		view() {
			return (
				<BoxRowLego
					title={'Styleboard Note'}
					header={ this.headerRenderer() }
					data={[{
						children: (
							<InputValidatedBit readonly={ this.props.readonly }
								maxlength={ this.state.max }
								value={ this.props.value }
								placeholder={ this.props.readonly && !this.props.value ? '–' : 'Write down your stylist note here…' }
								onChange={ this.onChange }
								type={ InputValidatedBit.TYPES.TEXTAREA }
								style={ Styles.container }
								inputContainerStyle={ Styles.textarea }
								inputStyle={ Styles.input }
							/>
						),
					}]}
					style={ this.props.style }
					contentContainerStyle={Styles.padder}
				/>
			)
		}
	}
)
