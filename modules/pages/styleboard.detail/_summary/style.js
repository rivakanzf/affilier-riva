import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	padder: {
		marginBottom: 0,
		marginTop: 0,
		marginLeft: 0,
		marginRight: 0,
	},
})
