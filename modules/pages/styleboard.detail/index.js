import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'

import StyleBoardService from 'app/services/style.board';

// import UserManager from 'app/managers/user';
// import OrderDetailManager from 'app/managers/order.detail';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';

import TabLego from 'modules/legos/tab';


import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import ActionsPart from './_actions'
import DetailPart from './_detail'
import StylistNotePart from './_stylist.note'
import SummaryPart from './_summary'
import HistoryPart from './_history'
// import StyleHistoryPart from 'modules/pages/user.detail/_style.history';
// import {
// 	StyleProfilePart,
// } from 'modules/pages/user.detail';

import Styles from './style';

import { debounce } from 'lodash';


export default ConnectHelper(
	class StyleBoardDetailPage extends PageModel {

		static routeName = 'styleboard.detail'

		static stateToProps(state) {
			return {
				isHeadOrStylist: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist', 'stylist'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				isChanged: false,
				tabIndex: p.tabIndex || 0,
				token: undefined,
				data: {},
				changes: {},
			})
		}

		roles = ['headstylist', 'stylist', 'fulfillment', 'analyst']

		tabs = [{
			title: 'General',
			onPress: this.onChangeTab.bind(this, 0),
		}, {
			title: 'History',
			icon: 'style-sheet',
			onPress: this.onChangeTab.bind(this, 1),
		}]

		onAuthorized = token => {
			this.setState({
				token,
				isLoading: true,
			}, () => {
				StyleBoardService.getStyleBoardDetail(this.props.id, this.state.token).then(res => {
					this.setState({
						isLoading: false,
						data: res,
					})
				}).catch(err => {
					this.warn(err)
					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			})
		}

		onNavigateToStyleboards = () => {
			this.navigator.navigate('styleboard')
		}

		onChanged = (status, changelog, stylistNote) => {
			if (changelog) {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						changelog,
						status,
					},
					changes: {},
				})
			} else if(stylistNote) {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						status,
						stylist_note: stylistNote,
					},
					changes: {},
				})
			} else {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						status,
					},
					changes: {},
				})
			}
		}

		onStylistChanged = (id, name) => {
			if (!this.state.data.stylist) {
				this.state.data.stylist = {}
			}

			this.state.data.stylist.id = id
			this.state.data.stylist.name = name
		}

		onUpdateData = (key, value) => {
			this.state.changes[key] = value

			if(key === 'stylist_note') {
				this.onForceUpdate()
			}

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onForceUpdate = debounce(() => {
			this.forceUpdate()
		}, 500)

		getChanges = () => {
			return this.state.changes
		}

		contentRenderer = () => {
			switch(this.state.tabIndex) {
			case 0:
				return (
					<BoxBit unflex row>
						<BoxBit style={Styles.padder}>
							<DetailPart
								id={ this.props.id }
								userId={ this.state.data.user.id }
								changeable={ this.props.isHeadOrStylist }
								client={ this.state.data.user.name }
								stylist={ !!this.state.data.stylist ? this.state.data.stylist.full_name : 'Choose' }
								title={ this.state.data.title }
								outfit={ this.state.data.outfit }
								attire={ this.state.data.attire }
								product= { this.state.data.product }
								note={ this.state.data.note }
								reference={ this.state.data.assets }
								// previewSchedule={ this.state.data.order.detail.shipment_at }
								onStylistChanged={ this.onStylistChanged }
								onUpdate={ this.onUpdateData }
							/>
						</BoxBit>
						<BoxBit>
							<ActionsPart
								key={ this.state.data.status }
								id={ this.props.id }
								changeable={ this.props.isHeadOrStylist }

								disabled={ !this.state.isChanged }
								changeGetter={ this.getChanges }

								status={ this.state.data.status }
								stylistNote={ this.state.data.stylist_note }
								updatedAt={ this.state.data.updated_at }
								onChanged={ this.onChanged }
							/>
							<StylistNotePart
								readonly={ this.state.data.status !== 'STYLING' }
								value={ this.state.data.stylist_note }
								onChange={ this.onUpdateData.bind(this, 'stylist_note') }
								style={ Styles.pad }
							/>
							<SummaryPart
								stylecards= { this.state.data.stylecards }
								style={ Styles.pad }
							/>
						</BoxBit>
					</BoxBit>
				)
			case 1:
				return (
					<HistoryPart
						data={this.state.data.histories}
					/>
				)
			default:
				break
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{
						title: 'Styleboard',
						onPress: this.onNavigateToStyleboards,
					}, {
						title: `#SB-${this.props.id}`,
					}]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Styleboard"
							/>
							<TabLego activeIndex={ this.state.tabIndex } tabs={ this.tabs } style={ Styles.tab } />

							{ this.state.isLoading ? (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							) : this.contentRenderer()}
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
