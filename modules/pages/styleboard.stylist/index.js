import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';
import AuthenticationHelper from 'utils/helpers/authentication'

import StyleBoardService from 'app/services/style.board';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';

import BadgeStatusLego from 'modules/legos/badge.status';
import SelectStatusLego from 'modules/legos/select.status';
import SelectStylistLego from 'modules/legos/select.stylist';
import TableLego from 'modules/legos/table';

import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import Styles from './style';

import { isEqual } from 'lodash';


export default ConnectHelper(
	class StylesheetStylistPage extends PageModel {

		static routeName = 'styleboard.stylist'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist', 'analyst', 'fulfillment'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isUpdatingData: [],
				offset: 0,
				count: 160,
				total: 0,
				search: p.search || undefined,
				styleboards: [],
				filter: {
					status: 'PENDING',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		tableHeader = [{
			title: 'ID',
			width: .8,
		}, {
			title: 'Product',
			width: 2,
		}, {
			title: 'SPQ',
			align: 'center',
			width: 1,
		}, {
			title: 'Client',
			width: 1.5,
		}, {
			title: 'Stylist',
			width: 1.5,
		}, {
			title: 'Requested On',
			width: 1,
		}, {
			title: 'Preview On',
			width: 1,
		}, {
			title: 'Seen On',
			width: 1,
		}, {
			title: 'Status',
			width: 1.2,
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token && (
					!isEqual(pS.offset, this.state.offset)
					|| !isEqual(pS.search, this.state.search)
					|| !isEqual(pS.filter.status, this.state.filter.status)
					|| !isEqual(pS.filter.type, this.state.filter.type)
				)
			) {
				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				StyleBoardService.getStyleBoards({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(filter.status ? { status: filter.status } : {}),
					...(this.props.isManager ? {} : { mine: true }),
				}, this.state.token).then(res => {
					if(id === this.getterId) {
						this.setState({
							isLoading: false,
							total: res.count,
							styleboards: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			this.setState({
				offset: 0,
				filter: {
					...this.state.filter,
					[key]: val,
				},
			})
		}

		onChangeStylist = (stylesheet, key, val) => {
			this.setState({
				isUpdatingData: [...this.state.isUpdatingData, stylesheet.id],
			}, () => {
				StyleBoardService.assignStylist(stylesheet.id, key, this.state.token).then(isUpdated => {
					if (isUpdated) {
						stylesheet.stylist = val

						this.utilities.notification.show({
							title: 'Success',
							message: `Stylist assigned to stylesheet #${stylesheet.id}`,
							type: 'SUCCESS',
						})
					} else {
						throw new Error('Update false')
					}
				}).catch(err => {
					this.warn(err)

					this.utilities.notification.show({
						title: 'Oops',
						message: 'Something went wrong with your request, please try again later',
					})
				}).finally(() => {
					this.setState({
						isUpdatingData: this.state.isUpdatingData.filter(id => id !== stylesheet.id),
					})
				})
			})

			// this.utilities.alert.show({
			// 	title: 'Assign Stylist',
			// 	message: `Are you sure want to assign ${val} to stylesheet #${stylesheet.id}?`,
			// 	actions: [{
			// 		title: 'Yes',
			// 		onPress: () => {


			// 		},
			// 	}, {
			// 		title: 'No',
			// 		type: 'CANCEL',
			// 		onPress: () => {
			// 			this.forceUpdate()
			// 		},
			// 	}]
			// })
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToDetail = id => {
			this.navigator.navigate(`styleboard/${id}`)
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		rowRenderer = styleboard => {
			return {
				data: [{
					title: `#SB-${styleboard.id}`,
				}, {
					title: styleboard.title,
				}, {
					children: (
						<IconBit
							name={ styleboard.spq ? 'circle-checkmark' : 'circle-null' }
							color={ styleboard.spq ? Colors.green.primary : Colors.red.primary }
							size={ 24 }
						/>
					),
				}, {
					title: styleboard.client || '-',
				}, {
					// eslint-disable-next-line no-nested-ternary
					children: this.props.isManager
						? this.state.isUpdatingData.indexOf(styleboard.id) === -1 ? (
							<SelectStylistLego isRequired
								placeholder="-"
								stylist={ styleboard.stylist }
								onChange={ this.onChangeStylist.bind(this, styleboard) }
								style={ Styles.stylist }
							/>
						) : (
							<BoxBit centering>
								<LoaderBit simple />
							</BoxBit>
						) : undefined,
				}, {
					title: '-',
					children: (
						<BoxBit unflex>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.format(styleboard.created_at, 'DD MMM YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								at { TimeHelper.format(styleboard.created_at, 'HH:mm') }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: '-',
					children: (
						<BoxBit unflex>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.format(styleboard.preview_at, 'DD MMM YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								at { TimeHelper.format(styleboard.preview_at, 'HH:mm') }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: '-',
					children: styleboard.seen_at ? (
						<BoxBit unflex>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.format(styleboard.seen_at, 'DD MMM YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								at { TimeHelper.format(styleboard.seen_at, 'HH:mm') }
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					children: (
						<BadgeStatusLego status={ styleboard.status } />
					),
				}],
				onPress: this.onGoToDetail.bind(this, styleboard.id),
			}
		}

		view() {

			return (
				<PageAuthorizedComponent
					roles={['stylist', 'headstylist', 'fulfillment', 'analyst']}
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Styleboard' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Styleboards"
								searchPlaceholder={ 'ID, Client, or Stylist' }
								search={ this.state.search }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} >
								<SelectStatusLego emptyTitle="All Status" type={SelectStatusLego.TYPES.STYLEBOARD_STATUSES} status={this.state.filter.status} onChange={this.onChangeFilter.bind(this, 'status')} />
							</HeaderFilterComponent>
							<TableLego isLoading={this.state.isLoading}
								headers={this.tableHeader}
								rows={this.state.styleboards.map(this.rowRenderer)}
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
