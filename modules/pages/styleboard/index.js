import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import AuthenticationHelper from 'utils/helpers/authentication'

import PurchaseService from 'app/services/purchase';
import StyleboardService from 'app/services/style.board';

import Colors from 'coeur/constants/color';
import Linking from 'coeur/libs/linking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import LinkBit from 'modules/bits/link';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import BadgeStatusSmallLego from 'modules/legos/badge.status.small';
import ColorLego from 'modules/legos/color';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import PickerVariantPagelet from 'modules/pagelets/picker.variant';
import QuickViewItemPagelet from 'modules/pagelets/quick.view.item';
import QuickViewProductPagelet from 'modules/pagelets/quick.view.product';
import QuickViewStyleboardPagelet from 'modules/pagelets/quick.view.styleboard';

import Styles from './style';

import { capitalize, uniqBy } from 'lodash'


export default ConnectHelper(
	class StylesheetPage extends PageModel {

		static routeName = 'styleboard'

		static stateToProps(state) {
			return {
				isInventory: AuthenticationHelper.isAuthorized(state.me.roles, 'inventory'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				activeId: -1,
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					type: undefined,
					date: undefined,
					status: 'STYLING',
					purchase_request_status: undefined,
					order_detail_status: undefined,
					without_po: 'false',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		roles = ['inventory', 'fulfillment', 'analyst']

		paths = [{
			title: 'Styleboard',
		}]

		headers = [{
			title: 'Title',
			width: 1.6,
		}, {
			title: 'Stylist',
			width: 1,
		}, {
			title: 'Preview On',
			width: .5,
		}, {
			title: '',
			width: .1,
		}, {
			title: 'Status',
			width: .8,
		}, {
			title: 'SPQ',
			align: 'center',
			width: .9,
		}, {
			title: 'Seen On',
			width: .5,
		}, {
			title: '',
			width: .2,
		}, {
			title: '',
			width: .2,
		}]

		childHeaders = [{
			title: 'Product',
			width: 1.6,
		}, {
			title: 'Category',
			width: .7,
		}, {
			title: 'Color',
			width: .6,
		}, {
			title: 'Size',
			width: .3,
		}, {
			title: 'Status',
			width: .5,
		}, {
			title: 'Note',
			width: .65,
		}, {
			title: '',
			width: .1,
		}, {
			title: 'Actions',
			width: 1.4,
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.filter.status !== this.state.filter.status
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		actions = inventory => {
			const actions = []

			if(
				this.props.isInventory
			) {
				if (inventory.variant_id) {
					actions.push({
						slug: 'set',
						title: 'Change Variant',
						theme: ButtonBit.TYPES.THEMES.SECONDARY,
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'setting' ? ButtonBit.TYPES.STATES.LOADING : this.setable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onSetVariant.bind(this, inventory),
					}, {
						slug: 'view',
						title: 'View Variant',
						onPress: this.onViewVariant.bind(this, inventory),
					})
				} else {
					actions.push({
						slug: 'set',
						title: 'Set Variant',
						theme: ButtonBit.TYPES.THEMES.SECONDARY,
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'setting' ? ButtonBit.TYPES.STATES.LOADING : this.setable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onSetVariant.bind(this, inventory),
					})
				}
			}

			return uniqBy(actions, 'slug')
		}

		status(status) {
			return `[${status.split('_').join(' ')}]`
		}

		changeable() {
			return this.props.isInventory
		}

		setable(inventory) {
			return !inventory.inventory_id
		}

		// receiveable(inventory) {
		// 	return inventory.status === 'PURCHASED'
		// }

		// rejectable(inventory) {
		// 	return inventory.status === 'REQUESTING'
		// }

		// approveable(inventory) {
		// 	return inventory.status === 'REQUESTING'
		// }

		// cancelable(inventory) {
		// 	return inventory.status === 'REQUEST_APPROVED'
		// }

		// purchaseable(inventory) {
		// 	return inventory.status === 'REQUEST_APPROVED'
		// }

		// droppable(inventory) {
		// 	return inventory.status === 'PURCHASED'
		// }

		// packable(inventory) {
		// 	return (inventory.inventory_id && inventory.status === 'BOOKED')
		// 		|| inventory.status === 'INVENTORY_RECEIVED'
		// }

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				StyleboardService.filterStyleboard({
					offset,
					limit: count,
					search,
					// stylesheet_status: 'PUBLISHED',
					status: filter.status,
				}, this.state.token).then(res => {
					if (id === this.getterId) {
						this.setState({
							isLoading: false,
							total: res.count,
							data: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops…',
							message: err ? err.detail || err.message || err : 'Something went wrong.',
						})
					})
				})
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigate = href => {
			this.navigator.navigate(href)
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onToggleActive = id => {
			if(this.state.activeId !== id) {
				this.setState({
					activeId: id,
				})
			} else {
				this.setState({
					activeId: -1,
				})
			}
		}

		onQuickView = styleboard => {
			this.warn('Please set quick view', styleboard.id)
			// TODO
			this.utilities.alert.modal({
				component: (
					<QuickViewStyleboardPagelet id={ styleboard.id } onNavigate={ this.onNavigate } mayPublish />
				),
			})
		}

		onQuickViewInventory = inventory => {
			if (inventory.inventory_id) {
				this.utilities.alert.modal({
					component: (
						<QuickViewItemPagelet
							id={ inventory.inventory_id }
							type={ QuickViewItemPagelet.TYPES.INVENTORY }
						/>
					),
				})
			} else if(inventory.purchase_request_id) {
				this.utilities.alert.modal({
					component: (
						<QuickViewItemPagelet
							id={ inventory.purchase_request_id }
							type={ QuickViewItemPagelet.TYPES.PURCHASE_REQUEST }
						/>
					),
				})
			} else {
				this.utilities.alert.modal({
					component: (
						<QuickViewItemPagelet
							id={ inventory.id }
							type={ QuickViewItemPagelet.TYPES.STYLESHEET_INVENTORY }
						/>
					),
				})
			}
		}

		onNavigateToInventory = () => {
			Linking.open(window.location.href.split('/')[0] + '/inventory')
		}

		onSetVariant = inventory => {
			this.utilities.alert.modal({
				component: (
					<PickerVariantPagelet
						onClose={ () => {
							inventory.isLoading = false
							this.forceUpdate()
						} }
						onSelect={ this.onConfirmSetVariant.bind(this, inventory) }
						onNavigateToInventory={ this.onNavigateToInventory }
						search={ inventory.title }

						title={ inventory.title }
						brand={ inventory.brand }
						brandId={ inventory.brand_id }
						category={ inventory.category }
						categoryId={ inventory.category_id }
						size={ inventory.size }
						sizeId={ inventory.size_id }
						colors={ inventory.colors.map(color => color.title) }
						colorsIds={ inventory.colors.map(color => color.id) }
					/>


				),
			})
		}

		onConfirmSetVariant = (inventory, variantId) => {
			inventory.isLoading = 'setting'

			this.forceUpdate()

			PurchaseService.updateRequest(inventory.purchase_request_id, {
				variant_id: variantId,
			}, this.state.token).then(isUpdated => {
				inventory.isLoading = false

				if (isUpdated) {
					inventory.variant_id = variantId

					this.utilities.notification.show({
						title: 'Yeay!',
						message: 'Success approving request.',
						type: 'SUCCESS',
					})

					this.forceUpdate()
				} else {
					throw new Error('Update failed.')
				}
			}).catch(err => {
				inventory.isLoading = false
				this.forceUpdate()

				throw err
			}).catch(this.onError)
		}

		onViewVariant = inventory => {
			this.utilities.alert.modal({
				component: (
					<QuickViewProductPagelet
						variantId={ inventory.variant_id }
						includeZeroInventory
					/>
				),
			})
		}

		onError = err => {
			this.warn(err)

			this.utilities.notification.show({
				title: 'Oops…',
				message: err ? (err.detail && err.detail.message || err.detail) || err.message || err : 'Something went wrong.',
			})
		}

		rowRenderer = styleboard => {
			const isActive = this.state.activeId === styleboard.id

			return {
				data: [{
					children: (
						<BoxBit style={ Styles.multiline }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								<TextBit style={ Styles.id }>[#SB-{ styleboard.id }]</TextBit> { styleboard.title }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ styleboard.order ? `${ styleboard.order } – ${ styleboard.user }` : styleboard.user }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: styleboard.stylist,
				}, {
					title: '-',
					children: styleboard.shipment_at ? (
						<BoxBit style={ Styles.multiline }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.moment(styleboard.shipment_at).format('DD/MM/YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ TimeHelper.moment(styleboard.shipment_at).fromNow() }
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					title: '',
				}, {
					children: (
						<BadgeStatusLego status={ styleboard.status } />
					),
				}, {
					children: (
						<IconBit
							name={ styleboard.spq ? 'circle-checkmark' : 'circle-null' }
							color={ styleboard.spq ? Colors.green.primary : Colors.red.primary }
							size={ 24 }
						/>
					),
				}, {
					title: '-',
					children: styleboard.seen_at ? (
						<BoxBit style={ Styles.multiline }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.moment(styleboard.seen_at).format('DD/MM/YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.note }>
								{ TimeHelper.moment(styleboard.seen_at).fromNow() }
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					children: (
						<BoxBit row style={ Styles.icons }>
							<TouchableBit unflex centering onPress={ this.onQuickView.bind(this, styleboard) }>
								<IconBit
									name={ 'quick-view' }
									color={ Colors.primary }
								/>
							</TouchableBit>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit style={ Styles.dropdown }>
							<IconBit
								name={ isActive ? 'dropdown-up' : 'dropdown' }
								size={ 20 }
							/>
						</BoxBit>
					),
				}],
				children: isActive ? styleboard.stylesheets.map(this.stylesheetRenderer.bind(this, styleboard)) : false,
				onPress: this.onToggleActive.bind(this, styleboard.id),
				style: !!styleboard.isLoading ? Styles.loading : undefined,
			}
		}

		stylesheetRenderer = (styleboard, stylesheet, i) => {
			return (
				<BoxBit unflex accessible={ stylesheet.status === 'PUBLISHED' } style={ stylesheet.status === 'PUBLISHED' ? undefined : Styles.disabled }>
					<TableLego key={ stylesheet.id } compact small
						headerless={ i !== 0 }
						bodyless={ i === 0 ? !stylesheet.inventories.length && !!styleboard.inventoryCount : !stylesheet.inventories.length }
						isLoading={ this.state.isLoading }
						headers={ this.childHeaders }
						rows={ stylesheet.inventories.map(this.childRenderer.bind(this, stylesheet.status)) }
						style={ Styles.table } />
				</BoxBit>
			)
		}

		childRenderer = (stylesheetStatus, inventory) => {
			const actions = this.actions(inventory)
			const disabled = !this.changeable(inventory) || !actions.length

			return {
				data: [{
					children: (
						<BoxBit style={ Styles.multiline }>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
								<TextBit style={Styles.id}>[{ inventory.seller }]</TextBit>
							</GeomanistBit>
							{ inventory.url ? (
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
									<LinkBit target={ LinkBit.TYPES.BLANK } href={ inventory.url } underline>
										{ inventory.brand } – { inventory.title }
									</LinkBit>
								</GeomanistBit>
							) : (
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
									{ inventory.brand } – { inventory.title }
								</GeomanistBit>
							) }
						</BoxBit>
					),
				}, {
					title: inventory.category,
				}, {
					title: '-',
					children: (
						<BoxBit row style={ Styles.colors }>
							<ColorLego colors={ inventory.colors } size={ 16 } radius={ 8 } border={ 0 } style={Styles.color} />
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
								{ inventory.colors.map(color => capitalize(color.title)).join('/') }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: inventory.size,
				}, {
					children: (
						<BadgeStatusSmallLego tight status={ stylesheetStatus === 'PUBLISHED' ? inventory.status : 'UNPUBLISHED' } />
					),
				}, {
					title: inventory.note,
				}, {
					title: '',
				}, {
					children: (
						<BoxBit accessible={ this.changeable(inventory) } row unflex style={ Styles.actions }>
							{ disabled ? (
								<IconBit name="lock" size={ 16 } />
							) : actions.map(this.actionRenderer) }
						</BoxBit>
					),
				}],
				onPress: this.onQuickViewInventory.bind(this, inventory),
				disabled,
				style: disabled ? Styles.loading : undefined,
			}
		}

		actionRenderer = action => {
			return (
				<ButtonBit weight="medium"
					key={ action.slug }
					title={ action.title }
					theme={ action.theme }
					state={ action.state }
					onPress={ action.onPress }
					type={ ButtonBit.TYPES.SHAPES.PROGRESS }
					width={ ButtonBit.TYPES.WIDTHS.FIT }
					size={ ButtonBit.TYPES.SIZES.MIDGET }
					style={ Styles.button }
				/>
			)
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={ this.paths }
				>
					{ () => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent
								title="Styleboards"
								searchPlaceholder={ 'Order number' }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								<SelectStatusLego placeholder={ 'Styleboard Status' } type={ SelectStatusLego.TYPES.STYLEBOARD_STATUSES } status={ this.state.filter.status } onChange={ this.onChangeFilter.bind(this, 'status') } />
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.headers }
								rows={ this.state.data.map(this.rowRenderer) }
								style={ Styles.padder } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
