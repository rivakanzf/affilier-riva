import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'utils/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	main: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	padder: {
		marginBottom: 16,
	},

	actions: {
		flexWrap: 'wrap',
		marginBottom: -4,
	},

	note: {
		color: Colors.black.palette(2, .6),
	},

	id: {
		color: Colors.primary,
	},

	icons: {
		justifyContent: 'flex-end',
	},

	button: {
		marginRight: 4,
		marginBottom: 4,
	},

	loading: {
		transition: '1s all',
		filter: 'grayscale(.6)',
	},

	multiline: {
		justifyContent: 'center',
	},

	table: {
		marginTop: 8,
		marginBottom: -12,
		marginLeft: -8,
		marginRight: -8,
		paddingTop: 4,
		paddingBottom: 4,
	},

	color: {
		marginRight: 4,
	},

	colors: {
		alignItems: 'center',
	},

	dropdown: {
		justifyContent: 'center',
		alignItems: 'flex-end',
	},

	disabled: {
		opacity: .5,
		filter: 'grayscale(1)',
	},

})
