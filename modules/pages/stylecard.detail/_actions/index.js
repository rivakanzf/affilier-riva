import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import StylecardService from 'app/services/stylecard';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ClipboardBit from 'modules/bits/clipboard';
import GeomanistBit from 'modules/bits/geomanist';
import SwitchBit from 'modules/bits/switch';
import SelectionBit from 'modules/bits/selection'

import BoxRowLego from 'modules/legos/box.row';
import SelectStatusLego from 'modules/legos/select.status';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import StylistNotePagelet from 'modules/pagelets/stylist.note';

import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				changeable: PropTypes.bool,

				title: PropTypes.string,
				slug: PropTypes.string,

				disabled: PropTypes.bool,
				changeGetter: PropTypes.func,

				collectionIds: PropTypes.array,

				isBundle: PropTypes.bool,
				isPublic: PropTypes.bool,
				isMaster: PropTypes.bool,
				inventoryOnly: PropTypes.bool,
				isRecomendation: PropTypes.bool,
				status: PropTypes.string,
				changelog: PropTypes.string,
				stylistNote: PropTypes.string,
				updatedAt: PropTypes.string,
				onChanged: PropTypes.func,
				onToggleBundle: PropTypes.func,
				onTogglePublic: PropTypes.func,
				onToggleMaster: PropTypes.func,
				onToggleInventoryOnly: PropTypes.func,
				onToggleRecomendation: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me.fullName,
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isChanging: false,
				status: p.status,
				isSimple: true,
			})
		}

		styleboardType = [
			{
				title: 'Pre-Styled (Will hide automatically if any variant is not available)',
				key: 'prestyled',
				selected: (this.props.isBundle && this.props.inventoryOnly && !this.props.isRecomendation),
			},
			{
				title: 'Regular Bundle (Mainly will be displayed on Journal, displat the longest shipping option)',
				key: 'regularBundle',
				selected: (this.props.isBundle && !this.props.inventoryOnly && !this.props.isRecomendation),
			},
			{
				title: 'Regular (General use and Styleboard-Request)',
				key: 'regular',
				selected: (!this.props.isBundle && !this.props.inventoryOnly && !this.props.isRecomendation),
			},
			{
				title: 'Whatsapp Weekly Recommendation',
				key: 'weeklyRecommendation',
				selected: this.props.isRecomendation,
			},
		]

		disabledStatuses = () => {
			switch(this.props.status) {
			case 'PENDING':
				return [					  'PUBLISHED',	'APPROVED'	]
			case 'STYLING':
				return ['PENDING', 							'APPROVED'	]
			case 'PUBLISHED':
				return ['PENDING',							'APPROVED'	]
			case 'APPROVED':
				return ['PENDING', 'STYLING', 'PUBLISHED',  'APPROVED'	]
			case 'EXCEPTION':
				return this.props.changeable ? ['PENDING', 'APPROVED'] : ['PENDING', 'STYLING', 'PUBLISHED', 'APPROVED']
			default:
				return []
			}
		}

		// action() {
		// 	if(this.props.changeable) {
		// 		return 'Update Stylesheet'
		// 	} else {
		// 		switch (this.props.status) {
		// 		case 'PENDING':
		// 			return 'Start Styling'
		// 		case 'STYLING':
		// 			return 'Submit'
		// 		case 'PUBLISHED':
		// 		case 'APPROVED':
		// 			return 'Reopen'
		// 		case 'EXCEPTION':
		// 		default:
		// 			return 'Update Stylesheet'
		// 		}
		// 	}
		// }

		buttonState() {
			if (this.state.isChanging) {
				return ButtonBit.TYPES.STATES.LOADING
			} else if (this.props.disabled && this.props.status === this.state.status) {
				return ButtonBit.TYPES.STATES.DISABLED
			} else {
				return ButtonBit.TYPES.STATES.NORMAL
			}
		}

		onToggleSimpleMode = () => {
			// if(!this.state.isSimple) {
			// 	this.props.onToggleMaster(false)
			// 	this.props.onTogglePublic(false)
			// 	this.props.onToggleBundle(false)
			// 	this.props.onToggleInventoryOnly(false)
			// 	this.props.onToggleRecomendation(false)
			// }
			this.setState({
				isSimple: !this.state.isSimple,
			})
		}

		onModalRequestClose = () => {
			this.setState({
				isChanging: false,
			})

			this.props.utilities.alert.hide()
		}

		onChangeStatus = status => {
			this.setState({
				status,
			})
		}

		onUpdateData = () => {
			this.setState({
				isChanging: true,
			}, () => {
				return StylecardService
					.update(this.props.id, {
						...this.props.changeGetter(),
					}, undefined, this.props.token)
					.then(() => {
						this.props.utilities.notification.show({
							title: 'Success',
							message: 'Data updated.',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.setState({
							isChanging: false,
						})
					})
					.catch(this.onError)
			})
		}

		onUpdate = () => {
			if(this.state.status === 'PUBLISHED' && (this.props.isBundle && this.props.inventoryOnly)) {
				if(this.props.collectionIds.length === 0) {
					return this.onError({detail: 'pre styled harus memiliki collection !'})
				}
			}

			if(this.state.status === 'PUBLISHED' && (!this.props.title || this.props.title.length === 0) ) {
				return this.onError({detail: 'Stylecard harus memiliki title !'})
			}

			if(this.props.changeable) {
				// can change into anything
				if(this.state.status === 'PUBLISHED') {
					// publishing or approving, need to set stylist note
					this.props.utilities.alert.modal({
						component: (
							<StylistNotePagelet value={ this.props.changeGetter().stylist_note || this.props.stylistNote } onClose={ this.onModalRequestClose } onSubmit={ this.onPublishStylesheet.bind(this, this.state.status) } />
						),
					})
				} else {
					this.props.utilities.alert.modal({
						component: (
							<ModalPromptPagelet
								title="Add note"
								message="Short, descriptive note on why you made this change."
								placeholder="Input here"
								onCancel={this.onModalRequestClose}
								onConfirm={this.onConfirmChangeStatus}
							/>
						),
					})
				}
			} else {
				// forward only
				switch(this.state.status) {
				case 'STYLING':
					// start styling
					if(this.props.status === 'PUBLISHED') {
						// back to styling
						this.props.utilities.alert.modal({
							component: (
								<ModalPromptPagelet
									title="Why?"
									message="Short, descriptive note on why you reopen this style card."
									placeholder="Input here"
									onCancel={this.onModalRequestClose}
									onConfirm={this.onStartStyling}
								/>
							),
						})
					} else if(this.props.status === 'PENDING') {
						this.onStartStyling()
					} else {
						this.props.utilities.notification.show({
							title: 'Oops…',
							message: 'Status invalid. Contact administrator.',
						})
					}
					break
				case 'PUBLISHED':
					// submitting
					this.props.utilities.alert.modal({
						component: (
							<StylistNotePagelet value={ this.props.changeGetter().stylist_note || this.props.stylistNote } onClose={ this.onModalRequestClose } onSubmit={ this.onPublishStylesheet.bind(this, 'PUBLISHED') } />
						),
					})
					break
				case 'PENDING':
				case 'APPROVED':
				case 'EXCEPTION':
				default:
					this.props.utilities.notification.show({
						title: 'Oops…',
						message: 'Status invalid. Contact administrator.',
					})

					break
				}
			}
		}

		onStartStyling = note => {
			this.setState({
				isChanging: true,
			}, () => {
				return Promise.resolve().then(() => {
					if (this.props.disabled) {
						// No data changes
						return StylecardService.setStatus(this.props.id, 'STYLING', note, this.props.token)
					} else {
						return StylecardService
							.update(this.props.id, {
								...this.props.changeGetter(),
								status: 'STYLING',
							}, note, this.props.token)
					}
				}).then(() => {
					this.props.utilities.notification.show({
						title: 'Go…Go…Go…',
						message: `Happy styling, ${ this.props.me } 🤗`,
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.setState({
						isChanging: false,
					}, () => {
						this.props.onChanged &&
						this.props.onChanged('STYLING', note)
					})
				}).catch(this.onError)
			})
		}

		onPublishStylesheet = (status, stylistNote) => {
			return Promise.resolve().then(() => {

				this.setState({
					isChanging: true,
				})

				if(this.props.disabled) {
					// No data change
					return StylecardService.publish(this.props.id, status, stylistNote, {}, this.props.token)
				} else {
					return StylecardService
						.update(this.props.id, {
							...this.props.changeGetter(),
							status,
							stylist_note: stylistNote,
						}, undefined, this.props.token)
				}
			}).then(() => {
				this.props.utilities.notification.show({
					title: 'Congratulations 🎉🎉🎉',
					message: 'One style card down. Let\'s keep the spirit up!',
					type: this.props.utilities.notification.TYPES.SUCCESS,
				})

				this.onModalRequestClose()

				this.setState({
					isChanging: false,
				}, () => {
					this.props.onChanged &&
					this.props.onChanged(status, undefined, stylistNote)
				})
			}).catch(this.onError)
		}

		onConfirmChangeStatus = note => {
			this.setState({
				isChanging: true,
			}, () => {
				return Promise.resolve().then(() => {
					if(this.props.disabled) {
						// No data change
						return StylecardService.setStatus(this.props.id, this.state.status, note, this.props.token)
					} else {
						return StylecardService
							.update(this.props.id, {
								...this.props.changeGetter(),
								status: this.state.status,
							}, note, this.props.token)
					}
				}).then(() => {
					this.props.utilities.notification.show({
						title: 'Status changed',
						message: 'Successfully change status.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.setState({
						isChanging: false,
					}, () => {
						this.props.onChanged &&
						this.props.onChanged(this.state.status, note)
					})
				}).catch(this.onError)
			})
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				timeout: 10000,
			})

			this.setState({
				isChanging: false,
			})
		}

		onChangeStyleboardType = (key) => {
			switch(key) {
			case 'prestyled':
				this.props.onTogglePublic(true)
				this.props.onToggleBundle(true)
				this.props.onToggleInventoryOnly(true)

				this.props.onToggleRecomendation(false)
				return(true)
			case 'regularBundle':

				this.props.onToggleBundle(true)

				this.props.onTogglePublic(false)
				this.props.onToggleInventoryOnly(false)
				this.props.onToggleRecomendation(false)
				return(true)


			case 'regular':
				this.props.onToggleBundle(false)
				this.props.onTogglePublic(false)
				this.props.onToggleInventoryOnly(false)
				this.props.onToggleRecomendation(false)
				return(true)

			case 'weeklyRecommendation':
				this.props.onToggleRecomendation(true)
				this.props.onToggleBundle(false)
				this.props.onTogglePublic(false)
				this.props.onToggleInventoryOnly(false)
				return(true)

			default:
				return('no condition met')
			}

		}

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Simple Mode',
							content: 'Activate Simple Mode',
						}, {
							children: <SwitchBit
								onChange={ this.onToggleSimpleMode }
								value={ this.state.isSimple }
								style={ Styles.switch }
							/>,
						}],
					}, {
						data: [{
							title: 'Master Style Card',
							content: this.props.isMaster ? 'Stylist can duplicate from this style card' : 'Style card cannot be duplicated (default)',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: this.props.changeable ? (
								<SwitchBit onChange={ this.props.onToggleMaster } value={ this.props.isMaster } style={ Styles.switch } />
							) : undefined,
						}],
					}, this.state.isSimple && {
						data: [{
							children: (
								<SelectionBit
									placeholder={ 'Select Stylecard Type '}
									options={ this.styleboardType }
									onChange={ this.onChangeStyleboardType }
								/>
							),
						}],
					}, !this.state.isSimple && {
						data: [{
							title: 'Prestyled',
							content: this.props.isPublic ? 'This prestyled will be shown on the list' : 'This prestyled is not publicly available (default)',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: this.props.changeable ? (
								<SwitchBit onChange={ this.props.onTogglePublic } value={ this.props.isPublic } style={ Styles.switch } />
							) : undefined,
						}],
					}, !this.state.isSimple && {
						data: [{
							title: 'Sold in Bundle',
							content: this.props.isBundle ? 'Variant within this style card can only be purchased as a bundle' : 'Variant can be purchased separately (default)',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: this.props.changeable ? (
								<SwitchBit onChange={ this.props.onToggleBundle } value={ this.props.isBundle } style={ Styles.switch } />
							) : undefined,
						}],
					}, !this.state.isSimple && {
						data: [{
							title: 'Use Inventory Only',
							content: this.props.inventoryOnly ? 'This style card valid for inventory purchase only' : 'This style card can create purchase request (default)',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: this.props.changeable ? (
								<SwitchBit onChange={ this.props.onToggleInventoryOnly } value={ this.props.inventoryOnly } style={ Styles.switch } />
							) : undefined,
						}],
					}, !this.state.isSimple && {
						data: [{
							title: 'Is weekly recommendation ?',
							content: this.props.isRecomendation ? 'This stylecard is part of weekly recommendations' : 'this stylecard is not part of weekly recommendations',
							span: 4,
							contentStyle: Styles.content,
						}, {
							children: this.props.changeable ? (
								<SwitchBit onChange={ this.props.onToggleRecomendation } value={ this.props.isRecomendation } style={ Styles.switch } />
							) : undefined,
						}],
					}, {
						data: [{
							title: 'Stylecard Status',
							children: (
								<BoxBit unflex>
									<SelectStatusLego disabled={ !this.props.changeable } key={ this.state.updater } unflex isRequired
										type={ SelectStatusLego.TYPES.STYLECARD_STATUSES }
										status={ this.props.status }
										disabledStatuses={ this.disabledStatuses() }
										onChange={ this.onChangeStatus }
									/>
									<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={ Styles.time }>
										Last updated: {
											this.props.updatedAt
												? TimeHelper.format(this.props.updatedAt, 'DD MMMM YYYY HH:mm')
												: '-'
										}
									</GeomanistBit>
								</BoxBit>
							),
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ 'UPDATE STYLECARD' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.buttonState() }
									onPress={ this.props.status === this.state.status ? this.onUpdateData : this.onUpdate }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Public URL',
							children: (
								<ClipboardBit
									autogrow={ 44 }
									value={`helloyuna.io/stylecard/${this.props.id}${this.props.isRecomendation ? '?tr_source=wareco2207&utm_source=wareco2207&utm_medium=wa&utm_campaign=wa_blast' : ''}`}
									style={ Styles.clipboard } />
							),
						}],
					}, {
						data: [{
							title: 'Last Change Log',
							children: (
								<ClipboardBit autogrow={ 44 } value={ this.props.changelog || '-' } style={ Styles.clipboard } />
							),
						}],
					}]}
				/>
			)
		}
	}
)
