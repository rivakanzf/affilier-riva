import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import QueryStatefulModel from 'coeur/models/query.stateful';

import TimeHelper from 'coeur/helpers/time';
import LineupHelper from 'utils/helpers/lineup';

import UtilitiesContext from 'coeur/contexts/utilities';

import StylecardService from 'app/services/stylecard';

import BoxBit from 'modules/bits/box';
import InputFileBit from 'modules/bits/input.file'
import InputValidatedBit from 'modules/bits/input.validated';
import LoaderBit from 'modules/bits/loader';
import NavTextBit from 'modules/bits/nav.text';
import SelectionBit from 'modules/bits/selection';
import SelectionMultipleBit from 'modules/bits/selection.multiple';
import ButtonBit from 'modules/bits/button'
import ImageBit from 'modules/bits/image'

// import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';
import SelectStylistLego from 'modules/legos/select.stylist';
import RowImageUserLego from 'modules/legos/row.image.user';

import ImageEditorRecommendationPagelet from 'modules/pagelets/image.editor.recommendation'

import UserService from 'app/services/user.new'

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends QueryStatefulModel {

		static propTypes(PropTypes) {
			return {
				isLoading: PropTypes.bool,
				id: PropTypes.id,
				assetIds: PropTypes.arrayOf(PropTypes.id),
				changeable: PropTypes.bool,
				title: PropTypes.string,
				slug: PropTypes.string,
				stylist: PropTypes.string,
				count: PropTypes.number,
				note: PropTypes.string,
				image: PropTypes.string,
				lineup: PropTypes.string,
				assets: PropTypes.array,
				collectionIds: PropTypes.arrayOf(PropTypes.id),
				onStylistChanged: PropTypes.func,
				onUpdate: PropTypes.func,
				onGetRecommendations: PropTypes.func,
			}
		}

		static getDerivedStateFromProps(nP, nS) {
			if(!nS.isLoaded) {
				const image = nP.image || null
				return {
					...nS,
					image,
					isLoaded: true,
				}
			} else {
				return nS
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static propsToQuery(state) {
			return [`{
				collections: collectionsList(filter: {
					deletedAt: {
						isNull: true
					}
				}) {
					id
					title
				}
			}`, {}, state.me.token]
		}

		static contexts = [
			UtilitiesContext,
		]

		static defaultProps = {
			collectionIds: [],
		}

		constructor(p) {
			super(p, {
				isUpdating: false,
				isUploading: false,
				image: p.image,
				isLoaded: false,
				croppedImages: {
					image: '',
					coor: [],
				},
			})
		}

		validateNumber = input => {
			return !!+input
		}

		onChange = (key, value) => {
			this.setState({
				[key]: value,
			}, () => {
				this.props.onUpdate &&
				this.props.onUpdate(key, +value ? +value : value)
			})
			
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onGetRecommendations = () => {
			this.props.onGetRecommendations(this.state.image, this.state.imageLink)
		}

		onGetRecommendationsManual = (source) => {
			this.props.utilities.alert.modal({
				component: (
					<ImageEditorRecommendationPagelet
						// variantId={ this.props.variantId }
						source={source}
						onPress={this.onModalRequestClose}
						onAddCroppedImage={ this.onAddCroppedImage }
						// newIndex={this.state.images.length}
						// transform={this.transform}
					/>
				),
			})
		}

		onAddCroppedImage = (croppedImage, coor) => {
			this.setState({
				croppedImages: {
					image: croppedImage,
					coor,
				},
			})
			this.props.utilities.alert.hide()
		}

		onChangeStylist = (key, val) => {
			this.setState({
				isUpdatingData: true,
			}, () => {
				StylecardService.assignStylist(this.props.id, key, this.props.token).then(isUpdated => {
					if (isUpdated) {
						this.props.utilities.notification.show({
							title: 'Success',
							message: `Stylist assigned to style card #${ this.props.id }`,
							type: 'SUCCESS',
						})

						this.props.onStylistChanged &&
						this.props.onStylistChanged(key, val)
					} else {
						throw new Error('Update false')
					}
				}).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops',
						message: 'Something went wrong with your request, please try again later',
					})
				}).finally(() => {
					this.setState({
						isUpdating: false,
					})
				})
			})
		}

		onChangeCollection = selections => {
			this.props.onUpdate &&
			this.props.onUpdate('collection_ids', selections.map(s => s.key))
		}

		onDataLoaded = (id, image) => {
			if(id === 'loader' && image === undefined) {
				this.setState({image: undefined}, () => {
					this.props.onImageRemoved()
				})
			} else if(id === 'adder') {
				this.setState({isUploading: true}, () => {
					UserService.upload(this.props.id, {
						image,
					}, this.props.token).then(res => {
						// this.props.onImageLoaded
						// && this.props.onImageLoaded(res)
	
						this.onChange('image', res.url)
						this.setState({image: res.url, isUploading: false, imageLink: res.metadata.url})
					})
				})
			}
		}

		viewOnLoading() {
			return (
				<BoxRowLego
					title={`Style Card #SC-${ this.props.id }`}
					data={[{
						data: [{
							children: (
								<LoaderBit simple />
							),
						}],
					}]}
					contentContainerStyle={ Styles.padder }
				/>
			)
		}

		view() {
			return (
				<React.Fragment>
					<BoxRowLego
						title={`Style Card #SC-${ this.props.id }`}
						data={[{
							data: [{
								title: 'Stylist',
								content: this.props.stylist,
								// eslint-disable-next-line no-nested-ternary
								children: this.props.changeable ? this.state.isUpdating ? (
									<BoxBit centering>
										<LoaderBit simple />
									</BoxBit>
								) : (
									<SelectStylistLego stylist={ this.props.stylist } onChange={ this.onChangeStylist } />
								) : this.props.client ? (
									<NavTextBit title={this.props.stylist} link="View style cards" href={'stylecard'} param={{ search: this.props.client }} />
								) : undefined,
							}, {
								title: 'Created At',
								content: TimeHelper.format(this.props.createdAt, 'DD MMMM YYYY'),
							}],
						}]}
						contentContainerStyle={ Styles.padder }
					/>
					<BoxRowLego
						title={'Detail'}
						data={[{
							data: [ this.props.image || this.props.changeable ? {
								title: 'Image',
								children: (
									this.state.isUploading
										? <LoaderBit simple />
										: <InputFileBit
											key={ this.state.image }
											id={this.state.image ? 'loader' : 'adder'}
											maxSize={3}
											value={ this.state.image && `//${this.state.image}` }
											onDataLoaded={this.onDataLoaded}
											// style={Styles.image}
										/>
										// <InputImageUrlBit
										// 	placeholder="Input image URL here…"
										// 	value={ this.props.image }
										// 	disabled={ !this.props.changeable }
										// 	onChange={ this.onChange.bind(this, 'image') }
										// 	imageStyle={ Styles.image }
										// />
									
								),
							} : null],
						},
						{
							data: [this.state.imageLink || this.state.image ? {
								children: (
									<BoxBit row>
										<ButtonBit
											title={'Get recommendations'}
											onPress={ this.onGetRecommendations }
										/>
										{/* <ButtonBit
											title={'Get recommendations (Manual)'}
											onPress={
												() => this.onGetRecommendationsManual(
													this.state.imageLink ?
														this.state.imageLink :
														'http://res.cloudinary.com/dh56t7y6j/image/upload/v1621844304/' + this.state.image
												)
											}
										/> */}
										{/* { !!this.state.croppedImages.image &&
											<BoxBit>
												<ImageBit source={ this.state.croppedImages.image }/>
												{`(${this.state.croppedImages.coor.x1},${this.state.croppedImages.coor.y1},${this.state.croppedImages.coor.x2},${this.state.croppedImages.coor.y2})`}
											</BoxBit>
										} */}
									</BoxBit>
								),
							} : null],
						}, {
							data: [!!this.state.croppedImages.image ? {
								children: (
									<BoxBit>
										<ImageBit source={ this.state.croppedImages.image } style={{ width: 100, height: 100}}/>
										{`(${this.state.croppedImages.coor.x1},${this.state.croppedImages.coor.y1},${this.state.croppedImages.coor.x2},${this.state.croppedImages.coor.y2})`}
									</BoxBit>
								),
							} : null],
						},
						{
							data:[{
								title: 'Title',
								children: (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.INPUT }
										placeholder="Input here…"
										value={ this.props.title }
										onChange={ this.onChange.bind(this, 'title') }
									/>
								),
							}, {
								title: 'Slug',
								content: this.props.slug || '-',
								children: this.props.changeable ? (
									<InputValidatedBit
										type={ InputValidatedBit.TYPES.INPUT }
										value={ this.props.slug }
										onChange={ this.onChange.bind(this, 'slug') }
									/>
								) : undefined,
							}],
						}, {
							data: [{
								title: 'Item Count',
								content: this.props.count || 0,
								children: this.props.changeable ? (
									<InputValidatedBit maxlength={ 1 }
										type={ InputValidatedBit.TYPES.INPUT }
										value={ this.props.count }
										validator={ this.validateNumber }
										onChange={ this.onChange.bind(this, 'count') }
									/>
								) : undefined,
							}, {
								title: 'Collections',
								content: this.props.data.collections.filter(c => {
									return this.props.collectionIds.indexOf(c.id) > -1
								}).map(c => c.title).join(', '),
								children: this.props.changeable ? (
									<SelectionMultipleBit options={ this.props.data.collections.map(c => {
										return {
											key: c.id,
											title: c.title,
											selected: this.props.collectionIds.indexOf(c.id) > -1,
										}
									})} onChange={ this.onChangeCollection } />
								) : undefined,
							}],
						}, {
							data: [{
								title: 'Line-up',
								content: this.props.lineup || '-',
								children: this.props.changeable ? (
									<SelectionBit
										options={ LineupHelper.lineups }
										value={ this.props.lineup || 'None' }
										onChange={ this.onChange.bind(this, 'lineup') }
									/>
								) : undefined,
							}],
						}, {
							data: [{
								title: 'Note',
								// content: this.props.note || '-',
								contentStyle: Styles.notes,
								children: (
									<InputValidatedBit
										value={ this.props.note }
										// placeholder={ this.props.readonly && !this.props.value ? '–' : 'Write down your stylist note here…' }
										onChange={ this.onChange.bind(this, 'note') }
										type={ InputValidatedBit.TYPES.TEXTAREA }
										// style={ Styles.container }
										// inputContainerStyle={ Styles.textarea }
										// inputStyle={ Styles.input }
									/>
								),
							}],
						}, {
							children: (
								<RowImageUserLego title="Attachments" addable={ false } id={ this.props.userId } assetIds={ this.props.assetIds } />
							),
						}]}
						contentContainerStyle={ Styles.padder }
						style={ Styles.marginTop }
					/>
				</React.Fragment>
			)
		}
	}
)
