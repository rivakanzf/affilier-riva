import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PromiseStatefulModel from 'coeur/models/promise.stateful';

import TimeHelper from 'coeur/helpers/time';
import UtilitiesContext from 'coeur/contexts/utilities'
import PageContext from 'coeur/contexts/page'

import Colors from 'utils/constants/color';

import IconBit from 'modules/bits/icon';
import NavTextBit from 'modules/bits/nav.text';
import TouchableBit from 'modules/bits/touchable';
import BoxRowLego from 'modules/legos/box.row'
import GeomanistBit from 'modules/bits/geomanist'

import BadgeStatusLego from 'modules/legos/badge.status';
import TableLego from 'modules/legos/table';

import QuickViewStyleboardPagelet from 'modules/pagelets/quick.view.styleboard'

import Styles from './style';


export default ConnectHelper(
	class StyleboardPart extends PromiseStatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.array,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
			PageContext,
		]

		header = [{
			title: 'ID',
			width: 1,
		}, {
			title: 'Title',
			width: 4,
		}, {
			title: 'Client',
			width: 3,
		}, {
			title: 'Status',
			width: 1,
		}, {
			title: 'Created At',
			width: 2,
		}, {
			title: '',
			width: .5,
			align: 'right',
		}]

		onQuickView = id => {
			this.props.utilities.alert.modal({
				component: (
					<QuickViewStyleboardPagelet
						id={ id }
						onNavigate={ this.props.page.navigator.navigate }
					/>
				),
			})
		}

		rowRenderer = styleboard => {
			return {
				data: [{
					title: `#SB-${ styleboard.id }`,
				}, {
					title: styleboard.title,
				}, {
					title: '-',
					children: styleboard.user ? (
						<NavTextBit
							title={ styleboard.user.name }
							link="View Profile"
							href={ `client/${ styleboard.user.id }` }
						/>
					) : undefined,
				}, {
					children: (
						<BadgeStatusLego status={ styleboard.status } />
					),
				}, {
					title: TimeHelper.format(styleboard.created_at, 'DD MMM YYYY HH:mm'),
				}, {
					children: (
						<TouchableBit centering unflex onPress={ this.onQuickView.bind(this, styleboard.id) }>
							<IconBit
								name="quick-view"
								color={ Colors.primary }
							/>
						</TouchableBit>
					),
				}],
			}
		}

		view() {
			return (
				<BoxRowLego
					title={'Styleboard'}
				>
					<TableLego isLoading={ this.state.isLoading }
						headers={ this.header }
						rows={ this.props.data.map(this.rowRenderer) }
						style={ Styles.padder }
					/>
				</BoxRowLego>
			)
		}
	}
)
