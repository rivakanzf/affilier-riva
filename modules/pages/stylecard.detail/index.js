import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'

import StylecardService from 'app/services/stylecard';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';

import TabLego from 'modules/legos/tab';

import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import StylecardVariantListPagelet from 'modules/pagelets/stylecard.variant.list';
import RecommendationsPagelet from 'modules/pagelets/recommendations'

import ActionsPart from './_actions'
import DetailPart from './_detail'
import StylistNotePart from './_stylist.note'
import StyleboardPart from './_styleboard'

import Styles from './style';

import { debounce } from 'lodash';


export default ConnectHelper(
	class StylecardDetailPage extends PageModel {

		static routeName = 'stylecard.detail'

		static stateToProps(state) {
			return {
				token: state.me.token,
				isHeadOrStylist: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist', 'stylist'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				isChanged: false,
				tabIndex: p.tabIndex || 0,
				token: undefined,
				data: {},
				changes: {},
				itemCount: 0,
			})
		}

		roles = ['headstylist', 'stylist', 'analyst']

		tabs = [{
			title: 'General',
			onPress: this.onChangeTab.bind(this, 0),
		}, {
			title: 'Style Boards',
			icon: 'style-sheet',
			onPress: this.onChangeTab.bind(this, 1),
		}]

		componentDidMount() {
			StylecardService.getViewer(this.props.id, this.props.token)
		}

		onAuthorized = token => {
			this.setState({
				token,
				isLoading: true,
			}, () => {
				StylecardService.getStylecardDetailed(this.props.id, this.state.token).then(res => {
					this.setState({
						isLoading: false,
						data: res,
						itemCount: res.variants.length,
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onGetRecommendations = (image, imageLink) => {
			this.utilities.alert.modal({
				component: (
					<RecommendationsPagelet
						onClose={ this.onCloseRecommendationsModal }
						image={ image }
						imageLink={ imageLink }
						stylecardStatus={ this.state.data.status }
						stylecardId={ parseInt(this.props.id, 10) }
						id={this.props.id}
						token={this.props.token}
						itemCount={ this.state.itemCount }
						onAddItem={ this.onAddItem }
					/>
						
				),
			})
		}

		onCloseRecommendationsModal = () => {
			this.utilities.alert.hide()
		}

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			})
		}

		onNavigateToStylecards = () => {
			this.navigator.navigate('stylecard')
		}

		onAddItem = () => {
			this.setState((prevState) => ({
				itemCount: prevState.itemCount + 1,
			}))
		}

		onChanged = (status, changelog, stylistNote) => {
			if (changelog) {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						changelog,
						status,
					},
					changes: {},
				})
			} else if(stylistNote) {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						status,
						stylist_note: stylistNote,
					},
					changes: {},
				})
			} else {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						status,
					},
					changes: {},
				})
			}
		}

		onStylistChanged = (id, name) => {
			this.state.data.stylist = name
		}

		onUpdateData = (key, value) => {
			this.state.changes[key] = value

			if(key === 'stylist_note' || key === 'is_public' || key === 'is_master' || key === 'is_bundle' || key === 'inventory_only' || key === 'is_recomendation' || key === 'image' || key === 'slug' || key === 'collection_ids' || key === 'title') {
				this.onForceUpdate()
			}

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onImageRemoved = () => {
			this.setState({
				data: {
					...this.state.data,
					image: null,
				},
				isChanged: true,
			})
			// delete this.state.data.image
		}

		onForceUpdate = debounce(() => {
			this.forceUpdate()
		}, 500)

		getChanges = () => {
			return this.state.changes
		}

		contentRenderer = index => {
			switch (index) {
			case 0:
			default:
				return (
					<BoxBit unflex row>
						<BoxBit style={ Styles.padder }>
							<DetailPart
								isLoading={ this.state.isLoading }
								id={ this.props.id }
								assetIds={ this.state.data.asset_ids }
								changeable={ this.props.isHeadOrStylist }
								stylist={ this.state.data.stylist }
								title={ this.state.data.title }
								slug={ this.state.data.slug }
								count={ this.state.data.count }
								image={ this.state.data.image }
								note={ this.state.data.note }
								lineup={ this.state.data.lineup }
								createdAt={ this.state.data.created_at }
								collectionIds={ this.state.data.collection_ids }
								onStylistChanged={ this.onStylistChanged }
								onUpdate={ this.onUpdateData }
								onImageRemoved = {this.onImageRemoved}
								onGetRecommendations={ this.onGetRecommendations.bind(this) }
							/>
							<StyleboardPart data={ this.state.data.styleboards } />
						</BoxBit>
						<BoxBit>
							<ActionsPart
								key={ this.state.data.status }
								id={ this.props.id }
								changeable={ this.props.isHeadOrStylist }

								title={ this.state.changes.title === undefined ? this.state.data.title : this.state.changes.title }
								slug={ this.state.changes.slug === undefined ? this.state.data.slug : this.state.changes.slug }
								id={ this.props.id }

								collectionIds={ this.state.changes.collection_ids === undefined ? this.state.data.collection_ids : this.state.changes.collection_ids }

								disabled={ !this.state.isChanged }
								changeGetter={ this.getChanges }
								isBundle={ this.state.changes.is_bundle === undefined ? this.state.data.is_bundle : this.state.changes.is_bundle }
								isPublic={ this.state.changes.is_public === undefined ? this.state.data.is_public : this.state.changes.is_public }
								isMaster={ this.state.changes.is_master === undefined ? this.state.data.is_master : this.state.changes.is_master }
								inventoryOnly={ this.state.changes.inventory_only === undefined ? this.state.data.inventory_only : this.state.changes.inventory_only }
								isRecomendation={ this.state.changes.is_recomendation === undefined ? this.state.data.is_recomendation : this.state.changes.is_recomendation }


								status={ this.state.data.status }
								changelog={ this.state.data.changelog }
								stylistNote={ this.state.data.stylist_note }
								updatedAt={ this.state.data.updated_at }
								onChanged={ this.onChanged }
								
								onToggleBundle={ this.onUpdateData.bind(this, 'is_bundle') }
								onTogglePublic={ this.onUpdateData.bind(this, 'is_public') }
								onToggleMaster={ this.onUpdateData.bind(this, 'is_master') }
								onToggleInventoryOnly={ this.onUpdateData.bind(this, 'inventory_only') }
								onToggleRecomendation={ this.onUpdateData.bind(this, 'is_recomendation') }
							/>
							<StylistNotePart
								readonly={ this.state.data.status !== 'STYLING' }
								value={ this.state.data.stylist_note }
								onChange={ this.onUpdateData.bind(this, 'stylist_note') }
								style={ Styles.pad }
							/>
							<StylecardVariantListPagelet
								id={ this.props.id }
								isBundle={ this.state.changes.is_bundle === undefined ? this.state.data.is_bundle : this.state.changes.is_bundle }
								userNote={ this.state.data.note }
								stylistNote={ this.state.changes.stylist_note || this.state.data.stylist_note }
								onChanged={ this.onChanged }
								style={ Styles.pad }
							/>
						</BoxBit>
					</BoxBit>
				)
			case 1:
				return (
					<StyleboardPart data={ this.state.data.styleboards } />
				)
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{
						title: 'Stylecard',
						onPress: this.onNavigateToStylecards,
					}, {
						title: `#SC-${this.props.id}`,
					}]}
				>
					{() => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent
								title="Stylecards"
							/>
							<TabLego activeIndex={ this.state.tabIndex } tabs={ this.tabs } style={ Styles.tab } />
							{ this.state.isLoading ? (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							) : this.contentRenderer(this.state.tabIndex) }
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
