/* eslint-disable no-nested-ternary */
import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import CommonHelper from 'coeur/helpers/common';
import AuthenticationHelper from 'utils/helpers/authentication'

import StylecardService from 'app/services/stylecard';

import Colors from 'utils/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import InputCheckboxBit from 'modules/bits/input.checkbox';
import LoaderBit from 'modules/bits/loader';
import NavTextTitleBit from 'modules/bits/nav.text.title';
import SelectionBit from 'modules/bits/selection'

import BadgeStatusLego from 'modules/legos/badge.status';
import SelectStatusLego from 'modules/legos/select.status';
import SelectStylistLego from 'modules/legos/select.stylist';
import TableLego from 'modules/legos/table';

import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import QuickCreateStylecardPagelet from 'modules/pagelets/quick.create.stylecard'

import Styles from './style';

import { isEqual, isNil, isArray } from 'lodash';

export default ConnectHelper(
	class StylecardPage extends PageModel {

		static routeName = 'stylecard'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist', 'stylist', 'analyst', 'fulfillment'),
			}
		}

		constructor(p) {
			const query = CommonHelper.getQueryString()
			super(p, {
				isLoading: false,
				isUpdatingData: [],
				offset: query.offset
					? isArray(query.offset)
						? query.offset[0]
						: query.offset
					: 0,
				count: 60,
				total: 0,
				search: query.search || null,
				stylecards: [],
				filter: {
					status: query.status ? query.status.toUpperCase() : null,
					master: query.master || null,
					type: query.type || null,
				},
				token: null,
			})

			this.getterId = 1
		}

		tableHeader = [{
			title: 'ID',
			width: .8,
		}, {
			title: 'Title',
			width: 2.5,
		}, {
			title: 'Collection',
			width: 1.5,
		}, {
			title: 'Stylist',
			width: 1.5,
		}, {
			title: 'Show',
			width: .5,
			align: 'center',
		}, {
			title: 'Is Master',
			width: .5,
			align: 'center',
		}, {
			title: 'Status',
			width: 1,
			align: 'right',
		}]

		stylecardType = [
			{
				title: 'Pre-Styled (Will hide automatically if any variant is not available)',
				key: 'prestyled',
				selected: this.state.filter.type === 'prestyled' ? true : false,
				// selected: (this.props.isBundle && this.props.inventoryOnly && !this.props.isRecomendation),
			},
			{
				title: 'Regular Bundle (Mainly will be displayed on Journal, displat the longest shipping option)',
				key: 'regularBundle',
				selected: this.state.filter.type === 'regularBundle' ? true : false,
				// selected: (this.props.isBundle && !this.props.inventoryOnly && !this.props.isRecomendation),
			},
			{
				title: 'Regular (General use and Styleboard-Request)',
				key: 'regular',
				selected: this.state.filter.type === 'regular' ? true : false,
				// selected: (!this.props.isBundle && !this.props.inventoryOnly && !this.props.isRecomendation),
			},
			{
				title: 'Whatsapp Weekly Recommendation',
				key: 'weeklyRecommendation',
				selected: this.state.filter.type === 'weeklyRecommendation' ? true : false,
				// selected: this.props.isRecomendation,
			},
			{
				title: 'All styleboard',
				key: 'all',
				selected: this.state.filter.type === 'all' ? true : false,
			},
		]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token && (
					!isEqual(pS.offset, this.state.offset)
					|| !isEqual(pS.search, this.state.search)
					|| !isEqual(pS.filter, this.state.filter)
				)
			) {
				const { offset, search, filter } = this.state
				const query = {
					offset,
					...(search && { search }),
					...(filter.status && { status: filter.status.toLowerCase() }),
					...(filter.type && { type: filter.type }),
					...(filter.master && { master: filter.master }),
				}

				window.history.pushState({}, '', `/stylecard?${Object.keys(query).map(key => `${key}=${query[key]}`).join('&')}`)

				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getFilterStylecard = type => {
			let filterStylecard
			if(type === 'prestyled') {
				filterStylecard = {
					is_public: true,
					is_recomendation: false,
					have_collection: true,
					inventory_only: true,
					is_bundle: true,
				}
			} else if(type === 'regularBundle') {
				filterStylecard = {
					is_public: false,
					is_recomendation: false,
					have_collection: null,
					inventory_only: false,
					is_bundle: false,
				}
			} else if(type === 'regular') {
				filterStylecard = {
					is_public: false,
					is_recomendation: false,
					have_collection: false,
					inventory_only: false,
					is_bundle: null,
				}
			} else if(type === 'weeklyRecommendation') {
				filterStylecard = {
					is_public: null,
					is_recomendation: true,
					have_collection: null,
					inventory_only: null,
					is_bundle: null,
				}
			} else {
				filterStylecard = {
					is_public: null,
					is_recomendation: null,
					have_collection: null,
					inventory_only: null,
					is_bundle: null,
				}
			}

			return filterStylecard
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				const filterStylecard = this.getFilterStylecard(filter.type)

				StylecardService.getStylecards({
					offset,
					limit: count,
					...(!!search && { search }),
					...(!this.props.isManager && { mine: true }),
					with_variant_shallow: true,
					with_variant: true,
					with_collection: true,
					...(filter.status && { status: filter.status.toUpperCase() }),
					...(filter.master && { is_master: filter.master }),
					...(!isNil(filterStylecard.is_public) && {
						is_public: filterStylecard.is_public,
					}),
					...(!isNil(filterStylecard.is_recomendation) && {
						is_recomendation: filterStylecard.is_recomendation,
					}),
					...(!isNil(filterStylecard.have_collection) && {
						have_collection: filterStylecard.have_collection,
					}),
					// dari awal, inventory_only dan is_bunlde gak dipake. Belum tau kenapa.
					...(!isNil(filterStylecard.inventory_only) && {
						inventory_only: filterStylecard.inventory_only,
					}),
					// dari awal memang is_bunlde
					...(!isNil(filterStylecard.is_bundle) && {
						is_bundle: filterStylecard.is_bundle,
					}),
				}, this.state.token).then(res => {
					if(id === this.getterId) {
						this.setState({
							isLoading: false,
							total: res.count,
							stylecards: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			this.setState(state => ({
				offset: 0,
				filter: {
					...state.filter,
					[key]: val,
				},
			}))
		}

		onChangeStylist = (stylecard, key, val) => {
			this.setState({
				isUpdatingData: [...this.state.isUpdatingData, stylecard.id],
			}, () => {
				StylecardService.assignStylist(stylecard.id, key, this.state.token).then(isUpdated => {
					if (isUpdated) {
						stylecard.stylist = val

						this.utilities.notification.show({
							title: 'Success',
							message: `Stylist assigned to stylecard #${ stylecard.id }`,
							type: 'SUCCESS',
						})
					} else {
						throw new Error('Update false')
					}
				}).catch(err => {
					this.warn(err)

					this.utilities.notification.show({
						title: 'Oops',
						message: 'Something went wrong with your request, please try again later',
					})
				}).finally(() => {
					this.setState({
						isUpdatingData: this.state.isUpdatingData.filter(id => id !== stylecard.id),
					})
				})
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToDetail = id => {
			this.navigator.navigate(`stylecard/${ id }`)
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onCreateStylecard = () => {
			if (this.props.isManager) {
				this.utilities.menu.show({
					title: 'Select style card type',
					actions: [{
						title: 'Default style card',
						type: this.utilities.menu.TYPES.ACTION.RADIO,
						onPress: () => {
							this.utilities.alert.modal({
								component: (
									<QuickCreateStylecardPagelet
										onCreated={ this.getData }
									/>
								),
							})
						},
					}, {
						title: 'Pre-style',
						type: this.utilities.menu.TYPES.ACTION.RADIO,
						onPress: () => {
							this.utilities.alert.modal({
								component: (
									<QuickCreateStylecardPagelet
										type="PRESTYLED"
										onCreated={ this.getData }
									/>
								),
							})
						},
					}],
				})
			} else {
				this.utilities.alert.modal({
					component: (
						<QuickCreateStylecardPagelet
							onCreated={ this.getData }
						/>
					),
				})
			}
		}

		rowRenderer = stylecard => {
			return {
				data: [{
					title: `#SC-${ stylecard.id }`,
				}, {
					children: (
						<NavTextTitleBit
							title={ stylecard.title || '-' }
							description={ stylecard.slug || '-' }
						/>
					),
				}, {
					title: stylecard.collections.map(c => c.title).join(', ') || '-',
				}, {
					// eslint-disable-next-line no-nested-ternary
					children: this.props.isManager
						? this.state.isUpdatingData.indexOf(stylecard.id) === -1
							? (
								<SelectStylistLego isRequired
									placeholder="-"
									stylist={ stylecard.stylist }
									onChange={ this.onChangeStylist.bind(this, stylecard) }
									style={ Styles.stylist }
								/>
							)
							: (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							)
						: undefined,
				}, {
					children: stylecard.show ? (
						<IconBit
							name="circle-checkmark"
							color={ Colors.green.palette(1) }
							size={ 20 }
						/>
					) : (
						<IconBit
							name="circle-null"
							color={ Colors.solid.grey.palette(5) }
							size={ 20 }
						/>
					),
				}, {
					children: stylecard.is_master ? (
						<IconBit
							name="circle-checkmark"
							color={ Colors.green.palette(1) }
							size={ 20 }
						/>
					) : (
						<IconBit
							name="circle-null"
							color={ Colors.solid.grey.palette(5) }
							size={ 20 }
						/>
					),
				}, {
					children: (
						<BadgeStatusLego status={ stylecard.status } />
					),
				}],
				onPress: this.onGoToDetail.bind(this, stylecard.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={['stylist', 'headstylist', 'analyst']}
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Stylecard' }]}
				>
					{() => (
						<BoxBit style={ Styles.main }>
							<HeaderPageComponent
								title="Style Card"
								searchPlaceholder={ 'ID or Stylist' }
								search={ this.state.search }
								onSearch={ this.onSearch }
								buttonPlaceholder={ 'Create Style Card' }
								onPress={ this.onCreateStylecard }
							/>
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								<SelectStatusLego
									emptyTitle="All Status"
									type={ SelectStatusLego.TYPES.STYLESHEET_STATUSES }
									status={ this.state.filter.status }
									onChange={ this.onChangeFilter.bind(this, 'status') }
								/>
								<SelectionBit
									options={this.stylecardType}
									placeholder={'Select Stylecard Type'}
									onChange={ this.onChangeFilter.bind(this, 'type') }
								/>
								{/* <BoxBit style={ Styles.center }>
									<InputCheckboxBit title="Prestyled" description="Display have collection only" value={ this.state.filter.collection } onChange={ this.onChangeFilter.bind(this, 'collection') } />
								</BoxBit> */}
								{/* <BoxBit style={ Styles.center }>
									<InputCheckboxBit title="Public" description="Display public style cards only" value={ this.state.filter.public } onChange={ this.onChangeFilter.bind(this, 'public') } />
								</BoxBit> */}
								<BoxBit style={ Styles.center }>
									<InputCheckboxBit title="Master" description="Display master style cards only" value={ this.state.filter.master } onChange={ this.onChangeFilter.bind(this, 'master') } />
								</BoxBit>
								{/* <BoxBit style={ Styles.center }>
									<InputCheckboxBit title="Weekly Recommendation" description="Display weekly recommendation only" value={ this.state.filter.is_recomendation } onChange={ this.onChangeFilter.bind(this, 'is_recomendation') } />
								</BoxBit> */}
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.tableHeader }
								rows={ this.state.stylecards.map(this.rowRenderer) }
								style={ Styles.padder } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange } />
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
