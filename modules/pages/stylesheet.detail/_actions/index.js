import React from 'react';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import UtilitiesContext from 'coeur/contexts/utilities';

import StylesheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ClipboardBit from 'modules/bits/clipboard';
import GeomanistBit from 'modules/bits/geomanist';

import BoxRowLego from 'modules/legos/box.row';
import SelectStatusLego from 'modules/legos/select.status';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import StylistNotePagelet from 'modules/pagelets/stylist.note';

import Styles from './style';


export default ConnectHelper(
	class ActionsPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				changeable: PropTypes.bool,

				disabled: PropTypes.bool,
				changeGetter: PropTypes.func,

				status: PropTypes.string,
				note: PropTypes.string,
				stylistNote: PropTypes.string,
				updatedAt: PropTypes.string,
				onChanged: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me.fullName,
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isChanging: false,
				status: p.status,
			})
		}

		disabledStatuses = () => {
			switch(this.props.status) {
			case 'PENDING':
				return [					  'PUBLISHED',	'APPROVED'	]
			case 'STYLING':
				return ['PENDING', 							'APPROVED'	]
			case 'PUBLISHED':
				return ['PENDING',							'APPROVED'	]
			case 'APPROVED':
				return ['PENDING', 'STYLING', 'PUBLISHED',  'APPROVED'	]
			case 'EXCEPTION':
				return this.props.changeable ? ['PENDING', 'APPROVED'] : ['PENDING', 'STYLING', 'PUBLISHED', 'APPROVED']
			default:
				return []
			}
		}

		// action() {
		// 	if(this.props.changeable) {
		// 		return 'Update Stylesheet'
		// 	} else {
		// 		switch (this.props.status) {
		// 		case 'PENDING':
		// 			return 'Start Styling'
		// 		case 'STYLING':
		// 			return 'Submit'
		// 		case 'PUBLISHED':
		// 		case 'APPROVED':
		// 			return 'Reopen'
		// 		case 'EXCEPTION':
		// 		default:
		// 			return 'Update Stylesheet'
		// 		}
		// 	}
		// }

		buttonState() {
			if (this.state.isChanging) {
				return ButtonBit.TYPES.STATES.LOADING
			} else if (this.props.disabled && this.props.status === this.state.status) {
				return ButtonBit.TYPES.STATES.DISABLED
			} else {
				return ButtonBit.TYPES.STATES.NORMAL
			}
		}

		onModalRequestClose = () => {
			this.setState({
				isChanging: false,
			})

			this.props.utilities.alert.hide()
		}

		onChangeStatus = status => {
			this.setState({
				status,
			})
		}

		onUpdateData = () => {
			this.setState({
				isChanging: true,
			}, () => {
				return StylesheetService
					.update(this.props.id, {
						...this.props.changeGetter(),
					}, undefined, this.props.token)
					.then(() => {
						this.props.utilities.notification.show({
							title: 'Success',
							message: 'Data updated.',
							type: this.props.utilities.notification.TYPES.SUCCESS,
						})

						this.setState({
							isChanging: false,
						})
					})
					.catch(this.onError)
			})
		}

		onUpdate = () => {
			if(this.props.changeable) {
				// can change into anything
				if(this.state.status === 'PUBLISHED') {
					// publishing or approving, need to set stylist note
					this.props.utilities.alert.modal({
						component: (
							<StylistNotePagelet value={ this.props.changeGetter().stylist_note || this.props.stylistNote } onClose={ this.onModalRequestClose } onSubmit={ this.onPublishStylesheet.bind(this, this.state.status) } />
						),
					})
				} else {
					this.props.utilities.alert.modal({
						component: (
							<ModalPromptPagelet
								title="Add note"
								message="Short, descriptive note on why you made this change."
								placeholder="Input here"
								onCancel={this.onModalRequestClose}
								onConfirm={this.onConfirmChangeStatus}
							/>
						),
					})
				}
			} else {
				// forward only
				switch(this.state.status) {
				case 'STYLING':
					// start styling
					if(this.props.status === 'PUBLISHED') {
						// back to styling
						this.props.utilities.alert.modal({
							component: (
								<ModalPromptPagelet
									title="Why?"
									message="Short, descriptive note on why you reopen this stylesheet."
									placeholder="Input here"
									onCancel={this.onModalRequestClose}
									onConfirm={this.onStartStyling}
								/>
							),
						})
					} else if(this.props.status === 'PENDING') {
						this.onStartStyling()
					} else {
						this.props.utilities.notification.show({
							title: 'Oops…',
							message: 'Status invalid. Contact administrator.',
						})
					}
					break
				case 'PUBLISHED':
					// submitting
					this.props.utilities.alert.modal({
						component: (
							<StylistNotePagelet value={ this.props.changeGetter().stylist_note || this.props.stylistNote } onClose={ this.onModalRequestClose } onSubmit={ this.onPublishStylesheet.bind(this, 'PUBLISHED') } />
						),
					})
					break
				case 'PENDING':
				case 'APPROVED':
				case 'EXCEPTION':
				default:
					this.props.utilities.notification.show({
						title: 'Oops…',
						message: 'Status invalid. Contact administrator.',
					})

					break
				}
			}
		}

		onStartStyling = note => {
			this.setState({
				isChanging: true,
			}, () => {
				return Promise.resolve().then(() => {
					if (this.props.disabled) {
						// No data changes
						return StylesheetService.setStylesheetStatus(this.props.id, 'STYLING', note, this.props.token)
					} else {
						return StylesheetService
							.update(this.props.id, {
								...this.props.changeGetter(),
								status: 'STYLING',
							}, note, this.props.token)
					}
				}).then(() => {
					this.props.utilities.notification.show({
						title: 'Go…Go…Go…',
						message: `Happy styling, ${this.props.me} 🤗`,
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.setState({
						isChanging: false,
					}, () => {
						this.props.onChanged &&
						this.props.onChanged('STYLING', note)
					})
				}).catch(this.onError)
			})
		}

		onPublishStylesheet = (status, stylistNote) => {
			return Promise.resolve().then(() => {

				this.setState({
					isChanging: true,
				})

				if(this.props.disabled) {
					// No data change
					return StylesheetService.publish(this.props.id, status, stylistNote, {}, this.props.token)
				} else {
					return StylesheetService
						.update(this.props.id, {
							...this.props.changeGetter(),
							status,
							stylist_note: stylistNote,
						}, undefined, this.props.token)
				}
			}).then(() => {
				this.props.utilities.notification.show({
					title: 'Congratulations 🎉🎉🎉',
					message: 'One stylesheet down. Let\'s keep the spirit up!',
					type: this.props.utilities.notification.TYPES.SUCCESS,
				})

				this.onModalRequestClose()

				this.setState({
					isChanging: false,
				}, () => {
					this.props.onChanged &&
					this.props.onChanged(status, undefined, stylistNote)
				})
			}).catch(this.onError)
		}

		onConfirmChangeStatus = note => {
			this.setState({
				isChanging: true,
			}, () => {
				return Promise.resolve().then(() => {
					if(this.props.disabled) {
						// No data change
						return StylesheetService.setStylesheetStatus(this.props.id, this.state.status, note, this.props.token)
					} else {
						return StylesheetService
							.update(this.props.id, {
								...this.props.changeGetter(),
								status: this.state.status,
							}, note, this.props.token)
					}
				}).then(() => {
					this.props.utilities.notification.show({
						title: 'Status changed',
						message: 'Successfully change status.',
						type: this.props.utilities.notification.TYPES.SUCCESS,
					})

					this.setState({
						isChanging: false,
					}, () => {
						this.props.onChanged &&
						this.props.onChanged(this.state.status, note)
					})
				}).catch(this.onError)
			})
		}

		onError = err => {
			this.warn(err)

			this.props.utilities.notification.show({
				title: 'Oops…',
				message: err ? err.detail || err.message || err : 'Something went wrong, please try again later.',
				timeout: 10000,
			})

			this.setState({
				isChanging: false,
			})
		}

		view() {
			return (
				<BoxRowLego
					title="Actions"
					data={[{
						data: [{
							title: 'Stylesheet Status',
							children: (
								<BoxBit unflex>
									<SelectStatusLego disabled={ !this.props.changeable } key={this.state.updater} unflex isRequired
										type={SelectStatusLego.TYPES.STYLESHEET_STATUSES}
										status={ this.props.status }
										disabledStatuses={this.disabledStatuses()}
										onChange={this.onChangeStatus}
									/>
									<GeomanistBit
										type={GeomanistBit.TYPES.NOTE_1}
										style={Styles.time}
									>
										Last updated: {
											this.props.updatedAt
												? TimeHelper.format(this.props.updatedAt, 'DD MMMM YYYY HH:mm')
												: '-'
										}
									</GeomanistBit>
								</BoxBit>
							),
						}, {
							title: 'Save Changes',
							children: (
								<ButtonBit
									weight="medium"
									title={ 'UPDATE STYLESHEET' }
									type={ ButtonBit.TYPES.SHAPES.PROGRESS }
									state={ this.buttonState() }
									onPress={ this.props.status === this.state.status ? this.onUpdateData : this.onUpdate }
								/>
							),
						}],
					}, {
						data: [{
							title: 'Last Change Log',
							children: (
								<ClipboardBit autogrow={44} value={ this.props.note || '-' } style={ Styles.clipboard } />
							),
						}],
					}]}
				/>
			)
		}
	}
)
