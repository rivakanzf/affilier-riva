import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import TimeHelper from 'coeur/helpers/time';
import LineupHelper from 'utils/helpers/lineup';

import UtilitiesContext from 'coeur/contexts/utilities';

import StyleSheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import InputCurrencyBit from 'modules/bits/input.currency';
import InputValidatedBit from 'modules/bits/input.validated';
import LoaderBit from 'modules/bits/loader';
import NavTextBit from 'modules/bits/nav.text';
import SelectionBit from 'modules/bits/selection';

// import BadgeStatusLego from 'modules/legos/badge.status';
import BoxRowLego from 'modules/legos/box.row';
import SelectStylistLego from 'modules/legos/select.stylist';
import RowImageUserLego from 'modules/legos/row.image.user';

import ExchangeDetailsPagelet from 'modules/pagelets/exchange.details';

import Styles from './style';


export default ConnectHelper(
	class DetailPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				replacingId: PropTypes.id,
				userId: PropTypes.id,
				assetIds: PropTypes.arrayOf(PropTypes.id),
				changeable: PropTypes.bool,
				title: PropTypes.string,
				client: PropTypes.string,
				stylist: PropTypes.string,
				type: PropTypes.string,
				count: PropTypes.number,
				note: PropTypes.string,
				lineup: PropTypes.string,
				value: PropTypes.number,
				minValue: PropTypes.number,
				realValue: PropTypes.number,
				assets: PropTypes.array,
				shipmentAt: PropTypes.string,
				onStylistChanged: PropTypes.func,
				onUpdate: PropTypes.func,
			}
		}

		static stateToProps(state) {
			return {
				token: state.me.token,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isUpdating: false,
			})
		}

		types = [{
			key: 'FIRST',
			title: 'First Stylesheet',
		}, {
			key: 'REPEAT',
			title: 'Repeat Stylesheet',
		}, {
			key: 'EXCHANGE',
			title: 'Exchange Stylesheet',
		}]

		validateNumber = input => {
			return !!+input
		}

		onChange = (key, value) => {
			this.props.onUpdate &&
			this.props.onUpdate(key, +value ? +value : value)
		}

		onChangeStylist = (key, val) => {
			this.setState({
				isUpdatingData: true,
			}, () => {
				StyleSheetService.assignStylist(this.props.id, key, this.props.token).then(isUpdated => {
					if (isUpdated) {
						this.props.utilities.notification.show({
							title: 'Success',
							message: `Stylist assigned to stylesheet #${this.props.id}`,
							type: 'SUCCESS',
						})

						this.props.onStylistChanged &&
						this.props.onStylistChanged(key, val)
					} else {
						throw new Error('Update false')
					}
				}).catch(err => {
					this.warn(err)

					this.props.utilities.notification.show({
						title: 'Oops',
						message: 'Something went wrong with your request, please try again later',
					})
				}).finally(() => {
					this.setState({
						isUpdating: false,
					})
				})
			})
		}

		view() {
			return (
				<React.Fragment>
					<BoxRowLego
						title={`Stylesheet #ST-${this.props.id}`}
						data={[{
							data: [{
								title: 'Client',
								content: '-',
								children: this.props.client ? (
									<NavTextBit title={this.props.client} link="View profile" href={`client/${this.props.userId}`} />
								) : undefined,
							}, {
								title: 'Stylist',
								content: '-',
								// eslint-disable-next-line no-nested-ternary
								children: this.props.changeable ? this.state.isUpdating ? (
									<BoxBit centering>
										<LoaderBit simple />
									</BoxBit>
								) : (
									<SelectStylistLego stylist={ this.props.stylist } onChange={ this.onChangeStylist } />
								) : this.props.client ? (
									<NavTextBit title={this.props.stylist} link="View style sheets" href={'stylesheet'} param={{ search: this.props.client }} />
								) : undefined,
							}],
						}, {
							data: [{
								title: 'Product',
								content: this.props.title,
							}, {
								title: 'Shipment Schedule',
								content: this.props.shipmentAt ? TimeHelper.format(this.props.shipmentAt, 'DD MMMM YYYY') : '-',
							}],
						}]}
						contentContainerStyle={Styles.padder}
					/>
					<BoxRowLego
						title={'Detail'}
						data={[
							{
								data:[{
									title: 'Title',
									children: (
										<InputValidatedBit
											type={ InputValidatedBit.TYPES.INPUT }
											placeholder="Input here…"
											value={ this.props.title }
											onChange={ this.onChange.bind(this, 'title') }
										/>
									),
								}],
							},
							{
								data: [
									{
										title: 'Item Count',
										content: this.props.count || 0,
										children: this.props.changeable ? (
											<InputValidatedBit maxlength={1}
												type={ InputValidatedBit.TYPES.INPUT }
												value={ this.props.count }
												validator={ this.validateNumber }
												onChange={ this.onChange.bind(this, 'count') }
											/>
										) : undefined,
									}, {
										title: 'Stylesheet Type',
										content: this.props.type || '-',
										children: this.props.changeable ? (
											<SelectionBit disabled
												options={ this.types }
												value={ this.props.type || null }
												// onChange={ this.onChange.bind(this, 'type') }
											/>
										) : undefined,
									}],
							}, {
								data: [{
									title: 'Line-up',
									content: this.props.lineup || '-',
									children: this.props.changeable ? (
										<SelectionBit
											options={ LineupHelper.lineups }
											value={ this.props.lineup || 'None' }
											onChange={ this.onChange.bind(this, 'lineup') }
										/>
									) : undefined,
								}],
							}, this.props.changeable ? {
								data: [{
									title: 'Value',
									description: 'Desirable total retail value',
									children: (
										<InputCurrencyBit
											value={ this.props.value }
											onChange={ this.onChange.bind(this, 'value') }
										/>
									),
								}, {
									title: 'Minimum Value',
									description: 'Minimum total retail value that must be met',
									children: (
										<InputCurrencyBit
											value={ this.props.minValue }
											onChange={ this.onChange.bind(this, 'min_value') }
										/>
									),
								}],
							} : null, this.props.changeable ? {
								data: [{
									title: 'Real Value',
									description: 'Maximum total purchase value',
									children: (
										<InputCurrencyBit
											value={ this.props.realValue }
											onChange={ this.onChange.bind(this, 'real_value') }
										/>
									),
								}, {
									blank: true,
								}],
							} : null, {
								data: [{
									title: 'Note',
									content: this.props.note || '-',
									contentStyle: Styles.notes,
								}],
							}, {
								children: (
									<RowImageUserLego title="Attachments" addable={ false } id={ this.props.userId } assetIds={ this.props.assetIds } />
								),
							}]}
						contentContainerStyle={Styles.padder}
						style={Styles.marginTop}
					/>
					{ this.props.replacingId ? (
						<ExchangeDetailsPagelet id={this.props.replacingId} readonly title="Previous Feedback" tab="exchanged" style={Styles.marginTop} />
					) : false }
				</React.Fragment>
			)
		}
	}
)
