import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	padder: {
		marginBottom: 0,
		marginTop: 0,
		marginLeft: 0,
		marginRight: 0,
	},
	content: {

	},
	counter: {
		color: Colors.black.palette(2, .6),
	},
	notes: {
		// backgroundColor: Colors.grey.palette(1, .04),
		// paddingLeft: Sizes.margin.default,
		// paddingRight: Sizes.margin.default,
		// paddingTop: 12,
		// paddingBottom: 12,
		minHeight: 88,
	},
	container: {
		flexGrow: 1,
	},
	textarea: {
		borderWidth: 0,
	},
	input: {
		backgroundColor: Colors.grey.palette(5),
		maxHeight: '100%',
		paddingBottom: 12,
		paddingTop: 12,
	},
})
