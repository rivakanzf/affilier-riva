import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import AuthenticationHelper from 'utils/helpers/authentication'

import StyleSheetService from 'app/services/style.sheets';

// import UserManager from 'app/managers/user';
// import OrderDetailManager from 'app/managers/order.detail';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';

import TabLego from 'modules/legos/tab';

import PageAuthorizedComponent from 'modules/components/page.authorized';
import HeaderPageComponent from 'modules/components/header.page';

import ExchangeDetailsPagelet from 'modules/pagelets/exchange.details';
import ImagesUserPagelet from 'modules/pagelets/images.user';
import StylesheetInventoryListPagelet from 'modules/pagelets/stylesheet.inventory.list';
import UserAnswersPagelet from 'modules/pagelets/user.answers';
import UserNotePagelet from 'modules/pagelets/user.note';
import UserStylesheetPagelet from 'modules/pagelets/user.stylesheet';

import ActionsPart from './_actions'
import DetailPart from './_detail'
import StylistNotePart from './_stylist.note'
// import StyleHistoryPart from 'modules/pages/user.detail/_style.history';
// import {
// 	StyleProfilePart,
// } from 'modules/pages/user.detail';

import Styles from './style';

import { debounce } from 'lodash';


export default ConnectHelper(
	class StyleSheetsDetailPage extends PageModel {

		static routeName = 'stylesheet.detail'

		static stateToProps(state) {
			return {
				isHeadStylist: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				isChanged: false,
				tabIndex: p.tabIndex || 0,
				token: undefined,
				data: {},
				changes: {},
			})
		}

		roles = ['headstylist', 'stylist', 'fulfillment', 'analyst']

		tabs = [{
			title: 'General',
			onPress: this.onChangeTab.bind(this, 0),
		}, {
			title: 'Style Profile',
			icon: 'style-profile',
			onPress: this.onChangeTab.bind(this, 1),
		}, {
			title: 'Style History',
			icon: 'style-sheet',
			onPress: this.onChangeTab.bind(this, 2),
		}, {
			title: 'User Notes',
			icon: 'edit',
			onPress: this.onChangeTab.bind(this, 3),
		}, {
			title: 'User Images',
			icon: 'images',
			onPress: this.onChangeTab.bind(this, 4),
		}]

		onAuthorized = token => {
			this.setState({
				token,
				isLoading: true,
			}, () => {
				StyleSheetService.getStyleSheetDetail(this.props.id, this.state.token).then(res => {
					this.setState({
						isLoading: false,
						data: res,
					})
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			})
		}

		onNavigateToStylesheets = () => {
			this.navigator.navigate('stylesheet')
		}

		onChanged = (status, changelog, stylistNote) => {
			if (changelog) {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						changelog,
						status,
					},
					changes: {},
				})
			} else if(stylistNote) {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						status,
						stylist_note: stylistNote,
					},
					changes: {},
				})
			} else {
				this.setState({
					data: {
						...this.state.data,
						...this.state.changes,
						status,
					},
					changes: {},
				})
			}
		}

		onStylistChanged = (id, name) => {
			this.state.data.stylist = name
		}

		onUpdateData = (key, value) => {
			this.state.changes[key] = value

			if(key === 'stylist_note') {
				this.onForceUpdate()
			}

			if(!this.state.isChanged) {
				this.setState({
					isChanged: true,
				})
			}
		}

		onForceUpdate = debounce(() => {
			this.forceUpdate()
		}, 500)

		getChanges = () => {
			return this.state.changes
		}

		contentRenderer = index => {
			switch (index) {
			case 0:
			default:
				return (
					<BoxBit unflex row>
						<BoxBit style={Styles.padder}>
							<DetailPart
								id={ this.props.id }
								replacingId={ this.state.data.replacing_id }
								userId={ this.state.data.client_id }
								assetIds={ this.state.data.asset_ids }
								changeable={ this.props.isHeadStylist }
								type={ this.state.data.type }
								client={ this.state.data.client }
								stylist={ this.state.data.stylist }
								title={ this.state.data.title }
								count={ this.state.data.count }
								note={ this.state.data.note }
								lineup={ this.state.data.lineup }
								value={ this.state.data.value }
								minValue={ this.state.data.min_value }
								realValue={ this.state.data.real_value }
								shipmentAt={ this.state.data.shipment_at }
								onStylistChanged={ this.onStylistChanged }
								onUpdate={ this.onUpdateData }
							/>
						</BoxBit>
						<BoxBit>
							<ActionsPart
								key={ this.state.data.status }
								id={ this.props.id }
								changeable={ this.props.isHeadStylist }

								disabled={ !this.state.isChanged }
								changeGetter={ this.getChanges }

								status={ this.state.data.status }
								note={ this.state.data.changelog }
								stylistNote={ this.state.data.stylist_note }
								updatedAt={ this.state.data.updated_at }
								onChanged={ this.onChanged }
							/>
							<StylistNotePart
								readonly={ this.state.data.status !== 'STYLING' }
								value={ this.state.data.stylist_note }
								onChange={ this.onUpdateData.bind(this, 'stylist_note') }
								style={ Styles.pad }
							/>
							{ this.state.data.exchange_id ? (
								<ExchangeDetailsPagelet readonly
									id={ this.state.data.exchange_id }
									title={ 'Feedback' }
									style={ Styles.pad }
								/>
							) : (
								<StylesheetInventoryListPagelet
									key={ `inventory${this.state.data.status}` }
									id={ this.props.id }
									userName={ this.state.data.client }
									userNote={ this.state.data.note }
									stylistNote={ this.state.changes.stylist_note || this.state.data.stylist_note }
									onChanged={ this.onChanged }
									style={ Styles.pad }
								/>
							) }
						</BoxBit>
					</BoxBit>
				)
			case 1:
				return (
					<UserAnswersPagelet id={ this.state.data.client_id } date={ this.state.data.started_at || undefined } />
				)
			case 2:
				return (
					<UserStylesheetPagelet id={ this.state.data.client_id } />
				)
			case 3:
				return (
					<UserNotePagelet id={ this.state.data.client_id } />
				)
			case 4:
				return (
					// TODO, FIX ADDABLE
					<ImagesUserPagelet id={ this.state.data.client_id } />
				)
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={ this.onAuthorized }
					paths={[{
						title: 'Stylesheets',
						onPress: this.onNavigateToStylesheets,
					}, {
						title: `#ST-${this.props.id}`,
					}]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Stylesheets"
							/>
							<TabLego activeIndex={this.state.tabIndex} tabs={this.tabs} style={Styles.tab} />
							{ this.state.isLoading ? (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							) : this.contentRenderer(this.state.tabIndex) }
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
