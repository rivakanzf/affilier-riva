import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';
import AuthenticationHelper from 'utils/helpers/authentication'

import StyleSheetService from 'app/services/style.sheets';

import BoxBit from 'modules/bits/box';
import LoaderBit from 'modules/bits/loader';

import BadgeStatusLego from 'modules/legos/badge.status';
import SelectStatusLego from 'modules/legos/select.status';
import SelectStylistLego from 'modules/legos/select.stylist';
import TableLego from 'modules/legos/table';

import HeaderPageComponent from 'modules/components/header.page'
import HeaderFilterComponent from 'modules/components/header.filter'
import PageAuthorizedComponent from 'modules/components/page.authorized'

import Styles from './style';

import { capitalize, isEqual } from 'lodash';


export default ConnectHelper(
	class StylesheetStylistPage extends PageModel {

		static routeName = 'stylesheet.stylist'

		static stateToProps(state) {
			return {
				isManager: AuthenticationHelper.isAuthorized(state.me.roles, 'headstylist'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				isUpdatingData: [],
				offset: 0,
				count: 160,
				total: 0,
				search: p.search || undefined,
				stylesheets: [],
				filter: {
					status: undefined,
					type: undefined,
				},
				token: undefined,
			})

			this.getterId = 1
		}

		tableHeader = [{
			title: 'ID',
			width: .8,
		}, {
			title: 'Product',
			width: 3,
		}, {
			title: 'Type',
			width: 1,
		}, {
			title: 'Client',
			width: 2,
		}, {
			title: 'Stylist',
			width: 2,
		}, {
			title: 'Ship Sched.',
			width: 1,
		}, {
			title: 'Status',
			width: 1.5,
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token && (
					!isEqual(pS.offset, this.state.offset)
					|| !isEqual(pS.search, this.state.search)
					|| !isEqual(pS.filter.status, this.state.filter.status)
					|| !isEqual(pS.filter.type, this.state.filter.type)
				)
			) {
				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			}, this.getData)
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				StyleSheetService.getStyleSheets({
					offset,
					limit: count,
					...(!!search ? { search } : {}),
					...(filter.status ? { status: filter.status } : {}),
					...(filter.type ? { type: filter.type } : {}),
					...(this.props.isManager ? {} : { mine: true }),
				}, this.state.token).then(res => {
					if(id === this.getterId) {
						this.setState({
							isLoading: false,
							total: res.count,
							stylesheets: res.data,
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong with your request, please try again later',
						})
					})
				})
			})
		}

		onModalRequestClose = () => {
			this.utilities.alert.hide()
		}

		onChangeFilter = (key, val) => {
			this.setState({
				offset: 0,
				filter: {
					...this.state.filter,
					[key]: val,
				},
			})
		}

		onChangeStylist = (stylesheet, key, val) => {
			this.setState({
				isUpdatingData: [...this.state.isUpdatingData, stylesheet.id],
			}, () => {
				StyleSheetService.assignStylist(stylesheet.id, key, this.state.token).then(isUpdated => {
					if (isUpdated) {
						stylesheet.stylist = val

						this.utilities.notification.show({
							title: 'Success',
							message: `Stylist assigned to stylesheet #${stylesheet.id}`,
							type: 'SUCCESS',
						})
					} else {
						throw new Error('Update false')
					}
				}).catch(err => {
					this.warn(err)

					this.utilities.notification.show({
						title: 'Oops',
						message: 'Something went wrong with your request, please try again later',
					})
				}).finally(() => {
					this.setState({
						isUpdatingData: this.state.isUpdatingData.filter(id => id !== stylesheet.id),
					})
				})
			})

			// this.utilities.alert.show({
			// 	title: 'Assign Stylist',
			// 	message: `Are you sure want to assign ${val} to stylesheet #${stylesheet.id}?`,
			// 	actions: [{
			// 		title: 'Yes',
			// 		onPress: () => {


			// 		},
			// 	}, {
			// 		title: 'No',
			// 		type: 'CANCEL',
			// 		onPress: () => {
			// 			this.forceUpdate()
			// 		},
			// 	}]
			// })
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onGoToDetail = id => {
			this.navigator.navigate(`stylesheet/${id}`)
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		rowRenderer = stylesheet => {
			return {
				data: [{
					title: `#ST-${stylesheet.id}`,
				}, {
					title: stylesheet.title,
				}, {
					title: capitalize(stylesheet.type),
				}, {
					title: stylesheet.client || '-',
				}, {
					// eslint-disable-next-line no-nested-ternary
					children: this.props.isManager
						? this.state.isUpdatingData.indexOf(stylesheet.id) === -1
							? (
								<SelectStylistLego isRequired
									placeholder="-"
									stylist={ stylesheet.stylist }
									onChange={ this.onChangeStylist.bind(this, stylesheet) }
									style={ Styles.stylist }
								/>
							)
							: (
								<BoxBit centering>
									<LoaderBit simple />
								</BoxBit>
							)
						: undefined,
				}, {
					title: TimeHelper.format(stylesheet.shipment_at, 'DD MMM YYYY'),
				}, {
					children: (
						<BadgeStatusLego status={ stylesheet.status } />
					),
				}],
				onPress: this.onGoToDetail.bind(this, stylesheet.id),
			}
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={['stylist', 'headstylist', 'fulfillment']}
					onAuthorized={ this.onAuthorized }
					paths={[{ title: 'Stylesheet' }]}
				>
					{() => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Stylesheet"
								searchPlaceholder={ 'ID, Client, or Stylist' }
								search={ this.state.search }
								onSearch={this.onSearch} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} >
								<SelectStatusLego emptyTitle="All Type" placeholder="Select Type" type={SelectStatusLego.TYPES.STYLESHEETS} status={this.state.filter.status} onChange={this.onChangeFilter.bind(this, 'type')} />
								<SelectStatusLego emptyTitle="All Status" type={SelectStatusLego.TYPES.STYLESHEET_STATUSES} status={this.state.filter.status} onChange={this.onChangeFilter.bind(this, 'status')} />
							</HeaderFilterComponent>
							<TableLego isLoading={this.state.isLoading}
								headers={this.tableHeader}
								rows={this.state.stylesheets.map(this.rowRenderer)}
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					)}
				</PageAuthorizedComponent>
			)
		}
	}
)
