import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';
import TimeHelper from 'coeur/helpers/time';

import AuthenticationHelper from 'utils/helpers/authentication'

import PurchaseService from 'app/services/purchase';
import StylesheetService from 'app/services/style.sheets';
import InventoryService from 'app/services/inventory';

import Colors from 'coeur/constants/color';
import Linking from 'coeur/libs/linking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import GeomanistBit from 'modules/bits/geomanist';
import IconBit from 'modules/bits/icon';
import InputDateBit from 'modules/bits/input.date';
import LinkBit from 'modules/bits/link';
import SelectionBit from 'modules/bits/selection';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import BadgeStatusLego from 'modules/legos/badge.status';
import BadgeStatusSmallLego from 'modules/legos/badge.status.small';
import ColorLego from 'modules/legos/color';
import SelectStatusLego from 'modules/legos/select.status';
import TableLego from 'modules/legos/table';

import HeaderFilterComponent from 'modules/components/header.filter';
import HeaderPageComponent from 'modules/components/header.page';
import PageAuthorizedComponent from 'modules/components/page.authorized';

import ModalPromptPagelet from 'modules/pagelets/modal.prompt';
import PickerVariantPagelet from 'modules/pagelets/picker.variant';
import PurchaseOrderPicker from 'modules/pagelets/purchase.order.picker';
import PurchaseRequestApprovePagelet from 'modules/pagelets/purchase.request.approve';
import PurchaseRequestUpdatePagelet from 'modules/pagelets/purchase.request.update';
import QuickViewItemPagelet from 'modules/pagelets/quick.view.item';
import QuickViewProductPagelet from 'modules/pagelets/quick.view.product';
import QuickViewOrderPagelet from 'modules/pagelets/quick.view.order';
import QuickViewStylesheetPagelet from 'modules/pagelets/quick.view.stylesheet';

import Styles from './style';

import { capitalize, uniqBy } from 'lodash'


export default ConnectHelper(
	class StylesheetPage extends PageModel {

		static routeName = 'stylesheet'

		static stateToProps(state) {
			return {
				isInventory: AuthenticationHelper.isAuthorized(state.me.roles, 'inventory'),
				isPurchasing: AuthenticationHelper.isAuthorized(state.me.roles, 'purchasing'),
				isFulfillment: AuthenticationHelper.isAuthorized(state.me.roles, 'fulfillment'),
				isPacking: AuthenticationHelper.isAuthorized(state.me.roles, 'packing'),
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				activeId: -1,
				data: [],
				offset: 0,
				count: 40,
				total: 0,
				search: '',
				filter: {
					type: undefined,
					date: undefined,
					status: 'PUBLISHED',
					purchase_request_status: undefined,
					order_detail_status: undefined,
					without_po: 'false',
				},
				token: undefined,
			})

			this.getterId = 1
		}

		roles = ['inventory', 'fulfillment', 'purchasing', 'packing']

		paths = [{
			title: 'Stylesheets',
		}]

		headers = [{
			title: 'Title',
			width: 1.6,
		}, {
			title: 'Stylist',
			width: 1,
		}, {
			title: 'Shipment',
			width: .5,
		}, {
			title: '',
			width: .1,
		}, {
			title: 'Order Detail Status',
			width: .8,
		}, {
			title: '',
			width: 1.4,
		}, {
			title: '',
			width: .2,
		}, {
			title: '',
			width: .2,
		}]

		childHeaders = [{
			title: 'Product',
			width: 1.6,
		}, {
			title: 'Category',
			width: .7,
		}, {
			title: 'Color',
			width: .6,
		}, {
			title: 'Size',
			width: .3,
		}, {
			title: 'Status',
			width: .5,
		}, {
			title: 'Note',
			width: .65,
		}, {
			title: '',
			width: .05,
		}, {
			title: 'Actions',
			width: 1.4,
		}]

		POStatuses = [{
			key: 'true',
			title: 'Without purchase order only',
		}, {
			key: 'false',
			title: 'Do not apply',
		}]

		componentDidUpdate(pP, pS) {
			if (
				this.state.token
				&& (
					pS.token !== this.state.token
					|| pS.search !== this.state.search
					|| pS.filter.type !== this.state.filter.type
					|| pS.filter.date !== this.state.filter.date
					|| pS.filter.status !== this.state.filter.status
					|| pS.filter.purchase_request_status !== this.state.filter.purchase_request_status
					|| pS.filter.order_detail_status !== this.state.filter.order_detail_status
					|| pS.filter.without_po !== this.state.filter.without_po
					|| pS.offset !== this.state.offset
					|| pS.count !== this.state.count
				)
			) {
				this.getData()
			}
		}

		onAuthorized = token => {
			this.setState({
				token,
			})
		}

		actions = inventory => {
			const actions = []

			if (
				this.props.isInventory
			) {
				if (inventory.variant_id) {
					actions.push({
						slug: 'set',
						title: 'Change Variant',
						theme: ButtonBit.TYPES.THEMES.SECONDARY,
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'setting' ? ButtonBit.TYPES.STATES.LOADING : this.setable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onSetVariant.bind(this, inventory),
					}, {
						slug: 'view',
						title: 'View Variant',
						onPress: this.onViewVariant.bind(this, inventory),
					})
				} else {
					actions.push({
						slug: 'set',
						title: 'Set Variant',
						theme: ButtonBit.TYPES.THEMES.SECONDARY,
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'setting' ? ButtonBit.TYPES.STATES.LOADING : this.setable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onSetVariant.bind(this, inventory),
					})
				}
			}

			if(
				this.props.isFulfillment
				&& (
					this.rejectable(inventory) ||
					this.approveable(inventory) ||
					this.cancelable(inventory) ||
					this.refundable(inventory)
				)
			) {
				actions.push({
					slug: 'reject',
					title: 'Reject',
					theme: ButtonBit.TYPES.THEMES.SECONDARY,
					// eslint-disable-next-line no-nested-ternary
					state: inventory.isLoading === 'rejecting' ? ButtonBit.TYPES.STATES.LOADING : this.rejectable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
					onPress: this.onReject.bind(this, inventory),
				}, this.approveable(inventory) ? {
					slug: 'approve',
					title: 'Approve',
					// eslint-disable-next-line no-nested-ternary
					state: inventory.isLoading === 'approving' ? ButtonBit.TYPES.STATES.LOADING : this.approveable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
					onPress: this.onApprove.bind(this, inventory),
				} : {
					slug: 'cancel',
					title: 'Cancel',
					theme: ButtonBit.TYPES.THEMES.SECONDARY,
					// eslint-disable-next-line no-nested-ternary
					state: inventory.isLoading === 'canceling' ? ButtonBit.TYPES.STATES.LOADING : this.cancelable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
					onPress: this.onCancel.bind(this, inventory),
				})

				if (this.refundable(inventory)) {
					actions.push({
						slug: 'refund',
						title: 'Refund',
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'refunding' ? ButtonBit.TYPES.STATES.LOADING : !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onRefund.bind(this, inventory),
					})
				}
			}

			if(
				this.props.isPurchasing
				&& (
					this.cancelable(inventory) ||
					(inventory.purchase_order_id && this.droppable(inventory)) ||
					this.purchaseable(inventory)
				)
			) {
				actions.push({
					slug: 'cancel',
					title: 'Cancel',
					theme: ButtonBit.TYPES.THEMES.SECONDARY,
					// eslint-disable-next-line no-nested-ternary
					state: inventory.isLoading === 'canceling' ? ButtonBit.TYPES.STATES.LOADING : this.cancelable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
					onPress: this.onCancel.bind(this, inventory),
				}, inventory.purchase_order_id && this.droppable(inventory) ? {
					slug: 'revoke',
					title: 'Revoke',
					theme: ButtonBit.TYPES.THEMES.SECONDARY,
					// eslint-disable-next-line no-nested-ternary
					state: inventory.isLoading === 'revoking' ? ButtonBit.TYPES.STATES.LOADING : !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
					onPress: this.onRevoke.bind(this, inventory),
				} : {
					slug: 'purchase',
					title: 'Purchase',
					// eslint-disable-next-line no-nested-ternary
					state: inventory.isLoading === 'purchasing' ? ButtonBit.TYPES.STATES.LOADING : this.purchaseable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
					onPress:  this.onPurchase.bind(this, inventory),
				})
			}

			if(
				this.props.isPacking
				&& (
					this.receiveable(inventory) ||
					this.packable(inventory) ||
					this.unbookable(inventory)
				)
			) {
				if(this.receiveable(inventory)) {
					actions.push({
						slug: 'receive',
						title: 'Receive',
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'receiving' ? ButtonBit.TYPES.STATES.LOADING : !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onReceive.bind(this, inventory),
					}, {
						slug: 'exception',
						title: 'Exception',
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'excepting' ? ButtonBit.TYPES.STATES.LOADING : !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onException.bind(this, inventory),
					})
				} else {
					actions.push(inventory.status === 'PACKED' ? {
						slug: 'unpack',
						title: 'Unpack',
						theme: ButtonBit.TYPES.THEMES.SECONDARY,
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'packing' ? ButtonBit.TYPES.STATES.LOADING : !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onUnpack.bind(this, inventory),
					} : {
						slug: 'pack',
						title: 'Pack',
						// eslint-disable-next-line no-nested-ternary
						state: inventory.isLoading === 'packing' ? ButtonBit.TYPES.STATES.LOADING : this.packable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
						onPress: this.onPack.bind(this, inventory),
					})

					if (!this.refundable(inventory)) {
						actions.push({
							slug: 'change',
							title: 'Please Change',
							theme: ButtonBit.TYPES.THEMES.SECONDARY,
							// eslint-disable-next-line no-nested-ternary
							state: inventory.isLoading === 'changing' ? ButtonBit.TYPES.STATES.LOADING : this.unbookable(inventory) && !inventory.isLoading ? ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED,
							onPress: this.onChange.bind(this, inventory),
						})
					}
				}
			}

			return uniqBy(actions, 'slug')
		}

		status(status) {
			return `[${status.split('_').join(' ')}]`
		}

		changeable(inventory) {
			return !inventory.is_locked
				|| (!inventory.inventory_id && inventory.status !== 'LOCKED')
		}

		setable(inventory) {
			return !inventory.inventory_id
		}

		refundable(inventory) {
			return !inventory.stylesheet_inventory_id && inventory.is_locked
		}

		unbookable(inventory) {
			return inventory.status === 'BOOKED' || inventory.status === 'INVENTORY_RECEIVED' || inventory.status === 'PACKED'
		}

		receiveable(inventory) {
			return inventory.status === 'PURCHASED'
		}

		rejectable(inventory) {
			return inventory.status === 'REQUESTING'
		}

		approveable(inventory) {
			return inventory.status === 'REQUESTING'
		}

		cancelable(inventory) {
			return inventory.status === 'REQUEST_APPROVED'
		}

		purchaseable(inventory) {
			return inventory.status === 'REQUEST_APPROVED'
		}

		droppable(inventory) {
			return inventory.status === 'PURCHASED'
		}

		packable(inventory) {
			return (inventory.inventory_id && inventory.status === 'BOOKED')
				|| inventory.status === 'INVENTORY_RECEIVED'
		}

		getData = () => {
			const {
				offset,
				count,
				filter,
				search,
			} = this.state

			this.getterId++;

			const id = this.getterId

			this.setState({
				isLoading: true,
			}, () => {
				StylesheetService.getStyleSheets({
					offset,
					limit: count,
					search,
					...(filter.type ? { type: filter.type } : {}),
					...(filter.purchase_request_status ? { purchase_request_status: filter.purchase_request_status } : {}),
					...(filter.order_detail_status ? { order_detail_status: filter.order_detail_status } : {}),
					status: filter.status,
					date: filter.date,
					without_po: filter.without_po,
					with_inventories: 'true',
					with_history: 'true',
					have_order: 'true',
					no_shipment: 'false',
				}, this.state.token)
					.then(res => {
						if (id === this.getterId) {
							this.setState({
								isLoading: false,
								total: res.count,
								data: res.data,
							})
						}
					})
					.catch(err => {
						this.warn(err)

						this.setState({
							isLoading: false,
						}, () => {
							this.utilities.notification.show({
								title: 'Oops…',
								message: err ? err.detail || err.message || err : 'Something went wrong.',
							})
						})
					})
			})
		}

		onModalRequestClose = () => {
			this.props.utilities.alert.hide()
		}

		onNavigate = href => {
			this.navigator.navigate(href)
		}

		onChangeFilter = (key, val) => {
			const filter = { ...this.state.filter }
			filter[key] = val;

			this.setState({
				offset: 0,
				filter,
			})
		}

		onSearch = val => {
			this.setState({
				offset: 0,
				search: val,
			})
		}

		onNavigationChange = (offset, count) => {
			this.setState({
				offset,
				count,
			})
		}

		onToggleActive = id => {
			if(this.state.activeId !== id) {
				this.setState({
					activeId: id,
				})
			} else {
				this.setState({
					activeId: -1,
				})
			}
		}

		onQuickView = stylesheet => {
			if(stylesheet.order_id) {
				this.utilities.menu.show({
					title: 'Select quick view type',
					actions: [{
						title: 'Stylesheet',
						onPress: () => {
							this.utilities.alert.modal({
								component: (
									<QuickViewStylesheetPagelet
										id={ stylesheet.id }
										onNavigate={ this.onNavigate }
									/>
								),
							})
						},
					}, {
						title: 'Order',
						onPress: () => {
							this.utilities.alert.modal({
								component: (
									<QuickViewOrderPagelet
										id={ stylesheet.order_id }
										onClose={ this.onModalRequestClose }
										onNavigateToOrder={ () => {
											Linking.open(window.location.href.split('/')[0] + '/order/' + stylesheet.order_id)
										} }
									/>
								),
							})
						},
					}],
				})
			} else {
				this.utilities.alert.modal({
					component: (
						<QuickViewStylesheetPagelet
							id={ stylesheet.id }
							onClose={ this.onModalRequestClose }
							onNavigate={ this.onNavigate }
							onNavigateToStylesheet={ () => {
								Linking.open(window.location.href.split('/')[0] + '/stylesheet/' + stylesheet.id)
							} }
						/>
					),
				})
			}
		}

		onQuickViewInventory = inventory => {
			if (inventory.inventory_id) {
				this.utilities.alert.modal({
					component: (
						<QuickViewItemPagelet
							id={ inventory.inventory_id }
							type={ QuickViewItemPagelet.TYPES.INVENTORY }
						/>
					),
				})
			} else if(inventory.purchase_request_id) {
				this.utilities.alert.modal({
					component: (
						<QuickViewItemPagelet
							id={ inventory.purchase_request_id }
							type={ QuickViewItemPagelet.TYPES.PURCHASE_REQUEST }
						/>
					),
				})
			} else {
				this.utilities.alert.modal({
					component: (
						<QuickViewItemPagelet
							id={ inventory.id }
							type={ QuickViewItemPagelet.TYPES.STYLESHEET_INVENTORY }
						/>
					),
				})
			}
		}

		onNavigateToInventory = () => {
			Linking.open(window.location.href.split('/')[0] + '/inventory')
		}

		onReject = inventory => {
			if(inventory.purchase_order_id) {
				this.utilities.menu.show({
					actions: [{
						title: 'Put into inventory',
						type: this.utilities.menu.TYPES.SELECTION,
						onPress: this.onRejectAndPutIntoInventory.bind(this, inventory),
					}, {
						title: 'Reject request',
						type: this.utilities.menu.TYPES.SELECTION,
						onPress: this.onConfirmRejection.bind(this, inventory),
					}],
				})
			} else {
				this.onConfirmRejection(inventory)
			}
		}

		onConfirmRejection = inventory => {
			inventory.isLoading = 'rejecting'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why this purchase request need to be changed. *required"
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							PurchaseService.rejectRequest(inventory.purchase_request_id, note, this.state.token).then(isUpdated => {
								inventory.isLoading = false

								if (isUpdated) {
									inventory.status = 'REQUEST_REJECTED'
									inventory.note = note || inventory.note

									this.utilities.notification.show({
										title: 'Yeay!',
										message: 'Success rejecting request.',
										type: 'SUCCESS',
									})

									this.forceUpdate()
								} else {
									throw new Error('Rejection failed.')
								}
							}).catch(err => {
								inventory.isLoading = false
								this.forceUpdate()

								throw err
							}).catch(this.onError)
						} }
					/>
				),
			})
		}

		onRejectAndPutIntoInventory = inventory => {
			inventory.isLoading = 'rejecting'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why you put this into inventory. *required"
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							PurchaseService.getDetail(inventory.purchase_request_id, {
								basic: true,
							}, this.state.token).then(pr => {
								return PurchaseService.revokeRequest(inventory.purchase_request_id, inventory.purchase_order_id, note, {
									...pr,
								}, this.state.token).then(isUpdated => {
									if(isUpdated) {
										return PurchaseService.rejectRequest(inventory.purchase_request_id, note, this.state.token)
									} else {
										inventory.isLoading = false
										this.forceUpdate()

										throw new Error('Rejection failed.')
									}
								})
							}).then(isUpdated => {
								inventory.isLoading = false

								if (isUpdated) {
									inventory.status = 'REQUEST_REJECTED'
									inventory.note = note || inventory.note

									this.utilities.notification.show({
										title: 'Yeay!',
										message: 'Success rejecting request.',
										type: 'SUCCESS',
									})

									this.forceUpdate()
								} else {
									throw new Error('Rejection failed.')
								}
							}).catch(err => {
								inventory.isLoading = false
								this.forceUpdate()

								throw err
							}).catch(this.onError)
						} }
					/>
				),
			})

		}

		onApprove = inventory => {
			if(inventory.purchase_order_id) {
				this.utilities.menu.show({
					actions: [{
						title: 'Approve without changing',
						type: this.utilities.menu.TYPES.SELECTION,
						onPress: this.onConfirmApproval.bind(this, inventory),
					}, {
						title: 'Change request detail',
						type: this.utilities.menu.TYPES.SELECTION,
						onPress: this.onApproveWithEdit.bind(this, inventory),
					}],
				})
			} else {
				this.onConfirmApproval(inventory)
			}
		}

		onConfirmApproval = inventory => {
			inventory.isLoading = 'approving'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Add approval note for purchasing team to notice. (optional)"
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							PurchaseService.approveRequest(inventory.purchase_request_id, note, this.state.token).then(isUpdated => {
								inventory.isLoading = false

								if (isUpdated) {
									if (inventory.purchase_order_id) {
										inventory.status = 'PURCHASED'
									} else {
										inventory.status = 'REQUEST_APPROVED'
									}
									inventory.note = note || inventory.note

									this.utilities.notification.show({
										title: 'Yeay!',
										message: 'Success approving request.',
										type: 'SUCCESS',
									})

									this.forceUpdate()
								} else {
									throw new Error('Approval failed.')
								}
							}).catch(err => {
								inventory.isLoading = false
								this.forceUpdate()

								throw err
							}).catch(this.onError)
						} }
					/>
				),
			})
		}

		onApproveWithEdit = inventory => {
			inventory.isLoading = 'approving'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Add approval note for purchasing team to notice. (optional)"
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							this.utilities.alert.modal({
								component: (
									<PurchaseRequestUpdatePagelet editable
										id={ inventory.purchase_request_id }
										onUpdated={ () => {
											PurchaseService.approveRequest(inventory.purchase_request_id, note, this.state.token).then(isUpdated => {
												inventory.isLoading = false

												if (isUpdated) {
													if(inventory.purchase_order_id) {
														inventory.status = 'PURCHASED'
													} else {
														inventory.status = 'REQUEST_APPROVED'
													}
													inventory.note = note || inventory.note

													this.utilities.notification.show({
														title: 'Yeay!',
														message: 'Success approving request.',
														type: 'SUCCESS',
													})

													this.forceUpdate()
												} else {
													throw new Error('Approval failed.')
												}
											}).catch(err => {
												inventory.isLoading = false
												this.forceUpdate()

												throw err
											}).catch(this.onError)
										} }
									/>
								),
							})
						} }
					/>
				),
			})
		}

		onCancel = inventory => {
			inventory.isLoading = 'canceling'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why this approved purchase request got cancelled. *required"
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							PurchaseService.updateRequest(inventory.purchase_request_id, {
								status: 'PENDING',
								note,
							}, this.state.token).then(isUpdated => {
								inventory.isLoading = false

								if (isUpdated) {
									inventory.status = 'REQUESTING'
									inventory.note = note || inventory.note

									this.utilities.notification.show({
										title: 'Yeay!',
										message: 'Success canceling request.',
										type: 'SUCCESS',
									})

									this.forceUpdate()
								} else {
									throw new Error('Cancellation failed.')
								}
							}).catch(err => {
								inventory.isLoading = false
								this.forceUpdate()

								throw err
							}).catch(this.onError)
						} }
					/>
				),
			})
		}

		onRevoke = inventory => {
			inventory.isLoading = 'revoking'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why you cancel this purchase. *required"
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							PurchaseService.revokeRequest(inventory.purchase_request_id, inventory.purchase_order_id, note, undefined, this.state.token).then(isUpdated => {
								if (isUpdated) {
									inventory.status = 'REQUESTING'
									inventory.note = note || inventory.note

									this.utilities.notification.show({
										title: 'Yeay!',
										message: 'Success revoking request.',
										type: 'SUCCESS',
									})

									this.forceUpdate()
								} else {
									throw new Error('Revoke failed.')
								}
							}).catch(err => {
								inventory.isLoading = false
								this.forceUpdate()

								throw err
							}).catch(this.onError)
						} }
					/>
				),
			})
		}

		onPurchase = inventory => {
			inventory.isLoading = 'purchasing'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<PurchaseOrderPicker
						ids={[ inventory.purchase_request_id ]}
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
						} }
						onUpdate={ () => {
							inventory.status = 'PURCHASING'
							inventory.isLoading = false

							this.utilities.notification.show({
								title: 'Yeay!',
								message: 'Success adding request to purchase order.',
								type: 'SUCCESS',
							})

							this.forceUpdate()
						} }
					/>
				),
			})
		}

		onReceive = inventory => {
			inventory.isLoading = 'receiving'

			this.forceUpdate()

			PurchaseService.updateRequest(inventory.purchase_request_id, {
				status: 'RECEIVED',
			}, this.state.token).then(isUpdated => {
				inventory.isLoading = false

				if (isUpdated) {
					inventory.status = 'INVENTORY_RECEIVED'

					this.utilities.notification.show({
						title: 'Yeay!',
						message: 'Success receiving item.',
						type: 'SUCCESS',
					})

					this.forceUpdate()
				} else {
					throw new Error('Receiving failed.')
				}
			}).catch(err => {
				inventory.isLoading = false
				this.forceUpdate()

				throw err
			}).catch(this.onError)
		}

		onException = inventory => {
			inventory.isLoading = 'excepting'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why this inventory exceptioned. *required"
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							PurchaseService.despiseRequest(inventory.purchase_request_id, {
								quantity: 1,
								main: true,
								note,
							}, this.state.token).then(isUpdated => {
								inventory.isLoading = false

								if(isUpdated) {
									inventory.status = 'REQUEST_EXCEPTION'

									this.utilities.notification.show({
										title: 'Yeay!',
										message: 'Success excepting item.',
										type: 'SUCCESS',
									})

									this.forceUpdate()
								} else {
									throw new Error('Excepting failed.')
								}
							}).catch(err => {
								inventory.isLoading = false
								this.forceUpdate()

								throw err
							}).catch(this.onError)
						} }
					/>
				),
			})
		}

		onPack = inventory => {
			if(inventory.inventory_id) {
				const stylesheetId = this.state.activeId

				inventory.isLoading = 'packing'

				this.forceUpdate()

				const note = `Inventory packed into stylesheet #${stylesheetId}`

				InventoryService.cancelBooking(inventory.inventory_id, note, 'UNAVAILABLE', 'STYLESHEET', stylesheetId, false, this.state.token).then(isUpdated => {
					inventory.status = 'PACKED'
					inventory.isLoading = false

					this.utilities.notification.show({
						title: 'Yeay!',
						message: 'Success packing inventory.',
						type: 'SUCCESS',
					})

					this.forceUpdate()

					return isUpdated
				}).catch(err => {
					inventory.isLoading = false
					this.forceUpdate()

					throw err
				}).catch(this.onError)
			} else {
				if (inventory.variant_id) {
					this.onConfirmPacking(undefined, inventory, inventory.variant_id)
				} else {
					inventory.isLoading = 'packing'

					this.forceUpdate()

					this.utilities.alert.modal({
						component: (
							<PickerVariantPagelet
								onClose={ () => {
									inventory.isLoading = false
									this.forceUpdate()
								} }
								search={ inventory.title }
								onSelect={ this.onConfirmPacking.bind(this, undefined, inventory) }
								onNavigateToInventory={ this.onNavigateToInventory }
							/>
						),
					})
				}
			}
		}

		onConfirmPacking = (status, inventory, variantId) => {
			this.utilities.alert.modal({
				component: (
					<PurchaseRequestApprovePagelet
						status={ status }
						orderId={ inventory.purchase_order_id }
						variantId={ variantId }
						purchaseRequests={ [{
							id: inventory.purchase_request_id,
							isMain: true,
							quantity: 1,
						}] }
						onClose={ () => {
							inventory.isLoading = false
							this.forceUpdate()
						} }
						onCancel={ () => {
							if(status) {
								this.onChange(inventory)
							} else {
								this.onPack(inventory)
							}
						} }
						onApproved={ inventoryIds => {
							inventory.inventory_id = inventoryIds[0]

							if(!status) {
								this.onPack(inventory)
							} else {
								inventory.status = 'INVENTORY_EXCEPTION'
								inventory.isLoading = false

								this.forceUpdate()
							}
						} }
					/>
				),
			})
		}

		onUnpack = inventory => {
			const stylesheetId = this.state.activeId

			inventory.isLoading = 'packing'

			this.forceUpdate()

			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why this inventory need to be unpacked. *required"
						onCancel={ () => {
							inventory.isLoading = false
							this.forceUpdate()
							this.onModalRequestClose()
						} }
						onConfirm={ note => {
							InventoryService.unpackInventory(inventory.inventory_id, note, 'BOOKED', stylesheetId, this.state.token).then(() => {
								inventory.status = 'BOOKED'
								inventory.note = note || inventory.note
								inventory.isLoading = false

								// change stylesheet status
								const stylesheet = this.state.data.find(s => s.id === stylesheetId)

								if(stylesheet.order_detail_status === 'PRIMED') {
									stylesheet.order_detail_status = 'PROCESSING'
								}

								this.utilities.notification.show({
									title: 'Yeay!',
									message: 'Success unpacking inventory.',
									type: 'SUCCESS',
								})

								this.forceUpdate()
							}).catch(err => {
								inventory.isLoading = false
								this.forceUpdate()

								throw err
							}).catch(this.onError)
						} }
					/>
				),
			})
		}

		onChange = inventory => {
			this.utilities.menu.show({
				title: 'Change Into',
				actions: [{
					title: 'Available',
					type: this.utilities.menu.TYPES.SELECTION,
					disabled: true,
					description: '',
					onPress: () => {
						this.onConfirmChange(inventory, 'AVAILABLE')
					},
				}, {
					title: 'Defect',
					type: this.utilities.menu.TYPES.SELECTION,
					description: 'Inventory defect / cannot be used, stylist must change this inventory.',
					onPress: () => {
						this.onConfirmChange(inventory, 'DEFECT')
					},
				}, {
					title: 'Exception',
					type: this.utilities.menu.TYPES.SELECTION,
					description: 'Inventory made unavailable, stylist must change this inventory.',
					onPress: () => {
						this.onConfirmChange(inventory, 'EXCEPTION')
					},
				}],
			})
		}

		onConfirmChange = (inventory, status) => {
			if(inventory.inventory_id) {
				const stylesheetId = this.state.activeId

				inventory.isLoading = 'changing'

				this.forceUpdate()

				this.utilities.alert.modal({
					component: (
						<ModalPromptPagelet
							title="Add note"
							message="Tell us why this inventory need to be changed. *required"
							onCancel={ () => {
								inventory.isLoading = false
								this.forceUpdate()
								this.onModalRequestClose()
							} }
							onConfirm={ note => {
								InventoryService.unpackInventory(inventory.inventory_id, note, status, stylesheetId, this.state.token).then(() => {
									if(status === 'AVAILABLE') {
										inventory.status = 'INVENTORY_INVALID'
									} else {
										inventory.status = 'INVENTORY_EXCEPTION'
									}
									inventory.note = note || inventory.note
									inventory.isLoading = false

									this.utilities.notification.show({
										title: 'Yeay!',
										message: 'Success unpacking inventory.',
										type: 'SUCCESS',
									})

									this.forceUpdate()
								}).catch(err => {
									inventory.isLoading = false
									this.forceUpdate()

									throw err
								}).catch(this.onError)
							} }
						/>
					),
				})
			} else {
				if (inventory.variant_id) {
					this.onConfirmPacking(status, inventory, inventory.variant_id)
				} else {
					inventory.isLoading = 'changing'

					this.forceUpdate()

					this.utilities.alert.modal({
						component: (
							<PickerVariantPagelet
								onClose={ () => {
									inventory.isLoading = false
									this.forceUpdate()
								} }
								search={ inventory.title }
								onSelect={ this.onConfirmPacking.bind(this, status, inventory) }
								onNavigateToInventory={ this.onNavigateToInventory }
							/>
						),
					})
				}
			}
		}

		onSetVariant = inventory => {
			this.utilities.alert.modal({
				component: (
					<PickerVariantPagelet
						onClose={ () => {
							inventory.isLoading = false
							this.forceUpdate()
						} }
						search={ inventory.title }
						onSelect={ this.onConfirmSetVariant.bind(this, inventory) }
						onNavigateToInventory={ this.onNavigateToInventory }

						title={ inventory.title }
						brand={ inventory.brand }
						brandId={ inventory.brand_id }
						category={ inventory.category }
						categoryId={ inventory.category_id }
						size={ inventory.size }
						sizeId={ inventory.size_id }
						colors={ inventory.colors.map(color => color.title) }
						colorsIds={ inventory.colors.map(color => color.id) }
					/>
				),
			})
		}

		onConfirmSetVariant = (inventory, variantId) => {
			inventory.isLoading = 'setting'

			this.forceUpdate()

			PurchaseService.updateRequest(inventory.purchase_request_id, {
				variant_id: variantId,
			}, this.state.token).then(isUpdated => {
				inventory.isLoading = false

				if (isUpdated) {
					inventory.variant_id = variantId

					this.utilities.notification.show({
						title: 'Yeay!',
						message: 'Success setting variant.',
						type: 'SUCCESS',
					})

					this.forceUpdate()
				} else {
					throw new Error('Update failed.')
				}
			}).catch(err => {
				inventory.isLoading = false
				this.forceUpdate()

				throw err
			}).catch(this.onError)
		}

		onViewVariant = inventory => {
			this.utilities.alert.modal({
				component: (
					<QuickViewProductPagelet
						variantId={ inventory.variant_id }
						includeZeroInventory
					/>
				),
			})
		}

		onRefund = inventory => {
			if (inventory.inventory_id) {
				this.utilities.menu.show({
					title: 'Refund and Change Into',
					actions: [{
						title: 'Available',
						type: this.utilities.menu.TYPES.SELECTION,
						disabled: true,
						description: '',
						onPress: () => {
							this.onSetRefundNote(note => {
								this.onConfirmRefund(inventory, note, 'AVAILABLE')
							})
						},
					}, {
						title: 'Defect',
						type: this.utilities.menu.TYPES.SELECTION,
						description: 'Inventory defect / cannot be used, stylist must change this inventory.',
						onPress: () => {
							this.onSetRefundNote(note => {
								this.onConfirmRefund(inventory, note, 'DEFECT')
							})
						},
					}, {
						title: 'Exception',
						type: this.utilities.menu.TYPES.SELECTION,
						description: 'Inventory made unavailable, stylist must change this inventory.',
						onPress: () => {
							this.onSetRefundNote(note => {
								this.onConfirmRefund(inventory, note, 'EXCEPTION')
							})
						},
					}],
				})
			} else {
				this.onSetRefundNote(note => {
					this.onConfirmRefund(inventory, note)
				})
			}
		}

		onSetRefundNote = onConfirm => {
			this.utilities.alert.modal({
				component: (
					<ModalPromptPagelet
						title="Add note"
						message="Tell us why this inventory need to be refunded. *required"
						onCancel={ this.onModalRequestClose }
						onConfirm={ onConfirm }
					/>
				),
			})
		}

		onConfirmRefund = (inventory, note, status) => {
			const stylesheetId = this.state.activeId

			inventory.isLoading = 'refunding'

			this.forceUpdate()

			StylesheetService.onRefundInventory(inventory.id, status, note, this.state.token).then(() => {
				// change stylesheet status
				const stylesheet = this.state.data.find(s => s.id === stylesheetId)

				stylesheet.inventories.splice(stylesheet.inventories.findIndex(i => i.id === inventory.id), 1)

				if (!stylesheet.inventories.length) {
					stylesheet.order_detail_status = 'RESOLVED'
				}

				this.utilities.notification.show({
					title: 'Yeay!',
					message: 'Success refunding inventory.',
					type: 'SUCCESS',
				})

				this.forceUpdate()
			}).catch(err => {
				inventory.isLoading = false
				this.forceUpdate()

				throw err
			}).catch(this.onError)
		}

		onError = err => {
			this.warn(err)

			this.utilities.notification.show({
				title: 'Oops…',
				message: err ? (err.detail && err.detail.message || err.detail) || err.message || err : 'Something went wrong.',
			})
		}

		rowRenderer = stylesheet => {
			const isActive = this.state.activeId === stylesheet.id

			return {
				data: [{
					children: (
						<BoxBit style={ Styles.multiline }>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								<TextBit style={ Styles.id }>[#ST-{ stylesheet.id }]</TextBit> { stylesheet.title }
							</GeomanistBit>
							<GeomanistBit type={GeomanistBit.TYPES.NOTE_1} style={ Styles.note }>
								{ stylesheet.order ? `${ stylesheet.order } – ${ stylesheet.user }` : stylesheet.user }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: stylesheet.stylist,
				}, {
					title: '-',
					children: stylesheet.shipment_at ? (
						<BoxBit style={ Styles.multiline }>
							<GeomanistBit type={ GeomanistBit.TYPES.PARAGRAPH_3 }>
								{ TimeHelper.moment(stylesheet.shipment_at).format('DD/MM/YYYY') }
							</GeomanistBit>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 } style={Styles.note}>
								{ TimeHelper.moment(stylesheet.shipment_at).fromNow() }
							</GeomanistBit>
						</BoxBit>
					) : undefined,
				}, {
					title: '',
				}, {
					children: (
						<BadgeStatusLego status={ stylesheet.order_detail_status } />
					),
				}, {
					title: '',
				}, {
					children: (
						<BoxBit row style={ Styles.icons }>
							<TouchableBit unflex centering onPress={ this.onQuickView.bind(this, stylesheet) }>
								<IconBit
									name={ 'quick-view' }
									color={ Colors.primary }
								/>
							</TouchableBit>
						</BoxBit>
					),
				}, {
					children: (
						<BoxBit style={ Styles.dropdown }>
							<IconBit
								name={isActive ? 'dropdown-up' : 'dropdown'}
								size={20}
							/>
						</BoxBit>
					),
				}],
				children: isActive ? (
					<TableLego compact small isLoading={ this.state.isLoading }
						headers={ this.childHeaders }
						rows={ stylesheet.inventories.map(this.childRenderer) }
						style={ Styles.table } />
				) : false,
				onPress: this.onToggleActive.bind(this, stylesheet.id),
				style: !!stylesheet.isLoading ? Styles.loading : undefined,
			}
		}

		childRenderer = inventory => {
			const actions = this.actions(inventory)
			const disabled = !actions.length

			return {
				data: [{
					children: (
						<BoxBit style={ Styles.multiline }>
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_2 }>
								<TextBit style={Styles.id}>[{ inventory.seller }]</TextBit>
							</GeomanistBit>
							{ inventory.url ? (
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
									<LinkBit target={ LinkBit.TYPES.BLANK } href={ inventory.url } underline>
										{ inventory.brand } – { inventory.title }
									</LinkBit>
								</GeomanistBit>
							) : (
								<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
									{ inventory.brand } – { inventory.title }
								</GeomanistBit>
							) }
						</BoxBit>
					),
				}, {
					title: inventory.category,
				}, {
					title: '-',
					children: (
						<BoxBit row style={ Styles.colors }>
							<ColorLego colors={ inventory.colors } size={ 16 } radius={ 8 } border={ 0 } style={Styles.color} />
							<GeomanistBit type={ GeomanistBit.TYPES.NOTE_1 }>
								{ inventory.colors.map(color => capitalize(color.title)).join('/') }
							</GeomanistBit>
						</BoxBit>
					),
				}, {
					title: inventory.size,
				}, {
					children: (
						<BadgeStatusSmallLego tight status={ inventory.status } />
					),
				}, {
					title: inventory.note,
				}, {
					title: '',
				}, {
					children: (
						<BoxBit accessible={ !disabled } row unflex style={ Styles.actions }>
							{ disabled ? (
								<IconBit name="lock" size={ 16 } />
							) : actions.map(this.actionRenderer) }
						</BoxBit>
					),
				}],
				onPress: this.onQuickViewInventory.bind(this, inventory),
				disabled,
				style: disabled ? Styles.loading : undefined,
			}
		}

		actionRenderer = action => {
			return (
				<ButtonBit weight="medium"
					key={ action.slug }
					title={ action.title }
					theme={ action.theme }
					state={ action.state }
					onPress={ action.onPress }
					type={ ButtonBit.TYPES.SHAPES.PROGRESS }
					width={ ButtonBit.TYPES.WIDTHS.FIT }
					size={ ButtonBit.TYPES.SIZES.MIDGET }
					style={ Styles.button }
				/>
			)
		}

		view() {
			return (
				<PageAuthorizedComponent
					roles={ this.roles }
					onAuthorized={this.onAuthorized}
					paths={ this.paths }
				>
					{ () => (
						<BoxBit style={Styles.main}>
							<HeaderPageComponent
								title="Stylesheets"
								searchPlaceholder={ 'Order number' }
								onSearch={ this.onSearch } />
							<HeaderFilterComponent
								current={ this.state.offset }
								count={ this.state.count }
								total={ this.state.total }
								onNavigationChange={ this.onNavigationChange }>
								{/* <SelectStatusLego placeholder={ 'Stylesheet Status' } type={ SelectStatusLego.TYPES.STYLESHEET_STATUSES } status={ this.state.filter.status } onChange={this.onChangeFilter.bind(this, 'status')} /> */}
								<SelectStatusLego placeholder={ 'Order Detail Status' } type={ SelectStatusLego.TYPES.ORDER_DETAIL_STATUSES } status={ this.state.filter.order_detail_status } onChange={this.onChangeFilter.bind(this, 'order_detail_status')} />
								<SelectStatusLego placeholder={ 'Purchase Request Status' } type={ SelectStatusLego.TYPES.PURCHASE_REQUESTS } status={ this.state.filter.purchase_request_status } onChange={this.onChangeFilter.bind(this, 'purchase_request_status')} />
								<SelectionBit placeholder={ 'Purchase Order Status' } options={ this.POStatuses } onChange={ this.onChangeFilter.bind(this, 'without_po') } />
								<InputDateBit inside title="Shipment Date" value={ this.state.filter.date } onChange={this.onChangeFilter.bind(this, 'date')} />
							</HeaderFilterComponent>
							<TableLego isLoading={ this.state.isLoading }
								headers={ this.headers }
								rows={ this.state.data.map(this.rowRenderer) }
								style={Styles.padder} />
							<HeaderFilterComponent
								current={this.state.offset}
								count={this.state.count}
								total={this.state.total}
								onNavigationChange={this.onNavigationChange} />
						</BoxBit>
					) }
				</PageAuthorizedComponent>
			)
		}
	}
)
