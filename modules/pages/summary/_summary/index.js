import React from 'react'
import StatefulModel from 'coeur/models/stateful'
import ConnectHelper from 'coeur/helpers/connect'

import TabLego from 'modules/legos/tab'
import BoxRowLego from 'modules/legos/box.row'

import BoxBit from 'modules/bits/box'
import GeomanistBit from 'modules/bits/geomanist'
import LoaderBit from 'modules/bits/loader'

import Styles from './style'

export default ConnectHelper(
	class SummaryPart extends StatefulModel {

		constructor(p) {
			super(p, {
				isLoading: false,
				tabIndex: p.tabIndex || 0,
				token: undefined,
			})
		}

		tabs = [{
			title: 'Clicks',
			icon: 'circle-checkmark',
			onPress: this.onChangeTab.bind(this, 0),
		}, {
			title: 'Conversion',
			icon: 'shipments',
			onPress: this.onChangeTab.bind(this, 1),
		}, {
			title: 'Conversion Rate',
			icon: 'track',
			onPress: this.onChangeTab.bind(this, 2),
		}, {
			title: 'Orders',
			icon: 'purchases',
			onPress: this.onChangeTab.bind(this, 3),
		}, {
			title: 'Estimated Commissions',
			icon: 'wallet',
			onPress: this.onChangeTab.bind(this, 4),
		}]

		onChangeTab(tabIndex) {
			this.setState({
				tabIndex,
			})
		}

		contentRenderer = index => {
			switch (index) {
			case 0:
			default:
				return (
					<BoxBit unflex row>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Total Clicks : #number
							</GeomanistBit>
							<BoxBit style={Styles.lineChart}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									Display Line Chart for clicks
								</GeomanistBit>
							</BoxBit>
							<BoxBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									List Clicks over period of x time
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>
				)
			case 1:
				return (
					<BoxBit unflex row>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Total Conversions : #number
							</GeomanistBit>
							<BoxBit style={Styles.lineChart}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									Display Line Chart for conversions
								</GeomanistBit>
							</BoxBit>
							<BoxBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									List conversions over period of x time
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>
				)
			case 2:
				return (
					<BoxBit unflex row>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Conversion Percent : %percent
							</GeomanistBit>
							<BoxBit style={Styles.lineChart}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									Display Line Chart for conversion percent (?)
								</GeomanistBit>
							</BoxBit>
							<BoxBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									List conversion percent over period of x time
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>
				)
			case 3:
				return (
					<BoxBit unflex row>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Total Order : #number
							</GeomanistBit>
							<BoxBit style={Styles.lineChart}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									Display Line Chart for orders (?)
								</GeomanistBit>
							</BoxBit>
							<BoxBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									List orders over period of x time
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>
				)
			case 4:
				return (
					<BoxBit unflex row>
						<BoxBit>
							<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
								Total Estimated Commisions : $number
							</GeomanistBit>
							<BoxBit style={Styles.lineChart}>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									Display Line Chart for estimated commissions (?)
								</GeomanistBit>
							</BoxBit>
							<BoxBit>
								<GeomanistBit type={GeomanistBit.TYPES.PARAGRAPH_3}>
									List estimated commissions over period of x time
								</GeomanistBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>
				)
			}
		}

		view() {
			return (
				<BoxRowLego
					title="Sum Text"
					data={[{
						data: [{
							title: '',
							children: (
								<TabLego activeIndex={this.state.tabIndex} tabs={this.tabs}/>
							),
						}],
					}, {
						data: [{
							title: '',
							children: (
								this.state.isLoading ? (
									<BoxBit centering>
										<LoaderBit simple />
									</BoxBit>
								) : this.contentRenderer(this.state.tabIndex)
							),
						}],
					}]}
				/>
			)
		}
	}
)
