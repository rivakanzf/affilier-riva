module.exports = {
	apps: [{
		name: 'Affilier',
		script: './server/index.js',
		instances: 'max',
		env: {
			NODE_PATH: './',
		},
	}],
	deploy: {
		development: {
			user: 'arch',
			host: '13.212.96.107',
			ref: 'origin/release-2.3',
			repo: 'git@bitbucket.org:yuna-frontend/bureau.git',
			path: '/home/arch/bureau',
			'pre-deploy': 'git reset --hard',
			'post-deploy': './scripts/setenv development && yarn install && npm run bundle && pm2 reload ./scripts/ecosystem.config.js && pm2 ls',
		},
		staging: {
			user: 'arch',
			host: '54.254.132.42',
			ref: 'origin/release-2.3',
			repo: 'git@bitbucket.org:yuna-frontend/bureau.git',
			path: '/home/arch/bureau',
			'pre-deploy': 'git reset --hard',
			'post-deploy': './scripts/setenv staging && git submodule update --init && npm i && npm run bundle && pm2 reload ./scripts/ecosystem.config.js && pm2 ls',
		},
		production: {
			user: 'arch',
			host: '13.212.208.55',
			ref: 'origin/release-2.3',
			repo: 'git@bitbucket.org:yuna-frontend/bureau.git',
			path: '/home/arch/bureau',
			'pre-deploy': 'git reset --hard',
			'post-deploy': './scripts/setenv production && git submodule update --init && npm i && npm run bundle && pm2 reload ./scripts/ecosystem.config.js && pm2 ls',
		},
	},
}
