const express = require('express')
const fs = require('fs')
const DotEnv = require('dotenv')
const app = express()

DotEnv.config()

const port = Number(process.env.PORT) + Number(process.env.NODE_APP_INSTANCE || 0)

app.use(express.static('public'))
app.get('*', function(req, res) {
	fs.readFile(`public${req.path}.html`, function(err, file) {
		if (err) {
			fs.readFile('public/index.html', function(e, index) {
				res.type('html').send(index)
			})
		} else {
			res.type('html').send(file);
		}
	});
})

app.listen(port, function() {
	console.log('Affilier listening on port ' + port + '!')
})
