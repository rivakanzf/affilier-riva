export default {
	getUserAgent() {
		return navigator.userAgent
	},
	getSystemName() {
		return navigator.platform
	},
	getDeviceId() {
		return null
	},
}
